module Main where

import Bench qualified (main)

main :: IO ()
main = Bench.main
