# Architecture

This page is a brief overview of the codebase for Pixelpusher. I hope it helps
you find your way around.


## Client, Server, and Shared Library

Pixelpusher is a server-based networked multiplayer game. Its codebase is split
into three parts: (1) a shared library, (2) a client, and (3) a server.

1. The shared library implements the entirety of the game logic, which runs on
   both clients and servers. Crucially, the game logic is a deterministic
   function depending only on player inputs and the current game state. This
   allows the server to synchronize the clients' game states just by
   broadcasting player inputs to the clients.

2. The client collects player inputs, sends them to the server, receives the
   inputs of other players, uses those inputes to update a local copy of the
   game state, and displays the game state to the player through visuals and
   sounds.

3. The server receives player inputs from each of the clients and broadcasts
   snapshots of the player inputs. It also updates a copy of the game
   state to send to new players joining a game.


## Shared Library

The shared library is contained in the [`/src`](/src) directory.


### [`Pixelpusher.Game`](/src/Pixelpusher/Game)

The game logic.

The main path of the game logic is partitioned into layers. From outermost to
innermost, they are:

- [`Pixelpusher.Game.State`](/src/Pixelpusher/Game/State.hs)
- [`Pixelpusher.Game.State.Outer`](/src/Pixelpusher/Game/State/Outer.hs)
- [`Pixelpusher.Game.State.Store`](/src/Pixelpusher/Game/State/Store.hs)
- [`Pixelpusher.Game.State.Core`](/src/Pixelpusher/Game/State/Core.hs)

[`Pixelpusher.Game.Bots.AI`](/src/Pixelpusher/Game/Bots/AI.hs) is the entry point to the bot logic.

### [`Pixelpusher.Store`](/src/Pixelpusher/Store)

An implementation of the [Sparse Set](https://manenko.com/2021/05/23/sparse-sets.html) data structure.

Also defines the custom index types used to store and retrieve data from our
sparse sets (see [`Pixelpusher.Store.EntityID`](/src/Pixelpusher/Store/EntityID.hs)).


### [`Pixelpusher.Network`](/src/Pixelpusher/Network)

The protocols used by the client and server to communicate.


### [`Pixelpusher.Custom`](/src/Pixelpusher/Custom)

Ad hoc bits and pieces.


## Client

The client is contained in the [`/client`](/client) directory. The entry point is
[`Client.Main`](/client/Client/Main.hs).


### [`Client.GameLoop`](/client/Client/GameLoop.hs)

The game loop.


### [`Client.Visuals`](/client/Client/Visuals)

[`Client.Visuals.Game`](/client/Client/Visuals/Game.hs) is the entry point to the in-game visuals.

[`Client.Visuals.Env`](/client/Client/Visuals/Env.hs) defines the client's rendering context.

[`Client.Visuals.Shaders`](/client/Client/Visuals/Shaders) contains the game's visual assets
(which are just shaders).


### [`Client.Audio`](/client/Client/Audio)

[`Client.Audio.Game`](/client/Client/Audio/Game.hs) is the entry point to the in-game audio.

[`Client.Audio.Env`](/client/Client/Audio/Env.hs) defines the client's audio context.


## Server

The server is contained in the [`/server`](/server) directory. The entry point is
[`Server.Main`](/server/Server/Main.hs)


## Sound effects

The game's sound effects are contained in the [`/sounds`](/sounds) directory.

The sound effects are present in two forms:

1. As `.opus` audio files, which are imported and played by the game client;
   and
2. As `.dsp` [Faust](https://faust.grame.fr/) source files, which are compiled
   to the `.opus` files (see [`/sounds/justfile`](/sounds/justfile)).
