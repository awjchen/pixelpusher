# Pixelpusher

<img src="./screenshots/vfx.png" width="960">

Pixelpusher is a drone-based team combat game that emphasizes movement and positioning above all else. Control space and manage risk with every move of your drones. Play capture the flag with up to 32 players.

Based on my favourite part of [diep.io](https://diep.io).

  [Trailer](https://youtu.be/LlgfU41jvCI)
| [Steam](https://store.steampowered.com/app/2495130/Pixelpusher/)

## Play

To play, download the latest client from
[Steam](https://store.steampowered.com/app/2495130/Pixelpusher/) or
[itch.io](https://aetup.itch.io/pixelpusher).
Alternatively, build the client from source using the build instructions below.

### Controls

|          |         |
| ------------- | ------------- |
| Move overseer | WASD |
| Dash overseer | Space+WASD |
| Psionic storm | Left Shift |
| Drop flag     | Left Alt |
| Attract drones | Release both mouse buttons |
| Charge drone dash | Hold either mouse button |
| Repel drones | Hold both mouse buttons |
| Show help | F1-F5 |
| Menu | Escape |


You *really* need a mouse. Keybindings can be customized.

## Build from source (stack)

Build the game using the [stack](https://docs.haskellstack.org) build tool.

### macOS

```
brew install openal-soft
brew install opusfile
stack run pixelpusher-client --extra-lib-dirs=/opt/homebrew/opt/openal-soft/lib --extra-lib-dirs=/opt/homebrew/opt/opusfile/lib
```

### Ubuntu (20.04 LTS)

```
sudo apt install libglfw3-dev libopenal-dev libopusfile-dev libxi-dev libxxf86vm-dev libxcursor-dev libxinerama-dev pkg-config zlib1g-dev
stack run pixelpusher-client
```

## Build from source (Nix)

Alternatively, you can build the game using [Nix](https://nixos.org/).

Note: If you are not using NixOS, you may need to use [nixGL](https://github.com/guibou/nixGL) to run the client executable:

```
nixGL /path/to/pixelpusher-client
```

### Stack

```
stack --nix run pixelpusher-client
```

### Nix

```
nix-build
./result/bin/pixelpusher-client
```

### Nix flakes

```
nix run gitlab:awjchen/pixelpusher
```

## Host a server

The game server `pixelpusher-server` listens on port 30303 over both TCP and
UDP.

To change the server configuration,
edit the server's configuration files (automatically created on startup)
and reload them from the server's console.
