-- This module is empty in order to work around the fact that stack cannot load
-- more than one main module at a time.

module Main where

import Headless.Main qualified

main :: IO ()
main = Headless.Main.headlessMain
