{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Headless.Main (
  headlessMain,
) where

import Control.Concurrent (setNumCapabilities, threadDelay)
import Control.Concurrent.Async
import Control.Monad (forever, replicateM, unless)
import Control.Monad.ST
import Data.Foldable (for_)
import Data.IORef
import Data.List (intercalate)
import Data.Text.IO qualified as Text
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (doesFileExist)
import System.IO
import System.Log.FastLogger
import System.Log.FastLogger qualified as FastLogger
import System.Random.MWC qualified as MWC
import Toml qualified

import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.State
import Pixelpusher.Game.Team

import Headless.CommandLine

--------------------------------------------------------------------------------

headlessMain :: IO ()
headlessMain =
  withLogger $ \logger -> do
    writeLog logger "Starting pixelpusher-headless"
    parseArgs >>= \case
      RunHeadlessBotGame headlessParams -> do
        hSetBuffering stdout NoBuffering
        loadGameParameters logger (hbgp_gameParamsFilepath headlessParams)
          >>= \case
            Nothing ->
              putStrLn "Could not load game parameters. See logs for details."
            Just gameParams ->
              runHeadlessBotGame headlessParams gameParams
      WriteGameParameters -> do
        hSetBuffering stdout LineBuffering
        writeGameParameters logger
    writeLog logger "Exiting pixelpusher-headless"

withLogger :: (TimedFastLogger -> IO a) -> IO a
withLogger action = do
  getFormattedTime <- FastLogger.newTimeCache FastLogger.simpleTimeFormat
  let logType =
        LogFile
          FileLogSpec
            { log_file = "pixelpusher-headless.log"
            , log_file_size = 4000 * 1024
            , log_backup_number = 2
            }
          FastLogger.defaultBufSize
  withTimedFastLogger getFormattedTime logType action

-- Default log format
writeLog :: TimedFastLogger -> LogStr -> IO ()
writeLog logger logStr =
  logger $ \timeStr -> toLogStr timeStr <> " " <> logStr <> "\n"

--------------------------------------------------------------------------------

-- | Run simulations to determine win ratios between bot AIs.
runHeadlessBotGame :: HeadlessBotGameParams -> BaseGameParams -> IO ()
runHeadlessBotGame HeadlessBotGameParams{..} gameParams = do
  setNumCapabilities hbgp_numCapabilities

  scoreRefs <- replicateM hbgp_numCapabilities (newIORef initialScore)
  gameAsyncs <- traverse (async . runGame gameParams) scoreRefs
  readerAsync <-
    async $ do
      let loop = do
            threadDelay 1_000_000 -- microseconds
            totalScore <- mconcat <$> traverse readIORef scoreRefs
            putStr $
              concat
                [ "\rWins "
                , show (westVictories totalScore)
                , "-"
                , show (eastVictories totalScore)
                ]
            let Score{westVictories, eastVictories} = totalScore
                maxGamesReached =
                  case hbgp_maxGames of
                    Nothing -> False
                    Just maxGames ->
                      westVictories + eastVictories >= maxGames
                minGamesReached =
                  case hbgp_minGames of
                    Nothing -> True
                    Just minGames ->
                      westVictories + eastVictories >= minGames
                sigmaReached =
                  case hbgp_maxSigma of
                    Nothing -> False
                    Just maxSigma ->
                      let sigma =
                            fromIntegral @_ @Double (eastVictories - westVictories)
                              / sqrt (fromIntegral (eastVictories + westVictories))
                      in  abs sigma > maxSigma
            unless
              ((maxGamesReached || sigmaReached) && minGamesReached)
              loop
      loop
  _ <- waitAnyCancel (readerAsync : gameAsyncs)
  pure ()

runGame :: BaseGameParams -> IORef Score -> IO ()
runGame params scoreRef = do
  gen <- MWC.createSystemSeed >>= MWC.restore
  gameState <- stToIO $ initializeGameStateWithGen params gen
  forever $ do
    gameEvents <- stToIO $ integrateGameState mempty mempty Nothing gameState
    for_ (ge_gamePhaseTransitions gameEvents) $ \case
      MatchEnd winningTeam ->
        modifyIORef' scoreRef $ \score ->
          case winningTeam of
            TeamW -> score{westVictories = westVictories score + 1}
            TeamE -> score{eastVictories = eastVictories score + 1}
      _ -> pure ()

--------------------------------------------------------------------------------

data Score = Score
  { westVictories :: Int
  , eastVictories :: Int
  }

initialScore :: Score
initialScore = Score 0 0

instance Semigroup Score where
  Score w1 e1 <> Score w2 e2 = Score (w1 + w2) (e1 + e2)

instance Monoid Score where
  mempty = initialScore

--------------------------------------------------------------------------------

-- Note: We should never overwrite the game parameters file, if it exists.
loadGameParameters :: TimedFastLogger -> FilePath -> IO (Maybe BaseGameParams)
loadGameParameters logger gameParamsFilePath =
  (fmap . fmap) makeBaseGameParams $
    doesFileExist gameParamsFilePath >>= \case
      False -> do
        writeLog logger $
          toLogStr $
            concat
              [ "Could not find/open '"
              , gameParamsFilePath
              , "'."
              ]
        pure Nothing
      True -> do
        parseResult <- Toml.decode <$> Text.readFile gameParamsFilePath
        case parseResult of
          Toml.Failure errors -> do
            writeLog logger $
              toLogStr $
                "Error reading game parameters: " <> intercalate "; " errors
            pure Nothing
          Toml.Success warnings gameParams -> do
            unless (null warnings) $ do
              writeLog logger $
                toLogStr $
                  "Warnings encountered while reading game parameters: "
                    <> intercalate "; " warnings
            pure $! Just gameParams

writeGameParameters :: TimedFastLogger -> IO ()
writeGameParameters logger = do
  withWriteFileViaTemp defaultGameParamsFilePath $ \h ->
    Prettyprinter.renderIO h $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode defaultTomlGameParams
  let msg = "Default game parameters written to " <> defaultGameParamsFilePath
  writeLog logger $ toLogStr msg
  putStrLn msg

defaultGameParamsFilePath :: FilePath
defaultGameParamsFilePath = "pixelpusher-params.toml"
