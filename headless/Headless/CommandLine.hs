{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Headless.CommandLine (
  Command (..),
  HeadlessBotGameParams (..),
  parseArgs,
) where

import Options.Applicative

data Command
  = RunHeadlessBotGame HeadlessBotGameParams
  | WriteGameParameters

data HeadlessBotGameParams = HeadlessBotGameParams
  { hbgp_gameParamsFilepath :: FilePath
  , hbgp_numCapabilities :: Int
  , hbgp_maxGames :: Maybe Int
  , hbgp_minGames :: Maybe Int
  , hbgp_maxSigma :: Maybe Double
  }

parseArgs :: IO Command
parseArgs =
  execParser $
    info (helper <*> headlessCommand) $
      header ">>= Pixelpusher Headless =<<"

headlessCommand :: Parser Command
headlessCommand =
  WriteGameParameters
    <$ writeGameParams
      <|> fmap RunHeadlessBotGame runHeadlessBotGame

writeGameParams :: Parser ()
writeGameParams =
  flag () () $
    mconcat
      [ long "write-game-parameters"
      , short 'W'
      , help "Write out default game parameters"
      ]

runHeadlessBotGame :: Parser HeadlessBotGameParams
runHeadlessBotGame = do
  hbgp_gameParamsFilepath <-
    strOption $
      mconcat
        [ long "game-params"
        , short 'p'
        , metavar "FILE"
        , help "Game parameters to simulate"
        ]

  hbgp_numCapabilities <-
    option auto $
      mconcat
        [ short 'N'
        , help "Number of simulations to run in parallel"
        , value 1
        ]

  hbgp_maxGames <-
    optional $
      option auto $
        mconcat
          [ long "max-games"
          , short 'G'
          , metavar "N"
          , help "Stop simulating when the total number simulated reaches this value"
          ]

  hbgp_minGames <-
    optional $
      option auto $
        mconcat
          [ long "min-games"
          , short 'g'
          , metavar "N"
          , help "Minimum number of games to simulate before stopping"
          ]

  hbgp_maxSigma <-
    optional $
      option auto $
        mconcat
          [ long "max-standard-deviations"
          , short 'S'
          , help $
              "Stop simulating when the observed win ratio is more than"
                <> " the specified number of standard deviations away from"
                <> " a ratio of 1:1"
          ]

  pure HeadlessBotGameParams{..}
