{-
A /game server/ is a thread that runs an isolated game session.

It collects and, in effect, timestamps player inputs and broadcasts collection
of inputs to all players.

It also maintains a copy of the game state in order to allow new players to
join after a game has started.

Information external to the game (e.g. player communication) is handled without
informing the game logic.

Also, the game server has a rudimentary implementation for saving replays of
games.
-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module Server.GameServer (
  GameServerEnv (..),
  GameServerHandle,
  gsh_close,
  gsh_putPlayerInput,
  gsh_issueCommand,
  spawnGameServer,
) where

import Control.Concurrent (forkIO)
import Control.Concurrent.Chan.Unagi.Bounded qualified as UCB
import Control.Exception
import Control.Monad (forM_, forever, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.ST
import Control.Monad.Trans.State.Strict
import Data.Bifunctor (second)
import Data.ByteString qualified as BS
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields
import Data.IORef
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict qualified as Map
import Data.Maybe (catMaybes, mapMaybe)
import Data.Monoid (Endo (..))
import Data.Serialize qualified as Serialize
import Data.Traversable (for)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory)
import System.IO
import System.Log.FastLogger

import Pixelpusher.Custom.Clock (runClock)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.SmallList (makeSmallList)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameCommand (GameCommand (..))
import Pixelpusher.Game.GameEvents (
  GameEvents (ge_gamePhaseTransitions),
  GamePhaseTransition (GameRestart),
 )
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls (fromControlsTransport)
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.Protocol
import Pixelpusher.Game.State
import Pixelpusher.Game.Time

import Server.GameServerCommand (GameServerCommand (..))
import Server.Logging
import Server.Network.Interface (OutgoingMsg (..), TCPClientCommand (..))
import Server.Network.UserID (UserID)
import Server.PlayerInputBuffer

--------------------------------------------------------------------------------
-- Interface types

data GameServerEnv = GameServerEnv
  { gse_logger :: TimedFastLogger
  , gse_putOutgingMsg :: OutgoingMsg ServerGameMsg -> IO ()
  , gse_mReplayFile :: Maybe FilePath
  , gse_getMaxIdleSeconds :: IO Int
  }

data GameServerHandle = GameServerHandle
  { _gsh_close :: IO ()
  , _gsh_putPlayerInput :: UserID -> ClientGameMsg -> IO ()
  , _gsh_issueCommand :: GameServerCommand -> IO ()
  }

gsh_close :: GameServerHandle -> IO ()
gsh_close = _gsh_close

gsh_putPlayerInput :: GameServerHandle -> UserID -> ClientGameMsg -> IO ()
gsh_putPlayerInput = _gsh_putPlayerInput

gsh_issueCommand :: GameServerHandle -> GameServerCommand -> IO ()
gsh_issueCommand = _gsh_issueCommand

data GameServerException = GSE_TooManyGameCommands
  deriving (Show)

instance Exception GameServerException

--------------------------------------------------------------------------------
-- Internal types

-- | An exception for killing game servers
data GameCancelled = GameCancelled
  deriving stock (Show)

instance Exception GameCancelled

-- | Game server internal state
data GameServerState = GameServerState
  { gss_players :: WIM.IntMap UserID PlayerID
  , gss_observers :: WIS.IntSet UserID
  , gss_playerIDPool :: PlayerIDPool
  }
  deriving stock (Generic)

initialGameServerState :: GameServerState
initialGameServerState =
  GameServerState
    { gss_players = WIM.empty
    , gss_observers = WIS.empty
    , gss_playerIDPool = newPlayerIDPool C.maxPlayersPerGame
    }

-- | Process `GameServerCommand`s into `GameCommand`s, reporting new players.
processServerCommands ::
  (Monad m) =>
  [GameServerCommand] ->
  StateT
    GameServerState
    m
    ([GameCommand], [(UserID, PlayerID)], [UserID])
processServerCommands gameServerCmds = do
  (gameCmdsMaybe, playerChanges, observerChanges) <-
    unzip3 <$> mapM processServerCommand gameServerCmds
  -- Simulate the addition and removal of players and observers
  let gameCmds = catMaybes gameCmdsMaybe
      newPlayers =
        WIM.toList $
          -- Note: The order of composition is crucial. We want simulate the
          -- execution of the game commands in the order they are given to us.
          foldr (\(Endo f) acc -> acc . f) id playerChanges WIM.empty
      newObservers =
        WIS.toList $
          foldr (\(Endo f) acc -> acc . f) id observerChanges WIS.empty
  pure (gameCmds, newPlayers, newObservers)

-- Note: This function should not be used directly.
processServerCommand ::
  (Monad m) =>
  GameServerCommand ->
  StateT
    GameServerState
    m
    ( Maybe GameCommand
    , Endo (WIM.IntMap UserID PlayerID) -- updates to the players
    , Endo (WIS.IntSet UserID) -- updates to the observers
    )
processServerCommand cmd = case cmd of
  ServerPlayerJoin uid name playerClass ->
    field @"gss_playerIDPool" %%= borrowPlayerID >>= \case
      Nothing ->
        -- This should not happen. We currently ignore the error, though we
        -- could instead kick the user and log the error.
        pure (Nothing, Endo id, Endo id)
      Just pid -> do
        field @"gss_players" %= WIM.insert uid pid
        let playerJoin = PlayerJoin pid name playerClass Nothing
        pure (Just playerJoin, Endo (WIM.insert uid pid), Endo id)
  ServerPlayerLeave uid ->
    field @"gss_players" . at uid <<.= Nothing >>= \case
      Nothing ->
        -- This should not happen. We currently ignore the error.
        pure (Nothing, Endo id, Endo id)
      Just pid -> do
        field @"gss_playerIDPool" %= returnPlayerID pid
        pure (Just (PlayerLeave pid), Endo (WIM.delete uid), Endo id)
  ServerObserverJoin uid -> do
    field @"gss_observers" %= WIS.insert uid
    pure (Nothing, Endo id, Endo (WIS.insert uid))
  ServerObserverLeave uid -> do
    field @"gss_observers" %= WIS.delete uid
    pure (Nothing, Endo id, Endo (WIS.delete uid))
  ServerSwitchParams newParams ->
    pure (Just (SwitchGameParams newParams), Endo id, Endo id)

--------------------------------------------------------------------------------

spawnGameServer ::
  GameServerEnv ->
  BaseGameParams ->
  Int -> -- Room ID
  (Either SomeException [GamePhaseTransition] -> IO ()) ->
  IO GameServerHandle
spawnGameServer env gameParams roomID putGameMsg = do
  playerInputBuffer <- newPlayerInputBuffer

  mReplayWriter <-
    for (gse_mReplayFile env) $ \filePath -> do
      (queueIn, queueOut) <- UCB.newChan 1024 -- bounded channel
      let logReplayException (e :: SomeException) = do
            writeLog (gse_logger env) $ toLogStr $ show e
            throwIO e
      threadId <-
        forkIO $
          handle @SomeException logReplayException $ -- Report replay writer exceptions
            handle (\GameCancelled -> pure ()) $ -- Swallow game-cancelling exceptions
              replayWriter (gse_logger env) queueOut filePath
      let putReplayOutput =
            -- Block when queue is full. Replays should only be captured during
            -- playtests where this would be okay.
            UCB.writeChan queueIn
          killReplayWriter = throwTo threadId GameCancelled
      pure (putReplayOutput, killReplayWriter)
  let mPutReplayOutput = fmap fst mReplayWriter
      mKillReplayWriter = fmap snd mReplayWriter

  threadId <-
    forkIO $
      handle @SomeException (putGameMsg . Left) $ -- Report game server crashes
        handle (\GameCancelled -> pure ()) $ -- Swallow game-cancelling exceptions
          runGameServer
            env
            gameParams
            roomID
            playerInputBuffer
            (putGameMsg . Right)
            mPutReplayOutput
  pure $
    GameServerHandle
      { _gsh_close =
          throwTo threadId GameCancelled >> sequence_ mKillReplayWriter
      , _gsh_putPlayerInput = putPlayerInput playerInputBuffer
      , _gsh_issueCommand = addGameServerCommand playerInputBuffer
      }

runGameServer ::
  GameServerEnv ->
  BaseGameParams ->
  Int -> -- Room ID
  PlayerInputBuffer ->
  ([GamePhaseTransition] -> IO ()) ->
  Maybe (ReplayOutput -> IO ()) ->
  IO a
runGameServer env gameParams roomID playerInputBuffer putGameEvent mPutReplayOutput = do
  -- Intialize
  gameState <- initializeGameState gameParams
  -- Record initial state
  for_ mPutReplayOutput $ \putReplayOutput -> do
    snapshot <- stToIO $ freeze gameState
    putReplayOutput $ ReplayNewGame snapshot
  -- Loop
  flip evalStateT initialGameServerState $
    runClock C.tickDelay_ns $
      gameServer
        (gse_logger env)
        roomID
        playerInputBuffer
        putGameEvent
        (gse_putOutgingMsg env)
        mPutReplayOutput
        gameState
        (gse_getMaxIdleSeconds env)

data ReplayOutput
  = ReplayNewGame GameStateSnapshot
  | ReplayUpdate GameStateUpdate

-- | Thread for writing replays
replayWriter ::
  TimedFastLogger ->
  UCB.OutChan ReplayOutput ->
  FilePath ->
  IO a
replayWriter logger replayOutputsQueue baseFilePath = do
  counterRef <- newIORef (1 :: Int)
  handleRef <- newIORef Nothing

  -- Try hard to save replays on exit
  uninterruptibleMask $ \restore -> do
    let loopHandler (e :: SomeException) = do
          readIORef handleRef >>= traverse_ hClose
          throwIO e
    handle loopHandler . restore $
      forever $
        UCB.readChan replayOutputsQueue >>= \case
          ReplayUpdate gameStateUpdate -> do
            readIORef handleRef >>= traverse_ \h ->
              BS.hPut h $ Serialize.encode gameStateUpdate
          ReplayNewGame gameStateSnapshot -> do
            readIORef handleRef >>= traverse_ hClose
            count <- readIORef counterRef
            modifyIORef' counterRef succ

            -- Create directory if missing
            do
              let dir = takeDirectory baseFilePath
                  createDirHandler (e :: IOException) = do
                    writeLog logger $
                      toLogStr $
                        concat
                          [ "Error: Could not create directory at '"
                          , dir
                          , "' for writing replay"
                          ]
                    throwIO e -- just giving up
              handle createDirHandler $ createDirectoryIfMissing True dir

            -- Open file
            do
              let filePath = baseFilePath <> "-" <> show count
                  openFileHandler (_ :: IOException) = do
                    writeLog logger $
                      toLogStr $
                        concat
                          [ "Warning: Could not open file '"
                          , filePath
                          , "' for writing replay"
                          ]
                    pure Nothing
              mask_ $ do
                maybeHandle <-
                  handle openFileHandler $ do
                    h <- openFile filePath WriteMode
                    hSetBuffering h (BlockBuffering Nothing)
                    pure $ Just h
                writeIORef handleRef maybeHandle

            -- Write initial snapshot
            readIORef handleRef >>= traverse_ \h -> do
              BS.hPut h $ Serialize.encode gameStateSnapshot

gameServer ::
  TimedFastLogger ->
  Int -> -- Room ID
  PlayerInputBuffer ->
  ([GamePhaseTransition] -> IO ()) ->
  (OutgoingMsg ServerGameMsg -> IO ()) ->
  Maybe (ReplayOutput -> IO ()) ->
  GameState RealWorld ->
  IO Int ->
  StateT GameServerState IO ()
gameServer
  logger
  roomID
  playerInputBuffer
  putGameEvent
  putMsg
  mPutReplayOutput
  gameState
  getMaxIdleSeconds = do
    -- Poll for inputs
    (gameServerCmds, debugCmd, userCommands) <-
      liftIO $ takePlayerInputs playerInputBuffer

    -- Process game server commands
    (serverGameCmds, newPlayers, newObservers) <-
      processServerCommands gameServerCmds

    players <- gets gss_players
    let playerCommands =
          WIM.fromList
            $ mapMaybe -- ignoring errors
              (\(uid, ctrls) -> (,ctrls) <$> WIM.lookup uid players)
            $ Map.toList userCommands
    observers <- gets gss_observers

    -- Drop down to IO for the rest ...
    liftIO $ do
      -- Send delta-updates first to minimize latency
      smallServerGameCmds <-
        maybe (throwIO GSE_TooManyGameCommands) pure $
          makeSmallList serverGameCmds
      oldTime <- stToIO $ getGameSyncTime gameState
      let update =
            GameStateUpdate
              { deltaUpdate_time = nextSyncTick oldTime
              , deltaUpdate_playerControls = makePlayerIDMap32 playerCommands
              , deltaUpdate_gameCommands = smallServerGameCmds
              , deltaUpdate_debugCommand = debugCmd
              }
      putMsg $
        UDPBroadcast (WIM.keys players ++ WIS.toList observers) $
          ServerGameData $
            ServerDeltaUpdate update

      -- Step game state, forward game events to manager
      let playerCommands' = WIM.map fromControlsTransport playerCommands
      phaseTransitions <-
        fmap ge_gamePhaseTransitions . stToIO $
          integrateGameState playerCommands' serverGameCmds debugCmd gameState
      putGameEvent phaseTransitions

      -- Output to replay
      for_ mPutReplayOutput $ \putReplayOutput -> do
        if GameRestart `elem` phaseTransitions
          then do
            snapshot <- stToIO $ freeze gameState
            putReplayOutput $ ReplayNewGame snapshot
          else putReplayOutput $ ReplayUpdate update

      -- Send game state snapshots to new players
      do
        let newPlayers' = map (second Just) newPlayers
            newObservers' = map ((,Nothing)) newObservers
        for_ (NE.nonEmpty (newPlayers' ++ newObservers')) $ \newUsers -> do
          snapshot <- stToIO $ freeze gameState
          forM_ newUsers $ \(userID, playerIDMaybe) ->
            putMsg $
              TCPSendTo userID $
                Client_TCPSend $
                  ServerGameData $
                    ServerSnapshot playerIDMaybe roomID snapshot

      -- Periodically check for idle players
      when (oldTime `atMultipleOfSync` Ticks (5 * C.tickRate_hz)) $ do
        maxIdleSeconds <- getMaxIdleSeconds
        let idleTicks = Ticks $ C.tickRate_hz * fromIntegral maxIdleSeconds
        mIdlePlayers <-
          NE.nonEmpty <$> getIdlePlayers playerInputBuffer idleTicks
        for_ mIdlePlayers $ \idlePlayers ->
          forM_ idlePlayers $ \userID -> do
            putMsg $ TCPSendTo userID $ Client_Kick "Player idle"
            writeLog logger $
              "Kicked idle user (userID: " <> toLogStr (show userID) <> ")."
