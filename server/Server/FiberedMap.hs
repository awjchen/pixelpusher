{-# LANGUAGE BlockArguments #-}

module Server.FiberedMap (
  FiberedMap,
  empty,
  lookupKey,
  insertKey,
  deleteKey,
  groupSizes,
  lookupGroup,
  deleteGroup,
) where

import Control.Monad (forM)
import Control.Monad.Trans.State.Strict
import Data.Foldable (toList, traverse_)
import Data.Generics.Product.Fields
import Data.Maybe (catMaybes)
import Data.Set qualified as S
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.WrappedIntMap qualified as WIM

--------------------------------------------------------------------------------

-- | A map that maintains inverse images of its inhabited values (fibers). This
-- data structure is designed specifically for the game manager. In particular,
-- empty fibers are maintained until explicitly deleted (in order to model game
-- rooms with no players).
data FiberedMap k v = FiberedMap
  { fm_map :: WIM.IntMap k v
  , fm_fibers :: WIM.IntMap v (S.Set k)
  }
  deriving stock (Generic)

empty :: FiberedMap k v
empty =
  FiberedMap
    { fm_map = WIM.empty
    , fm_fibers = WIM.empty
    }

-- Operations on keys

lookupKey :: (WIM.IsInt k) => k -> FiberedMap k v -> Maybe v
lookupKey k = view (to fm_map . at k)

-- Note: If the key is already present, it is first removed from its existing
-- group.
insertKey ::
  (WIM.IsInt k, Ord k, WIM.IsInt v) =>
  k ->
  v ->
  FiberedMap k v ->
  FiberedMap k v
insertKey k v = execState $ do
  field @"fm_map" . at k <.= Just v >>= traverse_ \existingValue ->
    field @"fm_fibers" %= removeFromGroup k existingValue
  field @"fm_fibers" . at v %= Just . maybe (S.singleton k) (S.insert k)

deleteKey ::
  (WIM.IsInt k, Ord k, WIM.IsInt v) =>
  k ->
  FiberedMap k v ->
  (Maybe v, FiberedMap k v)
deleteKey k = runState $ do
  mGrpVal <- field @"fm_map" . at k <<.= Nothing
  case mGrpVal of
    Nothing -> pure Nothing
    Just v -> do
      field @"fm_fibers" %= removeFromGroup k v
      pure $ Just v

-- Operations on groups

groupSizes :: (WIM.IsInt v) => FiberedMap k v -> [(v, Int)]
groupSizes = (map . fmap) S.size . WIM.toList . fm_fibers

lookupGroup :: (WIM.IsInt v) => v -> FiberedMap k v -> [k]
lookupGroup v = maybe [] S.toList . view (to fm_fibers . at v)

deleteGroup ::
  (WIM.IsInt k, WIM.IsInt v) => v -> FiberedMap k v -> ([k], FiberedMap k v)
deleteGroup v grpMap = flip runState grpMap $ do
  mKeySet <- field @"fm_fibers" . at v <<.= Nothing
  case mKeySet of
    Nothing -> pure []
    Just keySet -> fmap catMaybes $
      forM (toList keySet) $
        \k -> (fmap . fmap) (const k) $ field @"fm_map" . at k <<.= Nothing

-- Helpers

-- Note: If a fiber would become empty after removal of a key, the newly empty
-- fiber is intentionally not removed.
removeFromGroup ::
  (Ord k, WIM.IsInt v) =>
  k ->
  v ->
  WIM.IntMap v (S.Set k) ->
  WIM.IntMap v (S.Set k)
removeFromGroup k v = ix v %~ S.delete k
