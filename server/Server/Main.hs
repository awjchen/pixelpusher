{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

-- | The entry point of the server
module Server.Main (
  serverMain,
) where

import Control.Concurrent (setNumCapabilities)
import Control.Concurrent.Async (async, waitAnyCancel, withAsync)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Exception
import Control.Monad (forever, unless, void)
import Control.Monad.IO.Class (liftIO)
import Data.IORef
import Data.List (intercalate, isPrefixOf)
import Data.SemVer qualified as SV
import Data.Text.IO qualified as Text
import GHC.Conc (getNumCapabilities, getNumProcessors)
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (createDirectoryIfMissing, doesFileExist)
import System.Exit (die)
import System.FilePath (takeDirectory)
import System.IO
import System.IO.Error (isEOFError)
import System.Log.FastLogger
import System.Log.FastLogger qualified as FastLogger
import System.Metrics.Prometheus.Concurrent.Registry qualified as Prometheus.Registry
import System.Metrics.Prometheus.Http.Scrape (serveMetrics)
import Text.Read (readMaybe)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.GameEvents (GamePhaseTransition)
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Protocol (ServerGameMsg)

import Server.Config.Dynamic
import Server.Config.Static
import Server.GameManager (GameManagerEnv (..), runGameManager)
import Server.GameManagerCommand
import Server.GameServerID (GameServerID)
import Server.Init
import Server.Logging
import Server.Metrics
import Server.Network

--------------------------------------------------------------------------------

serverMain :: IO ()
serverMain = do
  getFormattedTime <- FastLogger.newTimeCache FastLogger.simpleTimeFormat
  let logType =
        LogFile
          FileLogSpec
            { log_file = "pixelpusher-server.log"
            , log_file_size = 4000 * 1024
            , log_backup_number = 4
            }
          FastLogger.defaultBufSize
  withTimedFastLogger getFormattedTime logType $ \logger ->
    flip finally (writeLog logger "Exiting pixelpusher-server.") $ do
      writeLog logger "Starting pixelpusher-server."
      hSetBuffering stdout NoBuffering
      Text.putStrLn $ ">>= pixelpusher-server-" <> SV.toText C.version <> " =<<"
      parseCommandLineArgs >>= \case
        Left errMsg -> putStrLn errMsg
        Right staticConfig -> do
          case ssc_debugMode staticConfig of
            NoDebugMode -> pure ()
            DebugMode -> do
              writeLog logger "Debug mode enabled."
              putStrLn "Debug mode enabled"
          runServer logger staticConfig

--------------------------------------------------------------------------------

runServer ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO ()
runServer logger staticConfig = do
  -- Create replay folder
  case ssc_replayFile staticConfig of
    Nothing -> pure ()
    Just replayFilePath -> do
      let replayDir = takeDirectory replayFilePath
      createDirectoryIfMissing True replayDir

  -- Load dynamic game parameters
  dynamicConfigRef <-
    newIORef =<< either die pure =<< loadDynamicConfig logger staticConfig

  -- Parse game parameters
  baseGameParams <- either die pure =<< loadGameParameters logger staticConfig

  -- Initialize metrics
  registry <- Prometheus.Registry.new
  metricsRefs <- initializeServerMetrics registry

  -- Serve prometheus metrics
  let serveMetrics' =
        serveMetrics
          prometheusPort
          ["metrics"]
          (Prometheus.Registry.sample registry)
  withAsync serveMetrics' $ \_ -> do
    -- Spawn threads and set up communication between them
    (incomingMsgQueueIn, incomingMsgQueueOut) <- UC.newChan

    let putIncomingMsg = UC.writeChan incomingMsgQueueIn . GMC_ClientMsg
    (networkServerAsync, putOutgoingMsg) <-
      spawnNetworkServer
        (makeNetworkServerConfig staticConfig dynamicConfigRef)
        metricsRefs
        putIncomingMsg
    putStrLn $
      "Listening on port "
        <> show (ssc_port staticConfig)
        <> " (both TCP and UDP)"

    let putGameMsg =
          (fmap . fmap) (UC.writeChan incomingMsgQueueIn) GMC_GameServerMsg
        managerEnv =
          makeGameManagerEnv
            logger
            staticConfig
            (readIORef dynamicConfigRef)
            putOutgoingMsg
            putGameMsg
    gameManagerAsync <-
      async $
        runGameManager
          managerEnv
          (readIORef dynamicConfigRef)
          incomingMsgQueueOut
          baseGameParams

    let setGameParams = UC.writeChan incomingMsgQueueIn . GMC_SwitchGameParams
    consoleAsync <-
      async $
        console
          logger
          staticConfig
          (atomicWriteIORef dynamicConfigRef)
          setGameParams

    void $
      reportException logger $
        waitAnyCancel [networkServerAsync, gameManagerAsync, consoleAsync]

loadDynamicConfig ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO (Either String DynamicServerConfig)
loadDynamicConfig logger staticConfig = do
  fmap (validateDynamicConfig =<<) $
    loadTomlFileWithDefault
      logger
      "server parameters"
      (ssc_dynamicConfigFile staticConfig)
      defaultDynamicServerConfigFilePath
      defaultDynamicServerConfigRaw

loadGameParameters ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO (Either String BaseGameParams)
loadGameParameters logger staticConfig =
  fmap (fmap makeBaseGameParams) $
    loadTomlFileWithDefault
      logger
      "game parameters"
      (ssc_gameParamsFile staticConfig)
      defaultGameParamsFilePath
      defaultTomlGameParams

defaultGameParamsFilePath :: FilePath
defaultGameParamsFilePath = "pixelpusher-params.toml"

loadTomlFileWithDefault ::
  (Toml.FromValue a, Toml.ToTable a) =>
  TimedFastLogger ->
  String ->
  Maybe FilePath ->
  FilePath ->
  a ->
  IO (Either String a)
loadTomlFileWithDefault logger label mFilePath defaultFilePath defaultValue = do
  let label' = toLogStr label

  filePath <-
    case mFilePath of
      Nothing -> do
        writeLog logger $
          "Using default "
            <> label'
            <> " filepath: "
            <> toLogStr defaultFilePath
        pure defaultFilePath
      Just filePath -> do
        writeLog logger $
          "Using custom "
            <> label'
            <> " filepath: "
            <> toLogStr filePath
        pure filePath

  filePathExists <- doesFileExist filePath
  unless filePathExists $ do
    writeLog logger $
      "No file found at filepath "
        <> toLogStr filePath
        <> ". Writing default "
        <> label'
        <> " to filepath."
    withWriteFileViaTemp filePath $ \h ->
      Prettyprinter.renderIO h $
        Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
          Toml.encode defaultValue

  parseResult <-
    Toml.decode <$> Text.readFile filePath >>= \case
      Toml.Failure errors -> do
        writeLog logger $
          "Error reading "
            <> label'
            <> ": "
            <> toLogStr (intercalate "; " errors)
        pure $
          Left $
            "Error reading "
              <> label
              <> ": "
              <> intercalate ";" errors
      Toml.Success warnings gameParams -> do
        unless (null warnings) $ do
          writeLog logger $
            "Warnings encountered while reading "
              <> label'
              <> ": "
              <> toLogStr (intercalate "; " warnings)
        pure $ Right gameParams

  liftIO $ putStrLn ("Using " <> label <> " from " <> filePath)
  pure $! parseResult

-- | A minimal console
console ::
  TimedFastLogger ->
  StaticServerConfig ->
  (DynamicServerConfig -> IO ()) ->
  (BaseGameParams -> IO ()) ->
  IO a
console logger staticConfig setDynamicConfig setGameParams = do
  let loadGameParameters' =
        loadGameParameters logger staticConfig >>= \case
          Left errMsg -> do
            putStrLn "Could not reload game parameters"
            putStrLn errMsg
          Right gameParams -> do
            setGameParams gameParams
            putStrLn "Reloaded game parameters"

  let loadDynamicConfig' =
        loadDynamicConfig logger staticConfig >>= \case
          Left errMsg -> do
            putStrLn "Could not reload server parameters"
            putStrLn errMsg
          Right dynConfig -> do
            setDynamicConfig dynConfig
            putStrLn "Reloaded server parameters"

  let exceptionHandler =
        [ Handler $ \(SomeAsyncException e) ->
            throwIO e
        , Handler $ \(e :: IOException) ->
            if isEOFError e
              then pure ()
              else putStrLn $ "Encountered error: " <> show e
        , Handler $ \(SomeException e) ->
            putStrLn $ "Encountered error: " <> show e
        ]

  putStrLn ""
  printConsoleHelp

  forever $
    flip catches exceptionHandler $ do
      putStr "\n> "
      line <- getLine
      if
        | line == "h" -> do
            printConsoleHelp
        | line == "r" -> do
            loadGameParameters'
            loadDynamicConfig'
        | line == "rg" ->
            loadGameParameters'
        | line == "rs" ->
            loadDynamicConfig'
        | "cpus" `isPrefixOf` line -> do
            let suffix = drop 4 line
            if null suffix
              then do
                numCapabilities <- getNumCapabilities
                putStrLn $ "Using " <> show numCapabilities <> " CPUs"
              else do
                case readMaybe @Int suffix of
                  Nothing ->
                    putStrLn "Could not parse number of CPUs"
                  Just numCapabilities
                    | numCapabilities > 0 -> do
                        numProcessors <- getNumProcessors
                        if numCapabilities > numProcessors
                          then do
                            putStrLn $
                              "Requested number of CPUs is higher than the number available ("
                                <> show numProcessors
                                <> "); no changes applied"
                          else do
                            setNumCapabilities numCapabilities
                            putStrLn $ "Set number of CPUs to " <> show numCapabilities
                    | otherwise ->
                        putStrLn "Cannot use a non-positive number of CPUs"
        | otherwise ->
            putStrLn $ "Unknown command '" <> line <> "'"

printConsoleHelp :: IO ()
printConsoleHelp = do
  putStrLn $
    intercalate
      "\n"
      [ "Console commands"
      , "  h                Show console commands"
      , "  r                Reload all configuration"
      , "                     (game parameters, server parameters)"
      , "  rg               Reload game parameters only"
      , "  rs               Reload server parameters only"
      , "  cpus             Show number of CPUs in use"
      , "  cpus <number>    Set number of CPUs to use"
      , ""
      , "Console keybindings"
      , "  Ctrl+C           Stop server and exit"
      , "                     (you might need to press it twice)"
      ]

makeNetworkServerConfig ::
  StaticServerConfig -> IORef DynamicServerConfig -> NetworkConfig
makeNetworkServerConfig staticConfig dynamicConfigRef =
  NetworkConfig
    { nc_debugMode = ssc_debugMode staticConfig
    , nc_port = ssc_port staticConfig
    , nc_getMaxConnections = dsc_maxTotalPlayers <$> readIORef dynamicConfigRef
    }

makeGameManagerEnv ::
  TimedFastLogger ->
  StaticServerConfig ->
  IO DynamicServerConfig ->
  (OutgoingMsg ServerGameMsg -> IO ()) ->
  (GameServerID -> Either SomeException [GamePhaseTransition] -> IO ()) ->
  GameManagerEnv
makeGameManagerEnv
  logger
  staticConfig
  getDynamicConfig
  putOutgoingMsg
  putGameMsg =
    GameManagerEnv
      { gme_logger = logger
      , gme_debugMode = ssc_debugMode staticConfig
      , gme_mReplayFilePath = ssc_replayFile staticConfig
      , gme_putOutgoingMsg = putOutgoingMsg
      , gme_putGameMsg = putGameMsg
      , gme_getMaxIdleSeconds = dsc_maxIdleSeconds <$> getDynamicConfig
      }

-- Helper
reportException :: TimedFastLogger -> IO a -> IO a
reportException logger =
  handle $ \(e :: SomeException) -> do
    writeLog logger $ "Unexpected exception: " <> toLogStr (show e)
    throwIO e
