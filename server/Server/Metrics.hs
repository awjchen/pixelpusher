{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Server.Metrics (
  ServerMetrics (..),
  initializeServerMetrics,
  prometheusPort,
) where

import System.Metrics.Prometheus.Concurrent.Registry
import System.Metrics.Prometheus.Metric.Gauge

newtype ServerMetrics = ServerMetrics
  { sm_connections :: Gauge
  }

initializeServerMetrics :: Registry -> IO ServerMetrics
initializeServerMetrics registry = do
  sm_connections <- registerGauge "connections" mempty registry
  pure ServerMetrics{..}

prometheusPort :: Int
prometheusPort = 30300
