-- | This module specifies the command line interface to the server.
module Server.Init (
  StaticServerConfig (..),
  parseCommandLineArgs,
) where

import Options.Applicative

import Server.Config.Static

-- | User-provided parameters may be inconsistent, in which case we return an
-- error message for the user.
parseCommandLineArgs :: IO (Either String StaticServerConfig)
parseCommandLineArgs =
  execParser $ info (helper <*> pStaticServerConfig) mempty
