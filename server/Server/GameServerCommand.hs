module Server.GameServerCommand (
  GameServerCommand (..),
) where

import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerClass (PlayerClass)
import Pixelpusher.Game.PlayerName (PlayerName)

import Server.Network.UserID (UserID)

data GameServerCommand
  = ServerPlayerJoin UserID PlayerName PlayerClass
  | ServerPlayerLeave UserID
  | ServerObserverJoin UserID
  | ServerObserverLeave UserID
  | -- | Just for playtesting
    ServerSwitchParams BaseGameParams
