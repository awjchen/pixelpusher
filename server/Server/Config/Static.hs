{-# LANGUAGE RecordWildCards #-}

module Server.Config.Static (
  StaticServerConfig (..),
  pStaticServerConfig,
) where

import Options.Applicative

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug

-- | Parameters from the command line
data StaticServerConfig = StaticServerConfig
  { ssc_port :: Int
  , ssc_dynamicConfigFile :: Maybe FilePath
  , ssc_gameParamsFile :: Maybe FilePath
  , ssc_replayFile :: Maybe FilePath
  , ssc_debugMode :: DebugMode
  }

-- | Raw parameters from the command line
data StaticServerConfigRaw = StaticServerConfigRaw
  { sscr_port :: Maybe Int
  , sscr_dynamicConfigFile :: Maybe FilePath
  , sscr_gameParamsFile :: Maybe FilePath
  , sscr_replayFile :: Maybe FilePath
  , sscr_debugMode :: DebugMode
  }

validateRawStaticConfig ::
  StaticServerConfigRaw -> Either String StaticServerConfig
validateRawStaticConfig StaticServerConfigRaw{..} = do
  port <-
    validate
      sscr_port
      C.defaultPort
      (\n -> 0 <= n && n <= 65535)
      "Bad port"
  pure $!
    StaticServerConfig
      { ssc_port = port
      , ssc_dynamicConfigFile = sscr_dynamicConfigFile
      , ssc_gameParamsFile = sscr_gameParamsFile
      , ssc_replayFile = sscr_replayFile
      , ssc_debugMode = sscr_debugMode
      }
  where
    validate :: Maybe a -> a -> (a -> Bool) -> String -> Either String a
    validate mInputValue defaultValue isValid errorMessage =
      case mInputValue of
        Nothing ->
          if isValid defaultValue
            then Right defaultValue
            else Left "Internal error: Invalid default value"
        Just a ->
          if isValid a
            then Right a
            else Left $ "Invalid parameters: " ++ errorMessage

pStaticServerConfig :: Parser (Either String StaticServerConfig)
pStaticServerConfig =
  fmap validateRawStaticConfig $
    StaticServerConfigRaw
      <$> pPort
      <*> pDynamicConfigFile
      <*> pGameConfigFile
      <*> pReplayFile
      <*> pDebugMode

pPort :: Parser (Maybe Int)
pPort =
  optional $
    option auto $
      mconcat
        [ long "port"
        , short 'p'
        , metavar "PORT"
        , help "The port on which to listen for incoming connections"
        ]

pDynamicConfigFile :: Parser (Maybe FilePath)
pDynamicConfigFile =
  optional $
    strOption $
      mconcat
        [ long "server-params"
        , short 's'
        , metavar "FILE"
        , help "File to use for setting server parameters"
        ]

pGameConfigFile :: Parser (Maybe FilePath)
pGameConfigFile =
  optional $
    strOption $
      mconcat
        [ long "game-params"
        , short 'g'
        , metavar "FILE"
        , help "File to use for setting game parameters"
        ]

pReplayFile :: Parser (Maybe FilePath)
pReplayFile =
  optional $
    strOption $
      mconcat
        [ long "replay"
        , short 'r'
        , metavar "NAME"
        , help "Record all games by dumping outgoing messages to files prefixed by NAME"
        ]

pDebugMode :: Parser DebugMode
pDebugMode =
  flag NoDebugMode DebugMode $
    mconcat
      [ long "debug-mode"
      , short 'd'
      , help "Run in the server in debug mode"
      ]
