{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Server.Config.Dynamic (
  DynamicServerConfig (..),
  DynamicServerConfigRaw (..),
  defaultDynamicServerConfigRaw,
  validateDynamicConfig,
  defaultDynamicServerConfigFilePath,
) where

import Data.String (IsString)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

-- | Parameters from a reloadable config file
data DynamicServerConfig = DynamicServerConfig
  { dsc_maxTotalPlayers :: Int
  , dsc_maxGameSize :: Word8
  , dsc_targetMaxGameSize :: Word8
  , dsc_targetMinGameSize :: Word8
  , dsc_maxIdleSeconds :: Int
  }

-- | Raw parameters from a reloadable config file
--
-- Note: We parse integral parameters using `Int` and not their final type
-- (e.g. `Word8`) in order to avoid overflow.
data DynamicServerConfigRaw = DynamicServerConfigRaw
  { maxConnections :: Int
  , maxGameSize :: Int
  , targetMaxGameSize :: Int
  , targetMinGameSize :: Int
  , maxIdleSeconds :: Int
  }
  deriving stock (Generic)

instance Toml.FromValue DynamicServerConfigRaw where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue DynamicServerConfigRaw where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable DynamicServerConfigRaw where
  toTable = Toml.genericToTable

defaultDynamicServerConfigRaw :: DynamicServerConfigRaw
defaultDynamicServerConfigRaw =
  DynamicServerConfigRaw
    { -- \| Maximum number of connections
      maxConnections = maxPlayersPerGame
    , -- \| Maximum number of players per game (hard limit)
      maxGameSize = maxPlayersPerGame
    , -- \| Preferred maximum number of players per game
      targetMaxGameSize = maxPlayersPerGame
    , -- \| Preferred minimum number of players per game
      targetMinGameSize = 8
    , -- \| Kick players if idle for this many seconds
      maxIdleSeconds = 60
    }
  where
    maxPlayersPerGame = fromIntegral C.maxPlayersPerGame

validateDynamicConfig ::
  DynamicServerConfigRaw -> Either String DynamicServerConfig
validateDynamicConfig DynamicServerConfigRaw{..} = do
  let maxPlayersPerGame' = fromIntegral C.maxPlayersPerGame
  maxConnections' <-
    validate
      maxConnections
      (> 0)
      "maxConnections must be positive"
  maxGameSize' <-
    validate
      maxGameSize
      (\n -> 0 < n && n <= maxPlayersPerGame')
      ( "maxGameSize must be positive and cannot be more than "
          ++ show C.maxPlayersPerGame
      )
  targetMaxGameSize' <-
    validate
      targetMaxGameSize
      (\n -> 0 < n && n <= maxGameSize')
      "targetMaxGameSize must be positive and cannot be more than maxGameSize"
  targetMinGameSize' <-
    validate
      targetMinGameSize
      (\n -> 0 < n && n <= targetMaxGameSize')
      "targetMinGameSize must be positive and cannot be more than targetMaxGameSize"
  maxIdleSeconds' <-
    validate
      maxIdleSeconds
      (> 0)
      "maxIdleSeconds must be positive"
  pure $!
    DynamicServerConfig
      { dsc_maxTotalPlayers = maxConnections'
      , dsc_maxGameSize = fromIntegral maxGameSize'
      , dsc_targetMaxGameSize = fromIntegral targetMaxGameSize'
      , dsc_targetMinGameSize = fromIntegral targetMinGameSize'
      , dsc_maxIdleSeconds = maxIdleSeconds'
      }
  where
    validate :: a -> (a -> Bool) -> String -> Either String a
    validate inputValue isValid errorMessage =
      if isValid inputValue
        then Right inputValue
        else Left $ "Invalid parameters: " ++ errorMessage

defaultDynamicServerConfigFilePath :: (IsString a) => a
defaultDynamicServerConfigFilePath = "pixelpusher-server-config.toml"
