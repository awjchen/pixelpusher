{-# LANGUAGE RecordWildCards #-}

module Server.Network.UserID (
  UserID,
  UserIDPool,
  newUserIDPool,
  withUserID,
) where

import Control.Exception
import Data.Foldable
import Data.IORef
import System.IO.Unsafe (unsafePerformIO)

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS

-- | Identifiers for connections to the server.
newtype UserID = UserID Int
  deriving newtype (Eq, Ord)

instance Show UserID where
  show (UserID uid) = show uid

instance WIM.IsInt UserID where
  toInt (UserID i) = WIM.makeIdentity i
  fromInt i = WIM.makeIdentity $ UserID i

instance WIS.IsInt UserID where
  toInt (UserID i) = WIS.makeIdentity i
  fromInt i = WIS.makeIdentity $ UserID i

-- | `UserID`s are (practically) unique. Based on the `Data.Unique` module.
newUserID :: IO UserID
newUserID = fmap UserID $ atomicModifyIORef' uniqSource $ dup . (+ 1)
  where
    dup a = (a, a)

uniqSource :: IORef Int
uniqSource = unsafePerformIO (newIORef 0)
{-# NOINLINE uniqSource #-}

-- Not actually a pool
data UserIDPool = UserIDPool
  { uidp_counter :: IORef Int
  , -- Dynamic limit
    uidp_getUserLimit :: IO Int
  }

newUserIDPool :: IO Int -> IO UserIDPool
newUserIDPool uidp_getUserLimit = do
  uidp_counter <- newIORef 0
  pure UserIDPool{..}

withUserID :: UserIDPool -> (Maybe UserID -> IO a) -> IO a
withUserID UserIDPool{..} action = do
  userLimit <- uidp_getUserLimit
  userID <- newUserID -- speculatively obtain a new `UserID`
  let initialize =
        atomicModifyIORef' uidp_counter $ \count ->
          if count < userLimit
            then (count + 1, Just userID)
            else (count, Nothing)
      finalize =
        traverse_ $ \_ -> atomicModifyIORef' uidp_counter $ (,()) . subtract 1
  bracket initialize finalize action
