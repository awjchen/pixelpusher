{-
The /client input filter/ is a thread through which all incoming messages from
clients are funneled.

It maintains the mapping from (UDP) socket addresses to `UserID`s and filters
out UDP messages from unknown clients.

It also handles the server's role in the (very simple) client-to-server UDP
protocol.
-}
{-# LANGUAGE BlockArguments #-}

module Server.Network.ClientInputFilter (
  runClientInputFilter,
) where

import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Monad (forever, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.State.Strict
import Data.Foldable (traverse_)
import Data.Generics.Product.Fields (field)
import Data.HashMap.Strict qualified as HM
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import Network.Socket

import Pixelpusher.Custom.SockAddr ()
import Pixelpusher.Network.Protocol

import Server.Network.Interface
import Server.Network.Internal
import Server.Network.UserID

--------------------------------------------------------------------------------

data ClientState = ClientState
  { recvSt_userID :: UserID
  , recvSt_nextSeqNum :: SeqNum
  }
  deriving stock (Generic)

-- | Run a thread for processing all client input. Maintains protocol state for
-- receiving messages from each client.
runClientInputFilter ::
  UC.OutChan (InputFilterCommand recv send) ->
  UC.InChan (SendFilterCommand send) ->
  (IncomingMsg recv -> IO ()) ->
  IO a
runClientInputFilter clientInputQueue sendQueue putIncomingMsg =
  let initClients = HM.empty :: HM.HashMap SockAddr ClientState
  in  flip evalStateT initClients $
        forever $
          liftIO (UC.readChan clientInputQueue)
            >>= clientInputFilter sendQueue putIncomingMsg

clientInputFilter ::
  UC.InChan (SendFilterCommand send) ->
  (IncomingMsg recv -> IO ()) ->
  InputFilterCommand recv send ->
  StateT (HM.HashMap SockAddr ClientState) IO ()
clientInputFilter sendQueue putIncomingMsg = \case
  InputFilter_Connection connUpdate -> do
    liftIO $
      UC.writeChan sendQueue $
        Send_ConnectionUpdate connUpdate
    case connUpdate of
      ClientJoin userID clientRole mRoomID sockAddr _ -> do
        liftIO $ putIncomingMsg $ NewConnection userID clientRole mRoomID
        -- TODO: disable message receiving from observers
        let msgState = ClientState userID firstSeqNum
        modify' $ HM.insert sockAddr msgState
      ClientLeave _ sockAddr -> do
        gets (HM.lookup sockAddr) >>= traverse_ \clientSt -> do
          liftIO $ putIncomingMsg $ Disconnection (recvSt_userID clientSt)
          modify' $ HM.delete sockAddr
  InputFilter_UDPData sockAddr udpData -> do
    gets (HM.lookup sockAddr) >>= traverse_ \clientSt -> do
      let ClientState userID nextSeqNum = clientSt
          updateSeqNum = clientUDPData_sequenceNumber udpData
      when (nextSeqNum `compareSeqNum` updateSeqNum /= GT) $ do
        let (ClientUDPData _ acks payload) = udpData
        liftIO $ do
          -- Forward player command
          putIncomingMsg $ IncomingMessage userID payload
          -- Forward acknowlegment
          UC.writeChan sendQueue $ Send_ClientAcks userID acks
        -- Update expected sequence number and loop
        let nextSeqNum' = incrementSeqNum updateSeqNum
        modify' $
          HM.adjust (set (field @"recvSt_nextSeqNum") nextSeqNum') sockAddr
  InputFilter_TCPData userID tcpData ->
    -- Note: There is currently no reason why TCP data from a known user needs
    -- to pass through this thread.

    -- Forward player command
    liftIO $ putIncomingMsg $ IncomingMessage userID tcpData
