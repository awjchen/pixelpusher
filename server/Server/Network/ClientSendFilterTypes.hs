module Server.Network.ClientSendFilterTypes (
  MsgNo,
  initialMsgNo,
  incrementMsgNo,
  QueueMsgKey,
  retransmissionKey,
  initialTransmissionKey,
) where

import Data.Hashable (Hashable)

import Pixelpusher.Network.Protocol

--------------------------------------------------------------------------------

-- | Messages are ordered using the order in which they are received by the
-- thread. Earlier messages are transmitted with higher priority.
newtype MsgNo = MsgNo Int
  deriving newtype (Eq, Ord)

initialMsgNo :: MsgNo
initialMsgNo = MsgNo minBound

incrementMsgNo :: MsgNo -> MsgNo
incrementMsgNo (MsgNo n) = MsgNo $ n + 1

-- This type serves as a sum type for `SeqNum` and `MsgNo`, relying on the fact
-- that `SeqNum` is a small integer and that `MsgNo` increments from
-- `minBound`. On 64-bit systems, there is no practical risk confusing these
-- two types of data.
newtype QueueMsgKey = QueueMsgKey Int
  deriving newtype (Eq, Ord, Hashable)

initialTransmissionKey :: MsgNo -> QueueMsgKey
initialTransmissionKey (MsgNo n) = QueueMsgKey n

retransmissionKey :: SeqNum -> QueueMsgKey
retransmissionKey = QueueMsgKey . fromIntegral . getSeqNum
