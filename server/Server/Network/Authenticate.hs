{-
Generate random tokens for authentication, allowing the bearer of a token to
pass a message to the creator of the token through an MVar.

This is used by the server for associating UDP socket addresses to TCP
connections.

Token generation depends on a counter so that, assuming a bounded token
generation rate, the tokens generated within a limited time interval are
guaranteed to be unique.
-}

module Server.Network.Authenticate (
  AuthStore,
  AuthToken,
  newAuthenticationStore,
  newToken,
  consumeToken,
  invalidateToken,
) where

import Control.Concurrent.MVar
import Data.Bits
import Data.Functor (void)
import Data.Generics.Product.Fields (field)
import Data.IORef
import Data.IntMap.Strict qualified as IM
import Data.Word
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random (randomIO)

--------------------------------------------------------------------------------

data AuthStore' a = AuthStore'
  { as_liveTokens :: IM.IntMap (MVar a)
  , as_counter :: Word16
  }
  deriving stock (Generic)

emptyStore :: AuthStore' a
emptyStore =
  AuthStore'
    { as_liveTokens = IM.empty
    , as_counter = 0
    }

type AuthToken = Int

newtype AuthStore a = AuthStore (IORef (AuthStore' a))

newAuthenticationStore :: IO (AuthStore a)
newAuthenticationStore = AuthStore <$> newIORef emptyStore

newToken :: AuthStore a -> IO (MVar a, AuthToken)
newToken (AuthStore storeRef) = do
  mvar <- newEmptyMVar
  token <- repeatUntil $ do
    randomInt <- randomIO
    atomicModifyIORef' storeRef $ \store ->
      let tentativeToken =
            shiftL randomInt 16 .|. fromIntegral (as_counter store)
      in  if tentativeToken `IM.member` as_liveTokens store
            then (store, Nothing)
            else
              ( store
                  & over (field @"as_liveTokens") (IM.insert tentativeToken mvar)
                  & over (field @"as_counter") (+ 1)
              , Just tentativeToken
              )
  pure (mvar, token)

consumeToken :: AuthStore a -> AuthToken -> a -> IO ()
consumeToken (AuthStore storeRef) token addr = do
  mMVar <- atomicModifyIORef' storeRef $ \store ->
    case IM.lookup token (as_liveTokens store) of
      Nothing -> (store, Nothing)
      Just mvar ->
        ( over (field @"as_liveTokens") (IM.delete token) store
        , Just mvar
        )
  case mMVar of
    Nothing -> pure ()
    Just mvar -> void $ tryPutMVar mvar addr

invalidateToken :: AuthStore a -> AuthToken -> IO ()
invalidateToken (AuthStore storeRef) token =
  atomicModifyIORef' storeRef $ \store ->
    (over (field @"as_liveTokens") (IM.delete token) store, ())

-- Helpers

repeatUntil :: IO (Maybe a) -> IO a
repeatUntil action = action >>= maybe (repeatUntil action) pure
