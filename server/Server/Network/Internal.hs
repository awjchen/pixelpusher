{-
Internal types used by the networking implementation
-}

module Server.Network.Internal (
  InputFilterCommand (..),
  SendFilterCommand (..),
  ClientConnectionUpdate (..),
  ClientRole (..),
) where

import Control.Concurrent.Chan.Unagi qualified as UC
import Network.Socket (SockAddr)

import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Protocol
import Pixelpusher.Network.Protocol (Acks, ClientUDPData)

import Server.Network.Interface
import Server.Network.UserID (UserID)

--------------------------------------------------------------------------------

data InputFilterCommand recv send
  = InputFilter_Connection (ClientConnectionUpdate send)
  | InputFilter_UDPData SockAddr (ClientUDPData recv)
  | InputFilter_TCPData UserID recv

data SendFilterCommand send
  = Send_OutgoingMsg (OutgoingMsg send)
  | Send_ClientAcks UserID Acks
  | Send_ConnectionUpdate (ClientConnectionUpdate send)

data ClientConnectionUpdate send
  = ClientJoin
      UserID
      (ClientRole PlayerName)
      (Maybe Int)
      SockAddr
      (UC.InChan (TCPClientCommand send))
  | ClientLeave UserID SockAddr
