{-
The /game manager/ is a thread that manages the multiple concurrent games
taking place on the server.

The game manager creates and destroys game sessions (rooms), assigns players to
these sessions, maintains a mapping from players to sessions, and forwards
incoming player commands to the proper session.

There is also some logic for manipulating the distribution of players across
game sessions to make it more likely that all games have a good number of
players.
-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}

module Server.GameManager (
  GameManagerEnv (..),
  runGameManager,
) where

import Control.Concurrent (forkIO)
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Exception (SomeException, bracket, mask_, uninterruptibleMask_)
import Control.Monad (forM_, forever, when, (>=>))
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.State.Strict
import Data.Bifunctor (bimap)
import Data.Foldable (find, maximumBy, minimumBy, traverse_)
import Data.Function (on)
import Data.Generics.Product.Fields
import Data.IORef
import Data.List (partition)
import Data.List.NonEmpty qualified as NE
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Text (Text)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Log.FastLogger

import Pixelpusher.Custom.Util
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Debug
import Pixelpusher.Game.GameEvents (GamePhaseTransition (..))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Protocol

import Server.Config.Dynamic
import Server.FiberedMap qualified as FM
import Server.GameManagerCommand
import Server.GameServer
import Server.GameServerCommand (GameServerCommand (..))
import Server.GameServerID
import Server.Logging
import Server.Network.Interface
import Server.Network.UserID

--------------------------------------------------------------------------------
-- Interface types

data GameManagerEnv = GameManagerEnv
  { gme_logger :: TimedFastLogger
  , gme_debugMode :: DebugMode
  , gme_mReplayFilePath :: Maybe FilePath
  , gme_putOutgoingMsg :: OutgoingMsg ServerGameMsg -> IO ()
  , gme_putGameMsg ::
      GameServerID ->
      Either SomeException [GamePhaseTransition] ->
      IO ()
  , gme_getMaxIdleSeconds :: IO Int
  }

makeGameServerEnv :: GameManagerEnv -> String -> GameServerEnv
makeGameServerEnv env suffix =
  GameServerEnv
    { gse_logger = gme_logger env
    , gse_putOutgingMsg = gme_putOutgoingMsg env
    , gse_mReplayFile = (++ suffix) <$> gme_mReplayFilePath env
    , gse_getMaxIdleSeconds = gme_getMaxIdleSeconds env
    }

--------------------------------------------------------------------------------
-- Internal state

data GameManagerState = GameManagerState
  { gms_gameParams :: BaseGameParams
  , gms_users :: FM.FiberedMap UserID GameServerID
  -- ^ A map from users to their game servers that can also be queried for the
  -- users hosted by a given game server.
  , gms_rooms :: WIM.IntMap GameServerID GameServerHandle
  -- ^ Game servers (we refer to game servers as rooms)
  }
  deriving stock (Generic)

initialState :: BaseGameParams -> GameManagerState
initialState params =
  GameManagerState
    { gms_gameParams = params
    , gms_users = FM.empty
    , gms_rooms = WIM.empty
    }

--------------------------------------------------------------------------------

runGameManager ::
  GameManagerEnv ->
  IO DynamicServerConfig ->
  UC.OutChan GameManagerCommand ->
  BaseGameParams ->
  IO a
runGameManager env getDynamicConfig cmdQueue gameParams =
  bracket
    (newIORef Map.empty)
    (readIORef >=> sequence_) -- Kill all game servers on exit
    $ \abortRef ->
      -- `abortRef` holds a map from `GameServerIDs` to IO actions that kill
      -- the corresponding game server thread.
      runGameManager' env getDynamicConfig cmdQueue abortRef gameParams

type AbortRef = IORef (Map GameServerID (IO ()))

runGameManager' ::
  GameManagerEnv ->
  IO DynamicServerConfig ->
  UC.OutChan GameManagerCommand ->
  AbortRef ->
  BaseGameParams ->
  IO a
runGameManager' env getDynamicConfig cmdQueue abortRef gameParams =
  flip evalStateT (initialState gameParams) $
    forever $
      liftIO (UC.readChan cmdQueue)
        >>= gameManager env getDynamicConfig abortRef

-- TODO: Currently, observers count towards the player limit for a game.
gameManager ::
  GameManagerEnv ->
  IO DynamicServerConfig ->
  AbortRef ->
  GameManagerCommand ->
  StateT GameManagerState IO ()
gameManager env getDynamicConfig abortRef = \case
  GMC_ClientMsg clientMsg -> case clientMsg of
    NewConnection userID clientRole mRoomID -> do
      dynConfig <- liftIO getDynamicConfig
      let allowConnection =
            case gme_debugMode env of
              DebugMode ->
                True
              NoDebugMode ->
                case clientRole of
                  ClientObserver{} -> False
                  ClientPlayer{} -> True
      if allowConnection
        then handleNewConnection env dynConfig abortRef userID clientRole mRoomID
        else
          liftIO $
            gme_putOutgoingMsg env $
              TCPSendTo userID $
                Client_Kick "Server rejected the observervation request"
    Disconnection userID -> do
      dynConfig <- liftIO getDynamicConfig
      handleDisconnection env dynConfig abortRef userID
    IncomingMessage userID clientGameMsg -> do
      let forwardClientGameMsg = do
            -- Forward message to the user's game server
            gets (FM.lookupKey userID . gms_users) >>= traverse_ \gameServerID -> do
              gsHandle <- unsafeGetGameServerHandle "forwardMessage" gameServerID
              liftIO $ gsh_putPlayerInput gsHandle userID clientGameMsg
      case clientGameMsg of
        ClientControls{} -> forwardClientGameMsg
        ClientDebugCommand{} ->
          case gme_debugMode env of
            DebugMode ->
              forwardClientGameMsg
            NoDebugMode ->
              liftIO $
                gme_putOutgoingMsg env $
                  TCPSendTo userID $
                    Client_Kick "Server received unwanted debug command"
  GMC_GameServerMsg gameServerID msg -> case msg of
    Left exception ->
      handleGameServerException env abortRef gameServerID exception
    Right gameEvents -> do
      dynConfig <- liftIO getDynamicConfig
      handleGamePhaseTransitions env dynConfig abortRef gameServerID gameEvents
  GMC_SwitchGameParams newParams -> do
    field @"gms_gameParams" .= newParams
    rooms <- gets gms_rooms
    liftIO $
      forM_ rooms $ \gameServerHandle ->
        gsh_issueCommand gameServerHandle $ ServerSwitchParams newParams

handleNewConnection ::
  GameManagerEnv ->
  DynamicServerConfig ->
  AbortRef ->
  UserID ->
  ClientRole PlayerName ->
  Maybe Int ->
  StateT GameManagerState IO ()
handleNewConnection env dynConfig abortRef userID clientRole mRoomID = do
  liftIO $
    writeLog (gme_logger env) $
      "New connection (user: " <> toLogStr (show userID) <> ")."
  maybe joinAnyRoom joinSpecificRoom mRoomID
  where
    joinAnyRoom :: StateT GameManagerState IO ()
    joinAnyRoom = do
      -- Find an open game to join. Create one if there are none.
      gameServerID <-
        gets (selectOpenGame dynConfig)
          >>= maybe (openGameServer env abortRef) pure
      unsafeGetGameServerHandle "joinAnyRoom" gameServerID
        >>= joinGame gameServerID

    joinSpecificRoom :: Int -> StateT GameManagerState IO ()
    joinSpecificRoom roomID = do
      let findRoom = find ((== roomID) . getGameServerID . fst) . FM.groupSizes
      gets (findRoom . gms_users) >>= \case
        -- Room not found
        Nothing -> liftIO $ disconnect "Room not found"
        -- Room found
        Just (gameServerID, nPlayers) ->
          -- Does the room have space?
          if nPlayers < fromIntegral (dsc_maxGameSize dynConfig)
            then
              unsafeGetGameServerHandle "joinSpecificRoom" gameServerID
                >>= joinGame gameServerID
            else liftIO $ disconnect "Room full"

    joinGame ::
      GameServerID -> GameServerHandle -> StateT GameManagerState IO ()
    joinGame gameServerID gsHandle = do
      -- Associate player with game
      field @"gms_users" %= FM.insertKey userID gameServerID
      -- Notify game
      liftIO $
        gsh_issueCommand gsHandle $
          case clientRole of
            ClientPlayer name playerClass ->
              ServerPlayerJoin userID name playerClass
            ClientObserver ->
              ServerObserverJoin userID
      liftIO $
        writeLog (gme_logger env) $
          "Added user "
            <> toLogStr (show userID)
            <> " to game "
            <> toLogStr (show gameServerID)
            <> "."

    disconnect :: Text -> IO ()
    disconnect msg = do
      gme_putOutgoingMsg env $ TCPSendTo userID $ Client_Kick msg
      writeLog (gme_logger env) $
        "Disconnecting user " <> toLogStr (show userID) <> "."

handleDisconnection ::
  GameManagerEnv ->
  DynamicServerConfig ->
  AbortRef ->
  UserID ->
  StateT GameManagerState IO ()
handleDisconnection env dynConfig abortRef userID = do
  liftIO $
    writeLog (gme_logger env) $
      "User " <> toLogStr (show userID) <> " disconnected."
  -- Disassociate player from game
  field @"gms_users" %%= FM.deleteKey userID >>= traverse_ \gameServerID -> do
    -- Notify game
    gsHandle <- unsafeGetGameServerHandle "disconnection" gameServerID
    liftIO $ gsh_issueCommand gsHandle $ ServerPlayerLeave userID
    pruneGames env dynConfig abortRef

handleGameServerException ::
  GameManagerEnv ->
  AbortRef ->
  GameServerID ->
  SomeException ->
  StateT GameManagerState IO ()
handleGameServerException env abortRef gameServerID exception = do
  liftIO $
    writeLog (gme_logger env) $
      "Exception in game "
        <> toLogStr (show gameServerID)
        <> ": "
        <> toLogStr (show exception)
  closeGameServer env abortRef clientMsg NoReconnect gameServerID
  where
    clientMsg = "Game crashed"

handleGamePhaseTransitions ::
  GameManagerEnv ->
  DynamicServerConfig ->
  AbortRef ->
  GameServerID ->
  [GamePhaseTransition] ->
  StateT GameManagerState IO ()
handleGamePhaseTransitions env dynConfig abortRef gameServerID gameEvents =
  forM_ gameEvents $ \case
    MatchEnd{} -> pure ()
    PlayMatchEndSfx -> pure ()
    GameRestart -> pure ()
    IntermissionEnding -> do
      -- If there are too few players in the game and enough slots in other
      -- games, close the game server to reallocate players.
      users <- gets gms_users
      let nPlayers = length $ FM.lookupGroup gameServerID users
          targetMin = fromIntegral $ dsc_targetMinGameSize dynConfig
          freeSlots =
            sum $
              map snd $
                filter (uncurry (&&) . bimap (/= gameServerID) (>= nPlayers)) $
                  FM.groupSizes users
      when (nPlayers <= freeSlots && nPlayers <= targetMin) $ do
        liftIO $
          writeLog (gme_logger env) $
            "Reallocating players from game "
              <> toLogStr (show gameServerID)
              <> "."
        closeGameServer
          env
          abortRef
          "Game room closed\n(reallocating players)"
          Reconnect
          gameServerID

openGameServer ::
  GameManagerEnv -> AbortRef -> StateT GameManagerState IO GameServerID
openGameServer env abortRef = do
  gsid <- liftIO newGameServerID
  gameParams <- gets gms_gameParams
  let gsEnv = makeGameServerEnv env ("-" ++ show gsid)
  gsHandle <- liftIO $
    mask_ $ do
      gsHandle <-
        spawnGameServer
          gsEnv
          gameParams
          (getGameServerID gsid)
          (gme_putGameMsg env gsid)
      atomicModifyIORef'_ abortRef $ Map.insert gsid $ gsh_close gsHandle
      pure gsHandle
  field @"gms_rooms" %= WIM.insert gsid gsHandle
  liftIO $
    writeLog (gme_logger env) $
      "Opened new game (id: " <> toLogStr (show gsid) <> ")."
  pure gsid

data ReconnectSignal = Reconnect | NoReconnect

closeGameServer ::
  GameManagerEnv ->
  AbortRef ->
  Text ->
  ReconnectSignal ->
  GameServerID ->
  StateT GameManagerState IO ()
closeGameServer env abortRef closeMsg reconnect gameServerID = do
  -- Remove game server from listing
  field @"gms_rooms" . at gameServerID <<.= Nothing >>= traverse_ \gsHandle ->
    -- Kill game server thread
    liftIO $
      uninterruptibleMask_ $ do
        -- Killing the game server in a new thread to prevent interruption of
        -- the call to `throwTo` (it is always interruptable), and to prevent
        -- potential stalling of the game manager thread (`throwTo` is
        -- blocking).
        _ <- forkIO $ gsh_close gsHandle
        atomicModifyIORef'_ abortRef $ Map.delete gameServerID
  liftIO $
    writeLog (gme_logger env) $
      "Closed game " <> toLogStr (show gameServerID) <> "."
  -- Disconnect users
  userIDs <- field @"gms_users" %%= FM.deleteGroup gameServerID
  let (rejoinCommand, logSuffix) = case reconnect of
        Reconnect -> (Client_Rejoin closeMsg, ", requesting rejoin.")
        NoReconnect -> (Client_Kick closeMsg, ".")
  liftIO $
    forM_ userIDs $ \userID -> do
      gme_putOutgoingMsg env $ TCPSendTo userID rejoinCommand
      writeLog (gme_logger env) $
        "Disconnecting user " <> toLogStr (show userID) <> logSuffix

-- | Select a game for a new player to join. Place new players in (1) the
-- largest undersized game, otherwise (2) the smallest well-sized game. Returns
-- `Nothing` if there are no open games.
selectOpenGame :: DynamicServerConfig -> GameManagerState -> Maybe GameServerID
selectOpenGame dynConfig = selectOpenGame' . FM.groupSizes . gms_users
  where
    selectOpenGame' :: [(a, Int)] -> Maybe a
    selectOpenGame' games = case NE.nonEmpty undersizedGames of
      Just ne_undersizedGames ->
        Just $ fst $ maximumBy (compare `on` snd) ne_undersizedGames
      Nothing -> case NE.nonEmpty wellSizedGames of
        Just ne_openWellSizedGames ->
          Just $ fst $ minimumBy (compare `on` snd) ne_openWellSizedGames
        Nothing -> Nothing
      where
        (undersizedGames, wellSizedGames) =
          partition ((< targetMinGameSize) . snd) $
            filter gameHasOpenSlot games

    targetMaxGameSize = fromIntegral $ dsc_targetMaxGameSize dynConfig
    targetMinGameSize = fromIntegral $ dsc_targetMinGameSize dynConfig
    gameHasOpenSlot = (< targetMaxGameSize) . snd

-- | Prune games with one or fewer players. We try to prune a game only if its
-- players would be placed in a game with more players if they were to
-- reconnect, but it's not a big deal.
pruneGames ::
  GameManagerEnv ->
  DynamicServerConfig ->
  AbortRef ->
  StateT GameManagerState IO ()
pruneGames env dynConfig abortRef =
  gets (selectGamesToPrune . FM.groupSizes . gms_users)
    >>= mapM_ closeGameServer'
  where
    closeGameServer' gameServerID = do
      let closeMsg = "Game room closed\n(single player remaining)"
      liftIO $
        writeLog (gme_logger env) $
          "Pruning single-player game " <> toLogStr (show gameServerID) <> "."
      closeGameServer
        env
        abortRef
        closeMsg
        NoReconnect
        gameServerID

    selectGamesToPrune :: [(a, Int)] -> [a]
    selectGamesToPrune games =
      let (smallGames, largeGames) = partition ((<= 1) . snd) games
          (emptyGames, singlePlayerGames) = partition ((== 0) . snd) smallGames
          targetMaxGameSize' = fromIntegral $ dsc_targetMaxGameSize dynConfig
          openSlots =
            sum $ map (\(_, n) -> max 0 $ targetMaxGameSize' - n) largeGames
      in  map fst $
            if null largeGames
              then emptyGames ++ safeTail singlePlayerGames
              else
                let (gs1, gs2) = splitAt openSlots singlePlayerGames
                in  emptyGames ++ gs1 ++ safeTail gs2

    safeTail :: [a] -> [a]
    safeTail = drop 1

--------------------------------------------------------------------------------
-- Helpers

atomicModifyIORef'_ :: IORef a -> (a -> a) -> IO ()
atomicModifyIORef'_ ref f = atomicModifyIORef' ref $ (,()) . f

-- This function is intended to be used on `GameServerID`s that are supposed to
-- be valid and for which we want to fail early should they be invalid.
unsafeGetGameServerHandle ::
  (Monad m) => String -> GameServerID -> StateT GameManagerState m GameServerHandle
unsafeGetGameServerHandle errorTag gameServerID =
  expectJust (errorTag ++ ": Non-existent GameServerID")
    <$> gets (WIM.lookup gameServerID . gms_rooms)
