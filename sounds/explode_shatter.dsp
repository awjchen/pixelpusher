import("stdfaust.lib");
import("bonk.dsp");
import("zita.dsp");

overseer_explode(var) = explode_shatter(var, 1.0, 0.2, 1600, 0.8, 2000);
flag_explode(var) = explode_shatter(var, 0.65, 0.5625, 3200, 0.5, 4000);

// Note: This process takes ~30 sec to compile on my laptop
explode_shatter(var, releaseSec, particles_gain, particles_freq, explosion_gain, explosion_freq) =
	particles*particles_gain + explosion*explosion_gain <: reverb

	with{
	nVars = 2;

	particles =
		no.multinoise(2*n)
		: par(i, n, rattle(freqs(varIndex(i)), rates(varIndex(i))) / sqrtN)
		:> _
		: *(10/baseRate)

		with{
			n = 32;
			sqrtN = sqrt(n);
			indexShift = n / nVars;
			varIndex(i) = ma.modulo(i + var*indexShift, n);

			freqs(i) = baseFreq + (maxFreq-baseFreq)*i/n;
			rates(i) = baseRate*(freqs(i)/baseFreq);
			baseFreq = particles_freq;
			maxFreq = baseFreq * 3;
			baseRate = 50;

			rattle(freq, rate, noise1, noise2) =
				sparse_noise(rate*intensityEnvelope, noise1)
				: strike(noise2)
				: annulus_resonate_particle(freq)
				: *(fadeEnvelope)
				;

			strike(noise) =
				strikeModel(noise, highpassCutoff, lowpassCutoff, sharpness, gain)
				with{
					highpassCutoff = 10;
					lowpassCutoff = maxFreq;
					sharpness = 0.2;
					gain = 1;
				};

			fadeEnvelope = en.ar(attackSec, releaseSec, 1 : ba.impulsify)
				with{ attackSec = 0; };
	};

	explosion =
		no.noises(3, ba.take(var+1, (0, 1)))
		: *(intensityEnvelope)
		: fi.lowpass(1, 10 + explosion_freq*freqMod)

		with{
			freqMod = pulse : en.asre(attackSec, sustainLevel, releaseSec*2)
				with{
					attackSec = 0.001;
					sustainLevel = 1;
					};
		};

	intensityEnvelope = pulse : en.are(attackSec, releaseSec)
		with{ attackSec = 0.07; };

	pulse = ba.pulsen(ma.SR * sustainSec, practicallyForever)
		with{
			sustainSec = 0.1;
			practicallyForever = ma.SR*100;
			};

	// Modified no.sparse_noise, taking a no.noise source as an argument
	sparse_noise(f0, noise) = sn
	with {
			saw = os.lf_sawpos(f0);
			sawdiff = saw - saw';
			e = float(noise); // float() keeps 4.656613e-10f scaling here instead of later
			eHeld = e : ba.latch(sawdiff);
			eHeldPos = 0.5 + 0.5 * eHeld;
			crossed = (saw >= eHeldPos) * (saw' < eHeldPos);
			sn = e' * float(crossed);
	};

	// Modified pm.strikeModel, taking a no.noise source as an argument
	strikeModel(noise, HPcutoff,LPcutoff,sharpness,gain,trigger) =
	noise : fi.highpass(2,HPcutoff) : fi.lowpass(2,LPcutoff) :
	*(en.ar(att,rel,trigger))*gain
	with{
			att = 0.002*sharpness;
			rel = att;
	};

};
