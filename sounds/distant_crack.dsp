import("stdfaust.lib");
import("zita.dsp");

distant_crack(var) =
	varNoise
	: *(envelope)
	: si.smooth(ba.tau2pole(4e-4))
	: fi.lowpass(2, 300)
	: *(8)
	<: reverb
	with{

	varNoise = no.noises(8, ba.take(var+1, (0, 1, 3, 6)));
	varDelaySecs = ba.take(var+1, (0.004, 0.007, 0.010, 0.013));
	varGain = ba.take(var+1, (0.94, 0.90, 0.87, 0.82));

	envelope = varGain * sqrt(ar1^2 + ar2^2);
	ar1 = ar;
	ar2 = de.delay(ba.sec2samp(1), ba.sec2samp(varDelaySecs), ar);
	ar = en.ar(sharpness, sharpness, 1);
	sharpness = 0.018;
};
