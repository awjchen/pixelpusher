import("stdfaust.lib");
import("zita.dsp");

soft_click(var) =
  no.noises(6, ba.take(var+1, (0, 2, 4, 5)))
  : fi.bandpass(1, freq/widthFactor, freq*widthFactor)
  : *(envelope*gain)
  <: reverb
  with {
    widthFactor = 1.015;
    attackSec = 0.0025;
    releaseSec = attackSec;
    envelope =
      en.ar(attackSec, releaseSec, ba.pulsen(attackSec*ma.SR, periodSamples))
      with {
        periodSamples = ma.SR*100; // big
      };
    gain = 8.0;

    freq = exp(log(freq0) + (t/dt) * (log(freq1) - log(freq0)))
      with {
        baseFreq = 210*8*(2^(2/12));
        freq0 = baseFreq;
        freq1 = baseFreq*1.5;
        dt = attackSec + releaseSec;
        t = ba.time / ma.SR;
      };
    };
