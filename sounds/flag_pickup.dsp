import("stdfaust.lib");
import("zita.dsp");

pickup = _ <: _,_;
drop = fi.lowpass(2, 1000) : *(2) <: reverb;

bonk(var, postfilter) =
	strike
	: annulus_resonate(var, freq, t60s)
	: *(envelope)
	: *(0.35)
	: postfilter

	with{

	freq = 210;
	t60s = 0.75;

	strike =
		1 :
		ba.impulsify :
		pm.strikeModel(highpassCutoff, lowpassCutoff, sharpness, gain)

		with{
			highpassCutoff = 10;
			lowpassCutoff = 9000;
			sharpness = 0.10;
			gain = 1;
		};

	// to remove the click at the start
	envelope = en.asr(attackSec, sustainLevel, releaseSec, trigger)
		with {
			attackSec = 0.001;
			sustainLevel = 1;
			releaseSec = 0; // irrelevant
			trigger = 1; // no release
		};
};

annulus_resonate(var, freq, t60s) =
  _ <:
  par(
		i,
		nModes,
		pm.modeFilter(modesFreqs(i), modesT60s(i), modesGainsNormalized(i))
		) :>
  _

  with{
    nModes = 5;

		modesFreqs(i) = freq * 2.5^i;

    modesT60s(i) = t60s * (refFreq / modesFreqs(i)); // for lack of information
    refFreq = 200;

		modesGains(i) = ba.db2linear(-2*sqrt(i)) * varGains(var, i);
		varGains(var, i) =
			ba.selector(
				var,
				nVariations,
				ba.take(i+1, (0.70, 0.70, 1.0, 1.0, 1.0)),
				ba.take(i+1, (0.55, 1.10, 1.0, 1.0, 1.0))
				);
		nVariations = 2;
    modesGainsNormalized(i) = modesGains(i) / gainSum;
    gainSum = sum(i,nModes,modesGains(i));
  };
