default: build

format:
    #!/usr/bin/env bash
    FILES=$(git diff HEAD --name-only --diff-filter=ACMR | grep \.hs\$)
    if [[ -n "$FILES" ]]; then
      fourmolu --mode inplace $FILES
    fi

ghc-options := '--ghc-options "-O2 -j3 +RTS -A128m -n2m -RTS"'

build:
    stack install {{ghc-options}}

client:
    stack install :pixelpusher-client {{ghc-options}}

server:
    stack install :pixelpusher-server {{ghc-options}}

headless:
    stack install :pixelpusher-headless {{ghc-options}}

bench:
    stack bench :pixelpusher-bench --ba '--stdev 0.30 --timeout 7000000 +RTS -T' {{ghc-options}}
