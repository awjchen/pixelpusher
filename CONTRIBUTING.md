# Contributing

The codebase is probably too messy and "in my head" for code contributions.
I'll get to work on cleaning up. Check back later!

Though, if you have a specific contribution in mind, let's chat!
Drop me a message at (aetup dot pixelpusher at gmail dot com) :)
