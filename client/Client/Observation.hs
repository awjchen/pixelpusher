module Client.Observation (
  ObservationParams (..),
) where

import Data.Text (Text)

newtype ObservationParams = ObservationParams
  { op_serverAddress :: Text
  }
