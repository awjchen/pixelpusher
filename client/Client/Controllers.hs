{-# LANGUAGE TemplateHaskell #-}

module Client.Controllers (
  controllerMapping,
) where

import Data.ByteString qualified as BS
import Data.FileEmbed (embedFile)

controllerMapping :: BS.ByteString
controllerMapping = $(embedFile "controllers/gamecontrollerdb.txt")
