module Client.Scenes.SinglePlayerGame (
  runSinglePlayerGame,
) where

import Control.Monad.ST (stToIO)
import Data.Generics.Product.Fields
import Lens.Micro
import System.Log.FastLogger

import Pixelpusher.Game.GameCommand
import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.State

import Client.Audio.Env
import Client.Config.Audio (AudioOptions)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.Visual (VisualOptions)
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

import Client.GameLoop (
  ShowTutorial (..),
  makeSinglePlayerGameLoop,
  runGameLoop,
 )

--------------------------------------------------------------------------------

runSinglePlayerGame ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  BaseGameParams ->
  Keybindings ->
  PlayerName ->
  PlayerClass ->
  Int ->
  Int ->
  IO ()
runSinglePlayerGame
  logger
  glEnv
  alEnv
  themeEnv
  stateRef
  rawGameParams
  keybindings
  playerName
  playerClass
  botLevel
  teamSize = do
    -- Set game parameters
    let gameParams =
          rawGameParams
            & field @"bots" . field @"bots_level"
              .~ botLevel
            & field @"bots" . field @"bots_minBotsPerTeam"
              .~ (0 :: Int)
            & field @"bots" . field @"bots_minTeamSize"
              .~ teamSize

    -- Initialize game state and add a player to the game
    let playerID = PlayerID minBound
        addPlayerCommand = PlayerJoin playerID playerName playerClass Nothing
        debugCmd = Nothing
    gameState <- initializeGameState gameParams
    _ <-
      stToIO $ integrateGameState mempty [addPlayerCommand] debugCmd gameState

    gameLoop <- makeSinglePlayerGameLoop playerID gameState

    runGameLoop logger glEnv alEnv themeEnv stateRef keybindings ShowTutorial gameLoop
