module Client.Scenes.Tutorial (
  runTutorial,
) where

import Control.Monad.ST (stToIO)
import Data.Generics.Product.Fields
import Lens.Micro
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameCommand
import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.State
import Pixelpusher.Game.Tutorial (playerTutorialTeam)

import Client.Audio.Env
import Client.Config.Audio (AudioOptions)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.Visual (VisualOptions)
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

import Client.GameLoop (
  ShowTutorial (..),
  makeSinglePlayerGameLoop,
  runGameLoop,
 )

--------------------------------------------------------------------------------

runTutorial ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  BaseGameParams ->
  Keybindings ->
  IO ()
runTutorial logger glEnv alEnv themeEnv stateRef rawGameParams keybindings = do
  -- Set game parameters
  let gameParams =
        rawGameParams
          & ( field @"bots" %~ \params ->
                params
                  & set (field @"bots_minBotsPerTeam") (0 :: Int)
                  & set (field @"bots_minTeamSize") (1 :: Int)
                  & set (field @"bots_level") (1 :: Int)
            )
          & ( field @"obstacles" %~ \params ->
                params
                  & set (field @"obstacle_obstaclesPerPlayer") (0 :: Fixed)
            )
          & ( field @"overseers" %~ \params ->
                params
                  & set
                    (field @"overseer_respawn_delay_ticks")
                    (3 * fromIntegral C.tickRate_hz :: Int)
                  & set
                    (field @"overseer_respawn_invulnerabilityTime_overseer_ticks")
                    (60 :: Int)
                  & set
                    (field @"overseer_respawn_invulnerabilityTime_drone_ticks")
                    (60 :: Int)
                  & set
                    (field @"overseer_respawn_bonusHealthFraction")
                    (1 :: Fixed)
            )
          & ( field @"misc" %~ \params ->
                params
                  & set (field @"misc_initialJoinDelay_ticks") (240 :: Int)
                  & set (field @"misc_worldSize_minPlayers") (1 :: Int)
            )
          & ( field @"shieldPowerups" %~ \params ->
                params
                  & set (field @"shieldPowerup_spawned_enable") False
                  & set (field @"shieldPowerup_dropped_enable") False
            )
          & ( field @"z_dev" %~ \params ->
                params
                  & set (field @"dev_enableTutorialMode") True
            )

  -- Initialize game state and add a player to the game
  let playerID = PlayerID minBound
      addPlayerCommand =
        PlayerJoin playerID tutorialPlayerName Templar (Just playerTutorialTeam)
      debugCmd = Nothing
  gameState <- initializeGameState gameParams
  _ <- stToIO $ integrateGameState mempty [addPlayerCommand] debugCmd gameState

  gameLoop <- makeSinglePlayerGameLoop playerID gameState

  runGameLoop logger glEnv alEnv themeEnv stateRef keybindings DoNotShowTutorial gameLoop
