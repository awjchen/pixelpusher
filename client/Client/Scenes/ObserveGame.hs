module Client.Scenes.ObserveGame (
  observeGame,
) where

import Control.Concurrent.Async (ExceptionInLinkedThread (..), withAsync)
import Control.Concurrent.Async qualified as Async (link)
import Control.Concurrent.MVar
import Control.Exception
import Control.Monad (void)
import Data.IORef
import Graphics.UI.GLFW qualified as GLFW
import System.Log.FastLogger

import Network.Socket

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.Protocol

import Client.Audio.Env
import Client.Config.Audio (AudioOptions (..), defaultAudioOptions)
import Client.Config.Controls
import Client.Config.Keybindings (defaultKeybindings)
import Client.Config.Visual (defaultVisualOptions)
import Client.Custom.CancellableTimeout (cancellableTimeout)
import Client.Logging (writeLog)
import Client.Network qualified
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

import Client.GameLoop (
  ShowTutorial (..),
  makeObserverGameLoop,
  runGameLoop,
 )
import Client.GameLoop.GameStateBuffer

--------------------------------------------------------------------------------

-- | Connect to a server and watch a streamed game.
observeGame ::
  TimedFastLogger ->
  ThemeEnv ->
  HostName ->
  Int -> -- Port
  Maybe Int -> -- Room
  IO ()
observeGame logger themeEnv server port mRoomID =
  -- Explictly disable VSync because I can't for the life of me figure out
  -- how to get things to run smoothly with vsync on Windows!!
  withGLEnv logger DisableVSync $ \glEnv -> do
    -- Make cursor visible for setting debug marks
    GLFW.setCursorInputMode (gle_window glEnv) GLFW.CursorInputMode'Normal
    withALEnv logger audioOptions $ \alEnv ->
      void $
        cancellableTimeout C.connTimeLimit_ns $ \cancelTimeout ->
          handle @SomeException
            (\e -> writeLog logger (toLogStr (show e)) >> print e >> throwIO e)
            $ let role = ClientObserver
              in  Client.Network.connect logger server port role mRoomID $
                    \debugMode conn -> do
                      cancelTimeout
                      startObservedGame
                        logger
                        glEnv
                        alEnv
                        themeEnv
                        debugMode
                        conn

audioOptions :: InitialAudioOptions
audioOptions =
  InitialAudioOptions
    { iao_masterGainPercent = ao_masterVolume defaultAudioOptions
    , iao_musicGainPercent = ao_musicVolume defaultAudioOptions
    , iao_effectsGainPercent = ao_effectsVolume defaultAudioOptions
    }

-- TODO: handle exceptions like in the multiplayer game loop

--------------------------------------------------------------------------------

startObservedGame ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  DebugMode ->
  Client.Network.Connection ->
  IO ()
startObservedGame logger glEnv alEnv themeEnv debugMode conn = do
  gameStateBuffer <- newGameStateBuffer
  latencyRef <- newIORef @(Maybe Int) Nothing
  sendMsgHandlesVar <- newEmptyMVar

  let runNetworkClient =
        Client.Network.runClient logger conn bufferPutMsg sendMsgHandlesVar putLatency
        where
          bufferPutMsg = insertUpdate gameStateBuffer . unServerGameMsg
          putLatency = writeIORef latencyRef . Just
  _ <- withAsync runNetworkClient $ \networkClientAsync ->
    handle unwrapLinkedException $ do
      Async.link networkClientAsync
      sendMsgHandles <- takeMVar sendMsgHandlesVar
      dummyStateRef <-
        newStateRef (defaultAudioOptions, defaultVisualOptions, defaultControlOptions)
      observerGameLoop <-
        makeObserverGameLoop debugMode sendMsgHandles gameStateBuffer latencyRef
      runGameLoop
        logger
        glEnv
        alEnv
        themeEnv
        dummyStateRef
        defaultKeybindings
        DoNotShowTutorial
        observerGameLoop
  pure ()

unwrapLinkedException :: ExceptionInLinkedThread -> IO a
unwrapLinkedException (ExceptionInLinkedThread _ e) = throwIO e
