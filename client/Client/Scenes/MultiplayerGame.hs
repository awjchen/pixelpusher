{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.MultiplayerGame (
  GameExitStatus (..),
  runMultiplayerGame,
) where

import Control.Applicative ((<|>))
import Control.Concurrent.Async (ExceptionInLinkedThread (..), withAsync)
import Control.Concurrent.Async qualified as Async (link)
import Control.Concurrent.MVar
import Control.Exception
import Data.Either (fromLeft)
import Data.Functor (($>))
import Data.IORef
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Lens.Micro.Platform.Custom
import System.Log.FastLogger

import Network.Socket

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Protocol
import Pixelpusher.Network.TCP.Protocol

import Client.Audio.Env
import Client.Config.Audio (AudioOptions)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.Visual (VisualOptions)
import Client.Custom.CancellableTimeout (cancellableTimeout)
import Client.Network qualified
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

import Client.GameLoop (
  ShowTutorial (..),
  networkedGameLoop,
  runGameLoop,
 )
import Client.GameLoop.GameStateBuffer

--------------------------------------------------------------------------------

-- | Connect to a server and run the game.
runMultiplayerGame ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  HostName ->
  Int -> -- Port
  PlayerName ->
  PlayerClass ->
  Maybe Int -> -- Room
  Keybindings ->
  IO GameExitStatus
runMultiplayerGame
  logger
  glEnv
  alEnv
  themeEnv
  stateRef
  server
  port
  playerName
  playerClass
  mRoomID
  keybindings =
    fmap (fromMaybe Exit_ConnectionTimeout) $
      cancellableTimeout C.connTimeLimit_ns $ \cancelTimeout ->
        fmap (fromLeft Exit_PlayerQuit) $
          tryJust handler $
            handle @SomeException (\e -> print e >> throwIO e) $
              let role = ClientPlayer playerName playerClass
              in  Client.Network.connect logger server port role mRoomID $
                    \debugMode conn -> do
                      cancelTimeout
                      startNetworkedGame
                        logger
                        glEnv
                        alEnv
                        themeEnv
                        stateRef
                        debugMode
                        conn
                        keybindings

data GameExitStatus
  = Exit_ConnectionTimeout
  | Exit_PlayerQuit
  | Exit_Error [Text]
  | Exit_Rejoin Text

handler :: SomeException -> Maybe GameExitStatus
handler e = handleIO e <|> handleTCPClose e

handleIO :: SomeException -> Maybe GameExitStatus
handleIO e =
  fromException @IOException e $> Exit_Error ["Could not connect to server"]

handleTCPClose :: SomeException -> Maybe GameExitStatus
handleTCPClose e =
  fromException @TCPConnectionClosedException e <&> \case
    ClosedBecause reconnect reason -> case reconnect of
      Reconnect ->
        Exit_Rejoin reason
      DoNotReconnect ->
        Exit_Error ["Connection closed by server: ", reason]
    UnexpectedClose ->
      Exit_Error ["Connection unexpectedly closed by server"]

--------------------------------------------------------------------------------

startNetworkedGame ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  DebugMode ->
  Client.Network.Connection ->
  Keybindings ->
  IO ()
startNetworkedGame logger glEnv alEnv themeEnv stateRef debugMode conn keybindings = do
  gameStateBuffer <- newGameStateBuffer
  latencyRef <- newIORef @(Maybe Int) Nothing
  sendMsgHandlesVar <- newEmptyMVar

  let runNetworkClient =
        Client.Network.runClient logger conn bufferPutMsg sendMsgHandlesVar putLatency
        where
          bufferPutMsg = insertUpdate gameStateBuffer . unServerGameMsg
          putLatency = writeIORef latencyRef . Just
  withAsync runNetworkClient $ \networkClientAsync ->
    handle unwrapLinkedException $ do
      Async.link networkClientAsync
      sendMsgHandles <- takeMVar sendMsgHandlesVar
      runGameLoop logger glEnv alEnv themeEnv stateRef keybindings DoNotShowTutorial $
        networkedGameLoop debugMode sendMsgHandles gameStateBuffer latencyRef

unwrapLinkedException :: ExceptionInLinkedThread -> IO a
unwrapLinkedException (ExceptionInLinkedThread _ e) = throwIO e
