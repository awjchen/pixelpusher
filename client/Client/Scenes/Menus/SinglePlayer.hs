{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.SinglePlayer (
  runSinglePlayerMenu,
  SinglePlayerResult (..),
  SinglePlayerOptions (..),
  emptySinglePlayerOptions,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.Env (ALEnv)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.SinglePlayer
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

runSinglePlayerMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  StateRef SinglePlayerOptions ->
  [String] ->
  IO (Maybe SinglePlayerResult)
runSinglePlayerMenu glEnv alEnv themeEnv keybindings controlOpts modelRef messages =
  menuScene'
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    (initialMenuState "single player" multiplayerMenu)
    (handleInput alEnv)

data SinglePlayerResult
  = SinglePlayer_Start
  | SinglePlayer_Back

type SinglePlayerWidgets :: MenuItemType -> Type
data SinglePlayerWidgets ty where
  Name :: SinglePlayerWidgets 'TextInput
  Class :: SinglePlayerWidgets ('EnumChoice PlayerClass)
  BotDifficulty :: SinglePlayerWidgets 'IntSlider
  TeamSize :: SinglePlayerWidgets 'IntSlider
  Start :: SinglePlayerWidgets 'Button
  Back :: SinglePlayerWidgets 'Button

multiplayerMenu ::
  NonEmpty (MenuItem SinglePlayerWidgets SinglePlayerOptions)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ TextInputWidget
        Name
        (field @"spo_playerName")
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , EnumChoiceWidget
        Class
        (field @"spo_playerClass")
        EnumChoiceConfig{esc_label = "starting class"}
    , IntSliderWidget
        BotDifficulty
        (field @"spo_botDifficulty")
        IntSliderConfig
          { isc_label = "bot level"
          , isc_min = 1
          , isc_max = 9
          , isc_step = 1
          }
    , IntSliderWidget
        TeamSize
        (field @"spo_teamSize")
        IntSliderConfig
          { isc_label = "team size"
          , isc_min = 1
          , isc_max = 8
          , isc_step = 1
          }
    , ButtonWidget
        Start
        ButtonConfig
          { bc_label = "start"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent SinglePlayerWidgets) ->
  IO (LoopContinue SinglePlayerResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished SinglePlayer_Start
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished SinglePlayer_Back
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged Class _newServer -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged BotDifficulty _newValue -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged TeamSize _newValue -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Start -> do
        uiSoundHigh alEnv
        pure $ LoopFinished SinglePlayer_Start
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished SinglePlayer_Back
      FocusTextInput _ ->
        pure LoopContinue
      UnfocusTextInput _ ->
        pure LoopContinue
