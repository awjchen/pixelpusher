{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Settings (
  runSettingsMenu,
  SettingsResult (..),
  settingsMenu,
  SettingsWidgets,
) where

import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings qualified as Config
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env hiding (FullscreenMode (..))

runSettingsMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Config.Keybindings ->
  ControlOptions ->
  [String] ->
  MenuState SettingsWidgets () ->
  IO (MenuState SettingsWidgets (), Maybe SettingsResult)
runSettingsMenu glEnv alEnv themeEnv keybindings controlOpts messages menuState = do
  menuScene
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    unitStateRef
    menuState
    (handleInput alEnv)

data SettingsResult
  = Settings_Audio
  | Settings_Visual
  | Settings_Control
  | Settings_Keybindings
  | Settings_Back

type SettingsWidgets :: MenuItemType -> Type
data SettingsWidgets ty where
  Audio :: SettingsWidgets 'Button
  Visual :: SettingsWidgets 'Button
  Control :: SettingsWidgets 'Button
  Keybindings :: SettingsWidgets 'Button
  Back :: SettingsWidgets 'Button

settingsMenu :: NonEmpty (MenuItem SettingsWidgets ())
settingsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget Audio ButtonConfig{bc_label = "audio"}
    , ButtonWidget Visual ButtonConfig{bc_label = "visual"}
    , ButtonWidget Control ButtonConfig{bc_label = "control"}
    , ButtonWidget Keybindings ButtonConfig{bc_label = "keybindings"}
    , ButtonWidget Back ButtonConfig{bc_label = "back"}
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent SettingsWidgets) ->
  IO (LoopContinue SettingsResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished Settings_Back
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Audio -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Audio
      ButtonPressed Visual -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Visual
      ButtonPressed Control -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Control
      ButtonPressed Keybindings -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Settings_Keybindings
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished Settings_Back
