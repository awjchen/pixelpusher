{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.PrivateMultiplayer (
  runPrivateMultiplayerMenu,
  PrivateMultiplayerResult (..),
  PrivateMultiplayerOptions (..),
) where

import Data.Char (isAsciiLower, isAsciiUpper, isDigit)
import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.Env (ALEnv)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.PrivateMultiplayer
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

runPrivateMultiplayerMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  StateRef PrivateMultiplayerOptions ->
  [String] ->
  IO (Maybe PrivateMultiplayerResult)
runPrivateMultiplayerMenu glEnv alEnv themeEnv keybindings controlOpts modelRef messages =
  menuScene'
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    (initialMenuState "private multiplayer" multiplayerMenu)
    (handleInput alEnv)

data PrivateMultiplayerResult
  = PrivateMultiplayer_Connect
  | PrivateMultiplayer_Back

type PrivateMultiplayerWidgets :: MenuItemType -> Type
data PrivateMultiplayerWidgets ty where
  Name :: PrivateMultiplayerWidgets 'TextInput
  Class :: PrivateMultiplayerWidgets ('EnumChoice PlayerClass)
  Connect :: PrivateMultiplayerWidgets 'Button
  Back :: PrivateMultiplayerWidgets 'Button

multiplayerMenu ::
  NonEmpty (MenuItem PrivateMultiplayerWidgets PrivateMultiplayerOptions)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ TextInputWidget
        Name
        (field @"pmo_serverAddress")
        TextInputConfig
          { tic_label = "server address"
          , tic_maxLength = 255
          , tic_permittedChars = isDomainNameOrIpv6Char
          }
    , TextInputWidget
        Name
        (field @"pmo_playerName")
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , EnumChoiceWidget
        Class
        (field @"pmo_playerClass")
        EnumChoiceConfig{esc_label = "starting class"}
    , ButtonWidget
        Connect
        ButtonConfig
          { bc_label = "connect"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

isDomainNameOrIpv6Char :: Char -> Bool
isDomainNameOrIpv6Char c =
  isDigit c || isAsciiLower c || isAsciiUpper c || c == '.' || c == '-' || c == ':'

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent PrivateMultiplayerWidgets) ->
  IO (LoopContinue PrivateMultiplayerResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished PrivateMultiplayer_Connect
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished PrivateMultiplayer_Back
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged Class _newServer -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Connect -> do
        uiSoundHigh alEnv
        pure $ LoopFinished PrivateMultiplayer_Connect
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished PrivateMultiplayer_Back
      FocusTextInput _ ->
        pure LoopContinue
      UnfocusTextInput _ ->
        pure LoopContinue
