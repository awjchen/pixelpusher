{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Keybindings (
  runKeybindingsMenu,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Generics.Product.Fields
import Data.IORef
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.Parameters.Test

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env hiding (FullscreenMode (..))
import Client.Visuals.FontAtlas (fa_fontSize)
import Client.Visuals.GL.Wrapped
import Client.Visuals.Rendering.DarkLayer (drawDarkLayer)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text

runKeybindingsMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  ControlOptions ->
  StateRef Keybindings ->
  [String] ->
  IO (Maybe ())
runKeybindingsMenu glEnv alEnv themeEnv controlOpts modelRef messages = do
  -- changes keybindings won't be reflected in the help overlay until after exiting this keybindings scene
  initialKeybindings <- readStateRef modelRef

  snd
    <$> customMenuScene
      glEnv
      themeEnv
      initialKeybindings -- changes keybindings won't be reflected in the help overlay until after exiting this keybindings scene
      controlOpts
      messages
      modelRef
      (initialMenuState "keybindings" keybindingsMenu)
      (handleInput glEnv alEnv)

type KeybindingsWidgets :: MenuItemType -> Type
data KeybindingsWidgets ty where
  OverseerUp :: KeybindingsWidgets 'Keybind
  OverseerLeft :: KeybindingsWidgets 'Keybind
  OverseerDown :: KeybindingsWidgets 'Keybind
  OverseerRight :: KeybindingsWidgets 'Keybind
  OverseerDash :: KeybindingsWidgets 'Keybind
  PsiStorm :: KeybindingsWidgets 'Keybind
  DropFlag :: KeybindingsWidgets 'Keybind
  Back :: KeybindingsWidgets 'Button

keybindingsMenu :: NonEmpty (MenuItem KeybindingsWidgets Keybindings)
keybindingsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ KeybindWidget
        OverseerUp
        kb_overseerUp
        KeybindConfig{kc_label = "up"}
    , KeybindWidget
        OverseerLeft
        kb_overseerLeft
        KeybindConfig{kc_label = "left"}
    , KeybindWidget
        OverseerDown
        kb_overseerDown
        KeybindConfig{kc_label = "down"}
    , KeybindWidget
        OverseerRight
        kb_overseerRight
        KeybindConfig{kc_label = "right"}
    , KeybindWidget
        OverseerDash
        kb_overseerDash
        KeybindConfig{kc_label = "dash"}
    , KeybindWidget
        PsiStorm
        kb_psiStorm
        KeybindConfig{kc_label = "psionic storm"}
    , KeybindWidget
        DropFlag
        kb_dropFlag
        KeybindConfig{kc_label = "drop flag"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  GLEnv ->
  ALEnv ->
  ((Keybindings -> Keybindings) -> IO ()) ->
  Maybe (MenuUpdateEvent KeybindingsWidgets) ->
  IO (LoopContinue ())
handleInput glEnv alEnv updateModel = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerUp -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "up" (field @"kb_overseerUp")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerLeft -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "left" (field @"kb_overseerLeft")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerDown -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "down" (field @"kb_overseerDown")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerRight -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "right" (field @"kb_overseerRight")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered OverseerDash -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "dash" (field @"kb_overseerDash")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered PsiStorm -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "psionic storm" (field @"kb_psiStorm")
        uiSoundHigh alEnv
        pure LoopContinue
      KeybindTriggered DropFlag -> do
        uiSoundHigh alEnv
        keybindTriggeredHandler "drop flag" (field @"kb_dropFlag")
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
  where
    keybindTriggeredHandler ::
      String ->
      ASetter' Keybindings GLFW.Key ->
      IO ()
    keybindTriggeredHandler label setter_ = do
      key <- getKeyPress glEnv label
      updateModel $ set setter_ key

getKeyPress :: GLEnv -> String -> IO GLFW.Key
getKeyPress glEnv label = do
  (screenSize, aspectRatio) <- getFramebufferDims $ gle_window glEnv

  runGL . flip runReaderT (glEnv, screenSize, aspectRatio) $ do
    drawDarkLayer emptyTestParams
    lift $ do
      let screenWidth = fromIntegral screenSize
          x = screenWidth / 2
          y = screenWidth * 3 / 4
          fontAtlas = gle_fontAtlasLarge glEnv
          spacing = fromIntegral $ fa_fontSize fontAtlas
          msgLines = ["press key for [" ++ label ++ "]"]
          color = Float4 1 1 1 1

      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_aspectRatio = aspectRatio
          , cvp_fontAtlas = fontAtlas
          , cvp_screenSize = fromIntegral screenSize
          , cvp_lines =
              snd $
                stackLinesDownward CharAlignCenter (Float2 x y) spacing $
                  map (color,) msgLines
          }
  GLFW.swapBuffers $ gle_window glEnv

  keyRef <- newIORef Nothing
  let keyCallback _window key _scanCode keyState _modifiers =
        case keyState of
          GLFW.KeyState'Pressed -> writeIORef keyRef (Just key)
          GLFW.KeyState'Released -> pure ()
          GLFW.KeyState'Repeating -> pure ()
  let loop = do
        GLFW.pollEvents
        readIORef keyRef >>= \case
          Nothing -> loop
          Just key -> pure key
  withKeyCallback glEnv keyCallback loop
