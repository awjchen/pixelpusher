{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Audio (
  runAudioMenu,
  AudioOptions (..),
  defaultAudioOptions,
  AudioOptionsWidgets,
  audioMenu,
  handleInput,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import Data.Text (Text)

import Client.Audio.Env
import Client.Config.Audio
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env hiding (FullscreenMode (..))

runAudioMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  StateRef AudioOptions ->
  [String] ->
  IO (Maybe ())
runAudioMenu glEnv alEnv themeEnv keybindings controlOpts modelRef messages =
  menuScene'
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    (initialMenuState "audio settings" audioMenu)
    (handleInput alEnv)

type AudioOptionsWidgets :: MenuItemType -> Type
data AudioOptionsWidgets ty where
  MasterVolume :: AudioOptionsWidgets 'IntSlider
  MusicVolume :: AudioOptionsWidgets 'IntSlider
  EffectsVolume :: AudioOptionsWidgets 'IntSlider
  VocoderTracks :: AudioOptionsWidgets 'Toggle
  Back :: AudioOptionsWidgets 'Button

audioMenu ::
  NonEmpty (MenuItem AudioOptionsWidgets AudioOptions)
audioMenu =
  fromJust . NonEmpty.nonEmpty $
    [ IntSliderWidget
        MasterVolume
        (field @"ao_masterVolume")
        (volumeSlider "master volume")
    , IntSliderWidget
        MusicVolume
        (field @"ao_musicVolume")
        (volumeSlider "music volume")
    , IntSliderWidget
        EffectsVolume
        (field @"ao_effectsVolume")
        (volumeSlider "effects volume")
    , ToggleWidget
        VocoderTracks
        (field @"ao_enableVocoderTracks")
        ToggleConfig{tc_label = "vocoder tracks"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

volumeSlider :: Text -> IntSliderConfig
volumeSlider label =
  IntSliderConfig
    { isc_label = label
    , isc_min = 0
    , isc_max = 100
    , isc_step = 5
    }

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent AudioOptionsWidgets) ->
  IO (LoopContinue ())
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged MasterVolume percent -> do
        setVolumePercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged MusicVolume percent -> do
        setMusicGainPercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      IntSliderChanged EffectsVolume percent -> do
        setEffectsGainPercent alEnv percent
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched VocoderTracks vocoderTracksEnabled -> do
        if vocoderTracksEnabled
          then enableVocoderTracks alEnv
          else disableVocoderTracks alEnv
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
