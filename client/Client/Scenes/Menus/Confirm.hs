{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Confirm (
  Confirmation (..),
  ConfirmWidgets,
  confirmMenu,
  handleInput,
) where

import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.Env
import Client.Menu
import Client.Menu.Pure

data Confirmation = Confirmed | Cancelled

type ConfirmWidgets :: MenuItemType -> Type
data ConfirmWidgets ty where
  Cancel :: ConfirmWidgets 'Button
  Confirm :: ConfirmWidgets 'Button

confirmMenu :: NonEmpty (MenuItem ConfirmWidgets ())
confirmMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget
        Cancel
        ButtonConfig{bc_label = "cancel"}
    , ButtonWidget
        Confirm
        ButtonConfig{bc_label = "confirm"}
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent ConfirmWidgets) ->
  IO (LoopContinue Confirmation)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished Cancelled
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Cancel -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Cancelled
      ButtonPressed Confirm -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Confirmed
