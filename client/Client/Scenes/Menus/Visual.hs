{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Visual (
  runVisualMenu,
  VisualOptions (..),
  TeamColorMode (..),
  defaultVisualOptions,
  VisualOptionsWidgets,
  visualMenu,
  handleInput,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.Visual
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env hiding (FullscreenMode (..))
import Client.Visuals.Env qualified as Graphics

runVisualMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  StateRef VisualOptions ->
  [String] ->
  IO (Maybe ())
runVisualMenu glEnv alEnv themeEnv keybindings controlOpts modelRef messages =
  menuScene'
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    (initialMenuState "visual settings" visualMenu)
    (handleInput glEnv alEnv)

type VisualOptionsWidgets :: MenuItemType -> Type
data VisualOptionsWidgets ty where
  Fullscreen :: VisualOptionsWidgets 'Toggle
  ScreenShake :: VisualOptionsWidgets 'Toggle
  TeamColorMode :: VisualOptionsWidgets ('EnumChoice TeamColorMode)
  VisualTheme :: VisualOptionsWidgets ('EnumChoice ThemeSelection)
  Back :: VisualOptionsWidgets 'Button

visualMenu ::
  NonEmpty (MenuItem VisualOptionsWidgets VisualOptions)
visualMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ToggleWidget
        Fullscreen
        (field @"vo_enableFullscreen")
        ToggleConfig{tc_label = "fullscreen"}
    , ToggleWidget
        ScreenShake
        (field @"vo_enableScreenShake")
        ToggleConfig{tc_label = "screenshake"}
    , EnumChoiceWidget
        TeamColorMode
        (field @"vo_teamColorMode")
        EnumChoiceConfig{esc_label = "team colors"}
    , EnumChoiceWidget
        VisualTheme
        (field @"vo_themeSelection")
        EnumChoiceConfig{esc_label = "theme"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  GLEnv ->
  ALEnv ->
  Maybe (MenuUpdateEvent VisualOptionsWidgets) ->
  IO (LoopContinue ())
handleInput glEnv alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched Fullscreen enableFullscreen -> do
        setFullscreenMode glEnv $
          if enableFullscreen then Graphics.Fullscreen else Graphics.Windowed
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched ScreenShake _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged TeamColorMode _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged VisualTheme _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
