{-# LANGUAGE CPP #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Title (
  runTitle,
  TitleResult (..),
  titleMenu,
  TitleWidgets,
) where

import Control.Concurrent (forkIO)
import Control.Monad (void)
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)
import System.Process.Typed

import Client.Audio.Env (ALEnv)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

runTitle ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  [String] ->
  MenuState TitleWidgets () ->
  IO (MenuState TitleWidgets (), Maybe TitleResult)
runTitle glEnv alEnv themeEnv keybindings controlOpts messages menuState = do
  menuScene
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    unitStateRef
    menuState
    (handleInput alEnv)

data TitleResult
  = TitleResult_Multiplayer
  | TitleResult_PrivateMultiplayer
  | TitleResult_SinglePlayer
  | TitleResult_Settings
  | TitleResult_Tutorial
  | TitleResult_Quit

type TitleWidgets :: MenuItemType -> Type
data TitleWidgets ty where
  Multiplayer :: TitleWidgets 'Button
  PrivateMultiplayer :: TitleWidgets 'Button
  SinglePlayer :: TitleWidgets 'Button
  Settings :: TitleWidgets 'Button
  Tutorial :: TitleWidgets 'Button
  Discord :: TitleWidgets 'Button
  Quit :: TitleWidgets 'Button

titleMenu :: NonEmpty (MenuItem TitleWidgets ())
titleMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget Multiplayer (ButtonConfig "multiplayer")
    , ButtonWidget PrivateMultiplayer (ButtonConfig "private multiplayer")
    , ButtonWidget SinglePlayer (ButtonConfig "single player")
    , ButtonWidget Tutorial (ButtonConfig "tutorial")
    , ButtonWidget Settings (ButtonConfig "settings")
    , ButtonWidget Discord (ButtonConfig "discord")
    , ButtonWidget Quit (ButtonConfig "quit")
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent TitleWidgets) ->
  IO (LoopContinue TitleResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape ->
        pure LoopContinue -- Disallow shortcut for exit on title screen
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Multiplayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Multiplayer
      ButtonPressed PrivateMultiplayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_PrivateMultiplayer
      ButtonPressed SinglePlayer -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_SinglePlayer
      ButtonPressed Settings -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Settings
      ButtonPressed Tutorial -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Tutorial
      ButtonPressed Discord -> do
        uiSoundHigh alEnv
        openDiscordServerInvite
        pure LoopContinue
      ButtonPressed Quit -> do
        uiSoundHigh alEnv
        pure $ LoopFinished TitleResult_Quit

openDiscordServerInvite :: IO ()
openDiscordServerInvite =
  void . forkIO . void . runProcess $
    proc
#if defined mingw32_HOST_OS
      "explorer"
#elif defined darwin_HOST_OS
      "open"
#else
      "xdg-open" -- Assuming linux
#endif
      ["https://discord.gg/j8hMRN7g3K"]
