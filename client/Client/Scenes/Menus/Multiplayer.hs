{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Multiplayer (
  runMultiplayerMenu,
  MultiplayerResult (..),
  MultiplayerOptions (..),
  emptyPresetMultiplayerOptions,
  Region (..),
  regionServerAddress,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerName (isNameChar, maxNameLength)

import Client.Audio.Env (ALEnv)
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Config.Multiplayer
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env

runMultiplayerMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  StateRef MultiplayerOptions ->
  [String] ->
  IO (Maybe MultiplayerResult)
runMultiplayerMenu glEnv alEnv themeEnv keybindings controlOpts modelRef messages =
  menuScene'
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    (initialMenuState "multiplayer" multiplayerMenu)
    (handleInput alEnv)

data MultiplayerResult
  = Multiplayer_Connect
  | Multiplayer_Back

type MultiplayerWidgets :: MenuItemType -> Type
data MultiplayerWidgets ty where
  Server :: MultiplayerWidgets ('EnumChoice Region)
  Name :: MultiplayerWidgets 'TextInput
  Class :: MultiplayerWidgets ('EnumChoice PlayerClass)
  Connect :: MultiplayerWidgets 'Button
  Back :: MultiplayerWidgets 'Button

multiplayerMenu ::
  NonEmpty (MenuItem MultiplayerWidgets MultiplayerOptions)
multiplayerMenu =
  fromJust . NonEmpty.nonEmpty $
    [ EnumChoiceWidget
        Server
        (field @"mo_region")
        EnumChoiceConfig{esc_label = "region"}
    , TextInputWidget
        Name
        (field @"mo_playerName")
        TextInputConfig
          { tic_label = "name"
          , tic_maxLength = maxNameLength
          , tic_permittedChars = isNameChar
          }
    , EnumChoiceWidget
        Class
        (field @"mo_playerClass")
        EnumChoiceConfig{esc_label = "starting class"}
    , ButtonWidget
        Connect
        ButtonConfig
          { bc_label = "connect"
          }
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent MultiplayerWidgets) ->
  IO (LoopContinue MultiplayerResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Multiplayer_Connect
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished Multiplayer_Back
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged Server _newServer -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged Class _newServer -> do
        uiSoundHigh alEnv
        pure LoopContinue
      TextChanged Name _newName -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Connect -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Multiplayer_Connect
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished Multiplayer_Back
      FocusTextInput _ ->
        pure LoopContinue
      UnfocusTextInput _ ->
        pure LoopContinue
