{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Pause (
  PauseMenuResult (..),
  PauseWidgets,
  pauseMenu,
  handleInput,
) where

import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.Env
import Client.Menu
import Client.Menu.Pure

data PauseMenuResult
  = Pause_ReturnToGame
  | Pause_AudioOptions
  | Pause_VisualOptions
  | Pause_ControlOptions
  | Pause_QuitGame

type PauseWidgets :: MenuItemType -> Type
data PauseWidgets ty where
  ReturnToGame :: PauseWidgets 'Button
  AudioOptions :: PauseWidgets 'Button
  VisualOptions :: PauseWidgets 'Button
  ControlOptions :: PauseWidgets 'Button
  QuitGame :: PauseWidgets 'Button

pauseMenu :: NonEmpty (MenuItem PauseWidgets ())
pauseMenu =
  fromJust . NonEmpty.nonEmpty $
    [ ButtonWidget
        ReturnToGame
        ButtonConfig{bc_label = "return to game"}
    , ButtonWidget
        AudioOptions
        ButtonConfig{bc_label = "audio settings"}
    , ButtonWidget
        VisualOptions
        ButtonConfig{bc_label = "visual settings"}
    , ButtonWidget
        ControlOptions
        ButtonConfig{bc_label = "control settings"}
    , ButtonWidget
        QuitGame
        ButtonConfig{bc_label = "quit game"}
    ]

handleInput ::
  ALEnv ->
  Maybe (MenuUpdateEvent PauseWidgets) ->
  IO (LoopContinue PauseMenuResult)
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just menuUpdateEvent -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished Pause_ReturnToGame
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed ReturnToGame -> do
        uiSoundLow alEnv
        pure $ LoopFinished Pause_ReturnToGame
      ButtonPressed AudioOptions -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Pause_AudioOptions
      ButtonPressed VisualOptions -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Pause_VisualOptions
      ButtonPressed ControlOptions -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Pause_ControlOptions
      ButtonPressed QuitGame -> do
        uiSoundHigh alEnv
        pure $ LoopFinished Pause_QuitGame
