{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Scenes.Menus.Controls (
  runControlsMenu,
  ControlOptions (..),
  defaultControlOptions,
  ControlsWidgets,
  controlsMenu,
  handleInput,
) where

import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.List.NonEmpty qualified as NonEmpty
import Data.Maybe (fromJust)

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env hiding (FullscreenMode (..))

runControlsMenu ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  Keybindings ->
  StateRef ControlOptions ->
  [String] ->
  IO (Maybe ())
runControlsMenu glEnv alEnv themeEnv keybindings modelRef messages = do
  initialControlOpts <- readStateRef modelRef
  menuScene'
    glEnv
    themeEnv
    keybindings
    initialControlOpts
    messages
    modelRef
    (initialMenuState "control settings" controlsMenu)
    (handleInput alEnv)

type ControlsWidgets :: MenuItemType -> Type
data ControlsWidgets ty where
  DroneControl :: ControlsWidgets ('EnumChoice DroneControl)
  SwapMouseButtons :: ControlsWidgets 'Toggle
  Back :: ControlsWidgets 'Button

controlsMenu :: NonEmpty (MenuItem ControlsWidgets ControlOptions)
controlsMenu =
  fromJust . NonEmpty.nonEmpty $
    [ EnumChoiceWidget
        DroneControl
        (field @"co_droneControl")
        EnumChoiceConfig{esc_label = "drone control"}
    , ToggleWidget
        SwapMouseButtons
        (field @"co_swapMouseButtons")
        ToggleConfig{tc_label = "swap mouse buttons"}
    , ButtonWidget
        Back
        ButtonConfig
          { bc_label = "back"
          }
    ]

handleInput ::
  ALEnv -> Maybe (MenuUpdateEvent ControlsWidgets) -> IO (LoopContinue ())
handleInput alEnv = \case
  Nothing ->
    pure LoopContinue -- Input did not do anything. We should play some notification.
  Just (menuUpdateEvent) -> do
    case menuUpdateEvent of
      Enter ->
        pure LoopContinue
      Escape -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
      MenuScrollUp _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      MenuScrollDown _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      EnumChoiceChanged DroneControl _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ToggleSwitched SwapMouseButtons _ -> do
        uiSoundHigh alEnv
        pure LoopContinue
      ButtonPressed Back -> do
        uiSoundLow alEnv
        pure $ LoopFinished ()
