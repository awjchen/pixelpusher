{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

-- | A scrappy replay viewer.
module Client.Scenes.Replay (
  PlayReplay (..),
  playReplay,
) where

import Control.Applicative (empty)
import Control.Exception (bracket_)
import Control.Monad (replicateM, unless, void, when)
import Control.Monad.IO.Class
import Control.Monad.Morph (hoist, lift)
import Control.Monad.ST
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State.Strict
import Data.ByteString qualified as BS
import Data.ByteString.Lazy qualified as BL
import Data.Foldable (foldlM, for_)
import Data.Generics.Product.Fields (field)
import Data.IORef
import Data.Maybe (fromMaybe, isNothing)
import Data.Sequence (Seq)
import Data.Sequence qualified as Seq
import Data.Serialize qualified as Serialize
import Data.Traversable (for)
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom
import System.IO
import System.Log.FastLogger

import Pixelpusher.Custom.Clock (runClock)
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.SmallList (emptySmallList, fromSmallList)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.GameEvents (GameEvents)
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls (fromControlsTransport)
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Protocol
import Pixelpusher.Game.State
import Pixelpusher.Game.Time

import Client.Audio.Env
import Client.Audio.Game (NotificationSfxPlayback (..), runGameAudio)
import Client.Config.Controls (defaultControlOptions)
import Client.Config.Keybindings
import Client.Config.Visual (ThemeSelection (CustomTheme))
import Client.Logging
import Client.ThemeEnv
import Client.Visuals.CameraInterpolation
import Client.Visuals.Env
import Client.Visuals.Game
import Client.Visuals.SceneDecorations
import Client.Visuals.TeamView

--------------------------------------------------------------------------------
-- Parsing

data Replay = Replay
  { replay_initState :: GameStateSnapshot
  , replay_updates :: [GameStateUpdate]
  }

loadReplay :: FilePath -> IO (Maybe Replay)
loadReplay = fmap readReplay . BL.readFile

readReplay :: BL.ByteString -> Maybe Replay
readReplay bs0 = either (const Nothing) Just $ do
  (initState, bs1) <-
    Serialize.runGetLazyState (Serialize.get @GameStateSnapshot) bs0
  let updates = getUpdates bs1
  pure $ Replay initState updates

getUpdates :: BL.ByteString -> [GameStateUpdate]
getUpdates bs =
  case Serialize.runGetLazyState (Serialize.get @GameStateUpdate) bs of
    Right (update, bs') -> update : getUpdates bs'
    Left _ -> []

--------------------------------------------------------------------------------
-- Playback

data PlaybackState = PlaybackState
  { ps_nextTime :: SyncTime
  , ps_keyFrames :: WIM.IntMap SyncTime (GameStateSnapshot, GameEvents)
  , ps_paused :: Bool
  , ps_view :: View
  , ps_cameraPos :: Float2
  , ps_cameraVel :: Float2
  , ps_events :: GameEvents
  , ps_debugMode :: DebugDisplayMode
  , ps_enableSpatialAudio :: Bool
  , ps_showUI :: Bool
  , ps_showHUD :: Bool
  , ps_showPlayerTags :: Bool
  , ps_showCursor :: Bool
  , ps_showFlagCaptureFx :: Bool
  }
  deriving stock (Generic)

data View
  = FreeView
  | ActorView ActorID
  | GlobalView
  deriving stock (Generic)

ps_cameraPosVel :: Lens' PlaybackState (Float2, Float2)
ps_cameraPosVel f st =
  f (ps_cameraPos st, ps_cameraVel st) <&> \(pos, vel) ->
    st
      { ps_cameraPos = pos
      , ps_cameraVel = vel
      }

initialPlaybackState :: GameStateSnapshot -> PlaybackState
initialPlaybackState snapshot =
  let initTime = getSnapshotTime snapshot
  in  PlaybackState
        { ps_nextTime = initTime
        , ps_keyFrames =
            WIM.singleton initTime (snapshot, mempty)
        , ps_paused = False
        , ps_view = FreeView
        , ps_cameraPos = Float2 0 0
        , ps_cameraVel = Float2 0 0
        , ps_events = mempty
        , ps_debugMode = DDM_None
        , ps_enableSpatialAudio = True
        , ps_showUI = True
        , ps_showHUD = True
        , ps_showPlayerTags = True
        , ps_showCursor = True
        , ps_showFlagCaptureFx = True
        }

data Event
  = TogglePause
  | Seek Ticks
  | PrevFrame
  | NextFrame
  | UseFreeCamera
  | UseGlobalCamera
  | TrackNextPlayer
  | TrackPreviousPlayer
  | ToggleMuteAll
  | ToggleMuteMusic
  | IncreaseVolume
  | DecreaseVolume
  | ToggleSpatialAudio
  | ToggleUIVisibility
  | ToggleHUDVisibility
  | TogglePlayerTagVisibility
  | ToggleCursorVisibility
  | ToggleNotificationSfxPlayback
  | NextDebugMode
  | PrevDebugMode
  | WriteGameState
  | SpliceOutJumps
  | ReloadCustomTheme
  | Quit
  deriving stock (Eq)

handleEvent ::
  ALEnv ->
  ThemeEnv ->
  GameState RealWorld ->
  WIM.IntMap ActorID a ->
  GameStateSnapshot ->
  (SyncTime -> Maybe GameStateUpdate) ->
  PlaybackState ->
  Event ->
  IO PlaybackState
handleEvent alEnv themeEnv gameState players initSnapshot lookupUpdate st event =
  case event of
    TogglePause -> pure $ over (field @"ps_paused") not st
    Seek ticks ->
      pure $ over (field @"ps_nextTime") (addSyncTicks ticks) st
    PrevFrame ->
      pure $ over (field @"ps_nextTime") prevSyncTick st
    NextFrame ->
      pure $ over (field @"ps_nextTime") nextSyncTick st
    UseFreeCamera ->
      pure $ set (field @"ps_view") FreeView st
    UseGlobalCamera ->
      pure $ set (field @"ps_view") GlobalView st
    TrackNextPlayer ->
      pure $
        over
          (field @"ps_view")
          ( \case
              ActorView uid -> ActorView $ maybe uid fst $ WIM.lookupGT uid players
              otherView ->
                maybe otherView (ActorView . fst) $ WIM.lookupMin players
          )
          st
    TrackPreviousPlayer ->
      pure $
        over
          (field @"ps_view")
          ( \case
              ActorView uid -> ActorView $ maybe uid fst $ WIM.lookupLT uid players
              otherView ->
                maybe otherView (ActorView . fst) $ WIM.lookupMax players
          )
          st
    ToggleMuteAll ->
      toggleMute alEnv >> pure st
    ToggleMuteMusic ->
      toggleMusicMute alEnv >> pure st
    IncreaseVolume ->
      increaseVolume alEnv >> pure st
    DecreaseVolume ->
      decreaseVolume alEnv >> pure st
    ToggleSpatialAudio ->
      pure $ over (field @"ps_enableSpatialAudio") not st
    ToggleUIVisibility ->
      pure $ over (field @"ps_showUI") not st
    ToggleHUDVisibility ->
      pure $ over (field @"ps_showHUD") not st
    TogglePlayerTagVisibility ->
      pure $ over (field @"ps_showPlayerTags") not st
    ToggleCursorVisibility ->
      pure $ over (field @"ps_showCursor") not st
    ToggleNotificationSfxPlayback ->
      pure $ over (field @"ps_showFlagCaptureFx") not st
    NextDebugMode ->
      pure $ over (field @"ps_debugMode") nextDebugDisplayMode st
    PrevDebugMode ->
      pure $ over (field @"ps_debugMode") prevDebugDisplayMode st
    WriteGameState -> do
      (_filePath, handle) <- openTempFile "." "pixelpusher-gameStateSnapshot"
      stToIO (freeze gameState) >>= BS.hPut handle . Serialize.encode
      hClose handle
      pure st
    SpliceOutJumps -> do
      spliceReplay gameState initSnapshot lookupUpdate
      pure st
    ReloadCustomTheme -> do
      reloadCustomTheme themeEnv
      pure st
    Quit ->
      pure st

spliceReplay ::
  GameState RealWorld ->
  GameStateSnapshot ->
  (SyncTime -> Maybe GameStateUpdate) ->
  IO ()
spliceReplay gameState initSnapshot lookupUpdate = do
  (_filePath, handle) <- openTempFile "." "pixelpusher-spliced-replay"
  BS.hPut handle $ Serialize.encode initSnapshot
  let initTime = getSnapshotTime initSnapshot

  snapshotJumps <- stToIO $ getGameSnapshotJumps gameState
  let timeInJump' time jump = sj_from jump <= time && time < sj_to jump
      timeInJump time = any (timeInJump' time) snapshotJumps

  currentTime <- stToIO $ getGameSyncTime gameState
  let inSyncTimes =
        takeWhile (<= currentTime) $
          iterate nextSyncTick (nextSyncTick initTime)

  flip evalStateT (SpliceState initTime Playing) $
    for_ inSyncTimes $ \inSyncTime -> do
      let rawUpdate =
            fromMaybe (error "splice out jumps: no such update") $
              lookupUpdate inSyncTime

      pauseState <- do
        field @"ss_pauseState"
          %= case deltaUpdate_debugCommand rawUpdate of
            Nothing -> id
            Just debugCmd ->
              case debugCmd of
                DC_SaveSnapshot _ -> id
                DC_LoadSnapshot _ -> id
                DC_Pause -> const Paused
                DC_Unpause -> \case
                  Paused -> Resuming $ addSyncTicks resumeDelayTicks inSyncTime
                  Resuming t -> Resuming t
                  Playing -> Playing
                DC_SaveMark{} -> id
                DC_ClearMark{} -> id
                DC_DamageDrones{} -> id
                DC_DamageOverseer{} -> id
                DC_SwitchTeams{} -> id
        field @"ss_pauseState" <%= \case
          Paused -> Paused
          Resuming t ->
            if inSyncTime >= t then Playing else Resuming t
          Playing -> Playing

      case pauseState of
        Paused -> pure ()
        Resuming _ -> pure ()
        Playing -> do
          unless (timeInJump inSyncTime) $ do
            outSyncTime <- field @"ss_outFrame" <%= nextSyncTick
            let update =
                  rawUpdate
                    { deltaUpdate_time = outSyncTime
                    , deltaUpdate_debugCommand =
                        -- Remove all debug comands
                        Nothing
                    }
            liftIO $ BS.hPut handle $ Serialize.encode update

  hClose handle

data SpliceState = SpliceState
  { ss_outFrame :: SyncTime
  , ss_pauseState :: PauseState
  }
  deriving stock (Generic)

keyCallback :: IORef (Seq Event) -> GLFW.KeyCallback
keyCallback eventsRef _window key _scanCode keySt mods =
  let addEvent :: Maybe Event -> IO ()
      addEvent Nothing = pure ()
      addEvent (Just event) =
        atomicModifyIORef' eventsRef $ \events -> (events Seq.|> event, ())
  in  addEvent $
        if keySt == GLFW.KeyState'Released
          then Nothing
          else case key of
            GLFW.Key'R ->
              Just ReloadCustomTheme
            -- Pausing
            GLFW.Key'Space -> Just TogglePause
            -- Seeking
            GLFW.Key'H ->
              if
                | GLFW.modifierKeysControl mods && GLFW.modifierKeysShift mods ->
                    Just $ Seek $ fromIntegral (-60 * C.tickRate_hz)
                | GLFW.modifierKeysShift mods ->
                    Just $ Seek $ fromIntegral (-C.tickRate_hz)
                | GLFW.modifierKeysControl mods ->
                    Just $ Seek $ fromIntegral (-15 * C.tickRate_hz)
                | otherwise ->
                    Just $ Seek $ fromIntegral (-4 * C.tickRate_hz)
            GLFW.Key'L ->
              if
                | GLFW.modifierKeysControl mods && GLFW.modifierKeysShift mods ->
                    Just $ Seek $ fromIntegral (60 * C.tickRate_hz)
                | GLFW.modifierKeysShift mods ->
                    Just $ Seek $ fromIntegral C.tickRate_hz
                | GLFW.modifierKeysControl mods ->
                    Just $ Seek $ fromIntegral (15 * C.tickRate_hz)
                | otherwise ->
                    Just $ Seek $ fromIntegral (4 * C.tickRate_hz)
            GLFW.Key'Comma -> Just PrevFrame
            GLFW.Key'Period -> Just NextFrame
            -- Camera
            GLFW.Key'F -> Just UseFreeCamera
            GLFW.Key'G -> Just UseGlobalCamera
            GLFW.Key'P -> Just TrackPreviousPlayer
            GLFW.Key'N ->
              Just $
                if GLFW.modifierKeysControl mods
                  then ToggleNotificationSfxPlayback
                  else TrackNextPlayer
            GLFW.Key'M -> ifCtrl ToggleMuteAll
            GLFW.Key'K -> ifCtrl ToggleMuteMusic
            GLFW.Key'A -> ifCtrl ToggleSpatialAudio
            GLFW.Key'9 -> ifCtrl DecreaseVolume
            GLFW.Key'0 -> ifCtrl IncreaseVolume
            GLFW.Key'U -> ifCtrl ToggleUIVisibility
            GLFW.Key'Y -> ifCtrl ToggleHUDVisibility
            GLFW.Key'T -> ifCtrl TogglePlayerTagVisibility
            GLFW.Key'C -> ifCtrl ToggleCursorVisibility
            GLFW.Key'D ->
              ifCtrl $
                if GLFW.modifierKeysShift mods
                  then PrevDebugMode
                  else NextDebugMode
            GLFW.Key'F12 -> ifCtrl WriteGameState
            GLFW.Key'F9 -> ifCtrl SpliceOutJumps
            GLFW.Key'Q -> ifCtrl Quit
            -- Otherwise
            _ -> Nothing
  where
    ifCtrl cmd = if GLFW.modifierKeysControl mods then Just cmd else Nothing

data PlayReplay
  = PlayReplay FilePath
  | SimulatedBotReplay BaseGameParams
  | SimulatedBotReplayFromSnapshot FilePath

playReplay :: TimedFastLogger -> ThemeEnv -> PlayReplay -> IO ()
playReplay logger themeEnv = \case
  SimulatedBotReplay params -> do
    gameState <- initializeGameState params
    initSnapshot <- stToIO $ freeze gameState
    let lookupUpdate :: SyncTime -> Maybe GameStateUpdate
        lookupUpdate time =
          if initialSyncTime <= time
            && diffSyncTime time initialSyncTime < botReplayTicks
            then
              Just
                GameStateUpdate
                  { deltaUpdate_time = time
                  , deltaUpdate_playerControls = makePlayerIDMap32 mempty
                  , deltaUpdate_gameCommands = emptySmallList
                  , deltaUpdate_debugCommand = Nothing
                  }
            else Nothing
    play logger themeEnv initSnapshot lookupUpdate (addSyncTicks botReplayTicks initialSyncTime)
  SimulatedBotReplayFromSnapshot snapshotFilepath -> do
    snapshotEither <- Serialize.decodeLazy <$> BL.readFile snapshotFilepath
    case snapshotEither of
      Left err -> putStrLn err
      Right snapshot -> do
        let initTime = getSnapshotTime snapshot
            lastTime = addSyncTicks botReplayTicks initTime
            lookupUpdate time =
              if initTime <= time && time < lastTime
                then
                  Just
                    GameStateUpdate
                      { deltaUpdate_time = time
                      , deltaUpdate_playerControls = makePlayerIDMap32 mempty
                      , deltaUpdate_gameCommands = emptySmallList
                      , deltaUpdate_debugCommand = Nothing
                      }
                else Nothing
        play logger themeEnv snapshot lookupUpdate lastTime
  PlayReplay replayFilePath -> do
    replayMaybe <- loadReplay replayFilePath
    case replayMaybe of
      Nothing ->
        writeLog logger $
          toLogStr $
            "Could not load replay from " <> replayFilePath <> "."
      Just replay -> do
        let initSnapshot = replay_initState replay
            updates =
              WIM.fromList $
                map
                  (\update -> (deltaUpdate_time update, update))
                  (replay_updates replay)
            lookupUpdate time = WIM.lookup time updates
            lastTime = maybe initialSyncTime fst $ WIM.lookupMax updates
        play logger themeEnv initSnapshot lookupUpdate lastTime

-- Arbitrary, artificial limit
botReplayTicks :: Ticks
botReplayTicks = fromIntegral C.tickRate_hz * 60 * 60 -- one hour should be enough

-- TODO: try to deduplicate this game loop logic

play ::
  TimedFastLogger ->
  ThemeEnv ->
  GameStateSnapshot ->
  (SyncTime -> Maybe GameStateUpdate) ->
  SyncTime ->
  IO ()
play logger themeEnv initSnapshot lookupUpdate lastTime = do
  let initState = initialPlaybackState initSnapshot

  gameState <- stToIO $ thaw initSnapshot
  eventsRef <- newIORef Seq.empty
  let audioOptions =
        InitialAudioOptions
          { iao_masterGainPercent = 70
          , iao_musicGainPercent = 100
          , iao_effectsGainPercent = 100
          }

  withGLEnv logger EnableVSync $ \glEnv ->
    withKeyCallback glEnv (keyCallback eventsRef) $
      withALEnv logger audioOptions $ \alEnv ->
        withTrackGains alEnv $ do
          void $
            runMaybeT $
              flip evalStateT initState $
                runClock C.frameDelay_ns $ do
                  continue <-
                    hoist lift $
                      playLoopBody glEnv alEnv themeEnv initSnapshot lastTime lookupUpdate eventsRef gameState
                  case continue of
                    Continue -> pure ()
                    DoNotContinue -> empty

withTrackGains :: ALEnv -> IO a -> IO a
withTrackGains alEnv =
  bracket_
    (setTrackGains alEnv initialGains Nothing)
    (setTrackGains alEnv zeroGains Nothing)
  where
    initialGains =
      TrackGains
        { tg_slow = 0
        , tg_fast = 1
        , tg_slowFast = 1
        , tg_haveEnemyFlag = 0
        , tg_alliedFlagStolen = 0
        , tg_chantPanning = 0
        }
    zeroGains =
      TrackGains
        { tg_slow = 0
        , tg_fast = 0
        , tg_slowFast = 0
        , tg_haveEnemyFlag = 0
        , tg_alliedFlagStolen = 0
        , tg_chantPanning = 0
        }

data Continue = Continue | DoNotContinue

playLoopBody ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  GameStateSnapshot ->
  SyncTime ->
  (SyncTime -> Maybe GameStateUpdate) ->
  IORef (Seq Event) ->
  GameState RealWorld ->
  StateT PlaybackState IO Continue
playLoopBody glEnv alEnv themeEnv initSnapshot lastTime lookupUpdate eventsRef gameState = do
  let initTime = getSnapshotTime initSnapshot

  players <- liftIO $ stToIO $ getGamePlayers gameState
  liftIO GLFW.pollEvents

  -- Handle framebuffer size changes
  liftIO $
    gle_pollFramebufferSizeUpdate glEnv >>= \case
      Nothing -> pure ()
      Just (w, h) -> setViewport (fromIntegral w) (fromIntegral h)

  -- Commands
  events <-
    liftIO $
      atomicModifyIORef' eventsRef (Seq.empty,)

  do
    st <- get
    st' <-
      liftIO $
        let handleEvent' =
              handleEvent alEnv themeEnv gameState players initSnapshot lookupUpdate
        in  foldlM handleEvent' st events
    put st'

  -- Get current game state
  mActorID <-
    gets ps_view <&> \case
      ActorView actorID -> Just actorID
      FreeView -> Nothing
      GlobalView -> Nothing

  gameScene <- liftIO $ stToIO $ getGameScene gameState mActorID
  currentTime <- liftIO $ stToIO $ getGameSyncTime gameState

  -- Update camera
  paused <- gets ps_paused
  case mActorID of
    Nothing -> do
      let dx = 0.3
      -- Read key states
      whenPressed GLFW.Key'W $ field @"ps_cameraVel" . _2 += dx
      whenPressed GLFW.Key'A $ field @"ps_cameraVel" . _1 -= dx
      whenPressed GLFW.Key'S $ field @"ps_cameraVel" . _2 -= dx
      whenPressed GLFW.Key'D $ field @"ps_cameraVel" . _1 += dx
      field @"ps_cameraVel" <%= Float.map2 (* 0.96)
        >>= (+=) (field @"ps_cameraPos")
    Just uid -> unless paused $ do
      let mOverseerView =
            pv_overseerView . snd
              <$> WIM.lookup uid (scene_players gameScene)
          stopCamera = field @"ps_cameraVel" .= Float2 0 0
          ticksPerFrame = getTicksPerFrame glEnv
      case mOverseerView of
        Nothing -> stopCamera
        Just overseerView ->
          ps_cameraPosVel
            %= overseerCameraInterpolation
              ticksPerFrame
              overseerView

  -- Render
  get >>= \playbackState -> liftIO $ do
    let gameEvents = ps_events playbackState
        cameraPos = ps_cameraPos playbackState
        debugMode = ps_debugMode playbackState
    theme <- getTheme themeEnv CustomTheme
    renderGameScene glEnv gameScene cameraPos defaultKeybindings defaultControlOptions $
      SceneDecorations
        { sceneDeco_theme = theme
        , sceneDeco_perspective =
            case ps_view playbackState of
              FreeView -> FreePerspective
              GlobalView -> GlobalPerspective
              ActorView actorID ->
                case WIM.lookup actorID (scene_players gameScene) of
                  Nothing -> FreePerspective
                  Just (player, playerView) ->
                    PlayerPerspective $
                      PlayerInfo actorID player playerView
        , sceneDeco_teamPerspective =
            case ps_view playbackState of
              FreeView -> Nothing
              GlobalView -> Nothing
              ActorView actorID ->
                case WIM.lookup actorID (scene_players gameScene) of
                  Nothing -> Nothing
                  Just (player, _) ->
                    Just $ view player_team player
        , sceneDeco_teamView = AbsoluteTeamView
        , sceneDeco_renderStatsOverlay = HideStats
        , sceneDeco_renderHelpOverlay = HideHelp
        , sceneDeco_roomID = Nothing
        , sceneDeco_networkLatency = Nothing
        , sceneDeco_elaspedClientTime = 0
        , sceneDeco_showTutorial = False
        , sceneDeco_showMenu = NoMenuScene -- TODO: enable menu?
        , sceneDeco_showOSCursor = DoNotShowOSCursor
        , sceneDeco_isFramePredicted = False
        , sceneDeco_enableScreenShake = True
        , sceneDeco_enableTutorialMode = False
        , sceneDeco_showDebugInfo = debugMode
        , sceneDeco_spatialAudioStatus = ps_enableSpatialAudio playbackState
        , sceneDeco_showUI = ps_showUI playbackState
        , sceneDeco_showHUD = ps_showHUD playbackState
        , sceneDeco_showPlayerTags = ps_showPlayerTags playbackState
        , sceneDeco_showCursor = ps_showCursor playbackState
        , sceneDeco_showFlagCaptureFlash = ps_showFlagCaptureFx playbackState
        }
    GLFW.swapBuffers (gle_window glEnv)
    runGameAudio
      alEnv
      (ps_enableSpatialAudio playbackState)
      ( if ps_showFlagCaptureFx playbackState
          then PlayNotificationSfx
          else DoNotPlayNotificationSfx
      )
      gameEvents
      gameScene
      cameraPos
      mActorID

  -- Compute next frame
  nextFrameGameEvents <- do
    targetTime <-
      field @"ps_nextTime"
        <%= max initTime
        . min lastTime
        . if paused then id else addSyncTicks stepFrames
    if targetTime == currentTime
      then pure mempty
      else do
        (keyframeTime, (keyframeSnapshot, keyframeEvents)) <-
          gets $
            fromMaybe (initTime, (initSnapshot, mempty))
              . WIM.lookupLE targetTime
              . ps_keyFrames
        if currentTime < targetTime
          && ( targetTime <= addSyncTicks stepFrames currentTime
                || currentTime >= keyframeTime
             )
          then
            let ticks =
                  fromIntegral . getTicks $
                    targetTime `diffSyncTime` currentTime
            in  fmap mconcat $
                  replicateM ticks $
                    integrate initTime lookupUpdate gameState
          else do
            liftIO $ stToIO $ thaw keyframeSnapshot >>= copy gameState
            frameEvents <-
              let ticks =
                    fromIntegral . getTicks $
                      targetTime `diffSyncTime` keyframeTime
               in replicateM ticks $ integrate initTime lookupUpdate gameState
            pure $
              if targetTime < currentTime
                then mempty
                else
                  mconcat $
                    (keyframeEvents :) $
                      take (fromIntegral stepFrames) frameEvents

  field @"ps_events" .= nextFrameGameEvents

  pure $ if Quit `elem` events then DoNotContinue else Continue
  where
    whenPressed :: GLFW.Key -> StateT s IO () -> StateT s IO ()
    whenPressed key action = do
      st <- liftIO $ GLFW.getKey (gle_window glEnv) key
      when (st == GLFW.KeyState'Pressed) action

-- Helpers

-- | Integrate the game state one step. Store a snapshot of the game if it is a
-- keyframe.
integrate ::
  SyncTime ->
  (SyncTime -> Maybe GameStateUpdate) ->
  GameState RealWorld ->
  StateT PlaybackState IO GameEvents
integrate initTime lookupUpdate gameState = zoom (field @"ps_keyFrames") $ do
  -- Integrate
  (newTime, gameEvents) <- liftIO $
    stToIO $ do
      nextTime <- nextSyncTick <$> getGameSyncTime gameState
      gameEvents <-
        fmap (fromMaybe mempty) $ do
          for (lookupUpdate nextTime) $ \update -> do
            let ctrls = WIM.map fromControlsTransport $ getPlayerIDMap32 $ deltaUpdate_playerControls update
                cmds = fromSmallList $ deltaUpdate_gameCommands update
                debugCmd = deltaUpdate_debugCommand update
            integrateGameState ctrls cmds debugCmd gameState -- mutates `gameState`
      newTime <- getGameSyncTime gameState
      pure (newTime, gameEvents)

  -- Save keyframe
  when (isKeyFrame initTime newTime) $ do
    frameDoesNotExist <- gets $ isNothing . WIM.lookup newTime
    when frameDoesNotExist $ do
      snapshot <- liftIO $ stToIO $ freeze gameState
      modify' $ WIM.insert newTime (snapshot, gameEvents)

  pure gameEvents

isKeyFrame :: SyncTime -> SyncTime -> Bool
isKeyFrame initTime n = (n `diffSyncTime` initTime) `mod` 64 == 0

-- Oh, so I've hard-coded for 60 Hz
stepFrames :: Ticks
stepFrames = 2
