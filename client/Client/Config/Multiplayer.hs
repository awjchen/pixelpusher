{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.Multiplayer (
  MultiplayerOptions (..),
  emptyPresetMultiplayerOptions,
  Region (..),
  regionServerAddress,
) where

import Data.String (IsString)
import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.PlayerClass

import Client.Menu.Show

data MultiplayerOptions = MultiplayerOptions
  { mo_region :: Region
  , mo_playerName :: Text
  , mo_playerClass :: PlayerClass
  }
  deriving stock (Generic)

emptyPresetMultiplayerOptions :: MultiplayerOptions
emptyPresetMultiplayerOptions =
  MultiplayerOptions
    { mo_region = NorthAmerica
    , mo_playerName = ""
    , mo_playerClass = defaultPlayerClass
    }

instance Toml.FromValue MultiplayerOptions where
  fromValue =
    Toml.withTable
      "multiplayer options"
      (Toml.genericParseTableDefaults emptyPresetMultiplayerOptions)

instance Toml.ToValue MultiplayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable MultiplayerOptions where
  toTable = Toml.genericToTable

data Region = NorthAmerica | Europe
  deriving stock (Bounded, Enum, Eq, Generic)

regionServerAddress :: Region -> Text
regionServerAddress = \case
  NorthAmerica -> "pixelpusher.one"
  Europe -> "pixelpusher-eu.top"

instance MenuShow Region where
  menuShow = \case
    NorthAmerica -> "N.America"
    Europe -> "Europe"

instance Toml.FromValue Region where
  fromValue = \case
    Toml.Text' _l txt
      | txt == northAmericaLabel -> pure NorthAmerica
      | txt == europeLabel -> pure Europe
    _ ->
      fail $
        "Region is not one of '"
          <> northAmericaLabel
          <> "' or '"
          <> europeLabel
          <> "'"

instance Toml.ToValue Region where
  toValue presetServer =
    Toml.Text $
      case presetServer of
        NorthAmerica -> northAmericaLabel
        Europe -> europeLabel

northAmericaLabel, europeLabel :: (IsString a) => a
northAmericaLabel = "NA"
europeLabel = "EU"
