{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.Visual (
  VisualOptions (..),
  TeamColorMode (..),
  ThemeSelection (..),
  defaultVisualOptions,
) where

import Data.String (IsString)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml

import Client.Menu.Show

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
--
-- TODO: Separate fields into two groups by whether or not the field is the
-- source of truth? In any case, the synchronization logic for the
-- non-source-of-truth fields should be shared.
data VisualOptions = VisualOptions
  { vo_enableFullscreen :: Bool
  , vo_enableScreenShake :: Bool
  , vo_teamColorMode :: TeamColorMode
  , vo_themeSelection :: ThemeSelection
  }
  deriving (Generic)

data TeamColorMode = AbsoluteTeamColors | RelativeTeamColors
  deriving (Bounded, Enum, Eq, Generic)

data ThemeSelection = DefaultTheme | CustomTheme
  deriving (Bounded, Enum, Eq, Generic)

defaultVisualOptions :: VisualOptions
defaultVisualOptions =
  VisualOptions
    { vo_enableFullscreen = True
    , vo_enableScreenShake = True
    , vo_teamColorMode = AbsoluteTeamColors
    , vo_themeSelection = DefaultTheme
    }

instance Toml.FromValue VisualOptions where
  fromValue =
    Toml.withTable
      "visual options"
      (Toml.genericParseTableDefaults defaultVisualOptions)

instance Toml.ToValue VisualOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable VisualOptions where
  toTable = Toml.genericToTable

--------------------------------------------------------------------------------
-- TeamColorMode instances

instance Toml.FromValue TeamColorMode where
  fromValue =
    \case
      Toml.Text' _l txt
        | txt == absoluteTeamColorsLabel -> pure AbsoluteTeamColors
        | txt == relativeTeamColorsLabel -> pure RelativeTeamColors
      _ ->
        fail $
          "Team color mode is not one of '"
            <> absoluteTeamColorsLabel
            <> "' or '"
            <> relativeTeamColorsLabel
            <> "'"

instance Toml.ToValue TeamColorMode where
  toValue = \case
    AbsoluteTeamColors -> Toml.Text absoluteTeamColorsLabel
    RelativeTeamColors -> Toml.Text relativeTeamColorsLabel

absoluteTeamColorsLabel, relativeTeamColorsLabel :: (IsString a) => a
absoluteTeamColorsLabel = "absolute"
relativeTeamColorsLabel = "relative"

instance MenuShow TeamColorMode where
  menuShow = \case
    AbsoluteTeamColors -> absoluteTeamColorsLabel
    RelativeTeamColors -> relativeTeamColorsLabel

--------------------------------------------------------------------------------
-- ThemeSelection instances

instance Toml.FromValue ThemeSelection where
  fromValue =
    \case
      Toml.Text' _l txt
        | txt == defaultThemeLabel -> pure DefaultTheme
        | txt == customThemeLabel -> pure CustomTheme
      _ ->
        fail $
          "Visual theme is not one of '"
            <> defaultThemeLabel
            <> "' or '"
            <> customThemeLabel
            <> "'"

instance Toml.ToValue ThemeSelection where
  toValue = \case
    DefaultTheme -> Toml.Text defaultThemeLabel
    CustomTheme -> Toml.Text customThemeLabel

defaultThemeLabel, customThemeLabel :: (IsString a) => a
defaultThemeLabel = "default"
customThemeLabel = "custom"

instance MenuShow ThemeSelection where
  menuShow = \case
    DefaultTheme -> defaultThemeLabel
    CustomTheme -> customThemeLabel
