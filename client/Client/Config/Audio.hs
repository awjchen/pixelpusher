{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.Audio (
  AudioOptions (..),
  defaultAudioOptions,
) where

import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
--
-- TODO: Separate fields into two groups by whether or not the field is the
-- source of truth? In any case, the synchronization logic for the
-- non-source-of-truth fields should be shared.
data AudioOptions = AudioOptions
  { ao_masterVolume :: Int
  , ao_musicVolume :: Int
  , ao_effectsVolume :: Int
  , ao_enableVocoderTracks :: Bool
  }
  deriving (Generic)

defaultAudioOptions :: AudioOptions
defaultAudioOptions =
  AudioOptions
    { ao_masterVolume = 70
    , ao_musicVolume = 100
    , ao_effectsVolume = 100
    , ao_enableVocoderTracks = True
    }

instance Toml.FromValue AudioOptions where
  fromValue =
    Toml.withTable
      "audiovisual options"
      (Toml.genericParseTableDefaults defaultAudioOptions)

instance Toml.ToValue AudioOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable AudioOptions where
  toTable = Toml.genericToTable
