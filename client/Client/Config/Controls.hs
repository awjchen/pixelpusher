{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.Controls (
  ControlOptions (..),
  DroneControl (..),
  defaultControlOptions,
) where

import Data.String (IsString)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml

import Client.Menu.Show

data ControlOptions = ControlOptions
  { co_swapMouseButtons :: Bool
  , co_droneControl :: DroneControl
  }
  deriving (Generic)

data DroneControl = AutomaticDroneControl | ManualDroneControl
  deriving stock (Bounded, Enum, Eq, Generic, Show)

defaultControlOptions :: ControlOptions
defaultControlOptions =
  ControlOptions
    { co_swapMouseButtons = False
    , co_droneControl = AutomaticDroneControl
    }

instance Toml.FromValue ControlOptions where
  fromValue =
    Toml.withTable
      "audiovisual options"
      (Toml.genericParseTableDefaults defaultControlOptions)

instance Toml.ToValue ControlOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable ControlOptions where
  toTable = Toml.genericToTable

instance Toml.FromValue DroneControl where
  fromValue = \case
    Toml.Text' _l txt
      | txt == automaticDroneControlLabel -> pure AutomaticDroneControl
      | txt == manualDroneControlLabel -> pure ManualDroneControl
    _ ->
      fail $
        "Drone control mode is not one of '"
          <> automaticDroneControlLabel
          <> "' or '"
          <> manualDroneControlLabel
          <> "'"

instance Toml.ToValue DroneControl where
  toValue = \case
    AutomaticDroneControl -> Toml.Text automaticDroneControlLabel
    ManualDroneControl -> Toml.Text manualDroneControlLabel

automaticDroneControlLabel, manualDroneControlLabel :: (IsString a) => a
automaticDroneControlLabel = "automatic"
manualDroneControlLabel = "manual"

instance MenuShow DroneControl where
  menuShow = \case
    AutomaticDroneControl -> automaticDroneControlLabel
    ManualDroneControl -> manualDroneControlLabel
