{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.SinglePlayer (
  SinglePlayerOptions (..),
  emptySinglePlayerOptions,
) where

import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.PlayerClass

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data SinglePlayerOptions = SinglePlayerOptions
  { spo_playerName :: Text
  , spo_botDifficulty :: Int
  , spo_teamSize :: Int
  , spo_playerClass :: PlayerClass
  }
  deriving (Generic)

emptySinglePlayerOptions :: SinglePlayerOptions
emptySinglePlayerOptions =
  SinglePlayerOptions
    { spo_playerName = ""
    , spo_botDifficulty = 1
    , spo_teamSize = 8
    , spo_playerClass = Zealot
    }

instance Toml.FromValue SinglePlayerOptions where
  fromValue =
    Toml.withTable
      "single player options"
      (Toml.genericParseTableDefaults emptySinglePlayerOptions)

instance Toml.ToValue SinglePlayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable SinglePlayerOptions where
  toTable = Toml.genericToTable
