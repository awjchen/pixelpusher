{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Config.CommandLineParameters (
  CommandLineParams (..),
  Command (..),
  parseArgs,
) where

import Data.Text (Text)
import Options.Applicative

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerClass

import Client.Config.PrivateMultiplayer (PrivateMultiplayerOptions (..))
import Client.Observation

--------------------------------------------------------------------------------

data Command
  = RunClient CommandLineParams
  | Replay FilePath
  | BotGame FilePath
  | LoadBotGameSnapshot FilePath
  | WriteGameParameters
  | ObserveGame ObservationParams

data CommandLineParams = CommandLineParams
  { clp_port :: Int
  , clp_multiplayerOptions :: Maybe PrivateMultiplayerOptions
  , clp_gameParamsFilepath :: Maybe FilePath
  }

parseArgs :: IO Command
parseArgs =
  execParser $
    info (helper <*> clientCommand) $
      header ">>= Pixelpusher Client =<<"

clientCommand :: Parser Command
clientCommand =
  fmap Replay replayFile
    <|> fmap BotGame runBotGame
    <|> fmap LoadBotGameSnapshot loadBotGameSnapshot
    <|> fmap RunClient commandLineParams
    <|> fmap (const WriteGameParameters) writeGameParams
    <|> fmap ObserveGame observeGame

--------------------------------------------------------------------------------

replayFile :: Parser FilePath
replayFile =
  strOption $
    mconcat
      [ long "replay"
      , short 'R'
      , metavar "FILE"
      , help "Play a replay file"
      ]

runBotGame :: Parser FilePath
runBotGame =
  strOption $
    mconcat
      [ long "bot-game"
      , short 'B'
      , metavar "FILE"
      , help "Run a bot game with the given game parameters"
      ]

loadBotGameSnapshot :: Parser FilePath
loadBotGameSnapshot =
  strOption $
    mconcat
      [ long "load-bot-game-snapshot"
      , short 'L'
      , metavar "FILE"
      , help "Load and run a bot game from a game snapshot"
      ]

writeGameParams :: Parser ()
writeGameParams =
  flag () () $
    mconcat
      [ long "write-game-parameters"
      , short 'W'
      , help "Write out default game parameters"
      ]

--------------------------------------------------------------------------------

commandLineParams :: Parser CommandLineParams
commandLineParams =
  CommandLineParams
    <$> port
    <*> multiplayerOptions
    <*> gameParamsFilepath

port :: Parser Int
port =
  option auto $
    mconcat
      [ long "port"
      , short 'P'
      , metavar "PORT"
      , value C.defaultPort
      , help "The port on the server to connect to"
      ]

multiplayerOptions :: Parser (Maybe PrivateMultiplayerOptions)
multiplayerOptions =
  optional $ do
    pmo_serverAddress <- serverAddress
    pmo_playerName <- name
    -- TODO: allow class selection from CLI
    let pmo_playerClass = Templar
    pure PrivateMultiplayerOptions{..}

serverAddress :: Parser Text
serverAddress =
  strOption $
    mconcat
      [ long "address"
      , short 'a'
      , metavar "ADDRESS"
      , help "The address of the game server"
      ]

name :: Parser Text
name =
  strOption $
    mconcat
      [ long "name"
      , short 'n'
      , metavar "NAME"
      , help "Join the game under the given name"
      ]

-- room :: Parser Text
-- room =
--   strOption $
--     mconcat
--       [ long "room"
--       , short 'r'
--       , metavar "INT"
--       , value ""
--       , help "Join the game room with the given ID (optional)"
--       ]

gameParamsFilepath :: Parser (Maybe FilePath)
gameParamsFilepath =
  optional $ do
    strOption $
      mconcat
        [ long "game-parameters"
        , short 'g'
        , metavar "FILEPATH"
        , help "The filepath of a game parameters file"
        ]

--------------------------------------------------------------------------------

observeGame :: Parser ObservationParams
observeGame =
  fmap ObservationParams $
    strOption $
      mconcat
        [ long "observe"
        , short 'o'
        , metavar "ADDRESS"
        , help "The address of the game server from which to stream a game"
        ]
