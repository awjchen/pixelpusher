{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Config.PrivateMultiplayer (
  PrivateMultiplayerOptions (..),
  emptyMultiplayerOptions,
) where

import Data.Text (Text)
import GHC.Generics (Generic)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml
import Pixelpusher.Game.PlayerClass

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data PrivateMultiplayerOptions = PrivateMultiplayerOptions
  { pmo_serverAddress :: Text
  , pmo_playerName :: Text
  , pmo_playerClass :: PlayerClass
  }
  deriving (Generic)

emptyMultiplayerOptions :: PrivateMultiplayerOptions
emptyMultiplayerOptions =
  PrivateMultiplayerOptions
    { pmo_serverAddress = ""
    , pmo_playerName = ""
    , pmo_playerClass = defaultPlayerClass
    }

instance Toml.FromValue PrivateMultiplayerOptions where
  fromValue =
    Toml.withTable
      "private multiplayer options"
      (Toml.genericParseTableDefaults emptyMultiplayerOptions)

instance Toml.ToValue PrivateMultiplayerOptions where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable PrivateMultiplayerOptions where
  toTable = Toml.genericToTable
