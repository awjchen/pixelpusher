{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Client.Config.Keybindings (
  Keybindings (..),
  defaultKeybindings,
  keyToString,
  keyToText,
) where

import Data.String
import Data.Text (Text)
import GHC.Generics (Generic)
import Graphics.UI.GLFW
import Graphics.UI.GLFW qualified as GLFW
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml qualified as Toml

--------------------------------------------------------------------------------

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data Keybindings = Keybindings
  { kb_overseerUp :: GLFW.Key
  , kb_overseerLeft :: GLFW.Key
  , kb_overseerDown :: GLFW.Key
  , kb_overseerRight :: GLFW.Key
  , kb_overseerDash :: GLFW.Key
  , kb_psiStorm :: GLFW.Key
  , kb_dropFlag :: GLFW.Key
  }
  deriving stock (Generic, Show)

defaultKeybindings :: Keybindings
defaultKeybindings =
  Keybindings
    { kb_overseerUp = Key'W
    , kb_overseerLeft = Key'A
    , kb_overseerDown = Key'S
    , kb_overseerRight = Key'D
    , kb_overseerDash = Key'Space
    , kb_psiStorm = Key'LeftShift
    , kb_dropFlag = Key'LeftAlt
    }

instance Toml.FromValue Keybindings where
  fromValue =
    Toml.withTable
      "keybindings"
      (Toml.genericParseTableDefaults defaultKeybindings)

instance Toml.ToValue Keybindings where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable Keybindings where
  toTable = Toml.genericToTable

-- Orphan instances
instance Toml.FromValue GLFW.Key where
  fromValue = \case
    Toml.Text' _l txt ->
      case keyFromText txt of
        Nothing -> fail "Invalid key"
        Just key -> pure key
    _ -> fail "Invalid key"

instance Toml.ToValue GLFW.Key where
  toValue = Toml.Text . keyToText

--------------------------------------------------------------------------------
-- GLFW.Key conversion

keyToString :: GLFW.Key -> String
keyToString = keyToString'

keyToText :: GLFW.Key -> Text
keyToText = keyToString'

-- TODO: Are these pragmas necessary?
{-# SPECIALIZE keyToString' :: GLFW.Key -> String #-}
{-# SPECIALIZE keyToString' :: GLFW.Key -> Text #-}
keyToString' :: (IsString a) => GLFW.Key -> a
keyToString' = \case
  Key'Unknown -> "Unknown"
  Key'Space -> "Space"
  Key'Apostrophe -> "Apostrophe"
  Key'Comma -> "Comma"
  Key'Minus -> "Minus"
  Key'Period -> "Period"
  Key'Slash -> "Slash"
  Key'0 -> "0"
  Key'1 -> "1"
  Key'2 -> "2"
  Key'3 -> "3"
  Key'4 -> "4"
  Key'5 -> "5"
  Key'6 -> "6"
  Key'7 -> "7"
  Key'8 -> "8"
  Key'9 -> "9"
  Key'Semicolon -> "Semicolon"
  Key'Equal -> "Equal"
  Key'A -> "A"
  Key'B -> "B"
  Key'C -> "C"
  Key'D -> "D"
  Key'E -> "E"
  Key'F -> "F"
  Key'G -> "G"
  Key'H -> "H"
  Key'I -> "I"
  Key'J -> "J"
  Key'K -> "K"
  Key'L -> "L"
  Key'M -> "M"
  Key'N -> "N"
  Key'O -> "O"
  Key'P -> "P"
  Key'Q -> "Q"
  Key'R -> "R"
  Key'S -> "S"
  Key'T -> "T"
  Key'U -> "U"
  Key'V -> "V"
  Key'W -> "W"
  Key'X -> "X"
  Key'Y -> "Y"
  Key'Z -> "Z"
  Key'LeftBracket -> "LeftBracket"
  Key'Backslash -> "Backslash"
  Key'RightBracket -> "RightBracket"
  Key'GraveAccent -> "GraveAccent"
  Key'World1 -> "World1"
  Key'World2 -> "World2"
  Key'Escape -> "Escape"
  Key'Enter -> "Enter"
  Key'Tab -> "Tab"
  Key'Backspace -> "Backspace"
  Key'Insert -> "Insert"
  Key'Delete -> "Delete"
  Key'Right -> "Right"
  Key'Left -> "Left"
  Key'Down -> "Down"
  Key'Up -> "Up"
  Key'PageUp -> "PageUp"
  Key'PageDown -> "PageDown"
  Key'Home -> "Home"
  Key'End -> "End"
  Key'CapsLock -> "CapsLock"
  Key'ScrollLock -> "ScrollLock"
  Key'NumLock -> "NumLock"
  Key'PrintScreen -> "PrintScreen"
  Key'Pause -> "Pause"
  Key'F1 -> "F1"
  Key'F2 -> "F2"
  Key'F3 -> "F3"
  Key'F4 -> "F4"
  Key'F5 -> "F5"
  Key'F6 -> "F6"
  Key'F7 -> "F7"
  Key'F8 -> "F8"
  Key'F9 -> "F9"
  Key'F10 -> "F10"
  Key'F11 -> "F11"
  Key'F12 -> "F12"
  Key'F13 -> "F13"
  Key'F14 -> "F14"
  Key'F15 -> "F15"
  Key'F16 -> "F16"
  Key'F17 -> "F17"
  Key'F18 -> "F18"
  Key'F19 -> "F19"
  Key'F20 -> "F20"
  Key'F21 -> "F21"
  Key'F22 -> "F22"
  Key'F23 -> "F23"
  Key'F24 -> "F24"
  Key'F25 -> "F25"
  Key'Pad0 -> "Pad0"
  Key'Pad1 -> "Pad1"
  Key'Pad2 -> "Pad2"
  Key'Pad3 -> "Pad3"
  Key'Pad4 -> "Pad4"
  Key'Pad5 -> "Pad5"
  Key'Pad6 -> "Pad6"
  Key'Pad7 -> "Pad7"
  Key'Pad8 -> "Pad8"
  Key'Pad9 -> "Pad9"
  Key'PadDecimal -> "PadDecimal"
  Key'PadDivide -> "PadDivide"
  Key'PadMultiply -> "PadMultiply"
  Key'PadSubtract -> "PadSubtract"
  Key'PadAdd -> "PadAdd"
  Key'PadEnter -> "PadEnter"
  Key'PadEqual -> "PadEqual"
  Key'LeftShift -> "LeftShift"
  Key'LeftControl -> "LeftControl"
  Key'LeftAlt -> "LeftAlt"
  Key'LeftSuper -> "LeftSuper"
  Key'RightShift -> "RightShift"
  Key'RightControl -> "RightControl"
  Key'RightAlt -> "RightAlt"
  Key'RightSuper -> "RightSuper"
  Key'Menu -> "Menu"

keyFromText :: Text -> Maybe GLFW.Key
keyFromText = \case
  "Unknown" -> Just Key'Unknown
  "Space" -> Just Key'Space
  "Apostrophe" -> Just Key'Apostrophe
  "Comma" -> Just Key'Comma
  "Minus" -> Just Key'Minus
  "Period" -> Just Key'Period
  "Slash" -> Just Key'Slash
  "0" -> Just Key'0
  "1" -> Just Key'1
  "2" -> Just Key'2
  "3" -> Just Key'3
  "4" -> Just Key'4
  "5" -> Just Key'5
  "6" -> Just Key'6
  "7" -> Just Key'7
  "8" -> Just Key'8
  "9" -> Just Key'9
  "Semicolon" -> Just Key'Semicolon
  "Equal" -> Just Key'Equal
  "A" -> Just Key'A
  "B" -> Just Key'B
  "C" -> Just Key'C
  "D" -> Just Key'D
  "E" -> Just Key'E
  "F" -> Just Key'F
  "G" -> Just Key'G
  "H" -> Just Key'H
  "I" -> Just Key'I
  "J" -> Just Key'J
  "K" -> Just Key'K
  "L" -> Just Key'L
  "M" -> Just Key'M
  "N" -> Just Key'N
  "O" -> Just Key'O
  "P" -> Just Key'P
  "Q" -> Just Key'Q
  "R" -> Just Key'R
  "S" -> Just Key'S
  "T" -> Just Key'T
  "U" -> Just Key'U
  "V" -> Just Key'V
  "W" -> Just Key'W
  "X" -> Just Key'X
  "Y" -> Just Key'Y
  "Z" -> Just Key'Z
  "LeftBracket" -> Just Key'LeftBracket
  "Backslash" -> Just Key'Backslash
  "RightBracket" -> Just Key'RightBracket
  "GraveAccent" -> Just Key'GraveAccent
  "World1" -> Just Key'World1
  "World2" -> Just Key'World2
  "Escape" -> Just Key'Escape
  "Enter" -> Just Key'Enter
  "Tab" -> Just Key'Tab
  "Backspace" -> Just Key'Backspace
  "Insert" -> Just Key'Insert
  "Delete" -> Just Key'Delete
  "Right" -> Just Key'Right
  "Left" -> Just Key'Left
  "Down" -> Just Key'Down
  "Up" -> Just Key'Up
  "PageUp" -> Just Key'PageUp
  "PageDown" -> Just Key'PageDown
  "Home" -> Just Key'Home
  "End" -> Just Key'End
  "CapsLock" -> Just Key'CapsLock
  "ScrollLock" -> Just Key'ScrollLock
  "NumLock" -> Just Key'NumLock
  "PrintScreen" -> Just Key'PrintScreen
  "Pause" -> Just Key'Pause
  "F1" -> Just Key'F1
  "F2" -> Just Key'F2
  "F3" -> Just Key'F3
  "F4" -> Just Key'F4
  "F5" -> Just Key'F5
  "F6" -> Just Key'F6
  "F7" -> Just Key'F7
  "F8" -> Just Key'F8
  "F9" -> Just Key'F9
  "F10" -> Just Key'F10
  "F11" -> Just Key'F11
  "F12" -> Just Key'F12
  "F13" -> Just Key'F13
  "F14" -> Just Key'F14
  "F15" -> Just Key'F15
  "F16" -> Just Key'F16
  "F17" -> Just Key'F17
  "F18" -> Just Key'F18
  "F19" -> Just Key'F19
  "F20" -> Just Key'F20
  "F21" -> Just Key'F21
  "F22" -> Just Key'F22
  "F23" -> Just Key'F23
  "F24" -> Just Key'F24
  "F25" -> Just Key'F25
  "Pad0" -> Just Key'Pad0
  "Pad1" -> Just Key'Pad1
  "Pad2" -> Just Key'Pad2
  "Pad3" -> Just Key'Pad3
  "Pad4" -> Just Key'Pad4
  "Pad5" -> Just Key'Pad5
  "Pad6" -> Just Key'Pad6
  "Pad7" -> Just Key'Pad7
  "Pad8" -> Just Key'Pad8
  "Pad9" -> Just Key'Pad9
  "PadDecimal" -> Just Key'PadDecimal
  "PadDivide" -> Just Key'PadDivide
  "PadMultiply" -> Just Key'PadMultiply
  "PadSubtract" -> Just Key'PadSubtract
  "PadAdd" -> Just Key'PadAdd
  "PadEnter" -> Just Key'PadEnter
  "PadEqual" -> Just Key'PadEqual
  "LeftShift" -> Just Key'LeftShift
  "LeftControl" -> Just Key'LeftControl
  "LeftAlt" -> Just Key'LeftAlt
  "LeftSuper" -> Just Key'LeftSuper
  "RightShift" -> Just Key'RightShift
  "RightControl" -> Just Key'RightControl
  "RightAlt" -> Just Key'RightAlt
  "RightSuper" -> Just Key'RightSuper
  "Menu" -> Just Key'Menu
  _ -> Nothing
