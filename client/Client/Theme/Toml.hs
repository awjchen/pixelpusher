module Client.Theme.Toml (
  TomlTheme (..),
  defaultTomlTheme,
) where

import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Toml as Toml

import Client.HexColor

--------------------------------------------------------------------------------

-- | Theme data suitable for saving to a .toml file
data TomlTheme = TomlTheme
  { background_color :: HexColor
  , background_pattern_color :: HexColor
  , minimap_background_areaFog_color :: HexColor
  , minimap_background_areaFog_opacity :: Double
  , minimap_background_areaVisible_color :: HexColor
  , minimap_background_areaVisible_opacity :: Double
  , minimap_shieldPowerupSpawners_color :: HexColor
  , minimap_shieldPowerupSpawners_opacity :: Double
  , minimap_teamBlue_base_color :: HexColor
  , minimap_teamBlue_base_opacity :: Double
  , minimap_teamBlue_flag_color :: HexColor
  , minimap_teamBlue_flagStealPing_color :: HexColor
  , minimap_teamBlue_flagStealPing_opacity :: Double
  , minimap_teamBlue_overseer_alive_color :: HexColor
  , minimap_teamBlue_overseer_dead_color :: HexColor
  , minimap_teamBlue_overseer_player_color :: HexColor
  , minimap_teamBlue_spawn_color :: HexColor
  , minimap_teamBlue_spawn_opacity :: Double
  , minimap_teamBlue_visionRing_color :: HexColor
  , minimap_teamBlue_visionRing_opacity :: Double
  , minimap_teamRed_base_color :: HexColor
  , minimap_teamRed_base_opacity :: Double
  , minimap_teamRed_flag_color :: HexColor
  , minimap_teamRed_flagStealPing_color :: HexColor
  , minimap_teamRed_flagStealPing_opacity :: Double
  , minimap_teamRed_overseer_alive_color :: HexColor
  , minimap_teamRed_overseer_dead_color :: HexColor
  , minimap_teamRed_overseer_player_color :: HexColor
  , minimap_teamRed_spawn_color :: HexColor
  , minimap_teamRed_spawn_opacity :: Double
  , minimap_teamRed_visionRing_color :: HexColor
  , minimap_teamRed_visionRing_opacity :: Double
  , obstacle_color :: HexColor
  , obstacle_opacity :: Double
  , psiStorm_color :: HexColor
  , psiStorm_boundary_opacity :: Double
  , psiStorm_fill_opacity :: Double
  , psiStorm_flash_color :: HexColor
  , psiStorm_flash_opacity :: Double
  , psiStorm_ripple_color :: HexColor
  , psiStorm_ripple_opacity :: Double
  , shieldPowerup_color :: HexColor
  , shieldPowerupSpawner_color :: HexColor
  , shieldPowerupSpawner_boundary_opacity :: Double
  , shieldPowerupSpawner_fill_opacity :: Double
  , shieldPowerupSpawner_flash_opacity :: Double
  , teamBlue_ability_cooldown_bar_color :: HexColor
  , teamBlue_base_color :: HexColor
  , teamBlue_base_opacity :: Double
  , teamBlue_control_radius_color :: HexColor
  , teamBlue_control_radius_opacity :: Double
  , teamBlue_dash_cooldown_bar_color :: HexColor
  , teamBlue_dash_dust_color :: HexColor
  , teamBlue_dash_wave_color :: HexColor
  , teamBlue_flag_color :: HexColor
  , teamBlue_flag_recovery_color :: HexColor
  , teamBlue_flag_recovery_opacity :: Double
  , teamBlue_player_color_1 :: HexColor
  , teamBlue_player_color_2 :: HexColor
  , teamBlue_player_color_3 :: HexColor
  , teamBlue_player_color_4 :: HexColor
  , teamBlue_player_color_5 :: HexColor
  , teamBlue_player_color_6 :: HexColor
  , teamBlue_player_color_unique :: HexColor
  , teamBlue_shield_color :: HexColor
  , teamBlue_spawn_color :: HexColor
  , teamBlue_spawn_opacity :: Double
  , teamBlue_team_color :: HexColor
  , teamRed_ability_cooldown_bar_color :: HexColor
  , teamRed_base_color :: HexColor
  , teamRed_base_opacity :: Double
  , teamRed_control_radius_color :: HexColor
  , teamRed_control_radius_opacity :: Double
  , teamRed_dash_cooldown_bar_color :: HexColor
  , teamRed_dash_dust_color :: HexColor
  , teamRed_dash_wave_color :: HexColor
  , teamRed_flag_color :: HexColor
  , teamRed_flag_recovery_color :: HexColor
  , teamRed_flag_recovery_opacity :: Double
  , teamRed_player_color_1 :: HexColor
  , teamRed_player_color_2 :: HexColor
  , teamRed_player_color_3 :: HexColor
  , teamRed_player_color_4 :: HexColor
  , teamRed_player_color_5 :: HexColor
  , teamRed_player_color_6 :: HexColor
  , teamRed_player_color_unique :: HexColor
  , teamRed_shield_color :: HexColor
  , teamRed_spawn_color :: HexColor
  , teamRed_spawn_opacity :: Double
  , teamRed_team_color :: HexColor
  , ui_controls_normal_text_color :: HexColor
  , ui_controls_darker_text_color :: HexColor
  , ui_status_text_color :: HexColor
  , ui_teamBlue_flag_icon_color :: HexColor
  , ui_teamBlue_flag_icon_opacity :: Double
  , ui_teamBlue_overseer_icon_color :: HexColor
  , ui_teamBlue_overseer_icon_opacity :: Double
  , ui_teamRed_flag_icon_color :: HexColor
  , ui_teamRed_flag_icon_opacity :: Double
  , ui_teamRed_overseer_icon_color :: HexColor
  , ui_teamRed_overseer_icon_opacity :: Double
  , ui_text_shadow_color :: HexColor
  , ui_timer_text_color :: HexColor
  , world_boundary_color :: HexColor
  }
  deriving stock (Generic)

instance Toml.FromValue TomlTheme where
  fromValue =
    Toml.parseTableFromValue
      (Toml.genericParseTableDefaults defaultTomlTheme)

instance Toml.ToValue TomlTheme where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable TomlTheme where
  toTable = Toml.genericToTable

defaultTomlTheme :: TomlTheme
defaultTomlTheme =
  TomlTheme
    { background_color = HexColor 0x06 0x06 0x06
    , background_pattern_color = HexColor 0x20 0x20 0x20
    , minimap_background_areaFog_color = HexColor 0x2d 0x2d 0x2d
    , minimap_background_areaFog_opacity = 0.35
    , minimap_background_areaVisible_color = HexColor 0x59 0x59 0x59
    , minimap_background_areaVisible_opacity = 0.35
    , minimap_shieldPowerupSpawners_color = HexColor 0xff 0xff 0xff
    , minimap_shieldPowerupSpawners_opacity = 0.1
    , minimap_teamBlue_base_color = HexColor 0x80 0x80 0xff
    , minimap_teamBlue_base_opacity = 0.33
    , minimap_teamBlue_flag_color = minimapFlagBlue
    , minimap_teamBlue_flagStealPing_color = minimapFlagBlue
    , minimap_teamBlue_flagStealPing_opacity = 0.5
    , minimap_teamBlue_overseer_alive_color = minimapOverseerAliveBlue
    , minimap_teamBlue_overseer_dead_color = minimapOverseerDeadBlue
    , minimap_teamBlue_overseer_player_color = minimapOverseerPlayerBlue
    , minimap_teamBlue_spawn_color = minimapOverseerAliveBlue
    , minimap_teamBlue_spawn_opacity = 0.25
    , minimap_teamBlue_visionRing_color = HexColor 0xff 0xff 0xff
    , minimap_teamBlue_visionRing_opacity = 0.12
    , minimap_teamRed_base_color = HexColor 0xff 0x80 0x80
    , minimap_teamRed_base_opacity = 0.33
    , minimap_teamRed_flag_color = minimapFlagRed
    , minimap_teamRed_flagStealPing_color = minimapFlagRed
    , minimap_teamRed_flagStealPing_opacity = 0.5
    , minimap_teamRed_overseer_alive_color = minimapOverseerAliveRed
    , minimap_teamRed_overseer_dead_color = minimapOverseerDeadRed
    , minimap_teamRed_overseer_player_color = minimapOverseerPlayerRed
    , minimap_teamRed_spawn_color = minimapOverseerAliveRed
    , minimap_teamRed_spawn_opacity = 0.25
    , minimap_teamRed_visionRing_color = HexColor 0xff 0xff 0xff
    , minimap_teamRed_visionRing_opacity = 0.12
    , obstacle_color = HexColor 0xff 0xff 0xff
    , obstacle_opacity = 0.24
    , psiStorm_boundary_opacity = 0.36
    , psiStorm_color = HexColor 0xff 0xff 0xa6
    , psiStorm_fill_opacity = 0.072
    , psiStorm_flash_color = HexColor 0xff 0xff 0xd9
    , psiStorm_flash_opacity = 1
    , psiStorm_ripple_color = HexColor 0xff 0xff 0x00
    , psiStorm_ripple_opacity = 0.85
    , shieldPowerup_color = HexColor 0xff 0xff 0xff
    , shieldPowerupSpawner_color = HexColor 0xff 0xff 0xff
    , shieldPowerupSpawner_boundary_opacity = 0.15
    , shieldPowerupSpawner_fill_opacity = 0.05
    , shieldPowerupSpawner_flash_opacity = 0.175
    , teamBlue_ability_cooldown_bar_color = HexColor 0xff 0xff 0x00
    , teamBlue_base_color = HexColor 0x80 0x80 0xff
    , teamBlue_base_opacity = 0.3
    , teamBlue_control_radius_color = HexColor 0xff 0xff 0xff
    , teamBlue_control_radius_opacity = 0.25
    , teamBlue_dash_cooldown_bar_color = HexColor 0x00 0xff 0xff
    , teamBlue_dash_dust_color = HexColor 0x80 0x80 0x80
    , teamBlue_dash_wave_color = HexColor 0xff 0xff 0xff
    , teamBlue_flag_color = HexColor 0x8c 0x85 0xd9
    , teamBlue_flag_recovery_color = HexColor 0x80 0x80 0xff
    , teamBlue_flag_recovery_opacity = 0.12
    , teamBlue_player_color_1 = turquoise
    , teamBlue_player_color_2 = blue
    , teamBlue_player_color_3 = indigo
    , teamBlue_player_color_4 = pastelTurquoise
    , teamBlue_player_color_5 = pastelBlue
    , teamBlue_player_color_6 = pastelIndigo
    , teamBlue_player_color_unique = teal
    , teamBlue_shield_color = HexColor 0xff 0xff 0xff
    , teamBlue_spawn_color = HexColor 0x80 0x80 0xff
    , teamBlue_spawn_opacity = 0.2
    , teamBlue_team_color = blue
    , teamRed_ability_cooldown_bar_color = HexColor 0xff 0xff 0x00
    , teamRed_base_color = HexColor 0xff 0x80 0x80
    , teamRed_base_opacity = 0.3
    , teamRed_control_radius_color = HexColor 0xff 0xff 0xff
    , teamRed_control_radius_opacity = 0.25
    , teamRed_dash_cooldown_bar_color = HexColor 0xff 0x00 0xff
    , teamRed_dash_dust_color = HexColor 0x80 0x80 0x80
    , teamRed_dash_wave_color = HexColor 0xff 0xff 0xff
    , teamRed_flag_color = HexColor 0xd9 0x6b 0x6b
    , teamRed_flag_recovery_color = HexColor 0xf7 0x4f 0x5c
    , teamRed_flag_recovery_opacity = 0.12
    , teamRed_player_color_1 = red
    , teamRed_player_color_2 = orange
    , teamRed_player_color_3 = pink
    , teamRed_player_color_4 = pastelRed
    , teamRed_player_color_5 = pastelOrange
    , teamRed_player_color_6 = pastelPink
    , teamRed_player_color_unique = gold
    , teamRed_shield_color = HexColor 0xff 0xff 0xff
    , teamRed_spawn_color = HexColor 0xff 0x80 0x80
    , teamRed_spawn_opacity = 0.3
    , teamRed_team_color = red
    , ui_controls_normal_text_color = HexColor 0x80 0x80 0x80
    , ui_controls_darker_text_color = HexColor 0x40 0x40 0x40
    , ui_status_text_color = HexColor 0x80 0x80 0x80
    , ui_teamBlue_flag_icon_color = HexColor 0x00 0x59 0xa1
    , ui_teamBlue_flag_icon_opacity = 0.68
    , ui_teamBlue_overseer_icon_color = HexColor 0x00 0x59 0xa1
    , ui_teamBlue_overseer_icon_opacity = 0.85
    , ui_teamRed_flag_icon_color = HexColor 0xa3 0x29 0x29
    , ui_teamRed_flag_icon_opacity = 0.68
    , ui_teamRed_overseer_icon_color = HexColor 0xa3 0x29 0x29
    , ui_teamRed_overseer_icon_opacity = 0.85
    , ui_text_shadow_color = HexColor 0x00 0x00 0x00
    , ui_timer_text_color = HexColor 0x99 0x99 0x99
    , world_boundary_color = HexColor 0x20 0x20 0x20
    }

--------------------------------------------------------------------------------
-- Default player colors

-- lab(60, 0, 70) (90 deg, 70 radius)
gold :: HexColor
gold = HexColor 0xad 0x8e 0x00

-- lab(60, 15, 56) (75 deg, 50 radius)
pastelOrange :: HexColor
pastelOrange = HexColor 0xbc 0x86 0x39

-- lab(60, 35, 61) (60 deg, 70 radius)
orange :: HexColor
orange = HexColor 0xdc 0x75 0x1d

-- lab(60, 41, 41) (45 deg, 50 radius)
pastelRed :: HexColor
pastelRed = HexColor 0xd8 0x76 0x54

-- lab(60, 61, 35) (30 deg, 70 radius)
red :: HexColor
red = HexColor 0xfa 0x59 0x57

-- lab(60, 56, 15) (15 deg, 50 radius)
pastelPink :: HexColor
pastelPink = HexColor 0xe3 0x6a 0x7c

-- lab(60, 70, 0) (0 deg, 70 radius)
pink :: HexColor
pink = HexColor 0xfb 0x4f 0x93

-- lab(60, -70, 0) (180 deg, 70 radius)
teal :: HexColor
teal = HexColor 0x00 0xad 0x8f

-- lab(60, -40, -12) (196.5 deg, 34.5 radius)
pastelTurquoise :: HexColor
pastelTurquoise = HexColor 0x15 0xa1 0xa1

-- lab(60, -58, -38) (213 deg, 70 radius)
turquoise :: HexColor
turquoise = HexColor 0x00 0xac 0xd2

-- lab(60, -8, -41) (258.5 deg, 34.5 radius)
pastelBlue :: HexColor
pastelBlue = HexColor 0x4e 0x97 0xcc

-- lab(60, -4, -70) (267 deg, 70 radius)
blue :: HexColor
blue = HexColor 0x00 0x9a 0xff

-- lab(60, 6, -41) (278.5 deg, 34.5 radius)
pastelIndigo :: HexColor
pastelIndigo = HexColor 0x72 0x91 0xcc

-- lab(60, 24, -66) (290 deg, 70 radius)
indigo :: HexColor
indigo = HexColor 0x61 0x89 0xff

--------------------------------------------------------------------------------
-- Default minimap colors

-- minimapOverseerDeadRed * 0.85
minimapOverseerAliveRed :: HexColor
minimapOverseerAliveRed = HexColor 0x9d 0x38 0x36

-- lab: 45 brightness, 30 deg, 55 radius
minimapOverseerDeadRed :: HexColor
minimapOverseerDeadRed = HexColor 0xb9 0x42 0x3f

-- 0.825 gold, 0.175 white
minimapOverseerPlayerRed :: HexColor
minimapOverseerPlayerRed = HexColor 0xbb 0xa2 0x2d

-- minimapOverseerDead * 0.85
minimapOverseerAliveBlue :: HexColor
minimapOverseerAliveBlue = HexColor 0x00 0x5d 0xa8

-- lab: 45 brightness, 267 deg, 55 radius
minimapOverseerDeadBlue :: HexColor
minimapOverseerDeadBlue = HexColor 0x00 0x6d 0xc6

-- 0.825 teal, 0.175 white
minimapOverseerPlayerBlue :: HexColor
minimapOverseerPlayerBlue = HexColor 0x2d 0xbb 0xa3

-- lab: 55 brightness, 30 deg, 70 radius
minimapFlagRed :: HexColor
minimapFlagRed = HexColor 0xe9 0x4b 0x4b

-- lab: 55 brightness, 267 deg, 70 radius
minimapFlagBlue :: HexColor
minimapFlagBlue = HexColor 0x00 0x87 0xfd
