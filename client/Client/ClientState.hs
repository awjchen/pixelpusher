{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- The entry point of the client
module Client.ClientState (
  ClientState (..),
  ClientStateSnapshot (..),
  loadClientState,
  saveClientState,
) where

import Control.Monad (unless)
import Data.List (intercalate)
import Data.String (IsString)
import Data.Text (Text)
import Data.Text.IO qualified as Text
import GHC.Generics (Generic)
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (doesFileExist)
import System.Log.FastLogger
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Custom.Toml qualified as Toml

import Client.Config.Audio (AudioOptions, defaultAudioOptions)
import Client.Config.Controls
import Client.Config.Keybindings (Keybindings, defaultKeybindings)
import Client.Config.Multiplayer
import Client.Config.PrivateMultiplayer
import Client.Config.SinglePlayer
import Client.Config.Visual (VisualOptions, defaultVisualOptions)
import Client.Logging
import Client.Menu.Pure
import Client.Scenes.Menus.Settings
import Client.Scenes.Menus.Title

--------------------------------------------------------------------------------

data ClientState = ClientState
  { cs_title :: MenuState TitleWidgets ()
  , cs_settings :: MenuState SettingsWidgets ()
  , cs_audio :: AudioOptions
  , cs_visual :: VisualOptions
  , cs_controls :: ControlOptions
  , cs_singlePlayer :: SinglePlayerOptions
  , cs_multiplayer :: MultiplayerOptions
  , cs_privateMultiplayer :: PrivateMultiplayerOptions
  , cs_keybindings :: Keybindings
  }
  deriving (Generic)

-- Note: These record field names are used for TOML serialization. Changing a
-- field name will cause a player's setting to be lost.
data ClientStateSnapshot = ClientStateSnapshot
  { css_audio :: AudioOptions
  , css_visual :: VisualOptions
  , css_controls :: ControlOptions
  , css_singlePlayer :: SinglePlayerOptions
  , css_multiplayer :: MultiplayerOptions
  , css_privateMultiplayer :: PrivateMultiplayerOptions
  , css_keybindings :: Keybindings
  }
  deriving (Generic)

defaultClientStateSnapshot :: ClientStateSnapshot
defaultClientStateSnapshot =
  ClientStateSnapshot
    { css_audio = defaultAudioOptions
    , css_visual = defaultVisualOptions
    , css_controls = defaultControlOptions
    , css_singlePlayer = emptySinglePlayerOptions
    , css_multiplayer = emptyPresetMultiplayerOptions
    , css_privateMultiplayer = emptyMultiplayerOptions
    , css_keybindings = defaultKeybindings
    }

instance Toml.FromValue ClientStateSnapshot where
  fromValue =
    Toml.withTable
      "client options"
      (Toml.genericParseTableDefaults defaultClientStateSnapshot)

instance Toml.ToValue ClientStateSnapshot where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable ClientStateSnapshot where
  toTable = Toml.genericToTable

thawClientState :: ClientStateSnapshot -> ClientState
thawClientState ClientStateSnapshot{..} =
  ClientState
    { cs_title = initialMenuState "pixelpusher" titleMenu
    , cs_settings = initialMenuState "settings" settingsMenu
    , cs_audio = css_audio
    , cs_visual = css_visual
    , cs_controls = css_controls
    , cs_singlePlayer = css_singlePlayer
    , cs_multiplayer = css_multiplayer
    , cs_privateMultiplayer = css_privateMultiplayer
    , cs_keybindings = css_keybindings
    }

freezeClientState :: ClientState -> ClientStateSnapshot
freezeClientState ClientState{..} =
  ClientStateSnapshot
    { css_audio = cs_audio
    , css_visual = cs_visual
    , css_controls = cs_controls
    , css_singlePlayer = cs_singlePlayer
    , css_multiplayer = cs_multiplayer
    , css_privateMultiplayer = cs_privateMultiplayer
    , css_keybindings = cs_keybindings
    }

defaultClientState :: ClientState
defaultClientState = thawClientState defaultClientStateSnapshot

--------------------------------------------------------------------------------

loadClientState :: TimedFastLogger -> IO (ClientState, [Text])
loadClientState logger = do
  exists <- doesFileExist clientStateFilePath
  if exists
    then do
      writeLog logger $
        "Reading previous settings from " <> clientStateFilePath
      parseResult <- Toml.decode <$> Text.readFile clientStateFilePath
      case parseResult of
        Toml.Failure errors -> do
          writeLog logger $
            "Error reading previous settings: "
              <> toLogStr (intercalate "; " errors)
              <> ". Starting with default settings."
          pure (defaultClientState, [clientStateResetWarning])
        Toml.Success warnings clientStateSnapshot -> do
          unless (null warnings) $
            writeLog logger $
              "Warnings encountered while reading last user parameters: "
                <> toLogStr (intercalate "; " warnings)
          pure (thawClientState clientStateSnapshot, [])
    else do
      writeLog logger $
        "File "
          <> clientStateFilePath
          <> " not found. Starting with default settings."
      pure (defaultClientState, [])

saveClientState :: TimedFastLogger -> ClientState -> IO ()
saveClientState logger clientState = do
  writeLog logger "Saving current settings."
  withWriteFileViaTemp clientStateFilePath $ \handle ->
    Prettyprinter.renderIO handle $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode $
          freezeClientState clientState

clientStateFilePath :: (IsString a) => a
clientStateFilePath = "pixelpusher-options.toml"

clientStateResetWarning :: Text
clientStateResetWarning = "Could not load settings; reverting to defaults."
