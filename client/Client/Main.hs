{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- The entry point of the client
module Client.Main (
  clientMain,
) where

import Control.Exception (finally)
import Control.Monad (unless)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.List (intercalate)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text
import Lens.Micro.Platform.Custom
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (doesFileExist)
import System.IO
import System.Log.FastLogger
import System.Log.FastLogger qualified as FastLogger
import Toml qualified

import Pixelpusher.Custom.Clock (threadDelayNSec)
import Pixelpusher.Custom.Float (Float4 (Float4))
import Pixelpusher.Custom.IO (withWriteFileViaTemp)
import Pixelpusher.Game.Constants qualified as C (defaultPort)
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerName

import Client.Audio.Env
import Client.ClientState
import Client.Config.CommandLineParameters
import Client.Logging
import Client.Menu.Pure (resetMenuNavigation)
import Client.Observation
import Client.Scenes.Menus.Audio
import Client.Scenes.Menus.Controls
import Client.Scenes.Menus.Keybindings
import Client.Scenes.Menus.Multiplayer
import Client.Scenes.Menus.PrivateMultiplayer
import Client.Scenes.Menus.Settings
import Client.Scenes.Menus.SinglePlayer
import Client.Scenes.Menus.Title
import Client.Scenes.Menus.Visual
import Client.Scenes.MultiplayerGame
import Client.Scenes.ObserveGame
import Client.Scenes.Replay
import Client.Scenes.SinglePlayerGame
import Client.Scenes.Tutorial
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env
import Client.Visuals.Rendering.BasicMessage (renderBasicMessage)
import Client.Visuals.Rendering.Preload (preloadVisuals)

--------------------------------------------------------------------------------

clientMain :: IO ()
clientMain = do
  getFormattedTime <- FastLogger.newTimeCache FastLogger.simpleTimeFormat
  let logType =
        LogFile
          FileLogSpec
            { log_file = "pixelpusher-client.log"
            , log_file_size = 4000 * 1024
            , log_backup_number = 2
            }
          FastLogger.defaultBufSize
  withTimedFastLogger getFormattedTime logType $ \logger -> do
    writeLog logger "Starting pixelpusher-client."
    hSetBuffering stdout LineBuffering

    -- Theme
    writeThemeFileIfMissing
    themeEnv <- newThemeEnv logger

    parseArgs >>= \case
      Replay replayFilePath -> do
        hSetBuffering stdout LineBuffering
        playReplay logger themeEnv (PlayReplay replayFilePath)
      BotGame paramsFilePath -> do
        hSetBuffering stdout LineBuffering
        gameParams <- loadGameParameters logger (Just paramsFilePath)
        playReplay logger themeEnv (SimulatedBotReplay gameParams)
      LoadBotGameSnapshot snapshotFilePath -> do
        hSetBuffering stdout LineBuffering
        playReplay logger themeEnv (SimulatedBotReplayFromSnapshot snapshotFilePath)
      RunClient cliParams -> do
        hSetBuffering stdout LineBuffering
        runClient logger themeEnv cliParams
      WriteGameParameters -> do
        hSetBuffering stdout LineBuffering
        writeGameParameters logger
      ObserveGame ObservationParams{..} -> do
        observeGame
          logger
          themeEnv
          (Text.unpack op_serverAddress)
          C.defaultPort
          Nothing
    writeLog logger "Exiting pixelpusher-client."

runClient :: TimedFastLogger -> ThemeEnv -> CommandLineParams -> IO ()
runClient logger themeEnv cliParams = do
  -- TODO: Inform user if there are errors loading the client state
  (initialClientState, clientStateMessages) <- loadClientState logger
  clientStateRef <- newStateRef initialClientState
  flip finally (readStateRef clientStateRef >>= saveClientState logger) $ do
    gameParams <- loadGameParameters logger (clp_gameParamsFilepath cliParams)

    let audioOptions = cs_audio initialClientState
        initialAudioOptions =
          InitialAudioOptions
            { iao_masterGainPercent = ao_masterVolume audioOptions
            , iao_musicGainPercent = ao_musicVolume audioOptions
            , iao_effectsGainPercent = ao_effectsVolume audioOptions
            }

    withALEnv logger initialAudioOptions $ \alEnv -> do
      -- Explictly disable VSync because I can't for the life of me figure out
      -- how to get things to run smoothly with vsync on Windows!!
      withGLEnv logger DisableVSync $ \glEnv -> do
        setFullscreenMode glEnv $
          if vo_enableFullscreen (cs_visual initialClientState)
            then Fullscreen
            else Windowed
        preloadVisuals glEnv
        let env =
              Env
                { env_clientState = clientStateRef
                , env_port = clp_port cliParams
                , env_glEnv = glEnv
                , env_alEnv = alEnv
                , env_gameParams = gameParams
                , env_logger = logger
                , env_themeEnv = themeEnv
                }
        flip runReaderT env $
          case clp_multiplayerOptions cliParams of
            Nothing ->
              titleMenuScene clientStateMessages
            Just multiplayerOpts -> do
              setClientState (field @"cs_privateMultiplayer") multiplayerOpts
              case parseMultiplayerOptions multiplayerOpts of
                Left err ->
                  privateMultiplayerMenuScene [err]
                Right runOptions ->
                  runMultiplayer privateMultiplayerMenuScene runOptions

--------------------------------------------------------------------------------
-- Game parameters

-- Note: We should never overwrite the game parameters file, if it exists.
loadGameParameters :: TimedFastLogger -> Maybe FilePath -> IO BaseGameParams
loadGameParameters logger = \case
  Nothing ->
    pure $ makeBaseGameParams defaultTomlGameParams
  Just gameParamsFilePath ->
    fmap makeBaseGameParams $
      doesFileExist gameParamsFilePath >>= \case
        False -> do
          writeLog logger $
            toLogStr $
              concat
                [ "Could not find/open '"
                , gameParamsFilePath
                , "'. Using default parameters."
                ]
          pure defaultTomlGameParams
        True -> do
          parseResult <- Toml.decode <$> Text.readFile gameParamsFilePath
          case parseResult of
            Toml.Failure errors -> do
              writeLog logger $
                toLogStr $
                  "Error reading game parameters: " <> intercalate "; " errors
              writeLog logger "Using default game parameters"
              pure defaultTomlGameParams
            Toml.Success warnings gameParams -> do
              writeLog logger . toLogStr $
                "Using game parameters from " <> gameParamsFilePath
              unless (null warnings) $ do
                writeLog logger $
                  toLogStr $
                    "Warnings encountered while reading game parameters: "
                      <> intercalate "; " warnings
              pure gameParams

writeGameParameters :: TimedFastLogger -> IO ()
writeGameParameters logger = do
  withWriteFileViaTemp defaultGameParamsFilePath $ \h ->
    Prettyprinter.renderIO h $
      Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
        Toml.encode defaultTomlGameParams
  let msg = "Default game parameters written to " <> defaultGameParamsFilePath
  writeLog logger $ toLogStr msg
  putStrLn msg

defaultGameParamsFilePath :: FilePath
defaultGameParamsFilePath = "pixelpusher-params.toml"

--------------------------------------------------------------------------------
-- State machine

type Scene a = ReaderT Env IO a

data Env = Env
  { env_clientState :: StateRef ClientState
  , env_port :: Int
  , env_glEnv :: GLEnv
  , env_alEnv :: ALEnv
  , env_gameParams :: BaseGameParams
  , env_logger :: TimedFastLogger
  , env_themeEnv :: ThemeEnv
  }

getsClientState :: (ClientState -> a) -> Scene a
getsClientState f = do
  Env{env_clientState} <- ask
  liftIO $ f <$> readStateRef env_clientState

setClientState :: Lens' ClientState a -> a -> Scene ()
setClientState l a = do
  Env{env_clientState} <- ask
  liftIO $ setStateRef l env_clientState a

overClientState :: Lens' ClientState a -> (a -> a) -> Scene ()
overClientState l f = do
  Env{env_clientState} <- ask
  liftIO $ overStateRef l env_clientState f

getOptsStateRef :: Scene (StateRef (AudioOptions, VisualOptions, ControlOptions))
getOptsStateRef = do
  Env{env_clientState} <- ask
  pure $
    zoomStateRef
      ( \f clientSt ->
          f (cs_audio clientSt, cs_visual clientSt, cs_controls clientSt)
            <&> \(audio, visual, controls) ->
              clientSt
                { cs_audio = audio
                , cs_visual = visual
                , cs_controls = controls
                }
      )
      env_clientState

titleMenuScene :: [Text] -> Scene ()
titleMenuScene messages = do
  Env{..} <- ask
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  titleMenuState <- getsClientState cs_title
  let messages' = map Text.unpack messages
  (newTitleMenuState, resultMaybe) <-
    liftIO $
      runTitle
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        messages'
        titleMenuState
  setClientState (field @"cs_title") newTitleMenuState
  for_ resultMaybe $ \case
    TitleResult_Multiplayer ->
      multiplayerMenuScene []
    TitleResult_PrivateMultiplayer ->
      privateMultiplayerMenuScene []
    TitleResult_SinglePlayer ->
      singlePlayerMenuScene []
    TitleResult_Settings ->
      settingsMenuScene []
    TitleResult_Tutorial ->
      tutorialScene
    TitleResult_Quit ->
      pure ()

multiplayerMenuScene :: [Text] -> Scene ()
multiplayerMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  let optsRef = zoomStateRef (field @"cs_multiplayer") env_clientState
  resultMaybe <- do
    liftIO $
      runMultiplayerMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \case
    Multiplayer_Connect -> do
      newOptions <- liftIO $ readStateRef optsRef
      let newOptions' =
            PrivateMultiplayerOptions
              { pmo_serverAddress =
                  regionServerAddress (mo_region newOptions)
              , pmo_playerName = mo_playerName newOptions
              , pmo_playerClass = mo_playerClass newOptions
              }
      case parseMultiplayerOptions newOptions' of
        Left err ->
          multiplayerMenuScene [err]
        Right runOptions ->
          runMultiplayer multiplayerMenuScene runOptions
    Multiplayer_Back ->
      titleMenuScene []

privateMultiplayerMenuScene :: [Text] -> Scene ()
privateMultiplayerMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  let optsRef = zoomStateRef (field @"cs_privateMultiplayer") env_clientState
  resultMaybe <- do
    liftIO $
      runPrivateMultiplayerMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \case
    PrivateMultiplayer_Connect -> do
      newOptions <- liftIO $ readStateRef optsRef
      case parseMultiplayerOptions newOptions of
        Left err ->
          privateMultiplayerMenuScene [err]
        Right runOptions ->
          runMultiplayer privateMultiplayerMenuScene runOptions
    PrivateMultiplayer_Back ->
      titleMenuScene []

singlePlayerMenuScene :: [Text] -> Scene ()
singlePlayerMenuScene messages = do
  Env{..} <- ask
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  let messages' = map Text.unpack messages
  let optsRef = zoomStateRef (field @"cs_singlePlayer") env_clientState
  resultMaybe <-
    liftIO $
      runSinglePlayerMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \result -> do
    SinglePlayerOptions{..} <- liftIO $ readStateRef optsRef
    case result of
      SinglePlayer_Start -> do
        case makePlayerNameText spo_playerName of
          Left err ->
            singlePlayerMenuScene [err]
          Right playerName -> do
            stateRef <- getOptsStateRef
            liftIO $
              runSinglePlayerGame
                env_logger
                env_glEnv
                env_alEnv
                env_themeEnv
                stateRef
                env_gameParams
                keybindings
                playerName
                spo_playerClass
                spo_botDifficulty
                spo_teamSize
            singlePlayerMenuScene []
      SinglePlayer_Back ->
        titleMenuScene []

settingsMenuScene :: [Text] -> Scene ()
settingsMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  settingsMenuState <- getsClientState cs_settings
  (newSettingsMenuState, resultMaybe) <-
    liftIO $
      runSettingsMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        messages'
        settingsMenuState
  setClientState (field @"cs_settings") newSettingsMenuState
  for_ resultMaybe $ \case
    Settings_Audio -> audioOptionsMenuScene []
    Settings_Visual -> visualOptionsMenuScene []
    Settings_Control -> controlsMenuScene []
    Settings_Keybindings -> keybindingsMenuScene []
    Settings_Back -> do
      overClientState (field @"cs_settings") resetMenuNavigation
      titleMenuScene []

audioOptionsMenuScene :: [Text] -> Scene ()
audioOptionsMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  let optsRef = zoomStateRef (field @"cs_audio") env_clientState

  -- Synchronize audio settings
  do
    AudioLevels{..} <- liftIO $ getAudioLevels env_alEnv
    liftIO . modifyStateRef optsRef $ \audioOpts ->
      audioOpts
        { ao_masterVolume = al_masterGainPercent
        , ao_musicVolume = al_musicGainPercent
        , ao_effectsVolume = al_effectsGainPercent
        }
  resultMaybe <-
    liftIO $
      runAudioMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \() -> do
    settingsMenuScene []

visualOptionsMenuScene :: [Text] -> Scene ()
visualOptionsMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  controlOpts <- getsClientState cs_controls
  let optsRef = zoomStateRef (field @"cs_visual") env_clientState

  -- Synchronize visual settings
  do
    enableFullscreen <-
      liftIO $
        getFullscreenMode env_glEnv <&> \case
          Fullscreen -> True
          Windowed -> False
    liftIO . modifyStateRef optsRef $ \visualOpts ->
      visualOpts{vo_enableFullscreen = enableFullscreen}
  resultMaybe <-
    liftIO $
      runVisualMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \() -> do
    settingsMenuScene []

controlsMenuScene :: [Text] -> Scene ()
controlsMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  keybindings <- getsClientState cs_keybindings
  let optsRef = zoomStateRef (field @"cs_controls") env_clientState
  resultMaybe <-
    liftIO $
      runControlsMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        keybindings
        optsRef
        messages'
  for_ resultMaybe $ \() -> do
    settingsMenuScene []

keybindingsMenuScene :: [Text] -> Scene ()
keybindingsMenuScene messages = do
  Env{..} <- ask
  let messages' = map Text.unpack messages
  controlOpts <- getsClientState cs_controls
  let optsRef = zoomStateRef (field @"cs_keybindings") env_clientState
  resultMaybe <-
    liftIO $
      runKeybindingsMenu
        env_glEnv
        env_alEnv
        env_themeEnv
        controlOpts
        optsRef
        messages'
  for_ resultMaybe $ \() -> do
    settingsMenuScene []

runMultiplayer ::
  ([Text] -> Scene ()) -> RunMultiplayerOptions -> Scene ()
runMultiplayer next options@RunMultiplayerOptions{..} = do
  Env{..} <- ask
  liftIO $ renderBasicMessage env_glEnv (Float4 1 1 1 1) "Connecting ..."
  keybindings <- getsClientState cs_keybindings
  stateRef <- getOptsStateRef
  gameExitStatus <-
    liftIO $
      runMultiplayerGame
        env_logger
        env_glEnv
        env_alEnv
        env_themeEnv
        stateRef
        rmo_serverAddress
        env_port
        rmo_playerName
        rmo_playerClass
        Nothing
        keybindings
  case gameExitStatus of
    Exit_ConnectionTimeout -> do
      next ["Could not connect to server (timeout)"]
    Exit_PlayerQuit -> do
      next []
    Exit_Error errMsgs ->
      next errMsgs
    Exit_Rejoin msg ->
      rejoinMultiplayer next options (Text.unpack msg)

rejoinMultiplayer ::
  ([Text] -> Scene ()) -> RunMultiplayerOptions -> String -> Scene ()
rejoinMultiplayer next options msg = do
  glEnv <- asks env_glEnv
  liftIO $ do
    renderBasicMessage glEnv (Float4 1 1 1 1) (msg ++ "\n\nReconnecting ...")
    threadDelayNSec 1_500_000_000
  -- Don't provide a room. If we're being asked to rejoin, the room has
  -- probably been closed.
  runMultiplayer next options

tutorialScene :: Scene ()
tutorialScene = do
  Env{..} <- ask
  keybindings <- getsClientState cs_keybindings
  stateRef <- getOptsStateRef
  liftIO $
    runTutorial
      env_logger
      env_glEnv
      env_alEnv
      env_themeEnv
      stateRef
      env_gameParams
      keybindings
  titleMenuScene []

--------------------------------------------------------------------------------
-- State machine helpers

data RunMultiplayerOptions = RunMultiplayerOptions
  { rmo_serverAddress :: String
  , rmo_playerName :: PlayerName
  , rmo_playerClass :: PlayerClass
  }

-- Helper
parseMultiplayerOptions ::
  PrivateMultiplayerOptions -> Either Text RunMultiplayerOptions
parseMultiplayerOptions PrivateMultiplayerOptions{..} = do
  rmo_serverAddress <-
    if Text.null pmo_serverAddress
      then Left "Please provide a server address"
      else Right $ Text.unpack pmo_serverAddress
  rmo_playerName <- makePlayerNameText pmo_playerName
  let rmo_playerClass = pmo_playerClass
  pure RunMultiplayerOptions{..}

-- -- Helper
-- parseRoomID :: Text -> Either Text (Maybe Int)
-- parseRoomID input =
--   if Text.null input
--     then Right Nothing
--     else case Text.decimal input of
--       Left _ ->
--         Left errMsg
--       Right (roomID, restOfInput) ->
--         if Text.null restOfInput then Right (Just roomID) else Left errMsg
--  where
--   errMsg = "Invalid room"
