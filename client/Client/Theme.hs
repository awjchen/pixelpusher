{-# LANGUAGE RecordWildCards #-}

module Client.Theme (
  Theme (..),
  defaultTheme,
  fromTomlTheme,
) where

import GHC.Float (double2Float)

import Pixelpusher.Custom.Float (Float3 (Float3), Float4)

import Client.HexColor
import Client.Theme.Toml
import Client.Visuals.GL.Util (fromRGB_A)

--------------------------------------------------------------------------------
-- Theme data suitable for rendering

data Theme = Theme
  { theme_backgroundColor :: Float3
  , theme_backgroundPatternColor :: Float3
  , theme_minimap_background_areaFogColor :: Float4
  , theme_minimap_background_areaVisibleColor :: Float4
  , theme_minimap_shieldPowerupSpawnersColor :: Float4
  , theme_minimap_teamBlue_baseColor :: Float4
  , theme_minimap_teamBlue_flagColor :: Float3
  , theme_minimap_teamBlue_flagStealPingColor :: Float4
  , theme_minimap_teamBlue_overseerAliveColor :: Float3
  , theme_minimap_teamBlue_overseerDeadColor :: Float3
  , theme_minimap_teamBlue_overseerPlayerColor :: Float3
  , theme_minimap_teamBlue_spawnColor :: Float4
  , theme_minimap_teamBlue_visionRingColor :: Float4
  , theme_minimap_teamRed_baseColor :: Float4
  , theme_minimap_teamRed_flagColor :: Float3
  , theme_minimap_teamRed_flagStealPingColor :: Float4
  , theme_minimap_teamRed_overseerAliveColor :: Float3
  , theme_minimap_teamRed_overseerDeadColor :: Float3
  , theme_minimap_teamRed_overseerPlayerColor :: Float3
  , theme_minimap_teamRed_spawnColor :: Float4
  , theme_minimap_teamRed_visionRingColor :: Float4
  , theme_obstacleColor :: Float3
  , theme_obstacleOpacity :: Float
  , theme_psiStormBoundaryOpacity :: Float
  , theme_psiStormColor :: Float3
  , theme_psiStormFillOpacity :: Float
  , theme_psiStormFlashColor :: Float3
  , theme_psiStormFlashOpacity :: Float
  , theme_psiStormRippleColor :: Float4
  , theme_shieldPowerupColor :: Float3
  , theme_shieldPowerupSpawnerColor :: Float3
  , theme_shieldPowerupSpawnerBoundaryOpacity :: Float
  , theme_shieldPowerupSpawnerFillOpacity :: Float
  , theme_shieldPowerupSpawnerFlashOpacity :: Float
  , theme_teamBlue_abilityCooldownBarColor :: Float3
  , theme_teamBlue_baseColor :: Float4
  , theme_teamBlue_controlRadiusColor :: Float4
  , theme_teamBlue_dashCooldownBarColor :: Float3
  , theme_teamBlue_dashDustColor :: Float3
  , theme_teamBlue_dashWaveColor :: Float3
  , theme_teamBlue_flagColor :: Float3
  , theme_teamBlue_flagRecoveryColor :: Float4
  , theme_teamBlue_playerColor1 :: Float3
  , theme_teamBlue_playerColor2 :: Float3
  , theme_teamBlue_playerColor3 :: Float3
  , theme_teamBlue_playerColor4 :: Float3
  , theme_teamBlue_playerColor5 :: Float3
  , theme_teamBlue_playerColor6 :: Float3
  , theme_teamBlue_playerColorUnique :: Float3
  , theme_teamBlue_shieldColor :: Float3
  , theme_teamBlue_spawnColor :: Float4
  , theme_teamBlue_teamColor :: Float3
  , theme_teamRed_abilityCooldownBarColor :: Float3
  , theme_teamRed_baseColor :: Float4
  , theme_teamRed_controlRadiusColor :: Float4
  , theme_teamRed_dashCooldownBarColor :: Float3
  , theme_teamRed_dashDustColor :: Float3
  , theme_teamRed_dashWaveColor :: Float3
  , theme_teamRed_flagColor :: Float3
  , theme_teamRed_flagRecoveryColor :: Float4
  , theme_teamRed_playerColor1 :: Float3
  , theme_teamRed_playerColor2 :: Float3
  , theme_teamRed_playerColor3 :: Float3
  , theme_teamRed_playerColor4 :: Float3
  , theme_teamRed_playerColor5 :: Float3
  , theme_teamRed_playerColor6 :: Float3
  , theme_teamRed_playerColorUnique :: Float3
  , theme_teamRed_shieldColor :: Float3
  , theme_teamRed_spawnColor :: Float4
  , theme_teamRed_teamColor :: Float3
  , theme_ui_controlsNormalTextColor :: Float3
  , theme_ui_controlsDarkerTextColor :: Float3
  , theme_ui_statusTextColor :: Float3
  , theme_ui_teamBlue_flagIconColor :: Float4
  , theme_ui_teamBlue_overseerIconColor :: Float4
  , theme_ui_teamRed_flagIconColor :: Float4
  , theme_ui_teamRed_overseerIconColor :: Float4
  , theme_ui_textShadowColor :: Float4
  , theme_ui_timerTextColor :: Float3
  , theme_worldBoundaryColor :: Float3
  }

defaultTheme :: Theme
defaultTheme = fromTomlTheme defaultTomlTheme

fromTomlTheme :: TomlTheme -> Theme
fromTomlTheme TomlTheme{..} =
  Theme
    { theme_backgroundColor = fromHexColor background_color
    , theme_backgroundPatternColor = fromHexColor background_pattern_color
    , theme_minimap_background_areaFogColor =
        fromRGB_A
          (fromHexColor minimap_background_areaFog_color)
          (double2Float minimap_background_areaFog_opacity)
    , theme_minimap_background_areaVisibleColor =
        fromRGB_A
          (fromHexColor minimap_background_areaVisible_color)
          (double2Float minimap_background_areaVisible_opacity)
    , theme_minimap_shieldPowerupSpawnersColor =
        fromRGB_A
          (fromHexColor minimap_shieldPowerupSpawners_color)
          (double2Float minimap_shieldPowerupSpawners_opacity)
    , theme_minimap_teamBlue_baseColor =
        fromRGB_A
          (fromHexColor minimap_teamBlue_base_color)
          (double2Float minimap_teamBlue_base_opacity)
    , theme_minimap_teamBlue_flagColor = fromHexColor minimap_teamBlue_flag_color
    , theme_minimap_teamBlue_flagStealPingColor =
        fromRGB_A
          (fromHexColor minimap_teamBlue_flagStealPing_color)
          (double2Float minimap_teamBlue_flagStealPing_opacity)
    , theme_minimap_teamBlue_overseerAliveColor =
        fromHexColor minimap_teamBlue_overseer_alive_color
    , theme_minimap_teamBlue_overseerDeadColor =
        fromHexColor minimap_teamBlue_overseer_dead_color
    , theme_minimap_teamBlue_overseerPlayerColor =
        fromHexColor minimap_teamBlue_overseer_player_color
    , theme_minimap_teamBlue_spawnColor =
        fromRGB_A
          (fromHexColor minimap_teamBlue_spawn_color)
          (double2Float minimap_teamBlue_spawn_opacity)
    , theme_minimap_teamBlue_visionRingColor =
        fromRGB_A
          (fromHexColor minimap_teamBlue_visionRing_color)
          (double2Float minimap_teamBlue_visionRing_opacity)
    , theme_minimap_teamRed_baseColor =
        fromRGB_A
          (fromHexColor minimap_teamRed_base_color)
          (double2Float minimap_teamRed_base_opacity)
    , theme_minimap_teamRed_flagColor = fromHexColor minimap_teamRed_flag_color
    , theme_minimap_teamRed_flagStealPingColor =
        fromRGB_A
          (fromHexColor minimap_teamRed_flagStealPing_color)
          (double2Float minimap_teamRed_flagStealPing_opacity)
    , theme_minimap_teamRed_overseerAliveColor =
        fromHexColor minimap_teamRed_overseer_alive_color
    , theme_minimap_teamRed_overseerDeadColor =
        fromHexColor minimap_teamRed_overseer_dead_color
    , theme_minimap_teamRed_overseerPlayerColor =
        fromHexColor minimap_teamRed_overseer_player_color
    , theme_minimap_teamRed_spawnColor =
        fromRGB_A
          (fromHexColor minimap_teamRed_spawn_color)
          (double2Float minimap_teamRed_spawn_opacity)
    , theme_minimap_teamRed_visionRingColor =
        fromRGB_A
          (fromHexColor minimap_teamRed_visionRing_color)
          (double2Float minimap_teamRed_visionRing_opacity)
    , theme_obstacleColor = fromHexColor obstacle_color
    , theme_obstacleOpacity = double2Float obstacle_opacity
    , theme_psiStormBoundaryOpacity = double2Float psiStorm_boundary_opacity
    , theme_psiStormColor = fromHexColor psiStorm_color
    , theme_psiStormFillOpacity = double2Float psiStorm_fill_opacity
    , theme_psiStormFlashColor = fromHexColor psiStorm_flash_color
    , theme_psiStormFlashOpacity = double2Float psiStorm_flash_opacity
    , theme_psiStormRippleColor =
        fromRGB_A
          (fromHexColor psiStorm_ripple_color)
          (double2Float psiStorm_ripple_opacity)
    , theme_shieldPowerupColor = fromHexColor shieldPowerup_color
    , theme_shieldPowerupSpawnerColor = fromHexColor shieldPowerupSpawner_color
    , theme_shieldPowerupSpawnerBoundaryOpacity =
        double2Float shieldPowerupSpawner_boundary_opacity
    , theme_shieldPowerupSpawnerFillOpacity =
        double2Float shieldPowerupSpawner_fill_opacity
    , theme_shieldPowerupSpawnerFlashOpacity =
        double2Float shieldPowerupSpawner_flash_opacity
    , theme_teamBlue_abilityCooldownBarColor =
        fromHexColor teamBlue_ability_cooldown_bar_color
    , theme_teamBlue_baseColor =
        fromRGB_A
          (fromHexColor teamBlue_base_color)
          (double2Float teamBlue_base_opacity)
    , theme_teamBlue_controlRadiusColor =
        fromRGB_A
          (fromHexColor teamBlue_control_radius_color)
          (double2Float teamBlue_control_radius_opacity)
    , theme_teamBlue_dashCooldownBarColor =
        fromHexColor teamBlue_dash_cooldown_bar_color
    , theme_teamBlue_dashDustColor = fromHexColor teamBlue_dash_dust_color
    , theme_teamBlue_dashWaveColor = fromHexColor teamBlue_dash_wave_color
    , theme_teamBlue_flagColor = fromHexColor teamBlue_flag_color
    , theme_teamBlue_flagRecoveryColor =
        fromRGB_A
          (fromHexColor teamBlue_flag_recovery_color)
          (double2Float teamBlue_flag_recovery_opacity)
    , theme_teamBlue_playerColor1 = fromHexColor teamBlue_player_color_1
    , theme_teamBlue_playerColor2 = fromHexColor teamBlue_player_color_2
    , theme_teamBlue_playerColor3 = fromHexColor teamBlue_player_color_3
    , theme_teamBlue_playerColor4 = fromHexColor teamBlue_player_color_4
    , theme_teamBlue_playerColor5 = fromHexColor teamBlue_player_color_5
    , theme_teamBlue_playerColor6 = fromHexColor teamBlue_player_color_6
    , theme_teamBlue_playerColorUnique = fromHexColor teamBlue_player_color_unique
    , theme_teamBlue_shieldColor = fromHexColor teamBlue_shield_color
    , theme_teamBlue_spawnColor =
        fromRGB_A
          (fromHexColor teamBlue_spawn_color)
          (double2Float teamBlue_spawn_opacity)
    , theme_teamBlue_teamColor = fromHexColor teamBlue_team_color
    , theme_teamRed_abilityCooldownBarColor =
        fromHexColor teamRed_ability_cooldown_bar_color
    , theme_teamRed_baseColor =
        fromRGB_A
          (fromHexColor teamRed_base_color)
          (double2Float teamRed_base_opacity)
    , theme_teamRed_controlRadiusColor =
        fromRGB_A
          (fromHexColor teamRed_control_radius_color)
          (double2Float teamRed_control_radius_opacity)
    , theme_teamRed_dashCooldownBarColor =
        fromHexColor teamRed_dash_cooldown_bar_color
    , theme_teamRed_dashDustColor = fromHexColor teamRed_dash_dust_color
    , theme_teamRed_dashWaveColor = fromHexColor teamRed_dash_wave_color
    , theme_teamRed_flagColor = fromHexColor teamRed_flag_color
    , theme_teamRed_flagRecoveryColor =
        fromRGB_A
          (fromHexColor teamRed_flag_recovery_color)
          (double2Float teamRed_flag_recovery_opacity)
    , theme_teamRed_playerColor1 = fromHexColor teamRed_player_color_1
    , theme_teamRed_playerColor2 = fromHexColor teamRed_player_color_2
    , theme_teamRed_playerColor3 = fromHexColor teamRed_player_color_3
    , theme_teamRed_playerColor4 = fromHexColor teamRed_player_color_4
    , theme_teamRed_playerColor5 = fromHexColor teamRed_player_color_5
    , theme_teamRed_playerColor6 = fromHexColor teamRed_player_color_6
    , theme_teamRed_playerColorUnique = fromHexColor teamRed_player_color_unique
    , theme_teamRed_shieldColor = fromHexColor teamRed_shield_color
    , theme_teamRed_spawnColor =
        fromRGB_A
          (fromHexColor teamRed_spawn_color)
          (double2Float teamRed_spawn_opacity)
    , theme_teamRed_teamColor = fromHexColor teamRed_team_color
    , theme_ui_controlsNormalTextColor =
        fromHexColor ui_controls_normal_text_color
    , theme_ui_controlsDarkerTextColor =
        fromHexColor ui_controls_darker_text_color
    , theme_ui_statusTextColor = fromHexColor ui_status_text_color
    , theme_ui_teamBlue_flagIconColor =
        fromRGB_A
          (fromHexColor ui_teamBlue_flag_icon_color)
          (double2Float ui_teamBlue_flag_icon_opacity)
    , theme_ui_teamBlue_overseerIconColor =
        fromRGB_A
          (fromHexColor ui_teamBlue_overseer_icon_color)
          (double2Float ui_teamBlue_overseer_icon_opacity)
    , theme_ui_teamRed_flagIconColor =
        fromRGB_A
          (fromHexColor ui_teamRed_flag_icon_color)
          (double2Float ui_teamRed_flag_icon_opacity)
    , theme_ui_teamRed_overseerIconColor =
        fromRGB_A
          (fromHexColor ui_teamRed_overseer_icon_color)
          (double2Float ui_teamRed_overseer_icon_opacity)
    , theme_ui_textShadowColor =
        -- Hard-coding full opacity for now
        fromRGB_A (fromHexColor ui_text_shadow_color) 1
    , theme_ui_timerTextColor = fromHexColor ui_timer_text_color
    , theme_worldBoundaryColor = fromHexColor world_boundary_color
    }

fromHexColor :: HexColor -> Float3
fromHexColor (HexColor r g b) = Float3 (f r) (f g) (f b)
  where
    f = (/ 255) . fromIntegral
