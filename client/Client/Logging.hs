{-# LANGUAGE OverloadedStrings #-}

-- See also "Server.Logging"
module Client.Logging (
  writeLog,
  toLogStr,
) where

import System.Log.FastLogger

-- Default log format
writeLog :: TimedFastLogger -> LogStr -> IO ()
writeLog logger logStr =
  logger $ \timeStr -> toLogStr timeStr <> " " <> logStr <> "\n"
