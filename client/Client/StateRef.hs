{-# LANGUAGE RecordWildCards #-}

module Client.StateRef (
  StateRef,
  newStateRef,
  unitStateRef,
  readStateRef,
  writeStateRef,
  modifyStateRef,
  zoomStateRef,
  setStateRef,
  overStateRef,
) where

import Data.IORef
import Lens.Micro.Platform.Custom

-- | An abstract IORef that supports "zoom"-ing with lenses. Not thread-safe.

-- I wanted to use the state effect from the "cleff" package, but "cleff" is
-- marked as broken on nixpkgs, so I'm doing this instead.
data StateRef a = StateRef
  { sr_read :: IO a
  , sr_modify :: (a -> a) -> IO ()
  }

newStateRef :: a -> IO (StateRef a)
newStateRef initVal =
  newIORef initVal <&> \ref ->
    StateRef
      { sr_read = readIORef ref
      , sr_modify = modifyIORef' ref
      }

unitStateRef :: StateRef ()
unitStateRef =
  StateRef
    { sr_read = pure ()
    , sr_modify = const (pure ())
    }

readStateRef :: StateRef a -> IO a
readStateRef StateRef{sr_read} = sr_read
{-# INLINE readStateRef #-}

writeStateRef :: StateRef a -> a -> IO ()
writeStateRef StateRef{sr_modify} = sr_modify . const
{-# INLINE writeStateRef #-}

modifyStateRef :: StateRef a -> (a -> a) -> IO ()
modifyStateRef StateRef{sr_modify} = sr_modify
{-# INLINE modifyStateRef #-}

zoomStateRef :: Lens' a b -> StateRef a -> StateRef b
zoomStateRef l StateRef{..} =
  StateRef
    { sr_read = view l <$> sr_read
    , sr_modify = sr_modify . over l
    }

-- | Lensy version of 'writeStateRef'
setStateRef :: Lens' a b -> StateRef a -> b -> IO ()
setStateRef l StateRef{sr_modify} = sr_modify . over l . const
{-# INLINE setStateRef #-}

-- | Lensy version of 'modifyStateRef'
overStateRef :: Lens' a b -> StateRef a -> (b -> b) -> IO ()
overStateRef l StateRef{sr_modify} = sr_modify . over l
{-# INLINE overStateRef #-}
