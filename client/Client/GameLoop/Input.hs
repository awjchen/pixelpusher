{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE MultiWayIf #-}

module Client.GameLoop.Input (
  InputEventsHandle,
  newInputEventsHandle,
  gameKeyCallback,
  optionsKeyCallback,
  pollInputs,
  FrameInputs (..),
  PlayerContinueStatus (..),
  UIEvent (..),
) where

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TQueue
import Control.Monad.IO.Class
import Data.Foldable (for_)
import Data.IORef
import Data.Maybe (fromMaybe)
import GHC.Float
import Graphics.UI.GLFW hiding (KeyCallback)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Debug
import Pixelpusher.Game.PlayerControls

import Client.Config.Controls
import Client.Config.Keybindings
import Client.Constants qualified as CC
import Client.GlobalKeyCallback (globalKeyCallback)
import Client.Menu.GLFW qualified
import Client.Menu.Input (MenuInputEvent (..), MenuInputQueue)
import Client.Menu.Input qualified as Menu
import Client.ThemeEnv
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.GLFW.KeyCallback
import Client.Visuals.SceneDecorations

--------------------------------------------------------------------------------
-- Input callbacks

{-
Quering with `GLFW.getKey` is not guaranteed to capture all keypresses (e.g. if
the key is pressed and released between calls to `GLFW.pollEvents`), so
key callbacks should be used for keypresses that must be captured.
-}

data InputEventsHandle = InputEventsHandle
  { inputEventsQueue :: TQueue UIEvent
  , debugEventsQueue :: TQueue DebugCommand
  }

newInputEventsHandle :: IO InputEventsHandle
newInputEventsHandle = InputEventsHandle <$> newTQueueIO <*> newTQueueIO

data UIEvent
  = EscapePressed
  | CtrlPressed

gameKeyCallback' ::
  TQueue UIEvent ->
  TQueue DebugCommand ->
  IORef Fixed2 ->
  IORef (Maybe ActorID) ->
  KeyCallback
gameKeyCallback' uiEventQueue debugEventQueue cursorPosRef actorIDRef =
  KeyCallback $ \_window key _scanCode keySt mods ->
    let firstPress = keySt == GLFW.KeyState'Pressed
    in  if
          | key == GLFW.Key'Escape && firstPress ->
              Just . atomically $
                writeTQueue uiEventQueue EscapePressed
          | (key == GLFW.Key'LeftControl || key == GLFW.Key'RightControl) && firstPress ->
              Just . atomically $
                writeTQueue uiEventQueue CtrlPressed
          | key == GLFW.Key'1 && firstPress ->
              Just . atomically . writeTQueue debugEventQueue $
                if GLFW.modifierKeysControl mods
                  then DC_SaveSnapshot Snapshot1
                  else DC_LoadSnapshot Snapshot1
          | key == GLFW.Key'2 && firstPress ->
              Just . atomically . writeTQueue debugEventQueue $
                if GLFW.modifierKeysControl mods
                  then DC_SaveSnapshot Snapshot2
                  else DC_LoadSnapshot Snapshot2
          | key == GLFW.Key'3 && firstPress ->
              Just . atomically . writeTQueue debugEventQueue $
                if GLFW.modifierKeysControl mods
                  then DC_SaveSnapshot Snapshot3
                  else DC_LoadSnapshot Snapshot3
          | key == GLFW.Key'P && firstPress ->
              Just . atomically . writeTQueue debugEventQueue $
                if GLFW.modifierKeysControl mods
                  then DC_Unpause
                  else DC_Pause
          | key == GLFW.Key'6 && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark1
                    else DC_SaveMark Mark1 cursorPos
          | key == GLFW.Key'7 && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark2
                    else DC_SaveMark Mark2 cursorPos
          | key == GLFW.Key'8 && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark3
                    else DC_SaveMark Mark3 cursorPos
          | key == GLFW.Key'9 && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark4
                    else DC_SaveMark Mark4 cursorPos
          | key == GLFW.Key'0 && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark5
                    else DC_SaveMark Mark5 cursorPos
          | key == GLFW.Key'Minus && firstPress ->
              Just $ do
                cursorPos <- readIORef cursorPosRef
                atomically . writeTQueue debugEventQueue $
                  if GLFW.modifierKeysControl mods
                    then DC_ClearMark Mark6
                    else DC_SaveMark Mark6 cursorPos
          | key == GLFW.Key'M && firstPress ->
              Just $ do
                actorIDMaybe <- readIORef actorIDRef
                for_ actorIDMaybe $ \actorID ->
                  atomically . writeTQueue debugEventQueue $
                    DC_DamageDrones actorID
          | key == GLFW.Key'N && firstPress ->
              Just $ do
                actorIDMaybe <- readIORef actorIDRef
                for_ actorIDMaybe $ \actorID ->
                  atomically . writeTQueue debugEventQueue $
                    DC_DamageOverseer actorID
          | key == GLFW.Key'T && firstPress ->
              Just $ do
                actorIDMaybe <- readIORef actorIDRef
                for_ actorIDMaybe $ \actorID ->
                  atomically . writeTQueue debugEventQueue $
                    DC_SwitchTeams actorID
          | otherwise ->
              Nothing

gameKeyCallback ::
  ThemeEnv ->
  InputEventsHandle ->
  IORef Fixed2 ->
  IORef (Maybe ActorID) ->
  GLFW.KeyCallback
gameKeyCallback themeEnv inputEventsHandle cursorPosRef actorIDRef =
  let InputEventsHandle uiEventQueue debugEventQueue = inputEventsHandle
  in  toGLFWKeyCallback $
        globalKeyCallback themeEnv
          <> gameKeyCallback' uiEventQueue debugEventQueue cursorPosRef actorIDRef

optionsKeyCallback ::
  ThemeEnv ->
  MenuInputQueue ->
  Keybindings ->
  GLFW.KeyCallback
optionsKeyCallback themeEnv menuInputQueue keybindings =
  toGLFWKeyCallback $
    globalKeyCallback themeEnv
      <> Client.Menu.GLFW.keyCallback menuInputQueue keybindings

--------------------------------------------------------------------------------
-- Input polling

data FrameInputs = FrameInputs
  { fi_controls :: PlayerControlsTransport
  , fi_continueStatus :: PlayerContinueStatus
  , fi_displayStats :: DisplayStats
  , fi_displayHelp :: DisplayHelp
  , fi_uiEvents :: [UIEvent]
  , fi_menuEvents :: [MenuInputEvent]
  , fi_debugEvents :: [DebugCommand]
  }

data PlayerContinueStatus
  = PlayerContinue
  | PlayerQuit

-- | Calls `GLFW.pollEvents`
pollInputs ::
  GLEnv ->
  InputEventsHandle ->
  MenuInputQueue ->
  Keybindings ->
  ControlOptions ->
  Float2 ->
  IO FrameInputs
pollInputs
  glEnv
  inputEventsHandle
  menuInputQueue
  keybindings
  controlOpts
  cameraPos = do
    GLFW.pollEvents
    uiEvents <- atomically $ flushTQueue $ inputEventsQueue inputEventsHandle
    debugEvents <- atomically $ flushTQueue $ debugEventsQueue inputEventsHandle
    menuEvents <- Menu.flushQueue menuInputQueue
    playerControls <- do
      let window = gle_window glEnv
      windowSize <- GLFW.getWindowSize window
      readPlayerControls
        (GLFW.getKey window)
        (GLFW.getMouseButton window)
        (GLFW.getCursorPos window)
        (GLFW.getGamepadState GLFW.Joystick'1) -- hard-coding for first controller
        windowSize
        cameraPos
        keybindings
        controlOpts
    (statsOverlay, helpScreen) <- checkHolds glEnv
    playerContinueStatus <- checkPlayerQuit glEnv
    pure
      FrameInputs
        { fi_controls = playerControls
        , fi_continueStatus = playerContinueStatus
        , fi_displayStats = statsOverlay
        , fi_displayHelp = helpScreen
        , fi_uiEvents = uiEvents
        , fi_menuEvents = menuEvents
        , fi_debugEvents = debugEvents
        }

readPlayerControls ::
  (Applicative f) =>
  (Key -> f KeyState) ->
  (MouseButton -> f MouseButtonState) ->
  f (Double, Double) ->
  f (Maybe GamepadState) ->
  (Int, Int) ->
  Float2 ->
  Keybindings ->
  ControlOptions ->
  f PlayerControlsTransport
readPlayerControls
  glfwGetKey
  glfwGetMouseButton
  glfwGetCursorPos
  glfwGetGamepadState
  windowSize
  cameraPos
  keybindings
  controlOpts = do
    let isPressed = fmap keyIsPressed . glfwGetKey
    up <- isPressed $ kb_overseerUp keybindings
    left <- isPressed $ kb_overseerLeft keybindings
    down <- isPressed $ kb_overseerDown keybindings
    right <- isPressed $ kb_overseerRight keybindings
    psiStorm <- isPressed $ kb_psiStorm keybindings
    overseerDash <- isPressed $ kb_overseerDash keybindings
    dropFlag <- isPressed $ kb_dropFlag keybindings

    let isMousePressed = fmap (== MouseButtonState'Pressed) . glfwGetMouseButton
    rawMouse1 <- isMousePressed MouseButton'1
    rawMouse2 <- isMousePressed MouseButton'2
    cursorPos <- glfwGetCursorPos

    gamepadStateMaybe <- glfwGetGamepadState

    pure $
      let invertButton =
            case co_droneControl controlOpts of
              AutomaticDroneControl -> not
              ManualDroneControl -> id
          (mouse1, mouse2) =
            if co_swapMouseButtons controlOpts
              then (invertButton rawMouse2, rawMouse1)
              else (invertButton rawMouse1, rawMouse2)
          droneCmd =
            case (mouse1, mouse2) of
              (False, False) -> DroneNeutral
              (False, True) -> DroneRepulsion
              (True, False) -> DroneAttraction
              (True, True) -> DroneNeutral

          moveCmd =
            let keyMoveCmd_x = case (left, right) of
                  (False, False) -> EQ
                  (False, True) -> GT
                  (True, False) -> LT
                  (True, True) -> EQ
                keyMoveCmd_y = case (down, up) of
                  (False, False) -> EQ
                  (False, True) -> GT
                  (True, False) -> LT
                  (True, True) -> EQ
                keyMoveCmd =
                  case (keyMoveCmd_x, keyMoveCmd_y) of
                    (LT, LT) -> MoveSW
                    (LT, EQ) -> MoveW
                    (LT, GT) -> MoveNW
                    (EQ, LT) -> MoveS
                    (EQ, EQ) -> MoveNeutral
                    (EQ, GT) -> MoveN
                    (GT, LT) -> MoveSE
                    (GT, EQ) -> MoveE
                    (GT, GT) -> MoveNE

                controllerMoveCmd =
                  fromMaybe MoveNeutral $
                    flip fmap gamepadStateMaybe $ \gamepadState ->
                      let leftStickCmd =
                            joystickMoveCommand
                              (GLFW.getAxisState gamepadState GLFW.GamepadAxis'LeftX)
                              (GLFW.getAxisState gamepadState GLFW.GamepadAxis'LeftY)
                          rightStickCmd =
                            joystickMoveCommand
                              (GLFW.getAxisState gamepadState GLFW.GamepadAxis'RightX)
                              (GLFW.getAxisState gamepadState GLFW.GamepadAxis'RightY)
                      in  if leftStickCmd /= MoveNeutral
                            then leftStickCmd
                            else rightStickCmd
            in  if controllerMoveCmd /= MoveNeutral
                  then controllerMoveCmd
                  else keyMoveCmd

          psiStormCmd =
            let gamepadPsiStorm =
                  fromMaybe False $
                    flip fmap gamepadStateMaybe $ \gamepadState ->
                      let triggerPressed axis =
                            GLFW.getAxisState gamepadState axis > -0.8
                      in  triggerPressed GLFW.GamepadAxis'LeftTrigger
                            || triggerPressed GLFW.GamepadAxis'RightTrigger
            in  if psiStorm || gamepadPsiStorm then PsiStormOn else PsiStormOff

          overseerDashCmd =
            let gamepadOverseerDash =
                  fromMaybe False $
                    flip fmap gamepadStateMaybe $ \gamepadState ->
                      let buttonPressed button =
                            GLFW.getButtonState
                              gamepadState
                              button
                              == GLFW.GamepadButtonState'Pressed
                      in  buttonPressed GLFW.GamepadButton'LeftBumper
                            || buttonPressed GLFW.GamepadButton'RightBumper
            in  if overseerDash || gamepadOverseerDash
                  then OverseerDashOn
                  else OverseerDashOff

          flagCmd =
            let gamepadDropFlag =
                  fromMaybe False $
                    flip fmap gamepadStateMaybe $ \gamepadState ->
                      let buttonPressed button =
                            GLFW.getButtonState
                              gamepadState
                              button
                              == GLFW.GamepadButtonState'Pressed
                      in  buttonPressed GLFW.GamepadButton'LeftThumb
                            || buttonPressed GLFW.GamepadButton'RightThumb
            in  if dropFlag || gamepadDropFlag then FlagDrop else FlagNeutral

          buttonStates =
            defaultButtonStates
              & control_move
              .~ moveCmd
              & control_drone
              .~ droneCmd
              & control_psiStorm
              .~ psiStormCmd
              & control_flag
              .~ flagCmd
              & control_overseerDash
              .~ overseerDashCmd

          worldPos =
            CS.intoPos (CS.gameView CC.viewRadius cameraPos) $
              CS.fromPos (cursorCoordSystem windowSize) $
                uncurry Float2 $
                  over both double2Float cursorPos
      in  makeControlsTransport buttonStates worldPos

-- Helper
cursorCoordSystem :: (Int, Int) -> CS.CoordSystem
cursorCoordSystem windowSize =
  CS.makeCoordSystem
    (l / 2)
    (V2 CS.PreserveAxis CS.FlipAxis)
    (Float2 (w / 2) (h / 2))
  where
    (w, h) = over both fromIntegral windowSize
    l = min w h

checkHolds :: GLEnv -> IO (DisplayStats, DisplayHelp)
checkHolds glEnv = do
  let isPressed = fmap keyIsPressed . GLFW.getKey (gle_window glEnv)
  tab <- liftIO $ isPressed GLFW.Key'Tab
  f1 <- liftIO $ isPressed GLFW.Key'F1
  f2 <- liftIO $ isPressed GLFW.Key'F2
  f3 <- liftIO $ isPressed GLFW.Key'F3
  f4 <- liftIO $ isPressed GLFW.Key'F4
  f5 <- liftIO $ isPressed GLFW.Key'F5
  let statsOverlay
        | tab = DisplayStats
        | otherwise = HideStats
      helpScreen
        | f1 = ShowHelpInstructions
        | f2 = ShowHelpVisuals
        | f3 = ShowHelpDetailedMechanics
        | f4 = ShowHelpPlayerClasses
        | f5 = ShowHelpThemes
        | otherwise = HideHelp
  pure (statsOverlay, helpScreen)

checkPlayerQuit :: GLEnv -> IO PlayerContinueStatus
checkPlayerQuit glEnv = do
  windowclose <- GLFW.windowShouldClose $ gle_window glEnv
  pure $
    if windowclose
      then PlayerQuit
      else PlayerContinue

-- Helper
keyIsPressed :: GLFW.KeyState -> Bool
keyIsPressed = (/=) GLFW.KeyState'Released

-- Helper
joystickMoveCommand :: Float -> Float -> MoveCommand
joystickMoveCommand x y =
  let radius = sqrt $ x * x + y * y
      angle = atan2 (-y) x -- in [-pi, pi]
      angle' = floor $ angle / pi * 8 :: Int -- in [-8, 8]
      nonNeutralMoveDir
        | angle' < -7 = MoveW
        | angle' < -5 = MoveSW
        | angle' < -3 = MoveS
        | angle' < -1 = MoveSE
        | angle' < 1 = MoveE
        | angle' < 3 = MoveNE
        | angle' < 5 = MoveN
        | angle' < 7 = MoveNW
        | otherwise = MoveW
  in  if radius < 0.2 then MoveNeutral else nonNeutralMoveDir
