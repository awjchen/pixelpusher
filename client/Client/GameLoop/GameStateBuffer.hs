{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Client.GameLoop.GameStateBuffer (
  GameStateBuffer,
  newGameStateBuffer,
  insertUpdate,
  FrameOutputs (..),
  getNextScene,
) where

import Control.Concurrent.MVar
import Control.Monad (when)
import Control.Monad.ST
import Data.Functor ((<&>))
import Data.IORef
import Data.Int (Int32)
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Maybe (mapMaybe)
import Data.STRef
import Data.Traversable (for)
import Data.Vector qualified as V
import GHC.IO (ioToST)
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Clock (getMonotonicTimeNSecInt)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.SmallList (fromSmallList)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls (fromControlsTransport)
import Pixelpusher.Game.PlayerID (PlayerID)
import Pixelpusher.Game.Protocol
import Pixelpusher.Game.State
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

-- | Accepts out-of-order server updates and returns in-sequence
-- (specifications of) rendering frames. This is done by temporarily assuming
-- that any delayed frames have the same player controls as the previous frame
-- and that they have no new game events. When delayed frames finally arrive,
-- we backtrack to the last non-predicted game state and reapply updates.
--
-- I intend for this type to be thread-safe: access requires an `MVar` lock and
-- the API does not expose mutable references.
data GameStateBuffer = GameStateBuffer
  { gsb_bufferRef :: IORef (GameStateBuffer' RealWorld)
  , gsb_writeLock :: MVar ()
  , gsb_readLock :: MVar ()
  , gsb_stateCopyLock :: MVar ()
  }

data GameStateBuffer' s
  = NoSession (STRef s (Map SyncTime GameStateUpdate)) -- updates
  | Session (Buffer s)

-- TODO: Define this type as an abstract type in a separate module.
data Buffer s = Buffer
  { buf_roomID :: RoomID
  , buf_playerID :: Maybe PlayerID
  , buf_trueState :: GameState s
  -- ^ The latest known "true" game state.
  , buf_updates :: STRef s (Map SyncTime GameStateUpdate)
  -- ^ A buffer of delta updates
  , buf_targetTime :: STRef s SyncTime
  -- ^ The number of the next frame to be rendered. Currently only used by
  -- `getNextScene`.
  , buf_stepResults :: STRef s (WIM.IntMap SyncTime InternalFrameOutputs)
  -- ^ Outputs from intergating the game state
  , buf_trueStateCopy :: GameState s
  -- ^ A copy of the true game state to allow prediction to proceed without
  -- acquiring the write lock
  , buf_predictedState :: GameState s
  -- ^ A cache for the latest predicted game state.
  , buf_predictionValid :: IORef PredictionStatus
  -- ^ The predicted state should be marked as invalid (out-of-date) whenever
  -- a delayed update finally arrives, so we know to recompute a more
  -- accurate prediction.
  }

type RoomID = Int

data InternalFrameOutputs = InternalFrameOutputs
  { ifo_gameScene :: GameScene
  , ifo_gameEvents :: GameEvents
  , ifo_isFramePredicted :: Bool
  , ifo_monotonicTimeNsec :: Maybe Int
  -- ^ The time at which the frame was computed. 'Nothing' for predicted
  -- frames.
  }

overGameEvents ::
  (GameEvents -> GameEvents) ->
  InternalFrameOutputs ->
  InternalFrameOutputs
overGameEvents f sr = sr{ifo_gameEvents = f (ifo_gameEvents sr)}

data PredictionStatus
  = PredictionInvalid
  | PredictionValid SyncTime

-- | Initialize the buffer.
newGameStateBuffer :: IO GameStateBuffer
newGameStateBuffer = do
  gsb_bufferRef <- stToIO (NoSession <$> newSTRef Map.empty) >>= newIORef
  gsb_writeLock <- newMVar ()
  gsb_readLock <- newMVar ()
  gsb_stateCopyLock <- newMVar ()
  pure GameStateBuffer{..}

newBuffer ::
  RoomID ->
  Maybe PlayerID ->
  GameStateSnapshot ->
  Map SyncTime GameStateUpdate ->
  IORef PredictionStatus ->
  ST s (Buffer s)
newBuffer roomID playerIdMaybe snapshot updates predictionStatus =
  Buffer
    <$> pure roomID
    <*> pure playerIdMaybe
    <*> thaw snapshot
    <*> newSTRef updates
    <*> newSTRef (getSnapshotTime snapshot)
    <*> newSTRef WIM.empty
    <*> (MWC.initialize V.empty >>= initializeGameStateWithGen defaultBaseGameParams)
    <*> (MWC.initialize V.empty >>= initializeGameStateWithGen defaultBaseGameParams)
    <*> pure predictionStatus

--------------------------------------------------------------------------------
-- Inserting updates

-- | Update the buffer with a server update.
insertUpdate :: GameStateBuffer -> ServerGameData -> IO ()
insertUpdate GameStateBuffer{..} serverGameData = do
  withMVar gsb_writeLock $ \() ->
    readIORef gsb_bufferRef >>= \case
      NoSession updatesRef ->
        case serverGameData of
          ServerSnapshot playerIdMaybe roomID gameStateSnapshot -> do
            buffer <-
              stToIO $ do
                predictionStatus <- ioToST $ newIORef PredictionInvalid
                updates <- readSTRef updatesRef
                buffer <-
                  newBuffer
                    roomID
                    playerIdMaybe
                    gameStateSnapshot
                    updates
                    predictionStatus
                integrateAndPrune buffer
                copy (buf_trueStateCopy buffer) (buf_trueState buffer)
                pure buffer
            -- Change buffer type
            atomicWriteIORef gsb_bufferRef (Session buffer)
          ServerDeltaUpdate deltaUpdate ->
            stToIO $
              modifySTRef' updatesRef $
                Map.insert (deltaUpdate_time deltaUpdate) deltaUpdate
      Session buffer -> do
        putUpdate buffer serverGameData
        stToIO $ integrateAndPrune buffer
        withMVar gsb_stateCopyLock $ \() ->
          stToIO $
            -- TODO: only perform copy if the true state was udpated
            copy (buf_trueStateCopy buffer) (buf_trueState buffer)

integrateAndPrune :: Buffer RealWorld -> ST RealWorld ()
integrateAndPrune buffer = do
  updates <- readSTRef (buf_updates buffer)
  tryIntegrate
    (buf_playerID buffer)
    updates
    (buf_trueState buffer)
    (buf_stepResults buffer)

  -- Remove used updates
  trueTime <- getGameSyncTime (buf_trueState buffer)
  modifySTRef' (buf_updates buffer) $ snd . Map.split trueTime

  -- Remove old game scenes
  targetTime <- readSTRef (buf_targetTime buffer)
  modifySTRef' (buf_stepResults buffer) $
    let historySize = C.tickRate_hz
    in  snd . WIM.split (addSyncTicks (Ticks (-historySize)) targetTime)

-- | Places the content of the server update into the buffer. Invalidates the
-- predicted state when a delayed update is received.
putUpdate :: Buffer RealWorld -> ServerGameData -> IO ()
putUpdate buffer = \case
  ServerSnapshot _playerID _roomID gameStateSnapshot -> do
    -- Overwrite the latest true state with the snapshot if the snapshot is
    -- more recent.
    let snapshotTime = getSnapshotTime gameStateSnapshot
    bufferTime <- stToIO $ getGameSyncTime (buf_trueState buffer)
    when (snapshotTime > bufferTime) $ do
      stToIO $
        thaw gameStateSnapshot >>= copy (buf_trueState buffer)
      atomicWriteIORef (buf_predictionValid buffer) PredictionInvalid
  ServerDeltaUpdate deltaUpdate -> do
    stToIO $
      modifySTRef' (buf_updates buffer) $
        Map.insert (deltaUpdate_time deltaUpdate) deltaUpdate
    -- Invalidate the prediction cache.
    atomicModifyIORef' (buf_predictionValid buffer) . fmap (,()) $
      \case
        PredictionInvalid -> PredictionInvalid
        PredictionValid predictionTime ->
          if deltaUpdate_time deltaUpdate <= predictionTime
            then PredictionInvalid
            else PredictionValid predictionTime

-- | Step the game state as far as possible using the provided updates, and
-- save the frame outputs for each step taken.
tryIntegrate ::
  Maybe PlayerID ->
  Map SyncTime GameStateUpdate ->
  GameState RealWorld ->
  STRef RealWorld (WIM.IntMap SyncTime InternalFrameOutputs) ->
  ST RealWorld ()
tryIntegrate playerIdMaybe deltaUpdates gameState gameScenesRef = do
  nextTime' <- nextSyncTick <$> getGameSyncTime gameState
  case Map.lookup nextTime' deltaUpdates of
    Nothing -> pure ()
    Just deltaUpdate -> do
      let ctrls =
            WIM.map fromControlsTransport $
              getPlayerIDMap32 $
                deltaUpdate_playerControls deltaUpdate
          cmds = fromSmallList $ deltaUpdate_gameCommands deltaUpdate
          debugCmd = deltaUpdate_debugCommand deltaUpdate
      gameEvents <- integrateGameState ctrls cmds debugCmd gameState

      gameScene <- getGameScene gameState (PlayerActor <$> playerIdMaybe)
      time <- getGameSyncTime gameState
      monotonicTime <- ioToST getMonotonicTimeNSecInt
      modifySTRef' gameScenesRef $
        WIM.insert time $
          InternalFrameOutputs gameScene gameEvents False (Just monotonicTime)
      tryIntegrate playerIdMaybe deltaUpdates gameState gameScenesRef

--------------------------------------------------------------------------------
-- Reading scenes

-- | Get the rendering specification for the next frame. Also indicates whether
-- too many frame requests are being made relative to the rate at which server
-- updates are provided.
getNextScene :: GameStateBuffer -> Ticks -> IO (Maybe FrameOutputs, Int32)
getNextScene GameStateBuffer{..} integrationSteps =
  withMVar gsb_readLock $ \() ->
    readIORef gsb_bufferRef >>= \case
      NoSession{} ->
        pure (Nothing, 0)
      Session buffer ->
        getNextScene' gsb_stateCopyLock buffer integrationSteps

data FrameOutputs = FrameOutputs
  { fo_gameScene :: GameScene
  , fo_gameEvents :: GameEvents
  , fo_roomID :: RoomID
  , fo_playerID :: Maybe PlayerID
  , fo_isFramePredicted :: Bool
  , fo_monotonicTimeNSec :: Maybe Int
  -- ^ The time at which the frame was computed. 'Nothing' for predicted
  -- frames.
  }

getNextScene' ::
  MVar () -> Buffer RealWorld -> Ticks -> IO (Maybe FrameOutputs, Int32)
getNextScene' stateCopyLock buffer integrationSteps = do
  startTime <- stToIO $ readSTRef (buf_targetTime buffer)
  let !targetTime = addSyncTicks integrationSteps startTime
  stToIO $ writeSTRef (buf_targetTime buffer) targetTime

  trueStateTime <- stToIO $ getGameSyncTime $ buf_trueState buffer
  deltaUpdates <- stToIO $ readSTRef (buf_updates buffer)
  let integrationTimeLimit = addSyncTicks integrationRange trueStateTime
        where
          integrationRange = Ticks ((3 * C.tickRate_hz) `div` 4)
  gameScenes <- stToIO $ readSTRef $ buf_stepResults buffer

  mFrameOutput <-
    if
      | targetTime <= trueStateTime -> do
          -- We have the next frame: return it

          -- But first invalidate the prediction cache
          atomicWriteIORef (buf_predictionValid buffer) PredictionInvalid
          pure $
            let initGameEvents =
                  mconcat $
                    map ifo_gameEvents $
                      mapMaybe (`WIM.lookup` gameScenes) $
                        syncTimeRange
                          (nextSyncTick startTime)
                          (prevSyncTick targetTime)
            in  overGameEvents (initGameEvents <>)
                  <$> WIM.lookup targetTime gameScenes
      | targetTime <= integrationTimeLimit -> do
          -- We don't have the next frame, and we're not too far ahead:
          -- predict and return it

          let predictedState = buf_predictedState buffer

          doCopy <-
            atomicModifyIORef' (buf_predictionValid buffer) $
              \case
                PredictionInvalid ->
                  (PredictionValid targetTime, True)
                PredictionValid time ->
                  (PredictionValid time, False)

          when doCopy $
            withMVar stateCopyLock $ \() ->
              stToIO $
                copy predictedState (buf_trueStateCopy buffer)

          lastPredictedTime <- stToIO $ getGameSyncTime predictedState
          integrateGameEvents <-
            let predictionSteps = targetTime `diffSyncTime` lastPredictedTime
             in stToIO $
                  integrateWithPrediction
                    startTime
                    predictionSteps
                    deltaUpdates
                    predictedState
          gameScene <-
            stToIO $
              getGameScene predictedState $
                PlayerActor <$> buf_playerID buffer
          pure $
            let initGameEvents =
                  mconcat $
                    map ifo_gameEvents $
                      mapMaybe (`WIM.lookup` gameScenes) $
                        syncTimeRange
                          (nextSyncTick startTime)
                          lastPredictedTime
                gameEvents = initGameEvents <> integrateGameEvents
            in  Just $
                  InternalFrameOutputs gameScene gameEvents True Nothing
      | otherwise ->
          -- We don't have the next frame, and we're way ahead of the server:
          -- do nothing and return nothing
          pure Nothing

  pure $
    let highestObservedTime =
          maybe trueStateTime (max trueStateTime . fst) $
            Map.lookupMax deltaUpdates
        ticksAheadOfServer =
          getTicks $ targetTime `diffSyncTime` highestObservedTime
        mOutputs =
          mFrameOutput <&> \frameOutput ->
            FrameOutputs
              { fo_gameScene = ifo_gameScene frameOutput
              , fo_gameEvents = ifo_gameEvents frameOutput
              , fo_roomID = fromIntegral $ buf_roomID buffer
              , fo_playerID = buf_playerID buffer
              , fo_isFramePredicted = ifo_isFramePredicted frameOutput
              , fo_monotonicTimeNSec = ifo_monotonicTimeNsec frameOutput
              }
    in  (mOutputs, ticksAheadOfServer)

-- | Advance the game state the specified number of steps under the assumption
-- that missing updates are empty.
integrateWithPrediction ::
  SyncTime -> -- Only collect game events that occur after this time
  Ticks ->
  Map SyncTime GameStateUpdate ->
  GameState s ->
  ST s GameEvents
integrateWithPrediction startTime (Ticks steps) deltaUpdates gameState = do
  currentTick <- getGameSyncTime gameState
  let integrationStepTimes =
        take (fromIntegral steps) $
          iterate nextSyncTick (nextSyncTick currentTick)
  fmap mconcat $
    for integrationStepTimes $ \time -> do
      let (ctrls, cmds, debugCmd) = case Map.lookup time deltaUpdates of
            Nothing -> (mempty, [], Nothing)
            Just deltaUpdate ->
              ( WIM.map fromControlsTransport $
                  getPlayerIDMap32 $
                    deltaUpdate_playerControls deltaUpdate
              , fromSmallList $ deltaUpdate_gameCommands deltaUpdate
              , deltaUpdate_debugCommand deltaUpdate
              )
      gameEvents <- integrateGameState ctrls cmds debugCmd gameState
      pure $ if time > startTime then gameEvents else mempty
