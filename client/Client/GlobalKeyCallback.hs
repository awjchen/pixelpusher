{-# LANGUAGE MultiWayIf #-}

module Client.GlobalKeyCallback (
  globalKeyCallback,
) where

import Graphics.UI.GLFW qualified as GLFW

import Client.ThemeEnv
import Client.Visuals.GLFW.KeyCallback

globalKeyCallback :: ThemeEnv -> KeyCallback
globalKeyCallback themeEnv =
  KeyCallback $ \_window key _scanCode keySt mods ->
    let firstPress = keySt == GLFW.KeyState'Pressed
    in  if
          | key == GLFW.Key'R
              && firstPress
              && GLFW.modifierKeysControl mods ->
              Just $ reloadCustomTheme themeEnv
          | otherwise ->
              Nothing
