module Client.Custom.TSignal (
  -- * TSignal
  TSignal,
  newTSignal,
  readTSignal,
  writeTSignal,
  modifyTSignal,

  -- * TSignalReceiver
  TSignalReceiver,
  newTSignalReceiver,
  readTSignalReceiver,
  tryReadTSignalReceiver,
) where

import Control.Concurrent.STM
import Data.Functor ((<&>))
import Data.Strict.Tuple (Pair ((:!:)))

-- | A TVar with a version that increments on writes.
newtype TSignal a = TSignal (TVar (Pair Version a))

type Version = Int -- 'Int' should be sufficient for this game.

-- | Create a new 'TSignal' holding the supplied value.
newTSignal :: a -> STM (TSignal a)
newTSignal val = TSignal <$> newTVar (0 :!: val)

readTSignal :: TSignal a -> STM a
readTSignal (TSignal var) =
  readTVar var <&> \(_version :!: val) -> val

-- | Write the supplied value into to 'TSignal'.
writeTSignal :: (Eq a) => TSignal a -> a -> STM ()
writeTSignal (TSignal var) newVal =
  modifyTVar' var $ \(version :!: oldVal) ->
    if oldVal == newVal
      then version :!: oldVal
      else succ version :!: newVal

-- | Modify the value of a 'TSignal'.
modifyTSignal :: (Eq a) => TSignal a -> (a -> a) -> STM ()
modifyTSignal (TSignal var) f =
  modifyTVar' var $ \(version :!: oldVal) ->
    let newVal = f oldVal
    in  if oldVal == newVal
          then version :!: oldVal
          else succ version :!: newVal

-- | A reference to a 'TSignal' that listens for changes to its value.
data TSignalReceiver a = TSignalReceiver (TSignal a) (TVar Version)

-- | Create a new 'TSignalReceiver' for a 'TSignal'.
newTSignalReceiver :: TSignal a -> STM (a, TSignalReceiver a)
newTSignalReceiver (TSignal signalVar) = do
  (version :!: value) <- readTVar signalVar
  versionVar <- newTVar version
  pure (value, TSignalReceiver (TSignal signalVar) versionVar)

-- | Block until a new value can be read from the underlying 'TSignal'.
readTSignalReceiver :: TSignalReceiver a -> STM a
readTSignalReceiver (TSignalReceiver (TSignal signalVar) versionVar) = do
  (version :!: val) <- readTVar signalVar
  lastReadVersion <- readTVar versionVar
  if version > lastReadVersion
    then writeTVar versionVar version >> pure val
    else retry

-- | A version of 'readTSignalReceiver' which does not retry. Instead, it
-- returns nothing if no new value is available.
tryReadTSignalReceiver :: TSignalReceiver a -> STM (Maybe a)
tryReadTSignalReceiver (TSignalReceiver (TSignal signalVar) versionVar) = do
  (version :!: val) <- readTVar signalVar
  lastReadVersion <- readTVar versionVar
  if version > lastReadVersion
    then writeTVar versionVar version >> pure (Just val)
    else pure Nothing
