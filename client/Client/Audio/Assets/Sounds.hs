{-# LANGUAGE TemplateHaskell #-}

module Client.Audio.Assets.Sounds where

import Data.ByteString qualified as BS
import Data.FileEmbed (embedFile)
import Data.Vector.NonEmpty (NonEmptyVector)
import Data.Vector.NonEmpty qualified as VNE

bonkLowOpus :: NonEmptyVector BS.ByteString
bonkLowOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/bonk_low_clear_0.opus")
    , $(embedFile "sounds/bonk_low_clear_1.opus")
    , $(embedFile "sounds/bonk_low_clear_2.opus")
    , $(embedFile "sounds/bonk_low_clear_3.opus")
    ]

bonkLowMuffledOpus :: NonEmptyVector BS.ByteString
bonkLowMuffledOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/bonk_low_muffled_0.opus")
    , $(embedFile "sounds/bonk_low_muffled_1.opus")
    , $(embedFile "sounds/bonk_low_muffled_2.opus")
    , $(embedFile "sounds/bonk_low_muffled_3.opus")
    ]

bonkHighOpus :: NonEmptyVector BS.ByteString
bonkHighOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/bonk_high_clear_0.opus")
    , $(embedFile "sounds/bonk_high_clear_1.opus")
    , $(embedFile "sounds/bonk_high_clear_2.opus")
    , $(embedFile "sounds/bonk_high_clear_3.opus")
    ]

bonkHighMuffledOpus :: NonEmptyVector BS.ByteString
bonkHighMuffledOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/bonk_high_muffled_0.opus")
    , $(embedFile "sounds/bonk_high_muffled_1.opus")
    , $(embedFile "sounds/bonk_high_muffled_2.opus")
    , $(embedFile "sounds/bonk_high_muffled_3.opus")
    ]

smallWhooshLowOpus :: NonEmptyVector BS.ByteString
smallWhooshLowOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/small_whoosh_low_0.opus")
    , $(embedFile "sounds/small_whoosh_low_1.opus")
    , $(embedFile "sounds/small_whoosh_low_2.opus")
    , $(embedFile "sounds/small_whoosh_low_3.opus")
    ]

smallWhooshHighOpus :: NonEmptyVector BS.ByteString
smallWhooshHighOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/small_whoosh_high_0.opus")
    , $(embedFile "sounds/small_whoosh_high_1.opus")
    , $(embedFile "sounds/small_whoosh_high_2.opus")
    , $(embedFile "sounds/small_whoosh_high_3.opus")
    ]

staticShockOpus :: NonEmptyVector BS.ByteString
staticShockOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/static_shock_0.opus")
    , $(embedFile "sounds/static_shock_1.opus")
    , $(embedFile "sounds/static_shock_2.opus")
    , $(embedFile "sounds/static_shock_3.opus")
    ]

distantCrackOpus :: NonEmptyVector BS.ByteString
distantCrackOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/distant_crack_0.opus")
    , $(embedFile "sounds/distant_crack_1.opus")
    , $(embedFile "sounds/distant_crack_2.opus")
    , $(embedFile "sounds/distant_crack_3.opus")
    ]

overseerExplodeOpus :: NonEmptyVector BS.ByteString
overseerExplodeOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/overseer_explode_0.opus")
    , $(embedFile "sounds/overseer_explode_1.opus")
    ]

flagExplodeOpus :: NonEmptyVector BS.ByteString
flagExplodeOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/flag_explode_0.opus")
    , $(embedFile "sounds/flag_explode_1.opus")
    ]

notifyFlagScorePositive :: BS.ByteString
notifyFlagScorePositive =
  $(embedFile "sounds/notify_flag_score_positive.opus")

notifyFlagScoreNegative :: BS.ByteString
notifyFlagScoreNegative =
  $(embedFile "sounds/notify_flag_score_negative.opus")

notifyFlagStealPositive :: BS.ByteString
notifyFlagStealPositive =
  $(embedFile "sounds/notify_flag_steal_positive.opus")

notifyFlagStealNegative :: BS.ByteString
notifyFlagStealNegative =
  $(embedFile "sounds/notify_flag_steal_negative.opus")

notifyFlagRecoverPositive :: BS.ByteString
notifyFlagRecoverPositive =
  $(embedFile "sounds/notify_flag_recover_positive.opus")

notifyFlagRecoverNegative :: BS.ByteString
notifyFlagRecoverNegative =
  $(embedFile "sounds/notify_flag_recover_negative.opus")

softClickOpus :: NonEmptyVector BS.ByteString
softClickOpus =
  VNE.unsafeFromList
    [ $(embedFile "sounds/soft_click_0.opus")
    , $(embedFile "sounds/soft_click_1.opus")
    , $(embedFile "sounds/soft_click_2.opus")
    , $(embedFile "sounds/soft_click_3.opus")
    ]

flagPickup :: NonEmptyVector BS.ByteString
flagPickup =
  VNE.unsafeFromList
    [ $(embedFile "sounds/flag_pickup_0.opus")
    , $(embedFile "sounds/flag_pickup_1.opus")
    ]

flagDrop :: NonEmptyVector BS.ByteString
flagDrop =
  VNE.unsafeFromList
    [ $(embedFile "sounds/flag_drop_0.opus")
    , $(embedFile "sounds/flag_drop_1.opus")
    ]

pixelpusherIntroOpus :: BS.ByteString
pixelpusherIntroOpus =
  $(embedFile "music/pixel_pusher_intro.opus")
