{-# LANGUAGE RecordWildCards #-}

-- | This is the top-level module for the game's audio.
module Client.Audio.Game (
  NotificationSfxPlayback (..),
  runGameAudio,
) where

import Control.Arrow ((&&&))
import Control.Monad (unless, when)
import Data.Foldable (find, foldl', for_)
import Data.List (partition, sortOn)
import Data.Ord (Down (Down))
import Foreign.C.Types (CFloat)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2, toFloats)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CastEvents (
  DashCastEvent (..),
  DashType (..),
  PsiStormCastEvent (..),
 )
import Pixelpusher.Game.CollisionEvents (
  CollisionParticipants (..),
  NeutralCollision (..),
 )
import Pixelpusher.Game.CombatEvents (
  CollisionDamage (..),
  CollisionDamageParticipants (..),
  PsiStormEffect (..),
 )
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents (
  FlagCapture (..),
  FlagPickup (..),
  FlagRecovery (..),
  FlagType (BaseFlagType),
 )
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player (Player, player_team)
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.State
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Audio.Env
import Client.Constants qualified as CC

--------------------------------------------------------------------------------

data NotificationSfxPlayback
  = PlayNotificationSfx
  | DoNotPlayNotificationSfx

runGameAudio ::
  ALEnv ->
  Bool ->
  NotificationSfxPlayback ->
  GameEvents ->
  GameScene ->
  Float2 ->
  Maybe ActorID ->
  IO ()
runGameAudio alEnv enableSpatialAudio notificationSfx fo_gameEvents gameScene cameraPos mActorID = do
  let mPlayerStuff =
        flip WIM.lookup (scene_players gameScene) =<< mActorID
      mPlayerView = snd <$> mPlayerStuff

  do
    let audioParams = gp_audio $ scene_gameParams gameScene
        (listenPos, listenRadius) =
          case mPlayerView of
            Nothing -> (cameraPos, CC.viewRadius)
            Just playerView ->
              case pv_overseerView playerView of
                OV_Dead OverseerViewDead{ovd_deathTime, ovd_lastPos} ->
                  let ticksSinceDeath =
                        fromIntegral $
                          scene_time gameScene `diffTime` ovd_deathTime
                      timeConstantTicks = fromIntegral C.tickRate_hz
                      radius =
                        CC.viewRadius
                          * exp (-ticksSinceDeath / timeConstantTicks)
                  in  (toFloats ovd_lastPos, radius)
                OV_Alive OverseerViewAlive{ova_pos} ->
                  (toFloats ova_pos, CC.viewRadius)
        listenTeam = pv_team <$> mPlayerView
    playSfx
      alEnv
      audioParams
      enableSpatialAudio
      notificationSfx
      listenRadius
      listenPos
      listenTeam
      mActorID
      (scene_gamePhase gameScene)
      fo_gameEvents

  adjustMusicGains alEnv gameScene mPlayerStuff

--------------------------------------------------------------------------------

playSfx ::
  ALEnv ->
  AudioParams ->
  Bool ->
  NotificationSfxPlayback ->
  Float ->
  Float2 ->
  Maybe Team ->
  Maybe ActorID ->
  GamePhase ->
  GameEvents ->
  IO ()
playSfx
  alEnv
  params
  enableSpatialAudio
  notificationSfx
  listenRadius
  listenPos
  listenTeam
  listenActor
  gamePhase
  gameEvents = do
    let withGainPos ::
          (Applicative f) =>
          GainPos ->
          (CFloat -> (CFloat, CFloat) -> f ()) ->
          f ()
        withGainPos GainPos{_gain, _relPos = Float2 x y} f =
          when (_gain > 0) $
            let relPos' =
                  if enableSpatialAudio
                    then (realToFrac x, realToFrac y)
                    else (0, 0)
            in  f (realToFrac _gain) relPos'
        averageGainPosition_ :: Int -> [Fixed2] -> GainPos
        averageGainPosition_ limit positions =
          averageGainPosition limit $ map (1,) positions
        averageGainPosition :: Int -> [(Float, Fixed2)] -> GainPos
        averageGainPosition limit gainPositions
          | null gainPositions = zeroGainPos
          | otherwise =
              let gainRelPos =
                    flip map gainPositions $ \(gain, pos) ->
                      let relPos = Fixed.toFloats pos - listenPos
                          distGain =
                            distanceGain listenRadius params (Float.norm2 relPos)
                      in  (gain * distGain, relPos)
                  (!sumSqGains, !sumRelPos) =
                    foldl' f (0, 0) $
                      if length gainRelPos <= limit
                        then gainRelPos
                        else take limit $ sortOn (Down . fst) gainRelPos
                    where
                      f (!sumSqGains', !sumRelPos') (gain, relPos) =
                        let gain2 = gain * gain
                        in  ( sumSqGains' + gain2
                            , sumRelPos' + Float.map2 (* gain2) relPos
                            )
              in  if sumSqGains <= 0
                    then zeroGainPos
                    else
                      GainPos
                        (sqrt sumSqGains)
                        (Float.map2 (/ sumSqGains) sumRelPos)

    -- Collision bonks
    let (overseerBonks, droneBonks) =
          partitionCollisions $
            map fromCollisionDamage (ge_collisions gameEvents)
    withGainPos
      (averageGainPosition 4 overseerBonks)
      (playBonk alEnv BonkLow)
    withGainPos
      (averageGainPosition 4 droneBonks)
      (playBonk alEnv BonkHigh)

    -- Neutral collision bonks
    let (overseerMuffledBonks, droneMuffledBonks) =
          partitionCollisions $
            map fromNeutralCollision (ge_neutralCollisions gameEvents)
    withGainPos
      (averageGainPosition 4 overseerMuffledBonks)
      (playBonkMuffled alEnv BonkLow)
    withGainPos
      (averageGainPosition 4 droneMuffledBonks)
      (playBonkMuffled alEnv BonkHigh)

    -- Overseer deaths
    withGainPos
      (averageGainPosition_ 4 (map ode_pos (ge_overseerDeaths gameEvents)))
      (playOverseerExplosion alEnv)

    -- Flag captures: explosion
    withGainPos
      (averageGainPosition_ 4 (map flagCapture_position (ge_flagCaptures gameEvents)))
      (playFlagExplosion alEnv)

    case notificationSfx of
      DoNotPlayNotificationSfx -> pure ()
      PlayNotificationSfx -> do
        -- Flag captures: chord
        -- Note: Not restricted to in-game game phase because it currently prevents
        -- playing of the game-ending flag capture
        let (alliedFlagCaptures, enemyFlagCaptures) =
              partition (\team -> maybe True (== team) listenTeam) $
                map flagCapture_team $
                  ge_flagCaptures gameEvents
        unless (null alliedFlagCaptures) $
          playFlagNotification alEnv NotifyFlagScorePositive 1 (0, 0)
        unless (null enemyFlagCaptures) $
          playFlagNotification alEnv NotifyFlagScoreNegative 1 (0, 0)

        when (inGame gamePhase) $ do
          -- Flag pickups: arpeggio
          do
            let (alliedFlagPickups, enemyFlagPickups) =
                  partition (\team -> maybe True (== team) listenTeam) $
                    map flagPickup_carrierTeam $
                      filter ((== BaseFlagType) . flagPickup_type) $
                        ge_flagPickups gameEvents
            unless (null alliedFlagPickups) $
              playFlagNotification alEnv NotifyFlagStealPositive 1 (0, 0)
            unless (null enemyFlagPickups) $
              playFlagNotification alEnv NotifyFlagStealNegative 1 (0, 0)

          -- Flag recoveries: downward arpeggio
          do
            let (alliedFlagRecoveries, enemyFlagRecoveries) =
                  partition (\team -> maybe True (== team) listenTeam) $
                    map flagRecovery_team $
                      ge_flagRecoveries gameEvents
            unless (null alliedFlagRecoveries) $
              playFlagNotification alEnv NotifyFlagRecoverPositive 1 (0, 0)
            unless (null enemyFlagRecoveries) $
              playFlagNotification alEnv NotifyFlagRecoverNegative 1 (0, 0)

    -- Dashes
    do
      let (droneDashes, overseerDashes) =
            partition
              ((\case DroneDash -> True; OverseerDash -> False) . dce_dashType)
              (ge_dashes gameEvents)
          dashGain = Fixed.toFloat . dce_effectsMagnitudeFrac
      withGainPos
        ( averageGainPosition
            16
            (map (dashGain &&& dce_pos) droneDashes)
        )
        (playWhoosh alEnv WhooshHigh)
      withGainPos
        ( averageGainPosition
            3
            (map (dashGain &&& dce_pos) overseerDashes)
        )
        (playWhoosh alEnv WhooshLow)

    -- Psi-storm casts
    withGainPos
      (averageGainPosition_ 4 (map psce_pos (ge_psiStormCasts gameEvents)))
      (playDistantCrack alEnv)

    -- Psi-storm damage
    withGainPos
      ( averageGainPosition_
          16
          (map pse_entityPos (ge_psiStormEffects gameEvents))
      )
      (playStaticShock alEnv)

    -- Shield powerup pickup
    for_ listenActor $ \actorID ->
      for_ (find ((== actorID) . spe_overseerOwner) (ge_shieldPickups gameEvents)) $ \_ ->
        playSoftClick alEnv 1.0

    -- Flag pickup
    for_ listenActor $ \actorID ->
      for_ (find ((== actorID) . flagPickup_actor) (ge_flagPickups gameEvents)) $ \_ ->
        playFlagPickup alEnv 1.0

    for_ listenActor $ \actorID ->
      for_ (find ((== actorID) . fde_actor) (ge_flagDrops gameEvents)) $ \_ ->
        playFlagDrop alEnv 1.0

    -- Match end sfx
    when (PlayMatchEndSfx `elem` ge_gamePhaseTransitions gameEvents) $
      -- Naming: It was originally an intro but we're using it for match end
      playPixelpusherIntro alEnv

data AudibleCollision = AudibleCollision
  { ac_participants :: CollisionParticipants
  , ac_position :: Fixed2
  , ac_damage :: Fixed
  }

fromCollisionDamage :: CollisionDamage -> AudibleCollision
fromCollisionDamage CollisionDamage{..} =
  AudibleCollision
    { ac_participants = forgetCollisionDamage cd_participants
    , ac_position = cd_position
    , ac_damage =
        let isOverseerInvolved =
              case cd_participants of
                CollisionDamageOverseers{} -> True
                CollisionDamageDrones{} -> False
                CollisionDamageOverseerDrone{} -> True
            minDamage =
              if cd_isKillingBlow && isOverseerInvolved
                then 5 -- warning: constant without reference
                else 0
        in  max minDamage cd_damage
    }

forgetCollisionDamage :: CollisionDamageParticipants -> CollisionParticipants
forgetCollisionDamage = \case
  CollisionDamageOverseers{} -> CollisionOverseers
  CollisionDamageDrones{} -> CollisionDrones
  CollisionDamageOverseerDrone{} -> CollisionOverseerDrone

fromNeutralCollision :: NeutralCollision -> AudibleCollision
fromNeutralCollision NeutralCollision{..} =
  AudibleCollision
    { ac_participants = nce_participants
    , ac_position = nce_position
    , ac_damage = dampedDamage
    }
  where
    dampedDamage = max 0 $ nce_damageEquivalent - minDmg -- Ignore small collisions
    minDmg = 0.1

partitionCollisions ::
  [AudibleCollision] ->
  ([(Float, Fixed2)], [(Float, Fixed2)])
partitionCollisions = foldr f ([], [])
  where
    f audibleCollision (overseers, drones) =
      case ac_participants audibleCollision of
        CollisionOverseers ->
          ( (gain, pos) : overseers
          , drones
          )
        CollisionOverseerDrone ->
          ( (gain * invSqrt2, pos) : overseers
          , (gain * invSqrt2, pos) : drones
          )
        CollisionDrones ->
          ( overseers
          , (gain, pos) : drones
          )
      where
        gain = collisionDamageGain audibleCollision
        pos = ac_position audibleCollision
        invSqrt2 = 1 / sqrt 2

collisionDamageGain :: AudibleCollision -> Float
collisionDamageGain audibleCollision =
  let dmg = Fixed.toFloat $ ac_damage audibleCollision
      dmgMax = 64
  in  sqrt $ dmg / dmgMax

distanceGain :: Float -> AudioParams -> Float -> Float
distanceGain listenRadius params dist =
  1 - (min maxDist (max minDist dist) - minDist) / (maxDist - minDist)
  where
    minDist =
      listenRadius
        * Fixed.toFloat (gp_audioFalloffStart_viewRadiusFraction params)
    maxDist =
      listenRadius
        * Fixed.toFloat (gp_audioFalloffEnd_viewRadiusFraction params)

data GainPos = GainPos
  { _gain :: Float
  , _relPos :: Float2
  }

zeroGainPos :: GainPos
zeroGainPos = GainPos{_gain = 0, _relPos = 0}

--------------------------------------------------------------------------------

adjustMusicGains ::
  ALEnv ->
  GameScene ->
  Maybe (Player, PlayerView) ->
  IO ()
adjustMusicGains alEnv gameScene = \case
  Nothing ->
    setTrackGains
      alEnv
      TrackGains
        { tg_slow = 0
        , tg_fast = 1
        , tg_slowFast = 1
        , tg_haveEnemyFlag = 0
        , tg_alliedFlagStolen = 0
        , tg_chantPanning = 0
        }
      Nothing
  Just (player, playerView) ->
    case scene_gamePhase gameScene of
      GamePhase_Intermission{} ->
        let viewTeam = view player_team player
            trackGains =
              TrackGains
                { tg_slow = 0
                , tg_fast = 0
                , tg_slowFast = 0
                , tg_haveEnemyFlag = 0
                , tg_alliedFlagStolen = 0
                , tg_chantPanning = teamChantPanning viewTeam
                }
        in  -- hack: taking into account adaptive music decay factor and bgm bpm
            setTrackGains alEnv trackGains (Just 12)
      GamePhase_Game{} ->
        let renderingComps = scene_renderingComps gameScene
            viewTeam = view player_team player
            isInView' = case pv_overseerView playerView of
              OV_Dead{} -> const False
              OV_Alive OverseerViewAlive{ova_pos} ->
                isInView CC.viewRadius (toFloats ova_pos)
            numEnemyOverseersInView =
              length $
                flip filter (rcs_overseers renderingComps) $ \rc ->
                  viewTeam /= or_team (rendering_entityRendering rc)
                    && isInView' rc
            numEnemyDronesInView =
              length $
                flip filter (rcs_drones renderingComps) $ \rc ->
                  viewTeam /= dr_team (rendering_entityRendering rc)
                    && isInView' rc
            fastGain =
              min 1 $
                fromIntegral numEnemyOverseersInView
                  + 0.125 * fromIntegral numEnemyDronesInView
            alliedBaseFlags =
              length $
                flip filter (scene_flags gameScene) $
                  \(team, _, isBaseFlag) ->
                    team == viewTeam && isBaseFlag == BaseFlagType
            alliedFlagStolenGain =
              if gp_enableTutorialMode (scene_gameParams gameScene)
                then 0
                else fromIntegral (1 - alliedBaseFlags)
            enemyBaseFlags =
              length $
                flip filter (scene_flags gameScene) $
                  \(team, _, isBaseFlag) ->
                    team /= viewTeam && isBaseFlag == BaseFlagType
            haveEnemyFlagGain =
              if gp_enableTutorialMode (scene_gameParams gameScene)
                then 0
                else fromIntegral (1 - enemyBaseFlags)
            trackGains =
              TrackGains
                { tg_slow = 1 - fastGain
                , tg_fast = fastGain
                , tg_slowFast = 1
                , tg_haveEnemyFlag = haveEnemyFlagGain
                , tg_alliedFlagStolen = alliedFlagStolenGain
                , tg_chantPanning = teamChantPanning viewTeam
                }
        in  setTrackGains alEnv trackGains Nothing

teamChantPanning :: Team -> CFloat
teamChantPanning TeamW = 1
teamChantPanning TeamE = -1
