{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Audio.Env (
  ALEnv,
  InitialAudioOptions (..),
  withALEnv,
  getVolume,
  toggleMute,
  setVolumePercent,
  increaseVolume,
  decreaseVolume,
  TrackGains (..),
  setTrackGains,
  setMusicGainPercent,
  toggleMusicMute,
  getEffectsGainPercent,
  setEffectsGainPercent,
  Muted (..),
  getMusicMute,
  enableVocoderTracks,
  disableVocoderTracks,
  AudioLevels (..),
  getAudioLevels,
  BonkPitch (..),
  playBonk,
  playBonkMuffled,
  playStaticShock,
  WhooshPitch (..),
  playWhoosh,
  playDistantCrack,
  playOverseerExplosion,
  playFlagExplosion,
  FlagNotification (..),
  playFlagNotification,
  playSoftClick,
  playFlagPickup,
  playFlagDrop,
  playPixelpusherIntro,
) where

import Data.IORef (IORef, atomicModifyIORef', newIORef, readIORef)
import Data.Vector.NonEmpty (NonEmptyVector)
import Data.Vector.NonEmpty qualified as VNE
import Foreign qualified
import Foreign.C.Types (CFloat)
import Sound.OpenAL.FFI.AL qualified as AL
import Sound.OpenAL.FFI.ALC qualified as ALC
import System.Log.FastLogger
import System.Random.MWC qualified as MWC

import Client.Audio.AL.SourceRing (PlayOptions (..), SourceRing)
import Client.Audio.AL.SourceRing qualified as SourceRing
import Client.Audio.AL.Wrapped (sourceOpusFile)
import Client.Audio.Assets.Sounds qualified as Sounds
import Client.Audio.Music (MusicHandle, Muted (..), TrackGains)
import Client.Audio.Music qualified as Music
import Client.Audio.Volume (Volume)
import Client.Audio.Volume qualified as Volume
import Client.Logging

-------------------------------------------------------------------------------

data ALEnv = ALEnv
  { ale_gen :: MWC.GenIO
  , ale_volume :: IORef Volume.Volume
  , ale_effectsVolume :: IORef Volume.Volume
  , ale_music :: MusicHandle
  , ale_generalSourceRing :: SourceRing
  , ale_bonkSourceRing :: SourceRing
  , ale_stingerSourceRing :: SourceRing
  , ale_muffledBonkSourceRing :: SourceRing
  , ale_bonkLowClearBuffers :: NonEmptyVector AL.Buffer
  , ale_bonkLowMuffledBuffers :: NonEmptyVector AL.Buffer
  , ale_bonkHighClearBuffers :: NonEmptyVector AL.Buffer
  , ale_bonkHighMuffledBuffers :: NonEmptyVector AL.Buffer
  , ale_smallWhooshLowBuffers :: NonEmptyVector AL.Buffer
  , ale_smallWhooshHighBuffers :: NonEmptyVector AL.Buffer
  , ale_staticShockBuffers :: NonEmptyVector AL.Buffer
  , ale_distantCrackBuffer :: NonEmptyVector AL.Buffer
  , ale_overseerExplodeBuffer :: NonEmptyVector AL.Buffer
  , ale_flagExplodeBuffer :: NonEmptyVector AL.Buffer
  , ale_notifyFlagScorePositive :: AL.Buffer
  , ale_notifyFlagScoreNegative :: AL.Buffer
  , ale_notifyFlagStealPositive :: AL.Buffer
  , ale_notifyFlagStealNegative :: AL.Buffer
  , ale_notifyFlagRecoverPositive :: AL.Buffer
  , ale_notifyFlagRecoverNegative :: AL.Buffer
  , ale_softClick :: NonEmptyVector AL.Buffer
  , ale_flagPickup :: NonEmptyVector AL.Buffer
  , ale_flagDrop :: NonEmptyVector AL.Buffer
  , ale_pixelpusherIntro :: AL.Buffer
  }

data InitialAudioOptions = InitialAudioOptions
  { iao_masterGainPercent :: Int
  , iao_musicGainPercent :: Int
  , iao_effectsGainPercent :: Int
  }

withALEnv :: TimedFastLogger -> InitialAudioOptions -> (ALEnv -> IO ()) -> IO ()
withALEnv logger options action = do
  device <- ALC.alcOpenDevice Foreign.nullPtr
  context <- ALC.alcCreateContext device Foreign.nullPtr

  ok <- ALC.alcMakeContextCurrent context
  if ok == 1
    then do
      withALEnv' options action
    else writeLog logger "Failed to set OpenAL context."

  ALC.alcDestroyContext context
  _ <- ALC.alcCloseDevice device

  pure ()

withALEnv' :: InitialAudioOptions -> (ALEnv -> IO ()) -> IO ()
withALEnv' options action = do
  ale_gen <- MWC.createSystemRandom
  ale_volume <- do
    let vol = Volume.initialVolume (iao_masterGainPercent options)
    setVolume vol
    newIORef vol
  ale_effectsVolume <-
    newIORef $ Volume.initialVolume (iao_effectsGainPercent options)
  ale_bonkLowClearBuffers <- traverse sourceOpusFile Sounds.bonkLowOpus
  ale_bonkLowMuffledBuffers <- traverse sourceOpusFile Sounds.bonkLowMuffledOpus
  ale_bonkHighClearBuffers <- traverse sourceOpusFile Sounds.bonkHighOpus
  ale_bonkHighMuffledBuffers <- traverse sourceOpusFile Sounds.bonkHighMuffledOpus
  ale_smallWhooshLowBuffers <- traverse sourceOpusFile Sounds.smallWhooshLowOpus
  ale_smallWhooshHighBuffers <- traverse sourceOpusFile Sounds.smallWhooshHighOpus
  ale_staticShockBuffers <- traverse sourceOpusFile Sounds.staticShockOpus
  ale_distantCrackBuffer <- traverse sourceOpusFile Sounds.distantCrackOpus
  ale_overseerExplodeBuffer <- traverse sourceOpusFile Sounds.overseerExplodeOpus
  ale_flagExplodeBuffer <- traverse sourceOpusFile Sounds.flagExplodeOpus
  ale_notifyFlagScorePositive <- sourceOpusFile Sounds.notifyFlagScorePositive
  ale_notifyFlagScoreNegative <- sourceOpusFile Sounds.notifyFlagScoreNegative
  ale_notifyFlagStealPositive <- sourceOpusFile Sounds.notifyFlagStealPositive
  ale_notifyFlagStealNegative <- sourceOpusFile Sounds.notifyFlagStealNegative
  ale_notifyFlagRecoverPositive <- sourceOpusFile Sounds.notifyFlagRecoverPositive
  ale_notifyFlagRecoverNegative <- sourceOpusFile Sounds.notifyFlagRecoverNegative
  ale_softClick <- traverse sourceOpusFile Sounds.softClickOpus
  ale_flagPickup <- traverse sourceOpusFile Sounds.flagPickup
  ale_flagDrop <- traverse sourceOpusFile Sounds.flagDrop
  ale_pixelpusherIntro <- sourceOpusFile Sounds.pixelpusherIntroOpus

  let nBonkSources = 8
      nStingerSources = 1
      nGeneralSources = 64 - 2 * nBonkSources - nStingerSources
  ale_generalSourceRing <- SourceRing.init nGeneralSources
  ale_bonkSourceRing <- SourceRing.init nBonkSources
  ale_muffledBonkSourceRing <- SourceRing.init nBonkSources
  ale_stingerSourceRing <- SourceRing.init nStingerSources

  Music.withMusic (iao_musicGainPercent options) $ \ale_music -> do
    let alEnv = ALEnv{..}
    setFixedParameters alEnv
    action alEnv

-- Configure the listener with fixed, predetermined values.
-- Depends on `ALEnv` to ensure that OpenAL has been initialized.
setFixedParameters :: ALEnv -> IO ()
setFixedParameters _alEnv = do
  -- Listener position
  -- This is the default value; I'm just being explicit.
  Foreign.withArray @CFloat [0, 0, 0] $ \ptr ->
    AL.alListenerfv AL.POSITION $ Foreign.castPtr ptr

  -- Listener orientation
  -- "at" vector [0, 0, -1], pointing out from your nose
  -- "up" vector [0, 1, 0], pointing out from the top of your head
  -- These are the default values; I'm just being explicit.
  Foreign.withArray @CFloat [0, 0, -1, 0, 1, 0] $ \ptr ->
    AL.alListenerfv AL.ORIENTATION $ Foreign.castPtr ptr

  -- Distance model
  -- AL_NONE is 0, according to the openal-soft source:
  -- https://github.com/kcat/openal-soft/blob/f8299c60ecf8a227d81c57108c795ce4ea192559/include/AL/al.h#L80
  AL.alDistanceModel 0

-------------------------------------------------------------------------------
-- Volume

getVolume :: ALEnv -> IO Volume.Volume
getVolume env = readIORef (ale_volume env)

toggleMute :: ALEnv -> IO ()
toggleMute env = adjustVolume (ale_volume env) Volume.toggleMute

setVolumePercent :: ALEnv -> Int -> IO ()
setVolumePercent env gainPercent =
  adjustVolume (ale_volume env) (Volume.setGainPercent gainPercent)

increaseVolume :: ALEnv -> IO ()
increaseVolume env = adjustVolume (ale_volume env) Volume.increaseVolume

decreaseVolume :: ALEnv -> IO ()
decreaseVolume env = adjustVolume (ale_volume env) Volume.decreaseVolume

-- Helper
adjustVolume ::
  IORef Volume.Volume -> (Volume.Volume -> Volume.Volume) -> IO ()
adjustVolume volumeRef modifyVolume = do
  newVol <-
    atomicModifyIORef' volumeRef $ \v ->
      let v' = modifyVolume v in (v', v')
  setVolume newVol

-- Internal helper
setVolume :: Volume.Volume -> IO ()
setVolume volume =
  AL.alListenerf AL.GAIN $
    if Volume.muted volume
      then 0
      else Volume.gain volume * 1.5 -- boost volume

-- | Set the gains for each component of the music.
setTrackGains :: ALEnv -> TrackGains -> Maybe CFloat -> IO ()
setTrackGains env = Music.setTrackGains (ale_music env)

-- | Set the global music gain
setMusicGainPercent :: ALEnv -> Int -> IO ()
setMusicGainPercent env = Music.setMusicGainPercent (ale_music env)

-- | Set the gains for each component of the music.
toggleMusicMute :: ALEnv -> IO ()
toggleMusicMute env = Music.toggleMusicMute (ale_music env)

-- | Check whether the music is muted.
getMusicMute :: ALEnv -> IO Muted
getMusicMute env = Music.getMusicMute (ale_music env)

enableVocoderTracks :: ALEnv -> IO ()
enableVocoderTracks env = Music.enableVocoderTracks (ale_music env)

disableVocoderTracks :: ALEnv -> IO ()
disableVocoderTracks env = Music.disableVocoderTracks (ale_music env)

getEffectsGainPercent :: ALEnv -> IO Int
getEffectsGainPercent env =
  Volume.gainPercent <$> readIORef (ale_effectsVolume env)

setEffectsGainPercent :: ALEnv -> Int -> IO ()
setEffectsGainPercent env gainPercent =
  adjustVolume (ale_effectsVolume env) (Volume.setGainPercent gainPercent)

data AudioLevels = AudioLevels
  { al_masterGainPercent :: Int
  , al_musicGainPercent :: Int
  , al_effectsGainPercent :: Int
  }

getAudioLevels :: ALEnv -> IO AudioLevels
getAudioLevels alEnv = do
  al_masterGainPercent <- Volume.gainPercent <$> readIORef (ale_volume alEnv)
  al_musicGainPercent <- Music.getMusicGainPercent (ale_music alEnv)
  al_effectsGainPercent <- Volume.gainPercent <$> readIORef (ale_effectsVolume alEnv)
  pure AudioLevels{..}

-------------------------------------------------------------------------------
-- Playing sounds

data BonkPitch = BonkLow | BonkHigh

playBonk :: ALEnv -> BonkPitch -> CFloat -> (CFloat, CFloat) -> IO ()
playBonk env bonkPitch rawGain pos =
  let buffers = case bonkPitch of
        BonkLow -> ale_bonkLowClearBuffers env
        BonkHigh -> ale_bonkHighClearBuffers env
      gain = rawGain * 0.85
      pitchShiftRadius = 0.002
      sourceRing = ale_bonkSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

playBonkMuffled :: ALEnv -> BonkPitch -> CFloat -> (CFloat, CFloat) -> IO ()
playBonkMuffled env bonkPitch rawGain pos =
  let buffers =
        case bonkPitch of
          BonkLow -> ale_bonkLowMuffledBuffers env
          BonkHigh -> ale_bonkHighMuffledBuffers env
      gain = rawGain
      pitchShiftRadius = 0.002
      sourceRing = ale_muffledBonkSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

data WhooshPitch = WhooshLow | WhooshHigh

playWhoosh :: ALEnv -> WhooshPitch -> CFloat -> (CFloat, CFloat) -> IO ()
playWhoosh env whooshPitch rawGain pos =
  let (buffer, pitchGainFactor) =
        case whooshPitch of
          WhooshLow -> (ale_smallWhooshLowBuffers env, 1.0)
          WhooshHigh -> (ale_smallWhooshHighBuffers env, 0.15)
      gain = pitchGainFactor * rawGain * 1.3
      pitchShiftRadius = 0.002
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

playStaticShock :: ALEnv -> CFloat -> (CFloat, CFloat) -> IO ()
playStaticShock env rawGain pos =
  let buffers = ale_staticShockBuffers env
      gain = 0.122 * rawGain
      pitchShiftRadius = 0.004
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

playDistantCrack :: ALEnv -> CFloat -> (CFloat, CFloat) -> IO ()
playDistantCrack env rawGain pos =
  let buffer = ale_distantCrackBuffer env
      gain = rawGain
      pitchShiftRadius = 0.002
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

playOverseerExplosion :: ALEnv -> CFloat -> (CFloat, CFloat) -> IO ()
playOverseerExplosion env rawGain pos =
  let buffer = ale_overseerExplodeBuffer env
      gain = rawGain * 0.84
      pitchShiftRadius = 0
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

playFlagExplosion :: ALEnv -> CFloat -> (CFloat, CFloat) -> IO ()
playFlagExplosion env rawGain pos =
  let buffer = ale_flagExplodeBuffer env
      gain = rawGain * 0.6
      pitchShiftRadius = 0
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

data FlagNotification
  = NotifyFlagScorePositive
  | NotifyFlagScoreNegative
  | NotifyFlagStealPositive
  | NotifyFlagStealNegative
  | NotifyFlagRecoverPositive
  | NotifyFlagRecoverNegative

playFlagNotification ::
  ALEnv -> FlagNotification -> CFloat -> (CFloat, CFloat) -> IO ()
playFlagNotification env var rawGain pos =
  let buffer =
        case var of
          NotifyFlagScorePositive -> ale_notifyFlagScorePositive env
          NotifyFlagScoreNegative -> ale_notifyFlagScoreNegative env
          NotifyFlagStealPositive -> ale_notifyFlagStealPositive env
          NotifyFlagStealNegative -> ale_notifyFlagStealNegative env
          NotifyFlagRecoverPositive -> ale_notifyFlagRecoverPositive env
          NotifyFlagRecoverNegative -> ale_notifyFlagRecoverNegative env
      gain = rawGain * 0.25
      pitchShiftRadius = 0
      sourceRing = ale_generalSourceRing env
  in  play
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

playSoftClick :: ALEnv -> CFloat -> IO ()
playSoftClick env rawGain =
  let buffers = ale_softClick env
      gain = rawGain * 0.7
      pitchShiftRadius = 0.004
      pos = (0, 0) -- at overseer itself
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

playFlagPickup :: ALEnv -> CFloat -> IO ()
playFlagPickup env rawGain =
  let buffers = ale_flagPickup env
      gain = rawGain * 0.35
      pitchShiftRadius = 0
      pos = (0, 0) -- at overseer itself
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

playFlagDrop :: ALEnv -> CFloat -> IO ()
playFlagDrop env rawGain =
  let buffers = ale_flagDrop env
      gain = rawGain * 0.35
      pitchShiftRadius = 0
      pos = (0, 0) -- at overseer itself
      sourceRing = ale_generalSourceRing env
  in  playRandom
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffers
        gain
        pos
        pitchShiftRadius

playPixelpusherIntro :: ALEnv -> IO ()
playPixelpusherIntro env =
  let buffer = ale_pixelpusherIntro env
      sourceRing = ale_stingerSourceRing env
      gain = 0.3
      pitchShiftRadius = 0
      pos = (0, 0)
  in  play
        (ale_gen env)
        (readIORef (ale_effectsVolume env))
        sourceRing
        buffer
        gain
        pos
        pitchShiftRadius

-- Helper
playRandom ::
  MWC.GenIO ->
  IO Volume ->
  SourceRing ->
  NonEmptyVector AL.Buffer ->
  CFloat ->
  (CFloat, CFloat) ->
  CFloat ->
  IO ()
playRandom gen getSfxVol sourceRing buffers gain pos pitchShiftRadius = do
  buffer <- (buffers VNE.!) <$> MWC.uniformRM (0, VNE.length buffers - 1) gen
  play gen getSfxVol sourceRing buffer gain pos pitchShiftRadius

-- Helper
play ::
  MWC.GenIO ->
  IO Volume ->
  SourceRing ->
  AL.Buffer ->
  CFloat ->
  (CFloat, CFloat) ->
  CFloat ->
  IO ()
play gen getSfxVol sourceRing buffer gain (pos_x, pos_y) pitchShiftRadius = do
  effectsGain <- Volume.gain <$> getSfxVol
  pitchShift <-
    if pitchShiftRadius <= 0
      then pure 1
      else MWC.uniformRM (1 - pitchShiftRadius, 1 + pitchShiftRadius) gen
  SourceRing.play sourceRing buffer $
    PlayOptions
      { play_gain = gain * effectsGain
      , play_pitch = pitchShift
      , play_pos_x = pos_x
      , play_pos_y = pos_y
      }
