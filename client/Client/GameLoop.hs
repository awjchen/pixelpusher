{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- | This module implements several game loops for different purposes: for (1)
-- playing in networked games, (2) playing in local, single-player games
-- against bots, and (3) observing networked games.
--
-- We intend for the game loops to share as much logic as possible to reduce
-- the burden of exercising the code during manual testing. The main drawback
-- is that the shared logic and its implementation is suboptimal for all of the
-- game loops. When possible, we favour the game loop for playing in
-- networked games, to the detriment of the other game loops;
-- for instance, because we have forced the single-player game loop into the
-- structure used by the networked game loop, it has slightly more input
-- latency and its code does not make use of all of the state available to it.
module Client.GameLoop (
  GameLoop,
  ShowTutorial (..),
  runGameLoop,
  networkedGameLoop,
  makeSinglePlayerGameLoop,
  makeObserverGameLoop,
) where

import Control.Concurrent.Async
import Control.Exception (bracket_)
import Control.Monad (when)
import Control.Monad.IO.Class
import Control.Monad.Morph (hoist, lift)
import Control.Monad.ST (RealWorld, stToIO)
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Except
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields (field)
import Data.IORef
import Data.Int (Int32)
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom
import System.Log.FastLogger
import System.Mem (performMinorGC)

import Pixelpusher.Custom.Clock (runAdjustableClock, runClock)
import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Protocol
import Pixelpusher.Game.State
import Pixelpusher.Game.Time

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings
import Client.Logging
import Client.Menu.Input (MenuInputEvent, MenuInputQueue)
import Client.Menu.Input qualified as Menu
import Client.Menu.Pure
import Client.Network (ClientSendHandles (..))
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.CameraInterpolation
import Client.Visuals.Env
import Client.Visuals.Game
import Client.Visuals.SceneDecorations
import Client.Visuals.TeamView

import Client.Audio.Game (NotificationSfxPlayback (..), runGameAudio)
import Client.GameLoop.FrameSelect
import Client.GameLoop.GameStateBuffer
import Client.GameLoop.Input
import Client.Menu
import Client.Scenes.Menus.Audio
import Client.Scenes.Menus.Audio qualified as AudioMenu
import Client.Scenes.Menus.Confirm
import Client.Scenes.Menus.Confirm qualified as ConfirmMenu
import Client.Scenes.Menus.Controls
import Client.Scenes.Menus.Controls qualified as ControlsMenu
import Client.Scenes.Menus.Pause
import Client.Scenes.Menus.Pause qualified as PauseMenu
import Client.Scenes.Menus.Visual
import Client.Scenes.Menus.Visual qualified as VisualMenu

--------------------------------------------------------------------------------

data RenderLoopState = RenderLoopState
  { rls_renderTicks :: Int32
  , rls_cameraPosVel :: (Float2, Float2)
  , rls_frameSelectState :: FrameSelectState
  , rls_lastGameStateBufferOutputs :: Maybe FrameOutputs
  }
  deriving stock (Generic)

data InputLoopState = InputLoopState
  { ils_inputTicks :: Int
  , ils_lastInputUpdateTicks :: PreviousInputUpdateTicks
  , ils_lastInputUpdate :: PlayerControlsTransport
  , ils_showTutorial :: ShowTutorial
  , ils_showMenu :: ShowMenu
  , ils_audioMenuState :: MenuState AudioOptionsWidgets AudioOptions
  , ils_visualMenuState :: MenuState VisualOptionsWidgets VisualOptions
  , ils_controlsMenuState :: MenuState ControlsWidgets ControlOptions
  , ils_pauseMenuState :: MenuState PauseWidgets ()
  , ils_confirmQuitMenuState :: MenuState ConfirmWidgets ()
  }
  deriving stock (Generic)

data PreviousInputUpdateTicks = PreviousInputUpdateTicks
  { piut_older :: Int
  , piut_newer :: Int
  }

pushInputUpdateTick ::
  Int -> PreviousInputUpdateTicks -> PreviousInputUpdateTicks
pushInputUpdateTick newTick previousTicks =
  PreviousInputUpdateTicks
    { piut_older = piut_newer previousTicks
    , piut_newer = newTick
    }

data ShowTutorial = ShowTutorial | DoNotShowTutorial

data ShowMenu
  = DoNotShowMenu
  | ShowPauseMenu
  | ShowAudioOptionsMenu
  | ShowVisualOptionsMenu
  | ShowControlOptionsMenu
  | ShowConfirmQuitMenu

initialRenderLoopState :: RenderLoopState
initialRenderLoopState =
  RenderLoopState
    { rls_renderTicks = 0
    , rls_cameraPosVel = (Float2 0 0, Float2 0 0) -- arbitrary
    , rls_frameSelectState = initialFrameSelectState
    , rls_lastGameStateBufferOutputs = Nothing
    }

initialInputLoopState :: ShowTutorial -> InputLoopState
initialInputLoopState showTutorial =
  InputLoopState
    { ils_inputTicks = 0
    , ils_lastInputUpdateTicks = PreviousInputUpdateTicks 0 0
    , ils_lastInputUpdate = defaultControlsTransport
    , ils_showTutorial = showTutorial
    , ils_showMenu = DoNotShowMenu
    , ils_audioMenuState = initialMenuState "audio settings" audioMenu
    , ils_visualMenuState = initialMenuState "visual settings" visualMenu
    , ils_controlsMenuState = initialMenuState "control settings" controlsMenu
    , ils_pauseMenuState = initialMenuState "" pauseMenu
    , ils_confirmQuitMenuState = initialMenuState "quit game?" confirmMenu
    }

ils_showTutorial_showMenu ::
  Lens' InputLoopState (ShowTutorial, ShowMenu)
ils_showTutorial_showMenu f s =
  let showTutorial = ils_showTutorial s
      showMenu = ils_showMenu s
  in  f (showTutorial, showMenu)
        <&> \(showTutorial', showMenu') ->
          s
            & field @"ils_showTutorial"
            .~ showTutorial'
            & field @"ils_showMenu"
            .~ showMenu'

--------------------------------------------------------------------------------

data GameLoop = GameLoop
  { gl_inputLoop :: InputLoop
  , gl_renderLoop :: RenderLoop
  }

newtype InputLoop
  = InputLoop
      ( ThemeEnv ->
        StateRef (AudioOptions, VisualOptions, ControlOptions) ->
        IORef UIView ->
        IORef CameraPos ->
        IORef CursorPos ->
        IORef (Maybe ActorID) ->
        GLEnv ->
        ALEnv ->
        InputEventsHandle ->
        MenuInputQueue ->
        Keybindings ->
        StateT InputLoopState IO PlayerContinueStatus
      )

newtype RenderLoop
  = RenderLoop
      ( (Int -> IO ()) ->
        IO (AudioOptions, VisualOptions, ControlOptions) ->
        IORef UIView ->
        IORef CameraPos ->
        IORef (Maybe ActorID) ->
        GLEnv ->
        ALEnv ->
        ThemeEnv ->
        Keybindings ->
        StateT RenderLoopState IO ()
      )

type CameraPos = Float2
type CursorPos = Fixed2

data UIView = UIView
  { uiv_showTutorial :: ShowTutorial
  , uiv_showMenu :: ShowMenu
  , uiv_audioMenuState :: MenuState AudioOptionsWidgets AudioOptions
  , uiv_visualMenuState :: MenuState VisualOptionsWidgets VisualOptions
  , uiv_controlsMenuState :: MenuState ControlsWidgets ControlOptions
  , uiv_pauseMenuState :: MenuState PauseWidgets ()
  , uiv_confirmQuitMenuState :: MenuState ConfirmWidgets ()
  , uiv_displayStats :: DisplayStats
  , uiv_displayHelp :: DisplayHelp
  }

extractUIView :: DisplayStats -> DisplayHelp -> InputLoopState -> UIView
extractUIView displayStats showHelpScreen InputLoopState{..} =
  UIView
    { uiv_showTutorial = ils_showTutorial
    , uiv_showMenu = ils_showMenu
    , uiv_audioMenuState = ils_audioMenuState
    , uiv_visualMenuState = ils_visualMenuState
    , uiv_controlsMenuState = ils_controlsMenuState
    , uiv_pauseMenuState = ils_pauseMenuState
    , uiv_confirmQuitMenuState = ils_confirmQuitMenuState
    , uiv_displayStats = displayStats
    , uiv_displayHelp = showHelpScreen
    }

inputLoopRateMultiplier :: Int
inputLoopRateMultiplier = 8

inputLoopDelay_ns :: Int
inputLoopDelay_ns = C.tickDelay_ns `div` inputLoopRateMultiplier

-- | This function must be called from the main thread because it performs
-- calls to GLFW.
runGameLoop ::
  TimedFastLogger ->
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  Keybindings ->
  ShowTutorial ->
  GameLoop ->
  IO ()
runGameLoop logger glEnv alEnv themeEnv stateRef keybindings showTutorial gameLoop = do
  withMusicGains alEnv $ do
    inputEvents <- newInputEventsHandle
    menuInputQueue <- Menu.newQueue
    cameraPosRef <- newIORef $ fst $ rls_cameraPosVel initialRenderLoopState
    cursorPosRef <- newIORef 0
    actorIDRef <- newIORef Nothing
    let keyCallback =
          gameKeyCallback themeEnv inputEvents cursorPosRef actorIDRef
    withKeyCallback glEnv keyCallback $ do
      let initLoopState = initialInputLoopState showTutorial
          initUIView = extractUIView HideStats HideHelp initLoopState
      uiViewRef <- newIORef initUIView

      let renderingThread =
            bracket_ (GLFW.makeContextCurrent (Just (gle_window glEnv))) (GLFW.makeContextCurrent Nothing) $
              flip execStateT initialRenderLoopState $
                runAdjustableClock C.tickDelay_ns $ \delayClock ->
                  let RenderLoop renderLoop = gl_renderLoop gameLoop
                  in  renderLoop
                        delayClock
                        (readStateRef stateRef)
                        uiViewRef
                        cameraPosRef
                        actorIDRef
                        glEnv
                        alEnv
                        themeEnv
                        keybindings
      bracket_ (GLFW.makeContextCurrent Nothing) (GLFW.makeContextCurrent (Just (gle_window glEnv))) $
        withAsyncBound renderingThread $
          \renderingThreadAsync ->
            evalContT . callCC $ \k -> do
              -- provides a way to break out of the loop
              flip evalStateT initLoopState $
                runClock inputLoopDelay_ns $ do
                  -- the loop
                  playerContinueStatus <-
                    hoist lift $ do
                      let InputLoop inputLoop = gl_inputLoop gameLoop
                      inputLoop
                        themeEnv
                        stateRef
                        uiViewRef
                        cameraPosRef
                        cursorPosRef
                        actorIDRef
                        glEnv
                        alEnv
                        inputEvents
                        menuInputQueue
                        keybindings
                  case playerContinueStatus of
                    PlayerQuit -> do
                      liftIO $
                        poll renderingThreadAsync >>= \case
                          Nothing -> pure ()
                          Just result -> case result of
                            Left err ->
                              writeLog logger $
                                "Exception in rendering thread: "
                                  <> toLogStr (show err)
                            Right _ -> pure ()
                      lift $ k ()
                    PlayerContinue ->
                      liftIO performMinorGC

-- | Set music gains when starting and stopping the game loop
withMusicGains :: ALEnv -> IO a -> IO a
withMusicGains alEnv =
  bracket_
    (setTrackGains alEnv initialGains Nothing)
    (setTrackGains alEnv zeroGains Nothing)
  where
    initialGains =
      TrackGains
        { tg_slow = 0
        , tg_fast = 1
        , tg_slowFast = 1
        , tg_haveEnemyFlag = 0
        , tg_alliedFlagStolen = 0
        , tg_chantPanning = 0
        }
    zeroGains =
      TrackGains
        { tg_slow = 0
        , tg_fast = 0
        , tg_slowFast = 0
        , tg_haveEnemyFlag = 0
        , tg_alliedFlagStolen = 0
        , tg_chantPanning = 0
        }

--------------------------------------------------------------------------------

-- | For playing a networked game hosted by a server. This is the most
-- important game mode.
networkedGameLoop ::
  DebugMode ->
  ClientSendHandles ClientGameMsg ->
  GameStateBuffer ->
  IORef (Maybe Int) ->
  GameLoop
networkedGameLoop debugMode sendHandles gameStateBuffer networkLatencyRef =
  GameLoop
    { gl_inputLoop = networkedInputLoop debugMode sendHandles
    , gl_renderLoop = networkedRenderLoop gameStateBuffer networkLatencyRef
    }

networkedInputLoop :: DebugMode -> ClientSendHandles ClientGameMsg -> InputLoop
networkedInputLoop debugMode sendHandles =
  InputLoop $ \themeEnv stateRef uiViewRef cameraPosRef cursorPosRef actorIDRef glEnv alEnv inputEvents menuInputQueue keybindings -> do
    inputTicks <- field @"ils_inputTicks" <%= (+) 1
    lastUpdateTicks <- gets ils_lastInputUpdateTicks

    -- Inputs
    controlOpts <- liftIO $ view _3 <$> readStateRef stateRef
    ( playerControls
      , playerContinueStatus
      , displayStats
      , showHelpScreen
      , debugCommands
      ) <-
      sharedInputs
        glEnv
        alEnv
        themeEnv
        stateRef
        inputEvents
        menuInputQueue
        keybindings
        controlOpts
        cameraPosRef
        cursorPosRef
        actorIDRef

    -- Send player controls to the server. Send updates regularly, but also
    -- send controls immediately when a button is pressed or released (up to a
    -- rate limit).
    let underRateLimit =
          inputTicks - piut_older lastUpdateTicks >= inputLoopRateMultiplier
    when underRateLimit $ do
      oldPlayerControlsButtons <-
        gets $ getControlsTransportButtons . ils_lastInputUpdate
      let newPlayerControlsButtons = getControlsTransportButtons playerControls
          shouldSendUpdate =
            newPlayerControlsButtons /= oldPlayerControlsButtons -- send if button state changes
              || inputTicks - piut_newer lastUpdateTicks >= inputLoopRateMultiplier -- or if we've reached a delay limit
      when shouldSendUpdate $ do
        liftIO $ client_sendUDP sendHandles (ClientControls playerControls)
        field @"ils_lastInputUpdate" .= playerControls
        field @"ils_lastInputUpdateTicks" %= pushInputUpdateTick inputTicks

    -- Update UI view
    gets (extractUIView displayStats showHelpScreen)
      >>= liftIO . atomicWriteIORef uiViewRef

    -- Send debug commands
    case debugMode of
      NoDebugMode -> pure ()
      DebugMode ->
        for_ debugCommands $
          liftIO . client_sendTCP sendHandles . ClientDebugCommand

    pure playerContinueStatus

networkedRenderLoop :: GameStateBuffer -> IORef (Maybe Int) -> RenderLoop
networkedRenderLoop gameStateBuffer networkLatencyRef =
  RenderLoop $ \delayClock readOpts uiViewRef cameraPosRef actorIDRef glEnv alEnv themeEnv keybindings -> do
    uiView <- lift $ readIORef uiViewRef

    -- Rate control
    let ticksPerFrame = getTicksPerFrame glEnv
    clientTicks <- field @"rls_renderTicks" <%= (+) 1
    when (clientTicks `rem` ticksPerFrame == 0) $ do
      -- Get next frame
      (frameMaybe, frameType) <- do
        frameMaybe <- do
          zoom (field @"rls_frameSelectState") $
            getFrame delayClock gameStateBuffer (Ticks ticksPerFrame)
        case frameMaybe of
          Nothing ->
            (,OldFrame) <$> gets rls_lastGameStateBufferOutputs
          Just gsbOutputs -> do
            field @"rls_lastGameStateBufferOutputs" .= Just gsbOutputs
            liftIO $
              writeIORef actorIDRef $
                PlayerActor <$> fo_playerID gsbOutputs
            pure (Just gsbOutputs, NewFrame)

      -- Audiovisuals
      networkLatencyMaybe <- liftIO $ readIORef networkLatencyRef
      for_ frameMaybe $ \FrameOutputs{..} ->
        for_ fo_playerID $ \playerID ->
          zoom (field @"rls_cameraPosVel") $ do
            (audioOpts, visualOpts, controlOpts) <- liftIO readOpts
            sharedAudioVisuals
              frameType
              glEnv
              alEnv
              clientTicks
              networkLatencyMaybe
              themeEnv
              audioOpts
              visualOpts
              controlOpts
              keybindings
              uiView
              fo_gameScene
              fo_gameEvents
              fo_isFramePredicted
              (Just fo_roomID)
              (CameraFollowsActor (PlayerActor playerID))

    gets (fst . rls_cameraPosVel) >>= \cameraPos ->
      liftIO $ atomicWriteIORef cameraPosRef cameraPos

--------------------------------------------------------------------------------

-- | For playing a local, single-player game against bots
makeSinglePlayerGameLoop :: PlayerID -> GameState RealWorld -> IO GameLoop
makeSinglePlayerGameLoop playerID gameState = do
  gameEventsRef <- newIORef []
  playerControlsRef <- newIORef defaultControls
  pure
    GameLoop
      { gl_inputLoop = singlePlayerInputLoop playerControlsRef
      , gl_renderLoop =
          singlePlayerRenderLoop playerID gameState playerControlsRef gameEventsRef
      }

singlePlayerInputLoop :: IORef PlayerControls -> InputLoop
singlePlayerInputLoop playerControlsRef =
  InputLoop $ \themeEnv stateRef uiViewRef cameraPosRef cursorPosRef actorIDRef glEnv alEnv inputEvents menuInputQueue keybindings -> do
    -- Inputs
    controlOpts <- liftIO $ view _3 <$> readStateRef stateRef
    ( playerControlsTransport
      , playerContinueStatus
      , displayStats
      , showHelpScreen
      , _
      ) <-
      sharedInputs
        glEnv
        alEnv
        themeEnv
        stateRef
        inputEvents
        menuInputQueue
        keybindings
        controlOpts
        cameraPosRef
        cursorPosRef
        actorIDRef

    -- Update player controls
    liftIO $
      atomicWriteIORef
        playerControlsRef
        (fromControlsTransport playerControlsTransport)

    -- Update UI view
    gets (extractUIView displayStats showHelpScreen)
      >>= liftIO . atomicWriteIORef uiViewRef

    pure playerContinueStatus

singlePlayerRenderLoop ::
  PlayerID ->
  GameState RealWorld ->
  IORef PlayerControls ->
  IORef [GameEvents] ->
  RenderLoop
singlePlayerRenderLoop playerID gameState playerControlsRef gameEventsRef =
  RenderLoop $ \_ readOpts uiViewRef cameraPosRef _actorIDRef glEnv alEnv themeEnv keybindings -> do
    uiView <- lift $ readIORef uiViewRef
    playerControls <- lift $ readIORef playerControlsRef

    -- Step game state
    let controlsMap = WIM.singleton playerID playerControls
        gameCommands = []
    stepGameEvents <-
      case uiv_showMenu uiView of
        DoNotShowMenu -> do
          let debugCmd = Nothing
          lift . stToIO $
            integrateGameState controlsMap gameCommands debugCmd gameState
        ShowPauseMenu ->
          pure mempty
        ShowAudioOptionsMenu ->
          pure mempty
        ShowVisualOptionsMenu ->
          pure mempty
        ShowControlOptionsMenu ->
          pure mempty
        ShowConfirmQuitMenu ->
          pure mempty

    let ticksPerFrame = getTicksPerFrame glEnv
    clientTicks <- field @"rls_renderTicks" <%= (+) 1
    if clientTicks `rem` ticksPerFrame /= 0
      then do
        -- Record game events
        liftIO $ modifyIORef' gameEventsRef (stepGameEvents :)
      else do
        -- Collect all game events since last render
        gameEvents <-
          mconcat . reverse . (stepGameEvents :)
            <$> liftIO (readIORef gameEventsRef)
        liftIO $ writeIORef gameEventsRef []

        -- Audiovisuals
        gameScene <-
          lift $ stToIO $ getGameScene gameState $ Just (PlayerActor playerID)
        let networkLatencyMaybe = Nothing
            roomIDMaybe = Nothing
        zoom (field @"rls_cameraPosVel") $ do
          (audioOpts, visualOpts, controlOpts) <- liftIO readOpts
          sharedAudioVisuals
            NewFrame
            glEnv
            alEnv
            clientTicks
            networkLatencyMaybe
            themeEnv
            audioOpts
            visualOpts
            controlOpts
            keybindings
            uiView
            gameScene
            gameEvents
            False
            roomIDMaybe
            (CameraFollowsActor (PlayerActor playerID))

    gets (fst . rls_cameraPosVel) >>= \cameraPos ->
      liftIO $ atomicWriteIORef cameraPosRef cameraPos

--------------------------------------------------------------------------------

-- | For observation of a game streamed from a server
makeObserverGameLoop ::
  DebugMode ->
  ClientSendHandles ClientGameMsg ->
  GameStateBuffer ->
  IORef (Maybe Int) ->
  IO GameLoop
makeObserverGameLoop debugMode sendHandles gameStateBuffer networkLatencyRef = do
  playerControlsRef <- newIORef defaultControls
  pure
    GameLoop
      { gl_inputLoop =
          observerInputLoop debugMode sendHandles playerControlsRef
      , gl_renderLoop =
          observerRenderLoop gameStateBuffer networkLatencyRef playerControlsRef
      }

observerInputLoop ::
  DebugMode ->
  ClientSendHandles ClientGameMsg ->
  IORef PlayerControls ->
  InputLoop
observerInputLoop debugMode sendHandles playerControlsRef =
  InputLoop $ \themeEnv stateRef uiViewRef cameraPosRef cursorPosRef actorIDRef glEnv alEnv inputEvents menuInputQueue keybindings -> do
    inputTicks <- field @"ils_inputTicks" <%= (+) 1

    -- Inputs
    controlOpts <- liftIO $ view _3 <$> readStateRef stateRef
    ( playerControls
      , playerContinueStatus
      , displayStats
      , showHelpScreen
      , debugCommands
      ) <-
      sharedInputs
        glEnv
        alEnv
        themeEnv
        stateRef
        inputEvents
        menuInputQueue
        keybindings
        controlOpts
        cameraPosRef
        cursorPosRef
        actorIDRef

    -- Update player controls
    liftIO $
      atomicWriteIORef playerControlsRef $
        fromControlsTransport playerControls

    -- Send dummy player controls to the server in order to trigger the sending
    -- of acknowledgements of server messages.
    --
    -- TODO: Could specialize observer client-server communication
    when (inputTicks `mod` inputLoopRateMultiplier == 0) $
      liftIO $
        client_sendUDP sendHandles (ClientControls defaultControlsTransport)

    -- Update UI view
    gets (extractUIView displayStats showHelpScreen)
      >>= liftIO . atomicWriteIORef uiViewRef

    -- Send debug commands
    case debugMode of
      NoDebugMode -> pure ()
      DebugMode ->
        for_ debugCommands $
          liftIO . client_sendTCP sendHandles . ClientDebugCommand

    pure playerContinueStatus

observerRenderLoop ::
  GameStateBuffer ->
  IORef (Maybe Int) ->
  IORef PlayerControls ->
  RenderLoop
observerRenderLoop gameStateBuffer networkLatencyRef playerControlsRef =
  RenderLoop $ \delayClock readOpts uiViewRef cameraPosRef _actorIDRef glEnv alEnv themeEnv keybindings -> do
    uiView <- lift $ readIORef uiViewRef
    playerControls <- lift $ readIORef playerControlsRef

    -- Rate control
    let ticksPerFrame = getTicksPerFrame glEnv
    clientTicks <- field @"rls_renderTicks" <%= (+) 1
    when (clientTicks `rem` ticksPerFrame == 0) $ do
      -- Get next frame
      (frameMaybe, frameType) <- do
        frameMaybe <- do
          zoom (field @"rls_frameSelectState") $
            getFrame delayClock gameStateBuffer (Ticks ticksPerFrame)
        case frameMaybe of
          Nothing ->
            (,OldFrame) <$> gets rls_lastGameStateBufferOutputs
          Just gsbOutputs -> do
            field @"rls_lastGameStateBufferOutputs" .= Just gsbOutputs
            pure (Just gsbOutputs, NewFrame)

      -- Audiovisuals
      networkLatencyMaybe <- liftIO $ readIORef networkLatencyRef
      for_ frameMaybe $ \FrameOutputs{..} ->
        zoom (field @"rls_cameraPosVel") $ do
          (audioOpts, visualOpts, controlOpts) <- liftIO readOpts
          sharedAudioVisuals
            frameType
            glEnv
            alEnv
            clientTicks
            networkLatencyMaybe
            themeEnv
            audioOpts
            visualOpts
            controlOpts
            keybindings
            uiView
            fo_gameScene
            fo_gameEvents
            fo_isFramePredicted
            (Just fo_roomID)
            (FreeCamera playerControls)
    gets (fst . rls_cameraPosVel) >>= \cameraPos ->
      liftIO $ atomicWriteIORef cameraPosRef cameraPos

--------------------------------------------------------------------------------

-- | Input handling shared between the networked, single-player, and observer
-- game loops.
sharedInputs ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  InputEventsHandle ->
  MenuInputQueue ->
  Keybindings ->
  ControlOptions ->
  IORef Float2 ->
  IORef Fixed2 ->
  IORef (Maybe ActorID) ->
  StateT
    InputLoopState
    IO
    ( PlayerControlsTransport
    , PlayerContinueStatus
    , DisplayStats
    , DisplayHelp
    , [DebugCommand]
    )
sharedInputs
  glEnv
  alEnv
  themeEnv
  stateRef
  inputEvents
  menuInputQueue
  keybindings
  controlOpts
  cameraPosRef
  cursorPosRef
  actorIDRef = do
    cameraPos <- liftIO $ readIORef cameraPosRef

    FrameInputs{..} <-
      liftIO $
        pollInputs glEnv inputEvents menuInputQueue keybindings controlOpts cameraPos
    let cursorPos = control_worldPos $ fromControlsTransport fi_controls
    liftIO $ atomicWriteIORef cursorPosRef cursorPos

    -- Handle UI events
    zoom ils_showTutorial_showMenu $
      runUIEvents glEnv themeEnv menuInputQueue keybindings fi_uiEvents

    -- Handle menu events
    menuContinueStatus <-
      runMenuEvents
        glEnv
        alEnv
        themeEnv
        stateRef
        inputEvents
        cursorPosRef
        actorIDRef
        fi_menuEvents

    let continueStatus =
          case fi_continueStatus of
            PlayerQuit -> PlayerQuit
            PlayerContinue -> case menuContinueStatus of
              PlayerQuit -> PlayerQuit
              PlayerContinue -> PlayerContinue
    pure
      ( fi_controls
      , continueStatus
      , fi_displayStats
      , fi_displayHelp
      , fi_debugEvents
      )

data FrameType = NewFrame | OldFrame

data CameraMovement
  = CameraFollowsActor ActorID
  | FreeCamera PlayerControls

-- | Audiovisuals shared between the networked and single-player game loops.
--
-- Calls `GLFW.swapBuffers`
sharedAudioVisuals ::
  FrameType ->
  GLEnv ->
  ALEnv ->
  Int32 ->
  Maybe Int ->
  ThemeEnv ->
  AudioOptions ->
  VisualOptions ->
  ControlOptions ->
  Keybindings ->
  UIView ->
  GameScene ->
  GameEvents ->
  Bool ->
  Maybe Int ->
  CameraMovement ->
  StateT
    (Float2, Float2) -- (camera pos, camera vel)
    IO
    ()
sharedAudioVisuals
  frameType
  glEnv
  alEnv
  clientTicks
  networkLatencyMaybe
  themeEnv
  audioOpts
  visualOpts
  controlOpts
  keybindings
  uiView
  gameScene
  gameEvents
  isFramePredicted
  roomIDMaybe
  cameraMovement = do
    -- Handle framebuffer size changes. Needs to be done here, on the rendering
    -- thread.
    liftIO $
      gle_pollFramebufferSizeUpdate glEnv >>= \case
        Nothing -> pure ()
        Just (w, h) -> setViewport (fromIntegral w) (fromIntegral h)
    -- Visuals before audio
    renderVisuals
    -- Only play audio for new frames
    case frameType of
      OldFrame -> pure ()
      NewFrame -> do
        cameraPos <- gets fst
        let actorIdMaybe =
              case cameraMovement of
                CameraFollowsActor actorID -> Just actorID
                FreeCamera{} -> Nothing
        liftIO $
          runGameAudio
            alEnv
            enableSpatialAudio
            PlayNotificationSfx
            gameEvents
            gameScene
            cameraPos
            actorIdMaybe
    where
      enableSpatialAudio = True
      renderVisuals = do
        let ticksPerFrame = getTicksPerFrame glEnv
            ( cameraInterpolation
              , scenePerspective
              , teamPerspective
              , teamView
              ) =
                case cameraMovement of
                  FreeCamera playerControls ->
                    let moveDir =
                          Fixed.toFloats $ arrowKeyDirection playerControls
                        cameraInterpolation' (pos, vel) =
                          let vel' = vel * 0.97 + Float.map2 (* 0.225) moveDir
                              pos' = pos + 0.5 * (vel + vel')
                          in  (pos', vel')
                    in  (cameraInterpolation', FreePerspective, Nothing, AbsoluteTeamView)
                  CameraFollowsActor actorID ->
                    let (player, playerView) =
                          expectJust "renderVisuals: could not find ActorID" $
                            WIM.lookup actorID (scene_players gameScene)
                        cameraInterpolation' =
                          overseerCameraInterpolation
                            ticksPerFrame
                            (pv_overseerView playerView)
                        scenePerspective' =
                          PlayerPerspective $
                            PlayerInfo actorID player playerView
                        teamPerspective' =
                          Just $ view player_team player
                        teamView' =
                          case vo_teamColorMode visualOpts of
                            AbsoluteTeamColors ->
                              AbsoluteTeamView
                            RelativeTeamColors ->
                              RelativeTeamView $ view player_team player
                    in  (cameraInterpolation', scenePerspective', teamPerspective', teamView')

        modify' $
          case frameType of
            OldFrame -> id
            NewFrame -> cameraInterpolation
        newCameraPos <- gets fst

        theme <- liftIO $ getTheme themeEnv (vo_themeSelection visualOpts)
        liftIO $ do
          renderGameScene glEnv gameScene newCameraPos keybindings controlOpts $
            SceneDecorations
              { sceneDeco_theme = theme
              , sceneDeco_perspective = scenePerspective
              , sceneDeco_teamPerspective = teamPerspective
              , sceneDeco_teamView = teamView
              , sceneDeco_renderStatsOverlay = uiv_displayStats uiView
              , sceneDeco_renderHelpOverlay = uiv_displayHelp uiView
              , sceneDeco_roomID = roomIDMaybe
              , sceneDeco_networkLatency = networkLatencyMaybe
              , sceneDeco_elaspedClientTime = Ticks clientTicks
              , sceneDeco_showTutorial =
                  case uiv_showTutorial uiView of
                    ShowTutorial -> True
                    DoNotShowTutorial -> False
              , sceneDeco_showMenu =
                  case uiv_showMenu uiView of
                    DoNotShowMenu ->
                      NoMenuScene
                    ShowPauseMenu ->
                      ShowMenuScene () (uiv_pauseMenuState uiView)
                    ShowAudioOptionsMenu ->
                      ShowMenuScene audioOpts (uiv_audioMenuState uiView)
                    ShowVisualOptionsMenu ->
                      ShowMenuScene visualOpts (uiv_visualMenuState uiView)
                    ShowControlOptionsMenu ->
                      ShowMenuScene controlOpts (uiv_controlsMenuState uiView)
                    ShowConfirmQuitMenu ->
                      ShowMenuScene () (uiv_confirmQuitMenuState uiView)
              , sceneDeco_showOSCursor = ShowOSCursor
              , sceneDeco_isFramePredicted = isFramePredicted
              , sceneDeco_enableScreenShake = vo_enableScreenShake visualOpts
              , sceneDeco_enableTutorialMode =
                  gp_enableTutorialMode (scene_gameParams gameScene)
              , sceneDeco_showDebugInfo = DDM_None
              , sceneDeco_spatialAudioStatus = enableSpatialAudio
              , sceneDeco_showUI = True
              , sceneDeco_showHUD = True
              , sceneDeco_showPlayerTags = True
              , sceneDeco_showCursor = True
              , sceneDeco_showFlagCaptureFlash = True
              }
        liftIO $ GLFW.swapBuffers $ gle_window glEnv

--------------------------------------------------------------------------------
-- Menu event handling

runMenuEvents ::
  GLEnv ->
  ALEnv ->
  ThemeEnv ->
  StateRef (AudioOptions, VisualOptions, ControlOptions) ->
  InputEventsHandle ->
  IORef Fixed2 ->
  IORef (Maybe ActorID) ->
  [MenuInputEvent] ->
  StateT InputLoopState IO PlayerContinueStatus
runMenuEvents glEnv alEnv themeEnv stateRef inputEvents cursorPosRef actorIDRef menuEvents = do
  gets ils_showMenu >>= \case
    DoNotShowMenu ->
      pure PlayerContinue
    ShowConfirmQuitMenu -> do
      runMenu
        menuEvents
        unitStateRef
        (field @"ils_confirmQuitMenuState")
        (ConfirmMenu.handleInput alEnv)
        \case
          Confirmed -> do
            pure PlayerQuit
          Cancelled -> do
            field @"ils_showMenu" .= ShowPauseMenu
            pure PlayerContinue
    ShowPauseMenu -> do
      runMenu
        menuEvents
        unitStateRef
        (field @"ils_pauseMenuState")
        (PauseMenu.handleInput alEnv)
        \case
          Pause_ReturnToGame -> do
            field @"ils_pauseMenuState" %= resetMenuNavigation
            field @"ils_showMenu" .= DoNotShowMenu
            -- Disable menu keybindings
            liftIO $
              GLFW.setKeyCallback (gle_window glEnv) $
                Just $
                  gameKeyCallback themeEnv inputEvents cursorPosRef actorIDRef
            pure PlayerContinue
          Pause_AudioOptions -> do
            field @"ils_audioMenuState" %= resetMenuNavigation
            field @"ils_showMenu" .= ShowAudioOptionsMenu
            pure PlayerContinue
          Pause_VisualOptions -> do
            field @"ils_visualMenuState" %= resetMenuNavigation
            field @"ils_showMenu" .= ShowVisualOptionsMenu
            pure PlayerContinue
          Pause_ControlOptions -> do
            field @"ils_controlsMenuState" %= resetMenuNavigation
            field @"ils_showMenu" .= ShowControlOptionsMenu
            pure PlayerContinue
          Pause_QuitGame -> do
            field @"ils_confirmQuitMenuState" %= resetMenuNavigation
            field @"ils_showMenu" .= ShowConfirmQuitMenu
            pure PlayerContinue
    ShowAudioOptionsMenu -> do
      -- Synchronize audio settings
      audioLevels <- liftIO $ getAudioLevels alEnv
      liftIO $ overStateRef _1 stateRef $ \audioOptions ->
        audioOptions
          { ao_masterVolume = al_masterGainPercent audioLevels
          , ao_musicVolume = al_musicGainPercent audioLevels
          , ao_effectsVolume = al_effectsGainPercent audioLevels
          }
      runMenu
        menuEvents
        (zoomStateRef _1 stateRef)
        (field @"ils_audioMenuState")
        (AudioMenu.handleInput alEnv)
        \_ -> do
          field @"ils_showMenu" .= ShowPauseMenu
          pure PlayerContinue
    ShowVisualOptionsMenu -> do
      -- Synchronize visual settings
      enableFullscreen <-
        liftIO $
          getFullscreenMode glEnv <&> \case
            Fullscreen -> True
            Windowed -> False
      liftIO $ overStateRef _2 stateRef $ \visualOptions ->
        visualOptions{vo_enableFullscreen = enableFullscreen}
      runMenu
        menuEvents
        (zoomStateRef _2 stateRef)
        (field @"ils_visualMenuState")
        (VisualMenu.handleInput glEnv alEnv)
        \_ -> do
          field @"ils_showMenu" .= ShowPauseMenu
          pure PlayerContinue
    ShowControlOptionsMenu -> do
      runMenu
        menuEvents
        (zoomStateRef _3 stateRef)
        (field @"ils_controlsMenuState")
        (ControlsMenu.handleInput alEnv)
        \_ -> do
          field @"ils_showMenu" .= ShowPauseMenu
          pure PlayerContinue

runMenu ::
  [MenuInputEvent] ->
  StateRef model ->
  Lens' InputLoopState (MenuState ids model) ->
  (Maybe (MenuUpdateEvent ids) -> IO (LoopContinue result)) ->
  (result -> StateT InputLoopState IO PlayerContinueStatus) ->
  StateT InputLoopState IO PlayerContinueStatus
runMenu menuEvents modelRef menuStateLens updateHandler k = do
  result <-
    runExceptT $
      for_ menuEvents $ \inputEvent -> do
        model0 <- liftIO $ readStateRef modelRef
        menuState0 <- lift $ use menuStateLens
        result <-
          case handleMenuInput inputEvent model0 menuState0 of
            Nothing ->
              lift $ lift $ updateHandler Nothing
            Just (!model1, !state1, updateEvent) ->
              lift $ do
                liftIO $ writeStateRef modelRef model1
                menuStateLens .= state1
                lift $ updateHandler (Just updateEvent)
        case result of
          LoopContinue -> pure ()
          LoopFinished loopResult -> throwE loopResult
  case result of
    Right () -> pure PlayerContinue
    Left r -> k r

--------------------------------------------------------------------------------
-- UI event handling

runUIEvents ::
  GLEnv ->
  ThemeEnv ->
  MenuInputQueue ->
  Keybindings ->
  [UIEvent] ->
  StateT (ShowTutorial, ShowMenu) IO ()
runUIEvents glEnv themeEnv menuInputs keybindings =
  traverse_ (runUIEvent glEnv themeEnv menuInputs keybindings)

runUIEvent ::
  GLEnv ->
  ThemeEnv ->
  MenuInputQueue ->
  Keybindings ->
  UIEvent ->
  StateT (ShowTutorial, ShowMenu) IO ()
runUIEvent glEnv themeEnv menuInputs keybindings = \case
  EscapePressed -> do
    -- Enable menu keybindings
    _2 .= ShowPauseMenu
    liftIO $
      GLFW.setKeyCallback (gle_window glEnv) $
        Just $
          optionsKeyCallback themeEnv menuInputs keybindings
  CtrlPressed ->
    -- Disable tutorial
    _1 .= DoNotShowTutorial
