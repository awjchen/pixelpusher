module Client.HexColor (
  HexColor (..),
) where

import Data.Char (isHexDigit)
import Data.List qualified
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Word (Word8)
import GHC.Generics
import Numeric (readHex, showHex)
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Matcher qualified as Toml
import Toml.Schema.ToValue qualified as Toml

data HexColor = HexColor Word8 Word8 Word8
  deriving stock (Generic)

instance Toml.ToValue HexColor where
  toValue (HexColor r g b) =
    Toml.Text . Text.pack $
      showHex2 r <> showHex2 g <> showHex2 b
    where
      showHex2 x = leftPad 2 '0' $ showHex x ""

leftPad :: Int -> a -> [a] -> [a]
leftPad n x xs
  | n > l = replicate (n - l) x ++ xs
  | otherwise = xs
  where
    l = length xs

instance Toml.FromValue HexColor where
  fromValue = \case
    Toml.Text' l txt ->
      case parseHexColor txt of
        Left errMsg -> Toml.failAt l errMsg
        Right hexColor -> pure hexColor
    _ ->
      fail "Expected string for hex color"

parseHexColor :: Text -> Either String HexColor
parseHexColor rgb
  | Text.length rgb /= 6 = Left "Hex color string is not 6 characters long"
  | not (Text.all isHexDigit rgb) =
      let nonHexDigits = Text.filter (not . isHexDigit) rgb
      in  Left $
            "Hex color string contains character(s) that are not hex digits"
              <> "\""
              <> Text.unpack nonHexDigits
              <> "\""
  | otherwise =
      -- The input is a string of hex digit of length 6
      let (r, gb) = Text.splitAt 2 rgb
          (g, b) = Text.splitAt 2 gb
      in  Right $
            HexColor
              (unsafeReadHexPair r)
              (unsafeReadHexPair g)
              (unsafeReadHexPair b)
  where
    -- Unsafe function for parsing a hex pair
    unsafeReadHexPair :: Text -> Word8
    unsafeReadHexPair =
      fst
        . maybe (error "failed to parse hex pair") fst
        . Data.List.uncons
        . readHex
        . Text.unpack
