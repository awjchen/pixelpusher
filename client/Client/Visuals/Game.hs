{-# LANGUAGE MultiWayIf #-}

-- | This is the top-level module for the game's visuals.
module Client.Visuals.Game (
  renderGameScene,
) where

import Control.Monad (when)
import Control.Monad.Trans.Reader (ReaderT (..), runReaderT)
import Data.Foldable (traverse_)
import GHC.Float (double2Float)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom (both, over)

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.State (GameScene (..))

import Client.Config.Controls
import Client.Config.Keybindings (Keybindings)
import Client.Menu (drawMenu)
import Client.Visuals.Env (GLEnv (..), getFramebufferDims)
import Client.Visuals.GL.Wrapped
import Client.Visuals.Rendering.Cursor (drawCursor)
import Client.Visuals.Rendering.DarkLayer (drawDarkLayer)
import Client.Visuals.Rendering.GameUI.Bottom (drawUIBottom)
import Client.Visuals.Rendering.GameUI.Overlay (EnableRespawnTips (..), drawOverlay)
import Client.Visuals.Rendering.GameUI.Top (drawUITop)
import Client.Visuals.Rendering.GameUI.TutorialInstructions
import Client.Visuals.Rendering.GameUI.TutorialOverlay (
  drawTutorialOverlay,
  shouldShowTutorial,
 )
import Client.Visuals.Rendering.GameWorld (drawGameWorld)
import Client.Visuals.SceneDecorations (
  DisplayHelp (..),
  SceneDecorations (..),
  ShowMenuScene (..),
 )

renderGameScene ::
  GLEnv ->
  GameScene ->
  Float2 ->
  Keybindings ->
  ControlOptions ->
  SceneDecorations ->
  IO ()
renderGameScene glEnv scene cameraPos keybindings controlOpts decorations = do
  (screenSize, aspectRatio) <- getFramebufferDims $ gle_window glEnv

  -- This is different from the framebuffer size
  windowSize <-
    uncurry Float2 . over both fromIntegral
      <$> GLFW.getWindowSize (gle_window glEnv)

  cursorPos <-
    uncurry Float2 . over both double2Float
      <$> GLFW.getCursorPos (gle_window glEnv)

  runGL $ do
    glClearColor 0 0 0 1
    glClear GL_COLOR_BUFFER_BIT
    flip runReaderT (glEnv, screenSize, aspectRatio) $ do
      drawGameWorld scene cameraPos decorations
      drawGameUI scene decorations keybindings controlOpts
      when (sceneDeco_showCursor decorations) $ do
        let showOSCursor = sceneDeco_showOSCursor decorations
            testParams = gp_test (scene_gameParams scene)
        drawCursor testParams showOSCursor windowSize cursorPos scene decorations cameraPos

drawGameUI ::
  GameScene ->
  SceneDecorations ->
  Keybindings ->
  ControlOptions ->
  ReaderT (GLEnv, Int, Float) GL ()
drawGameUI scene decorations keybindings controlOpts = do
  let gameParams = scene_gameParams scene
      shouldShowTutorial' =
        shouldShowTutorial
          gameParams
          (sceneDeco_elaspedClientTime decorations)
          && sceneDeco_showTutorial decorations
  if
    | ShowMenuScene model menuState <- sceneDeco_showMenu decorations -> do
        drawDarkLayer (gp_test gameParams)
        drawMenu [] model menuState
    | shouldShowTutorial' -> do
        drawTutorialOverlay
          gameParams
          controlOpts
          (sceneDeco_elaspedClientTime decorations)
        drawOverlay gameParams DisableRespawnTips scene decorations keybindings controlOpts
    | otherwise -> do
        let showUI = sceneDeco_showUI decorations
        when (not (gp_enableTutorialMode gameParams) && showUI) $
          drawUITop scene decorations

        -- The bottom UI clashes with help text, so disable it when showing help
        case sceneDeco_renderHelpOverlay decorations of
          HideHelp ->
            when showUI $ drawUIBottom gameParams scene decorations keybindings
          ShowHelpInstructions ->
            pure ()
          ShowHelpVisuals ->
            pure ()
          ShowHelpDetailedMechanics ->
            pure ()
          ShowHelpPlayerClasses ->
            pure ()
          ShowHelpThemes ->
            pure ()

        when (gp_enableTutorialMode gameParams) $
          traverse_
            drawTutorialInstruction
            (tutorialPhaseInstruction keybindings controlOpts (scene_tutorialPhase scene))
        let enableRespawnTips =
              if gp_enableTutorialMode gameParams
                then DisableRespawnTips
                else EnableRespawnTips
        drawOverlay gameParams enableRespawnTips scene decorations keybindings controlOpts
