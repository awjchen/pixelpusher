{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.CoordinateSystems (
  CoordSystem,
  cs_axisScaling,
  cs_origin,
  AxisOrientation (PreserveAxis, FlipAxis),
  composeCoordSystem,
  invertCoordSystem,
  composeInvCoordSystem,
  makeCoordSystem,
  intoPos,
  fromPos,
  intoVec,
  fromVec,
  intoScalar,
  fromScalar,
  gameView,
  gameWorld,
  viewport,
  centeredViewport,
) where

import Data.Coerce (coerce)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.Parameters

--------------------------------------------------------------------------------

-- | 2D coordinate systems defined by scaling of each axis and an origin. The
-- axis scaling factors must have the same magnitude but may have different
-- signs. This is enforced by the smart constructor `makeCoordSystem`.
--
-- TODO: Docs: These are affine transformations
data CoordSystem = CoordSystem
  { _cs_scalarScaling :: Float
  , _cs_axisScaling :: {-# UNPACK #-} Float2 -- magnitudes of components are equal to '_cs_scalarScaling'
  , _cs_origin :: {-# UNPACK #-} Float2
  }

cs_axisScaling :: CoordSystem -> Float2
cs_axisScaling = _cs_axisScaling

cs_origin :: CoordSystem -> Float2
cs_origin = _cs_origin

newtype AxisOrientation = AxisOrientation Float

pattern PreserveAxis :: AxisOrientation
pattern PreserveAxis = AxisOrientation 1

pattern FlipAxis :: AxisOrientation
pattern FlipAxis = AxisOrientation (-1)

{-# COMPLETE PreserveAxis, FlipAxis :: AxisOrientation #-}

instance Monoid CoordSystem where
  mempty =
    CoordSystem
      { _cs_scalarScaling = 1
      , _cs_axisScaling = 1
      , _cs_origin = 0
      }

instance Semigroup CoordSystem where
  -- \| Apply the right transform first, followed by the left transform
  (<>) = composeCoordSystem

-- | Apply the right transform first, followed by the left transform
composeCoordSystem :: CoordSystem -> CoordSystem -> CoordSystem
composeCoordSystem cs2 cs1 =
  CoordSystem
    { _cs_scalarScaling = _cs_scalarScaling cs2 * _cs_scalarScaling cs1
    , _cs_axisScaling = _cs_axisScaling cs2 * _cs_axisScaling cs1
    , _cs_origin = _cs_origin cs2 + _cs_axisScaling cs2 * _cs_origin cs1
    }

invertCoordSystem :: CoordSystem -> CoordSystem
invertCoordSystem CoordSystem{..} =
  CoordSystem
    { _cs_scalarScaling = recip _cs_scalarScaling
    , _cs_axisScaling = Float.map2 recip _cs_axisScaling
    , _cs_origin = -_cs_origin / _cs_axisScaling
    }

-- | Apply the right transform first, followed by the inverse of the left
-- transform
composeInvCoordSystem :: CoordSystem -> CoordSystem -> CoordSystem
composeInvCoordSystem cs2 cs1 =
  CoordSystem
    { _cs_scalarScaling = _cs_scalarScaling cs1 / _cs_scalarScaling cs2
    , _cs_axisScaling = _cs_axisScaling cs1 / _cs_axisScaling cs2
    , _cs_origin = (_cs_origin cs1 - _cs_origin cs2) / _cs_axisScaling cs2
    }

makeCoordSystem :: Float -> V2 AxisOrientation -> Float2 -> CoordSystem
makeCoordSystem scale (V2 orientation_x orientation_y) origin =
  CoordSystem
    { _cs_scalarScaling = scale
    , _cs_axisScaling =
        Float2
          (scale * coerce orientation_x)
          (scale * coerce orientation_y)
    , _cs_origin = origin
    }

intoPos :: CoordSystem -> Float2 -> Float2
intoPos cs v = v * _cs_axisScaling cs + _cs_origin cs

fromPos :: CoordSystem -> Float2 -> Float2
fromPos cs v = (v - _cs_origin cs) / _cs_axisScaling cs

intoVec :: CoordSystem -> Float2 -> Float2
intoVec cs = (* _cs_axisScaling cs)

fromVec :: CoordSystem -> Float2 -> Float2
fromVec cs = (/ _cs_axisScaling cs)

intoScalar :: CoordSystem -> Float -> Float
intoScalar cs = (* _cs_scalarScaling cs)

fromScalar :: CoordSystem -> Float -> Float
fromScalar cs = (/ _cs_scalarScaling cs)

--------------------------------------------------------------------------------

gameView :: Float -> Float2 -> CoordSystem
gameView viewRadius = makeCoordSystem viewRadius (V2 PreserveAxis PreserveAxis)

gameWorld :: GameParams -> CoordSystem
gameWorld params =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 0 0)
  where
    radius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics params

viewport :: Int -> CoordSystem
viewport screenSize =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 radius radius)
  where
    radius = 0.5 * fromIntegral screenSize

centeredViewport :: Int -> CoordSystem
centeredViewport screenSize =
  makeCoordSystem radius (V2 PreserveAxis PreserveAxis) (Float2 0 0)
  where
    radius = 0.5 * fromIntegral screenSize
