{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.TeamView (
  TeamView (..),
  TeamColor (TeamRed, TeamBlue),
  teamDefaultColor,
  viewTeamColor,
  viewPlayerColor,
  primaryPlayerColor,
) where

import Data.Word (Word8)

import Pixelpusher.Custom.Float (Float3)
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.Team

import Client.Theme

--------------------------------------------------------------------------------

-- TODO: rename: to something like team color setting
data TeamView
  = AbsoluteTeamView
  | RelativeTeamView Team

newtype TeamColor = TeamColor Word8

{-# COMPLETE TeamRed, TeamBlue :: TeamColor #-}
pattern TeamRed :: TeamColor
pattern TeamRed = TeamColor 0
pattern TeamBlue :: TeamColor
pattern TeamBlue = TeamColor 1

teamRed :: TeamColor
teamRed = TeamColor 0

teamBlue :: TeamColor
teamBlue = TeamColor 1

viewTeamColor :: TeamView -> Team -> TeamColor
viewTeamColor teamView team =
  case teamView of
    AbsoluteTeamView ->
      case team of
        TeamW -> teamRed
        TeamE -> teamBlue
    RelativeTeamView viewTeam -> if team == viewTeam then teamBlue else teamRed

teamDefaultColor :: Theme -> TeamColor -> Float3
teamDefaultColor theme = \case
  TeamRed -> theme_teamRed_teamColor theme
  TeamBlue -> theme_teamBlue_teamColor theme

viewPlayerColor :: Theme -> Maybe ActorID -> TeamView -> PlayerColor -> Float3
viewPlayerColor theme mViewPid = case mViewPid of
  Nothing -> viewPlayerColor' theme
  Just viewPid -> \teamView color@(PlayerColor team _ pid) ->
    if viewPid == pid
      then case teamView of
        AbsoluteTeamView ->
          case team of
            TeamW -> theme_teamRed_playerColorUnique theme
            TeamE -> theme_teamBlue_playerColorUnique theme
        RelativeTeamView viewTeam ->
          if viewTeam == team
            then theme_teamBlue_playerColorUnique theme
            else theme_teamRed_playerColorUnique theme
      else viewPlayerColor' theme teamView color

-- Helper
viewPlayerColor' :: Theme -> TeamView -> PlayerColor -> Float3
viewPlayerColor' theme teamView (PlayerColor team color _) =
  case teamView of
    AbsoluteTeamView ->
      case team of
        TeamW -> teamRedPlayerColors theme color
        TeamE -> teamBluePlayerColors theme color
    RelativeTeamView viewTeam ->
      if viewTeam == team
        then teamBluePlayerColors theme color
        else teamRedPlayerColors theme color

primaryPlayerColor :: Theme -> TeamColor -> Float3
primaryPlayerColor Theme{..} viewTeam =
  case viewTeam of
    TeamRed -> theme_teamRed_playerColorUnique
    TeamBlue -> theme_teamBlue_playerColorUnique

teamBluePlayerColors :: Theme -> ColorVariant -> Float3
teamBluePlayerColors Theme{..} = \case
  Color1 -> theme_teamBlue_playerColor1
  Color2 -> theme_teamBlue_playerColor2
  Color3 -> theme_teamBlue_playerColor3
  Color4 -> theme_teamBlue_playerColor4
  Color5 -> theme_teamBlue_playerColor5
  Color6 -> theme_teamBlue_playerColor6

teamRedPlayerColors :: Theme -> ColorVariant -> Float3
teamRedPlayerColors Theme{..} = \case
  Color1 -> theme_teamRed_playerColor1
  Color2 -> theme_teamRed_playerColor2
  Color3 -> theme_teamRed_playerColor3
  Color4 -> theme_teamRed_playerColor4
  Color5 -> theme_teamRed_playerColor5
  Color6 -> theme_teamRed_playerColor6
