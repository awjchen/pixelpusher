{-# LANGUAGE TemplateHaskell #-}

module Client.Visuals.Assets.Fonts where

import Data.ByteString qualified as BS
import Data.FileEmbed (embedFile)

signikaRegular :: BS.ByteString
signikaRegular = $(embedFile "fonts/signika/Signika-Regular.ttf")
