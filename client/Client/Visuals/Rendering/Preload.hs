module Client.Visuals.Rendering.Preload (
  preloadVisuals,
) where

import Control.Monad.IO.Class (liftIO)
import Data.Foldable (for_)
import Data.Vector.Storable.Sized qualified as VSS
import Graphics.UI.GLFW qualified as GLFW

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerID
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env (GLEnv (GLEnv))
import Client.Visuals.GL.Wrapped (runGL)
import Client.Visuals.Shaders.Background
import Client.Visuals.Shaders.CastRipple
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.ControlPoint
import Client.Visuals.Shaders.CooldownArc
import Client.Visuals.Shaders.CooldownBar
import Client.Visuals.Shaders.Cross
import Client.Visuals.Shaders.CursorAttract
import Client.Visuals.Shaders.CursorRepel
import Client.Visuals.Shaders.Dash
import Client.Visuals.Shaders.DashRing
import Client.Visuals.Shaders.Drone
import Client.Visuals.Shaders.DroneControlRadius
import Client.Visuals.Shaders.Explosion
import Client.Visuals.Shaders.Flag
import Client.Visuals.Shaders.Flare
import Client.Visuals.Shaders.Fog
import Client.Visuals.Shaders.Line
import Client.Visuals.Shaders.MinimapBackground
import Client.Visuals.Shaders.ObstacleRepulsion
import Client.Visuals.Shaders.Overseer
import Client.Visuals.Shaders.OverseerRemains
import Client.Visuals.Shaders.PingFlash
import Client.Visuals.Shaders.PixelFilter
import Client.Visuals.Shaders.PowerupSpawner
import Client.Visuals.Shaders.PsiStorm
import Client.Visuals.Shaders.PsiStormOrb
import Client.Visuals.Shaders.Quad
import Client.Visuals.Shaders.Ring
import Client.Visuals.Shaders.ShieldPowerup
import Client.Visuals.Shaders.SoftObstacle
import Client.Visuals.Shaders.Spark
import Client.Visuals.Shaders.Sparkle
import Client.Visuals.Shaders.TeamBase
import Client.Visuals.Shaders.WorldBounds
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView

-- | Draw all visuals to force them to be loaded onto the GPU. This prevents
-- lazy loading of visuals from causing in-game interruptions.
preloadVisuals :: GLEnv -> IO ()
preloadVisuals
  -- We pattern match on the `GLEnv` constructor so that we don't forget to
  -- use a visual.
  ( GLEnv
      gle_window
      _gle_refreshRate
      gle_overseer
      gle_drone
      gle_softObstacle
      gle_explosion
      gle_controlPoint
      gle_circle
      gle_quad
      gle_droneControlRadius
      gle_overseerRemains
      gle_char
      gle_fontAtlasSmall
      gle_fontAtlasRegular
      gle_fontAtlasLarge
      _gle_uiPadding
      gle_pingFlash
      gle_ring
      gle_spark
      gle_background
      gle_castRipple
      gle_cross
      gle_dash
      gle_dashRing
      gle_cooldownArc
      gle_cooldownBar
      gle_worldBounds
      gle_psiStorm
      gle_flag
      gle_teamBase
      gle_fog
      gle_cursorAttract
      gle_cursorRepel
      _gle_framebufferSizeUpdate
      _gle_keyCallback
      _gle_charCallback
      gle_obstacleRepulsion
      gle_sparkle
      gle_shieldPowerup
      gle_line
      gle_psiStormOrb
      gle_powerupSpawner
      gle_minimapBackground
      gle_flare
      gle_pixelFilter
    ) = do
    -- Parameters
    let cameraPos = Float2 0 0

    screenSize <-
      uncurry min <$> GLFW.getFramebufferSize gle_window
    let gameParams = makeGameParams 0 defaultBaseGameParams
    let time = addTicks (Ticks 60) initialTime
    let gameViewCS = CS.gameView CC.viewRadius cameraPos
    let mViewPlayer = Nothing
        teamView = AbsoluteTeamView
    let teamColor =
          PlayerColor TeamW defaultColorVariant (PlayerActor unsafeDebugPlayerID)
    let centeredViewportCS = CS.centeredViewport screenSize
        aspectRatio = 1
    let theme = defaultTheme

    -- Drawing in the same order as in `GLEnv`

    runGL $ do
      drawOverseer gle_overseer $
        OverseerVParams
          { ovp_aspectRatio = aspectRatio
          , ovp_coordSystem = gameViewCS
          , ovp_testParams = gp_test gameParams
          , ovp_instances =
              pure $
                OverseerInstance
                  { oi_radius = 16
                  , oi_pos = 0
                  , oi_prevPos1 = 0
                  , oi_prevPos2 = 0
                  , oi_prevPos3 = 0
                  , oi_prevPos4 = 0
                  , oi_prevPos5 = 0
                  , oi_prevPos6 = 0
                  , oi_prevPos7 = 0
                  , oi_prevPos8 = 0
                  , oi_prevPos9 = 0
                  , oi_prevPos10 = 0
                  , oi_healthFraction = 0.5
                  , oi_shieldsFraction = 0.5
                  , oi_color = 1
                  , oi_recentDamage = 0.5
                  , oi_isInvulnerable = False
                  , oi_class = Templar
                  , oi_angleOffset = 0
                  , oi_psiStormReadySeconds = -1
                  , oi_angleFins = 0
                  , oi_ticksSinceDash = Nothing
                  , oi_shieldColor = 1
                  }
          }

      drawDrone gle_drone $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                fromDroneRendering mViewPlayer theme teamView time $
                  RenderingComponent
                    { rendering_entityRendering =
                        DroneRendering
                          { dr_color = Right teamColor
                          , dr_healthFraction = 0.5
                          , dr_recentDamage = 0.5
                          , dr_actorID = Nothing
                          , dr_prevPos1 = 0
                          , dr_prevPos2 = 0
                          , dr_prevPos3 = 0
                          , dr_prevPos4 = 0
                          , dr_prevPos5 = 0
                          , dr_prevPos6 = 0
                          , dr_prevPos7 = 0
                          , dr_prevPos8 = 0
                          , dr_prevPos9 = 0
                          , dr_prevPos10 = 0
                          , dr_isInvulnerable = False
                          , dr_entityID = 0
                          , dr_team = TeamW
                          , dr_class = Templar
                          , dr_ticksSinceDash = Nothing
                          , dr_chargeFraction = 0
                          }
                    , rendering_pos = 0
                    , rendering_radius = 16
                    }
          }

      drawSoftObstacle gle_softObstacle $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                SoftObstacleInstance
                  { soi_modelScale = 120
                  , soi_modelTrans = 0
                  , soi_color = 1
                  , soi_maxOpacity = 0.12
                  , soi_minOpacity = 0
                  }
          }

      drawExplosion gle_explosion $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                ExplosionInstance
                  { ei_center = 0
                  , ei_offsetVel = 1
                  , ei_color = 1
                  }
          }

      drawControlPoint gle_controlPoint $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                fromControlPointRendering teamView $
                  RenderingComponent
                    { rendering_entityRendering =
                        ControlPointRendering_Neutral
                    , rendering_pos = 0
                    , rendering_radius = 120
                    }
          }

      drawCircle gle_circle $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CircleInstance
                  { ci_size = 120
                  , ci_center = 0
                  , ci_color = 1
                  , ci_fuzziness = 1
                  }
          }

      -- Not drawing `gle_quad` here, drawing it last instead

      drawDroneControlRadius gle_droneControlRadius $
        DroneControlRadiusVParams
          { dcrvp_aspectRatio = aspectRatio
          , dcrvp_modelTrans = 0
          , dcrvp_modelScale = 300
          , dcrvp_viewScale = recip CC.viewRadius
          , dcrvp_viewCenter = cameraPos
          , dcrvp_color = 1
          }

      drawOverseerRemains gle_overseerRemains $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                fromOverseerRemainsParticle mViewPlayer theme teamView time $
                  Particle
                    { particle_endTime = addTicks (Ticks 120) time
                    , particle_startTime = time
                    , particle_type =
                        OverseerRemainsParticle
                          { orp_color = teamColor
                          , orp_pos = 0
                          , orp_scale = 16
                          }
                    }
          }

      let fontAtlases =
            [ gle_fontAtlasLarge
            , gle_fontAtlasRegular
            , gle_fontAtlasSmall
            ]
      for_ fontAtlases $ \fontAtlas ->
        drawChar gle_char $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlas
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                pure $
                  let screenPos =
                        CS.intoPos (CS.viewport screenSize) $
                          CS.fromPos gameViewCS 0
                  in  (CharAlignCenter, screenPos, [(1, "asdf")])
            }

      drawPingFlash gle_pingFlash $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = centeredViewportCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                PingFlashInstance
                  { pfi_size = 16
                  , pfi_center = 0
                  , pfi_color = 1
                  , pfi_ticksElapsed = 1
                  }
          }

      drawRing gle_ring $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                RingInstance
                  { ri_modelTrans = 0
                  , ri_modelScale = 32
                  , ri_color = 1
                  , ri_thickness = 4
                  , ri_fuzziness = 0.1
                  }
          }

      drawSpark gle_spark $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              fromPsiStormDamageParticle
                mViewPlayer
                theme
                teamView
                time
                Particle
                  { particle_endTime = addTicks (Ticks 120) time
                  , particle_startTime = time
                  , particle_type =
                      PsiStormDamageParticle
                        { psdp_pos = 0
                        , psdp_vel = 0
                        , psdp_mPlayerColor = Nothing
                        , psdp_damage = 1
                        }
                  }
          }

      drawBackground gle_background $
        let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
        in  BackgroundVParams
              { bvp_aspectRatio = aspectRatio
              , bvp_viewScale = recip CC.viewRadius
              , bvp_viewCenter = cameraPos
              , bvp_modelScale = worldRadius * 2
              , bvp_modelTrans = Float2 0 0
              , bvp_worldRadius = worldRadius
              , bvp_backgroundColor = 0
              , bvp_patternColor = 0
              , bvp_scoreSeconds = 0
              , bvp_scorePos = 0
              , bvp_scoreColor = 0
              }

      drawWorldBounds gle_worldBounds $
        let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
        in  WorldBoundsVParams
              { wbvp_aspectRatio = aspectRatio
              , wbvp_viewScale = recip CC.viewRadius
              , wbvp_viewCenter = cameraPos
              , wbvp_modelScale = worldRadius * 2
              , wbvp_modelTrans = Float2 0 0
              , wbvp_color = 0
              }

      drawCastRipple gle_castRipple $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CastRippleInstance
                  { cri_size = 60.0
                  , cri_center = 0
                  , cri_color = 1
                  , cri_ringWidth = 2.0
                  , cri_gradientWidth = 8.0
                  , cri_progress = 0.5
                  }
          }

      drawCross gle_cross $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = centeredViewportCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CrossInstance
                  { ci_size = 16
                  , ci_center = 0
                  , ci_color = 1
                  }
          }

      drawDash gle_dash $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                DashInstance
                  { di_center = 0
                  , di_initialVelocity = 0
                  , di_scale = 1
                  , di_angle = 0
                  , di_ticksElapsed = 0
                  , di_secondsElapsed = 0
                  , di_color = 1
                  }
          }

      drawDashRing gle_dashRing $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                DashRingInstance
                  { dri_center = 0
                  , dri_radius = 1
                  , dri_initialVelocity = 0
                  , dri_direction = 0
                  , dri_ticksElapsed = 0
                  , dri_secondsElapsed = 0
                  , dri_color = 1
                  }
          }

      drawCooldownArc gle_cooldownArc $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CooldownArcInstance
                  { cai_center = 1
                  , cai_arcRadius = 1
                  , cai_angleLow_degrees = 0
                  , cai_angleMid_degrees = 0.5
                  , cai_angleHigh_degrees = 1
                  , cai_colorLow = 0
                  , cai_colorHigh = 1
                  , cai_arcThickness = 1
                  , cai_bloomRadius = 0.5
                  }
          }

      drawCooldownBar gle_cooldownBar $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CooldownBarInstance
                  { cbi_center = 0
                  , cbi_widthRadius = 8
                  , cbi_heightRadius = 1
                  , cbi_bloomRadius = 0.5
                  , cbi_color = 1
                  }
          }

      drawPsiStorm gle_psiStorm $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                PsiStormInstance
                  { psi_modelScale = 144
                  , psi_modelTrans = 0
                  , psi_elapsedSeconds = 0
                  , psi_opacityFactor = 1
                  , psi_stormColor = 1
                  , psi_boundaryOpacity = 1
                  , psi_fillOpacity = 1
                  , psi_bloomColor = 1
                  }
          }

      drawFlag gle_flag $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                FlagInstance
                  { fi_size = 8
                  , fi_center = 0
                  , fi_color = 1
                  }
          }

      drawTeamBase gle_teamBase $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                TeamBaseInstance
                  { tbi_size = 8
                  , tbi_center = 0
                  , tbi_color = 1
                  , tbi_fillOpacity = 1
                  }
          }

      drawFog gle_fog $
        FogVParams
          { fvp_aspectRatio = aspectRatio
          , fvp_viewScale = 1
          , fvp_viewCenter = 0
          , fvp_modelScale = 1
          , fvp_modelTrans = 0
          , fvp_fogRadius = 1
          }

      drawCursorAttract gle_cursorAttract $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CursorAttractInstance
                  { cai_size = 120
                  , cai_center = 0
                  , cai_color = 1
                  }
          }

      drawCursorRepel gle_cursorRepel $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                CursorRepelInstance
                  { cri_size = 120
                  , cri_center = 0
                  , cri_color = 1
                  }
          }

      drawObstacleRepulsion gle_obstacleRepulsion $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                ObstacleRepulsionInstance
                  { ori_modelScale = 8
                  , ori_modelTrans = 0
                  , ori_direction = Float2 1 0
                  , ori_opacity = 1
                  }
          }

      drawSparkle gle_sparkle $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                SparkleInstance
                  { si_size = 1
                  , si_center = 0
                  , si_color = 1
                  , si_bloomBrightness = 1
                  }
          }

      drawShieldPowerup gle_shieldPowerup $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                ShieldPowerupInstance
                  { spi_size = 8
                  , spi_center = 0
                  , spi_color = 1
                  }
          }

      drawLine gle_line $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                LineInstance
                  { li_endpoint1 = Float2 0 0
                  , li_endpoint2 = Float2 0 1
                  , li_color = Float4 1 1 1 1
                  }
          }

      drawPsiStormOrb gle_psiStormOrb $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                PsiStormOrbInstance
                  { psoi_size = 8
                  , psoi_center = 0
                  , psoi_color = 1
                  , psoi_time = 0
                  , psoi_bloomRadiusFactor = 1
                  }
          }

      drawPowerupSpawner gle_powerupSpawner $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                PowerupSpawnerInstance
                  { psi_modelScale = 1
                  , psi_modelTrans = 0
                  , psi_color = 1
                  , psi_boundaryOpacity = 1
                  , psi_outerOpacity = 1
                  , psi_innerOpacity = 0.5
                  , psi_innerRadiusFraction = 0.5
                  }
          }

      drawMinimapBackground gle_minimapBackground $
        MinimapBackgroundVParams
          { mbvp_aspectRatio = aspectRatio
          , mbvp_viewScale = 1
          , mbvp_viewCenter = 0
          , mbvp_visionCenters = VSS.replicate 0
          , mbvp_visionCentersCount = 0
          , mbvp_visionRadius = 1
          , mbvp_fuzziness = 1
          , mbvp_visionColor = 1
          , mbvp_fogColor = 1
          }

      drawFlare gle_flare $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                FlareInstance
                  { fi_center = 0
                  , fi_radius = 1
                  , fi_bloomWidth = 1
                  , fi_color = 1
                  }
          }

      drawPixelFilter gle_pixelFilter $
        PixelFilterVParams
          { pfvp_screenDims = Float2 1920 1080
          , pfvp_fadeFrac = 0.5
          }

      -- Cover up everything
      drawQuad gle_quad $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = CS.centeredViewport screenSize
          , svp_testParams = gp_test gameParams
          , svp_instances =
              pure $
                QuadInstance
                  { qi_size =
                      let l = fromIntegral screenSize
                      in  Float2 l l
                  , qi_center = Float2 0 0
                  , -- Don't use 100% opacity so that the pixels actually have to
                    -- be computed. Not sure if this is actually necessary.
                    qi_color = Float4 0 0 0 0.99
                  }
          }

    liftIO $ GLFW.swapBuffers gle_window
