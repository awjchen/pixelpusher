{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.Rendering.GameWorld (
  drawGameWorld,
) where

import Control.Monad (guard, when)
import Control.Monad.Trans.Reader (ReaderT (..))
import Data.ByteString.Char8 qualified as BS
import Data.Foldable (foldl', for_, toList)
import Data.List qualified as List
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe (catMaybes, fromMaybe, listToMaybe, mapMaybe, maybeToList)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Util (ceilDiv)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Bots.AI (BotDebugState (..), getBotDebugState)
import Pixelpusher.Game.Bots.AI.Pathfinding (PathfindingResult (..))
import Pixelpusher.Game.CastEvents (DashType (..))
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.CollisionEvents (ObstacleOverlap (..))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.FlagComponent (FlagConditionStatus (..))
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.State
import Pixelpusher.Game.Team (Team (..), oppositeTeam)
import Pixelpusher.Game.TeamLocations (teamBaseLocation)
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.DSL (GLKind (..))
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Background
import Client.Visuals.Shaders.CastRipple
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.ControlPoint
import Client.Visuals.Shaders.CooldownArc
import Client.Visuals.Shaders.CooldownBar
import Client.Visuals.Shaders.Cross
import Client.Visuals.Shaders.Dash
import Client.Visuals.Shaders.DashRing
import Client.Visuals.Shaders.Drone
import Client.Visuals.Shaders.DroneControlRadius
import Client.Visuals.Shaders.Explosion
import Client.Visuals.Shaders.Explosion qualified as Explosion
import Client.Visuals.Shaders.Flag
import Client.Visuals.Shaders.Flare
import Client.Visuals.Shaders.Fog
import Client.Visuals.Shaders.Line
import Client.Visuals.Shaders.ObstacleRepulsion
import Client.Visuals.Shaders.Overseer
import Client.Visuals.Shaders.Overseer qualified as Overseer
import Client.Visuals.Shaders.OverseerRemains
import Client.Visuals.Shaders.PixelFilter
import Client.Visuals.Shaders.PowerupSpawner
import Client.Visuals.Shaders.PsiStorm
import Client.Visuals.Shaders.PsiStormOrb
import Client.Visuals.Shaders.Ring
import Client.Visuals.Shaders.ShieldPowerup
import Client.Visuals.Shaders.SoftObstacle
import Client.Visuals.Shaders.Spark
import Client.Visuals.Shaders.Sparkle
import Client.Visuals.Shaders.TeamBase
import Client.Visuals.Shaders.WorldBounds
import Client.Visuals.SharedVisuals
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Text

--------------------------------------------------------------------------------

drawGameWorld ::
  GameScene -> Float2 -> SceneDecorations -> ReaderT (GLEnv, Int, Float) GL ()
drawGameWorld scene rawCameraPos decorations = ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
  -- Unpacking
  let gameParams = scene_gameParams scene
      testParams = gp_test gameParams
  let particles = scene_particles scene
  let time = scene_time scene
  let theme = sceneDeco_theme decorations

  let mPlayerInfo = getPlayerPerspective $ sceneDeco_perspective decorations
      mActorID = pi_actorID <$> mPlayerInfo
      mPlayer = pi_player <$> mPlayerInfo
      mPlayerView = pi_playerView <$> mPlayerInfo
      teamView = sceneDeco_teamView decorations

  let screenSize' = fromIntegral screenSize

  -- Screenshake
  let screenshakeOffset =
        case mActorID of
          Nothing -> 0
          Just actorID ->
            let myScreenshakeParticles =
                  filter
                    ((== actorID) . sp_actorID . particle_type)
                    (particles_screenshake particles)
                vectors = flip map myScreenshakeParticles $ \p ->
                  let ScreenshakeParticle{..} = particle_type p
                      t =
                        fromIntegral (time `diffTime` particle_startTime p)
                          / fromIntegral C.tickRate_hz
                      maxDamage = 32 -- roughly overseer headbutt damage
                      hitStrengthFrac =
                        min (Fixed.toFloat sp_damage) maxDamage / maxDamage
                      magnitude0 =
                        let smallMag0 = 4
                            largeMag0 = 8
                        in  smallMag0
                              + hitStrengthFrac * (largeMag0 - smallMag0)
                      decayRate =
                        let decayRateFast = 20
                            decayRateSlow = 5
                        in  decayRateFast
                              + hitStrengthFrac * (decayRateSlow - decayRateFast)
                      freq = 2 * pi * 17.13 -- period of about 4 four frames at 60 FPS, and not close to an integer multiple
                      magnitude =
                        magnitude0 * exp (-decayRate * t) * sin (freq * t)
                  in  Float.map2 (* magnitude) $
                        Float.normalize2 $
                          Fixed.toFloats sp_direction
            in  foldl' (+) 0 vectors

  -- Camera
  let cameraPos =
        let regularCamera =
              if sceneDeco_enableScreenShake decorations
                then rawCameraPos + screenshakeOffset
                else rawCameraPos
        in  case sceneDeco_perspective decorations of
              FreePerspective -> regularCamera
              PlayerPerspective{} -> regularCamera
              GlobalPerspective -> Float2 0 0 -- world center
  let viewRadius =
        case sceneDeco_perspective decorations of
          FreePerspective -> CC.viewRadius
          PlayerPerspective{} -> CC.viewRadius
          GlobalPerspective ->
            Fixed.toFloat $ gp_worldRadius (gp_dynamics gameParams)

  -- Parameters
  let gameViewCS = CS.gameView viewRadius cameraPos
  let players = fst <$> scene_players scene
  let _mOverseerView = pv_overseerView <$> mPlayerView
      overseerPos =
        case sceneDeco_perspective decorations of
          FreePerspective -> cameraPos
          GlobalPerspective -> cameraPos
          PlayerPerspective playerInfo ->
            case pv_overseerView (pi_playerView playerInfo) of
              OV_Alive OverseerViewAlive{ova_pos} ->
                Fixed.toFloats ova_pos
              OV_Dead OverseerViewDead{ovd_lastPos} ->
                Fixed.toFloats ovd_lastPos
      renderingComps = scene_renderingComps scene

  -- Background
  drawBackground (gle_background glEnv) $
    let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
        (scoreSeconds, scorePos, scoreColor)
          | sceneDeco_showFlagCaptureFlash decorations =
              -- Assuming that there is at most one flag capture particle at a time
              case listToMaybe (particles_flagExplode particles) of
                Nothing ->
                  (-1, 0, 0)
                Just p ->
                  let FlagExplodeParticle{..} = particle_type p
                  in  ( fromIntegral (2 + time `diffTime` particle_startTime p)
                          / fromIntegral C.tickRate_hz
                      , Fixed.toFloats fep_pos
                      , let carrierTeam = oppositeTeam fep_flagTeam
                            brightnessFactor =
                              if gp_enableTutorialMode gameParams
                                then 0.35
                                else 1.00
                        in  Float.map3 (* brightnessFactor) $
                              teamDefaultColor
                                theme
                                (viewTeamColor teamView carrierTeam)
                      )
          | otherwise = noFlash
          where
            noFlash = (-1, 0, 0)
    in  BackgroundVParams
          { bvp_aspectRatio = aspectRatio
          , bvp_viewScale = recip viewRadius
          , bvp_viewCenter = cameraPos
          , bvp_modelScale = worldRadius * 2
          , bvp_modelTrans = Float2 0 0
          , bvp_worldRadius = worldRadius
          , bvp_backgroundColor = theme_backgroundColor theme
          , bvp_patternColor = theme_backgroundPatternColor theme
          , bvp_scoreSeconds = scoreSeconds
          , bvp_scorePos = scorePos
          , bvp_scoreColor = scoreColor
          }

  -- Background-dulling shadows
  drawCircle (gle_circle glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          let backgroundColor = theme_backgroundColor theme
              obstacleShadows =
                flip map (rcs_softObstacles renderingComps) $ \rc ->
                  CircleInstance
                    { ci_size = rendering_radius rc
                    , ci_center = rendering_pos rc
                    , ci_color = Util.fromRGB_A backgroundColor 0.85
                    , ci_fuzziness = 1
                    }
              psiStormShadows =
                flip map (rcs_psiStorms renderingComps) $ \rc ->
                  let psiStorm = rendering_entityRendering rc
                      endTime = psr_endTime psiStorm
                  in  CircleInstance
                        { ci_size = rendering_radius rc + 4 -- approximately the bloom width
                        , ci_center = rendering_pos rc
                        , ci_color =
                            let opacityFactor' =
                                  psiStormOpacityFactor gameParams (scene_time scene) endTime
                                baseOpacity = 0.25 -- subtle so that it doesn't distract the eye when it fades away
                            in  Util.fromRGB_A backgroundColor (baseOpacity * opacityFactor')
                        , ci_fuzziness = 4
                        }
              teamBaseShadows =
                flip map (rcs_teamBases renderingComps) $ \rc ->
                  CircleInstance
                    { ci_size = rendering_radius rc + 1
                    , ci_center = rendering_pos rc
                    , ci_color = Util.fromRGB_A backgroundColor 0.50
                    , ci_fuzziness = 2
                    }
              spawnAreaShadows =
                flip map (rcs_spawnAreas renderingComps) $ \rc ->
                  CircleInstance
                    { ci_size = rendering_radius rc + 1
                    , ci_center = rendering_pos rc
                    , ci_color = Util.fromRGB_A backgroundColor 0.35
                    , ci_fuzziness = 2
                    }
              shieldPowerupSpawnerShadows =
                flip map (mapPowerupSpawnLocations (gp_dynamics gameParams)) $
                  \(_, pos) ->
                    CircleInstance
                      { ci_size = shieldPowerupSpawnerRadius + 1
                      , ci_center = Fixed.toFloats pos
                      , ci_color = Util.fromRGB_A backgroundColor 0.35
                      , ci_fuzziness = 2
                      }
          in  concat
                [ teamBaseShadows
                , spawnAreaShadows
                , shieldPowerupSpawnerShadows
                , obstacleShadows
                , psiStormShadows
                ]
      }

  -- Debug marks
  when (sceneDeco_showUI decorations) $
    drawCross (gle_cross glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            let marks = scene_debugMarks scene
                makeCrossInstance color pos =
                  CrossInstance
                    { ci_size = 16
                    , ci_center = Fixed.toFloats pos
                    , ci_color = color
                    }
            in  catMaybes
                  [ makeCrossInstance markColor1 <$> dm_mark1 marks
                  , makeCrossInstance markColor2 <$> dm_mark2 marks
                  , makeCrossInstance markColor3 <$> dm_mark3 marks
                  , makeCrossInstance markColor4 <$> dm_mark4 marks
                  , makeCrossInstance markColor5 <$> dm_mark5 marks
                  , makeCrossInstance markColor6 <$> dm_mark6 marks
                  ]
        }

  -- Overseer remains
  drawOverseerRemains (gle_overseerRemains glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          map (fromOverseerRemainsParticle mActorID theme teamView time) $
            particles_overseerRemains particles
      }

  -- Shield powerup spawners
  when (gp_enableShieldPowerupSpawners gameParams) $
    drawPowerupSpawner (gle_powerupSpawner glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            flip map (mapPowerupSpawnLocations (gp_dynamics gameParams)) $ \(powerupLoc, pos) ->
              let periodTicks = gp_shieldPowerupRespawnPeriod gameParams
                  mapPowerups = scene_mapPowerups scene
                  (progressTicks, maybeSpawnTicks) =
                    case readMapPowerupStatus powerupLoc mapPowerups of
                      MapPowerupSpawned spawnTime ->
                        (0, Just (time `diffTime` spawnTime))
                      MapPowerupConsumed consumedTime ->
                        (time `diffTime` consumedTime, Nothing)
                  tickRate_hz' = fromIntegral C.tickRate_hz
                  progress = fromIntegral progressTicks / fromIntegral periodTicks
                  flash =
                    case maybeSpawnTicks of
                      Nothing -> 0
                      Just spawnTicks ->
                        let spawnSeconds = fromIntegral spawnTicks / tickRate_hz'
                        in  exp (-spawnSeconds / 0.075)
              in  PowerupSpawnerInstance
                    { psi_modelScale = shieldPowerupSpawnerRadius
                    , psi_modelTrans = Fixed.toFloats pos
                    , psi_color = theme_shieldPowerupSpawnerColor theme
                    , psi_boundaryOpacity =
                        theme_shieldPowerupSpawnerBoundaryOpacity theme
                    , psi_outerOpacity =
                        theme_shieldPowerupSpawnerFillOpacity theme
                    , psi_innerOpacity =
                        flash * theme_shieldPowerupSpawnerFlashOpacity theme
                    , psi_innerRadiusFraction = sqrt (1 - progress)
                    }
        }

  -- Soft obstacles
  drawSoftObstacle (gle_softObstacle glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances = flip map (rcs_softObstacles renderingComps) $ \rc ->
          SoftObstacleInstance
            { soi_modelScale = rendering_radius rc
            , soi_modelTrans = rendering_pos rc
            , soi_color = theme_obstacleColor theme
            , soi_maxOpacity = theme_obstacleOpacity theme
            , soi_minOpacity = 0
            }
      }

  -- Soft obstacle repulsion effect
  drawObstacleRepulsion (gle_obstacleRepulsion glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances = flip map (scene_obstacleOverlaps scene) $ \ObstacleOverlap{..} ->
          let entityRadius = Fixed.toFloat oo_entityRadius
              entityPosition = Fixed.toFloats oo_entityPosition
              entityMass = Fixed.toFloat oo_entityMass
              repulsionDirection = Fixed.toFloats oo_repulsionDirection
              repulsionAccel = Fixed.toFloat oo_repulsionAccel
          in  ObstacleRepulsionInstance
                { ori_modelScale = entityRadius
                , ori_modelTrans =
                    entityPosition
                      + Float.map2
                        (* (-0.9 * entityRadius))
                        repulsionDirection
                , ori_direction = repulsionDirection
                , ori_opacity =
                    let massPerMaxHealth = Fixed.toFloat C.massPerMaxHealth
                    in  min 1 $
                          repulsionAccel
                            * entityMass
                            / (1.5 * entityRadius * massPerMaxHealth)
                }
      }

  -- Team bases and spawn areas
  drawTeamBase (gle_teamBase glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          let teamBases =
                flip map (rcs_teamBases renderingComps) $ \rc ->
                  TeamBaseInstance
                    { tbi_size = rendering_radius rc
                    , tbi_center = rendering_pos rc
                    , tbi_color =
                        let team = tbr_team (rendering_entityRendering rc)
                        in  case viewTeamColor teamView team of
                              TeamRed -> theme_teamRed_baseColor theme
                              TeamBlue -> theme_teamBlue_baseColor theme
                    , tbi_fillOpacity = 1
                    }
              spawnAreas =
                flip map (rcs_spawnAreas renderingComps) $ \rc ->
                  TeamBaseInstance
                    { tbi_size = rendering_radius rc
                    , tbi_center = rendering_pos rc
                    , tbi_color =
                        let team = tsr_team (rendering_entityRendering rc)
                        in  case viewTeamColor teamView team of
                              TeamRed -> theme_teamRed_spawnColor theme
                              TeamBlue -> theme_teamBlue_spawnColor theme
                    , tbi_fillOpacity = 0
                    }
          in  spawnAreas ++ teamBases
      }

  -- Shield powerups
  drawShieldPowerup (gle_shieldPowerup glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          let liveShieldPowerups =
                flip map (rcs_shieldPowerups renderingComps) $ \rc ->
                  ShieldPowerupInstance
                    { spi_size = rendering_radius rc
                    , spi_center = rendering_pos rc
                    , spi_color =
                        let powerup = rendering_entityRendering rc
                            expiryTimeMaybe = spr_expiryTime powerup
                            opacityFactor =
                              case expiryTimeMaybe of
                                Nothing -> 1
                                Just expiryTime ->
                                  let secondsLeft =
                                        fromIntegral (expiryTime `diffTime` time)
                                          / fromIntegral C.tickRate_hz
                                      fadeProgress =
                                        max 0 $ min 1 $ secondsLeft / 2
                                      x = 1 - fadeProgress
                                  in  1 - x * x
                            opacity = 0.85 * opacityFactor
                        in  Util.fromRGB_A
                              (theme_shieldPowerupColor theme)
                              opacity
                    }
              particleShieldPowerups =
                flip map (particles_shieldPickup particles) $ \particle ->
                  let ShieldPickupParticle{..} = particle_type particle
                      ticks = time `diffTime` particle_startTime particle
                      seconds = fromIntegral ticks / fromIntegral C.tickRate_hz
                      opacity = exp (-seconds / 0.05)
                  in  ShieldPowerupInstance
                        { spi_size = Fixed.toFloat spp_radius
                        , spi_center = Fixed.toFloats spp_pos
                        , spi_color =
                            Util.fromRGB_A
                              (theme_shieldPowerupColor theme)
                              opacity
                        }
          in  liveShieldPowerups ++ particleShieldPowerups
      }

  -- Team base flag-capture hint text
  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_aspectRatio = aspectRatio
      , cvp_fontAtlas = gle_fontAtlasSmall glEnv
      , cvp_screenSize = fromIntegral screenSize
      , cvp_lines =
          flip mapMaybe (rcs_teamBases renderingComps) $ \rc ->
            let flagConditionStatus =
                  tbr_flagDepositPending (rendering_entityRendering rc)
                opacity = hintOpacityFromFlagCondition time flagConditionStatus
                screenPos =
                  CS.intoPos (CS.viewport screenSize) $
                    CS.fromPos gameViewCS $
                      rendering_pos rc & _2 +~ 10 - 0.5 * (1 - opacity) -- adjusted by hand
                color = Float4 0.5 0.5 0.5 opacity
                msg = "can't score while flag is away"
            in  Just (CharAlignCenter, screenPos, [(color, msg)])
      }

  -- World bounds
  drawWorldBounds (gle_worldBounds glEnv) $
    let worldRadius = Fixed.toFloat $ gp_worldRadius $ gp_dynamics gameParams
    in  WorldBoundsVParams
          { wbvp_aspectRatio = aspectRatio
          , wbvp_viewScale = recip viewRadius
          , wbvp_viewCenter = cameraPos
          , wbvp_modelScale = worldRadius
          , wbvp_modelTrans = Float2 0 0
          , wbvp_color = theme_worldBoundaryColor theme
          }

  -- Placeholders
  drawCircle (gle_circle glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances = flip map (rcs_placeholders renderingComps) $ \rc ->
          CircleInstance
            { ci_size = rendering_radius rc
            , ci_center = rendering_pos rc
            , ci_color = unPlaceholderRendering $ rendering_entityRendering rc
            , ci_fuzziness = 1
            }
      }

  -- Flag recovery areas
  drawControlPoint (gle_controlPoint glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          map
            (fromFlagCaptureAreaRendering (gp_dynamics gameParams) theme teamView)
            (rcs_flagCaptureAreas renderingComps)
      }

  -- Flags
  drawFlag (gle_flag glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          let flagInstance :: Float -> Float2 -> Team -> FlagInstance Cpu
              flagInstance radius pos team =
                FlagInstance
                  { fi_size = radius
                  , fi_center = pos
                  , fi_color =
                      flip Util.fromRGB_A 1 $
                        flagColors theme $
                          viewTeamColor teamView team
                  }
              fromFlag :: RenderingComponent FlagRendering -> (Float, Float2, Team)
              fromFlag rc =
                ( rendering_radius rc
                , rendering_pos rc
                , fr_team $ rendering_entityRendering rc
                )
              fromBaseFlag :: RenderingComponent BaseFlagRendering -> (Float, Float2, Team)
              fromBaseFlag rc =
                ( rendering_radius rc
                , rendering_pos rc
                , unBaseFlagRendering $ rendering_entityRendering rc
                )
              flags =
                map (uncurry3 flagInstance) $
                  map fromBaseFlag (rcs_baseFlags renderingComps)
                    ++ map fromFlag (rcs_flags renderingComps)

              flagParticleInstance :: Particle FlagFadeParticle -> FlagInstance Cpu
              flagParticleInstance particle =
                let pos = case particle_type particle of
                      FlagFade_Recover FlagRecoverParticle{frp_pos} ->
                        frp_pos
                      FlagFade_Pickup FlagPickupParticle{fpp_flagPos} ->
                        fpp_flagPos
                      FlagFade_Drop FlagDropParticle{fdp_pos} ->
                        fdp_pos
                      FlagFade_Respawn team ->
                        teamBaseLocation (gp_dynamics gameParams) team
                in  FlagInstance
                      { fi_size = Fixed.toFloat $ gp_flagRadius $ gp_dynamics gameParams
                      , fi_center = Fixed.toFloats pos
                      , fi_color =
                          let ticks =
                                time `diffTime` particle_startTime particle
                              seconds =
                                fromIntegral ticks / fromIntegral C.tickRate_hz
                              opacity = exp (-seconds / 0.05)
                          in  Float4 1 1 1 opacity
                      }
              flagParticles =
                map flagParticleInstance $ particles_flagFade particles
          in  flags ++ flagParticles -- particles on top
      }

  -- Flag flag-recovery hint text
  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_aspectRatio = aspectRatio
      , cvp_fontAtlas = gle_fontAtlasSmall glEnv
      , cvp_screenSize = fromIntegral screenSize
      , cvp_lines =
          flip mapMaybe (rcs_flagCaptureAreas renderingComps) $ \rc ->
            let flagConditionStatus =
                  fcar_recoveringFlag (rendering_entityRendering rc)
                opacity = hintOpacityFromFlagCondition time flagConditionStatus
                screenPos =
                  CS.intoPos (CS.viewport screenSize) $
                    CS.fromPos gameViewCS $
                      rendering_pos rc & _2 +~ 18 - 0.5 * (1 - opacity) -- adjusted by hand
                color = Float4 0.5 0.5 0.5 opacity
                msg = "recovering flag ..."
            in  Just (CharAlignCenter, screenPos, [(color, msg)])
      }

  -- Overseers
  drawOverseer (gle_overseer glEnv) $
    OverseerVParams
      { ovp_aspectRatio = aspectRatio
      , ovp_coordSystem = gameViewCS
      , ovp_testParams = testParams
      , ovp_instances =
          let overseers =
                flip map (rcs_overseers renderingComps) $ \rc ->
                  let actorID = or_actorID $ rendering_entityRendering rc
                      cursorPos =
                        maybe
                          0
                          (Fixed.toFloats . control_worldPos . player_controls)
                          (WIM.lookup actorID players)
                      Float2 x y = cursorPos - rendering_pos rc
                      finsAngle = -atan2 y x
                  in  fromOverseerRendering
                        time
                        theme
                        teamView
                        mActorID
                        finsAngle
                        rc
              deadOverseers =
                flip map (particles_overseerDeath particles) $ \particle ->
                  let ov = particle_type particle
                      actorID = odp_actorID ov
                      cursorPos =
                        maybe
                          0
                          (Fixed.toFloats . control_worldPos . player_controls)
                          (WIM.lookup actorID players)
                      Float2 x y = cursorPos - Fixed.toFloats (odp_pos ov)
                      finsAngle = -atan2 y x
                  in  Overseer.fromOverseerDeathParticle
                        time
                        finsAngle
                        particle
          in  deadOverseers ++ overseers
      }

  -- Drones
  do
    let getDashFullChargeSeconds :: PlayerControlState -> Float
        getDashFullChargeSeconds controlState =
          fromMaybe (-1) $ do
            guard $ gp_enableDroneDash $ gp_cast gameParams
            guard $ pcs_lastDroneCommand controlState == DroneNeutral
            pure $
              let chargingTicks =
                    time `diffTime` pcs_lastDroneCommandInitialTime controlState
                  fullChargeTicks =
                    chargingTicks
                      - gp_maxDroneDashChargeTicks (gp_cast gameParams)
              in  fromIntegral fullChargeTicks / fromIntegral C.tickRate_hz
    let mViewPlayerDashFullCharge =
          mPlayerInfo <&> \PlayerInfo{pi_actorID, pi_playerView} ->
            (pi_actorID, getDashFullChargeSeconds (pv_controlState pi_playerView))
    drawDrone (gle_drone glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            flip map (rcs_drones renderingComps) $
              fromDroneRendering mViewPlayerDashFullCharge theme teamView time
        }

  -- Cooldown bars, arcs
  when (sceneDeco_showHUD decorations) $ do
    let cooldownTicksToAng cooldownTicks =
          (75 / 10) * fromIntegral cooldownTicks / fromIntegral C.tickRate_hz
        cooldownTicksToDist cooldownTicks =
          4.5 * fromIntegral cooldownTicks / fromIntegral C.tickRate_hz
    for_ mPlayer $ \player ->
      for_ (rcs_overseers renderingComps) $ \rc -> do
        let actorID = view player_actorID player
            overseer = rendering_entityRendering rc
            playerClass = or_playerClass overseer
            teamColor = viewTeamColor teamView (or_team overseer)

        when (actorID == or_actorID overseer) $ do
          let makeCooldownArc
                angleLow
                angleMid
                angleHigh
                baseColor
                baseOpacity
                cooldownComplete =
                  CooldownArcInstance
                    { cai_center = rendering_pos rc
                    , cai_arcRadius = rendering_radius rc * 2.1
                    , cai_angleLow_degrees = angleLow
                    , cai_angleMid_degrees = angleMid
                    , cai_angleHigh_degrees = angleHigh
                    , cai_colorLow = Util.fromRGB_A baseColor baseOpacity
                    , cai_colorHigh = Float4 1 1 1 (0.35 * baseOpacity)
                    , cai_arcThickness = if cooldownComplete then 1.4 else 0.7
                    , cai_bloomRadius = 0.35
                    }
          let psiStormInstance =
                let psiStormControl =
                      view control_psiStorm (control_buttons (player_controls player))
                    remainingCooldownTicks =
                      or_psiStormCooldownUntil overseer `diffTime` time
                    cooldownComplete = remainingCooldownTicks <= 0
                    baseColor =
                      case teamColor of
                        TeamRed -> theme_teamRed_abilityCooldownBarColor theme
                        TeamBlue -> theme_teamBlue_abilityCooldownBarColor theme
                    baseOpacity =
                      case psiStormControl of
                        PsiStormOn -> 1
                        PsiStormOff -> 0.5
                    maxCooldown =
                      gp_psiStormCastCooldownTicks (gp_cast gameParams)
                    frac =
                      1
                        - fromIntegral remainingCooldownTicks
                          / fromIntegral maxCooldown
                    angleRadius = 0.5 * cooldownTicksToAng maxCooldown
                    angleMean = 180
                    angleLow = angleMean + angleRadius
                    angleHigh = angleMean - angleRadius
                    angleMid = angleLow + frac * (angleHigh - angleLow)
                in  makeCooldownArc
                      angleLow
                      angleMid
                      angleHigh
                      baseColor
                      baseOpacity
                      cooldownComplete
          let dashInstances =
                let (fullCharges, partialChargeTicks) =
                      getCurrentStamina
                        (gp_cast gameParams)
                        playerClass
                        time
                        (or_dashStamina overseer)
                    totalCharges =
                      getClassParam
                        playerClass
                        (gp_overseerDashMaxCharges (gp_cast gameParams))

                    maxCooldown =
                      getClassParam
                        playerClass
                        (gp_overseerDashCooldownTicks (gp_cast gameParams))

                    spacerLengthAngle =
                      0.5 * cooldownTicksToAng C.tickRate_hz -- arbitrary
                    chargeLengthAngle = cooldownTicksToAng maxCooldown
                    stepLengthAngle = chargeLengthAngle + spacerLengthAngle

                    totalLengthAngle =
                      let totalCharges' = fromIntegral totalCharges
                      in  totalCharges' * chargeLengthAngle
                            + (totalCharges' - 1) * spacerLengthAngle
                    angle0 = -0.5 * totalLengthAngle

                    baseColor =
                      case teamColor of
                        TeamRed -> theme_teamRed_dashCooldownBarColor theme
                        TeamBlue -> theme_teamBlue_dashCooldownBarColor theme
                    droneControl =
                      view
                        control_overseerDash
                        (control_buttons (player_controls player))
                    baseOpacity =
                      let fullOpacity = 1.0
                          regularOpacity = 0.5
                      in  if not (isDashReset (or_dashStamina overseer))
                            then regularOpacity
                            else case droneControl of
                              OverseerDashOn -> fullOpacity
                              OverseerDashOff -> regularOpacity

                    fullArcs =
                      flip map [0 .. fullCharges - 1] $ \i ->
                        let angleLow =
                              angle0 + fromIntegral i * stepLengthAngle
                            angleHigh = angleLow + chargeLengthAngle
                            cooldownComplete = True
                        in  makeCooldownArc
                              angleLow
                              angleHigh
                              angleHigh
                              baseColor
                              baseOpacity
                              cooldownComplete

                    partialArcMaybe =
                      if fullCharges == totalCharges
                        then Nothing
                        else
                          Just $
                            let angleLow =
                                  angle0
                                    + fromIntegral fullCharges * stepLengthAngle
                                angleHigh = angleLow + chargeLengthAngle
                                frac =
                                  fromIntegral partialChargeTicks
                                    / fromIntegral maxCooldown
                                angleMid = angleLow + frac * (angleHigh - angleLow)
                                cooldownComplete = False
                            in  makeCooldownArc
                                  angleLow
                                  angleMid
                                  angleHigh
                                  baseColor
                                  baseOpacity
                                  cooldownComplete

                    emptyArcs =
                      flip map [fullCharges + 1 .. totalCharges - 1] $ \i ->
                        let angleLow =
                              angle0 + fromIntegral i * stepLengthAngle
                            angleHigh = angleLow + chargeLengthAngle
                            cooldownComplete = False
                        in  makeCooldownArc
                              angleLow
                              angleLow
                              angleHigh
                              baseColor
                              baseOpacity
                              cooldownComplete
                in  fullArcs ++ maybeToList partialArcMaybe ++ emptyArcs
          let instances =
                case or_playerClass overseer of
                  Templar -> psiStormInstance : dashInstances
                  Zealot -> dashInstances
          -- Arc shadows
          drawCooldownArc (gle_cooldownArc glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  flip map instances $
                    \CooldownArcInstance{..} ->
                      let black = Float4 0 0 0 0.25
                      in  CooldownArcInstance
                            { cai_colorLow = black
                            , cai_colorHigh = black
                            , cai_bloomRadius = cai_bloomRadius + 0.5
                            , ..
                            }
              }

          -- The arcs themselves
          withBlendFunc GL_SRC_ALPHA GL_ONE $
            drawCooldownArc (gle_cooldownArc glEnv) $
              StandardVParams
                { svp_aspectRatio = aspectRatio
                , svp_coordSystem = gameViewCS
                , svp_testParams = testParams
                , svp_instances = instances
                }

          -- Drone respawn
          let droneRespawnInstance x_offset =
                CooldownBarInstance
                  { cbi_center =
                      rendering_pos rc
                        + Float2 x_offset (-1.2 * rendering_radius rc - 8) -- spacing
                  , cbi_widthRadius = 0.75
                  , cbi_heightRadius = 1.125
                  , cbi_bloomRadius = 0.5
                  , cbi_color =
                      case teamColor of
                        TeamRed -> Float4 0.587 0.480 0.00 0.70
                        TeamBlue -> Float4 0.00 0.567 0.00 0.70
                  }
          drawCooldownBar (gle_cooldownBar glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  flip concatMap (or_droneRespawnCooldowns overseer) $ \cooldownTicks ->
                    let offset = cooldownTicksToDist cooldownTicks
                    in  [ droneRespawnInstance offset
                        , droneRespawnInstance (-offset)
                        ]
              }

  -- Player tags
  when (sceneDeco_showPlayerTags decorations) $
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = gle_fontAtlasSmall glEnv
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines = flip map (rcs_overseers renderingComps) $ \rc ->
            let OverseerRendering{or_team, or_playerName, or_playerClass} =
                  rendering_entityRendering rc
                screenPos =
                  CS.intoPos (CS.viewport screenSize) $
                    CS.fromPos gameViewCS $
                      rendering_pos rc
                        & _2
                        +~ rendering_radius rc
                        + 10 -- adjusted by hand to avoid psi-storm orb
                color = case viewTeamColor teamView or_team of
                  TeamRed -> Float4 1 0.5 0.5 1
                  TeamBlue -> Float4 0.5 0.75 1.0 1
                nameStr = BS.unpack $ getPlayerName or_playerName
                -- Putting the class in the tag
                classStr =
                  case or_playerClass of
                    Templar -> "T"
                    Zealot -> "Z"
                tagStr = nameStr ++ " : " ++ classStr
            in  (CharAlignCenter, screenPos, [(color, tagStr)])
        }

  -- Overseer flag indicators
  drawFlag (gle_flag glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          flip mapMaybe (rcs_overseers renderingComps) $ \rc ->
            let ov = rendering_entityRendering rc
                elapsedTicks = time `diffTime` or_flagPickupDropTime ov
                flashOpacity =
                  let seconds =
                        fromIntegral elapsedTicks / fromIntegral C.tickRate_hz
                  in  exp (-seconds / 0.05)
                flagTeam = oppositeTeam (or_team ov)
                baseOpacity =
                  (1 - flashOpacity) * if or_hasFlag ov then 1 else 0
                finalOpacity = flashOpacity + baseOpacity
            in  if finalOpacity < 1e-3
                  then Nothing
                  else
                    Just $
                      let baseColor =
                            if or_hasFlag ov
                              then flagColors theme (viewTeamColor teamView flagTeam)
                              else Float3 0 0 0
                          white = Float3 1 1 1
                          finalColor =
                            Float.map3 (* (flashOpacity / finalOpacity)) white
                              + Float.map3 (* (baseOpacity / finalOpacity)) baseColor
                      in  FlagInstance
                            { fi_size = 0.35 * rendering_radius rc
                            , fi_center = rendering_pos rc
                            , fi_color = Util.fromRGB_A finalColor finalOpacity
                            }
      }

  -- Overseer psi-storm orbs
  do
    let (orbs, sparkles, orbShadowsMaybe) =
          unzip3 $ flip mapMaybe (rcs_overseers renderingComps) $ \rc -> do
            let OverseerRendering{..} = rendering_entityRendering rc
            guard $
              not $
                isActorBot or_actorID
                  && not (gp_botAllowPsiStorm (gp_bots gameParams))
            guard $ or_playerClass == Templar
            pure $
              let pos =
                    let period = 10 * C.tickRate_hz
                        t = getTime time `mod` period
                        eid = fromIntegral or_entityID
                        ang = 2 * pi * fromIntegral t / fromIntegral period + eid
                        sign = case or_team of TeamW -> 1; TeamE -> -1
                        r = rendering_radius rc + 1.5 * psiStormOrbRadius
                        offset = Float.map2 (* r) $ Float.angle (sign * ang)
                    in  rendering_pos rc + offset
                  baseColor =
                    let invulnerabilityFilter' =
                          if or_isInvulnerable
                            then Float.map3 invulnerabilityFilter
                            else id
                    in  invulnerabilityFilter' $
                          viewPlayerColor theme mActorID teamView or_color
                  castTime =
                    let cooldownTicks =
                          gp_psiStormCastCooldownTicks (gp_cast gameParams)
                    in  addTicks (-cooldownTicks) or_psiStormCooldownUntil
                  ticksSinceCast =
                    fromIntegral $ 2 + time `diffTime` castTime
                  ticksSinceReady =
                    fromIntegral $
                      time `diffTime` or_psiStormCooldownUntil
                  flashEnvelope =
                    max 0 $ 1 - ticksSinceCast / fromIntegral flareLifetimeTicks
                  fadeInEnveleope =
                    let fadeInTicks = fromIntegral C.tickRate_hz / 3
                    in  max 0 . min 1 $ 1 - ticksSinceReady / fadeInTicks
                  orb =
                    PsiStormOrbInstance
                      { psoi_size = psiStormOrbRadius
                      , psoi_center = pos
                      , psoi_color =
                          let opacity
                                | time >= or_psiStormCooldownUntil =
                                    let w = 0.85
                                    in  w + (1 - w) * fadeInEnveleope
                                | otherwise = flashEnvelope
                              whiteness
                                | time >= or_psiStormCooldownUntil =
                                    fadeInEnveleope
                                | otherwise =
                                    flashEnvelope
                          in  flip Util.fromRGB_A opacity $
                                Float.map3
                                  (\v -> whiteness + (1 - whiteness) * v)
                                  baseColor
                      , psoi_time =
                          fromIntegral
                            (time `diffTime` scene_roundStart scene)
                            / fromIntegral C.tickRate_hz
                      , psoi_bloomRadiusFactor =
                          recip $ 1 + 6 * fadeInEnveleope
                      }
                  sparkle =
                    SparkleInstance
                      { si_size = 64 * flashEnvelope
                      , si_center = pos
                      , si_color =
                          flip Util.fromRGB_A flashEnvelope $
                            let w = 0.5
                            in  Float.map3
                                  (\v -> w + (1 - w) * v)
                                  baseColor
                      , si_bloomBrightness = 1
                      }
                  orbShadowMaybe =
                    if time < or_psiStormCooldownUntil
                      then Nothing
                      else
                        Just
                          CircleInstance
                            { ci_size = psiStormOrbRadius + 1.5
                            , ci_center = pos
                            , ci_color =
                                let backgroundColor =
                                      theme_backgroundColor theme
                                in  Util.fromRGB_A backgroundColor 0.4
                            , ci_fuzziness = 2
                            }
              in  (orb, sparkle, orbShadowMaybe)

    drawCircle (gle_circle glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances = catMaybes orbShadowsMaybe
        }
    withBlendFunc GL_SRC_ALPHA GL_ONE $ do
      drawPsiStormOrb (gle_psiStormOrb glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = testParams
          , svp_instances = orbs
          }
      drawSparkle (gle_sparkle glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = testParams
          , svp_instances = sparkles
          }

  -- Dash particles
  drawDash (gle_dash glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          flip map (particles_dash particles) $ \particle ->
            let DashParticle{..} = particle_type particle
            in  DashInstance
                  { di_center = Fixed.toFloats dp_pos
                  , di_initialVelocity = Fixed.toFloats dp_vel
                  , di_scale = Fixed.toFloat dp_radius
                  , di_angle =
                      let Float2 x y = Fixed.toFloats dp_direction
                      in  atan2 y x
                  , di_ticksElapsed =
                      fromIntegral $
                        time `diffTime` particle_startTime particle
                  , di_secondsElapsed =
                      let ticksElapsed =
                            fromIntegral (time `diffTime` particle_startTime particle)
                      in  ticksElapsed / fromIntegral C.tickRate_hz
                  , di_color =
                      -- Reduce visibility of weak dashes to avoid giving
                      -- players the idea that spamming drone dash makes drones
                      -- go faster
                      let x = min 1 $ max 0 $ Fixed.toFloat dp_magnitudeFraction
                          opacity = 0.30 * x + 0.70 * x * x
                          color =
                            case viewTeamColor teamView dp_team of
                              TeamRed -> theme_teamRed_dashDustColor theme
                              TeamBlue -> theme_teamBlue_dashDustColor theme
                      in  Util.fromRGB_A color opacity
                  }
      }

  withBlendFunc GL_SRC_ALPHA GL_ONE $ do
    drawDashRing (gle_dashRing glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            flip mapMaybe (particles_dash particles) $ \particle ->
              let ticksElapsed = time `diffTime` particle_startTime particle
                  DashParticle{..} = particle_type particle
                  direction = Fixed.toFloats dp_direction
                  radius = Fixed.toFloat dp_radius
                  dashInstance =
                    DashRingInstance
                      { dri_center =
                          Fixed.toFloats dp_pos
                      , -- - Float.map2 (*radius) direction
                        dri_initialVelocity = Fixed.toFloats dp_vel
                      , dri_radius = 0.5 * radius
                      , dri_direction = direction
                      , dri_ticksElapsed = fromIntegral ticksElapsed
                      , dri_secondsElapsed =
                          fromIntegral ticksElapsed / fromIntegral C.tickRate_hz
                      , dri_color =
                          -- Reduce visibility of weak dashes to avoid giving
                          -- players the idea that spamming drone dash makes drones
                          -- go faster
                          let x = min 1 $ max 0 $ Fixed.toFloat dp_magnitudeFraction
                              scaling =
                                case dp_dashType of
                                  OverseerDash -> 1
                                  DroneDash -> 0.4
                              opacity = scaling * (0.30 * x + 0.70 * x * x)
                              color =
                                case viewTeamColor teamView dp_team of
                                  TeamRed -> theme_teamRed_dashDustColor theme
                                  TeamBlue -> theme_teamBlue_dashDustColor theme
                          in  Util.fromRGB_A color opacity
                      }
              in  if ticksElapsed > fromIntegral C.tickRate_hz `div` 4
                    then Nothing -- avoid unnecessary rendering
                    else Just $! dashInstance
        }

  -- Psi-storm cast
  withBlendFunc GL_SRC_ALPHA GL_ONE $ do
    drawCastRipple (gle_castRipple glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            flip map (particles_castRipple particles) $ \p ->
              let totalDuration =
                    getTicks $ particle_endTime p `diffTime` particle_startTime p
                  progressTicks =
                    getTicks $ time `diffTime` particle_startTime p
                  progressFrac =
                    fromIntegral progressTicks / fromIntegral totalDuration
                  CastRippleParticle pos = particle_type p
              in  CastRippleInstance
                    { cri_size = 90.0
                    , cri_center = Fixed.toFloats pos
                    , cri_color = theme_psiStormRippleColor theme
                    , cri_ringWidth = 2.0
                    , cri_gradientWidth = 8.0
                    , cri_progress = progressFrac
                    }
        }
    drawSparkle (gle_sparkle glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            flip mapMaybe (particles_castRipple particles) $ \particle ->
              let CastRippleParticle pos = particle_type particle
                  ticks = getTicks $ time `diffTime` particle_startTime particle
                  opacity = max 0 $ 1 - fromIntegral ticks / fromIntegral flareLifetimeTicks
              in  if opacity <= 0
                    then Nothing
                    else
                      Just
                        SparkleInstance
                          { si_size = 80 * opacity
                          , si_center = Fixed.toFloats pos
                          , si_color =
                              Util.fromRGB_A
                                (theme_psiStormFlashColor theme)
                                (opacity * theme_psiStormFlashOpacity theme)
                          , si_bloomBrightness = 1
                          }
        }

  -- Overseer death and flag score shockwaves
  withBlendFunc GL_SRC_ALPHA GL_ONE $ do
    let makeShockwave :: Time -> Float3 -> Float2 -> Float -> Float -> Maybe (CastRippleInstance Cpu)
        makeShockwave startTime color pos size opacity =
          let ticksElapsed = time `diffTime` startTime
              progress =
                min 1 $
                  fromIntegral ticksElapsed / fromIntegral flareLifetimeTicks
              castRippleInstance =
                CastRippleInstance
                  { cri_size = size
                  , cri_center = pos
                  , cri_color = Util.fromRGB_A color opacity
                  , cri_ringWidth = 2.0
                  , cri_gradientWidth = 8.0
                  , cri_progress = progress
                  }
          in  if ticksElapsed <= flareLifetimeTicks
                then Just castRippleInstance
                else Nothing
    drawCastRipple (gle_castRipple glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            let overseerDeaths =
                  flip mapMaybe (particles_overseerDeath particles) $ \p ->
                    let odp = particle_type p
                    in  makeShockwave
                          (particle_startTime p)
                          (viewPlayerColor theme mActorID teamView (odp_color odp))
                          (Fixed.toFloats (odp_pos odp))
                          140
                          0.175
                flagScores =
                  flip mapMaybe (particles_flagExplode particles) $ \p ->
                    let fep = particle_type p
                    in  makeShockwave
                          (particle_startTime p)
                          (flagColors theme (viewTeamColor teamView (fep_flagTeam fep)))
                          (Fixed.toFloats (fep_pos fep))
                          90
                          0.25
            in  overseerDeaths ++ flagScores
        }

  -- Overseer death explosions and flag score explosions
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawExplosion (gle_explosion glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            concatMap
              (Explosion.fromOverseerDeathParticle mActorID theme teamView time)
              (particles_overseerDeath particles)
              ++ concatMap
                (fromFlagExplodeParticle theme teamView time)
                (particles_flagExplode particles)
        }

  -- Drone death particles
  drawDrone (gle_drone glEnv) $
    StandardVParams
      { svp_aspectRatio = aspectRatio
      , svp_coordSystem = gameViewCS
      , svp_testParams = testParams
      , svp_instances =
          map (fromDroneDeathParticle time) $
            particles_droneDeath particles
      }

  -- Collision sparks
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawSpark (gle_spark glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            let collisionSparks =
                  concatMap (fromCollisionParticle mActorID theme teamView time) $
                    particles_collisionDamage particles
                psiStormSparks =
                  concatMap
                    (fromPsiStormDamageParticle mActorID theme teamView time)
                    (particles_psiStormDamage particles)
            in  collisionSparks ++ psiStormSparks
        }

  -- Flare effects for overseer death, flag scores, and collisions
  withBlendFunc GL_SRC_ALPHA GL_ONE $ do
    let makeFlare :: Time -> Float3 -> Float2 -> Float -> Float -> Float -> Maybe (FlareInstance Cpu)
        makeFlare startTime color pos radius baseOpacity bloomWidth =
          let ticksElapsed = time `diffTime` startTime
              fadeStartTicks = 2
              opacityFactor =
                let t = fromIntegral $ max 0 (ticksElapsed - fadeStartTicks)
                in  max 0 $ 1 - t / fromIntegral (flareLifetimeTicks - fadeStartTicks)
              flareInstance =
                FlareInstance
                  { fi_center = pos
                  , fi_radius = radius
                  , fi_bloomWidth = bloomWidth
                  , fi_color =
                      Util.fromRGB_A color (baseOpacity * opacityFactor)
                  }
          in  if ticksElapsed <= flareLifetimeTicks
                then Just flareInstance
                else Nothing
    drawFlare (gle_flare glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances =
            let overseerDeaths =
                  flip mapMaybe (particles_overseerDeath particles) $ \p ->
                    let odp = particle_type p
                    in  makeFlare
                          (particle_startTime p)
                          (viewPlayerColor theme mActorID teamView (odp_color odp))
                          (Fixed.toFloats (odp_pos odp))
                          (Fixed.toFloat (odp_scale odp))
                          0.08
                          75
                flagScores =
                  flip mapMaybe (particles_flagExplode particles) $ \p ->
                    let fep = particle_type p
                    in  makeFlare
                          (particle_startTime p)
                          (flagColors theme (viewTeamColor teamView (fep_flagTeam fep)))
                          (Fixed.toFloats (fep_pos fep))
                          (Fixed.toFloat (fep_radius fep))
                          0.15
                          35
                damageToOpacity damage =
                  let maxDamage = 50
                  in  (0.5 * min 1 (Fixed.toFloat damage / maxDamage))
                collisionDamage =
                  flip mapMaybe (particles_collisionDamage particles) $ \p ->
                    let cdp = particle_type p
                    in  makeFlare
                          (particle_startTime p)
                          (Float3 1 1 1)
                          (Fixed.toFloats (cdp_pos cdp))
                          0
                          (damageToOpacity (cdp_damage cdp))
                          16
                psiStormDamage =
                  flip mapMaybe (particles_psiStormDamage particles) $ \p ->
                    let psdp = particle_type p
                    in  makeFlare
                          (particle_startTime p)
                          (Float3 1 1 1)
                          (Fixed.toFloats (psdp_pos psdp))
                          0
                          (damageToOpacity (psdp_damage psdp))
                          16
            in  collisionDamage ++ psiStormDamage ++ overseerDeaths ++ flagScores
        }

  -- Psi-storms
  withBlendFunc GL_SRC_ALPHA GL_ONE $
    drawPsiStorm (gle_psiStorm glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameViewCS
        , svp_testParams = testParams
        , svp_instances = flip map (rcs_psiStorms renderingComps) $ \rc ->
            let psiStorm = rendering_entityRendering rc
                endTime = psr_endTime psiStorm
            in  PsiStormInstance
                  { psi_modelScale = rendering_radius rc
                  , psi_modelTrans = rendering_pos rc
                  , psi_elapsedSeconds =
                      let lifetime =
                            gp_psiStormLifetimeTicks $ gp_cast gameParams
                          elapsedTicks =
                            time `diffTime` addTicks (-lifetime) endTime
                      in  fromIntegral elapsedTicks / fromIntegral C.tickRate_hz
                  , psi_opacityFactor =
                      psiStormOpacityFactor gameParams (scene_time scene) endTime
                  , psi_stormColor = theme_psiStormColor theme
                  , psi_boundaryOpacity = theme_psiStormBoundaryOpacity theme
                  , psi_fillOpacity = theme_psiStormFillOpacity theme
                  , psi_bloomColor =
                      case psr_color psiStorm of
                        Nothing -> Float4 0 0 0 0
                        Just playerColor ->
                          Util.fromRGB_A
                            (viewPlayerColor theme mActorID teamView playerColor)
                            1
                  }
        }

  -- Pixel filter
  case sceneDeco_perspective decorations of
    FreePerspective -> pure ()
    GlobalPerspective -> pure ()
    PlayerPerspective playerInfo ->
      case pv_overseerView (pi_playerView playerInfo) of
        OV_Alive OverseerViewAlive{} -> pure ()
        OV_Dead OverseerViewDead{ovd_deathTime} ->
          drawPixelFilter (gle_pixelFilter glEnv) $
            PixelFilterVParams
              { pfvp_screenDims =
                  if aspectRatio >= 1
                    then
                      Float2
                        (fromIntegral @Int (round (screenSize' * aspectRatio)))
                        screenSize'
                    else
                      Float2
                        screenSize'
                        (fromIntegral @Int (round (screenSize' / aspectRatio)))
              , pfvp_fadeFrac =
                  let seconds =
                        fromIntegral (time `diffTime` ovd_deathTime)
                          / fromIntegral C.tickRate_hz
                      frac = min 1 (seconds / CC.deathFadeSeconds)
                      frac2 = frac * frac
                      frac4 = frac2 * frac2
                  in  frac4
              }

  -- Fog
  case sceneDeco_perspective decorations of
    PlayerPerspective{} ->
      drawFog (gle_fog glEnv) $
        FogVParams
          { fvp_aspectRatio = aspectRatio
          , fvp_viewScale = recip viewRadius
          , fvp_viewCenter = cameraPos
          , fvp_modelScale = viewRadius
          , fvp_modelTrans = overseerPos
          , fvp_fogRadius = viewRadius
          }
    FreePerspective -> pure ()
    GlobalPerspective -> pure ()

  -- Drone control radius
  when (sceneDeco_showHUD decorations) $
    case sceneDeco_perspective decorations of
      FreePerspective -> pure ()
      GlobalPerspective -> pure ()
      PlayerPerspective playerInfo ->
        let playerView = pi_playerView playerInfo
        in  case pv_overseerView playerView of
              OV_Dead{} -> pure ()
              OV_Alive{} -> do
                withBlendFunc GL_SRC_ALPHA GL_ONE $
                  drawDroneControlRadius (gle_droneControlRadius glEnv) $
                    DroneControlRadiusVParams
                      { dcrvp_aspectRatio = aspectRatio
                      , dcrvp_modelTrans = overseerPos
                      , dcrvp_modelScale =
                          Fixed.toFloat $
                            getClassParam
                              (pv_playerClass playerView)
                              (gp_controlRadius (gp_dynamics gameParams))
                      , dcrvp_viewScale = recip viewRadius
                      , dcrvp_viewCenter = cameraPos
                      , dcrvp_color =
                          let playerTeam =
                                view player_team (pi_player playerInfo)
                          in  case viewTeamColor teamView playerTeam of
                                TeamRed ->
                                  theme_teamRed_controlRadiusColor theme
                                TeamBlue ->
                                  theme_teamBlue_controlRadiusColor theme
                      }

  -- Pause overlay
  do
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        grey = Float4 0.5 0.5 0.5 1.0
        lightGrey = Float4 0.75 0.75 0.75 1.00

        displayTextMaybe =
          case scene_pauseState scene of
            Playing ->
              Nothing
            Paused ->
              Just $ List.singleton (grey, "Game paused")
            Resuming resumeTime ->
              Just
                [ (grey, "Resuming in ")
                ,
                  ( lightGrey
                  , show $
                      let resumingInTicks =
                            resumeTime `diffSyncTime` scene_syncTime scene
                      in  getTicks resumingInTicks `ceilDiv` C.tickRate_hz
                  )
                ]

    for_ displayTextMaybe $ \displayText -> do
      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_aspectRatio = aspectRatio
          , cvp_fontAtlas = fontAtlasLarge
          , cvp_screenSize = fromIntegral screenSize
          , cvp_lines =
              shadowText (fromIntegral fontSizeLarge) (Float4 0 0 0 1) $
                List.singleton
                  ( CharAlignCenter
                  , Float2 (screenSize' * 0.5) (screenSize' * 0.55)
                  , displayText
                  )
          }

  -- Pathfinding debug lines
  do
    let botStateMaybe = do
          actorID <- mActorID
          player <- fst <$> WIM.lookup actorID (scene_players scene)
          player_botState player
        pathfindingResultMaybe = do
          botState <- botStateMaybe
          case sceneDeco_showDebugInfo decorations of
            DDM_None -> Nothing
            DDM_Overseers -> bs_overseerPathfinding (getBotDebugState botState)
            DDM_Drones -> bs_dronePathfinding (getBotDebugState botState)
            DDM_PsiStorm -> Nothing
            DDM_Goals -> Nothing
            DDM_MovementPotentials -> Nothing
            DDM_DroneMovementPotentials -> Nothing
    for_ pathfindingResultMaybe $ \pathfindingResult -> do
      drawLine (gle_line glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = testParams
          , svp_instances =
              let PathfindingResult{..} = pathfindingResult
                  requestedStartPos = fmap Fixed.toFloats pr_requestedStartPos
                  requestedEndPos = fmap Fixed.toFloats pr_requestedEndPos
                  startPos = fmap Fixed.toFloats pr_startPos
                  endPos = fmap Fixed.toFloats pr_endPos
                  waypointPositions =
                    maybeToList startPos
                      ++ maybe [] (map Fixed.toFloats) pr_path
                  moveTargetLine =
                    maybeToList $ do
                      pos1 <- requestedStartPos
                      pos2 <- requestedEndPos
                      pure
                        LineInstance
                          { li_endpoint1 = pos1
                          , li_endpoint2 = pos2
                          , li_color = Float4 1 1 0 0.5
                          }
                  movedCurrentPosLine =
                    maybeToList $ do
                      pos1 <- requestedStartPos
                      pos2 <- startPos
                      pure
                        LineInstance
                          { li_endpoint1 = pos1
                          , li_endpoint2 = pos2
                          , li_color = Float4 1 0 0 0.5
                          }
                  movedMoveTargetLine =
                    maybeToList $ do
                      pos1 <- requestedEndPos
                      pos2 <- endPos
                      pure
                        LineInstance
                          { li_endpoint1 = pos1
                          , li_endpoint2 = pos2
                          , li_color = Float4 1 0 0 0.5
                          }
                  makeWaypointLine x y =
                    LineInstance
                      { li_endpoint1 = x
                      , li_endpoint2 = y
                      , li_color = Float4 0 1 0 0.5
                      }
                  waypointLines =
                    zipWith
                      makeWaypointLine
                      waypointPositions
                      (drop 1 waypointPositions)
                  graphEdgeLines =
                    pr_edges <&> \(pos1, pos2) ->
                      LineInstance
                        { li_endpoint1 = Fixed.toFloats pos1
                        , li_endpoint2 = Fixed.toFloats pos2
                        , li_color = Float4 0 0.5 1 0.5
                        }
              in  graphEdgeLines
                    ++ moveTargetLine
                    ++ waypointLines
                    ++ movedCurrentPosLine
                    ++ movedMoveTargetLine
          }

      for_ botStateMaybe $ \botState -> do
        let moveTargetLabel = bs_moveTargetLabel (getBotDebugState botState)

            fontAtlasSmall = gle_fontAtlasSmall glEnv
            fontSizeSmall = fa_fontSize fontAtlasSmall
            padding = gle_uiPadding glEnv

            lineSpacing = fromIntegral fontSizeSmall * 1.5
            y_top = screenSize' * 0.5

            posTopLeft = Float2 padding y_top
            grey = Float4 0.60 0.60 0.60 1

            (_, lines') =
              stackLinesDownward
                CharAlignLeft
                posTopLeft
                lineSpacing
                [(grey, moveTargetLabel)]

        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlasSmall
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText (fromIntegral fontSizeSmall) (Float4 0 0 0 1) lines'
            }

  -- Psi-storm cursor debug position
  let psiStormDebugMaybe = do
        actorID <- mActorID
        player <- fst <$> WIM.lookup actorID (scene_players scene)
        botState <- player_botState player
        case sceneDeco_showDebugInfo decorations of
          DDM_PsiStorm ->
            let BotDebugState{..} = getBotDebugState botState
            in  Just (bs_psiStormCursor, bs_threateningDrones)
          _ -> Nothing
  for_ psiStormDebugMaybe $ \(psiStormCursorPosMaybe, threateningDrones) -> do
    for_ psiStormCursorPosMaybe $ \psiStormCursorPos ->
      drawCircle (gle_circle glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = testParams
          , svp_instances =
              List.singleton
                CircleInstance
                  { ci_size = 8
                  , ci_center = Fixed.toFloats psiStormCursorPos
                  , ci_color = Float4 1 1 0 0.6
                  , ci_fuzziness = 1
                  }
          }
    withBlendFunc GL_SRC_ALPHA GL_ONE $
      drawRing (gle_ring glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = gameViewCS
          , svp_testParams = testParams
          , svp_instances =
              flip map threateningDrones $ \pos ->
                RingInstance
                  { ri_modelScale = 11
                  , ri_modelTrans = Fixed.toFloats pos
                  , ri_color = Float4 1 1 1 0.7
                  , ri_thickness = 1.5
                  , ri_fuzziness = 1
                  }
          }

  -- Bot goal debug info
  let goalDebug =
        case sceneDeco_showDebugInfo decorations of
          DDM_Goals -> do
            actorID <- mActorID
            player <- fst <$> WIM.lookup actorID (scene_players scene)
            getBotDebugState <$> player_botState player
          _ -> Nothing
  for_ goalDebug $ \BotDebugState{..} -> do
    let fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall
        padding = gle_uiPadding glEnv

        lineSpacing = fromIntegral fontSizeSmall * 1.5
        y_top = screenSize' * 0.5

        posTopLeft = Float2 padding y_top
        grey = Float4 0.60 0.60 0.60 1

        (_, lines') =
          stackLinesDownward CharAlignLeft posTopLeft lineSpacing $
            map
              (grey,)
              [ "total allied health: " ++ show @Int (round bs_totalAlliedHealth)
              , "total enemy health: " ++ show @Int (round bs_totalEnemyHealth)
              , "flag command: " ++ show bs_flagCmd
              ]

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = fontAtlasSmall
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            shadowText (fromIntegral fontSizeSmall) (Float4 0 0 0 1) lines'
        }

  -- Move potentials debug info
  let movePotentialDebugMaybe = do
        actorID <- mActorID
        player <- fst <$> WIM.lookup actorID (scene_players scene)
        botState <- player_botState player
        case sceneDeco_showDebugInfo decorations of
          DDM_MovementPotentials ->
            let BotDebugState{..} = getBotDebugState botState
            in  (,,)
                  <$> bs_movePotentials
                  <*> bs_chosenMovePos
                  <*> bs_longRangeAvoidancePotentials
          DDM_DroneMovementPotentials ->
            let BotDebugState{..} = getBotDebugState botState
            in  (,,)
                  <$> bs_droneMovePotentials
                  <*> bs_chosenDroneMovePos
                  <*> bs_longRangeAvoidancePotentials
          _ -> Nothing
  for_ movePotentialDebugMaybe $
    \(movePotentials, chosenMovePos, longRangePotentials) -> do
      for_ (List.NonEmpty.nonEmpty movePotentials) $ \movePotentials' -> do
        for_ (List.NonEmpty.nonEmpty longRangePotentials) $ \longRangePotentials' -> do
          let makePotentialSet potentials =
                let allPotentials = fmap snd potentials
                    min' = minimum allPotentials
                    max' = maximum allPotentials
                    range = max' - min'
                    opacity = 0.7
                    colorize val =
                      if min' /= max'
                        then
                          if val == min'
                            then Float4 0.0 0.5 1.0 opacity
                            else
                              let frac = (val - min') / range
                                  frac' = Fixed.toFloat frac
                              in  Float4 frac' frac' (1 - frac') opacity
                        else Float4 0.5 0.5 0.5 opacity
                in  flip map (toList potentials) $ \(pos, val) ->
                      CircleInstance
                        { ci_size = 4
                        , ci_center = Fixed.toFloats pos
                        , ci_color = colorize val
                        , ci_fuzziness = 1
                        }

              chosenCircle =
                CircleInstance
                  { ci_size = 2
                  , ci_center = Fixed.toFloats chosenMovePos
                  , ci_color = Float4 1 1 1 1
                  , ci_fuzziness = 1
                  }
              potentialCircles = makePotentialSet movePotentials'
              longRangePotentialCircles = makePotentialSet longRangePotentials'

          drawCircle (gle_circle glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  chosenCircle : potentialCircles ++ longRangePotentialCircles
              }

hintOpacityFromFlagCondition :: Time -> FlagConditionStatus -> Float
hintOpacityFromFlagCondition time = \case
  FlagConditionSatisfied timeStart ->
    hintOpacity (time `diffTime` timeStart)
  FlagConditionUnsatisfied timeStart timeEnd ->
    let opacityAtEnd = hintOpacity (timeEnd `diffTime` timeStart)
        fade =
          fromIntegral (time `diffTime` timeEnd)
            / fromIntegral hintTransitionTicks
    in  max 0 $ opacityAtEnd - fade

-- Helper
hintOpacity ::
  Ticks -> -- ticks since start of animation
  Float
hintOpacity ticks
  | ticks <= 0 = 0
  | ticks <= endFadeIn =
      fromIntegral ticks / fromIntegral transitionTicks
  | ticks <= startFadeOut = 1
  | ticks <= endFadeOut =
      fromIntegral (transitionTicks - (ticks - startFadeOut))
        / fromIntegral transitionTicks
  | otherwise = 0
  where
    durationTicks = 240
    transitionTicks = hintTransitionTicks
    endFadeIn = transitionTicks
    startFadeOut = endFadeIn + durationTicks
    endFadeOut = startFadeOut + transitionTicks

hintTransitionTicks :: Ticks
hintTransitionTicks = 12

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c

psiStormOpacityFactor :: GameParams -> Time -> Time -> Float
psiStormOpacityFactor gameParams sceneTime endTime =
  let ticksRemaining =
        max 0 $
          getTicks (endTime `diffTime` sceneTime)
      fadeTicks =
        getTicks $
          gp_psiStormFadeTicks (gp_dynamics gameParams)
  in  min 1 $
        fromIntegral ticksRemaining / fromIntegral fadeTicks
