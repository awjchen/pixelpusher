module Client.Visuals.Rendering.DarkLayer (
  drawDarkLayer,
) where

import Control.Monad.Trans.Reader

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float4 (Float4),
 )
import Pixelpusher.Game.Parameters.Test

import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Quad
import Client.Visuals.StandardInstancedVisual

drawDarkLayer :: TestParams -> ReaderT (GLEnv, Int, Float) GL ()
drawDarkLayer testParams = ReaderT $ \(glEnv, screenSize, aspectRatio) ->
  let screenWidth = fromIntegral screenSize
  in  drawQuad (gle_quad glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = CS.centeredViewport screenSize
          , svp_testParams = testParams
          , svp_instances =
              pure $
                QuadInstance
                  { qi_size = Float2 (aspectRatio * screenWidth) screenWidth
                  , qi_center = Float2 0 0
                  , qi_color = Float4 0 0 0 0.80
                  }
          }
