-- | Display text for the dedicated tutorial
module Client.Visuals.Rendering.GameUI.TutorialInstructions (
  TutorialInstruction (..),
  tutorialPhaseInstruction,
  drawTutorialInstruction,
  moveOverseerTutorialInstruction,
  moveDronesTutorialInstruction,
  dronesAreHeavyTutorialInstruction,
  showAllControlsTutorialInstruction,
) where

import Control.Monad.Trans.Reader

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.Tutorial

import Client.Config.Controls
import Client.Config.Keybindings
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (shadowText)

data TutorialInstruction = TutorialInstruction
  { ti_control :: String
  , ti_action :: String
  }

moveOverseerTutorialInstruction :: TutorialInstruction
moveOverseerTutorialInstruction =
  TutorialInstruction
    { ti_control = "WASD"
    , ti_action = "move overseer"
    }

moveDronesTutorialInstruction :: ControlOptions -> TutorialInstruction
moveDronesTutorialInstruction controlOpts =
  TutorialInstruction
    { ti_control =
        case co_droneControl controlOpts of
          ManualDroneControl ->
            let leftRight =
                  if co_swapMouseButtons controlOpts
                    then "right"
                    else "left"
            in  "Hold " ++ leftRight ++ " mouse button"
          AutomaticDroneControl ->
            "Mouse"
    , ti_action = "move drones"
    }

dronesAreHeavyTutorialInstruction :: TutorialInstruction
dronesAreHeavyTutorialInstruction =
  TutorialInstruction
    { ti_control = ""
    , ti_action = "Drones are heavy and accelerate slowly"
    }

showAllControlsTutorialInstruction :: TutorialInstruction
showAllControlsTutorialInstruction =
  TutorialInstruction
    { ti_control = "Hold F1-F5"
    , ti_action = "show help"
    }

tutorialPhaseInstruction ::
  Keybindings -> ControlOptions -> TutorialPhase -> Maybe TutorialInstruction
tutorialPhaseInstruction keybindings controlOpts = \case
  Tutorial_Start ->
    Nothing
  Tutorial_MoveOverseer ->
    Just moveOverseerTutorialInstruction
  Tutorial_MoveOverseerComplete ->
    Nothing
  Tutorial_MoveDrones ->
    Just $ moveDronesTutorialInstruction controlOpts
  Tutorial_MoveDronesComplete ->
    Nothing
  Tutorial_DronesAreHeavy ->
    Just dronesAreHeavyTutorialInstruction
  Tutorial_DronesAreHeavyComplete ->
    Nothing
  Tutorial_OverseerDash ->
    Just
      TutorialInstruction
        { ti_control =
            concat
              [ keyToString (kb_overseerDash keybindings)
              , "+"
              , keyToString (kb_overseerUp keybindings)
              , keyToString (kb_overseerLeft keybindings)
              , keyToString (kb_overseerDown keybindings)
              , keyToString (kb_overseerRight keybindings)
              ]
        , ti_action = "dash overseer"
        }
  Tutorial_OverseerDashComplete ->
    Nothing
  Tutorial_AttackOverseer ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Attack enemy overseers with drones"
        }
  Tutorial_AttackOverseerComplete ->
    Nothing
  Tutorial_CastPsiStorm ->
    Just
      TutorialInstruction
        { ti_control = keyToString (kb_psiStorm keybindings)
        , ti_action = "cast psionic storm at cursor"
        }
  Tutorial_CastPsiStormComplete ->
    Nothing
  Tutorial_KillDroneWithPsiStorm ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action =
            let key = keyToString (kb_psiStorm keybindings)
            in  "Cast psionic storm on the enemy drones (" ++ key ++ ")"
        }
  Tutorial_KillDroneWithPsiStormComplete ->
    Nothing
  Tutorial_UsePsiStormDefensively ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Use psionic storm to block the movement of drones"
        }
  Tutorial_UsePsiStormDefensivelyComplete ->
    Nothing
  Tutorial_TakeFlag ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Pick up the rival flag by moving onto it"
        }
  Tutorial_TakeFlagComplete ->
    Nothing
  Tutorial_FlagSlow ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Carrying the flag slows you"
        }
  Tutorial_FlagSlowComplete ->
    Nothing
  Tutorial_DropFlag ->
    Just
      TutorialInstruction
        { ti_control = keyToString (kb_dropFlag keybindings)
        , ti_action = "drop flag"
        }
  Tutorial_DropFlagComplete ->
    Nothing
  Tutorial_CaptureFlag ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Bring the flag to your base to score"
        }
  Tutorial_CaptureFlagComplete ->
    Nothing
  Tutorial_FullControls ->
    Just showAllControlsTutorialInstruction
  Tutorial_FullControlsComplete ->
    Nothing
  Tutorial_End ->
    Just
      TutorialInstruction
        { ti_control = ""
        , ti_action = "Tutorial complete! (press Escape)"
        }

drawTutorialInstruction :: TutorialInstruction -> ReaderT (GLEnv, Int, Float) GL ()
drawTutorialInstruction instruction =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let fontLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontLarge
        lineSpacing = fromIntegral $ (fontSizeLarge * 3) `div` 2

        screenWidth = fromIntegral screenSize

        x = 0.5 * screenWidth
        y = 0.5 * screenWidth

        lightGrey = Float4 0.75 0.75 0.75 1.0
        grey = Float4 0.5 0.5 0.5 1.0

    let TutorialInstruction{ti_control, ti_action} = instruction
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = fontLarge
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeLarge)
              (Float4 0 0 0 1)
              [
                ( CharAlignCenter
                , Float2 x (y + 3 * lineSpacing)
                ,
                  [ (lightGrey, ti_control)
                  , if null ti_control
                      then (grey, "")
                      else (grey, ": ")
                  , (grey, ti_action)
                  ]
                )
              ]
        }
