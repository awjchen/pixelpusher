-- | Overlay for a brief tutorial at the start of a game
module Client.Visuals.Rendering.GameUI.TutorialOverlay (
  drawTutorialOverlay,
  shouldShowTutorial,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (for_)

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Time

import Client.Config.Controls (ControlOptions)
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (shadowText)

import Client.Visuals.Rendering.GameUI.TutorialInstructions

shouldShowTutorial :: GameParams -> Ticks -> Bool
shouldShowTutorial params elapsedTime =
  elapsedTime <= totalTutorialDuration + gp_startSpawnDelayTicks params
    && not (gp_enableTutorialMode params) -- dedicated mode for tutorial

totalTutorialDuration :: Ticks
totalTutorialDuration =
  -- this hard-coded value needs to be updated when the timing of instructions
  -- changes
  3 * spacing + short + 3 * long

spacing :: Ticks
spacing = Ticks C.tickRate_hz

short :: Ticks
short = Ticks $ 3 * C.tickRate_hz

long :: Ticks
long = Ticks $ 6 * C.tickRate_hz

drawTutorialOverlay ::
  GameParams -> ControlOptions -> Ticks -> ReaderT (GLEnv, Int, Float) GL ()
drawTutorialOverlay params controlOpts ticksElapsed =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let fontLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontLarge
        lineSpacing = fromIntegral $ (fontSizeLarge * 3) `div` 2

        fontSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontSmall
        smallSpacing = fromIntegral $ fontSizeSmall `div` 2

        screenWidth = fromIntegral screenSize

        x = 0.5 * screenWidth
        y = 0.5 * screenWidth

        lightGrey = Float4 0.75 0.75 0.75 1.0
        grey = Float4 0.5 0.5 0.5 1.0
        darkGrey = Float4 0.3 0.3 0.3 1.0

    let playTicksElapsed = ticksElapsed - gp_startSpawnDelayTicks params
        mStage
          | playTicksElapsed < spacing =
              Nothing
          | playTicksElapsed < spacing + long =
              Just MoveDrones
          | playTicksElapsed < spacing + short + long =
              Just DronesHaveInertia
          | playTicksElapsed < 2 * spacing + short + long =
              Nothing
          | playTicksElapsed < 2 * spacing + short + 2 * long =
              Just MoveOverseer
          | playTicksElapsed < 3 * spacing + short + 2 * long =
              Nothing
          | playTicksElapsed < 3 * spacing + short + 3 * long =
              Just ExtraHelp
          | otherwise =
              Nothing

    for_ mStage $ \stage -> do
      let TutorialInstruction{ti_control, ti_action} =
            case stage of
              MoveOverseer -> moveOverseerTutorialInstruction
              MoveDrones -> moveDronesTutorialInstruction controlOpts
              DronesHaveInertia -> dronesAreHeavyTutorialInstruction
              ExtraHelp -> showAllControlsTutorialInstruction

      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_aspectRatio = aspectRatio
          , cvp_fontAtlas = fontLarge
          , cvp_screenSize = fromIntegral screenSize
          , cvp_lines =
              shadowText
                (fromIntegral fontSizeLarge)
                (Float4 0 0 0 1)
                [
                  ( CharAlignCenter
                  , Float2 x (y + 3 * lineSpacing)
                  ,
                    [ (lightGrey, ti_control)
                    , if null ti_control
                        then (grey, "")
                        else (grey, ": ")
                    , (grey, ti_action)
                    ]
                  )
                ]
          }

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = fontSmall
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeSmall)
              (Float4 0 0 0 1)
              [
                ( CharAlignLeft
                , Float2 smallSpacing smallSpacing
                , [(darkGrey, "Ctrl :: Skip tutorial")]
                )
              ]
        }

data Stage = MoveDrones | DronesHaveInertia | MoveOverseer | ExtraHelp
