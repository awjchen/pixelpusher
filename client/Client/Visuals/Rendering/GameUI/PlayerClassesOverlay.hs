{-# LANGUAGE ViewPatterns #-}

module Client.Visuals.Rendering.GameUI.PlayerClassesOverlay (
  drawPlayerClassesOverlay,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Semigroup (Product (..), stimesMonoid)

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Game.Bots.AI.Kinematics (restingDistance)
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerClass

import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (shadowTextWith)

drawPlayerClassesOverlay :: GameParams -> ReaderT (GLEnv, Int, Float) GL ()
drawPlayerClassesOverlay params = do
  (glEnv, screenSize, aspectRatio) <- ask
  let screenSize' = fromIntegral screenSize

      largeFont = gle_fontAtlasLarge glEnv
      largeFontSize = fa_fontSize largeFont

      smallFont = gle_fontAtlasSmall glEnv
      smallFontSize = fa_fontSize smallFont
      smallLineSpacing = fromIntegral smallFontSize * 1.5

  let combatParams = gp_combat params
      dynParams = gp_dynamics params
      castParams = gp_cast params

  let table :: [(String, String, String)]
      table =
        [ ("", zealotText, templarText)
        , ("", "", "")
        ,
          ( "overseer health"
          , showHealth $ getClassParam Zealot (gp_overseerMaxHealth combatParams)
          , showHealth $ getClassParam Templar (gp_overseerMaxHealth combatParams)
          )
        , let showTicks =
                (<> " s")
                  . showNum 1
                  . (/ fromIntegral C.tickRate_hz)
                  . fromIntegral @_ @Double
          in  ( "overseer full health regeneration time"
              , showTicks $ getClassParam Zealot (gp_quadraticHealthFullRegenTicks combatParams)
              , showTicks $ getClassParam Templar (gp_quadraticHealthFullRegenTicks combatParams)
              )
        ,
          ( "overseer acceleration"
          , showAccel $ getClassParam Zealot $ gp_overseerAcceleration dynParams
          , showAccel $ getClassParam Templar $ gp_overseerAcceleration dynParams
          )
        ,
          ( "overseer terminal velocity"
          , showVel $
              getClassParam Zealot (gp_overseerAcceleration dynParams)
                / getClassParam Zealot (gp_overseerDrag dynParams)
          , showVel $
              getClassParam Templar (gp_overseerAcceleration dynParams)
                / getClassParam Templar (gp_overseerDrag dynParams)
          )
        , let showDashDistance :: LinearDragParams -> Fixed -> String
              showDashDistance dragParams deltaV =
                showDist $ restingDistance dragParams deltaV
          in  ( "overseer dash distance"
              , showDashDistance
                  (getClassParam Zealot (gp_overseerDragParams (gp_bots params)))
                  (getClassParam Zealot (gp_overseerDashDeltaV castParams))
              , showDashDistance
                  (getClassParam Templar (gp_overseerDragParams (gp_bots params)))
                  (getClassParam Templar (gp_overseerDashDeltaV castParams))
              )
        , ("", "", "")
        , let showDroneCount x = show x ++ " drones"
          in  ( "drone count"
              , showDroneCount $ getClassParam Zealot (gp_overseerDroneLimit params)
              , showDroneCount $ getClassParam Templar (gp_overseerDroneLimit params)
              )
        ,
          ( "total drone health"
          , showHealth $
              getClassParam Zealot (gp_droneMaxHealth combatParams)
                * fromIntegral (getClassParam Zealot (gp_overseerDroneLimit params))
          , showHealth $
              getClassParam Templar (gp_droneMaxHealth combatParams)
                * fromIntegral (getClassParam Templar (gp_overseerDroneLimit params))
          )
        ,
          ( "drone acceleration"
          , showAccel $ getClassParam Zealot (gp_droneAcceleration dynParams)
          , showAccel $ getClassParam Templar (gp_droneAcceleration dynParams)
          )
        ,
          ( "drone terminal velocity"
          , showVel $
              getClassParam Zealot (gp_droneAcceleration dynParams)
                / getClassParam Zealot (gp_droneDrag dynParams)
          , showVel $
              getClassParam Templar (gp_droneAcceleration dynParams)
                / getClassParam Templar (gp_droneDrag dynParams)
          )
        , ("", "", "")
        ,
          ( "control radius"
          , showDist $ getClassParam Zealot (gp_controlRadius dynParams)
          , showDist $ getClassParam Templar (gp_controlRadius dynParams)
          )
        , ("", "", "")
        , ("class ability", "none", "psionic storm")
        ]

  let y0 = screenSize' * 0.7
      titleLine =
        ( CharAlignLeft
        , Float2
            (screenSize' * paddingScreenFraction)
            (y0 + fromIntegral largeFontSize)
        , [(lightGrey, "class parameters")]
        )
      tableLines =
        flip concatMap (zip [0 :: Int ..] table) $
          \(fromIntegral -> lineNo, (columnLabel, column1, column2)) ->
            let y = y0 - lineNo * smallLineSpacing
            in  [
                  ( CharAlignRight
                  , Float2 (screenSize' * 0.375) y
                  , [(lightGrey, columnLabel)]
                  )
                ,
                  ( CharAlignCenter
                  , Float2 (screenSize' * 0.50) y
                  , [(red, column1)]
                  )
                ,
                  ( CharAlignCenter
                  , Float2 (screenSize' * 0.65) y
                  , [(yellow, column2)]
                  )
                ]

  lift $ do
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = largeFont
        , cvp_screenSize = screenSize'
        , cvp_lines =
            shadowTextWith
              (0.5 * fromIntegral largeFontSize)
              darken
              [titleLine]
        }
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = smallFont
        , cvp_screenSize = screenSize'
        , cvp_lines =
            shadowTextWith (0.5 * fromIntegral smallFontSize) darken tableLines
        }

showNum :: (RealFrac a) => Int -> a -> String
showNum decimalPlaces x
  | decimalPlaces < 1 = show (round x :: Int)
  | otherwise =
      let divisor =
            getProduct $ stimesMonoid decimalPlaces (Product (10 :: Int))
          (beforeDecimal, afterDecimal) =
            round (x * fromIntegral divisor) `divMod` divisor
      in  show beforeDecimal
            <> "."
            <> leftPad decimalPlaces '0' (show afterDecimal)

leftPad :: Int -> Char -> String -> String
leftPad finalWidth c str =
  replicate (max 0 (finalWidth - length str)) c <> str

showDist :: Fixed -> String
showDist x =
  let value = Fixed.toDouble x * distanceScaling
  in  showNum 2 value <> " m"

showVel :: Fixed -> String
showVel x =
  let value = Fixed.toDouble x * fromIntegral C.tickRate_hz * distanceScaling
  in  showNum 2 value <> "  m/s"

showAccel :: Fixed -> String
showAccel x =
  let value =
        Fixed.toDouble x * sq (fromIntegral C.tickRate_hz) * distanceScaling
  in  showNum 2 value <> " m/s^2"

distanceScaling :: Double
distanceScaling = 0.01

showHealth :: Fixed -> String
showHealth x = showNum 1 x <> " hp"

sq :: (Num a) => a -> a
sq x = x * x

paddingScreenFraction :: Float
paddingScreenFraction = 0.05

-- TODO: deduplicate with help overlay function
darken :: Float4 -> Float4
darken (Float4 r g b a) = Float4 (0.2 * r) (0.2 * g) (0.2 * b) a

-- TODO: deduplicate with help overlay colors
lightGrey, red, yellow :: Float4
lightGrey = Float4 0.70 0.70 0.70 1
-- blue = Float4 0.48 0.64 0.80 1
red = Float4 0.80 0.56 0.60 1
yellow = Float4 0.71 0.61 0.45 1

-- green = Float4 0.42 0.67 0.60 1
