{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.Rendering.GameUI.Bottom (
  drawUIBottom,
) where

import Control.Monad.Trans.Reader
import Data.Bool (bool)
import Data.Maybe (catMaybes, fromMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2), Float3, Float4)
import Pixelpusher.Game.Debug
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.State
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Config.Keybindings
import Client.Theme
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (shadowText)

--------------------------------------------------------------------------------

data ClientStatus = ClientStatus
  { mRoomID :: Maybe Int
  , mLatency :: Maybe Int
  , isFramePredicted :: Bool
  , debugSnapshotLabels :: DebugSnapshotLabels
  , spatialAudioStatus :: Bool
  }

makeRightLines :: Theme -> ClientStatus -> [(Float3, String)]
makeRightLines theme ClientStatus{..} =
  map (theme_ui_statusTextColor theme,) $
    reverse $
      catMaybes $
        let DebugSnapshotLabels{..} = debugSnapshotLabels
            padding = 3
        in  [ if isFramePredicted then Just "." else Nothing
            , flip fmap mLatency $ \latency -> "Ping :: " ++ show latency ++ " ms"
            , -- , flip fmap mRoomID $ \roomID -> "Room :: " ++ show roomID
              if spatialAudioStatus
                then Nothing
                else Just "spatial audio disabled"
            , flip fmap dsl_current $ \(snapshotId, loadNo) ->
                leftPad0 padding (show snapshotId)
                  ++ "-"
                  ++ leftPad0 padding (show loadNo)
            , flip fmap dsl_slot1 $ \snapshotId ->
                "Slot 1: " ++ leftPad0 padding (show snapshotId)
            , flip fmap dsl_slot2 $ \snapshotId ->
                "Slot 2: " ++ leftPad0 padding (show snapshotId)
            , flip fmap dsl_slot3 $ \snapshotId ->
                "Slot 3: " ++ leftPad0 padding (show snapshotId)
            ]

leftPad0 :: Int -> String -> String
leftPad0 targetWidth str =
  let paddingChars = max 0 $ targetWidth - length str
  in  replicate paddingChars '0' ++ str

data ControlStatus = ControlStatus
  { time :: Time
  , keybindings :: Keybindings
  , mPlayerControlState :: Maybe PlayerControlState
  , mOverseerAlive :: Maybe Bool
  , mOverseerHasFlag :: Maybe Bool
  , mTeam :: Maybe Team
  , mPlayerClass :: Maybe PlayerClass
  }

makeLeftLines ::
  GameParams -> SceneDecorations -> ControlStatus -> [(Float3, String)]
makeLeftLines params decorations ControlStatus{..} =
  reverse $
    ifNotTutorial $
      catMaybes
        [ Just
            ( dashColor
            , concat
                [ "Dash :: "
                , showKey (kb_overseerDash keybindings)
                , "+"
                , showKey (kb_overseerUp keybindings)
                , showKey (kb_overseerLeft keybindings)
                , showKey (kb_overseerDown keybindings)
                , showKey (kb_overseerRight keybindings)
                ]
            )
        , case mPlayerClass of
            Just Templar ->
              Just
                ( castColor
                , psiStormName ++ " :: " ++ showKey (kb_psiStorm keybindings)
                )
            Just Zealot ->
              Just
                ( darkerColor
                , "[no class ability]"
                )
            _ -> Nothing
        , Just (dropFlagColor, "Drop flag :: " ++ showKey (kb_dropFlag keybindings))
        , Just (normalColor, "Show help :: hold F1-F5")
        ]
        ++ [(normalColor, "Menu :: Escape")]
  where
    ifNotTutorial xs =
      if sceneDeco_enableTutorialMode decorations then [] else xs

    showKey = drop 4 . show

    cooldownTimeColor = bool darkerColor normalColor . (time >=)
    castColor =
      fromMaybe normalColor $ do
        overseerAlive <- mOverseerAlive
        if overseerAlive
          then do
            playerControlState <- mPlayerControlState
            pure $
              cooldownTimeColor $
                pcs_psiStormCooldownUntil playerControlState
          else pure darkerColor
    dashColor =
      fromMaybe normalColor $ do
        overseerAlive <- mOverseerAlive
        if overseerAlive
          then do
            playerControlState <- mPlayerControlState
            playerClass <- mPlayerClass
            pure $
              cooldownTimeColor $
                dashReadyTime (gp_cast params) playerClass $
                  pcs_overseerDashStamina playerControlState
          else pure darkerColor
    dropFlagColor = maybe darkerColor (bool darkerColor normalColor) mOverseerHasFlag

    psiStormName = case mTeam of
      Nothing -> "Psionic storm"
      Just team -> case team of
        TeamW -> "Ionic storm"
        TeamE -> "Psionic storm"

    theme = sceneDeco_theme decorations
    normalColor = theme_ui_controlsNormalTextColor theme
    darkerColor = theme_ui_controlsDarkerTextColor theme

--------------------------------------------------------------------------------

drawUIBottom ::
  GameParams ->
  GameScene ->
  SceneDecorations ->
  Keybindings ->
  ReaderT (GLEnv, Int, Float) GL ()
drawUIBottom params scene decorations keybindings =
  let time = scene_time scene
      mPlayerInfo = getPlayerPerspective (sceneDeco_perspective decorations)
      controlStatus =
        ControlStatus
          { time = time
          , keybindings = keybindings
          , mPlayerControlState =
              -- TODO: cleanup: just pass mPlayerInfo instead of unpacking each
              -- of these fields
              pv_controlState . pi_playerView <$> mPlayerInfo
          , mOverseerAlive =
              mPlayerInfo <&> \playerInfo ->
                case pv_overseerView (pi_playerView playerInfo) of
                  OV_Alive{} -> True
                  OV_Dead{} -> False
          , mOverseerHasFlag =
              overseerViewHasFlag . pv_overseerView . pi_playerView
                <$> mPlayerInfo
          , mTeam =
              view player_team . pi_player <$> mPlayerInfo
          , mPlayerClass = pv_playerClass . pi_playerView <$> mPlayerInfo
          }
      clientStatus =
        ClientStatus
          { mRoomID = sceneDeco_roomID decorations
          , mLatency = sceneDeco_networkLatency decorations
          , isFramePredicted = sceneDeco_isFramePredicted decorations
          , debugSnapshotLabels = scene_debugSnapshotLabels scene
          , spatialAudioStatus = sceneDeco_spatialAudioStatus decorations
          }
  in  drawStatuses params decorations (Just controlStatus) (Just clientStatus)

drawStatuses ::
  GameParams ->
  SceneDecorations ->
  Maybe ControlStatus ->
  Maybe ClientStatus ->
  ReaderT (GLEnv, Int, Float) GL ()
drawStatuses params decorations mControlStatus mClientStatus =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let screenSize' = fromIntegral screenSize

    let fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall

        smallSpacing = fromIntegral $ fontSizeSmall `div` 2
        lineSpacing = fromIntegral $ 5 * fontSizeSmall `div` 4

    let makeDisplayLines ::
          CharAlignment ->
          [(Float3, String)] ->
          [(CharAlignment, Float2, [(Float4, String)])]
        makeDisplayLines align = zipWith (makeDisplayLine align) [0 ..]

        makeDisplayLine ::
          CharAlignment ->
          Int ->
          (Float3, String) ->
          (CharAlignment, Float2, [(Float4, String)])
        makeDisplayLine align lineNo (color, text) =
          ( align
          , Float2 pos_x (smallSpacing + fromIntegral lineNo * lineSpacing)
          , [(Util.fromRGB_A color 1, text)]
          )
          where
            pos_x = case align of
              CharAlignRight -> screenSize' - smallSpacing
              CharAlignLeft -> smallSpacing
              CharAlignCenter -> 0.5 * screenSize'
     in drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlasSmall
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                let theme = sceneDeco_theme decorations
                    leftLines =
                      maybe [] (makeLeftLines params decorations) mControlStatus
                    rightLines = maybe [] (makeRightLines theme) mClientStatus
                    displayLines =
                      makeDisplayLines CharAlignLeft leftLines
                        ++ makeDisplayLines CharAlignRight rightLines
                in  shadowText
                      (fromIntegral (fa_fontSize fontAtlasSmall))
                      (theme_ui_textShadowColor theme)
                      displayLines
            }
