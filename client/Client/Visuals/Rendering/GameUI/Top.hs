{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.Rendering.GameUI.Top (
  drawUITop,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (for_)
import Data.Maybe (mapMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Player
import Pixelpusher.Game.State
import Pixelpusher.Game.Time

import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (leftPad, shadowText)

import Client.Theme
import Client.Visuals.Rendering.GameUI.Minimap (drawMinimap)
import Client.Visuals.Rendering.GameUI.Notifications (
  drawNotificationLines,
  makeKillNotifications,
  makeLogNotifications,
  makePersonalKillNotifications,
 )
import Client.Visuals.Rendering.GameUI.Score (drawScore)

--------------------------------------------------------------------------------

-- | UI elements aligned to the top edge of the screen
drawUITop :: GameScene -> SceneDecorations -> ReaderT (GLEnv, Int, Float) GL ()
drawUITop gameScene@GameScene{..} decorations = do
  let mPlayer =
        pi_player <$> getPlayerPerspective (sceneDeco_perspective decorations)
      teamView = sceneDeco_teamView decorations
  screenSize <- asks (view _2)

  -- Minimap
  y_minimapBottom <-
    drawMinimap gameScene (sceneDeco_theme decorations) teamView mPlayer

  -- Flag capture notifications
  do
    let y_minimapBottom' =
          -- ack, the minimap position is in a different coordinate system
          view _2 $
            CS.intoPos (CS.viewport screenSize) $
              CS.fromPos (CS.centeredViewport screenSize) $
                Float2 0 y_minimapBottom
    drawNotificationLines CharAlignRight y_minimapBottom' $
      flip map (particles_logMsg scene_particles) $ \p ->
        ( notificationFade scene_time p
        , makeLogNotifications
            (sceneDeco_teamPerspective decorations)
            teamView
            (particle_type p)
        )

  -- Timer
  y_timerBottom <-
    drawTimer
      (sceneDeco_theme decorations)
      scene_gamePhase
      scene_time
      scene_roundStart

  -- Global kill notifications
  drawNotificationLines CharAlignLeft y_timerBottom $
    flip map (particles_killNotification scene_particles) $ \p ->
      ( notificationFade scene_time p
      , makeKillNotifications teamView (particle_type p)
      )

  -- Personal kill notifications
  do
    let mActorIDName =
          case sceneDeco_perspective decorations of
            FreePerspective -> Nothing
            GlobalPerspective -> Nothing
            PlayerPerspective PlayerInfo{pi_actorID, pi_player} ->
              Just (pi_actorID, view player_name pi_player)
    for_ mActorIDName $ \(actorID, playerName) ->
      drawNotificationLines CharAlignCenter (fromIntegral screenSize * 0.95) $
        flip mapMaybe (particles_killNotification scene_particles) $ \p -> do
          let kill = particle_type p
          killLine <- makePersonalKillNotifications actorID playerName kill
          pure (notificationFade scene_time p, killLine)

  -- Score
  drawScore gameScene decorations

notificationFade :: Time -> Particle a -> Float
notificationFade time p =
  let ticksFromEnd = particle_endTime p `diffTime` time
  in  min 1 . max 0 $
        fromIntegral ticksFromEnd / (0.25 * fromIntegral C.tickRate_hz)

-- | Returns a lower-bound y-coordinate for the timer text
drawTimer ::
  Theme -> GamePhase -> Time -> Time -> ReaderT (GLEnv, Int, Float) GL Float
drawTimer theme gamePhase time roundStart = ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
  let fontAtlasLarge = gle_fontAtlasLarge glEnv
      fontSizeLarge = fa_fontSize fontAtlasLarge
      padding = gle_uiPadding glEnv

      screenSize' = fromIntegral screenSize
      lineSpacing =
        fromIntegral $ (`div` 3) $ (* 2) fontSizeLarge -- adjusted by hand
      y_top = screenSize' - padding - lineSpacing

      posTopLeft = Float2 padding y_top
      textColor = theme_ui_timerTextColor theme
      opacity = 1

      roundTicks = (`diffTime` roundStart) $ case gamePhase of
        GamePhase_Game _ _ -> time
        GamePhase_Intermission roundEnd _ _ _ -> roundEnd
      secondsElapsed = getTicks roundTicks `div` fromIntegral C.tickRate_hz
      (minutes, seconds) = secondsElapsed `divMod` 60
      timeText = leftPad' (show minutes) ++ ":" ++ leftPad' (show seconds)
        where
          leftPad' = leftPad 2 '0'

  drawChar (gle_char glEnv) $
    CharVParams
      { cvp_aspectRatio = aspectRatio
      , cvp_fontAtlas = fontAtlasLarge
      , cvp_screenSize = fromIntegral screenSize
      , cvp_lines =
          shadowText
            (fromIntegral fontSizeLarge)
            (theme_ui_textShadowColor theme)
            [
              ( CharAlignLeft
              , posTopLeft
              , [(Util.fromRGB_A textColor opacity, timeText)]
              )
            ]
      }
  pure y_top
