module Client.Visuals.Rendering.GameUI.VisualGlossary (
  drawVisualGlossary,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.List qualified as List
import Data.Word (Word8)
import Lens.Micro
import Unsafe.Coerce (unsafeCoerce)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Parameters.Dynamics (shieldRadius)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.CoordinateSystems (CoordSystem)
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.DSL (GLKind (..))
import Client.Visuals.GL.Util (fromRGB_A)
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.Drone
import Client.Visuals.Shaders.Flag
import Client.Visuals.Shaders.Overseer
import Client.Visuals.Shaders.PsiStorm
import Client.Visuals.Shaders.PsiStormOrb
import Client.Visuals.Shaders.ShieldPowerup
import Client.Visuals.Shaders.SoftObstacle
import Client.Visuals.Shaders.TeamBase
import Client.Visuals.SharedVisuals (flagColors, psiStormOrbRadius)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Text (shadowTextWith)

drawVisualGlossary :: GameParams -> ReaderT (GLEnv, Int, Float) GL ()
drawVisualGlossary params = do
  (glEnv, screenSize, aspectRatio) <- ask
  let screenWidth = fromIntegral screenSize
      theme = defaultTheme

  do
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fromIntegral $ fa_fontSize fontAtlasLarge
        charLines =
          List.singleton
            ( CharAlignCenter
            , Float2
                (0.25 * screenWidth + 0.003 * screenWidth) -- offset for visual centering, adjusted manually
                (0.73 * screenWidth)
            , List.singleton (lightGrey, "visual glossary")
            )
    lift $
      drawChar (gle_char glEnv) $
        CharVParams
          { cvp_aspectRatio = aspectRatio
          , cvp_fontAtlas = fontAtlasLarge
          , cvp_screenSize = screenWidth
          , cvp_lines =
              shadowTextWith (0.5 * fontSizeLarge) darken charLines
          }

  let r = CC.viewRadius
      x1 = -0.5 * r
      x2 = 0.5 * r

  drawOverseerDemo params theme $
    Float2 x1 (0.3 * r)
  drawDroneDemo params theme $
    Float2 x1 (0.1 * r)
  drawShieldPowerupDemo params theme $
    Float2 x1 (-0.15 * r)
  drawOverseerPsiStormIndicatorDemo params theme $
    Float2 x1 (-0.35 * r)
  drawFlagDemo params theme $
    Float2 x1 (-0.60 * r)

  drawPsiStormDemo params theme $
    Float2 x2 (0.6375 * r)
  drawSoftObstacleDemo params $
    Float2 x2 (-0.075 * r)
  drawTeamBaseDemo params $
    Float2 x2 (-0.6875 * r)

gameCoordSystem :: CoordSystem
gameCoordSystem = CS.gameView CC.viewRadius (Float2 0 0)

readTextCoordSystem :: (Monad m) => ReaderT (GLEnv, Int, Float) m CoordSystem
readTextCoordSystem = do
  (_glEnv, screenSize, _) <- ask
  pure $ CS.viewport screenSize

drawTextUnderGameEntity ::
  Float2 -> Float -> Float -> String -> ReaderT (GLEnv, Int, Float) GL ()
drawTextUnderGameEntity entityPos entityRadius spacingFactor text = do
  textCoordSystem <- readTextCoordSystem
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let entityBottom = entityPos & _2 -~ entityRadius
        textAnchor =
          CS.intoPos textCoordSystem $ CS.fromPos gameCoordSystem entityBottom

    let fontAtlas = gle_fontAtlasSmall glEnv
        fontSize = fromIntegral $ fa_fontSize fontAtlas
        spacing = fontSize * spacingFactor
        grey = Float4 0.55 0.55 0.55 1
        charLines =
          [
            ( CharAlignCenter
            , textAnchor & _2 -~ spacing
            , [(grey, text)]
            )
          ]
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = fontAtlas
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            shadowTextWith (0.5 * fontSize) darken charLines
        }

drawOverseerDemo ::
  GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawOverseerDemo params theme anchor = do
  let overseerRadius =
        Fixed.toFloat $ gp_overseerRadius_base $ gp_dynamics params
      hSpacing = 3 * overseerRadius

  let pos1 = anchor & _1 -~ hSpacing
      pos2 = anchor
      pos3 = anchor & _1 +~ hSpacing

  (glEnv, _, aspectRatio) <- ask
  lift $
    drawOverseer (gle_overseer glEnv) $
      OverseerVParams
        { ovp_aspectRatio = aspectRatio
        , ovp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , ovp_testParams = gp_test params
        , ovp_instances =
            [ makeOverseer params theme pos1 1.0 0.0
            , makeOverseer params theme pos2 0.5 0.0
            , makeOverseer params theme pos3 0.1 0.0
            ]
        }

  drawTextUnderGameEntity
    anchor
    overseerRadius
    leftSpacingFactor
    "Overseers at 100%, 50%, and 10% health"

makeOverseer ::
  GameParams -> Theme -> Float2 -> Float -> Float -> OverseerInstance
makeOverseer params theme center healthFraction shieldFraction =
  OverseerInstance
    { oi_radius = Fixed.toFloat $ gp_overseerRadius_base $ gp_dynamics params
    , oi_pos = center
    , oi_prevPos1 = center -- no shadow
    , oi_prevPos2 = center -- no shadow
    , oi_prevPos3 = center -- no shadow
    , oi_prevPos4 = center -- no shadow
    , oi_prevPos5 = center -- no shadow
    , oi_prevPos6 = center -- no shadow
    , oi_prevPos7 = center -- no shadow
    , oi_prevPos8 = center -- no shadow
    , oi_prevPos9 = center -- no shadow
    , oi_prevPos10 = center -- no shadow
    , oi_healthFraction = healthFraction
    , oi_shieldsFraction = shieldFraction
    , oi_color = fromRGB_A (teamDefaultColor theme TeamRed) 1
    , oi_recentDamage = 0
    , oi_isInvulnerable = False
    , oi_class = Templar
    , oi_angleOffset = 0
    , oi_psiStormReadySeconds = -1
    , oi_angleFins = 0
    , oi_ticksSinceDash = Nothing
    , oi_shieldColor = theme_teamRed_shieldColor theme
    }

drawDroneDemo :: GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawDroneDemo params theme anchor = do
  let overseerRadius =
        Fixed.toFloat $ gp_overseerRadius_base $ gp_dynamics params
      droneRadius = Fixed.toFloat $ gp_droneRadius_base $ gp_dynamics params
      hSpacing = 3 * overseerRadius

  let pos1 = anchor & _1 -~ hSpacing
      pos2 = anchor
      pos3 = anchor & _1 +~ hSpacing

  (glEnv, _, aspectRatio) <- ask
  lift $ do
    drawDrone (gle_drone glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            [ makeDrone params theme pos1 1.0
            , makeDrone params theme pos2 0.5
            , makeDrone params theme pos3 0.1
            ]
        }
  drawTextUnderGameEntity
    anchor
    droneRadius
    leftSpacingFactor
    "Drones at 100%, 50%, and 10% health"

makeDrone :: GameParams -> Theme -> Float2 -> Float -> DroneInstance Cpu
makeDrone params theme center healthFraction =
  fromDroneRendering Nothing theme AbsoluteTeamView initialTime $
    RenderingComponent
      { rendering_pos = center
      , rendering_radius =
          Fixed.toFloat $ gp_droneRadius_base $ gp_dynamics params
      , rendering_entityRendering =
          DroneRendering
            { dr_color = Right $ PlayerColor team Color1 actorID
            , dr_healthFraction = healthFraction
            , dr_recentDamage = 0
            , dr_actorID = Just actorID
            , dr_prevPos1 = center
            , dr_prevPos2 = center
            , dr_prevPos3 = center
            , dr_prevPos4 = center
            , dr_prevPos5 = center
            , dr_prevPos6 = center
            , dr_prevPos7 = center
            , dr_prevPos8 = center
            , dr_prevPos9 = center
            , dr_prevPos10 = center
            , dr_isInvulnerable = False
            , dr_entityID = 0
            , dr_team = team
            , dr_class = Zealot
            , dr_ticksSinceDash = Nothing
            , dr_chargeFraction = 0
            }
      }
  where
    team = TeamW -- whatever was needed to make the color match the overseer
    actorID = PlayerActor $ unsafeCoerce (0 :: Word8) -- I just quickly need a 'ActorID'

drawShieldPowerupDemo ::
  GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawShieldPowerupDemo params theme anchor = do
  let overseerRadius =
        Fixed.toFloat $ gp_overseerRadius_base $ gp_dynamics params
      shieldPowerupRadius =
        let shieldHealth = gp_overseerDeathShieldPowerupHealth params
        in  Fixed.toFloat $ shieldRadius (gp_dynamics params) shieldHealth
      hSpacing = 1.5 * overseerRadius

  let pos1 = anchor & _1 -~ hSpacing
      pos2 = anchor & _1 +~ hSpacing

  (glEnv, _, aspectRatio) <- ask
  lift $ do
    drawShieldPowerup (gle_shieldPowerup glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              ShieldPowerupInstance
                { spi_size = shieldPowerupRadius
                , spi_center = pos1
                , spi_color = fromRGB_A (theme_teamRed_shieldColor theme) 0.85
                }
        }
    drawOverseer (gle_overseer glEnv) $
      OverseerVParams
        { ovp_aspectRatio = aspectRatio
        , ovp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , ovp_testParams = gp_test params
        , ovp_instances = [makeOverseer params theme pos2 1.0 0.5]
        }

  drawTextUnderGameEntity
    anchor
    overseerRadius
    leftSpacingFactor
    "Shield powerup, Overseer at 50% shields"

drawOverseerPsiStormIndicatorDemo ::
  GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawOverseerPsiStormIndicatorDemo params theme anchor = do
  let overseerRadius =
        Fixed.toFloat $ gp_overseerRadius_base $ gp_dynamics params

  (glEnv, _, aspectRatio) <- ask
  lift $ do
    drawOverseer (gle_overseer glEnv) $
      OverseerVParams
        { ovp_aspectRatio = aspectRatio
        , ovp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , ovp_testParams = gp_test params
        , ovp_instances =
            List.singleton $ makeOverseer params theme anchor 1.0 0.0
        }

    let pos =
          let ang = pi / 4 * 0.7
              r = overseerRadius + 1.5 * psiStormOrbRadius
              offset = Float.map2 (* r) $ Float.angle ang
          in  anchor + offset
        baseColor =
          teamDefaultColor theme (viewTeamColor (RelativeTeamView TeamW) TeamE)
    drawCircle (gle_circle glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameCoordSystem
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              CircleInstance
                { ci_size = psiStormOrbRadius + 1.5
                , ci_center = pos
                , ci_color =
                    let v = backgroundValue
                    in  Float4 v v v 0.4
                , ci_fuzziness = 2
                }
        }
    drawPsiStormOrb (gle_psiStormOrb glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = gameCoordSystem
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              PsiStormOrbInstance
                { psoi_size = psiStormOrbRadius
                , psoi_center = pos
                , psoi_color = fromRGB_A baseColor 0.85
                , psoi_time = 0
                , psoi_bloomRadiusFactor = 1
                }
        }

  drawTextUnderGameEntity
    anchor
    overseerRadius
    leftSpacingFactor
    "Overseer with psionic storm charge"

drawFlagDemo ::
  GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawFlagDemo params theme anchor = do
  let flagRadius = Fixed.toFloat $ gp_flagRadius (gp_dynamics params)
      hSpacing = 3 * flagRadius

  let pos1 = anchor & _1 -~ hSpacing
      pos2 = anchor & _1 +~ hSpacing

  (glEnv, _, aspectRatio) <- ask
  lift $
    drawFlag (gle_flag glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            [ makeFlag params theme pos1 TeamW
            , makeFlag params theme pos2 TeamE
            ]
        }

  drawTextUnderGameEntity
    anchor
    flagRadius
    leftSpacingFactor
    "Flags"

makeFlag :: GameParams -> Theme -> Float2 -> Team -> FlagInstance Cpu
makeFlag params theme pos team =
  FlagInstance
    { fi_size = Fixed.toFloat $ gp_flagRadius (gp_dynamics params)
    , fi_center = pos
    , fi_color =
        flip fromRGB_A 1 $
          flagColors theme $
            viewTeamColor AbsoluteTeamView team
    }

drawPsiStormDemo ::
  GameParams -> Theme -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawPsiStormDemo params theme anchor = do
  let psiStormRadius = Fixed.toFloat $ gp_psiStormRadius $ gp_dynamics params
  (glEnv, _, aspectRatio) <- ask
  lift $
    drawPsiStorm (gle_psiStorm glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              PsiStormInstance
                { psi_modelScale = psiStormRadius
                , psi_modelTrans = anchor
                , psi_elapsedSeconds = 0
                , psi_opacityFactor = 1
                , psi_stormColor = theme_psiStormColor theme
                , psi_boundaryOpacity = theme_psiStormBoundaryOpacity theme
                , psi_fillOpacity = theme_psiStormFillOpacity theme
                , psi_bloomColor = Float4 0 0 0 0
                }
        }

  drawTextUnderGameEntity
    anchor
    psiStormRadius
    rightSpacingFactor
    "Psionic storms damage ALL overseers and drones"

drawSoftObstacleDemo :: GameParams -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawSoftObstacleDemo params anchor = do
  let softObstacleRadius =
        Fixed.toFloat $ gp_softObstacleRadius $ gp_dynamics params
  (glEnv, _, aspectRatio) <- ask
  lift $
    drawSoftObstacle (gle_softObstacle glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              SoftObstacleInstance
                { soi_modelScale = softObstacleRadius
                , soi_modelTrans = anchor
                , soi_color = theme_obstacleColor defaultTheme
                , soi_maxOpacity = theme_obstacleOpacity defaultTheme
                , soi_minOpacity = 0
                }
        }

  drawTextUnderGameEntity
    anchor
    softObstacleRadius
    rightSpacingFactor
    "Obstacles have a soft collision boundary"

drawTeamBaseDemo :: GameParams -> Float2 -> ReaderT (GLEnv, Int, Float) GL ()
drawTeamBaseDemo params anchor = do
  let teamBaseRadius =
        Fixed.toFloat $ gp_teamBaseRadius $ gp_dynamics params
  (glEnv, _, aspectRatio) <- ask
  lift $
    drawTeamBase (gle_teamBase glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = CS.gameView CC.viewRadius (Float2 0 0)
        , svp_testParams = gp_test params
        , svp_instances =
            List.singleton
              TeamBaseInstance
                { tbi_size = teamBaseRadius
                , tbi_center = anchor
                , tbi_color = Float4 0.5 0.5 1 0.3 -- needs to be kept in sync with the game visual
                , tbi_fillOpacity = 1
                }
        }

  drawTextUnderGameEntity
    anchor
    teamBaseRadius
    rightSpacingFactor
    "Carry the other team's flag to your Base to score"

-- Spacing

leftSpacingFactor :: Float
leftSpacingFactor = 1.75

rightSpacingFactor :: Float
rightSpacingFactor = 1.25

-- Colors

darken :: Float4 -> Float4
darken (Float4 r g b a) = Float4 (0.2 * r) (0.2 * g) (0.2 * b) a

lightGrey :: Float4
lightGrey = Float4 0.70 0.70 0.70 1

backgroundValue :: Float
backgroundValue = 0
