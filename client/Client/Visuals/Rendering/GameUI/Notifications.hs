module Client.Visuals.Rendering.GameUI.Notifications (
  makeKillNotifications,
  makePersonalKillNotifications,
  makeLogNotifications,
  drawNotificationLines,
) where

import Control.Monad.Trans.Reader
import Data.Bifunctor (first)
import Data.ByteString.Char8 qualified as BS

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float4 (Float4),
 )
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team

import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.SharedVisuals (maxAspectRatio)
import Client.Visuals.TeamView
import Client.Visuals.Text (shadowText)

--------------------------------------------------------------------------------

makeKillNotifications ::
  TeamView -> KillNotificationParticle -> [(Float4, String)]
makeKillNotifications teamView kill =
  let killerName =
        maybe "  " getPlayerName' $ fmap snd $ knp_mKiller kill
      killedName = getPlayerName' $ knp_killedName kill
      killerColor =
        teamTextColor $ viewTeamColor teamView $ oppositeTeam $ knp_killedTeam kill
  in  [ (killerColor, killerName ++ " > ")
      , (greyTextColor, killedName)
      ]
  where
    getPlayerName' = BS.unpack . getPlayerName

makePersonalKillNotifications ::
  ActorID ->
  PlayerName ->
  KillNotificationParticle ->
  Maybe [(Float4, String)]
makePersonalKillNotifications actorID playerName kill
  | isKiller =
      Just $
        let killedName = getPlayerName' $ knp_killedName kill
        in  [ (darkGreyTextColor, "kill> ")
            , (greyTextColor, killedName)
            ]
  | isAssister =
      Just $
        let killedName = getPlayerName' $ knp_killedName kill
        in  [ (darkGreyTextColor, "assist> ")
            , (greyTextColor, killedName)
            ]
  | otherwise =
      Nothing
  where
    isKiller =
      case knp_mKiller kill of
        Nothing -> False
        Just (killerActorID, killerName) ->
          actorID == killerActorID && playerName == killerName
    isAssister = WIM.member actorID (knp_assisters kill)
    getPlayerName' = BS.unpack . getPlayerName

-- TODO: Rename the arguments of this function
makeLogNotifications ::
  Maybe Team -> TeamView -> LogMsgParticle -> [(Float4, String)]
makeLogNotifications viewingTeamMaybe teamView = \case
  LogMsg_FlagPickup FlagPickupParticle{fpp_carrierTeam = carrierTeam} ->
    let carrierColor = teamTextColor $ viewTeamColor teamView carrierTeam
    in  case viewingTeamMaybe of
          Nothing ->
            [
              ( greyTextColor
              , teamNameCapitalized carrierTeam <> " "
              )
            , (carrierColor, "stole")
            , (greyTextColor, " a flag")
            ]
          Just viewingTeam ->
            if viewingTeam == carrierTeam
              then
                [ (greyTextColor, "We ")
                , (carrierColor, "stole")
                , (greyTextColor, " their flag")
                ]
              else
                [ (greyTextColor, "They ")
                , (carrierColor, "stole")
                , (greyTextColor, " our flag")
                ]
  LogMsg_FlagCapture (FlagCaptureParticle carrierTeam _) ->
    let carrierColor = teamTextColor $ viewTeamColor teamView carrierTeam
    in  case viewingTeamMaybe of
          Nothing ->
            [
              ( greyTextColor
              , teamNameCapitalized carrierTeam <> " "
              )
            , (carrierColor, "captured")
            , (greyTextColor, " a flag")
            ]
          Just viewingTeam ->
            if viewingTeam == carrierTeam
              then
                [ (greyTextColor, "We ")
                , (carrierColor, "captured")
                , (greyTextColor, " their flag")
                ]
              else
                [ (greyTextColor, "They ")
                , (carrierColor, "captured")
                , (greyTextColor, " our flag")
                ]
  LogMsg_FlagRecover (FlagRecoverParticle carrierTeam _ _) ->
    let carrierColor = teamTextColor $ viewTeamColor teamView carrierTeam
    in  case viewingTeamMaybe of
          Nothing ->
            [
              ( greyTextColor
              , teamNameCapitalized carrierTeam <> " "
              )
            , (carrierColor, "recovered")
            , (greyTextColor, " their flag")
            ]
          Just viewingTeam ->
            if viewingTeam == carrierTeam
              then
                [ (greyTextColor, "We ")
                , (carrierColor, "recovered")
                , (greyTextColor, " our flag")
                ]
              else
                [ (greyTextColor, "They ")
                , (carrierColor, "recovered")
                , (greyTextColor, " their flag")
                ]
  LogMsg_AnyMsg msg -> [(greyTextColor, msg)]

greyTextColor :: Float4
greyTextColor = Float4 0.6 0.6 0.6 1

darkGreyTextColor :: Float4
darkGreyTextColor = Float4 0.3 0.3 0.3 1

teamTextColor :: TeamColor -> Float4
teamTextColor team = case team of
  TeamRed -> Float4 0.8 0.2 0.2 1
  TeamBlue -> Float4 0.2 0.5 0.8 1

drawNotificationLines ::
  CharAlignment ->
  Float ->
  [(Float, [(Float4, String)])] ->
  ReaderT (GLEnv, Int, Float) GL ()
drawNotificationLines align y_top notifications =
  ReaderT $ \(glEnv, screenSize, aspectRatio) ->
    let fontAtlas = gle_fontAtlasSmall glEnv
        fontSize = fa_fontSize fontAtlas

        aspectRatio' = min maxAspectRatio aspectRatio

        padding = gle_uiPadding glEnv
        lineSpacing = fromIntegral $ (fontSize * 5) `div` 4
        pos_x = case align of
          CharAlignLeft ->
            padding
          CharAlignRight ->
            fromIntegral screenSize * 0.5 * (1 + aspectRatio') - padding
          CharAlignCenter ->
            fromIntegral screenSize * 0.5
        (fades, msgs) = unzip notifications
        y_positions =
          let y_init = y_top - lineSpacing - padding
          in  scanl (-) y_init $ map (* lineSpacing) fades
    in  drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlas
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                let f fade pos_y msgs' =
                      let opacity = fade * fade * fade * fade
                          msgs'' = (map . first) (overOpacity (* opacity)) msgs'
                          pos = Float2 pos_x pos_y
                      in  shadowText
                            (fromIntegral fontSize)
                            (Float4 0 0 0 opacity)
                            [(align, pos, msgs'')]
                in  concat $ zipWith3 f fades y_positions msgs
            }

overOpacity :: (Float -> Float) -> Float4 -> Float4
overOpacity f (Float4 x y z w) = Float4 x y z (f w)
