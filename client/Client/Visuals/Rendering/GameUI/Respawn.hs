module Client.Visuals.Rendering.GameUI.Respawn (
  EnableRespawnTips (..),
  drawRespawnMessage,
) where

import Control.Monad (when)
import Control.Monad.Trans.Reader
import Data.Vector (Vector)
import Data.Vector qualified as Vector
import Data.Word (Word8)
import Lens.Micro.Platform.Custom
import Unsafe.Coerce (unsafeCoerce)

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear
import Pixelpusher.Custom.Util
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Overseer
import Client.Visuals.Shaders.Quad
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Text (shadowText)

-- Hack
data EnableRespawnTips = EnableRespawnTips | DisableRespawnTips

drawRespawnMessage ::
  GameParams ->
  EnableRespawnTips ->
  Theme ->
  TeamView ->
  Player ->
  PlayerClass ->
  Time ->
  Time ->
  Time ->
  ReaderT (GLEnv, Int, Float) GL ()
drawRespawnMessage
  params
  enableRespawnTips
  theme
  teamView
  player
  playerClass
  currentTime
  deathTime
  respawnTime =
    ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
      let tickRate = fromIntegral C.tickRate_hz
          joinTime = view player_joinTime player
          gameplaySeconds =
            fromIntegral (currentTime `diffTime` joinTime) / tickRate
          ticksUntilRespawn = getTicks $ respawnTime `diffTime` currentTime
          secondsSinceDeath =
            fromIntegral (currentTime `diffTime` deathTime) / tickRate
          secondsUntilRespawn =
            fromIntegral ticksUntilRespawn / tickRate
          ceilingSecondsUntilRespawn =
            max 1 $ -- hide the fact that we don't check respawns every tick
              ticksUntilRespawn `ceilDiv` fromIntegral C.tickRate_hz
          isFirstSpawn =
            currentTime `diffTime` joinTime
              <= gp_startSpawnDelayTicks params + fromIntegral C.tickRate_hz

      let coverOpacity =
            if isFirstSpawn && not (gp_enableTutorialMode params)
              then min 1 . max 0 $ 1 - 1.4 * (2 - secondsUntilRespawn)
              else 0
          versusOpacity =
            if isFirstSpawn && not (gp_enableTutorialMode params)
              then
                let frac = min 1 $ max 0 $ 1.4 * gameplaySeconds
                    frac2 = frac * frac
                    frac4 = frac2 * frac2
                in  frac4
              else 0
          respawnMessageOpacity =
            let frac = min 1 . max 0 $ 1.4 * secondsSinceDeath
                frac2 = frac * frac
                frac4 = frac2 * frac2
            in  frac4
          classSelectionOpacity =
            if isFirstSpawn
              then 0
              else
                let frac =
                      min 1 . max 0 $
                        secondsSinceDeath - 0.25 - CC.deathFadeSeconds
                    frac2 = frac * frac
                    frac4 = frac2 * frac2
                in  frac4
          tipOpacity =
            if isFirstSpawn
              then 0
              else
                let frac =
                      min 1 . max 0 $
                        secondsSinceDeath - 0.25 - CC.deathFadeSeconds
                    frac2 = frac * frac
                    frac4 = frac2 * frac2
                in  frac4

      let fontLarge = gle_fontAtlasLarge glEnv
          fontSizeLarge = fa_fontSize fontLarge
          lineSpacing = fromIntegral $ (fontSizeLarge * 3) `div` 2

          fontRegular = gle_fontAtlasRegular glEnv
          fontSizeRegular = fa_fontSize fontRegular

          fontSmall = gle_fontAtlasSmall glEnv
          fontSizeSmall = fa_fontSize fontSmall

          screenWidth = fromIntegral screenSize

          mid_x = 0.5 * screenWidth

          lightGrey = Float3 0.75 0.75 0.75
          grey = Float3 0.5 0.5 0.5
          darkGrey = Float3 0.15 0.15 0.15
          reallyDarkGrey = Float3 0.05 0.05 0.05

      when (respawnMessageOpacity > 0) $
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontLarge
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeLarge)
                  (Float4 0 0 0 respawnMessageOpacity)
                  [
                    ( CharAlignCenter
                    , Float2 mid_x (0.65 * screenWidth)
                    ,
                      [ (Util.fromRGB_A grey respawnMessageOpacity, "Spawning in ")
                      , (Util.fromRGB_A lightGrey respawnMessageOpacity, show ceilingSecondsUntilRespawn)
                      ]
                    )
                  ]
            }

      when (classSelectionOpacity > 0) $ do
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontRegular
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeRegular)
                  (Float4 0 0 0 classSelectionOpacity)
                  [
                    ( CharAlignCenter
                    , Float2 mid_x (0.60 * screenWidth)
                    , [(Util.fromRGB_A grey classSelectionOpacity, "as")]
                    )
                  ]
            }

        let classColor playerClass' =
              flip Util.fromRGB_A classSelectionOpacity $
                if playerClass == playerClass'
                  then grey
                  else
                    let ticksUntilDeadTime =
                          respawnTime `diffTime` currentTime
                            - respawnClassSelectionDeadTimeFinal
                        fadeTicks = Ticks $ C.tickRate_hz `div` 4
                        complement x = 1 - x
                        progressFrac =
                          complement $
                            max 0 . min 1 $
                              fromIntegral ticksUntilDeadTime
                                / fromIntegral fadeTicks
                    in  Float.map3 (* progressFrac) reallyDarkGrey
                          + Float.map3 (* (1 - progressFrac)) darkGrey

        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontRegular
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeRegular)
                  (Float4 0 0 0 classSelectionOpacity)
                  [
                    ( CharAlignCenter
                    , Float2 (mid_x - 0.07 * screenWidth) (0.50 * screenWidth)
                    , [(classColor Zealot, "zealot")]
                    )
                  ,
                    ( CharAlignCenter
                    , Float2 (mid_x + 0.07 * screenWidth) (0.50 * screenWidth)
                    , [(classColor Templar, "templar")]
                    )
                  ]
            }

      when (tipOpacity > 0) $ do
        case enableRespawnTips of
          DisableRespawnTips -> pure ()
          EnableRespawnTips -> do
            let tipText =
                  Vector.unsafeIndex respawnTips $
                    fromIntegral (getTime respawnTime) `mod` numRespawnTips
                tipColor = Util.fromRGB_A darkGrey tipOpacity
            drawChar (gle_char glEnv) $
              CharVParams
                { cvp_aspectRatio = aspectRatio
                , cvp_fontAtlas = gle_fontAtlasSmall glEnv
                , cvp_screenSize = fromIntegral screenSize
                , cvp_lines =
                    shadowText
                      (fromIntegral fontSizeSmall)
                      (Float4 0 0 0 tipOpacity)
                      [
                        ( CharAlignCenter
                        , Float2 mid_x (0.25 * screenWidth + 0 * lineSpacing)
                        , [(tipColor, tipText)]
                        )
                      ]
                }

      when (coverOpacity > 0) $
        drawQuad (gle_quad glEnv) $
          StandardVParams
            { svp_aspectRatio = aspectRatio
            , svp_coordSystem = CS.centeredViewport screenSize
            , svp_testParams = gp_test params
            , svp_instances =
                pure $
                  QuadInstance
                    { qi_size = Float2 (aspectRatio * screenWidth) screenWidth
                    , qi_center = Float2 0 0
                    , qi_color = Float4 0 0 0 coverOpacity
                    }
            }

      when (versusOpacity > 0) $ do
        let botID = unsafeCoerce (0 :: Word8)
            playerID = unsafeCoerce (0 :: Word8)
            radius = 28

        let makeColor :: Team -> ColorVariant -> Float4
            makeColor team colorVar =
              flip Util.fromRGB_A versusOpacity $
                viewPlayerColor
                  theme
                  playerID
                  teamView
                  (PlayerColor team colorVar (BotActor botID))

            makePrimaryColor :: Team -> Float4
            makePrimaryColor team =
              flip Util.fromRGB_A versusOpacity $
                primaryPlayerColor theme $
                  viewTeamColor teamView team

            makePos :: Team -> Float -> Float -> Float2
            makePos team x y =
              let dir = case team of TeamW -> 1; TeamE -> -1
                  vel = 0.02 * dir
                  offset = vel * gameplaySeconds
              in  Float2 ((x + offset) * radius * 2) (y * radius * 2)

        let angle = 0.05 * secondsUntilRespawn
            vel = 0.15
            flashSeconds = 0.7 * (gameplaySeconds - 1)
            (flashW, flashE) =
              case view player_team player of
                TeamW -> (flashSeconds, -1)
                TeamE -> (-1, flashSeconds)

        drawOverseer (gle_overseer glEnv) $
          OverseerVParams
            { ovp_aspectRatio = aspectRatio
            , ovp_coordSystem =
                CS.makeCoordSystem 425 (V2 CS.PreserveAxis CS.PreserveAxis) (Float2 0 0)
            , ovp_testParams = gp_test params
            , ovp_instances =
                [ makeOverseer radius Templar flashW (-angle + 0.00) (-vel) (makePos TeamW (-2.5) 0.0) (makePrimaryColor TeamW)
                , makeOverseer radius Zealot (-1) (angle + 0.15) (-vel) (makePos TeamW (-4.0) (-1.5)) (makeColor TeamW Color1)
                , makeOverseer radius Templar (-1) (angle + 0.30) (-vel) (makePos TeamW (-5.5) 0.0) (makeColor TeamW Color2)
                , makeOverseer radius Zealot (-1) (-angle + 0.45) (-vel) (makePos TeamW (-4.0) 1.5) (makeColor TeamW Color3)
                , makeOverseer radius Templar flashE (angle + 0.00) vel (makePos TeamE 2.5 0.0) (makePrimaryColor TeamE)
                , makeOverseer radius Zealot (-1) (-angle + 0.60) vel (makePos TeamE 4.0 (-1.5)) (makeColor TeamE Color1)
                , makeOverseer radius Zealot (-1) (-angle + 0.75) vel (makePos TeamE 5.5 0.0) (makeColor TeamE Color2)
                , makeOverseer radius Templar (-1) (angle + 0.90) vel (makePos TeamE 4.0 1.5) (makeColor TeamE Color3)
                ]
            }

        let text_y = 0.675 * screenWidth
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontLarge
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeLarge)
                  (Float4 0 0 0 versusOpacity)
                  [
                    ( CharAlignCenter
                    , Float2 (0.235 * screenWidth) text_y
                    ,
                      [
                        ( Util.fromRGB_A
                            (teamDefaultColor theme (viewTeamColor teamView TeamW))
                            versusOpacity
                        , "Formalists"
                        )
                      ]
                    )
                  ,
                    ( CharAlignCenter
                    , Float2 (0.765 * screenWidth) text_y
                    ,
                      [
                        ( Util.fromRGB_A
                            (teamDefaultColor theme (viewTeamColor teamView TeamE))
                            versusOpacity
                        , "Purists"
                        )
                      ]
                    )
                  ,
                    ( CharAlignCenter
                    , Float2 (0.5 * screenWidth) (0.5 * screenWidth)
                    ,
                      [
                        ( Float4 0.45 0.45 0.45 versusOpacity
                        , "vs"
                        )
                      ]
                    )
                  ]
            }

makeOverseer ::
  Float -> PlayerClass -> Float -> Float -> Float -> Float2 -> Float4 -> OverseerInstance
makeOverseer radius playerClass flashSeconds angle dir center color =
  OverseerInstance
    { oi_radius = radius
    , oi_pos = center
    , oi_prevPos1 = center + Float2 (radius * dir) 0
    , oi_prevPos2 = center + Float2 (radius * dir * 2) 0
    , oi_prevPos3 = center + Float2 (radius * dir * 3) 0
    , oi_prevPos4 = center + Float2 (radius * dir * 4) 0
    , oi_prevPos5 = center + Float2 (radius * dir * 5) 0
    , oi_prevPos6 = center + Float2 (radius * dir * 6) 0
    , oi_prevPos7 = center + Float2 (radius * dir * 7) 0
    , oi_prevPos8 = center + Float2 (radius * dir * 8) 0
    , oi_prevPos9 = center + Float2 (radius * dir * 9) 0
    , oi_prevPos10 = center + Float2 (radius * dir * 10) 0
    , oi_healthFraction = 1
    , oi_shieldsFraction = 0
    , oi_color = color
    , oi_recentDamage = 0
    , oi_isInvulnerable = False
    , oi_class = playerClass
    , oi_angleOffset = 0
    , oi_psiStormReadySeconds = flashSeconds
    , oi_angleFins = angle
    , oi_ticksSinceDash = Nothing
    , oi_shieldColor = 1 -- should be unused
    }

respawnTips :: Vector String
respawnTips =
  Vector.fromList . map ("Tip: " ++) $
    [ "Collisions deal equal damage to both entities" -- not true when invulnerable
    , "Harder collisions deal more damage"
    , "Collision damage cannot exceed the health of either entity"
    , "Overseer regeneration starts slow but accelerates"
    , "Damage to overseers stops their health regeneration"
    , "Damage blocked by overseer shields does not stop health regeneration"
    , "Drones regenerate faster the more damaged they are"
    , "Drones respawn after a fixed time"
    , "Charging drone dash reduces movement potential but increases flexibility"
    , "Dashing deals no damage on its own"
    , "Overseers and their drones are briefly invincible after respawning"
    , "Obstacles repel entities harder the closer they are"
    , "An overseer's shield capacity is equal to its health capacity"
    , "Psionic storm is useful for escaping"
    , "Your cursor cannot move beyond a fixed radius from your overseer"
    , "When casting psionic storm, charge drone dash to keep your drones in place"
    , "Don't rely on bots to play the objective (they are bad at it)"
    ]

numRespawnTips :: Int
numRespawnTips = Vector.length respawnTips
