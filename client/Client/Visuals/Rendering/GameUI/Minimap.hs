module Client.Visuals.Rendering.GameUI.Minimap (
  drawMinimap,
) where

import Control.Monad.Trans.Reader
import Data.List (partition)
import Data.Maybe (catMaybes, mapMaybe)
import Data.Vector.Storable.Sized qualified as VSS
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float3)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear (V2 (V2))
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Debug
import Pixelpusher.Game.MapPowerups (mapPowerupSpawnLocations)
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.State
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.Cross
import Client.Visuals.Shaders.MinimapBackground
import Client.Visuals.Shaders.PingFlash
import Client.Visuals.Shaders.Ring
import Client.Visuals.SharedVisuals (
  damageFilterWeight,
  maxAspectRatio,
  shieldPowerupSpawnerRadius,
 )
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView

--------------------------------------------------------------------------------

drawMinimap ::
  GameScene ->
  Theme ->
  TeamView ->
  Maybe Player ->
  ReaderT (GLEnv, Int, Float) GL Float
drawMinimap gameScene theme teamView mPlayer =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let GameScene
          { scene_gameParams = params
          , scene_time = time
          , scene_flags = flags
          , scene_renderingComps
          , scene_particles
          } = gameScene
        RenderingComponents
          { rcs_teamBases = teamBases
          , rcs_overseers = overseers
          , rcs_spawnAreas = spawnAreas
          } = scene_renderingComps
        overseerRemains = particles_overseerRemains scene_particles
        flagCaptures =
          (mapMaybe . traverse) getFlagPickupMessage $
            particles_logMsg scene_particles

    let screenSize' = fromIntegral screenSize
        minimapCoordSystem' = minimapCoordSystem glEnv screenSize' aspectRatio
        centeredViewportCS = CS.centeredViewport screenSize

    let mActorID = view player_actorID <$> mPlayer

    -- Overseers
    let (visibleEnemyOverseers, alliedOverseers) =
          case (gp_requestFogOfWar params, fmap (view player_team) mPlayer) of
            (True, Just team) ->
              filterOverseers
                (or_team . rendering_entityRendering)
                rendering_pos
                team
                Nothing
                overseers
            _ -> ([], overseers)
        (playerOverseers, filteredOverseers) =
          let (playerOverseers', otherFilteredOverseers) =
                partition
                  ( (== mActorID)
                      . Just
                      . or_actorID
                      . rendering_entityRendering
                  )
                  (visibleEnemyOverseers <> alliedOverseers)
          in  -- render player overseer first so that it appears below other
              -- overseers
              (playerOverseers', otherFilteredOverseers ++ playerOverseers')

    -- Background
    drawMinimapBackground (gle_minimapBackground glEnv) $
      let coordSystem =
            CS.composeInvCoordSystem minimapCoordSystem' centeredViewportCS
      in  MinimapBackgroundVParams
            { mbvp_aspectRatio = aspectRatio
            , mbvp_viewScale =
                Float.map2 recip $ CS.cs_axisScaling coordSystem
            , mbvp_viewCenter =
                CS.cs_origin coordSystem
            , mbvp_visionCenters =
                let take' = \case [] -> (0, []); x : xs -> (x, xs)
                    positions =
                      flip map alliedOverseers $
                        CS.fromPos (CS.gameWorld params)
                          . rendering_pos
                in  VSS.unfoldrN take' positions
            , mbvp_visionCentersCount = fromIntegral (length alliedOverseers)
            , mbvp_visionRadius =
                CS.fromScalar (CS.gameWorld params) CC.viewRadius
            , mbvp_fuzziness = 0.01
            , mbvp_visionColor = theme_minimap_background_areaVisibleColor theme
            , mbvp_fogColor = theme_minimap_background_areaFogColor theme
            }

    -- Team bases
    let teamBaseCircles = flip map teamBases $ \rc ->
          CircleInstance
            { ci_size =
                CS.intoScalar minimapCoordSystem' $
                  CS.fromScalar (CS.gameWorld params) (rendering_radius rc)
            , ci_center =
                CS.intoPos minimapCoordSystem' $
                  CS.fromPos (CS.gameWorld params) (rendering_pos rc)
            , ci_color =
                let team =
                      tbr_team $ rendering_entityRendering rc
                in  case viewTeamColor teamView team of
                      TeamRed -> theme_minimap_teamRed_baseColor theme
                      TeamBlue -> theme_minimap_teamBlue_baseColor theme
            , ci_fuzziness = 1
            }

    -- Team spawn areas
    let spawnAreaRings = flip map spawnAreas $ \rc ->
          RingInstance
            { ri_modelScale =
                CS.intoScalar minimapCoordSystem' $
                  CS.fromScalar (CS.gameWorld params) (rendering_radius rc)
            , ri_modelTrans =
                CS.intoPos minimapCoordSystem' $
                  CS.fromPos (CS.gameWorld params) (rendering_pos rc)
            , ri_color =
                let team = tsr_team (rendering_entityRendering rc)
                in  case viewTeamColor teamView team of
                      TeamRed -> theme_minimap_teamRed_spawnColor theme
                      TeamBlue -> theme_minimap_teamBlue_spawnColor theme
            , ri_thickness =
                CS.intoScalar minimapCoordSystem' $
                  CS.fromScalar (CS.gameWorld params) $
                    0.125 * rendering_radius rc
            , ri_fuzziness = 1
            }

    -- Powerup spawners
    let powerupSpawnerRings =
          flip map (mapPowerupSpawnLocations (gp_dynamics params)) $ \(_, pos) ->
            RingInstance
              { ri_modelScale =
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) shieldPowerupSpawnerRadius
              , ri_modelTrans =
                  CS.intoPos minimapCoordSystem' $
                    CS.fromPos (CS.gameWorld params) $
                      Fixed.toFloats pos
              , ri_color = theme_minimap_shieldPowerupSpawnersColor theme
              , ri_thickness =
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) $
                      0.125 * shieldPowerupSpawnerRadius
              , ri_fuzziness = 1
              }

    let (visibleEnemyOverseerRemains, alliedOverseerRemains) =
          case (gp_requestFogOfWar params, fmap (view player_team) mPlayer) of
            (True, Just team) ->
              filterOverseers
                ( (\(PlayerColor team' _ _) -> team')
                    . orp_color
                    . particle_type
                )
                (Fixed.toFloats . orp_pos . particle_type)
                team
                (Just (map rendering_pos alliedOverseers))
                overseerRemains
            _ -> ([], overseerRemains)
        filteredOverseerRemains = visibleEnemyOverseerRemains <> alliedOverseerRemains

    let playerOverseerVisionRings = flip map playerOverseers $ \rc ->
          RingInstance
            { ri_modelScale =
                CS.intoScalar minimapCoordSystem' $
                  CS.fromScalar (CS.gameWorld params) CC.viewRadius
            , ri_modelTrans =
                CS.intoPos minimapCoordSystem' $
                  CS.fromPos (CS.gameWorld params) (rendering_pos rc)
            , ri_color =
                let OverseerRendering{or_team} = rendering_entityRendering rc
                in  case viewTeamColor teamView or_team of
                      TeamRed -> theme_minimap_teamRed_visionRingColor theme
                      TeamBlue -> theme_minimap_teamBlue_visionRingColor theme
            , ri_thickness =
                CS.intoScalar minimapCoordSystem' 0.01
            , ri_fuzziness = 0.1
            }

    let overseerRemainsRings = flip map filteredOverseerRemains $ \particle ->
          RingInstance
            { ri_modelScale =
                (3 *) $
                  CS.intoScalar minimapCoordSystem' $
                    CS.fromScalar (CS.gameWorld params) overseerRadius
            , ri_modelTrans =
                CS.intoPos minimapCoordSystem' $
                  CS.fromPos (CS.gameWorld params) $
                    Fixed.toFloats $
                      orp_pos $
                        particle_type particle
            , ri_color =
                let PlayerColor team _ _ = orp_color (particle_type particle)
                    color =
                      case viewTeamColor teamView team of
                        TeamRed -> theme_minimap_teamRed_overseerDeadColor theme
                        TeamBlue -> theme_minimap_teamBlue_overseerDeadColor theme
                in  Util.fromRGB_A color 1
            , ri_thickness =
                CS.intoScalar minimapCoordSystem' $
                  CS.fromScalar (CS.gameWorld params) (overseerRadius / 3)
            , ri_fuzziness = 1
            }
          where
            overseerRadius =
              Fixed.toFloat $ gp_overseerRadius_base (gp_dynamics params)

    let overseerCircles = flip map filteredOverseers $ \rc ->
          let OverseerRendering{or_team, or_actorID, or_recentDamage} =
                rendering_entityRendering rc
          in  CircleInstance
                { ci_size =
                    (3 *) $
                      CS.intoScalar minimapCoordSystem' $
                        CS.fromScalar (CS.gameWorld params) $
                          rendering_radius rc
                , ci_center =
                    CS.intoPos minimapCoordSystem' $
                      CS.fromPos (CS.gameWorld params) (rendering_pos rc)
                , ci_color =
                    let teamColor = viewTeamColor teamView or_team
                        damageFilter x =
                          let w = damageFilterWeight or_recentDamage
                          in  w + (1 - w) * x
                        baseColor =
                          case teamColor of
                            TeamRed -> theme_minimap_teamRed_overseerAliveColor theme
                            TeamBlue -> theme_minimap_teamBlue_overseerAliveColor theme
                        playerColor =
                          case teamColor of
                            TeamRed -> theme_minimap_teamRed_overseerPlayerColor theme
                            TeamBlue -> theme_minimap_teamBlue_overseerPlayerColor theme
                        color =
                          Float.map3 damageFilter $
                            highlightPlayer
                              or_actorID
                              baseColor
                              playerColor
                    in  Util.fromRGB_A color 1.0
                , ci_fuzziness = 1
                }
          where
            highlightPlayer :: ActorID -> Float3 -> Float3 -> Float3
            highlightPlayer ovActorID baseColor playerColor =
              case mActorID of
                Nothing -> baseColor
                Just actorID ->
                  if actorID == ovActorID
                    then playerColor
                    else baseColor

    drawRing (gle_ring glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = centeredViewportCS
        , svp_testParams = gp_test params
        , svp_instances =
            concat
              [ spawnAreaRings
              , powerupSpawnerRings
              , playerOverseerVisionRings
              , overseerRemainsRings
              ]
        }

    drawCircle (gle_circle glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = centeredViewportCS
        , svp_testParams = gp_test params
        , svp_instances = teamBaseCircles ++ overseerCircles
        }

    -- Flags
    drawCross (gle_cross glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = centeredViewportCS
        , svp_testParams = gp_test params
        , svp_instances = flip map flags $ \(team, pos, _) ->
            CrossInstance
              { ci_size =
                  (3 *) $
                    CS.intoScalar minimapCoordSystem' $
                      CS.fromScalar (CS.gameWorld params) $
                        1.2 * Fixed.toFloat (gp_overseerRadius_base (gp_dynamics params))
              , ci_center =
                  CS.intoPos minimapCoordSystem' $
                    CS.fromPos (CS.gameWorld params) $
                      Fixed.toFloats pos
              , ci_color =
                  let color =
                        case viewTeamColor teamView team of
                          TeamRed -> theme_minimap_teamRed_flagColor theme
                          TeamBlue -> theme_minimap_teamBlue_flagColor theme
                  in  Util.fromRGB_A color 1
              }
        }

    -- Debug marks
    drawCross (gle_cross glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = centeredViewportCS
        , svp_testParams = gp_test params
        , svp_instances =
            let marks = scene_debugMarks gameScene
                makeCrossInstance color pos =
                  CrossInstance
                    { ci_size =
                        (4.5 *) $
                          CS.intoScalar minimapCoordSystem' $
                            CS.fromScalar (CS.gameWorld params) 16
                    , ci_center =
                        CS.intoPos minimapCoordSystem' $
                          CS.fromPos (CS.gameWorld params) $
                            Fixed.toFloats pos
                    , ci_color = color
                    }
            in  catMaybes
                  [ makeCrossInstance markColor1 <$> dm_mark1 marks
                  , makeCrossInstance markColor2 <$> dm_mark2 marks
                  , makeCrossInstance markColor3 <$> dm_mark3 marks
                  , makeCrossInstance markColor4 <$> dm_mark4 marks
                  , makeCrossInstance markColor5 <$> dm_mark5 marks
                  , makeCrossInstance markColor6 <$> dm_mark6 marks
                  ]
        }

    -- Flag steal flashes
    withBlendFunc GL_SRC_ALPHA GL_ONE $
      drawPingFlash (gle_pingFlash glEnv) $
        StandardVParams
          { svp_aspectRatio = aspectRatio
          , svp_coordSystem = centeredViewportCS
          , svp_testParams = gp_test params
          , svp_instances =
              flip map flagCaptures $ \particle ->
                let startTime = particle_startTime particle
                    ticksElapsed = getTicks $ time `diffTime` startTime
                    FlagPickupParticle team pos _ = particle_type particle
                in  PingFlashInstance
                      { pfi_size = CS.intoScalar minimapCoordSystem' 1.0
                      , pfi_center =
                          CS.intoPos minimapCoordSystem' $
                            CS.fromPos (CS.gameWorld params) $
                              Fixed.toFloats pos
                      , pfi_color =
                          case viewTeamColor teamView team of
                            TeamRed ->
                              theme_minimap_teamRed_flagStealPingColor theme
                            TeamBlue ->
                              theme_minimap_teamBlue_flagStealPingColor theme
                      , pfi_ticksElapsed = fromIntegral ticksElapsed
                      }
          }

    let y_bottom = view _2 $ CS.intoPos minimapCoordSystem' $ Float2 0 (-1)
    pure y_bottom

minimapCoordSystem :: GLEnv -> Float -> Float -> CS.CoordSystem
minimapCoordSystem glEnv screenSize aspectRatio =
  CS.makeCoordSystem radius (V2 CS.PreserveAxis CS.PreserveAxis) center
  where
    a = max 1 . min maxAspectRatio $ aspectRatio
    r = a + 2 - 2 * sqrt (a + 1)

    topRight =
      let x = 0.5 * screenSize
      in  Float2 (x * a) x
    padding = gle_uiPadding glEnv * 0.25

    overlapFrac = 0.085
    radius = 0.5 * (screenSize * (r + 0.5 * overlapFrac) - padding)
    center = Float.map2 (subtract (padding + radius)) topRight

filterOverseers ::
  (a -> Team) -> (a -> Float2) -> Team -> Maybe [Float2] -> [a] -> ([a], [a])
filterOverseers getTeam getPos alliedTeam mVisionPoints overseers =
  let (alliedOverseers, enemyOverseers) =
        partition ((==) alliedTeam . getTeam) overseers
      alliedPositions =
        case mVisionPoints of
          Nothing -> map getPos alliedOverseers
          Just visionPoints -> visionPoints
      inAlliedControlRadius pos =
        any
          ((< CC.viewRadius) . Float.norm2 . subtract pos)
          alliedPositions
      visibleEnemyOverseers =
        filter (inAlliedControlRadius . getPos) enemyOverseers
  in  (visibleEnemyOverseers, alliedOverseers)
