{-# LANGUAGE BlockArguments #-}

module Client.Visuals.Rendering.GameUI.Overlay (
  Overlay (..),
  EnableRespawnTips (..),
  drawOverlay,
) where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Data.Bifunctor (bimap)
import Data.ByteString.Char8 qualified as BS
import Data.Char (ord)
import Data.Foldable (foldl')
import Data.IntMap.Strict qualified as IM
import Data.List (partition, scanl')
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Custom.Util
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStats
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.State
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Config.Controls
import Client.Config.Keybindings
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Rendering.DarkLayer (drawDarkLayer)
import Client.Visuals.Rendering.GameUI.HelpOverlay (
  drawDetailedMechanicsOverlay,
  drawHelpOverlay,
  drawThemeHelp,
 )
import Client.Visuals.Rendering.GameUI.PlayerClassesOverlay
import Client.Visuals.Rendering.GameUI.Respawn
import Client.Visuals.Rendering.GameUI.VisualGlossary (drawVisualGlossary)
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Char
import Client.Visuals.SharedVisuals (scoreTeamColors)
import Client.Visuals.TeamView
import Client.Visuals.Text (shadowText)

--------------------------------------------------------------------------------
-- Overlays

data Overlay
  = NoOverlay
  | HelpOverlayInstructions
  | HelpOverlayVisuals
  | HelpOverlayDetailedMechanics
  | HelpOverlayPlayerClasses
  | HelpOverlayThemes
  | StatsOverlay [(PlayerName, Team, AggregatePlayerStats)]
  | -- | Next round time
    IntermissionOverlay
      Team
      Time

drawOverlay ::
  GameParams ->
  EnableRespawnTips ->
  GameScene ->
  SceneDecorations ->
  Keybindings ->
  ControlOptions ->
  ReaderT (GLEnv, Int, Float) GL ()
drawOverlay params enableRespawnTips scene decorations keybindings controlOpts =
  case chooseOverlay scene decorations of
    -- None
    NoOverlay ->
      -- Respawn message
      case sceneDeco_perspective decorations of
        GlobalPerspective -> pure ()
        FreePerspective -> pure ()
        PlayerPerspective playerInfo ->
          case pv_overseerView (pi_playerView playerInfo) of
            OV_Alive{} -> pure ()
            OV_Dead
              OverseerViewDead{ovd_deathTime, ovd_respawnTime} ->
                drawRespawnMessage
                  params
                  enableRespawnTips
                  (sceneDeco_theme decorations)
                  (sceneDeco_teamView decorations)
                  (pi_player playerInfo)
                  (pv_playerClass (pi_playerView playerInfo))
                  (scene_time scene)
                  ovd_deathTime
                  ovd_respawnTime
    -- Help
    HelpOverlayInstructions -> do
      let gameParams = scene_gameParams scene
      drawDarkLayer (gp_test gameParams)
      drawHelpOverlay keybindings controlOpts
    HelpOverlayVisuals -> do
      let gameParams = scene_gameParams scene
      drawDarkLayer (gp_test gameParams)
      drawVisualGlossary gameParams
    HelpOverlayDetailedMechanics -> do
      let gameParams = scene_gameParams scene
      drawDarkLayer (gp_test gameParams)
      drawDetailedMechanicsOverlay
    HelpOverlayPlayerClasses -> do
      let gameParams = scene_gameParams scene
      drawDarkLayer (gp_test gameParams)
      drawPlayerClassesOverlay gameParams
    HelpOverlayThemes -> do
      let gameParams = scene_gameParams scene
      drawDarkLayer (gp_test gameParams)
      drawThemeHelp
    -- Stats
    StatsOverlay leaderboardInfo -> do
      drawDarkLayer (gp_test (scene_gameParams scene))
      (glEnv, screenSize, _) <- ask
      let screenWidth = fromIntegral screenSize
          spacing = fromIntegral $ fa_fontSize $ gle_fontAtlasLarge glEnv
          y = screenWidth - 2 * spacing
      drawLeaderboard
        y
        teamView
        leaderboardInfo
    -- Intermission
    IntermissionOverlay winningTeam nextRoundTime ->
      drawIntermissionOverlay
        (gp_test (scene_gameParams scene))
        teamView
        (nextRoundTime `diffTime` scene_time scene)
        winningTeam
        (WIM.map fst (scene_players scene))
  where
    teamView = sceneDeco_teamView decorations

chooseOverlay :: GameScene -> SceneDecorations -> Overlay
chooseOverlay scene decorations =
  case sceneDeco_renderHelpOverlay decorations of
    ShowHelpInstructions ->
      HelpOverlayInstructions
    ShowHelpVisuals ->
      HelpOverlayVisuals
    ShowHelpDetailedMechanics ->
      HelpOverlayDetailedMechanics
    ShowHelpPlayerClasses ->
      HelpOverlayPlayerClasses
    ShowHelpThemes ->
      HelpOverlayThemes
    HideHelp ->
      case sceneDeco_perspective decorations of
        FreePerspective -> NoOverlay
        GlobalPerspective -> NoOverlay
        PlayerPerspective PlayerInfo{} -> do
          case sceneDeco_renderStatsOverlay decorations of
            DisplayStats ->
              let leaderboardInfo =
                    makeLeaderboardInfo $ WIM.map fst $ scene_players scene
              in  StatsOverlay leaderboardInfo
            HideStats ->
              case scene_gamePhase scene of
                GamePhase_Game _ _ -> NoOverlay
                GamePhase_Intermission roundEndTime nextRoundTime winningTeam _teamScore ->
                  if scene_time scene >= addTicks C.matchEndSfxDelayTicks roundEndTime
                    then IntermissionOverlay winningTeam nextRoundTime
                    else NoOverlay

--------------------------------------------------------------------------------

drawIntermissionOverlay ::
  TestParams ->
  TeamView ->
  Ticks ->
  Team ->
  WIM.IntMap ActorID Player ->
  ReaderT (GLEnv, Int, Float) GL ()
drawIntermissionOverlay
  testParams
  teamView
  ticksUntilNextRound
  winningTeam
  players = do
    drawDarkLayer testParams

    (glEnv, screenSize, aspectRatio) <- ask
    let screenWidth = fromIntegral screenSize
        color = scoreTeamColors (viewTeamColor teamView winningTeam)
        teamName = teamNameAllCaps winningTeam
        spacing = fromIntegral $ fa_fontSize $ gle_fontAtlasLarge glEnv
        x = screenWidth / 2
        y1 = screenWidth * 7 / 8
        y2 = y1 - spacing
        y3 = y2 - spacing

    do
      let y_leaderboard = y3 - spacing * 2
          leaderboardInfo = makeLeaderboardInfo players
      drawLeaderboard y_leaderboard teamView leaderboardInfo

    lift $ do
      drawChar (gle_char glEnv) $
        let fontSmall = gle_fontAtlasSmall glEnv
            fontSizeSmall = fromIntegral $ fa_fontSize fontSmall
        in  CharVParams
              { cvp_aspectRatio = aspectRatio
              , cvp_fontAtlas = fontSmall
              , cvp_screenSize = fromIntegral screenSize
              , cvp_lines =
                  shadowText fontSizeSmall (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y1
                      , pure (Float4 0.7 0.7 0.7 1.0, "winner")
                      )
              }
      drawChar (gle_char glEnv) $
        let fontLarge = gle_fontAtlasLarge glEnv
            fontSizeLarge = fromIntegral $ fa_fontSize fontLarge
        in  CharVParams
              { cvp_aspectRatio = aspectRatio
              , cvp_fontAtlas = fontLarge
              , cvp_screenSize = fromIntegral screenSize
              , cvp_lines =
                  shadowText fontSizeLarge (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y2
                      , pure (Util.fromRGB_A color 1, teamName)
                      )
              }

      let countdownSeconds =
            succ $ (fromIntegral ticksUntilNextRound - 1) `div` C.tickRate_hz
      drawChar (gle_char glEnv) $
        let fontRegular = gle_fontAtlasRegular glEnv
            fontSizeRegular = fromIntegral $ fa_fontSize fontRegular
            leftPad xs =
              replicate (max 0 (2 - length xs)) '0' ++ xs
        in  CharVParams
              { cvp_aspectRatio = aspectRatio
              , cvp_fontAtlas = fontRegular
              , cvp_screenSize = fromIntegral screenSize
              , cvp_lines =
                  shadowText fontSizeRegular (Float4 0 0 0 1) $
                    pure
                      ( CharAlignCenter
                      , Float2 x y3
                      , pure $
                          (,) (Float4 0.5 0.5 0.5 1.0) $
                            concat
                              [ "a new round will start in "
                              , leftPad (show countdownSeconds)
                              , " seconds"
                              ]
                      )
              }

makeLeaderboardInfo ::
  WIM.IntMap ActorID Player ->
  [(PlayerName, Team, AggregatePlayerStats)]
makeLeaderboardInfo players =
  players
    & WIM.toList
    -- & filter (isActorPlayer . fst)
    & map \(_, p) ->
      ( p ^. player_name
      , p ^. player_team
      , getAggregateStats (player_stats p)
      )

drawLeaderboard ::
  Float ->
  TeamView ->
  [(PlayerName, Team, AggregatePlayerStats)] ->
  ReaderT (GLEnv, Int, Float) GL ()
drawLeaderboard y_top teamView playerStats =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let fontSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontSmall
        testChar = '0'
        smallCharWidth =
          cd_advance $
            ac_charDims $
              expectJust ("font atlas missing character: " ++ [testChar]) $
                IM.lookup (ord testChar) $
                  fa_charMap fontSmall

        rowHeight =
          fromIntegral @Int $
            round @Double $
              (\x -> x * 9 / 8) $
                fromIntegral $
                  fa_fontSize $
                    gle_fontAtlasSmall glEnv
        y_row rowNo = y_top - rowHeight * fromIntegral rowNo
        padding = gle_uiPadding glEnv

        longestNameLength =
          maximum $ map (BS.length . getPlayerName . view _1) playerStats
        colWidths =
          flip map statsFieldLabels $
            fromIntegral @Int . ceiling . (* smallCharWidth) . (+ 0.5) . fromIntegral . uncurry max . bimap length length
        x_totalColumnWidth = foldl' (+) 0 colWidths
        x_colBoundaryOffsets = scanl' (+) 0 colWidths
        x_colCenterOffsets =
          let avg x y = 0.5 * (x + y)
          in  zipWith avg x_colBoundaryOffsets (drop 1 x_colBoundaryOffsets)

        nameWidth = smallCharWidth * fromIntegral longestNameLength
        totalWidth = nameWidth + x_totalColumnWidth + padding

        x_namesRight = 0.5 * (fromIntegral screenSize - totalWidth) + nameWidth
        x_statsLeft = x_namesRight + padding

    let statsCharLines :: Int -> [String] -> [CharLine]
        statsCharLines rowNo fields =
          flip map (zip x_colCenterOffsets fields) $ \(x_offset, field) ->
            (CharAlignCenter, Float2 (x_statsLeft + x_offset) (y_row rowNo), [(white, field)])

    let rowCharLines ::
          Int ->
          (PlayerName, Team, AggregatePlayerStats) ->
          [CharLine]
        rowCharLines rowNo (name, team, stats) =
          let statsLines = statsCharLines rowNo $ statsFields stats
              nameLine =
                ( CharAlignRight
                , Float2 x_namesRight (y_row rowNo)
                ,
                  [
                    ( case viewTeamColor teamView team of
                        TeamRed -> red
                        TeamBlue -> blue
                    , BS.unpack $ getPlayerName name
                    )
                  ]
                )
          in  nameLine : statsLines

    let (westStats, eastStats) =
          partition ((== TeamW) . view _2) playerStats
        (labels, sublabels) = unzip statsFieldLabels
        labelCharLines = statsCharLines 0 labels
        sublabelCharLines = statsCharLines 1 sublabels
        westCharLines = concat $ zipWith rowCharLines [3 ..] westStats
        eastCharLines =
          concat $
            zipWith rowCharLines [3 + 1 + length westStats ..] eastStats
        charLines =
          concat
            [labelCharLines, sublabelCharLines, westCharLines, eastCharLines]

    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = gle_fontAtlasSmall glEnv
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            shadowText
              (fromIntegral fontSizeSmall)
              (Float4 0 0 0 1)
              charLines
        }
  where
    white = Float4 0.7 0.7 0.7 1
    blue = Float4 0.5 0.75 1.0 1
    red = Float4 1 0.5 0.5 1

statsFieldLabels :: [(String, String)] -- (row 1, row 2)
statsFieldLabels =
  [ ("Flag", "captures")
  , ("Kills/Deaths/", "Assists")
  , ("Player", "damage")
  ]

statsFields :: AggregatePlayerStats -> [String]
statsFields stats =
  [ show (getFlagsCaptured stats)
  , concat
      [ show (getKills stats)
      , "/"
      , show (getDeaths stats)
      , "/"
      , show (getAssists stats)
      ]
  , show (getDamageDealt stats)
  ]
