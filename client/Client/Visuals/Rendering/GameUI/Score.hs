{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.Rendering.GameUI.Score (
  drawScore,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (foldl')
import Data.Strict.Tuple (Pair ((:!:)))

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.State
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamScore

import Client.Theme
import Client.Visuals.CoordinateSystems (viewport)
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.DSL (GLKind (..))
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Flag
import Client.Visuals.Shaders.Overseer
import Client.Visuals.SharedVisuals (scoreTeamColors)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Text (shadowText)

--------------------------------------------------------------------------------

drawScore :: GameScene -> SceneDecorations -> ReaderT (GLEnv, Int, Float) GL ()
drawScore GameScene{..} SceneDecorations{..} =
  case scene_gamePhase of
    GamePhase_Game _ teamScore ->
      drawTeamScorePlay
        scene_gameParams
        sceneDeco_theme
        sceneDeco_teamView
        teamScore
        (countOverseers (snd <$> scene_players))
    GamePhase_Intermission _ _ _ finalScore ->
      drawTeamScoreIntermission sceneDeco_teamView finalScore

countOverseers :: (Foldable f) => f PlayerView -> Team -> (Int, Int)
countOverseers playerViews =
  let aliveCount :!: deadCount =
        foldl' countOverseer (mempty :!: mempty) playerViews
  in  \case
        TeamW -> (teamScore_w aliveCount, teamScore_w deadCount)
        TeamE -> (teamScore_e aliveCount, teamScore_e deadCount)
  where
    countOverseer ::
      Pair TeamScore TeamScore -> PlayerView -> Pair TeamScore TeamScore
    countOverseer (aliveCount :!: deadCount) playerView =
      let team = pv_team playerView
      in  case pv_overseerView playerView of
            OV_Alive{} ->
              incrementTeamScore team aliveCount :!: deadCount
            OV_Dead{} ->
              aliveCount :!: incrementTeamScore team deadCount

drawTeamScorePlay ::
  GameParams ->
  Theme ->
  TeamView ->
  TeamScore ->
  (Team -> (Int, Int)) ->
  ReaderT (GLEnv, Int, Float) GL ()
drawTeamScorePlay params theme teamView teamScore overseerCounts =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let screenSize' = fromIntegral screenSize
        padding = gle_uiPadding glEnv

        fontSizeSmall = fa_fontSize $ gle_fontAtlasSmall glEnv
        iconWidth = fromIntegral fontSizeSmall * 1.1

        fadeFilter = Util.overRGB (Float.map3 (* 0.33))
        fadeFilter2 = Util.overRGB (Float.map3 (* 0.17))

    let lineSpacing = fromIntegral fontSizeSmall
        top = screenSize' - padding - lineSpacing * 3 / 4 -- adjusted by hand
        mid_x = screenSize' / 4
        flags_x = mid_x - iconWidth
        posFlags = Float2 flags_x top
        requiredScore = gp_flagsToWin params

        overseers_x = mid_x + iconWidth
        posOverseers = Float2 overseers_x top

    drawFlag (gle_flag glEnv) $
      StandardVParams
        { svp_aspectRatio = aspectRatio
        , svp_coordSystem = viewport screenSize
        , svp_testParams = gp_test params
        , svp_instances =
            let radius = iconWidth * 0.375
                westFlags =
                  let baseColor =
                        case viewTeamColor teamView TeamW of
                          TeamRed -> theme_ui_teamRed_flagIconColor theme
                          TeamBlue -> theme_ui_teamBlue_flagIconColor theme
                      score = teamScore_w teamScore
                      westColors =
                        replicate score baseColor
                          ++ replicate (requiredScore - score) (fadeFilter2 baseColor)
                      westPositions =
                        flip map [0 :: Int ..] $ \i ->
                          posFlags
                            + Float2
                              (-iconWidth * fromIntegral i)
                              (-iconWidth * 0.55)
                  in  zipWith (makeFlag radius) westColors westPositions
                eastFlags =
                  let baseColor =
                        case viewTeamColor teamView TeamE of
                          TeamRed -> theme_ui_teamRed_flagIconColor theme
                          TeamBlue -> theme_ui_teamBlue_flagIconColor theme
                      score = teamScore_e teamScore
                      eastColors =
                        replicate score baseColor
                          ++ replicate (requiredScore - score) (fadeFilter2 baseColor)
                      eastPositions =
                        flip map [0 :: Int ..] $ \i ->
                          posFlags
                            + Float2
                              (-iconWidth * fromIntegral i)
                              (iconWidth * 0.55)
                  in  zipWith (makeFlag radius) eastColors eastPositions
            in  westFlags ++ eastFlags
        }

    drawOverseer (gle_overseer glEnv) $
      OverseerVParams
        { ovp_aspectRatio = aspectRatio
        , ovp_coordSystem = viewport screenSize
        , ovp_testParams = gp_test params
        , ovp_instances =
            let radius = iconWidth * 0.335
                westOverseers =
                  let (nAlive, nDead) = overseerCounts TeamW
                      westColor =
                        case viewTeamColor teamView TeamW of
                          TeamRed -> theme_ui_teamRed_overseerIconColor theme
                          TeamBlue -> theme_ui_teamBlue_overseerIconColor theme
                      westColors =
                        replicate nAlive westColor
                          ++ replicate nDead (fadeFilter westColor)
                      westPositions =
                        flip map [0 :: Int ..] $ \i ->
                          posOverseers
                            + Float2
                              (iconWidth * fromIntegral i)
                              (-iconWidth * 0.55)
                  in  zipWith (makeOverseer radius) westColors westPositions
                eastOverseers =
                  let (nAlive, nDead) = overseerCounts TeamE
                      eastColor =
                        case viewTeamColor teamView TeamE of
                          TeamRed -> theme_ui_teamRed_overseerIconColor theme
                          TeamBlue -> theme_ui_teamBlue_overseerIconColor theme
                      eastColors =
                        replicate nAlive eastColor
                          ++ replicate nDead (fadeFilter eastColor)
                      eastPositions =
                        flip map [0 :: Int ..] $ \i ->
                          posOverseers
                            + Float2
                              (iconWidth * fromIntegral i)
                              (iconWidth * 0.55)
                  in  zipWith (makeOverseer radius) eastColors eastPositions
            in  westOverseers ++ eastOverseers
        }

makeFlag :: Float -> Float4 -> Float2 -> FlagInstance Cpu
makeFlag radius color center =
  FlagInstance
    { fi_size = radius
    , fi_center = center
    , fi_color = color
    }

makeOverseer :: Float -> Float4 -> Float2 -> OverseerInstance
makeOverseer radius color center =
  OverseerInstance
    { oi_radius = radius
    , oi_pos = center
    , oi_prevPos1 = center -- no shadow
    , oi_prevPos2 = center -- no shadow
    , oi_prevPos3 = center -- no shadow
    , oi_prevPos4 = center -- no shadow
    , oi_prevPos5 = center -- no shadow
    , oi_prevPos6 = center -- no shadow
    , oi_prevPos7 = center -- no shadow
    , oi_prevPos8 = center -- no shadow
    , oi_prevPos9 = center -- no shadow
    , oi_prevPos10 = center -- no shadow
    , oi_healthFraction = 1
    , oi_shieldsFraction = 0
    , oi_color = color
    , oi_recentDamage = 0
    , oi_isInvulnerable = False
    , oi_class = Templar
    , oi_angleOffset = 0
    , oi_psiStormReadySeconds = -1
    , oi_angleFins = 0
    , oi_ticksSinceDash = Nothing
    , oi_shieldColor = 1 -- should be unused
    }

drawTeamScoreIntermission ::
  TeamView -> TeamScore -> ReaderT (GLEnv, Int, Float) GL ()
drawTeamScoreIntermission teamView finalScore =
  ReaderT $ \(glEnv, screenSize, aspectRatio) ->
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        padding = gle_uiPadding glEnv

        screenSize' = fromIntegral screenSize
        center_x = screenSize' / 2
        lineSpacing =
          fromIntegral $
            (`div` 3) $
              (* 2) fontSizeLarge -- adjusted by hand
        top = screenSize' - padding - lineSpacing

        posRight = Float2 center_x top
        posLeft = Float2 center_x top

        scoreTextW = show $ teamScore_w finalScore
        scoretextE = show $ teamScore_e finalScore
    in  drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlasLarge
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowText
                  (fromIntegral fontSizeLarge)
                  (Float4 0 0 0 1)
                  [
                    ( CharAlignLeft
                    , posRight
                    ,
                      [
                        ( Util.fromRGB_A
                            (scoreTeamColors (viewTeamColor teamView TeamE))
                            1
                        , " " ++ scoretextE
                        )
                      ]
                    )
                  ,
                    ( CharAlignRight
                    , posLeft
                    ,
                      [
                        ( Util.fromRGB_A
                            (scoreTeamColors (viewTeamColor teamView TeamW))
                            1
                        , scoreTextW ++ " "
                        )
                      ]
                    )
                  ]
            }
