{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.Rendering.GameUI.HelpOverlay (
  drawHelpOverlay,
  drawDetailedMechanicsOverlay,
  drawCredits,
  drawThemeHelp,
) where

import Control.Monad (void)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Lens.Micro

import Pixelpusher.Custom.Float (Float2 (Float2), Float4 (Float4))

import Client.Config.Controls
import Client.Config.Keybindings
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text (
  shadowTextWith,
  stackIndentedLinesUpward,
 )

drawHelpOverlay :: Keybindings -> ControlOptions -> ReaderT (GLEnv, Int, Float) GL ()
drawHelpOverlay keybindings controlOpts = do
  (glEnv, screenSize, _) <- ask
  let bottomMargin = 0.07 * fromIntegral screenSize
      helpLines = helpOverlayLines keybindings controlOpts glEnv screenSize
  drawHelpText bottomMargin helpLines

drawDetailedMechanicsOverlay :: ReaderT (GLEnv, Int, Float) GL ()
drawDetailedMechanicsOverlay = do
  (glEnv, screenSize, _) <- ask
  let bottomMargin = 0.4 * fromIntegral screenSize
      helpLines = detailedMechanicsLines glEnv screenSize
  drawHelpText bottomMargin helpLines

drawCredits :: ReaderT (GLEnv, Int, Float) GL ()
drawCredits = do
  (glEnv, screenSize, _) <- ask
  let bottomMargin = 0.2 * fromIntegral screenSize
      helpLines = creditsLines glEnv screenSize
  drawHelpText bottomMargin helpLines

drawThemeHelp :: ReaderT (GLEnv, Int, Float) GL ()
drawThemeHelp = do
  (glEnv, screenSize, _) <- ask
  let bottomMargin = 0.2 * fromIntegral screenSize
      helpLines = themeHelpLines glEnv screenSize
  drawHelpText bottomMargin helpLines

data HelpScreenLine
  = DrawLines FontAtlas [(Float4, Float, String)]
  | DrawLinesColumns FontAtlas [(Float4, Float, Float, String, String)]
  | DrawLinesColumns2 FontAtlas [(Float4, Float, Float, Float, String, String, String)]
  | SmallSpacer
  | RegularSpacer

drawHelpText :: Float -> [HelpScreenLine] -> ReaderT (GLEnv, Int, Float) GL ()
drawHelpText bottomMargin helpScreenLines = ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
  let drawLines' :: FontAtlas -> Float2 -> [(Float4, Float, String)] -> GL Float2
      drawLines' fontAtlas startPos helpLines = do
        let fontSize = fa_fontSize fontAtlas
            lineSpacing = fromIntegral $ (fontSize * 3) `div` 2
            (finalPos, charLines) =
              stackIndentedLinesUpward
                CharAlignLeft
                startPos
                lineSpacing
                helpLines
        drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlas
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                shadowTextWith (0.5 * fromIntegral fontSize) darken charLines
            }
        pure $! finalPos

  let drawLines ::
        FontAtlas -> [(Float4, Float, String)] -> StateT Float2 GL ()
      drawLines fontAtlas helpLines = do
        startPos <- get
        finalPos <- lift $ drawLines' fontAtlas startPos helpLines
        put $! finalPos

  let drawLinesColumns ::
        FontAtlas -> [(Float4, Float, Float, String, String)] -> StateT Float2 GL ()
      drawLinesColumns fontAtlas helpLines = do
        startPos <- get
        _finalPos <-
          lift $
            drawLines' fontAtlas startPos $
              flip map helpLines $
                \(color, indent1, _indent2, text1, _text2) -> (color, indent1, text1)
        finalPos <-
          lift $
            drawLines' fontAtlas startPos $
              flip map helpLines $
                \(color, _indent1, indent2, _text1, text2) -> (color, indent2, text2)
        put $! finalPos

  let drawLinesColumns2 ::
        FontAtlas ->
        [(Float4, Float, Float, Float, String, String, String)] ->
        StateT Float2 GL ()
      drawLinesColumns2 fontAtlas helpLines = do
        startPos <- get
        _finalPos <-
          lift $
            drawLines' fontAtlas startPos $
              flip map helpLines $
                \(color, indent1, _indent2, _indent3, text1, _text2, _text3) ->
                  (color, indent1, text1)
        _finalPos <-
          lift $
            drawLines' fontAtlas startPos $
              flip map helpLines $
                \(color, _indent1, indent2, _indent3, _text1, text2, _text3) ->
                  (color, indent2, text2)
        finalPos <-
          lift $
            drawLines' fontAtlas startPos $
              flip map helpLines $
                \(color, _indent1, _indent2, indent3, _text1, _text2, text3) ->
                  (color, indent3, text3)
        put $! finalPos

  let leftPadding = 0.05 * fromIntegral screenSize
      bottomLeftCorner = Float2 leftPadding bottomMargin

  let smallFontSize = fromIntegral $ fa_fontSize $ gle_fontAtlasSmall glEnv
      regularFontSize = fromIntegral $ fa_fontSize $ gle_fontAtlasRegular glEnv
  let smallSpacer :: StateT Float2 GL ()
      smallSpacer = modify' $ _2 +~ 0.5 * smallFontSize
      regularSpacer :: StateT Float2 GL ()
      regularSpacer = modify' $ _2 +~ 0.5 * regularFontSize

  void . flip runStateT bottomLeftCorner $
    for_ (reverse helpScreenLines) $ \case
      DrawLines fontAtlas lines' ->
        drawLines fontAtlas lines'
      DrawLinesColumns fontAtlas lines' ->
        drawLinesColumns fontAtlas lines'
      DrawLinesColumns2 fontAtlas lines' ->
        drawLinesColumns2 fontAtlas lines'
      SmallSpacer ->
        smallSpacer
      RegularSpacer ->
        regularSpacer

helpOverlayLines ::
  Keybindings -> ControlOptions -> GLEnv -> Int -> [HelpScreenLine]
helpOverlayLines keybindings controlOpts glEnv screenSize =
  let titleIndent = 0
      headerIndent = 0.025 * fromIntegral screenSize
      bulletIndent = 0.050 * fromIntegral screenSize

      instructionLines :: [(Float4, Float, String)]
      instructionLines =
        map
          (red,bulletIndent,)
          [ "• Game mode: Capture the flag"
          , "• Take the other team's flag and carry it to your base to score"
          , "• To score, your flag must be at your base"
          ]

      gameControlLines :: [(Float4, Float, Float, Float, String, String, String)]
      gameControlLines =
        map
          ( \(str1, str2, str3) ->
              (yellow, bulletIndent, columnIndent, columnIndent2, str1, str2, str3)
          )
          [ ("", "keyboard", "controller")
          , ("• Move overseer", ":: " ++ moveKeys, ":: stick")
          , ("• Psionic storm", ":: " ++ keyToString kb_psiStorm, ":: trigger")
          ,
            ( "• Overseer dash"
            , concat
                [ ":: "
                , keyToString kb_overseerDash
                , "+"
                , keyToString kb_overseerUp
                , keyToString kb_overseerLeft
                , keyToString kb_overseerDown
                , keyToString kb_overseerRight
                ]
            , ":: bumper+stick"
            )
          , ("• Drop flag", ":: " ++ keyToString kb_dropFlag, ":: thumb")
          , ("", "", "")
          , ("• Attract drones", ":: " ++ mouseAttract, "")
          , ("• Repel drones", ":: " ++ mouseRepel, "")
          , ("• Charge drone dash", ":: " ++ mouseCharge, "")
          ]
        where
          columnIndent = bulletIndent + 0.18 * fromIntegral screenSize -- adjusted manually
          columnIndent2 = columnIndent + 0.18 * fromIntegral screenSize -- adjusted manually
          Keybindings{..} = keybindings
          moveKeys =
            concatMap
              keyToString
              [ kb_overseerUp
              , kb_overseerLeft
              , kb_overseerDown
              , kb_overseerRight
              ]
          (mouseLeft, mouseRight) =
            if co_swapMouseButtons controlOpts
              then ("right", "left")
              else ("left", "right")
          mouseAttract =
            case co_droneControl controlOpts of
              AutomaticDroneControl -> "Release both mouse buttons"
              ManualDroneControl -> "Hold " ++ mouseLeft ++ " mouse button"
          mouseRepel =
            case co_droneControl controlOpts of
              AutomaticDroneControl -> "Hold both mouse buttons"
              ManualDroneControl -> "Hold " ++ mouseRight ++ " mouse button"
          mouseCharge =
            case co_droneControl controlOpts of
              AutomaticDroneControl -> "Hold either mouse button"
              ManualDroneControl -> "Release or hold both mouse buttons"

      uiControlLines :: [(Float4, Float, Float, String, String)]
      uiControlLines =
        map
          (\(str1, str2) -> (blue, bulletIndent, columnIndent, str1, str2))
          [ ("• Show leaderboard", ":: Hold Tab (in game)")
          ]
        where
          columnIndent = bulletIndent + 0.18 * fromIntegral screenSize -- adjusted manually
      adviceLines :: [(Float4, Float, String)]
      adviceLines =
        map
          (green,bulletIndent,)
          [ "• Psionic storm is useful for escaping"
          , "• Harder collisions deal more damage"
          ]
  in  [ DrawLines (gle_fontAtlasLarge glEnv) [(lightGrey, titleIndent, "instructions")]
      , RegularSpacer
      , DrawLines (gle_fontAtlasRegular glEnv) [(red, headerIndent, "Game rules")]
      , SmallSpacer
      , DrawLines (gle_fontAtlasSmall glEnv) instructionLines
      , RegularSpacer
      , DrawLines (gle_fontAtlasRegular glEnv) [(yellow, headerIndent, "Game controls")]
      , SmallSpacer
      , DrawLinesColumns2 (gle_fontAtlasSmall glEnv) gameControlLines
      , RegularSpacer
      , DrawLines (gle_fontAtlasRegular glEnv) [(blue, headerIndent, "UI controls")]
      , SmallSpacer
      , DrawLinesColumns (gle_fontAtlasSmall glEnv) uiControlLines
      , RegularSpacer
      , DrawLines (gle_fontAtlasRegular glEnv) [(green, headerIndent, "Tips")]
      , SmallSpacer
      , DrawLines (gle_fontAtlasSmall glEnv) adviceLines
      ]

detailedMechanicsLines :: GLEnv -> Int -> [HelpScreenLine]
detailedMechanicsLines glEnv screenSize =
  let titleIndent = 0
      headerIndent = 0.025 * fromIntegral screenSize
      bulletIndent = 0.050 * fromIntegral screenSize
  in  [ DrawLines
          (gle_fontAtlasLarge glEnv)
          [(lightGrey, titleIndent, "detailed mechanics")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(red, headerIndent, "Drone dash")]
      , SmallSpacer
      , DrawLines (gle_fontAtlasSmall glEnv) . map (red,bulletIndent,) $
          [ "• Drones will build up a charge whenever they are not commanded to move"
          , "• The charge is released as impulse in the direction of the next movement command"
          , "• The magnitude of the impulse is proportional to the time spent charging"
          , "• Drones will flash when they have reached the maximum charge level" -- TODO: Drones that spawn after stopping move commands will flash too early
          , "• Drone dash is inefficient: charging and dashing covers less ground than simply moving"
          ]
      ]

creditsLines :: GLEnv -> Int -> [HelpScreenLine]
creditsLines glEnv screenSize =
  let titleIndent = 0
      headerIndent = 0.025 * fromIntegral screenSize
      bulletIndent = 0.050 * fromIntegral screenSize
  in  [ DrawLines
          (gle_fontAtlasLarge glEnv)
          [(lightGrey, titleIndent, "credits")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(red, headerIndent, "pixelpusher")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [(red, bulletIndent, "aetup")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(yellow, headerIndent, "Music")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [(yellow, bulletIndent, "luckytern")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(blue, headerIndent, "Sounds")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [(blue, bulletIndent, "aetup, luckytern")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(green, headerIndent, "Contributions to community")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [(green, bulletIndent, "Masterico")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(purple, headerIndent, "Special thanks")]
      , SmallSpacer
      , DrawLines (gle_fontAtlasSmall glEnv) . map (purple,bulletIndent,) $
          [ "Adam Comella, Armando Ramirez, Asphodalus, Jake Araujo-Simon, luckytern"
          , "Masterico, Reed Spool, sourencho, Tabortop Games, Teddy Pettit, Zarak Mahmud"
          ]
      ]

themeHelpLines :: GLEnv -> Int -> [HelpScreenLine]
themeHelpLines glEnv screenSize =
  let titleIndent = 0
      headerIndent = 0.025 * fromIntegral screenSize
      bulletIndent = 0.050 * fromIntegral screenSize
      bulletIndent2 = 0.075 * fromIntegral screenSize
  in  [ DrawLines
          (gle_fontAtlasLarge glEnv)
          [(lightGrey, titleIndent, "themes")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(red, headerIndent, "What are themes?")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [(red, bulletIndent, "• Themes change the colors used in the game")]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(yellow, headerIndent, "How to use a custom theme")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [ -- TODO: avoid hard-coding file name?
            (yellow, bulletIndent, "(1) Edit or replace the theme file 'pixelpusher-theme.toml'")
          , (yellow, bulletIndent2, "• On your computer, the theme file is in the same folder as this game client")
          , (yellow, bulletIndent, "(2) In the visual settings menu, set 'theme' to 'custom'")
          , (yellow, bulletIndent, "(3) Your custom theme should now be visible in game")
          , -- TODO: avoid hard-coding file name?
            (yellow, bulletIndent2, "• If not, check the log file 'pixelpusher-client.log' for errors")
          , (yellow, bulletIndent2, "• New versions of the game could cause errors if they change the theme file format")
          , (yellow, bulletIndent2, "• But I'll try not to do that")
          ]
      , RegularSpacer
      , DrawLines
          (gle_fontAtlasRegular glEnv)
          [(blue, headerIndent, "Tips for authoring themes")]
      , SmallSpacer
      , DrawLines
          (gle_fontAtlasSmall glEnv)
          [ (blue, bulletIndent, "• Press Ctrl+R to reload the themes file")
          ]
      ]

-- Colors

darken :: Float4 -> Float4
darken (Float4 r g b a) = Float4 (0.2 * r) (0.2 * g) (0.2 * b) a

lightGrey, blue, red, yellow, green, purple :: Float4
lightGrey = Float4 0.70 0.70 0.70 1
blue = Float4 0.48 0.64 0.80 1
red = Float4 0.80 0.56 0.60 1
yellow = Float4 0.71 0.61 0.45 1
green = Float4 0.42 0.67 0.60 1
purple = Float4 0.64 0.60 0.78 1
