module Client.Visuals.Rendering.BasicMessage (
  renderBasicMessage,
) where

import Graphics.UI.GLFW qualified as GLFW

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Visuals.Env
import Client.Visuals.FontAtlas (fa_fontSize)
import Client.Visuals.GL.Wrapped
import Client.Visuals.Shaders.Char
import Client.Visuals.Text

-- | Display text on the screen.
renderBasicMessage :: GLEnv -> Float4 -> String -> IO ()
renderBasicMessage glEnv color message = do
  (screenSize, aspectRatio) <- getFramebufferDims $ gle_window glEnv

  let screenWidth = fromIntegral screenSize
      x = screenWidth / 2
      y = screenWidth * 3 / 4
      fontAtlas = gle_fontAtlasLarge glEnv
      spacing = fromIntegral $ fa_fontSize fontAtlas
      msgLines = lines message

  runGL $ do
    glClearColor 0.0 0.0 0.0 1.0
    glClear GL_COLOR_BUFFER_BIT
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = fontAtlas
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines =
            snd $
              stackLinesDownward CharAlignCenter (Float2 x y) spacing $
                map (color,) msgLines
        }

  GLFW.swapBuffers $ gle_window glEnv
  GLFW.pollEvents
