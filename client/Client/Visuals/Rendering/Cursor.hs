module Client.Visuals.Rendering.Cursor (
  drawCursor,
) where

import Control.Monad.Trans.Reader
import Data.Foldable (for_)
import Data.Functor ((<&>))
import Lens.Micro.Platform.Custom (view)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2,
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.State (GameScene (..))

import Client.Constants qualified as CC
import Client.Visuals.CoordinateSystems qualified as CS
import Client.Visuals.Env
import Client.Visuals.GL.Wrapped
import Client.Visuals.SceneDecorations
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.CursorAttract
import Client.Visuals.Shaders.CursorRepel
import Client.Visuals.StandardInstancedVisual

--------------------------------------------------------------------------------

drawCursor ::
  TestParams ->
  ShowOSCursor ->
  Float2 ->
  Float2 ->
  GameScene ->
  SceneDecorations ->
  Float2 ->
  ReaderT (GLEnv, Int, Float) GL ()
drawCursor testParams showOSCursor windowSize cursorPos scene decorations cameraPos = do
  let mOverseerPosPlayerControlRadius =
        getPlayerPerspective (sceneDeco_perspective decorations)
          <&> \playerInfo ->
            ( Fixed.toFloats $
                overseerViewPosition $
                  pv_overseerView (pi_playerView playerInfo)
            , pi_player playerInfo
            , let playerClass = pv_playerClass (pi_playerView playerInfo)
              in  Fixed.toFloat $
                    getClassParam playerClass $
                      gp_controlRadius (gp_dynamics (scene_gameParams scene))
            )
  drawCursorHelper
    testParams
    showOSCursor
    windowSize
    cursorPos
    cameraPos
    mOverseerPosPlayerControlRadius

drawCursorHelper ::
  TestParams ->
  ShowOSCursor ->
  Float2 ->
  Float2 ->
  Float2 ->
  Maybe (Float2, Player, Float) ->
  ReaderT (GLEnv, Int, Float) GL ()
drawCursorHelper
  testParams
  showOSCursor
  windowSize
  cursorPos
  cameraPos
  mOverseerPosPlayer = do
    (glEnv, _, _) <- ask
    -- Note: This function uses screen coordinates, rather than framebuffer
    -- coordinates

    let windowRadius = 0.5 * minimum (Float.toList2 windowSize)
        windowCenter = Float.map2 (* 0.5) windowSize
        windowCoordSystem =
          CS.makeCoordSystem
            windowRadius
            (V2 CS.PreserveAxis CS.FlipAxis)
            windowCenter

    for_ mOverseerPosPlayer $ \(overseerPos, player, controlRadius) -> do
      let droneCommand =
            view control_drone $ control_buttons $ player_controls player
          droneCommandPos =
            Fixed.toFloats $ control_worldPos $ player_controls player
      let gameViewCS = CS.gameView CC.viewRadius cameraPos
          cursorPosGameWorld =
            CS.intoPos gameViewCS $
              CS.fromPos windowCoordSystem cursorPos
          truncatedCursorPos =
            truncateDroneCommandPos controlRadius overseerPos cursorPosGameWorld
          truncatedCommandPos =
            truncateDroneCommandPos controlRadius overseerPos droneCommandPos

      case showOSCursor of
        ShowOSCursor -> do
          drawGameWorldCursor testParams glEnv cameraPos cursorPosGameWorld droneCommand 0.5
          drawGameWorldCursor testParams glEnv cameraPos truncatedCursorPos droneCommand 0.5
        DoNotShowOSCursor -> do
          drawGameWorldCursor testParams glEnv cameraPos truncatedCommandPos droneCommand 1

truncateDroneCommandPos :: Float -> Float2 -> Float2 -> Float2
truncateDroneCommandPos controlRadius overseerPos commandPos =
  let (size, direction) = Float.normAndNormalized2 (commandPos - overseerPos)
      r = min controlRadius size
  in  Float.map2 (* r) direction + overseerPos

drawGameWorldCursor ::
  TestParams ->
  GLEnv ->
  Float2 ->
  Float2 ->
  DroneCommand ->
  Float ->
  ReaderT (GLEnv, Int, Float) GL ()
drawGameWorldCursor testParams glEnv cameraPos pos droneCommand opacityFactor = do
  ReaderT $ \(_, _, aspectRatio) -> do
    let gameViewCS = CS.gameView CC.viewRadius cameraPos

    withBlendFunc GL_SRC_ALPHA GL_ONE $ do
      case droneCommand of
        DroneNeutral ->
          drawCircle (gle_circle glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  pure $
                    CircleInstance
                      { ci_size = 5
                      , ci_center = pos
                      , ci_color = Float4 1 1 1 (0.70 * opacityFactor)
                      , ci_fuzziness = 1
                      }
              }
        DroneAttraction ->
          drawCursorAttract (gle_cursorAttract glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  pure $
                    CursorAttractInstance
                      { cai_size = 7.5
                      , cai_center = pos
                      , cai_color = Float4 1 1 1 opacityFactor
                      }
              }
        DroneRepulsion ->
          drawCursorRepel (gle_cursorRepel glEnv) $
            StandardVParams
              { svp_aspectRatio = aspectRatio
              , svp_coordSystem = gameViewCS
              , svp_testParams = testParams
              , svp_instances =
                  pure $
                    CursorRepelInstance
                      { cri_size = 7.5
                      , cri_center = pos
                      , cri_color = Float4 1 1 1 opacityFactor
                      }
              }
