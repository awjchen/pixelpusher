{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.FontAtlas (
  FontAtlas,
  fa_fontSize,
  fa_texture,
  fa_textureSize,
  fa_charMap,
  AtlasChar (..),
  CharDims (..),
  initFontAtlas,
) where

import Control.Monad (forM)
import Data.ByteString qualified as BS
import Data.IntMap.Strict qualified as IM
import Data.List (foldl')
import Data.Vector.Storable qualified as VS
import Foreign
import FreeType
import Graphics.GL.Core33
import Graphics.GL.Types
import Lens.Micro.Platform.Custom (view, _1, _2)

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Linear (V2 (V2))

import Client.Visuals.GL.Wrapped (Texture, genTexture, getTexture, runGL)

--------------------------------------------------------------------------------

-- This module is based on the texture atlas tutorial at:
-- https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_02

data FontAtlas = FontAtlas
  { _fa_fontSize :: Word32
  , _fa_texture :: Texture
  , _fa_textureSize :: V2 Word32
  , _fa_charMap :: IM.IntMap AtlasChar
  }

fa_fontSize :: FontAtlas -> Word32
fa_fontSize = _fa_fontSize

fa_texture :: FontAtlas -> Texture
fa_texture = _fa_texture

fa_textureSize :: FontAtlas -> V2 Word32
fa_textureSize = _fa_textureSize

fa_charMap :: FontAtlas -> IM.IntMap AtlasChar
fa_charMap = _fa_charMap

data AtlasChar = AtlasChar
  { ac_atlasPosition :: Float -- pixels, bottom left
  , ac_charDims :: {-# UNPACK #-} CharDims
  }
  deriving stock (Show)

data CharDims = CharDims
  { cd_size :: {-# UNPACK #-} Float2 -- Size of glyph, in pixels
  , cd_bearing :: {-# UNPACK #-} Float2 -- Offset from baseline to left/top of glyph, in pixels
  , cd_advance :: Float -- Offset to advance to next glyph, in pixels
  }
  deriving stock (Show)

data RawCharDims = RawCharDims
  { rcd_size :: {-# UNPACK #-} (V2 Word32) -- Size of glyph, in pixels
  , rcd_bearing :: {-# UNPACK #-} (V2 Int32) -- Offset from baseline to left/top of glyph, in pixels
  , rcd_advance64 :: Int32 -- Offset to advance to next glyph, in 1/64ths of a pixel
  }
  deriving stock (Show)

makeCharDims :: RawCharDims -> CharDims
makeCharDims RawCharDims{..} =
  CharDims
    { cd_size =
        let V2 x y = rcd_size in Float2 (fromIntegral x) (fromIntegral y)
    , cd_bearing =
        let V2 x y = rcd_bearing in Float2 (fromIntegral x) (fromIntegral y)
    , cd_advance =
        fromIntegral rcd_advance64 * 1.5625e-2 -- (/64)
    }

charOrds :: [Int]
charOrds =
  0x2022 -- • Bullet
    : 0x1f787 -- 🞉 Heavy White Circle
    : [32 .. 127] -- ascii

initFontAtlas :: BS.ByteString -> FT_UInt -> IO FontAtlas
initFontAtlas fontFile fontSize =
  ft_With_FreeType $ \ft ->
    withMemoryFace ft fontFile $ \face -> do
      ft_Set_Pixel_Sizes face 0 fontSize -- width = 0 --> dynamic width based on height

      -- Get char dimensions
      rawCharDims <- forM charOrds $ \charOrd -> do
        ft_Load_Char face (fromIntegral charOrd) FT_LOAD_RENDER
        glyph <- peek face >>= peek . frGlyph
        let advance = gsrAdvance glyph
            bitmap_left = gsrBitmap_left glyph
            bitmap_top = gsrBitmap_top glyph
            bitmap = gsrBitmap glyph
        let width = bWidth bitmap
            rows = bRows bitmap
        let rawCharDims =
              RawCharDims
                { rcd_size = V2 width rows
                , rcd_bearing = V2 bitmap_left bitmap_top
                , rcd_advance64 = fromIntegral $ vX advance
                }
        pure (charOrd, rawCharDims)

      -- Initialize texture for texture atlas. We just put all characters in a
      -- single row.
      let maxRows =
            foldl' max 0 $ map (view _2 . rcd_size . snd) rawCharDims
          -- Adding 1 pixel of horizontal padding after each character
          totalWidth =
            foldl' (+) 0 $ map ((+ 1) . view _1 . rcd_size . snd) rawCharDims

      glActiveTexture GL_TEXTURE0
      texture <- runGL genTexture

      withTexture GL_TEXTURE_2D (getTexture texture) $ do
        glPixelStorei GL_UNPACK_ALIGNMENT 1 -- Remove 4-byte alignment restriction for texture size
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_S GL_CLAMP_TO_EDGE
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_WRAP_T GL_CLAMP_TO_EDGE
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR
        glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR

        glTexImage2D
          GL_TEXTURE_2D
          0
          GL_RED
          (fromIntegral totalWidth)
          (fromIntegral maxRows)
          0
          GL_RED
          GL_UNSIGNED_BYTE
          nullPtr

        -- Initialize texture to 0 (transparent)
        VS.unsafeWith (VS.replicate @Word8 (fromIntegral (totalWidth * maxRows)) 0) $ \buffer -> do
          glTexSubImage2D
            GL_TEXTURE_2D
            0
            -- offset of subimage
            0
            0
            -- size of subimage
            (fromIntegral totalWidth)
            (fromIntegral maxRows)
            GL_RED
            GL_UNSIGNED_BYTE
            (castPtr buffer)

          -- Write characters onto texture
          charMap <- fmap IM.fromList $
            forAccumM rawCharDims 0 $
              \x_offset (charOrd, rawCharDims') -> do
                let V2 width rows = fromIntegral <$> rcd_size rawCharDims'

                ft_Load_Char face (fromIntegral charOrd) FT_LOAD_RENDER
                charBuffer <- fmap (bBuffer . gsrBitmap) $ peek face >>= peek . frGlyph

                glTexSubImage2D
                  GL_TEXTURE_2D
                  0
                  -- offset of subimage
                  x_offset
                  0
                  -- size of subimage
                  width
                  rows
                  GL_RED
                  GL_UNSIGNED_BYTE
                  (castPtr charBuffer)

                -- Adding 1 pixel of horizontal padding after each character
                let new_x_offset = x_offset + width + 1
                    atlasChar =
                      AtlasChar
                        { ac_atlasPosition = fromIntegral x_offset
                        , ac_charDims = makeCharDims rawCharDims'
                        }
                pure (new_x_offset, (charOrd, atlasChar))

          pure
            FontAtlas
              { _fa_fontSize = fontSize
              , _fa_texture = texture
              , _fa_textureSize = V2 totalWidth maxRows
              , _fa_charMap = charMap
              }

--------------------------------------------------------------------------------
-- Helpers

withMemoryFace ::
  FT_Library -> BS.ByteString -> (FT_Face -> IO a) -> IO a
withMemoryFace ft fontFile action =
  BS.useAsCString fontFile $ \fontFileCStr ->
    ft_With_Memory_Face
      ft
      (castPtr fontFileCStr)
      (fromIntegral (BS.length fontFile))
      0
      action

forAccumM :: (Monad m) => [a] -> b -> (b -> a -> m (b, c)) -> m [c]
forAccumM [] _ _ = pure []
forAccumM (x : xs) z f = do
  (!z', c) <- f z x
  cs <- forAccumM xs z' f
  pure $ c : cs

withTexture :: GLenum -> GLuint -> IO a -> IO a
withTexture target texture action = do
  glBindTexture target texture
  x <- action
  glBindTexture target 0
  pure x
