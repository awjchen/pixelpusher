{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}

module Client.Visuals.GL.Wrapped (
  -- * Re-exports
  module M,

  -- * Context
  GL,
  runGL,

  -- * Clearing
  glClear,
  glClearColor,

  -- * Buffers
  Buffer,
  genBuffer,
  withBuffer,
  glBufferData,
  glBufferDataEmpty,
  glBufferSubData,

  -- * Textures
  Texture,
  getTexture,
  genTexture,
  withTexture,
  glActiveTexture,

  -- * Vertex arrays
  VertexArray,
  genVertexArray,
  withVertexArray,
  glVertexAttribPointer,
  glEnableVertexAttribArray,
  glVertexAttribDivisor,

  -- * Shaders
  ShaderProgram,
  withShaderProgram,
  makeShaderProgram,

  -- * Uniforms
  UniformLoc,
  glUniform1f,
  glUniform2f,
  glUniform3f,
  glUniform4f,
  glUniform1i,
  glUniform2fv,
  glGetUniformLocation,

  -- * Misc
  sizeOfGLfloat,
  withBlendFunc,
  glDrawArrays,
  glDrawArraysInstanced,
) where

import Control.Monad (when)
import Data.Vector.Storable qualified as VS
import Foreign
import Foreign.C.String (peekCAString, withCAString, withCAStringLen)
import Graphics.GL.Core33 qualified as Internal
import Graphics.GL.Internal.Shared as M (
  pattern GL_ARRAY_BUFFER,
  pattern GL_CLAMP_TO_EDGE,
  pattern GL_COLOR_BUFFER_BIT,
  pattern GL_DYNAMIC_DRAW,
  pattern GL_FALSE,
  pattern GL_FLOAT,
  pattern GL_FRAGMENT_SHADER,
  pattern GL_LINEAR,
  pattern GL_LINE_LOOP,
  pattern GL_ONE,
  pattern GL_ONE_MINUS_SRC_ALPHA,
  pattern GL_RED,
  pattern GL_SRC_ALPHA,
  pattern GL_STATIC_DRAW,
  pattern GL_TEXTURE0,
  pattern GL_TEXTURE_2D,
  pattern GL_TEXTURE_MAG_FILTER,
  pattern GL_TEXTURE_MIN_FILTER,
  pattern GL_TEXTURE_WRAP_S,
  pattern GL_TEXTURE_WRAP_T,
  pattern GL_TRIANGLE_STRIP,
  pattern GL_UNPACK_ALIGNMENT,
  pattern GL_UNSIGNED_BYTE,
  pattern GL_VERTEX_SHADER,
 )
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2)

import Client.Logging
import Client.Visuals.GL.Wrapped.Internal

--------------------------------------------------------------------------------
-- Context

runGL :: GL a -> IO a
runGL = _runGL

--------------------------------------------------------------------------------
-- Clearing

glClear :: GLbitfield -> GL ()
glClear a = GL $ Internal.glClear a

glClearColor :: GLfloat -> GLfloat -> GLfloat -> GLfloat -> GL ()
glClearColor a b c d = GL $ Internal.glClearColor a b c d

--------------------------------------------------------------------------------
-- Buffers

newtype Buffer = Buffer GLuint

genBuffer :: GL Buffer
genBuffer =
  GL $ fmap Buffer $ alloca $ \ptr -> Internal.glGenBuffers 1 ptr >> peek ptr

-- | Warning: Uses of `withBuffer` should not be nested.
withBuffer :: GLenum -> Buffer -> GL a -> GL a
withBuffer glArray (Buffer buf) action = GL $ do
  Internal.glBindBuffer glArray buf
  x <- runGL action
  Internal.glBindBuffer glArray 0
  pure x

glBufferData ::
  (Storable a) =>
  GLenum ->
  GLsizeiptr ->
  VS.Vector a ->
  GLenum ->
  GL ()
glBufferData a b vec d =
  GL $
    VS.unsafeWith vec $ \ptr ->
      Internal.glBufferData a b (castPtr ptr) d

-- | Like `glBufferData`, but do not transfer any data.
glBufferDataEmpty :: GLenum -> GLsizeiptr -> GLenum -> GL ()
glBufferDataEmpty a b d = GL $ Internal.glBufferData a b nullPtr d

glBufferSubData ::
  (Storable a) =>
  GLenum ->
  GLsizeiptr ->
  GLsizeiptr ->
  VS.Vector a ->
  GL ()
glBufferSubData a b c vec =
  GL $
    VS.unsafeWith vec $ \ptr ->
      Internal.glBufferSubData a b c (castPtr ptr)

--------------------------------------------------------------------------------
-- Textures

newtype Texture = Texture GLuint

getTexture :: Texture -> GLuint
getTexture (Texture u) = u

genTexture :: GL Texture
genTexture =
  GL $
    fmap Texture $
      alloca $
        \ptr -> Internal.glGenTextures 1 ptr >> peek ptr

-- | Warning: Uses of `withTexture` should not be nested.
withTexture :: GLenum -> Texture -> GL a -> GL a
withTexture target (Texture tex) action = GL $ do
  Internal.glBindTexture target tex
  x <- runGL action
  Internal.glBindTexture target 0
  pure x

glActiveTexture :: GLenum -> GL ()
glActiveTexture a = GL $ Internal.glActiveTexture a

--------------------------------------------------------------------------------
-- Vertex arrays

newtype VertexArray = VertexArray GLenum

genVertexArray :: GL VertexArray
genVertexArray =
  GL $
    fmap VertexArray $
      alloca $
        \ptr -> Internal.glGenVertexArrays 1 ptr >> peek ptr

-- | Warning: Uses of `withVertexArray` should not be nested.
withVertexArray :: VertexArray -> GL a -> GL a
withVertexArray (VertexArray va) action = GL $ do
  Internal.glBindVertexArray va
  a <- runGL action
  Internal.glBindVertexArray 0
  pure a

glVertexAttribPointer ::
  GLuint ->
  GLint ->
  GLenum ->
  GLboolean ->
  GLsizei ->
  Int ->
  GL ()
glVertexAttribPointer a b c d e offset =
  GL $ Internal.glVertexAttribPointer a b c d e (offsetPtr offset)

glEnableVertexAttribArray :: GLuint -> GL ()
glEnableVertexAttribArray a = GL $ Internal.glEnableVertexAttribArray a

glVertexAttribDivisor :: GLuint -> GLuint -> GL ()
glVertexAttribDivisor a b = GL $ Internal.glVertexAttribDivisor a b

--------------------------------------------------------------------------------
-- Shaders

newtype ShaderProgram = ShaderProgram GLuint

-- | Warning: Uses of `withShaderProgram` should not be nested.
withShaderProgram :: ShaderProgram -> GL a -> GL a
withShaderProgram (ShaderProgram prog) action = GL $ do
  Internal.glUseProgram prog
  a <- runGL action
  Internal.glUseProgram 0
  pure a

makeShaderProgram :: TimedFastLogger -> String -> String -> GL ShaderProgram
makeShaderProgram logger vertexSrc fragmentSrc = GL $ do
  shaderProg <- Internal.glCreateProgram
  vertShader <- compileShader logger GL_VERTEX_SHADER vertexSrc
  fragShader <- compileShader logger GL_FRAGMENT_SHADER fragmentSrc

  Internal.glAttachShader shaderProg vertShader
  Internal.glAttachShader shaderProg fragShader
  Internal.glLinkProgram shaderProg
  checkProgramLinking logger shaderProg

  Internal.glDeleteShader vertShader
  Internal.glDeleteShader fragShader

  pure $ ShaderProgram shaderProg

compileShader :: TimedFastLogger -> GLenum -> String -> IO GLuint
compileShader logger shaderType shaderSrc = do
  shaderEnum <- Internal.glCreateShader shaderType
  withCAStringLen shaderSrc $ \(cStr, cStrLen) ->
    -- not sure for how long we need to keep these pointers alive
    with cStr $ \cStrP ->
      with (fromIntegral cStrLen) $ \cStrLenP ->
        Internal.glShaderSource shaderEnum 1 cStrP cStrLenP
  Internal.glCompileShader shaderEnum
  checkShaderCompilation logger shaderEnum
  pure shaderEnum

checkShaderCompilation :: TimedFastLogger -> GLuint -> IO ()
checkShaderCompilation logger shaderEnum = do
  success <- alloca $ \ptr ->
    Internal.glGetShaderiv shaderEnum Internal.GL_COMPILE_STATUS ptr >> peek ptr
  when (success == Internal.GL_FALSE) $ do
    let infoLogLen :: (Integral a) => a
        infoLogLen = 512
    allocaArray infoLogLen $ \infoLogCStr ->
      alloca $ \infoLogLengthP -> do
        Internal.glGetShaderInfoLog shaderEnum infoLogLen infoLogLengthP infoLogCStr
        errMsgLen <- peek infoLogLengthP
        errMsg <- peekCAString infoLogCStr
        writeLog logger $
          "Shader program compilation failed. Error message length: "
            <> toLogStr (show errMsgLen)
            <> ". Error message: "
            <> toLogStr errMsg
            <> "."

checkProgramLinking :: TimedFastLogger -> GLuint -> IO ()
checkProgramLinking logger program = do
  success <- alloca $ \ptr ->
    Internal.glGetProgramiv program Internal.GL_LINK_STATUS ptr >> peek ptr
  when (success == Internal.GL_FALSE) $ do
    writeLog logger "Program linking failed"

    let infoLogLen :: (Integral a) => a
        infoLogLen = 512
    allocaArray infoLogLen $ \infoLogCStr ->
      alloca $ \infoLogLengthP -> do
        Internal.glGetProgramInfoLog program infoLogLen infoLogLengthP infoLogCStr

        errMsgLen <- peek infoLogLengthP
        errMsg <- peekCAString infoLogCStr
        writeLog logger $
          "Shader program linking failed. Error message length: "
            <> toLogStr (show errMsgLen)
            <> ". Error message: "
            <> toLogStr errMsg
            <> "."

------------------------------------------------------------------------------
-- Uniforms

newtype UniformLoc = UniformLoc GLint

glUniform1f :: UniformLoc -> GLfloat -> GL ()
glUniform1f (UniformLoc i) b = GL $ Internal.glUniform1f i b

glUniform2f :: UniformLoc -> GLfloat -> GLfloat -> GL ()
glUniform2f (UniformLoc i) b c = GL $ Internal.glUniform2f i b c

glUniform3f :: UniformLoc -> GLfloat -> GLfloat -> GLfloat -> GL ()
glUniform3f (UniformLoc i) b c d = GL $ Internal.glUniform3f i b c d

glUniform4f :: UniformLoc -> GLfloat -> GLfloat -> GLfloat -> GLfloat -> GL ()
glUniform4f (UniformLoc i) b c d e = GL $ Internal.glUniform4f i b c d e

glUniform1i :: UniformLoc -> GLint -> GL ()
glUniform1i (UniformLoc i) b = GL $ Internal.glUniform1i i b

glUniform2fv :: UniformLoc -> GLsizei -> VS.Vector Float2 -> GL ()
glUniform2fv (UniformLoc i) n vec =
  GL $ VS.unsafeWith vec $ Internal.glUniform2fv i n . castPtr

glGetUniformLocation :: ShaderProgram -> String -> GL UniformLoc
glGetUniformLocation (ShaderProgram shaderProg) name =
  GL $
    fmap UniformLoc $
      withCAString name $
        Internal.glGetUniformLocation shaderProg

------------------------------------------------------------------------------
-- Misc

{-# INLINEABLE sizeOfGLfloat #-}
sizeOfGLfloat :: (Integral a) => a
sizeOfGLfloat = 4

-- | Warning: Uses of `withBlendFunc` should not be nested.
withBlendFunc :: GLenum -> GLenum -> GL a -> GL a
withBlendFunc sfactor dfactor action = GL $ do
  Internal.glBlendFunc sfactor dfactor
  x <- runGL action
  Internal.glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
  pure x

glDrawArrays :: GLenum -> GLint -> GLsizei -> GL ()
glDrawArrays a b c = GL $ Internal.glDrawArrays a b c

glDrawArraysInstanced :: GLenum -> GLint -> GLsizei -> GLsizei -> GL ()
glDrawArraysInstanced a b c d = GL $ Internal.glDrawArraysInstanced a b c d

------------------------------------------------------------------------------
-- Helpers

offsetPtr :: Int -> Ptr a
offsetPtr offset = castPtr $ plusPtr nullPtr offset
