{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.GL.Attribute (
  GLAttribute (..),
) where

import Control.Monad (foldM)
import Data.DList (DList)
import Data.DList qualified as DL
import Data.Kind
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector.Storable qualified as VS
import Data.Vector.Storable.Mutable qualified as VSM
import Foreign
import GHC.Generics
import Graphics.GL.Types

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped.Internal (GL (GL))

--------------------------------------------------------------------------------

-- | A class for types representing OpenGL vertex attribute data (or OpenGL
-- instance data) that contain float types only.
class GLAttribute a where
  -- | Number of floats in each field
  fieldSizes :: [Int]

  -- | Concatenation of the floats from all fields, in the same order as
  -- `fieldSizes`.
  toFloatsInstanced :: [a] -> GL (VS.Vector GLfloat)

  -- | GLSL layout declarations
  layoutDecs ::
    -- | Existing number of declarations
    Int ->
    -- | (New total number of declarations, new declarations)
    (Int, Text)

  -- Defaults
  default fieldSizes :: (GAttributeData (Rep a)) => [Int]
  fieldSizes = DL.toList $ gFieldSizes @(Rep a)
  {-# INLINE fieldSizes #-}

  default toFloatsInstanced ::
    (Generic a, GAttributeData (Rep a)) => [a] -> GL (VS.Vector GLfloat)
  toFloatsInstanced xs =
    GL $ do
      let len = length xs
          fieldSize = sum (fieldSizes @a)
      floatMVec@(VSM.MVector _ foreignPtr) <- VSM.new $ len * fieldSize
      _ <-
        withForeignPtr foreignPtr $ \ptr ->
          foldM (\p -> gPokeFloats p . from) ptr xs
      VS.unsafeFreeze floatMVec
  {-# INLINE toFloatsInstanced #-}

  default layoutDecs :: (GLRecord a) => Int -> (Int, Text)
  layoutDecs i =
    let fields = glFields @a
        layoutLines = Text.unlines $ zipWith f [i ..] fields
          where
            f loc GLRecordField{..} =
              Text.concat
                [ "layout (location = "
                , Text.pack (show loc)
                , ") in "
                , grf_typeName
                , " "
                , grf_fieldName
                , ";"
                ]
    in  (i + length fields, layoutLines)

-- | A class for deriving 'GLAtribute' for non-unit records in which all fields
-- are instances of 'GLFloatBufferData'.
class GAttributeData (f :: Type -> Type) where
  gFieldSizes :: DList Int
  gPokeFloats :: Ptr GLfloat -> f x -> IO (Ptr GLfloat)

-- Metadata (data, constructors, record selectors): passthrough
instance (GAttributeData f) => GAttributeData (M1 i c f) where
  gFieldSizes = gFieldSizes @f
  {-# INLINE gFieldSizes #-}
  gPokeFloats ptr (M1 x) = gPokeFloats ptr x
  {-# INLINE gPokeFloats #-}

-- Products: passthrough
instance (GAttributeData f, GAttributeData g) => GAttributeData (f :*: g) where
  gFieldSizes = gFieldSizes @f <> gFieldSizes @g
  {-# INLINE gFieldSizes #-}
  gPokeFloats ptr0 (x1 :*: x2) = do
    ptr1 <- gPokeFloats ptr0 x1
    gPokeFloats ptr1 x2
  {-# INLINE gPokeFloats #-}

-- Fields
instance (GLFloatBufferData c) => GAttributeData (K1 i c) where
  gFieldSizes = DL.singleton $ glFloat_nFloats @c
  {-# INLINE gFieldSizes #-}
  gPokeFloats ptr (K1 x) = glFloat_poke ptr x
  {-# INLINE gPokeFloats #-}

-- Unit
instance GAttributeData U1 where
  gFieldSizes = DL.empty
  {-# INLINE gFieldSizes #-}
  gPokeFloats ptr U1 = pure ptr
  {-# INLINE gPokeFloats #-}

--------------------------------------------------------------------------------
-- Basic 'GLAttribute' instances

-- | Empty attributes
instance GLAttribute () where
  fieldSizes = []
  toFloatsInstanced _ = pure VS.empty
  layoutDecs i = (i, mempty)

------------------------------------------------------------------------------
-- Filling buffers

-- | A class for types consisting entirely of 'Float's.
class GLFloatBufferData a where
  glFloat_nFloats :: Int
  glFloat_poke :: Ptr GLfloat -> a -> IO (Ptr GLfloat)

instance GLFloatBufferData Float where
  glFloat_nFloats = 1
  {-# INLINE glFloat_nFloats #-}
  glFloat_poke p x = do
    poke p x
    pure $ plusPtr p 4
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float2 where
  glFloat_nFloats = 2
  {-# INLINE glFloat_nFloats #-}
  glFloat_poke p (Float2 x y) = do
    poke p x
    pokeElemOff p 1 y
    pure $ plusPtr p 8
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float3 where
  glFloat_nFloats = 3
  {-# INLINE glFloat_nFloats #-}
  glFloat_poke p (Float3 x y z) = do
    poke p x
    pokeElemOff p 1 y
    pokeElemOff p 2 z
    pure $ plusPtr p 12
  {-# INLINE glFloat_poke #-}

instance GLFloatBufferData Float4 where
  glFloat_nFloats = 4
  {-# INLINE glFloat_nFloats #-}
  glFloat_poke p (Float4 x y z w) = do
    poke p x
    pokeElemOff p 1 y
    pokeElemOff p 2 z
    pokeElemOff p 3 w
    pure $ plusPtr p 16
  {-# INLINE glFloat_poke #-}
