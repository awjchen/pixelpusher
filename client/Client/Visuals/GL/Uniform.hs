{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilyDependencies #-}

-- | Deriving boilerplate for OpenGL uniforms
module Client.Visuals.GL.Uniform (
  GLUniform (..),
) where

import Data.Int (Int32)
import Data.Kind
import Data.Proxy
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector.Storable.Sized (Vector, fromSized)
import GHC.Generics
import GHC.TypeLits

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped

--------------------------------------------------------------------------------

-- | OpenGL uniform data types consisting entirely of `Float`s
--
-- TODO: Not just floats anymore. Rename and revise documentation.
class (GLTypeName a) => GLFloatUniformData a where
  writeUniform :: UniformLoc -> a -> GL ()

instance GLFloatUniformData Float where
  writeUniform = glUniform1f
  {-# INLINE writeUniform #-}

instance GLFloatUniformData Float2 where
  writeUniform location (Float2 x y) = glUniform2f location x y
  {-# INLINE writeUniform #-}

instance GLFloatUniformData Float3 where
  writeUniform location (Float3 x y z) = glUniform3f location x y z
  {-# INLINE writeUniform #-}

instance GLFloatUniformData Float4 where
  writeUniform location (Float4 x y z w) = glUniform4f location x y z w
  {-# INLINE writeUniform #-}

instance GLFloatUniformData Int32 where
  writeUniform = glUniform1i
  {-# INLINE writeUniform #-}

instance GLFloatUniformData Bool where
  writeUniform location b = glUniform1i location (if b then 1 else 0)
  {-# INLINE writeUniform #-}

instance (KnownNat n) => GLFloatUniformData (Vector n Float2) where
  writeUniform location vec =
    glUniform2fv
      location
      (fromIntegral (natVal (Proxy @n)))
      (fromSized vec)
  {-# INLINE writeUniform #-}

--------------------------------------------------------------------------------

-- | A class for types that can be stored in OpenGL uniforms.
class GLUniform a where
  type UniformLocations a = (r :: Type) | r -> a
  getUniformLocations :: ShaderProgram -> GL (UniformLocations a)
  writeUniforms :: UniformLocations a -> a -> GL ()
  uniformDeclarationsGLSL :: Text

  -- Defaults

  type UniformLocations a = DefaultLocations a

  default getUniformLocations ::
    (GUniform (Rep a), UniformLocations a ~ DefaultLocations a) =>
    ShaderProgram ->
    GL (UniformLocations a)
  getUniformLocations shaderProg =
    DefaultLocations
      <$> gGetUniformLocations @(Rep a) shaderProg
  {-# INLINE getUniformLocations #-}

  default writeUniforms ::
    (Generic a, GUniform (Rep a), UniformLocations a ~ DefaultLocations a) =>
    UniformLocations a ->
    a ->
    GL ()
  writeUniforms (DefaultLocations location) value =
    gWriteUniforms location (from value)
  {-# INLINE writeUniforms #-}

  default uniformDeclarationsGLSL ::
    (GLRecord a) => Text
  uniformDeclarationsGLSL =
    Text.unlines $
      flip map (glFields @a) $ \GLRecordField{..} ->
        "uniform " <> grf_typeName <> " " <> grf_fieldName <> grf_arraySuffix <> ";"
  {-# INLINE uniformDeclarationsGLSL #-}

-- | A wrapper around the type family GUniformLocations. Used in the default
-- implementation of `GLUniform` to avoid UndecidableInstances.
newtype DefaultLocations a = DefaultLocations (GUniformLocations (Rep a) ())

--------------------------------------------------------------------------------

-- | Deriving `GLUniform` for non-unit records in which all fields are
-- instances of `GLFloatUniformData`
class GUniform (f :: Type -> Type) where
  type GUniformLocations f :: Type -> Type
  gGetUniformLocations ::
    ShaderProgram ->
    GL (GUniformLocations f x)
  gWriteUniforms :: GUniformLocations f x -> f x -> GL ()

-- Data: passthrough
instance (GUniform f) => GUniform (D1 c f) where
  type GUniformLocations (D1 c f) = D1 c (GUniformLocations f)

  gGetUniformLocations shaderProg =
    M1 <$> gGetUniformLocations @f shaderProg
  {-# INLINE gGetUniformLocations #-}

  gWriteUniforms (M1 location) (M1 value) = gWriteUniforms location value
  {-# INLINE gWriteUniforms #-}

-- Constructors: passthrough
instance (GUniform f) => GUniform (C1 c f) where
  type GUniformLocations (C1 c f) = C1 c (GUniformLocations f)

  gGetUniformLocations shaderProg =
    M1 <$> gGetUniformLocations @f shaderProg
  {-# INLINE gGetUniformLocations #-}

  gWriteUniforms (M1 location) (M1 value) = gWriteUniforms location value
  {-# INLINE gWriteUniforms #-}

-- Products
instance (GUniform f, GUniform g) => GUniform (f :*: g) where
  type GUniformLocations (f :*: g) = GUniformLocations f :*: GUniformLocations g

  gGetUniformLocations shaderProg =
    (:*:)
      <$> gGetUniformLocations @f shaderProg
      <*> gGetUniformLocations @g shaderProg
  {-# INLINE gGetUniformLocations #-}

  gWriteUniforms (loc1 :*: loc2) (val1 :*: val2) =
    gWriteUniforms loc1 val1 >> gWriteUniforms loc2 val2
  {-# INLINE gWriteUniforms #-}

-- Record selectors and their fields
instance
  (KnownSymbol name, GLFloatUniformData c) =>
  GUniform (S1 ('MetaSel ('Just name) su ss ds) (K1 i c))
  where
  type
    GUniformLocations (S1 ('MetaSel ('Just name) su ss ds) (K1 i c)) =
      S1 ('MetaSel ('Just name) su ss ds) (K1 i UniformLoc)

  gGetUniformLocations shaderProg = do
    location <- glGetUniformLocation shaderProg $ symbolVal @name Proxy
    pure $ M1 $ K1 location
  {-# INLINE gGetUniformLocations #-}

  gWriteUniforms (M1 (K1 location)) (M1 (K1 value)) =
    writeUniform location value
  {-# INLINE gWriteUniforms #-}
