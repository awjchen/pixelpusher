{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Deriving boilerplate for records of GLSL types
module Client.Visuals.GL.Types (
  GLTypeName (..),
  GLRecordField (..),
  GLRecord (..),
) where

import Data.DList (DList)
import Data.DList qualified as DList
import Data.Int (Int32)
import Data.Kind
import Data.Proxy
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector.Storable.Sized (Vector)
import GHC.Generics
import GHC.TypeLits

import Pixelpusher.Custom.Float (Float2, Float3, Float4)

--------------------------------------------------------------------------------

class GLTypeName a where
  glslName :: Text
  glslArraySuffix :: Maybe Integer

instance GLTypeName Float where
  glslName = "float"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance GLTypeName Float2 where
  glslName = "vec2"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance GLTypeName Float3 where
  glslName = "vec3"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance GLTypeName Float4 where
  glslName = "vec4"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance GLTypeName Int32 where
  glslName = "int"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance GLTypeName Bool where
  glslName = "bool"
  {-# INLINE glslName #-}
  glslArraySuffix = Nothing
  {-# INLINE glslArraySuffix #-}

instance (KnownNat n) => GLTypeName (Vector n Float2) where
  glslName = "vec2"
  {-# INLINE glslName #-}
  glslArraySuffix = Just $ natVal (Proxy @n)
  {-# INLINE glslArraySuffix #-}

--------------------------------------------------------------------------------

data GLRecordField = GLRecordField
  { grf_fieldName :: Text
  , grf_typeName :: Text
  , grf_arraySuffix :: Text
  }

class GLRecord a where
  -- | Field names and the names of their GLSL types
  glFields :: [GLRecordField]
  default glFields :: (GGLRecord (Rep a)) => [GLRecordField]
  glFields = DList.toList $ gGlFields @(Rep a)
  {-# INLINE glFields #-}

class GGLRecord (f :: Type -> Type) where
  gGlFields :: DList GLRecordField

-- Data: passthrough
instance (GGLRecord f) => GGLRecord (M1 D c f) where
  gGlFields = gGlFields @f
  {-# INLINE gGlFields #-}

-- Constructors: passthrough
instance (GGLRecord f) => GGLRecord (M1 C c f) where
  gGlFields = gGlFields @f
  {-# INLINE gGlFields #-}

-- Products
instance (GGLRecord f, GGLRecord g) => GGLRecord (f :*: g) where
  gGlFields = gGlFields @f <> gGlFields @g
  {-# INLINE gGlFields #-}

-- Record selectors and their fields
instance
  (GLTypeName c, KnownSymbol name) =>
  GGLRecord (M1 S ('MetaSel ('Just name) su ss ds) (K1 i c))
  where
  gGlFields =
    DList.singleton
      GLRecordField
        { grf_fieldName = Text.pack (symbolVal @name Proxy)
        , grf_typeName = glslName @c
        , grf_arraySuffix =
            case glslArraySuffix @c of
              Nothing -> ""
              Just i -> "[" <> Text.pack (show i) <> "]"
        }
  {-# INLINE gGlFields #-}

-- Unit
instance GGLRecord U1 where
  gGlFields = DList.empty
  {-# INLINE gGlFields #-}
