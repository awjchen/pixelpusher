{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilyDependencies #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Ad-hoc embedded DSL for writing GLSL
module Client.Visuals.GL.DSL (
  Expr,
  render,
  literal,
  (.+),
  (+.),
  (.-),
  (-.),
  (.*),
  (*.),
  (./),
  (/.),
  Dim1 (..),
  Dim2 (..),
  Dim3 (..),
  Dim4 (..),
  vec2,
  vec3,
  vec4,
  (<#),
  (>#),
  (==#),
  sampleTexture2D,
  index,
  foldArrayN,
  ifThenElse',
  max',
  min',
  atan2,
  length,
  step,
  smoothstep,
  dot',
  exp',
  inversesqrt,
  clamp,
  mix,
  normalize,
  mod',
  and',
  floor',
  fract',
  abs',
  GLSL,
  CompiledShaders,
  vertShaderGlsl,
  fragShaderGlsl,
  compileShaders,
  bind,
  getTexture2D,
  -- Boilerplate
  GLSLStageLinkage,
  --
  GLKind (..),
  GLData,
  GLSLInput,
  GLUnit (..),
) where

import Prelude hiding (atan2, length)

import Control.Monad.Trans.State.Strict
import Data.DList qualified as DL
import Data.Foldable (toList)
import Data.Int (Int32)
import Data.Kind (Type)
import Data.List (intercalate)
import Data.Proxy
import Data.Sequence qualified as Seq
import Data.Text qualified as Text
import Data.Vector.Storable.Sized (Vector)
import GHC.Generics hiding (conName)
import GHC.TypeLits

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

import Client.Visuals.GL.Types
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject

--------------------------------------------------------------------------------

data Expr a where
  Variable :: String -> Expr a
  Literal :: (Literal a) => a -> Expr a
  InfixOp :: String -> Expr a -> Expr b -> Expr c
  BroadcastInfixOpR :: (FloatType a) => Char -> Expr Float -> Expr a -> Expr a
  BroadcastInfixOpL :: (FloatType a) => Char -> Expr a -> Expr Float -> Expr a
  Func1 :: String -> Expr a -> Expr b
  Func2 :: String -> Expr a -> Expr b -> Expr c
  Func3 :: String -> Expr a -> Expr b -> Expr c -> Expr d
  Func4 :: String -> Expr a -> Expr b -> Expr c -> Expr d -> Expr e
  Negate :: (Num (Expr a)) => Expr a -> Expr a
  Swizzle :: String -> Expr a -> Expr b
  IfThenElse :: Expr Bool -> Expr a -> Expr a -> Expr a
  ArraySubscript :: Expr (Vector n a) -> Expr Int32 -> Expr a -- no indexing safety

render :: Expr a -> String
render = \case
  Variable name -> name
  Literal x -> renderLit x
  InfixOp symbol expr1 expr2 -> renderInfixOp symbol expr1 expr2
  BroadcastInfixOpR symbol exprFloat exprVec ->
    renderBroadcastR symbol exprFloat exprVec
  BroadcastInfixOpL symbol exprVec exprFloat ->
    renderBroadcastL symbol exprVec exprFloat
  Func1 name expr -> func name [render expr]
  Func2 name expr1 expr2 -> func name [render expr1, render expr2]
  Func3 name expr1 expr2 expr3 ->
    func name [render expr1, render expr2, render expr3]
  Func4 name expr1 expr2 expr3 expr4 ->
    func name [render expr1, render expr2, render expr3, render expr4]
  Negate expr -> "-" ++ parensRender expr
  Swizzle str expr -> parensRender expr ++ "." ++ str
  IfThenElse exprBool expr1 expr2 ->
    concat
      [ parensRender exprBool
      , " ? "
      , parensRender expr1
      , " : "
      , parensRender expr2
      ]
  ArraySubscript arr idx -> parensRender arr ++ "[" ++ render idx ++ "]"

parens :: String -> String
parens s = "(" ++ s ++ ")"

func :: String -> [String] -> String
func name args = name ++ parens (intercalate ", " args)

parensRender :: Expr a -> String
parensRender expr = if isAtomic expr then render expr else parens (render expr)

isAtomic :: Expr a -> Bool
isAtomic = \case
  Variable{} -> True
  Literal{} -> True
  InfixOp{} -> False
  BroadcastInfixOpR{} -> False
  BroadcastInfixOpL{} -> False
  Func1{} -> True
  Func2{} -> True
  Func3{} -> True
  Func4{} -> True
  Negate{} -> False
  Swizzle{} -> True
  IfThenElse{} -> False
  ArraySubscript{} -> True

renderInfixOp :: String -> Expr a -> Expr b -> String
renderInfixOp symbol expr1 expr2 =
  parensRender expr1 ++ " " ++ symbol ++ " " ++ parensRender expr2

renderBroadcastR :: Char -> Expr Float -> Expr a -> String
renderBroadcastR symbol exprFloat exprVec =
  parensRender exprFloat ++ " " ++ [symbol] ++ " " ++ parensRender exprVec

renderBroadcastL :: Char -> Expr a -> Expr Float -> String
renderBroadcastL symbol exprVec exprFloat =
  parensRender exprVec ++ " " ++ [symbol] ++ " " ++ parensRender exprFloat

--------------------------------------------------------------------------------

class FloatType a

instance FloatType Float
instance FloatType Float2
instance FloatType Float3
instance FloatType Float4

--------------------------------------------------------------------------------

class Literal a where
  renderLit :: a -> String

instance Literal Float where
  renderLit = show

instance Literal Float2 where
  renderLit (Float2 x y) = func "vec2" $ map show [x, y]

instance Literal Float3 where
  renderLit (Float3 x y z) = func "vec3" $ map show [x, y, z]

instance Literal Float4 where
  renderLit (Float4 x y z w) = func "vec3" $ map show [x, y, z, w]

instance Literal Int32 where
  renderLit = show

literal :: (Literal a) => a -> Expr a
literal = Literal

--------------------------------------------------------------------------------
-- Numeric typeclasses

instance (Literal a, Num a) => Num (Expr a) where
  (+) = InfixOp "+"
  (-) = InfixOp "-"
  (*) = InfixOp "*"
  negate = Negate
  abs = Func1 "abs"
  signum = Func1 "sign"
  fromInteger = Literal . fromInteger

instance (Literal a, Fractional a) => Fractional (Expr a) where
  (/) = InfixOp "/"
  recip = InfixOp "/" (1 :: Expr a)
  fromRational = Literal . fromRational

instance (Literal a, Floating a) => Floating (Expr a) where
  pi = Literal pi
  exp = Func1 "exp"
  log = Func1 "log"
  sqrt = Func1 "sqrt"
  (**) = Func2 "pow"

  -- logBase :: a -> a -> a
  sin = Func1 "sin"
  cos = Func1 "cos"
  tan = Func1 "tan"
  asin = Func1 "asin"
  acos = Func1 "acos"
  atan = Func1 "atan" -- 1-parameter version of atan
  sinh = Func1 "sinh"
  cosh = Func1 "cosh"
  tanh = Func1 "tanh"
  asinh = Func1 "asinh"
  acosh = Func1 "acosh"
  atanh = Func1 "atanh"

-- GHC.Float.log1p :: a -> a
-- GHC.Float.expm1 :: a -> a
-- GHC.Float.log1pexp :: a -> a
-- GHC.Float.log1mexp :: a -> a

--------------------------------------------------------------------------------
-- Broadcast operations

-- Mnemonic: The side with a dot is the non-scalar

infixl 6 +.
(+.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(+.) = BroadcastInfixOpR '+'

infixl 6 .+
(.+) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.+) = BroadcastInfixOpL '+'

infixl 6 -.
(-.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(-.) = BroadcastInfixOpR '-'

infixl 6 .-
(.-) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.-) = BroadcastInfixOpL '-'

infixl 7 *.
(*.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(*.) = BroadcastInfixOpR '*'

infixl 7 .*
(.*) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(.*) = BroadcastInfixOpL '*'

infixl 7 /.
(/.) :: (FloatType a) => Expr Float -> Expr a -> Expr a
(/.) = BroadcastInfixOpR '/'

infixl 7 ./
(./) :: (FloatType a) => Expr a -> Expr Float -> Expr a
(./) = BroadcastInfixOpL '/'

--------------------------------------------------------------------------------
-- Swizzling
--
-- Currently, only "in-order" swizzles have been implemented

class Dim1 a where
  x_ :: Expr a -> Expr Float

  r_ :: Expr a -> Expr Float
  r_ = x_

class Dim2 a where
  y_ :: Expr a -> Expr Float
  xy_ :: Expr a -> Expr Float2
  yx_ :: Expr a -> Expr Float2

class Dim3 a where
  z_ :: Expr a -> Expr Float
  xz_ :: Expr a -> Expr Float2
  yz_ :: Expr a -> Expr Float2
  xyz_ :: Expr a -> Expr Float3

class Dim4 a where
  w_ :: Expr a -> Expr Float
  xw_ :: Expr a -> Expr Float2
  yw_ :: Expr a -> Expr Float2
  zw_ :: Expr a -> Expr Float2
  xyw_ :: Expr a -> Expr Float3
  xzw_ :: Expr a -> Expr Float3
  yzw_ :: Expr a -> Expr Float3
  xyzw_ :: Expr a -> Expr Float4

instance Dim1 Float where
  x_ = Swizzle "x"
instance Dim1 Float2 where
  x_ = Swizzle "x"
instance Dim1 Float3 where
  x_ = Swizzle "x"
instance Dim1 Float4 where
  x_ = Swizzle "x"

instance Dim2 Float2 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"
  yx_ = Swizzle "yx"
instance Dim2 Float3 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"
  yx_ = Swizzle "yx"
instance Dim2 Float4 where
  y_ = Swizzle "y"
  xy_ = Swizzle "xy"
  yx_ = Swizzle "yx"

instance Dim3 Float3 where
  z_ = Swizzle "z"
  xz_ = Swizzle "xz"
  yz_ = Swizzle "yz"
  xyz_ = Swizzle "xyz"
instance Dim3 Float4 where
  z_ = Swizzle "z"
  xz_ = Swizzle "xz"
  yz_ = Swizzle "yz"
  xyz_ = Swizzle "xyz"

instance Dim4 Float4 where
  w_ = Swizzle "w"
  xw_ = Swizzle "xw"
  yw_ = Swizzle "yw"
  zw_ = Swizzle "zw"
  xyw_ = Swizzle "xyw"
  xzw_ = Swizzle "xzw"
  yzw_ = Swizzle "yzw"
  xyzw_ = Swizzle "xyzw"

--------------------------------------------------------------------------------
-- Vector constructors

-- vec2

vec2 :: (Expr Float, Expr Float) -> Expr Float2
vec2 (x, y) = Func2 "vec2" x y

-- vec3

class Vec3 a where
  vec3 :: a -> Expr Float3

instance
  (a ~ Expr Float, b ~ Expr Float, c ~ Expr Float) =>
  Vec3 (a, b, c)
  where
  vec3 (x, y, z) = Func3 "vec3" x y z

instance (a ~ Expr Float2) => Vec3 (Expr Float, a) where
  vec3 (x, yz) = Func2 "vec3" x yz

instance (a ~ Expr Float) => Vec3 (Expr Float2, a) where
  vec3 (xy, z) = Func2 "vec3" xy z

-- vec4

class Vec4 a where
  vec4 :: a -> Expr Float4

instance
  (a ~ Expr Float, b ~ Expr Float, c ~ Expr Float, d ~ Expr Float) =>
  Vec4 (a, b, c, d)
  where
  vec4 (x, y, z, w) = Func4 "vec4" x y z w

instance (a ~ Expr Float2) => Vec4 (Expr Float, Expr Float, a) where
  vec4 (x, y, wz) = Func3 "vec4" x y wz

instance (a ~ Expr Float) => Vec4 (Expr Float, Expr Float2, a) where
  vec4 (x, yz, w) = Func3 "vec4" x yz w

instance (a ~ Expr Float, b ~ Expr Float) => Vec4 (Expr Float2, a, b) where
  vec4 (xy, z, w) = Func3 "vec4" xy z w

instance (a ~ Expr Float3) => Vec4 (Expr Float, a) where
  vec4 (x, yzw) = Func2 "vec4" x yzw

instance (a ~ Expr Float2) => Vec4 (Expr Float2, a) where
  vec4 (xy, zw) = Func2 "vec4" xy zw

instance (a ~ Expr Float) => Vec4 (Expr Float3, a) where
  vec4 (xyz, w) = Func2 "vec4" xyz w

--------------------------------------------------------------------------------
-- Comparison operators
-- Using a #-suffix to avoid clashes with the regular operators

(<#) :: Expr Float -> Expr Float -> Expr Bool
(<#) = InfixOp "<"

infix 4 <#

(>#) :: Expr Float -> Expr Float -> Expr Bool
(>#) = InfixOp ">"

infix 4 >#

(==#) :: (Literal a) => Expr a -> Expr a -> Expr Bool
(==#) = InfixOp "=="

infix 4 ==#

--------------------------------------------------------------------------------
-- Textures

sampleTexture2D :: Expr Texture2D -> Expr Float2 -> Expr Float4
sampleTexture2D = Func2 "texture"

--------------------------------------------------------------------------------
-- Arrays

-- | No bounds checking
index :: Expr (Vector n a) -> Expr Int32 -> Expr a
index = ArraySubscript

-- | No bounds checking
foldArrayN ::
  (GLTypeName b) =>
  Expr Int32 ->
  (Expr b -> Expr a -> GLSL (Expr b)) ->
  Expr b ->
  Expr (Vector n a) ->
  GLSL (Expr b)
foldArrayN n f z arr = do
  (countName, _countExpr) <- unsafeBind n
  (accName, accExpr) <- unsafeBind z
  loopIndexName <- GLSL newVar
  appendLine $
    concat
      [ "for(int "
      , loopIndexName
      , " = 0; "
      , loopIndexName
      , " < "
      , getVarName countName
      , ";++"
      , loopIndexName
      , ") {"
      ]
  newAccVal <- f accExpr (index arr (Variable loopIndexName))
  set accName newAccVal
  appendLine "}"
  pure accExpr

--------------------------------------------------------------------------------
-- Other functions

ifThenElse' :: Expr Bool -> Expr a -> Expr a -> Expr a
ifThenElse' = IfThenElse

max' :: (FloatType a) => Expr a -> Expr a -> Expr a
max' = Func2 "max"

min' :: (FloatType a) => Expr a -> Expr a -> Expr a
min' = Func2 "min"

atan2 :: (FloatType a) => Expr a -> Expr a -> Expr a
atan2 = Func2 "atan" -- 2-parameter version of atan

length :: (FloatType a) => Expr a -> Expr Float
length = Func1 "length"

-- Only one variant is implemented for now
step :: (FloatType a) => Expr Float -> Expr a -> Expr a
step = Func2 "step"

-- Only one variant is implemented for now
smoothstep :: (FloatType a) => Expr Float -> Expr Float -> Expr a -> Expr a
smoothstep = Func3 "smoothstep"

dot' :: (FloatType a) => Expr a -> Expr a -> Expr Float
dot' = Func2 "dot"

exp' :: (FloatType a) => Expr a -> Expr a
exp' = Func1 "exp"

inversesqrt :: (FloatType a) => Expr a -> Expr a
inversesqrt = Func1 "inversesqrt"

-- Only one variant is implemented for now
clamp :: (FloatType a) => Expr a -> Expr Float -> Expr Float -> Expr a
clamp = Func3 "clamp"

-- Only one variant is implemented for now
mix :: (FloatType a) => Expr a -> Expr a -> Expr Float -> Expr a
mix = Func3 "mix"

normalize :: (FloatType a) => Expr a -> Expr a
normalize = Func1 "normalize"

mod' :: (FloatType a) => Expr a -> Expr Float -> Expr a
mod' = Func2 "mod"

and' :: Expr Bool -> Expr Bool -> Expr Bool
and' = InfixOp "&&"

floor' :: (FloatType a) => Expr a -> Expr a
floor' = Func1 "floor"

fract' :: (FloatType a) => Expr a -> Expr a
fract' = Func1 "fract"

abs' :: (FloatType a) => Expr a -> Expr a
abs' = Func1 "abs"

--------------------------------------------------------------------------------

data GLSLState = GLSLState
  { nextVar :: Int
  , code :: Seq.Seq String
  , texture2DVars :: Seq.Seq String
  }

initialGLSLState :: GLSLState
initialGLSLState =
  GLSLState
    { nextVar = 0
    , code = mempty
    , texture2DVars = mempty
    }

newtype GLSL a = GLSL {runGLSL :: State GLSLState a}
  deriving newtype (Functor, Applicative, Monad)

newVar :: State GLSLState String
newVar = do
  varInt <- gets nextVar
  modify $ \st -> st{nextVar = varInt + 1}
  let varName = "_" ++ show varInt
  pure varName

-- Internal
appendLine :: String -> GLSL ()
appendLine line = GLSL $ modify $ \st -> st{code = code st Seq.|> line}

bind :: forall a. (GLTypeName a) => Expr a -> GLSL (Expr a)
bind = fmap snd . unsafeBind

newtype VarName a = VarName {getVarName :: String}

-- Internal
unsafeBind :: forall a. (GLTypeName a) => Expr a -> GLSL (VarName a, Expr a)
unsafeBind expr = GLSL $ do
  varName <- newVar

  let line =
        concat
          [ Text.unpack $ glslName @a
          , " "
          , varName
          , case glslArraySuffix @a of
              Nothing -> ""
              Just i -> "[" <> show i <> "]"
          , " = "
          , render expr
          , ";"
          ]
  modify $ \st -> st{code = code st Seq.|> line}

  pure (VarName varName, Variable varName)

-- Internal
set :: VarName a -> Expr a -> GLSL ()
set varName expr =
  GLSL $
    let line =
          concat
            [ getVarName varName
            , " = "
            , render expr
            , ";"
            ]
    in  modify $ \st -> st{code = code st Seq.|> line}

data Texture2D

-- | Each use of 'getTexture2D' generates a @uniform sampler2D@ declaration.
-- Use multiple times in order to access multiple textures.
getTexture2D :: GLSL (Expr Texture2D)
getTexture2D = GLSL $ do
  varName <- newVar
  modify $ \st -> st{texture2DVars = texture2DVars st Seq.|> varName}
  pure $ Variable varName

-- | GLSL vertex and fragment shader source code. The phantom type parameter
-- identifies the type of vertices expected by the vertex shader.
type CompiledShaders :: (GLKind -> Type) -> Type
data CompiledShaders vertices = CompiledShaders
  { _vertShaderGlsl :: String
  , _fragShaderGlsl :: String
  }

vertShaderGlsl :: CompiledShaders vertices -> String
vertShaderGlsl = _vertShaderGlsl

fragShaderGlsl :: CompiledShaders vertices -> String
fragShaderGlsl = _fragShaderGlsl

-- | Compile a vertex and fragment shader to a GLSL string. The output of the
-- vertex shader must match the input of the fragment shader. The shaders must
-- also use the same uniforms.
compileShaders ::
  forall vertices instances uniforms vars.
  ( GLSLInput (vertices Gpu)
  , GLAttribute (vertices Cpu)
  , GLSLInput (instances Gpu)
  , GLAttribute (instances Cpu)
  , GLSLInput (uniforms Gpu)
  , GLUniform (uniforms Cpu)
  , GLSLStageLinkage vars
  ) =>
  ( vertices Gpu ->
    instances Gpu ->
    uniforms Gpu ->
    GLSL (Expr Float2, vars)
  ) ->
  (uniforms Gpu -> vars -> GLSL (Expr Float4)) ->
  CompiledShaders vertices
compileShaders vertexShader fragmentShader =
  CompiledShaders
    { _vertShaderGlsl = compileVertexShader vertexShader
    , _fragShaderGlsl = compileFragmentShader fragmentShader
    }

-- | Compile a vertex shader to a GLSL string.
compileVertexShader ::
  forall vertices instances uniforms output.
  ( GLSLInput (vertices Gpu)
  , GLAttribute (vertices Cpu)
  , GLSLInput (instances Gpu)
  , GLAttribute (instances Cpu)
  , GLSLInput (uniforms Gpu)
  , GLUniform (uniforms Cpu)
  , GLSLStageLinkage output
  ) =>
  ( vertices Gpu ->
    instances Gpu ->
    uniforms Gpu ->
    GLSL (Expr Float2, output)
  ) ->
  String
compileVertexShader vertShader =
  let ((glPosition, outputs), glslState) =
        flip runState initialGLSLState . runGLSL $
          vertShader glslInput glslInput glslInput
      (nVertexFields, vertexLayout) = layoutDecs @(vertices Cpu) 0
      (_, instanceLayout) = layoutDecs @(instances Cpu) nVertexFields
      body = unlines $ toList $ code glslState
      texture2DUniformDeclarations =
        unlines $
          map texture2DUniformLine $
            toList $
              texture2DVars glslState
  in  unlines
        [ "#version 330 core"
        , ""
        , Text.unpack vertexLayout
        , Text.unpack instanceLayout
        , Text.unpack $ uniformDeclarationsGLSL @(uniforms Cpu)
        , texture2DUniformDeclarations
        , stageLink_outputDecs @output
        , "void main()"
        , "{"
        , body
        , stageLink_assign outputs
        , let glPosition4 = vec4 (glPosition, 0, 1)
          in  "gl_Position = " ++ render glPosition4 ++ ";"
        , "}"
        ]

-- | Compile a fragment shader to a GLSL string.
compileFragmentShader ::
  forall input uniforms.
  (GLSLInput (uniforms Gpu), GLUniform (uniforms Cpu)) =>
  (GLSLStageLinkage input) =>
  (uniforms Gpu -> input -> GLSL (Expr Float4)) ->
  String
compileFragmentShader fragShader =
  let (fragColor, glslState) =
        flip runState initialGLSLState . runGLSL $
          fragShader glslInput stageLink_vars
      body = unlines $ toList $ code glslState
      texture2DUniformDeclarations =
        unlines $
          map texture2DUniformLine $
            toList $
              texture2DVars glslState
  in  unlines
        [ "#version 330 core"
        , ""
        , Text.unpack $ uniformDeclarationsGLSL @(uniforms Cpu)
        , texture2DUniformDeclarations
        , ""
        , stageLink_inputDecs @input
        , "out vec4 FragColor;"
        , ""
        , "void main()"
        , "{"
        , body
        , "FragColor = " ++ render fragColor ++ ";"
        , "}"
        ]

texture2DUniformLine :: String -> String
texture2DUniformLine name =
  "uniform sampler2D " ++ name ++ ";"

--------------------------------------------------------------------------------
-- Boilerplate:
-- Creating variables for user-defined records of 'Expr's

prefixExprVar :: String -> String
prefixExprVar = (++) "_"

-- | For records of shader types used to transfer data between shader stages
class GLSLStageLinkage a where
  stageLink_vars :: a
  stageLink_inputDecs :: String
  stageLink_outputDecs :: String
  stageLink_assign :: a -> String

  default stageLink_vars :: (Generic a, GGLSLStageLinkage (Rep a)) => a
  stageLink_vars = to $ gStageLink_vars @(Rep a)

  default stageLink_inputDecs :: (GGLSLStageLinkage (Rep a)) => String
  stageLink_inputDecs = stageLinkageDecls @a "in"

  default stageLink_outputDecs :: (GGLSLStageLinkage (Rep a)) => String
  stageLink_outputDecs = stageLinkageDecls @a "out"

  default stageLink_assign :: (Generic a, GGLSLStageLinkage (Rep a)) => a -> String
  stageLink_assign x = unlines $ zipWith makeAssign fields values
    where
      fields = toList $ gStageLink_fields @(Rep a)
      values = toList $ gStageLink_values $ from x
      makeAssign ExprRecordField{erf_fieldName} value =
        concat [prefixExprVar erf_fieldName, " = ", value, ";"]

instance GLSLStageLinkage () where
  stageLink_vars = ()
  stageLink_inputDecs = ""
  stageLink_outputDecs = ""
  stageLink_assign () = ""

stageLinkageDecls :: forall a. (GGLSLStageLinkage (Rep a)) => String -> String
stageLinkageDecls str =
  unlines $
    flip map fields $ \ExprRecordField{..} ->
      concat
        [ str
        , " "
        , erf_typeName
        , " "
        , prefixExprVar erf_fieldName
        , erf_arraySuffix
        , ";"
        ]
  where
    fields = toList $ gStageLink_fields @(Rep a)

data ExprRecordField = ExprRecordField
  { erf_fieldName :: String
  , erf_typeName :: String
  , erf_arraySuffix :: String
  }

class GGLSLStageLinkage (f :: Type -> Type) where
  gStageLink_vars :: f x
  gStageLink_fields :: DL.DList ExprRecordField
  gStageLink_values :: f x -> DL.DList String

-- Data: pass-through
instance (GGLSLStageLinkage f) => GGLSLStageLinkage (D1 c f) where
  gStageLink_vars = M1 $ gStageLink_vars @f
  {-# INLINE gStageLink_vars #-}
  gStageLink_fields = gStageLink_fields @f
  {-# INLINE gStageLink_fields #-}
  gStageLink_values (M1 x) = gStageLink_values x
  {-# INLINE gStageLink_values #-}

-- Cons: pass-through
instance (GGLSLStageLinkage f) => GGLSLStageLinkage (C1 c f) where
  gStageLink_vars = M1 $ gStageLink_vars @f
  {-# INLINE gStageLink_vars #-}
  gStageLink_fields = gStageLink_fields @f
  {-# INLINE gStageLink_fields #-}
  gStageLink_values (M1 x) = gStageLink_values x
  {-# INLINE gStageLink_values #-}

-- Product: pass-through
instance
  (GGLSLStageLinkage f, GGLSLStageLinkage g) =>
  GGLSLStageLinkage (f :*: g)
  where
  gStageLink_vars = gStageLink_vars @f :*: gStageLink_vars @g
  {-# INLINE gStageLink_vars #-}
  gStageLink_fields = gStageLink_fields @f <> gStageLink_fields @g
  {-# INLINE gStageLink_fields #-}
  gStageLink_values (x :*: y) = gStageLink_values x <> gStageLink_values y
  {-# INLINE gStageLink_values #-}

-- Record selector and its field
instance
  (GLTypeName c, KnownSymbol name) =>
  GGLSLStageLinkage (S1 ('MetaSel ('Just name) su ss ds) (K1 i (Expr c)))
  where
  gStageLink_vars = M1 $ K1 $ Variable $ prefixExprVar (symbolVal @name Proxy)
  {-# INLINE gStageLink_vars #-}
  gStageLink_fields =
    DL.singleton
      ExprRecordField
        { erf_fieldName = symbolVal @name Proxy
        , erf_typeName = Text.unpack (glslName @c)
        , erf_arraySuffix =
            case glslArraySuffix @c of
              Nothing -> ""
              Just i -> "[" <> show i <> "]"
        }
  {-# INLINE gStageLink_fields #-}
  gStageLink_values (M1 (K1 expr)) = DL.singleton $ render expr
  {-# INLINE gStageLink_values #-}

--------------------------------------------------------------------------------
-- Boilerplate:
-- Higher-kinded data for mirroring user-defined records with shader types

type data GLKind = Cpu | Gpu

type GLData :: GLKind -> Type -> Type
type family GLData glKind ty = r | r -> ty glKind where
  -- Manually listing all supported types in order to satsify injectivity
  GLData Cpu Float = Float
  GLData Cpu Float2 = Float2
  GLData Cpu Float3 = Float3
  GLData Cpu Float4 = Float4
  GLData Cpu Int32 = Int32
  GLData Cpu Bool = Bool
  GLData Cpu (Vector n a) = Vector n a
  GLData Gpu ty = Expr ty

--------------------------------------------------------------------------------
-- Boilerplate

-- | For records of shader types used as inputs to shaders
class GLSLInput a where
  glslInput :: a
  default glslInput :: (Generic a, GGLSLInput (Rep a)) => a
  glslInput = to $ gGlslInput @(Rep a)

class GGLSLInput (f :: Type -> Type) where
  gGlslInput :: f x

-- Data: pass-through
instance (GGLSLInput f) => GGLSLInput (D1 c f) where
  gGlslInput = M1 $ gGlslInput @f
  {-# INLINE gGlslInput #-}

-- Cons: pass-through
instance (GGLSLInput f) => GGLSLInput (C1 c f) where
  gGlslInput = M1 $ gGlslInput @f
  {-# INLINE gGlslInput #-}

-- Product: pass-through
instance (GGLSLInput f, GGLSLInput g) => GGLSLInput (f :*: g) where
  gGlslInput = gGlslInput @f :*: gGlslInput @g
  {-# INLINE gGlslInput #-}

-- Record selector and its field
instance
  (GLTypeName c, KnownSymbol name) =>
  GGLSLInput (S1 ('MetaSel ('Just name) su ss ds) (K1 i (Expr c)))
  where
  gGlslInput = M1 $ K1 $ Variable $ symbolVal @name Proxy
  {-# INLINE gGlslInput #-}

--------------------------------------------------------------------------------

type GLUnit :: GLKind -> Type
data GLUnit k = GLUnit
  deriving stock (Generic)

deriving anyclass instance GLRecord (GLUnit Cpu)
deriving anyclass instance GLAttribute (GLUnit Cpu)

instance GLSLInput (GLUnit Gpu) where
  glslInput = GLUnit

instance GLSLStageLinkage (GLUnit Gpu) where
  stageLink_vars = GLUnit
  stageLink_inputDecs = ""
  stageLink_outputDecs = ""
  stageLink_assign GLUnit = ""
