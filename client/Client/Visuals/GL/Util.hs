module Client.Visuals.GL.Util (
  fromRGB_A,
  toRGB_A,
  overRGB,
  fromFloat22,
  fromFloat12,
  fromHex,
) where

import Data.Word (Word8)

import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )

fromRGB_A :: Float3 -> Float -> Float4
fromRGB_A (Float3 a b c) = Float4 a b c

toRGB_A :: Float4 -> (Float3, Float)
toRGB_A (Float4 a b c d) = (Float3 a b c, d)

overRGB :: (Float3 -> Float3) -> Float4 -> Float4
overRGB f (Float4 r g b a) =
  let Float3 r' g' b' = f (Float3 r g b)
  in  Float4 r' g' b' a

fromFloat22 :: Float2 -> Float2 -> Float4
fromFloat22 (Float2 a b) (Float2 c d) = Float4 a b c d

fromFloat12 :: Float -> Float2 -> Float3
fromFloat12 a (Float2 b c) = Float3 a b c

fromHex :: Word8 -> Word8 -> Word8 -> Float3
fromHex r g b = Float3 (f r) (f g) (f b)
  where
    f = (/ 255) . fromIntegral
