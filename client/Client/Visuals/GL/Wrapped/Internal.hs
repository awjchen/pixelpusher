module Client.Visuals.GL.Wrapped.Internal (
  GL (..),
) where

-- | A newtype wrapper around IO for GL actions only.
newtype GL a = GL {_runGL :: IO a}
  deriving newtype (Functor, Applicative, Monad)
