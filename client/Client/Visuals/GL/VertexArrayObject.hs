{-# LANGUAGE AllowAmbiguousTypes #-}

-- | Creating vetex array objects and instance data buffers
--
-- TODO: Rename module
module Client.Visuals.GL.VertexArrayObject (
  GLAttribute (..),
  setupVertexArrayObject,
  setupVertexArrayObjectInstanced,
) where

import Control.Monad (forM_)
import Data.List (scanl')
import Data.Proxy
import Foreign

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.Wrapped

-- | Create a vertex array object from the given attribute data
setupVertexArrayObject ::
  (GLAttribute vertexType) => [vertexType] -> GL VertexArray
setupVertexArrayObject attributes = do
  vao <- genVertexArray
  withVertexArray vao $ setupVertexAttributeBuffer attributes
  pure vao

-- | Create a vertex array object from the given attribute data, and allocate
-- an extra buffer to hold instance data of the type of the proxy argument.
setupVertexArrayObjectInstanced ::
  forall vertexType instanceType.
  (GLAttribute vertexType, GLAttribute instanceType) =>
  Word16 ->
  [vertexType] ->
  Proxy instanceType ->
  GL (VertexArray, Buffer)
setupVertexArrayObjectInstanced capacityCount attributes Proxy = do
  vao <- genVertexArray
  instanceBuffer <- withVertexArray vao $ do
    setupVertexAttributeBuffer attributes
    setupInstanceBuffer
      capacityCount
      (Proxy @instanceType)
      (length $ fieldSizes @vertexType)
  pure (vao, instanceBuffer)

setupVertexAttributeBuffer ::
  forall vertexType. (GLAttribute vertexType) => [vertexType] -> GL ()
setupVertexAttributeBuffer attributes = do
  -- Compute sizes
  let fieldFloats = fieldSizes @vertexType
      strideBytes = fromIntegral $ sum fieldFloats * sizeOfGLfloat
      bufferBytes =
        fromIntegral $
          strideBytes
            * fromIntegral (length attributes)

  -- Create GL buffer
  vbo <- genBuffer
  attributeData <- toFloatsInstanced attributes
  withBuffer GL_ARRAY_BUFFER vbo $ do
    glBufferData
      GL_ARRAY_BUFFER
      bufferBytes
      attributeData
      GL_STATIC_DRAW

    -- Configure vertex attributes
    let attributeIndices = [0 ..]
        cumulativeFloats = scanl' (+) 0 fieldFloats
    forM_ (zip3 attributeIndices fieldFloats cumulativeFloats) $
      \(idx, attrib_nFloats, offset_nFloats) -> do
        glVertexAttribPointer
          idx
          (fromIntegral attrib_nFloats)
          GL_FLOAT
          GL_FALSE
          strideBytes
          (offset_nFloats * sizeOfGLfloat)
        glEnableVertexAttribArray idx

setupInstanceBuffer ::
  forall instanceType.
  (GLAttribute instanceType) =>
  Word16 ->
  Proxy instanceType ->
  Int ->
  GL Buffer
setupInstanceBuffer capacityCount Proxy initialAttribute = do
  -- Compute sizes
  let fieldFloats = fieldSizes @instanceType
      strideBytes = fromIntegral $ sum fieldFloats * sizeOfGLfloat
      bufferBytes = fromIntegral strideBytes * fromIntegral capacityCount

  -- Create GL buffer
  instanceBuffer <- genBuffer
  withBuffer GL_ARRAY_BUFFER instanceBuffer $ do
    glBufferDataEmpty GL_ARRAY_BUFFER bufferBytes GL_DYNAMIC_DRAW

    -- Configure instance array
    let attributeIndices = [fromIntegral initialAttribute ..]
        cumulativeFloats = scanl' (+) 0 fieldFloats
    forM_ (zip3 attributeIndices fieldFloats cumulativeFloats) $
      \(idx, attrib_nFloats, offset_nFloats) -> do
        glVertexAttribPointer
          idx
          (fromIntegral attrib_nFloats)
          GL_FLOAT
          GL_FALSE
          strideBytes
          (offset_nFloats * sizeOfGLfloat)
        glEnableVertexAttribArray idx
        glVertexAttribDivisor idx 1

  pure instanceBuffer
