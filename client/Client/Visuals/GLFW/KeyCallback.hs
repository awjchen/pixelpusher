{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Redundant lambda" #-}

module Client.Visuals.GLFW.KeyCallback (
  KeyCallback (..),
  toGLFWKeyCallback,
) where

import Data.Maybe (fromMaybe)
import Graphics.UI.GLFW qualified as GLFW

newtype KeyCallback
  = KeyCallback (GLFW.Window -> GLFW.Key -> Int -> GLFW.KeyState -> GLFW.ModifierKeys -> Maybe (IO ()))

instance Semigroup KeyCallback where
  KeyCallback f1 <> KeyCallback f2 =
    KeyCallback $ \window key scanCode keyState modifiers ->
      case f1 window key scanCode keyState modifiers of
        Just action -> Just action
        Nothing -> f2 window key scanCode keyState modifiers

instance Monoid KeyCallback where
  mempty = KeyCallback $ \_ _ _ _ _ -> Nothing

toGLFWKeyCallback :: KeyCallback -> GLFW.KeyCallback
toGLFWKeyCallback (KeyCallback f) =
  \window key scancode keyState modifiers ->
    fromMaybe (pure ()) $ f window key scancode keyState modifiers
