{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Common pattern for instanced draw calls.
module Client.Visuals.StandardInstancedVisual (
  StandardInstancedVisual,
  StandardUniforms (..),
  StandardVParams (..),
  loadStandardVisual,
  drawStandardVisual,
) where

import Data.Kind (Type)
import Data.Proxy
import Foreign
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.Parameters.Test

import Client.Visuals.CoordinateSystems (CoordSystem, cs_axisScaling, cs_origin)
import Client.Visuals.GL.DSL hiding (length)
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped

--------------------------------------------------------------------------------

data StandardInstancedVisual a = StandardInstancedVisual
  { sv_vao :: VertexArray
  , sv_shaderProg :: ShaderProgram
  , sv_nVertices :: GLsizei
  , sv_instanceBuffer :: Buffer
  , sv_instanceBufferCapacityCount :: Word16
  , sv_uniformLocations :: UniformLocations (StandardUniforms Cpu)
  }

type StandardUniforms :: GLKind -> Type
data StandardUniforms k = StandardUniforms
  { su_aspectRatio :: GLData k Float
  , su_viewScale :: GLData k Float2
  , su_viewCenter :: GLData k Float2
  , su_test1 :: GLData k Float
  , su_test2 :: GLData k Float
  , su_test3 :: GLData k Float
  , su_test4 :: GLData k Float
  , su_test5 :: GLData k Float
  , su_test6 :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (StandardUniforms Cpu)
deriving anyclass instance GLUniform (StandardUniforms Cpu)
deriving anyclass instance GLSLInput (StandardUniforms Gpu)

type StandardVParams :: (GLKind -> Type) -> Type
data StandardVParams f = StandardVParams
  { svp_aspectRatio :: Float
  , svp_coordSystem :: CoordSystem
  , svp_testParams :: TestParams
  , svp_instances :: [f Cpu]
  }

loadStandardVisual ::
  forall instanceType vertexType.
  (GLAttribute (instanceType Cpu), GLAttribute (vertexType Cpu)) =>
  TimedFastLogger ->
  [vertexType Cpu] ->
  CompiledShaders vertexType ->
  Word16 ->
  GL (StandardInstancedVisual instanceType)
loadStandardVisual logger vertices glslShaders maxInstances = do
  (vao, instanceBuffer) <-
    setupVertexArrayObjectInstanced
      maxInstances
      vertices
      (Proxy :: Proxy (instanceType Cpu))
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl glslShaders)
      (fragShaderGlsl glslShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    StandardInstancedVisual
      { sv_vao = vao
      , sv_shaderProg = shaderProg
      , sv_nVertices = fromIntegral $ length vertices
      , sv_instanceBuffer = instanceBuffer
      , sv_instanceBufferCapacityCount = maxInstances
      , sv_uniformLocations = uniforms
      }

drawStandardVisual ::
  forall instanceType.
  (GLAttribute (instanceType Cpu)) =>
  StandardInstancedVisual instanceType ->
  StandardVParams instanceType ->
  GL ()
drawStandardVisual visual StandardVParams{..} =
  withVertexArray (sv_vao visual) $
    withShaderProgram (sv_shaderProg visual) $ do
      -- Set uniforms
      writeUniforms (sv_uniformLocations visual) $
        let coordSystem = svp_coordSystem
            TestParams{..} = svp_testParams
        in  StandardUniforms
              { su_aspectRatio = svp_aspectRatio
              , su_viewScale = Float.map2 recip $ cs_axisScaling coordSystem
              , su_viewCenter = cs_origin coordSystem
              , su_test1 = Fixed.toFloat gp_testFloat1
              , su_test2 = Fixed.toFloat gp_testFloat2
              , su_test3 = Fixed.toFloat gp_testFloat3
              , su_test4 = Fixed.toFloat gp_testFloat4
              , su_test5 = Fixed.toFloat gp_testFloat5
              , su_test6 = Fixed.toFloat gp_testFloat6
              }

      -- Set instance data
      let capacityCount = sv_instanceBufferCapacityCount visual
          instances = take (fromIntegral capacityCount) svp_instances
          nInstances = length instances
          bytes =
            fromIntegral $
              nInstances
                * sum (fieldSizes @(instanceType Cpu))
                * sizeOfGLfloat
      instanceData <- toFloatsInstanced instances
      withBuffer GL_ARRAY_BUFFER (sv_instanceBuffer visual) $
        glBufferSubData
          GL_ARRAY_BUFFER
          0
          bytes
          instanceData

      -- Draw
      glDrawArraysInstanced
        GL_TRIANGLE_STRIP
        0
        (sv_nVertices visual)
        (fromIntegral nInstances)
{-# INLINEABLE drawStandardVisual #-}
