{-# LANGUAGE TypeFamilies #-}

-- | Common arrangements of vertices for drawing 2D shapes.
module Client.Visuals.Vertices (
  Position (..),
  square,
  diamond,
  hexagon,
  annulus,
  disk,
  LabelledPosition (..),
  labelledAnnulus,
) where

import Data.Kind (Type)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2 (Float2))
import Pixelpusher.Custom.Float qualified as Float

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.VertexArrayObject

--------------------------------------------------------------------------------

-- | A wrapper around a 2D vector with the derived boilerplate needed for our
-- shader DSL.
type Position :: GLKind -> Type
newtype Position glKind = Position {position :: GLData glKind Float2}
  deriving stock (Generic)

deriving anyclass instance GLRecord (Position Cpu)
deriving anyclass instance GLAttribute (Position Cpu)
deriving anyclass instance GLSLInput (Position Gpu)

type LabelledPosition :: GLKind -> Type
data LabelledPosition k = LabelledPosition
  { label :: GLData k Float
  , position :: GLData k Float2
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (LabelledPosition Cpu)
deriving anyclass instance GLAttribute (LabelledPosition Cpu)
deriving anyclass instance GLSLInput (LabelledPosition Gpu)

-- | A square. Intended for use with TRIANGLE_STRIP.
square :: Float -> [Float2]
square radius =
  map
    (Float.map2 (* radius) . uncurry Float2)
    [(-1, -1), (-1, 1), (1, -1), (1, 1)]

-- | A diamond. Intended for use with TRIANGLE_STRIP.
diamond :: Float -> [Float2]
diamond radius =
  map
    (Float.map2 (* radius) . uncurry Float2)
    [(-1, 0), (0, 1), (0, -1), (1, 0)]

-- | A hexagon. Intended for use with TRIANGLE_STRIP.
hexagon :: Float -> [Float2]
hexagon radius = map (Float.map2 (* radius) . hexPt) [0, 1, 5, 2, 4, 3]

hexPt :: Int -> Float2
hexPt i =
  let a = 2 * pi * fromIntegral i / 6
      r = 2 / sqrt 3 -- 1 / cos(pi/6)
  in  Float2 (r * cos a) (r * sin a)

-- | A disk. Intended for use with TRIANGLE_STRIP.
disk :: Int -> Float -> [Float2]
disk n_int outerRadius =
  let innerRadius = 0 in annulus n_int innerRadius outerRadius

-- | An annulus. Intended for use with TRIANGLE_STRIP.
annulus :: Int -> Float -> Float -> [Float2]
annulus n_int innerRadius outerRadius =
  flip map (labelledAnnulus n_int innerRadius outerRadius) $
    \LabelledPosition{position} -> position

-- | An annulus. The third coordinate is 0 if a vertex belongs to the inner
-- circle and 1 if it belongs to the outer circle. Intended for use with
-- TRIANGLE_STRIP.
--
-- TODO: Merge implementation with `annulus`
labelledAnnulus :: Int -> Float -> Float -> [LabelledPosition Cpu]
labelledAnnulus n_int innerRadius outerRadius =
  let n_float = fromIntegral n_int
      toAngle x = 2 * pi * x / n_float
      coveringScale = 1 / cos (pi / n_float)
      outerCircle =
        flip map [0 .. n_int] $
          circle (outerRadius * coveringScale) . toAngle . fromIntegral
      innerCircle =
        flip map [0 .. n_int] $
          circle innerRadius . toAngle . (+ 0.5) . fromIntegral
  in  interleave
        (map (LabelledPosition 1) outerCircle)
        (map (LabelledPosition 0) innerCircle)
  where
    circle r ang = Float2 (r * cos ang) (r * sin ang)

--------------------------------------------------------------------------------
-- Helpers

interleave :: [a] -> [a] -> [a]
interleave (x : xs) (y : ys) = x : y : interleave xs ys
interleave xs [] = xs
interleave [] ys = ys
