{-# LANGUAGE RecordWildCards #-}

module Client.Visuals.SharedVisuals (
  damageFilterWeight,
  invulnerabilityFilter,
  flagColors,
  castAttackTicks,
  scoreTeamColors,
  shieldPowerupSpawnerRadius,
  flareLifetimeTicks,
  maxAspectRatio,
  psiStormOrbRadius,
) where

import Pixelpusher.Custom.Float (Float3 (Float3))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time (Ticks)

import Client.Theme
import Client.Visuals.TeamView (TeamColor (..))

--------------------------------------------------------------------------------

damageFilterWeight :: Float -> Float
damageFilterWeight recentDamage =
  min 1 (recentDamage / hardHitDamage)
  where
    hardHitDamage = 5

invulnerabilityFilter :: Float -> Float
invulnerabilityFilter x =
  let weight = 0.4
  in  weight + (1 - weight) * x

flagColors :: Theme -> TeamColor -> Float3
flagColors Theme{..} = \case
  TeamRed -> theme_teamRed_flagColor
  TeamBlue -> theme_teamBlue_flagColor

castAttackTicks :: (Floating a) => a
castAttackTicks = fromIntegral C.tickRate_hz / 12

scoreTeamColors :: TeamColor -> Float3
scoreTeamColors = \case
  TeamRed -> Float3 0.95 0.22 0.40
  TeamBlue -> Float3 0.00 0.50 1.00

shieldPowerupSpawnerRadius :: Float
shieldPowerupSpawnerRadius = 60

flareLifetimeTicks :: Ticks
flareLifetimeTicks = 20

maxAspectRatio :: Float
maxAspectRatio = 1.25

psiStormOrbRadius :: Float
psiStormOrbRadius = 4
