-- | Utility functions for rendering text.
module Client.Visuals.Text (
  stackLinesUpward,
  stackIndentedLinesUpward,
  stackLinesDownward,
  stackCompoundLinesDownward,
  shadowText,
  shadowTextWith,
  shadowOffsetText,
  leftPad,
) where

import Data.List (mapAccumL, mapAccumR)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Visuals.Shaders.Char

--------------------------------------------------------------------------------

stackLinesUpward ::
  (Traversable t) =>
  CharAlignment ->
  Float2 ->
  Float ->
  t (a, String) ->
  (Float2, t (CharLine' a))
stackLinesUpward horizontalAlignment bottomCorner lineSpacing =
  mapAccumR f bottomCorner
  where
    f :: Float2 -> (a, String) -> (Float2, CharLine' a)
    f anchor (a, text) =
      ( anchor & _2 +~ lineSpacing
      , (horizontalAlignment, anchor, [(a, text)])
      )
{-# INLINEABLE stackLinesUpward #-}

stackIndentedLinesUpward ::
  (Traversable t) =>
  CharAlignment ->
  Float2 ->
  Float ->
  t (a, Float, String) ->
  (Float2, t (CharLine' a))
stackIndentedLinesUpward horizontalAlignment bottomCorner lineSpacing =
  mapAccumR f bottomCorner
  where
    f :: Float2 -> (a, Float, String) -> (Float2, CharLine' a)
    f anchor (a, indentation, text) =
      ( anchor & _2 +~ lineSpacing
      , (horizontalAlignment, anchor & _1 +~ indentation, [(a, text)])
      )
{-# INLINEABLE stackIndentedLinesUpward #-}

stackLinesDownward ::
  (Traversable t) =>
  CharAlignment ->
  Float2 ->
  Float ->
  t (a, String) ->
  (Float2, t (CharLine' a))
stackLinesDownward horizontalAlignment topCorner lineSpacing =
  stackCompoundLinesDownward horizontalAlignment topCorner lineSpacing
    . fmap (: [])
{-# INLINEABLE stackLinesDownward #-}

stackCompoundLinesDownward ::
  (Traversable t) =>
  CharAlignment ->
  Float2 ->
  Float ->
  t [(a, String)] ->
  (Float2, t (CharLine' a))
stackCompoundLinesDownward horizontalAlignment topCorner lineSpacing =
  mapAccumL f topCorner
  where
    f :: Float2 -> [(a, String)] -> (Float2, CharLine' a)
    f topLeft textFragments =
      ( topLeft & _2 -~ lineSpacing
      , (horizontalAlignment, topLeft, textFragments)
      )
{-# INLINEABLE stackCompoundLinesDownward #-}

shadowText :: Float -> Float4 -> [CharLine] -> [CharLine]
shadowText fontSize shadowColor textLines =
  let shadowedTextLines =
        map
          (mapCharLine (const shadowColor) . shadowOffsetText fontSize)
          textLines
  in  shadowedTextLines ++ textLines

shadowTextWith :: Float -> (Float4 -> Float4) -> [CharLine] -> [CharLine]
shadowTextWith fontSize getShadowColor textLines =
  let shadowedTextLines =
        map
          (mapCharLine getShadowColor . shadowOffsetText fontSize)
          textLines
  in  shadowedTextLines ++ textLines

-- | Note: You have to manually append the returned lines before the text being
-- shadowed so that the original text is drawn over the shadow
shadowOffsetText :: Float -> CharLine' a -> CharLine' a
shadowOffsetText fontSize textLine =
  let offset = fontSize / 16
      offsetVec = Float2 offset (-offset)
  in  textLine & _2 +~ offsetVec
{-# INLINE shadowOffsetText #-}

leftPad :: Int -> a -> [a] -> [a]
leftPad n x xs
  | n > l = replicate (n - l) x ++ xs
  | otherwise = xs
  where
    l = length xs
