module Client.Visuals.MovementTrails (
  radiusFactor1,
  radiusFactor2,
  radiusFactor3,
  radiusFactor4,
  radiusFactor5,
  radiusFactor6,
  radiusFactor7,
  radiusFactor8,
  radiusFactor9,
  radiusFactor10,
  opacityFactor1,
  opacityFactor2,
  opacityFactor3,
  opacityFactor4,
  opacityFactor5,
  opacityFactor6,
  opacityFactor7,
  opacityFactor8,
  opacityFactor9,
  opacityFactor10,
) where

import Client.Visuals.GL.DSL

radiusFactor1
  , radiusFactor2
  , radiusFactor3
  , radiusFactor4
  , radiusFactor5
  , radiusFactor6
  , radiusFactor7
  , radiusFactor8
  , radiusFactor9
  , radiusFactor10 ::
    Expr Float
radiusFactor1 = radiusFactor 1
radiusFactor2 = radiusFactor 2
radiusFactor3 = radiusFactor 3
radiusFactor4 = radiusFactor 4
radiusFactor5 = radiusFactor 5
radiusFactor6 = radiusFactor 6
radiusFactor7 = radiusFactor 7
radiusFactor8 = radiusFactor 8
radiusFactor9 = radiusFactor 9
radiusFactor10 = radiusFactor 10

-- The quadratic component in the radius factor smoothens the relationship
-- between entity speed and movement trail salience.
--
-- With only a linear component, the movement trail is invisible up to a
-- speed proportional to the difference in radius between trail parts, and
-- thereafter increases in length proportionally to increases in speed.
--
-- If we use a quadratic component to attenuate the difference in radius
-- between latter parts of the movement trail, smaller velocities can be
-- visualized by the movement trail. Furthermore, the quadratic component
-- causes the change in radius between trail parts to decrease linearly;
-- this creates a smooth transition in the visual responsiveness to movement
-- speed from threshold speed.

radiusFactor :: Int -> Expr Float
radiusFactor i =
  literal $
    1
      - fromIntegral i * linearStep
      + fromIntegral (i * i) * linearStep / lastDiff
  where
    linearStep = 0.02
    numParts = 10
    lastDiff = 2 * numParts - 1

opacityFactor1
  , opacityFactor2
  , opacityFactor3
  , opacityFactor4
  , opacityFactor5
  , opacityFactor6
  , opacityFactor7
  , opacityFactor8
  , opacityFactor9
  , opacityFactor10 ::
    Expr Float
opacityFactor1 = opacityFactor 1
opacityFactor2 = opacityFactor 2
opacityFactor3 = opacityFactor 3
opacityFactor4 = opacityFactor 4
opacityFactor5 = opacityFactor 5
opacityFactor6 = opacityFactor 6
opacityFactor7 = opacityFactor 7
opacityFactor8 = opacityFactor 8
opacityFactor9 = opacityFactor 9
opacityFactor10 = opacityFactor 10

-- We decrease the opacity linearly. This results in a falling-off of
-- perceptual brightness at the tail end.

opacityFactor :: Int -> Expr Float
opacityFactor i =
  literal $ fromIntegral (numParts + 1 - i) * 0.065
  where
    numParts = 10
