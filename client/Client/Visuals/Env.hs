{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

-- | Context for all rendering
module Client.Visuals.Env (
  GLEnv (..),
  RefreshRate (..),
  getTicksPerFrame,
  getRefreshRate,
  RequestVSync (..),
  withGLEnv,
  FullscreenMode (..),
  getFullscreenMode,
  setFullscreenMode,
  toggleFullScreen,
  setViewport,
  getFramebufferDims,
  withKeyCallback,
  withCharCallback,
) where

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TVar
import Control.Exception (bracket, bracket_)
import Control.Monad (unless, void)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.ByteString.Char8 qualified as BS8
import Data.Foldable (traverse_)
import Data.Functor ((<&>))
import Data.IORef
import Data.Int (Int32)
import Data.Text (Text)
import Data.Text qualified as Text
import Graphics.GL.Core33
import Graphics.UI.GLFW (OpenGLProfile (..), WindowHint (..))
import Graphics.UI.GLFW qualified as GLFW
import System.Log.FastLogger

import Client.Controllers (controllerMapping)
import Client.Logging
import Client.Visuals.Assets.Fonts (signikaRegular)
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Wrapped (runGL)
import Client.Visuals.Shaders.Background
import Client.Visuals.Shaders.CastRipple
import Client.Visuals.Shaders.Char
import Client.Visuals.Shaders.Circle
import Client.Visuals.Shaders.ControlPoint
import Client.Visuals.Shaders.CooldownArc
import Client.Visuals.Shaders.CooldownBar
import Client.Visuals.Shaders.Cross
import Client.Visuals.Shaders.CursorAttract
import Client.Visuals.Shaders.CursorRepel
import Client.Visuals.Shaders.Dash
import Client.Visuals.Shaders.DashRing
import Client.Visuals.Shaders.Drone
import Client.Visuals.Shaders.DroneControlRadius
import Client.Visuals.Shaders.Explosion
import Client.Visuals.Shaders.Flag
import Client.Visuals.Shaders.Flare
import Client.Visuals.Shaders.Fog
import Client.Visuals.Shaders.Line
import Client.Visuals.Shaders.MinimapBackground
import Client.Visuals.Shaders.ObstacleRepulsion
import Client.Visuals.Shaders.Overseer
import Client.Visuals.Shaders.OverseerRemains
import Client.Visuals.Shaders.PingFlash
import Client.Visuals.Shaders.PixelFilter
import Client.Visuals.Shaders.PowerupSpawner
import Client.Visuals.Shaders.PsiStorm
import Client.Visuals.Shaders.PsiStormOrb
import Client.Visuals.Shaders.Quad
import Client.Visuals.Shaders.Ring
import Client.Visuals.Shaders.ShieldPowerup
import Client.Visuals.Shaders.SoftObstacle
import Client.Visuals.Shaders.Spark
import Client.Visuals.Shaders.Sparkle
import Client.Visuals.Shaders.TeamBase
import Client.Visuals.Shaders.WorldBounds
import Client.Visuals.StandardInstancedVisual

--------------------------------------------------------------------------------

-- | We create one OpenGL context which is passed between OS threads via
-- 'GLFW.makeContextCurrent'. We assume that only one thread makes calls to
-- OpenGL at a time.
--
-- Note that we should only use the main thread to make calls to GLFW. We
-- should also only use bound threads to make calls to OpenGL, after the
-- appropriate calls to 'GLFW.makeContextCurrent'.
data GLEnv = GLEnv
  { gle_window :: GLFW.Window
  , gle_refreshRate :: RefreshRate -- Monitor refresh rate at startup
  , gle_overseer :: StandardInstancedVisual OverseerGLInstance
  , gle_drone :: StandardInstancedVisual DroneInstance
  , gle_softObstacle :: StandardInstancedVisual SoftObstacleInstance
  , gle_explosion :: StandardInstancedVisual ExplosionInstance
  , gle_controlPoint :: StandardInstancedVisual ControlPointInstance
  , gle_circle :: StandardInstancedVisual CircleInstance
  , gle_quad :: StandardInstancedVisual QuadInstance
  , gle_droneControlRadius :: DroneControlRadiusVisual
  , gle_overseerRemains :: StandardInstancedVisual OverseerRemainsInstance
  , gle_char :: CharVisual
  , gle_fontAtlasSmall :: FontAtlas
  , gle_fontAtlasRegular :: FontAtlas
  , gle_fontAtlasLarge :: FontAtlas
  , gle_uiPadding :: Float
  , gle_pingFlash :: StandardInstancedVisual PingFlashInstance
  , gle_ring :: StandardInstancedVisual RingInstance
  , gle_spark :: StandardInstancedVisual SparkInstance
  , gle_background :: BackgroundVisual
  , gle_castRipple :: StandardInstancedVisual CastRippleInstance
  , gle_cross :: StandardInstancedVisual CrossInstance
  , gle_dash :: StandardInstancedVisual DashInstance
  , gle_dashRing :: StandardInstancedVisual DashRingInstance
  , gle_cooldownArc :: StandardInstancedVisual CooldownArcInstance
  , gle_cooldownBar :: StandardInstancedVisual CooldownBarInstance
  , gle_worldBounds :: WorldBoundsVisual
  , gle_psiStorm :: StandardInstancedVisual PsiStormInstance
  , gle_flag :: StandardInstancedVisual FlagInstance
  , gle_teamBase :: StandardInstancedVisual TeamBaseInstance
  , gle_fog :: FogVisual
  , gle_cursorAttract :: StandardInstancedVisual CursorAttractInstance
  , gle_cursorRepel :: StandardInstancedVisual CursorRepelInstance
  , gle_pollFramebufferSizeUpdate :: IO (Maybe (Int, Int)) -- should not be called by more than one thread at a time
  , gle_keyCallback :: IORef (Maybe GLFW.KeyCallback)
  -- ^ For simualting a stack of key callbacks. Not thread-safe.
  , gle_charCallback :: IORef (Maybe GLFW.CharCallback)
  -- ^ For simualting a stack of char callbacks. Not thread-safe.
  , gle_obstacleRepulsion :: StandardInstancedVisual ObstacleRepulsionInstance
  , gle_sparkle :: StandardInstancedVisual SparkleInstance
  , gle_shieldPowerup :: StandardInstancedVisual ShieldPowerupInstance
  , gle_line :: StandardInstancedVisual LineInstance
  , gle_psiStormOrb :: StandardInstancedVisual PsiStormOrbInstance
  , gle_powerupSpawner :: StandardInstancedVisual PowerupSpawnerInstance
  , gle_minimapBackground :: MinimapBackgroundVisual
  , gle_flare :: StandardInstancedVisual FlareInstance
  , gle_pixelFilter :: PixelFilterVisual
  }

data RefreshRate = Hz30 | Hz60 | Hz120

getTicksPerFrame :: (Num a) => GLEnv -> a
getTicksPerFrame glEnv =
  case gle_refreshRate glEnv of
    Hz30 -> 4
    Hz60 -> 2
    Hz120 -> 1

getRefreshRate :: (Num a) => GLEnv -> a
getRefreshRate glEnv =
  case gle_refreshRate glEnv of
    Hz30 -> 30
    Hz60 -> 60
    Hz120 -> 120

--------------------------------------------------------------------------------

data RequestVSync = EnableVSync | DisableVSync

-- | Must be called from the main thread
withGLEnv :: TimedFastLogger -> RequestVSync -> (GLEnv -> IO a) -> IO ()
withGLEnv logger vsyncRequest action =
  bracket initial final $ \case
    Left errMsg -> do
      GLFW.getError >>= withGlfwErrorText \glfwErr ->
        writeLog logger $ toLogStr $ errMsg <> "GLFW error: " <> glfwErr
    Right glEnv -> do
      GLFW.getError >>= withGlfwErrorText \glfwErr ->
        writeLog logger $
          toLogStr $
            "Successfully initialized graphics, but encountered a GLFW error: "
              <> glfwErr
      void $ action glEnv
  where
    initial = initGL logger vsyncRequest
    final _ = do
      GLFW.getError >>= withGlfwErrorText \glfwErr ->
        writeLog logger $ toLogStr $ "Found GLFW error: " <> glfwErr
      GLFW.terminate

withGlfwErrorText ::
  (Applicative f) => (Text -> f ()) -> Maybe (GLFW.Error, String) -> f ()
withGlfwErrorText action =
  traverse_ $ \(err, description) ->
    action $
      Text.pack (show err)
        <> "."
        <> "Description: "
        <> Text.pack description
        <> "."

initGL :: TimedFastLogger -> RequestVSync -> IO (Either Text GLEnv)
initGL logger vsyncRequest = runExceptT $ do
  initSuccess <- liftIO GLFW.init
  unless initSuccess $ throwE "Could not initialize GLFW."

  -- Window and monitor
  (window, windowLength, gle_refreshRate) <- do
    primMonitor <-
      ExceptT $
        annotate "Failed to detect monitor."
          <$> GLFW.getPrimaryMonitor
    videoMode <-
      ExceptT $
        annotate "Failed to get video mode."
          <$> GLFW.getVideoMode primMonitor
    let (windowLength, (windowPos_x, windowPos_y)) =
          defaultWindowLengthAndPos videoMode
        refreshRate = nearestRefreshRate $ GLFW.videoModeRefreshRate videoMode

    liftIO $ do
      GLFW.windowHint $ WindowHint'ContextVersionMajor 3
      GLFW.windowHint $ WindowHint'ContextVersionMinor 3
      GLFW.windowHint $ WindowHint'OpenGLProfile OpenGLProfile'Core
      GLFW.windowHint $ WindowHint'OpenGLForwardCompat True -- required for MacOS
    window <-
      ExceptT $
        annotate "Failed to create GLFW window."
          <$> GLFW.createWindow windowLength windowLength "pixelpusher" Nothing Nothing
    liftIO $ GLFW.setWindowPos window windowPos_x windowPos_y
    pure (window, windowLength, refreshRate)

  -- Controllers
  liftIO (GLFW.updateGamepadMappings (BS8.unpack controllerMapping)) >>= \case
    True -> pure ()
    False -> throwE "Failed to udpate gamepad mappings."

  liftIO $
    GLFW.getJoystickGUID GLFW.Joystick'1 >>= \case
      Nothing ->
        writeLog logger "Could not detect any controllers."
      Just guid ->
        GLFW.getGamepadName GLFW.Joystick'1 >>= \case
          Nothing ->
            writeLog logger $
              "Controller detected, but controller GUID " <> toLogStr guid <> "not found in database"
          Just name ->
            writeLog logger $ "Using controller '" <> toLogStr name <> "'"

  liftIO $ do
    -- Make context (must precede any OpenGL calls)
    GLFW.makeContextCurrent (Just window)

    -- Hide cursor. In menus, we don't support mouse input. In game, we render
    -- a custom cursor.
    GLFW.setCursorInputMode window GLFW.CursorInputMode'Hidden

    -- Check extensions
    swapControlTearAvailable <-
      (||)
        <$> GLFW.extensionSupported "WGL_EXT_swap_control_tear"
        <*> GLFW.extensionSupported "GLX_EXT_swap_control_tear"

    GLFW.swapInterval $
      case vsyncRequest of
        EnableVSync -> if swapControlTearAvailable then (-1) else 1
        DisableVSync -> 0

    -- Transparency
    glEnable GL_BLEND
    glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA

    (width, height) <- GLFW.getFramebufferSize window
    setViewport (fromIntegral width) (fromIntegral height)
    framebufferSizeUpdateVar <- newTVarIO (width, height)
    let gle_pollFramebufferSizeUpdate =
          Just <$> readTVarIO framebufferSizeUpdateVar
    GLFW.setFramebufferSizeCallback window $
      Just (framebufferSizeCallback framebufferSizeUpdateVar)

    let fontSize1080 = 24 :: Double
        fontSize = fontSize1080 * fromIntegral windowLength / 1080
        sizeSmall = round fontSize
        sizeRegular = round $ 1.5 * fontSize
        sizeLarge = round $ 2 * fontSize

    gle_fontAtlasSmall <- initFontAtlas signikaRegular sizeSmall
    gle_fontAtlasRegular <- initFontAtlas signikaRegular sizeRegular
    gle_fontAtlasLarge <- initFontAtlas signikaRegular sizeLarge
    let gle_uiPadding = fromIntegral $ (`div` 2) sizeLarge

    gle_keyCallback <- newIORef Nothing
    gle_charCallback <- newIORef Nothing

    runGL $ do
      gle_overseer <- loadOverseer logger
      gle_drone <- loadDrone logger
      gle_softObstacle <- loadSoftObstacle logger
      gle_explosion <- loadExplosion logger
      gle_controlPoint <- loadControlPoint logger
      gle_circle <- loadCircle logger
      gle_quad <- loadQuad logger
      gle_droneControlRadius <- loadDroneControlRadius logger
      gle_overseerRemains <- loadOverseerRemains logger
      gle_char <- loadChar logger
      gle_pingFlash <- loadPingFlash logger
      gle_ring <- loadRing logger
      gle_spark <- loadSpark logger
      gle_background <- loadBackground logger
      gle_castRipple <- loadCastRipple logger
      gle_cross <- loadCross logger
      gle_dash <- loadDash logger
      gle_dashRing <- loadDashRing logger
      gle_cooldownArc <- loadCooldownArc logger
      gle_cooldownBar <- loadCooldownBar logger
      gle_worldBounds <- loadWorldBounds logger
      gle_psiStorm <- loadPsiStorm logger
      gle_flag <- loadFlag logger
      gle_teamBase <- loadTeamBase logger
      gle_fog <- loadFog logger
      gle_cursorAttract <- loadCursorAttract logger
      gle_cursorRepel <- loadCursorRepel logger
      gle_obstacleRepulsion <- loadObstacleRepulsion logger
      gle_sparkle <- loadSparkle logger
      gle_shieldPowerup <- loadShieldPowerup logger
      gle_line <- loadLine logger
      gle_psiStormOrb <- loadPsiStormOrb logger
      gle_powerupSpawner <- loadPowerupSpawner logger
      gle_minimapBackground <- loadMinimapBackground logger
      gle_flare <- loadFlare logger
      gle_pixelFilter <- loadPixelFilter logger
      let gle_window = window
      pure GLEnv{..}

framebufferSizeCallback ::
  TVar (Int, Int) -> GLFW.Window -> Int -> Int -> IO ()
framebufferSizeCallback framebufferSizeVar _window w h = do
  -- Create a notification for setting the viewport on one other thread
  atomically $ writeTVar framebufferSizeVar (w, h)

-- | Helper. Set the OpenGL viewport to the entire framebuffer, except when the
-- width of the framebuffer is smaller than its height; in that case, set the
-- viewport to the largest square centered in the framebuffer.
setViewport :: Int32 -> Int32 -> IO ()
setViewport w h =
  if w >= h
    then glViewport 0 0 w h
    else glViewport 0 ((h - w) `div` 2) w w

-- | Get the current size of the framebuffer in terms of its minimum dimension
-- and its aspect ratio.
getFramebufferDims :: GLFW.Window -> IO (Int, Float)
getFramebufferDims window = do
  (width, height) <- GLFW.getFramebufferSize window
  let screenSize = min width height
      aspectRatio
        | height == 0 = 1
        | width >= height = fromIntegral width / fromIntegral height
        | otherwise = 1
  pure (screenSize, aspectRatio)

--------------------------------------------------------------------------------
-- Refresh rate

-- Arbitrary projection of a monitor's refresh rate onto the three supported
-- refresh rates supported by the game.
nearestRefreshRate :: Int -> RefreshRate
nearestRefreshRate x
  | x < 45 = Hz30
  | x < 90 = Hz60
  | otherwise = Hz120

--------------------------------------------------------------------------------
-- Fullscreen

data FullscreenMode = Fullscreen | Windowed

-- | Must be called from the main thread
getFullscreenMode :: GLEnv -> IO FullscreenMode
getFullscreenMode glEnv =
  GLFW.getWindowMonitor (gle_window glEnv) <&> \case
    Nothing -> Windowed
    Just _ -> Fullscreen

-- | Must be called from the main thread
setFullscreenMode :: GLEnv -> FullscreenMode -> IO ()
setFullscreenMode glEnv = setFullscreenMode' (gle_window glEnv)

setFullscreenMode' :: GLFW.Window -> FullscreenMode -> IO ()
setFullscreenMode' window requestedMode = do
  GLFW.getWindowMonitor window >>= \case
    Nothing ->
      -- Window is currently windowed
      case requestedMode of
        Windowed ->
          pure ()
        Fullscreen ->
          void $
            runMaybeT $ do
              monitor <- MaybeT GLFW.getPrimaryMonitor
              videoMode <- MaybeT $ GLFW.getVideoMode monitor
              liftIO $ GLFW.setFullscreen window monitor videoMode
    Just monitor ->
      -- Window is currently fullscreened
      case requestedMode of
        Fullscreen ->
          pure ()
        Windowed ->
          void $
            runMaybeT $ do
              (windowLength, (x, y)) <-
                fmap defaultWindowLengthAndPos $ MaybeT $ GLFW.getVideoMode monitor
              liftIO $
                GLFW.setWindowed window windowLength windowLength x y

-- | Must be called from the main thread
toggleFullScreen :: GLFW.Window -> IO ()
toggleFullScreen window = do
  mMonitor <- GLFW.getWindowMonitor window
  case mMonitor of
    Nothing -> void $
      runMaybeT $ do
        monitor <- MaybeT GLFW.getPrimaryMonitor
        videoMode <- MaybeT $ GLFW.getVideoMode monitor
        liftIO $ GLFW.setFullscreen window monitor videoMode
    Just monitor -> void $
      runMaybeT $ do
        (windowLength, (x, y)) <-
          fmap defaultWindowLengthAndPos $ MaybeT $ GLFW.getVideoMode monitor
        liftIO $
          GLFW.setWindowed window windowLength windowLength x y

defaultWindowLengthAndPos :: GLFW.VideoMode -> (Int, (Int, Int))
defaultWindowLengthAndPos videoMode =
  let width = GLFW.videoModeWidth videoMode
      height = GLFW.videoModeHeight videoMode
      minLength = min width height
      windowLength = (7 * minLength) `div` 8
      x = (width - windowLength) `div` 2
      y = (height - windowLength) `div` 2
  in  (windowLength, (x, y))

--------------------------------------------------------------------------------
-- GLFW KeyCallback and CharCallback management

-- | Install a new GLFW key callback before running the given action and
-- restore the current key callback after the action finishes.
withKeyCallback ::
  GLEnv -> GLFW.KeyCallback -> IO a -> IO a
withKeyCallback glEnv keyCallback action = do
  oldKeyCallback <- readIORef (gle_keyCallback glEnv)
  let window = gle_window glEnv
      before = do
        writeIORef (gle_keyCallback glEnv) $ Just keyCallback
        GLFW.setKeyCallback window $ Just keyCallback
      after = do
        writeIORef (gle_keyCallback glEnv) oldKeyCallback
        GLFW.setKeyCallback window oldKeyCallback
  bracket_ before after action

-- | Install a new GLFW char callback before running the given action and
-- restore the current char callback after the action finishes.
withCharCallback ::
  GLEnv -> GLFW.CharCallback -> IO a -> IO a
withCharCallback glEnv charCallback action = do
  oldCharCallback <- readIORef (gle_charCallback glEnv)
  let window = gle_window glEnv
      before = do
        writeIORef (gle_charCallback glEnv) $ Just charCallback
        GLFW.setCharCallback window $ Just charCallback
      after = do
        writeIORef (gle_charCallback glEnv) oldCharCallback
        GLFW.setCharCallback window oldCharCallback
  bracket_ before after action

--------------------------------------------------------------------------------
-- Helpers

annotate :: e -> Maybe a -> Either e a
annotate e m = case m of
  Nothing -> Left e
  Just a -> Right a
