{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The walls of the game world
module Client.Visuals.Shaders.WorldBounds (
  loadWorldBounds,
  drawWorldBounds,
  WorldBoundsVisual,
  WorldBoundsVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import Data.Kind (Type)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float3, Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data WorldBoundsVisual = WorldBoundsVisual
  { wbv_vao :: VertexArray
  , wbv_shaderProg :: ShaderProgram
  , wbv_nVertices :: GLsizei
  , wbv_uniformLocations :: UniformLocations (WorldBoundsVParams Cpu)
  }

type WorldBoundsVParams :: GLKind -> Type
data WorldBoundsVParams k = WorldBoundsVParams
  { wbvp_aspectRatio :: GLData k Float
  , wbvp_viewScale :: GLData k Float
  , wbvp_viewCenter :: GLData k Float2
  , wbvp_modelScale :: GLData k Float
  , wbvp_modelTrans :: GLData k Float2
  , wbvp_color :: GLData k Float3
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (WorldBoundsVParams Cpu)
deriving anyclass instance GLUniform (WorldBoundsVParams Cpu)
deriving anyclass instance GLSLInput (WorldBoundsVParams Gpu)

--------------------------------------------------------------------------------

drawWorldBounds :: WorldBoundsVisual -> WorldBoundsVParams Cpu -> GL ()
drawWorldBounds worldBounds params =
  withVertexArray (wbv_vao worldBounds) $
    withShaderProgram (wbv_shaderProg worldBounds) $ do
      -- Set uniforms
      writeUniforms (wbv_uniformLocations worldBounds) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (wbv_nVertices worldBounds)

loadWorldBounds :: TimedFastLogger -> GL WorldBoundsVisual
loadWorldBounds logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    WorldBoundsVisual
      { wbv_vao = vao
      , wbv_shaderProg = shaderProg
      , wbv_nVertices = fromIntegral $ P.length vertices
      , wbv_uniformLocations = uniforms
      }

vertices :: [LabelledPosition Cpu]
vertices = labelledAnnulus 120 0.99 1.01

compiledShaders :: CompiledShaders LabelledPosition
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {worldPos :: Expr Float2}
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  LabelledPosition Gpu ->
  GLUnit Gpu ->
  WorldBoundsVParams Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader LabelledPosition{..} GLUnit WorldBoundsVParams{..} = do
  radius <-
    bind $ (1 - label) * wbvp_modelScale + label * (wbvp_modelScale + fadeWidth)
  worldPos <- bind $ wbvp_modelTrans + radius *. position
  screenPos <-
    bind $
      wbvp_viewScale
        *. (worldPos - wbvp_viewCenter)
        / vec2 (wbvp_aspectRatio, 1)
  pure (screenPos, PipelineVars worldPos)

fragShader :: WorldBoundsVParams Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader WorldBoundsVParams{..} PipelineVars{worldPos} = do
  r <- bind $ length worldPos
  dr <- bind $ r - wbvp_modelScale
  innerMask <- bind $ smoothstep (-0.5) 0.5 dr
  outerMask <- do
    fadeFrac <- bind $ max' 0 $ min' 1 $ 1 - dr / fadeWidth
    fadeFrac2 <- bind $ fadeFrac * fadeFrac
    bind $ fadeFrac2 * fadeFrac2
  opacity <- bind $ innerMask * outerMask
  pure $ vec4 (wbvp_color, opacity)

fadeWidth :: Expr Float
fadeWidth = 180
