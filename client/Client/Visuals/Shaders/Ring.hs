{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic circle
module Client.Visuals.Shaders.Ring (
  loadRing,
  drawRing,
  RingInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Constants qualified as CC
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type RingInstance :: GLKind -> Type
data RingInstance k = RingInstance
  { ri_modelScale :: GLData k Float
  , ri_modelTrans :: GLData k Float2
  , ri_color :: GLData k Float4
  , ri_thickness :: GLData k Float
  , ri_fuzziness :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (RingInstance Cpu)
deriving anyclass instance GLAttribute (RingInstance Cpu)
deriving anyclass instance GLSLInput (RingInstance Gpu)

loadRing :: TimedFastLogger -> GL (StandardInstancedVisual RingInstance)
loadRing logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawRing ::
  StandardInstancedVisual RingInstance ->
  StandardVParams RingInstance ->
  GL ()
drawRing = drawStandardVisual

-- This visual is currently only used for pings
maxInstances :: Word16
maxInstances = CC.maxVisiblePings

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.03

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  , size :: Expr Float
  , thickness :: Expr Float
  , fuzziness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  RingInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} RingInstance{..} StandardUniforms{..} =
  pure $
    let screenPos =
          su_viewScale
            * (ri_modelTrans - su_viewCenter + ri_modelScale *. position)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { pos = position .* ri_modelScale
            , color = ri_color
            , size = ri_modelScale
            , thickness = ri_thickness
            , fuzziness = ri_fuzziness
            }
    in  (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{pos, color, size, thickness, fuzziness} = do
  r <- bind $ length pos
  let fuzz_width = fuzziness
      outer_bound = size
      inner_bound = size - thickness
  outer_mask <-
    bind $
      1 - smoothstep (outer_bound - fuzz_width) (outer_bound + fuzz_width) r
  inner_mask <-
    bind $
      smoothstep (inner_bound - fuzz_width) (inner_bound + fuzz_width) r
  opacity <- bind $ inner_mask * outer_mask
  pure $ vec4 (xyz_ color, w_ color * opacity)
