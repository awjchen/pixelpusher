{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic disks
module Client.Visuals.Shaders.Circle (
  loadCircle,
  drawCircle,
  CircleInstance (..),
) where

import Prelude hiding (length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CircleInstance :: GLKind -> Type
data CircleInstance k = CircleInstance
  { ci_size :: GLData k Float
  , ci_center :: GLData k Float2
  , ci_color :: GLData k Float4
  , ci_fuzziness :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CircleInstance Cpu)
deriving anyclass instance GLAttribute (CircleInstance Cpu)
deriving anyclass instance GLSLInput (CircleInstance Gpu)

--------------------------------------------------------------------------------

loadCircle :: TimedFastLogger -> GL (StandardInstancedVisual CircleInstance)
loadCircle logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawCircle ::
  StandardInstancedVisual CircleInstance ->
  StandardVParams CircleInstance ->
  GL ()
drawCircle = drawStandardVisual

maxInstances :: Word16
maxInstances =
  fromIntegral C.maxActorsPerGame -- overseers on minimap
    + 2 -- team bases on minimap

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.05

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , fuzziness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CircleInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CircleInstance{..} StandardUniforms{..} = do
  pos <- bind $ (ci_size + 0.5 * ci_fuzziness) *. position
  screenPos <-
    bind $
      su_viewScale
        * (ci_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let radius = ci_size
      color = ci_color
      fuzziness = ci_fuzziness
  pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ r - radius
  fuzz <- bind $ 0.5 * fuzziness
  opacity <- bind $ 1 - smoothstep (-fuzz) fuzz dr
  pure $ vec4 (xyz_ color, opacity * w_ color)
