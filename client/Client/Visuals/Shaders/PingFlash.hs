{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Effect for loud visual notification
module Client.Visuals.Shaders.PingFlash (
  loadPingFlash,
  drawPingFlash,
  PingFlashInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Constants qualified as CC
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type PingFlashInstance :: GLKind -> Type
data PingFlashInstance k = PingFlashInstance
  { pfi_size :: GLData k Float
  , pfi_center :: GLData k Float2
  , pfi_color :: GLData k Float4
  , pfi_ticksElapsed :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (PingFlashInstance Cpu)
deriving anyclass instance GLAttribute (PingFlashInstance Cpu)
deriving anyclass instance GLSLInput (PingFlashInstance Gpu)

loadPingFlash ::
  TimedFastLogger -> GL (StandardInstancedVisual PingFlashInstance)
loadPingFlash logger =
  loadStandardVisual logger vertices compiledShaders CC.maxVisiblePings

drawPingFlash ::
  StandardInstancedVisual PingFlashInstance ->
  StandardVParams PingFlashInstance ->
  GL ()
drawPingFlash = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  PingFlashInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} PingFlashInstance{..} StandardUniforms{..} = do
  progressFrac <-
    bind $ pfi_ticksElapsed / (1.5 * fromIntegral C.tickRate_hz)
  compl <- bind $ 1 - progressFrac
  progress <- bind $ 1 - compl * compl * compl
  pure $
    let screenPos =
          su_viewScale
            * (pfi_center - su_viewCenter + pfi_size *. position)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { pos = position
            , color = pfi_color
            , progress = progress
            , fuzziness = 1.5 / pfi_size
            }
    in  (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  , progress :: Expr Float
  , fuzziness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  outerCutoff <- bind $ 1 - exp (-2 * progress)
  ring_width <- bind $ 0.04 + 0.2 * (1 - progress)
  innerCutoff <- bind $ outerCutoff - ring_width
  innerMask <-
    bind $
      smoothstep (innerCutoff - fuzziness) (innerCutoff + fuzziness) r
  outerMask <-
    bind $
      1 - smoothstep (outerCutoff - fuzziness) (outerCutoff + fuzziness) r
  opacity <- bind $ innerMask * outerMask
  pure $ vec4 (xyz_ color, w_ color * opacity)
