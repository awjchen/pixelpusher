{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Lines of text
module Client.Visuals.Shaders.Char (
  loadChar,
  drawChar,
  CharVisual,
  CharVParams (..),
  CharLine,
  CharLine',
  mapCharLine,
  CharAlignment (..),
) where

import Data.Char (ord)
import Data.IntMap.Strict qualified as IM
import Data.Kind (Type)
import Data.Maybe (mapMaybe)
import Data.Proxy
import Data.Traversable (mapAccumL)
import Foreign
import GHC.Generics
import Graphics.GL.Types
import Lens.Micro.Platform.Custom hiding (chars)
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)
import Pixelpusher.Custom.Float qualified as Float

import Client.Visuals.FontAtlas
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL hiding (step)
import Client.Visuals.GL.Types
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data CharVisual = CharVisual
  { cv_vao :: VertexArray
  , cv_shaderProg :: ShaderProgram
  , cv_nVertices :: GLsizei
  , cv_instanceBuffer :: Buffer
  , cv_instanceBufferCapacityCount :: Word16
  , cv_uniformLocations :: UniformLocations (CharUniforms Cpu)
  }

data CharVParams = CharVParams
  { cvp_aspectRatio :: Float
  , cvp_fontAtlas :: FontAtlas
  , cvp_screenSize :: Float
  , cvp_lines :: [CharLine]
  }

type CharLine = CharLine' Float4 -- color

type CharLine' a = (CharAlignment, ScreenPosition, [(a, String)])

type ScreenPosition = Float2

data CharAlignment = CharAlignLeft | CharAlignCenter | CharAlignRight

mapCharLine :: (a -> b) -> CharLine' a -> CharLine' b
mapCharLine = over (_3 . traverse . _1)

type CharUniforms :: GLKind -> Type
data CharUniforms k = CharUniforms
  { cu_aspectRatio :: GLData k Float
  , cu_screenSize :: GLData k Float
  , cu_textureSize :: GLData k Float2
  }
  deriving stock (Generic)

deriving stock instance Show (CharUniforms Cpu)
deriving anyclass instance GLRecord (CharUniforms Cpu)
deriving anyclass instance GLUniform (CharUniforms Cpu)
deriving anyclass instance GLSLInput (CharUniforms Gpu)

type CharInstance :: GLKind -> Type
data CharInstance k = CharInstance
  { ci_screenPos :: GLData k Float2 -- pixels, bottom left
  , ci_texturePos :: GLData k Float -- pixels, bottom left
  , ci_charSize :: GLData k Float2 -- pixels
  , ci_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving stock instance Show (CharInstance Cpu)
deriving anyclass instance GLRecord (CharInstance Cpu)
deriving anyclass instance GLAttribute (CharInstance Cpu)
deriving anyclass instance GLSLInput (CharInstance Gpu)

--------------------------------------------------------------------------------

drawChar :: CharVisual -> CharVParams -> GL ()
drawChar visual params = do
  glActiveTexture GL_TEXTURE0
  withVertexArray (cv_vao visual) $
    withShaderProgram (cv_shaderProg visual) $ do
      let atlas = cvp_fontAtlas params
          charMap = fa_charMap atlas

      -- Set uniforms
      let uniforms =
            CharUniforms
              { cu_aspectRatio = cvp_aspectRatio params
              , cu_screenSize = cvp_screenSize params
              , cu_textureSize =
                  Float.fromVec2 $
                    fromIntegral <$> fa_textureSize atlas
              }
      writeUniforms (cv_uniformLocations visual) uniforms

      -- Generate instances
      let alignedCharInstances =
            concatMap (uncurry3 (makeInstances charMap)) $ cvp_lines params

      -- Set instance data
      let capacityCount = fromIntegral $ cv_instanceBufferCapacityCount visual
          instances = take capacityCount alignedCharInstances
          nInstances = Prelude.length instances
          bytes =
            fromIntegral $
              nInstances * sum (fieldSizes @(CharInstance Cpu)) * sizeOfGLfloat

      instanceData <- toFloatsInstanced instances
      withBuffer GL_ARRAY_BUFFER (cv_instanceBuffer visual) $
        glBufferSubData
          GL_ARRAY_BUFFER
          0
          bytes
          instanceData

      -- Draw
      withTexture GL_TEXTURE_2D (fa_texture atlas) $
        glDrawArraysInstanced
          GL_TRIANGLE_STRIP
          0
          (cv_nVertices visual)
          (fromIntegral nInstances)
  where
    makeInstances ::
      IM.IntMap AtlasChar ->
      CharAlignment ->
      Float2 ->
      [(Float4, String)] ->
      [CharInstance Cpu]
    makeInstances charMap align screenPos coloredText =
      let chars =
            flip concatMap coloredText $ \(c, str) ->
              map (c,) $ mapMaybe (flip IM.lookup charMap . ord) str
          initialOrigin = screenPos
          (finalOrigin, rawCharInstances) = mapAccumL step initialOrigin chars
          advance = finalOrigin ^. _1 - initialOrigin ^. _1
          alignedCharInstances = case align of
            CharAlignLeft -> rawCharInstances
            CharAlignCenter ->
              flip map rawCharInstances $ \charInst ->
                charInst
                  { ci_screenPos =
                      ci_screenPos charInst & _1 -~ (0.5 * advance)
                  }
            CharAlignRight ->
              flip map rawCharInstances $ \charInst ->
                charInst
                  { ci_screenPos =
                      ci_screenPos charInst & _1 -~ advance
                  }
      in  alignedCharInstances

    step :: Float2 -> (Float4, AtlasChar) -> (Float2, CharInstance Cpu)
    step origin (color, atlasChar) =
      let charDims = ac_charDims atlasChar
          Float2 w h = cd_size charDims
          charInstance =
            CharInstance
              { ci_screenPos =
                  origin -- pixels, bottom left
                    + cd_bearing charDims
                    + Float2 0 (-h)
              , ci_texturePos = ac_atlasPosition atlasChar -- pixels, bottom left
              , ci_charSize = Float2 w h -- pixels
              , ci_color = color
              }
      in  (origin & _1 +~ cd_advance charDims, charInstance)

--------------------------------------------------------------------------------

loadChar :: TimedFastLogger -> GL CharVisual
loadChar logger = do
  let instanceBufferSizeCount = 2048 + 1024
  (vao, instanceBuffer) <-
    setupVertexArrayObjectInstanced
      instanceBufferSizeCount
      vertices
      (Proxy @(CharInstance Cpu))
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniformLocs <- getUniformLocations shaderProg
  pure
    CharVisual
      { cv_vao = vao
      , cv_shaderProg = shaderProg
      , cv_nVertices = fromIntegral $ Prelude.length vertices
      , cv_instanceBuffer = instanceBuffer
      , cv_instanceBufferCapacityCount = instanceBufferSizeCount
      , cv_uniformLocations = uniformLocs
      }

vertices :: [Position Cpu]
vertices =
  map (Position . uncurry Float2) [(0, 0), (0, 1), (1, 0), (1, 1)]

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { texCoords :: Expr Float2
  , textColor :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CharInstance Gpu ->
  CharUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CharInstance{..} CharUniforms{..} = do
  screenRelPos <- bind $ position * ci_charSize
  screenPos <-
    bind $
      -- This transform is a hack so that I didn't have to change a lot of code
      -- when introducing a non-square aspect ratio. It causes text drawn text
      -- at (0, 0) to be placed the bottom-left of the largest square centered
      -- in the window.
      ((ci_screenPos + screenRelPos) .* (2 / cu_screenSize) .+ (-1))
        / vec2 (cu_aspectRatio, 1)
  let pipelineVars =
        PipelineVars
          { texCoords =
              vec2
                ( (ci_texturePos + x_ position * x_ ci_charSize) / x_ cu_textureSize
                , (y_ ci_charSize - y_ position * y_ ci_charSize) / y_ cu_textureSize
                )
          , textColor = ci_color
          }
  pure (screenPos, pipelineVars)

fragShader :: CharUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  atlasTexture <- getTexture2D
  sampled <- bind $ r_ $ sampleTexture2D atlasTexture texCoords
  pure $ vec4 (xyz_ textColor, w_ textColor * sampled)

--------------------------------------------------------------------------------
-- Helpers

uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (a, b, c) = f a b c
