{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Client.Visuals.Shaders.Flare (
  loadFlare,
  drawFlare,
  FlareInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type FlareInstance :: GLKind -> Type
data FlareInstance k = FlareInstance
  { fi_center :: GLData k Float2
  , fi_radius :: GLData k Float
  , fi_bloomWidth :: GLData k Float
  , fi_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (FlareInstance Cpu)
deriving anyclass instance GLAttribute (FlareInstance Cpu)
deriving anyclass instance GLSLInput (FlareInstance Gpu)

loadFlare :: TimedFastLogger -> GL (StandardInstancedVisual FlareInstance)
loadFlare logger =
  loadStandardVisual logger vertices compiledShaders maxInstances
  where
    maxInstances = fromIntegral C.maxActorsPerGame

drawFlare ::
  StandardInstancedVisual FlareInstance ->
  StandardVParams FlareInstance ->
  GL ()
drawFlare = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

-- Visually, the brightness of the normal distribution 2.5 standard
-- deviations from its mean is negligible.

vertShader ::
  Position Gpu ->
  FlareInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} FlareInstance{..} StandardUniforms{..} = do
  pos <- bind $ position .* (fi_radius + fi_bloomWidth * 2.5)
  let screenPos =
        su_viewScale
          * (fi_center - su_viewCenter + pos)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { pos = pos
          , centerRadius = fi_radius
          , bloomWidth = fi_bloomWidth
          , color = fi_color
          }
  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , centerRadius :: Expr Float
  , bloomWidth :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ max' 0 (r - centerRadius)
  x <- bind $ dr / bloomWidth
  opacity <- bind $ exp (-x * x)
  pure $ vec4 (xyz_ color, w_ color * opacity)
