{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Screen effect on overseer death
module Client.Visuals.Shaders.PixelFilter (
  loadPixelFilter,
  drawPixelFilter,
  PixelFilterVisual,
  PixelFilterVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import Data.Kind (Type)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data PixelFilterVisual = PixelFilterVisual
  { pfv_vao :: VertexArray
  , pfv_shaderProg :: ShaderProgram
  , pfv_nVertices :: GLsizei
  , pfv_uniformLocations :: UniformLocations (PixelFilterVParams Cpu)
  }

type PixelFilterVParams :: GLKind -> Type
data PixelFilterVParams k = PixelFilterVParams
  { pfvp_screenDims :: GLData k Float2
  , pfvp_fadeFrac :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (PixelFilterVParams Cpu)
deriving anyclass instance GLUniform (PixelFilterVParams Cpu)
deriving anyclass instance GLSLInput (PixelFilterVParams Gpu)

drawPixelFilter :: PixelFilterVisual -> PixelFilterVParams Cpu -> GL ()
drawPixelFilter fog params =
  withVertexArray (pfv_vao fog) $
    withShaderProgram (pfv_shaderProg fog) $ do
      -- Set uniforms
      writeUniforms (pfv_uniformLocations fog) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (pfv_nVertices fog)

loadPixelFilter :: TimedFastLogger -> GL PixelFilterVisual
loadPixelFilter logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    PixelFilterVisual
      { pfv_vao = vao
      , pfv_shaderProg = shaderProg
      , pfv_nVertices = fromIntegral $ P.length vertices
      , pfv_uniformLocations = uniforms
      }

vertices :: [Position Cpu]
vertices = map Position $ square 1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pixelHeight :: Expr Float2
  , fadeFrac :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  GLUnit Gpu ->
  PixelFilterVParams Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} GLUnit PixelFilterVParams{..} = do
  let screenPos = position
  pixelHeight <- bind $ (0.5 * position + 0.5) * pfvp_screenDims
  pure
    ( screenPos
    , PipelineVars
        { pixelHeight = pixelHeight
        , fadeFrac = pfvp_fadeFrac
        }
    )

fragShader :: PixelFilterVParams Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader PixelFilterVParams{} PipelineVars{..} = do
  h <- bind $ floor' pixelHeight
  a <- bind $ mod' h 2
  x <- bind $ (x_ a + y_ a) / 2
  opacity <- bind $ 1 - (1 - fadeFrac) * x
  pure $ vec4 (0, 0, 0, opacity)
