{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Drone entities
module Client.Visuals.Shaders.Drone (
  loadDrone,
  drawDrone,
  DroneInstance,
  fromDroneRendering,
  fromDroneDeathParticle,
) where

import Prelude hiding (atan2, length)

import Data.Kind (Type)
import Foreign
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float3, Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.State.Store.Combat (recentDamageDecayFactorPerTick)
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Theme
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.MovementTrails
import Client.Visuals.SharedVisuals
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type DroneInstance :: GLKind -> Type
data DroneInstance k = DroneInstance
  { di_modelScale :: GLData k Float
  , di_modelTrans :: GLData k Float2
  , di_coreOpacity :: GLData k Float
  , di_color :: GLData k Float4
  , di_damageOpacity :: GLData k Float
  , di_angleOffset :: GLData k Float
  , di_shadowOffsets12 :: GLData k Float4
  , di_shadowOffsets34 :: GLData k Float4
  , di_shadowOffsets56 :: GLData k Float4
  , di_shadowOffsets78 :: GLData k Float4
  , di_shadowOffsets910 :: GLData k Float4
  , di_ridgesScale :: GLData k Float
  , di_dashChargeFullSeconds :: GLData k Float
  -- ^ A value less than 0 means that the dash is on cooldown
  , di_dashEffect :: GLData k Float2
  -- ^ Fields: seconds since dash (float), intensity (float)
  , di_chargeFraction :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (DroneInstance Cpu)
deriving anyclass instance GLAttribute (DroneInstance Cpu)
deriving anyclass instance GLSLInput (DroneInstance Gpu)

loadDrone ::
  TimedFastLogger -> GL (StandardInstancedVisual DroneInstance)
loadDrone logger =
  loadStandardVisual logger vertices compiledShaders C.maxNumEntities

drawDrone ::
  StandardInstancedVisual DroneInstance ->
  StandardVParams DroneInstance ->
  GL ()
drawDrone = drawStandardVisual

--------------------------------------------------------------------------------

fromDroneRendering ::
  Maybe (ActorID, Float) ->
  Theme ->
  TeamView ->
  Time ->
  RenderingComponent DroneRendering ->
  DroneInstance Cpu
fromDroneRendering mViewPlayerDashReady theme teamView time rc =
  DroneInstance
    { di_modelScale = rendering_radius rc
    , di_modelTrans = rendering_pos rc
    , di_coreOpacity = dr_healthFraction dr
    , di_color =
        let color =
              Float.map3 (invincibilityFilter' . darkeningFilter) $
                either decayColor (viewPlayerColor theme mViewPlayer teamView) $
                  dr_color dr
        in  Util.fromRGB_A color 1
    , di_damageOpacity = damageFilterWeight (dr_recentDamage dr)
    , di_angleOffset = droneRotation time $ dr_entityID dr
    , di_shadowOffsets12 =
        Util.fromFloat22
          (dr_prevPos1 dr - rendering_pos rc)
          (dr_prevPos2 dr - rendering_pos rc)
    , di_shadowOffsets34 =
        Util.fromFloat22
          (dr_prevPos3 dr - rendering_pos rc)
          (dr_prevPos4 dr - rendering_pos rc)
    , di_shadowOffsets56 =
        Util.fromFloat22
          (dr_prevPos5 dr - rendering_pos rc)
          (dr_prevPos6 dr - rendering_pos rc)
    , di_shadowOffsets78 =
        Util.fromFloat22
          (dr_prevPos7 dr - rendering_pos rc)
          (dr_prevPos8 dr - rendering_pos rc)
    , di_shadowOffsets910 =
        Util.fromFloat22
          (dr_prevPos9 dr - rendering_pos rc)
          (dr_prevPos10 dr - rendering_pos rc)
    , di_ridgesScale = droneRidgesScale (dr_class dr)
    , di_dashChargeFullSeconds =
        case mViewPlayerDashReady of
          Nothing -> noDashReadySeconds
          Just (viewActorID, dashReadySeconds) ->
            if Just viewActorID /= dr_actorID dr
              then noDashReadySeconds
              else dashReadySeconds
    , di_dashEffect =
        case dr_ticksSinceDash dr of
          Nothing ->
            noSecondsSinceDash
          Just (ticksSinceDash, intensity) ->
            let secondsSinceDash =
                  fromIntegral ticksSinceDash / fromIntegral C.tickRate_hz
            in  Float2 secondsSinceDash intensity
    , di_chargeFraction = dr_chargeFraction dr
    }
  where
    mViewPlayer = fmap fst mViewPlayerDashReady

    dr :: DroneRendering
    dr = rendering_entityRendering rc

    decayColor :: Team -> Float3
    decayColor team =
      Float.map3 (\x -> 0.45 * x + 0.15) $
        teamDefaultColor theme $
          viewTeamColor teamView team

    invincibilityFilter' :: Float -> Float
    invincibilityFilter' =
      if dr_isInvulnerable dr
        then invulnerabilityFilter
        else id

fromDroneDeathParticle ::
  Time -> Particle DroneDeathParticle -> DroneInstance Cpu
fromDroneDeathParticle time particle =
  DroneInstance
    { di_modelScale = Fixed.toFloat ddp_scale
    , di_modelTrans =
        Fixed.toFloats $
          ddp_pos
            + Fixed.map (* (0.5 * fromIntegral ticksSinceDeath)) ddp_vel
    , di_coreOpacity = 0.0
    , di_color = Float4 1 1 1 opacity
    , di_damageOpacity =
        damageFilterWeight $
          Fixed.toFloat ddp_recentDamageAtDeath
            * exp (-fromIntegral ticksSinceDeath * recentDamageDecayFactorPerTick)
    , di_angleOffset = droneRotation time ddp_eid
    , di_shadowOffsets12 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets34 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets56 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets78 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_shadowOffsets910 = Util.fromFloat22 (Float2 0 0) (Float2 0 0) -- no shadow
    , di_ridgesScale = droneRidgesScale ddp_class
    , di_dashChargeFullSeconds = 0
    , di_dashEffect = noSecondsSinceDash
    , di_chargeFraction = 0
    }
  where
    DroneDeathParticle{..} = particle_type particle
    ticksSinceDeath =
      getTicks $ diffTime time (particle_startTime particle)
    opacity =
      -- Not following decay rate of damage flash.
      0.7071 ** max 0 (fromIntegral ticksSinceDeath - 1)

-- Darken drone colours to make overseers stand out compared to drones.
darkeningFilter :: (Floating a) => a -> a
darkeningFilter x = let w = 0.2 in (1 - w) * x

-- Have drones rotate slowly over time. Vary drones by their `EntityID`
-- (arbitrary choice).
droneRotation :: Time -> Int -> Float
droneRotation time eid =
  let ticks = getTicks $ time `diffTime` initialTime
      eidPart = (* 0.125) $ fromIntegral $ eid .&. 127
      timePart = if even eid then t else -t
        where
          t = (2 * pi / 512 / 6 *) $ fromIntegral $ ticks .&. 511
  in  eidPart + timePart

droneRidgesScale :: PlayerClass -> Float
droneRidgesScale = \case Zealot -> 1; Templar -> 0

noDashReadySeconds :: Float
noDashReadySeconds = -1

noSecondsSinceDash :: Float2
noSecondsSinceDash = Float2 10 0

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ square 1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , invScale :: Expr Float
  , coreOpacity :: Expr Float
  , color :: Expr Float4
  , damageOpacity :: Expr Float
  , angleOffset :: Expr Float
  , shadowOffsets12 :: Expr Float4
  , shadowOffsets34 :: Expr Float4
  , shadowOffsets56 :: Expr Float4
  , shadowOffsets78 :: Expr Float4
  , shadowOffsets910 :: Expr Float4
  , ridgesScale :: Expr Float
  , dashReadySeconds :: Expr Float
  , dashEffect :: Expr Float2
  , chargeFraction :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  DroneInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Position{position}
  DroneInstance{..}
  StandardUniforms{..} = do
    lastShadowOffset <- bind $ zw_ di_shadowOffsets910 -- needs updating if more shadows are added
    offsetLength <- bind $ length lastShadowOffset
    lineRadius <- bind $ di_modelScale * (1.2 + 0.3 * di_chargeFraction) -- need at least 1.2 for movement trail, more for bloom
    boxRadiusX <- bind $ lineRadius + 0.5 * offsetLength
    boxRadiusY <- bind $ lineRadius
    scaledTranslatedVertex <-
      bind $
        vec2
          ( boxRadiusX * x_ position + 0.5 * offsetLength
          , boxRadiusY * y_ position
          )

    offsetDir <-
      bind $
        ifThenElse'
          (offsetLength <# 0.01)
          (vec2 (1, 0))
          (normalize lastShadowOffset)
    rotatedVertex <-
      bind $
        vec2
          ( x_ offsetDir * x_ scaledTranslatedVertex - y_ offsetDir * y_ scaledTranslatedVertex
          , y_ offsetDir * x_ scaledTranslatedVertex + x_ offsetDir * y_ scaledTranslatedVertex
          )

    let screenPos =
          su_viewScale
            * (rotatedVertex + di_modelTrans - su_viewCenter)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { pos = rotatedVertex ./ di_modelScale
            , invScale = recip di_modelScale
            , coreOpacity = di_coreOpacity
            , color = di_color
            , damageOpacity = di_damageOpacity
            , angleOffset = di_angleOffset
            , shadowOffsets12 = di_shadowOffsets12 ./ di_modelScale
            , shadowOffsets34 = di_shadowOffsets34 ./ di_modelScale
            , shadowOffsets56 = di_shadowOffsets56 ./ di_modelScale
            , shadowOffsets78 = di_shadowOffsets78 ./ di_modelScale
            , shadowOffsets910 = di_shadowOffsets910 ./ di_modelScale
            , ridgesScale = di_ridgesScale
            , dashReadySeconds = di_dashChargeFullSeconds
            , dashEffect = di_dashEffect
            , chargeFraction = di_chargeFraction
            }
    pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader StandardUniforms{} PipelineVars{..} = do
  -- Body boundaries
  bodyHexRadius <- bind $ hexRadius ridgesScale pos angleOffset
  bodyFuzziness <- bind $ invScale * 0.5
  let bodyMask bound = smoothstepCentered bound bodyFuzziness bodyHexRadius
      outer = 1.00
      mid = 0.85
      inner = 0.50
  bodyOuterMask <- bind $ bodyMask outer
  bodyMidMask <- bind $ bodyMask mid
  bodyInnerMask <- bind $ bodyMask inner

  -- Body regions
  shellMask <- bind $ (1 - bodyOuterMask) * bodyMidMask
  coreMask <- bind $ (1 - bodyMidMask) * bodyInnerMask

  -- Base color
  dashWeight <- do
    secondsSinceDash <- bind $ x_ dashEffect
    dashIntensity <- bind $ y_ dashEffect
    bind $ max' 0 $ min' 1 $ dashIntensity * exp (-secondsSinceDash / 0.1)
  baseColor <-
    bind $
      1
        - ( (1 -. xyz_ color .* (1 + 0.12 * dashWeight))
              .* (1 - 0.2 * dashWeight)
          )

  -- Bloom
  bloomMask <- do
    bloomRadius <-
      bind $ max' 0 $ max' (bodyHexRadius - outer) (inner - bodyHexRadius)
    scaledRadius <- bind $ bloomRadius / (0.1 + 1.5 * chargeFraction)
    let coreMin = 0.5
    baseBrightness <- bind $ 0.55 * (coreMin + (1 - coreMin) * coreOpacity)
    brightness <- bind $ baseBrightness * min' 1 (chargeFraction / 0.25)
    bind $ brightness * exp' (-40 * scaledRadius * scaledRadius)

  -- Unpack
  shadowOffset1 <- bind $ xy_ shadowOffsets12
  shadowOffset2 <- bind $ zw_ shadowOffsets12
  shadowOffset3 <- bind $ xy_ shadowOffsets34
  shadowOffset4 <- bind $ zw_ shadowOffsets34
  shadowOffset5 <- bind $ xy_ shadowOffsets56
  shadowOffset6 <- bind $ zw_ shadowOffsets56
  shadowOffset7 <- bind $ xy_ shadowOffsets78
  shadowOffset8 <- bind $ zw_ shadowOffsets78
  shadowOffset9 <- bind $ xy_ shadowOffsets910
  shadowOffset10 <- bind $ zw_ shadowOffsets910

  -- Shadows
  let coreMin = 0.25
  shadowBaseOpacity <- bind $ coreMin + (1 - coreMin) * coreOpacity

  shadowFuzziness <- bind $ invScale * 2.0
  let shadowMask posOffset sizeScaling =
        smoothstepCentered sizeScaling shadowFuzziness $
          hexRadius ridgesScale (pos - posOffset) angleOffset
  shadow1Mask <- bind $ shadowMask shadowOffset1 radiusFactor1
  shadow2Mask <- bind $ shadowMask shadowOffset2 radiusFactor2
  shadow3Mask <- bind $ shadowMask shadowOffset3 radiusFactor3
  shadow4Mask <- bind $ shadowMask shadowOffset4 radiusFactor4
  shadow5Mask <- bind $ shadowMask shadowOffset5 radiusFactor5
  shadow6Mask <- bind $ shadowMask shadowOffset6 radiusFactor6
  shadow7Mask <- bind $ shadowMask shadowOffset7 radiusFactor7
  shadow8Mask <- bind $ shadowMask shadowOffset8 radiusFactor8
  shadow9Mask <- bind $ shadowMask shadowOffset9 radiusFactor9
  shadow10Mask <- bind $ shadowMask shadowOffset10 radiusFactor10

  shadow1Opacity <- bind $ opacityFactor1 * (1 - shadow1Mask) * shadowBaseOpacity
  shadow2Opacity <- bind $ opacityFactor2 * (1 - shadow2Mask) * shadowBaseOpacity
  shadow3Opacity <- bind $ opacityFactor3 * (1 - shadow3Mask) * shadowBaseOpacity
  shadow4Opacity <- bind $ opacityFactor4 * (1 - shadow4Mask) * shadowBaseOpacity
  shadow5Opacity <- bind $ opacityFactor5 * (1 - shadow5Mask) * shadowBaseOpacity
  shadow6Opacity <- bind $ opacityFactor6 * (1 - shadow6Mask) * shadowBaseOpacity
  shadow7Opacity <- bind $ opacityFactor7 * (1 - shadow7Mask) * shadowBaseOpacity
  shadow8Opacity <- bind $ opacityFactor8 * (1 - shadow8Mask) * shadowBaseOpacity
  shadow9Opacity <- bind $ opacityFactor9 * (1 - shadow9Mask) * shadowBaseOpacity
  shadow10Opacity <- bind $ opacityFactor10 * (1 - shadow10Mask) * shadowBaseOpacity
  shadowOpacity <-
    bind $
      max'
        ( max'
            ( max'
                (max' shadow1Opacity shadow2Opacity)
                (max' shadow3Opacity shadow4Opacity)
            )
            ( max'
                (max' shadow5Opacity shadow6Opacity)
                (max' shadow7Opacity shadow8Opacity)
            )
        )
        (max' shadow9Opacity shadow10Opacity)

  -- Cooldown ready flash
  cooldownWeight <- do
    progress <- bind $ 6 * dashReadySeconds

    let flashWidth = 0.5
    moveRadius <- bind $ 1 + flashWidth
    flashCenter <- bind $ -moveRadius + progress * 2 * moveRadius

    s <- bind $ let sqrt2 = 0.7071067811865475 in sqrt2 * (x_ pos + y_ pos)
    bind $
      step (flashCenter - flashWidth) s
        * (1 - step (flashCenter + flashWidth) s)

  -- Color composition (with manual blending for the core)
  let whiteFilter weight vec = weight +. (1 - weight) *. vec
  shellColor <-
    bind $
      whiteFilter (0.5 * cooldownWeight) $
        whiteFilter damageOpacity baseColor
  coreColor <-
    bind $
      whiteFilter (0.75 * 0.5 * cooldownWeight) $
        whiteFilter (0.75 * damageOpacity) $
          coreOpacity *. baseColor
  colorOut <-
    bind $
      shellMask *. shellColor
        + (1 - bodyMidMask) *. coreColor
        + bodyOuterMask *. baseColor

  -- Opacity composition (with manual blending for the core)
  opacityOut <-
    bind $
      w_ color
        * ( bodyOuterMask * max' bloomMask shadowOpacity
              + shellMask
              + coreMask * whiteFilter damageOpacity coreOpacity
              + (1 - bodyInnerMask) * bloomMask * coreOpacity
          )

  -- Output
  pure $ vec4 (colorOut, opacityOut)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)

hexRadius :: Expr Float -> Expr Float2 -> Expr Float -> Expr Float
hexRadius ridgesScale offset angleOffset =
  let radius = length offset
      angle' = atan2 (y_ offset) (x_ offset)
      finalAngle = angle' + angleOffset
  in  radius * hex ridgesScale finalAngle

-- We set the mean radius is slightly larger than one so that drones that are
-- touching will actually appear to be touching.
hex :: Expr Float -> Expr Float -> Expr Float
hex ridgesScale angle' = 1.01 + ridgesScale * 0.032 * sin (6 * angle')
