{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Effect for the psi-storm ability
module Client.Visuals.Shaders.PsiStorm (
  loadPsiStorm,
  drawPsiStorm,
  PsiStormInstance (..),
  psiStormWaves,
) where

import Prelude hiding (atan2, length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type PsiStormInstance :: GLKind -> Type
data PsiStormInstance k = PsiStormInstance
  { psi_modelScale :: GLData k Float
  , psi_modelTrans :: GLData k Float2
  , psi_elapsedSeconds :: GLData k Float
  , psi_opacityFactor :: GLData k Float
  , psi_stormColor :: GLData k Float3
  , psi_boundaryOpacity :: GLData k Float
  , psi_fillOpacity :: GLData k Float
  , psi_bloomColor :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (PsiStormInstance Cpu)
deriving anyclass instance GLAttribute (PsiStormInstance Cpu)
deriving anyclass instance GLSLInput (PsiStormInstance Gpu)

loadPsiStorm ::
  TimedFastLogger -> GL (StandardInstancedVisual PsiStormInstance)
loadPsiStorm logger =
  loadStandardVisual logger vertices compiledShaders C.maxPsiStorms

drawPsiStorm ::
  StandardInstancedVisual PsiStormInstance ->
  StandardVParams PsiStormInstance ->
  GL ()
drawPsiStorm = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ disk 32 1.00

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { localPos :: Expr Float2
  , globalPos :: Expr Float2
  , radius :: Expr Float
  , elapsedSeconds :: Expr Float
  , opacityFactor :: Expr Float
  , stormColor :: Expr Float3
  , boundaryOpacity :: Expr Float
  , fillOpacity :: Expr Float
  , bloomColor :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  PsiStormInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} PsiStormInstance{..} StandardUniforms{..} = do
  let bloomWidth = 5 -- adjusted by hand
  localPos <- bind $ (psi_modelScale + bloomWidth) *. position
  globalPos <- bind $ localPos + psi_modelTrans
  pure $
    let screenPos =
          su_viewScale
            * (globalPos - su_viewCenter)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { localPos = localPos
            , globalPos = globalPos
            , radius = psi_modelScale
            , elapsedSeconds = psi_elapsedSeconds
            , opacityFactor = psi_opacityFactor
            , stormColor = psi_stormColor
            , boundaryOpacity = psi_boundaryOpacity
            , fillOpacity = psi_fillOpacity
            , bloomColor = psi_bloomColor
            }
    in  (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  waves <- psiStormWaves elapsedSeconds globalPos

  r <- bind $ length localPos

  (outerMask, stormBoundary) <- do
    outerMask <- bind $ smoothstep (-0.5) 0.5 (r - radius)

    s <- bind $ max' 0 (r - (radius - 120)) / 120
    s2 <- bind $ s * s
    s4 <- bind $ s2 * s2
    s8 <- bind $ s4 * s4
    s16 <- bind $ s8 * s8
    let boundary = s16

    pure (outerMask, boundary)

  storm <- do
    stormOpacity <-
      bind $ boundaryOpacity * stormBoundary + fillOpacity * (1 + waves)
    bind $ vec4 (stormColor, stormOpacity)

  bloom <- do
    s <- bind $ 0.33 * (r - radius)
    opacity <- bind $ 0.4 * exp (-s * s)
    bind $ vec4 (xyz_ bloomColor, w_ bloomColor * opacity)

  whole <- bind $ outerMask *. bloom + (1 - outerMask) *. storm
  pure $ vec4 (xyz_ whole, opacityFactor * w_ whole)

psiStormWaves :: Expr Float -> Expr Float2 -> GLSL (Expr Float)
psiStormWaves timeSeconds pos = do
  t <- bind $ 3 * timeSeconds + 0.025 * sin (10 * timeSeconds)
  let freq = 0.014
  bind $
    ( 0
        + sin (freq * 1.00 * dot' pos (vec2 (1, 0)) + 2.02 * t)
        + sin (freq * 1.02 * dot' pos (vec2 (0.65, -0.73)) + 2.47 * t)
        + sin (freq * 1.04 * dot' pos (vec2 (-0.56, -0.85)) + 1.54 * t)
        + 0.5 * sin (freq * 2.00 * dot' pos (vec2 (0.1, 0.9)) - 2.53 * t)
        + 0.5 * sin (freq * 2.04 * dot' pos (vec2 (0.82, 0.62)) - 1.46 * t)
        + 0.5 * sin (freq * 2.08 * dot' pos (vec2 (0.78, -0.58)) - 1.98 * t)
        + 0.25 * sin (freq * 4.00 * dot' pos (vec2 (0.5, 0.866)) + 2.23 * t)
        + 0.25 * sin (freq * 4.08 * dot' pos (vec2 (-0.917, 0.4)) + 1.77 * t)
        + 0.25 * sin (freq * 4.16 * dot' pos (vec2 (0.707, -0.707)) + 1.37 * t)
    )
      / 5.25
