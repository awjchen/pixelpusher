{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The large circle around your overseer that shows the range of your
-- commands
module Client.Visuals.Shaders.DroneControlRadius (
  loadDroneControlRadius,
  drawDroneControlRadius,
  DroneControlRadiusVisual,
  DroneControlRadiusVParams (..),
) where

import Prelude
import Prelude qualified as P

import Data.Kind (Type)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2 (Float2), Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data DroneControlRadiusVisual = DroneControlRadiusVisual
  { dcrv_vao :: VertexArray
  , dcrv_shaderProg :: ShaderProgram
  , dcrv_nVertices :: GLsizei
  , dcrv_uniformLocations :: UniformLocations (DroneControlRadiusVParams Cpu)
  }

type DroneControlRadiusVParams :: GLKind -> Type
data DroneControlRadiusVParams k = DroneControlRadiusVParams
  { dcrvp_aspectRatio :: GLData k Float
  , dcrvp_modelTrans :: GLData k Float2
  , dcrvp_modelScale :: GLData k Float
  , dcrvp_viewCenter :: GLData k Float2
  , dcrvp_viewScale :: GLData k Float
  , dcrvp_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (DroneControlRadiusVParams Cpu)
deriving anyclass instance GLUniform (DroneControlRadiusVParams Cpu)
deriving anyclass instance GLSLInput (DroneControlRadiusVParams Gpu)

--------------------------------------------------------------------------------

drawDroneControlRadius ::
  DroneControlRadiusVisual -> DroneControlRadiusVParams Cpu -> GL ()
drawDroneControlRadius dcrv params =
  withVertexArray (dcrv_vao dcrv) $
    withShaderProgram (dcrv_shaderProg dcrv) $ do
      writeUniforms (dcrv_uniformLocations dcrv) params
      glDrawArrays GL_LINE_LOOP 0 (dcrv_nVertices dcrv)

loadDroneControlRadius :: TimedFastLogger -> GL DroneControlRadiusVisual
loadDroneControlRadius logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    DroneControlRadiusVisual
      { dcrv_vao = vao
      , dcrv_shaderProg = shaderProg
      , dcrv_nVertices = fromIntegral $ P.length vertices
      , dcrv_uniformLocations = uniforms
      }

vertices :: [Position Cpu]
vertices =
  map Position $
    let n = 128 :: Int
        angles =
          map (\i -> 2 * pi * fromIntegral i / fromIntegral n) [1 .. n]
    in  map (\ang -> Float2 (cos ang) (sin ang)) angles

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  GLUnit Gpu ->
  DroneControlRadiusVParams Gpu ->
  GLSL (Expr Float2, GLUnit Gpu)
vertShader Position{position} GLUnit DroneControlRadiusVParams{..} =
  pure $
    let screenPos =
          dcrvp_viewScale
            *. (dcrvp_modelTrans - dcrvp_viewCenter + dcrvp_modelScale *. position)
            / vec2 (dcrvp_aspectRatio, 1)
    in  (screenPos, GLUnit)

fragShader :: DroneControlRadiusVParams Gpu -> GLUnit Gpu -> GLSL (Expr Float4)
fragShader DroneControlRadiusVParams{dcrvp_color} GLUnit = pure dcrvp_color
