{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | An expanding circle
module Client.Visuals.Shaders.Sparkle (
  loadSparkle,
  drawSparkle,
  SparkleInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Constants qualified as CC
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type SparkleInstance :: GLKind -> Type
data SparkleInstance k = SparkleInstance
  { si_size :: GLData k Float
  , si_center :: GLData k Float2
  , si_color :: GLData k Float4
  , si_bloomBrightness :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (SparkleInstance Cpu)
deriving anyclass instance GLAttribute (SparkleInstance Cpu)
deriving anyclass instance GLSLInput (SparkleInstance Gpu)

loadSparkle :: TimedFastLogger -> GL (StandardInstancedVisual SparkleInstance)
loadSparkle logger =
  loadStandardVisual logger vertices compiledShaders CC.maxVisiblePings

drawSparkle ::
  StandardInstancedVisual SparkleInstance ->
  StandardVParams SparkleInstance ->
  GL ()
drawSparkle = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.5

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  SparkleInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} SparkleInstance{..} StandardUniforms{..} =
  let screenPos =
        su_viewScale
          * (si_center - su_viewCenter + si_size *. position)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { size = si_size
          , pos = position
          , color = si_color
          , bloomBrightness = si_bloomBrightness
          }
  in  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { size :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , bloomBrightness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  v <- do
    let w' = 20
    w <- bind $ recip w'
    x' <- bind $ max' 0 $ abs (x_ pos * w') + w
    y' <- bind $ max' 0 $ abs (y_ pos * w') + w
    bind $ smoothstep (-1) 1 (1 / (x' * y') - 1)

  bloom <- do
    r2 <- bind $ x_ pos * x_ pos + y_ pos * y_ pos
    bind $ 0.25 * bloomBrightness * exp (-2 * r2)

  opacity <- bind $ max' v bloom
  pure $ vec4 (xyz_ color, w_ color * opacity)
