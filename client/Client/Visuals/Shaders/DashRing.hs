{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Client.Visuals.Shaders.DashRing (
  loadDashRing,
  drawDashRing,
  DashRingInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type DashRingInstance :: GLKind -> Type
data DashRingInstance k = DashRingInstance
  { dri_center :: GLData k Float2
  , dri_radius :: GLData k Float
  , dri_initialVelocity :: GLData k Float2
  , dri_direction :: GLData k Float2
  , dri_ticksElapsed :: GLData k Float
  , dri_secondsElapsed :: GLData k Float
  , dri_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (DashRingInstance Cpu)
deriving anyclass instance GLAttribute (DashRingInstance Cpu)
deriving anyclass instance GLSLInput (DashRingInstance Gpu)

loadDashRing :: TimedFastLogger -> GL (StandardInstancedVisual DashRingInstance)
loadDashRing logger =
  loadStandardVisual logger vertices compiledShaders C.maxNumEntities

drawDashRing ::
  StandardInstancedVisual DashRingInstance ->
  StandardVParams DashRingInstance ->
  GL ()
drawDashRing = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.01

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , secondsElapsed :: Expr Float
  , baseColor :: Expr Float4
  , radius :: Expr Float
  , fuzziness :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  DashRingInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} DashRingInstance{..} StandardUniforms{..} = do
  fuzziness <- do
    let fuzzSqPerSecond = 36 * 36
    bind $ sqrt $ 1 + fuzzSqPerSecond * dri_secondsElapsed
  radius <- bind $ dri_secondsElapsed * 180
  sizedPos <- bind $ position .* (radius + fuzziness)
  screenPos <-
    bind $
      su_viewScale
        * (sizedPos + dri_center - su_viewCenter)
        / vec2 (su_aspectRatio, 1)

  let pipelineVars =
        PipelineVars
          { pos = sizedPos
          , secondsElapsed = dri_secondsElapsed
          , baseColor = dri_color
          , radius = radius
          , fuzziness = fuzziness
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  sdf <- bind $ r - radius
  mask <-
    bind $ (1 - smoothstep 0 fuzziness sdf) * smoothstep (-fuzziness) 0 sdf
  fadeIn <- bind $ min' 1 $ secondsElapsed / 0.05
  fadeOut <- bind $ exp (-secondsElapsed / 0.03)
  opacity <- bind $ fadeIn * fadeOut * mask
  pure $ vec4 (xyz_ baseColor, w_ baseColor * opacity)
