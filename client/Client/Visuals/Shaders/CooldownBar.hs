{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The cooldown bars beneath your overseer
module Client.Visuals.Shaders.CooldownBar (
  loadCooldownBar,
  drawCooldownBar,
  CooldownBarInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CooldownBarInstance :: GLKind -> Type
data CooldownBarInstance k = CooldownBarInstance
  { cbi_center :: GLData k Float2
  , cbi_widthRadius :: GLData k Float
  , cbi_heightRadius :: GLData k Float
  , cbi_bloomRadius :: GLData k Float
  , cbi_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CooldownBarInstance Cpu)
deriving anyclass instance GLAttribute (CooldownBarInstance Cpu)
deriving anyclass instance GLSLInput (CooldownBarInstance Gpu)

loadCooldownBar ::
  TimedFastLogger -> GL (StandardInstancedVisual CooldownBarInstance)
loadCooldownBar logger =
  loadStandardVisual logger vertices compiledShaders C.maxNumEntities

drawCooldownBar ::
  StandardInstancedVisual CooldownBarInstance ->
  StandardVParams CooldownBarInstance ->
  GL ()
drawCooldownBar = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ square 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , widthRadius :: Expr Float
  , heightRadius :: Expr Float
  , bloomRadius :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CooldownBarInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CooldownBarInstance{..} StandardUniforms{..} = do
  bloomRadiusExt <- bind $ cbi_bloomRadius * 2
  x_radius <- bind $ bloomRadiusExt + cbi_widthRadius
  y_radius <- bind $ bloomRadiusExt + cbi_heightRadius
  localPosition <-
    bind $ vec2 (x_ position * x_radius, y_ position * y_radius)
  screenPos <-
    bind $
      su_viewScale
        * (localPosition + cbi_center - su_viewCenter)
        / vec2 (su_aspectRatio, 1)

  let pipelineVars =
        PipelineVars
          { pos = localPosition
          , widthRadius = cbi_widthRadius
          , heightRadius = cbi_heightRadius
          , bloomRadius = cbi_bloomRadius
          , color = cbi_color
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  dx <- bind $ max' 0 $ abs (x_ pos) - widthRadius + bloomRadius -- to make the zero-width bar look empty
  dy <- bind $ max' 0 $ abs (y_ pos) - heightRadius
  r2 <- bind $ dx * dx + dy * dy
  opacity <- bind $ exp $ negate $ 0.5 * r2 / (bloomRadius * bloomRadius)
  pure $ vec4 (xyz_ color, w_ color * opacity)
