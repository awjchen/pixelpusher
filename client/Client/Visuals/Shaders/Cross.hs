{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | 'X' shape UI element
module Client.Visuals.Shaders.Cross (
  loadCross,
  drawCross,
  CrossInstance (..),
) where

import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics

import Pixelpusher.Custom.Float (Float2, Float4)
import System.Log.FastLogger

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CrossInstance :: GLKind -> Type
data CrossInstance k = CrossInstance
  { ci_size :: GLData k Float
  , ci_center :: GLData k Float2
  , ci_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CrossInstance Cpu)
deriving anyclass instance GLAttribute (CrossInstance Cpu)
deriving anyclass instance GLSLInput (CrossInstance Gpu)

loadCross :: TimedFastLogger -> GL (StandardInstancedVisual CrossInstance)
loadCross logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawCross ::
  StandardInstancedVisual CrossInstance ->
  StandardVParams CrossInstance ->
  GL ()
drawCross = drawStandardVisual

maxInstances :: Word16
maxInstances = 16

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , size :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CrossInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CrossInstance{..} StandardUniforms{..} = do
  pos <- bind $ ci_size *. position
  rotatedPos <-
    bind $ -- rotate 45 degrees
      0.70710678 * vec2 (x_ pos - y_ pos, x_ pos + y_ pos)
  screenPos <-
    bind $
      su_viewScale
        * (ci_center - su_viewCenter + rotatedPos)
        / vec2 (su_aspectRatio, 1)
  let pipelineVars =
        PipelineVars
          { pos = pos
          , size = ci_size
          , color = ci_color
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r0 <- bind $ 0.15 * size
  r1 <- bind $ 0.90 * size

  px <- bind $ abs $ x_ pos
  py <- bind $ abs $ y_ pos

  d <- bind $ min' (max' (px - r1) (py - r0)) (max' (px - r0) (py - r1))

  opacity <- bind $ 1.0 - smoothstep (-0.5) 0.5 d
  pure $ vec4 (xyz_ color, w_ color * opacity)
