{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Team bases for capture the flag
module Client.Visuals.Shaders.TeamBase (
  loadTeamBase,
  drawTeamBase,
  TeamBaseInstance (..),
) where

import Prelude hiding (length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type TeamBaseInstance :: GLKind -> Type
data TeamBaseInstance k = TeamBaseInstance
  { tbi_size :: GLData k Float
  , tbi_center :: GLData k Float2
  , tbi_color :: GLData k Float4
  , tbi_fillOpacity :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (TeamBaseInstance Cpu)
deriving anyclass instance GLAttribute (TeamBaseInstance Cpu)
deriving anyclass instance GLSLInput (TeamBaseInstance Gpu)

--------------------------------------------------------------------------------

loadTeamBase ::
  TimedFastLogger -> GL (StandardInstancedVisual TeamBaseInstance)
loadTeamBase logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawTeamBase ::
  StandardInstancedVisual TeamBaseInstance ->
  StandardVParams TeamBaseInstance ->
  GL ()
drawTeamBase = drawStandardVisual

maxInstances :: Word16
maxInstances = 4

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , fillOpacity :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  TeamBaseInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} TeamBaseInstance{..} StandardUniforms{..} = do
  pos <- bind $ tbi_size *. position
  screenPos <-
    bind $
      su_viewScale
        * (tbi_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let pipelineVars =
        PipelineVars
          { radius = tbi_size
          , pos = pos
          , color = tbi_color
          , fillOpacity = tbi_fillOpacity
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ r - radius

  fill <- bind $ fillOpacity * 0.25 * (1 - smoothstep (-0.5) 0.5 (r - radius))

  line <- bind $ 1 - smoothstep 0 1 (abs dr)
  lineBloom <- bind $ 0.6 * exp' (-0.2 * dr * dr)

  opacity <- bind $ max' fill $ max' line lineBloom
  pure $ vec4 (xyz_ color, opacity * w_ color)
