{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Flag entities (for capture the flag)
module Client.Visuals.Shaders.Flag (
  loadFlag,
  drawFlag,
  FlagInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type FlagInstance :: GLKind -> Type
data FlagInstance k = FlagInstance
  { fi_size :: GLData k Float
  , fi_center :: GLData k Float2
  , fi_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (FlagInstance Cpu)
deriving anyclass instance GLAttribute (FlagInstance Cpu)
deriving anyclass instance GLSLInput (FlagInstance Gpu)

loadFlag :: TimedFastLogger -> GL (StandardInstancedVisual FlagInstance)
loadFlag logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawFlag ::
  StandardInstancedVisual FlagInstance ->
  StandardVParams FlagInstance ->
  GL ()
drawFlag = drawStandardVisual

maxInstances :: Word16
maxInstances =
  2 -- 2 teams
    * ( 1 -- one flag per team
          + 2 -- flag fade in/out effects
          + 1 -- accidentally rendering two flags during flag transitions?
          + 2 -- safety
      )
    + fromIntegral C.maxActorsPerGame

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 2.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  FlagInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Position{position}
  FlagInstance{..}
  StandardUniforms{..} = do
    pos <- bind $ fi_size *. position
    screenPos <-
      bind $
        su_viewScale
          * (fi_center + pos - su_viewCenter)
          / vec2 (su_aspectRatio, 1)
    let radius = fi_size
        color = fi_color
    pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos

  mask <- bind $ smoothstep (-0.5) 0.5 (r - radius)
  s <- bind $ 0.25 * (r - radius)
  bloom <- bind $ 0.4 * exp (-s * s)

  baseOpacity <- bind $ mask * bloom + (1 - mask)
  baseColor <- bind $ vec4 (xyz_ color, baseOpacity * w_ color)

  t <- bind $ r / radius
  t2 <- bind $ t * t
  t4 <- bind $ t2 * t2
  shineMask <- bind $ 0.3 * (1 - mask) * min' 1 t4
  shine <- bind $ vec4 (shineMask, shineMask, shineMask, 0)

  pure $ baseColor + shine
