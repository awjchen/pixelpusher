{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The fuzzy blob of colour that is an overseer's corpse
module Client.Visuals.Shaders.OverseerRemains (
  loadOverseerRemains,
  drawOverseerRemains,
  OverseerRemainsInstance,
  fromOverseerRemainsParticle,
) where

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Time

import Client.Theme
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type OverseerRemainsInstance :: GLKind -> Type
data OverseerRemainsInstance k = OverseerRemainsInstance
  { ori_modelScale :: GLData k Float
  , ori_modelTrans :: GLData k Float2
  , ori_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (OverseerRemainsInstance Cpu)
deriving anyclass instance GLAttribute (OverseerRemainsInstance Cpu)
deriving anyclass instance GLSLInput (OverseerRemainsInstance Gpu)

loadOverseerRemains ::
  TimedFastLogger -> GL (StandardInstancedVisual OverseerRemainsInstance)
loadOverseerRemains logger =
  loadStandardVisual logger vertices compiledShaders maxInstances
  where
    maxInstances = fromIntegral C.maxActorsPerGame

drawOverseerRemains ::
  StandardInstancedVisual OverseerRemainsInstance ->
  StandardVParams OverseerRemainsInstance ->
  GL ()
drawOverseerRemains = drawStandardVisual

--------------------------------------------------------------------------------

fromOverseerRemainsParticle ::
  Maybe ActorID ->
  Theme ->
  TeamView ->
  Time ->
  Particle OverseerRemainsParticle ->
  OverseerRemainsInstance Cpu
fromOverseerRemainsParticle mViewPlayer theme teamView time particle =
  let OverseerRemainsParticle{orp_color, orp_pos, orp_scale} =
        particle_type particle
      color =
        normalize1 $ viewPlayerColor theme mViewPlayer teamView orp_color
      t = getTicks $ time `diffTime` particle_startTime particle
      t_final =
        getTicks $
          particle_endTime particle `diffTime` particle_startTime particle
      t_inflect = 6 -- Ticks
      opacity =
        (* 0.4) $
          if t < t_inflect
            then fromIntegral t / fromIntegral t_inflect
            else
              let x =
                    fromIntegral (t - t_inflect)
                      / fromIntegral (t_final - t_inflect)
                  x2 = x * x
              in  1 - x2 * x2
  in  OverseerRemainsInstance
        { ori_modelScale = Fixed.toFloat $ 2.5 * orp_scale
        , ori_modelTrans = Fixed.toFloats orp_pos
        , ori_color = Util.fromRGB_A color opacity
        }
  where
    normalize1 :: Float3 -> Float3
    normalize1 v =
      let m = maximum $ Float.toList3 v
      in  Float.map3 (/ m) v

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon outerRadius
  where
    outerRadius = 2.5

-- Visually, the brightness of the normal distribution 2.5 standard
-- deviations from its mean is negligible.

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  OverseerRemainsInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} OverseerRemainsInstance{..} StandardUniforms{..} =
  let screenPos =
        su_viewScale
          * (ori_modelTrans - su_viewCenter + ori_modelScale *. position)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { pos = position
          , color = ori_color
          }
  in  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r2 <- bind $ let x = x_ pos; y = y_ pos in x * x + y * y
  opacity <- bind $ exp (-r2)
  pure $ vec4 (xyz_ color, w_ color * opacity)
