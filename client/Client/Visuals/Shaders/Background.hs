{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The pattern in the background
module Client.Visuals.Shaders.Background (
  loadBackground,
  drawBackground,
  BackgroundVisual,
  BackgroundVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Data.Kind (Type)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float3, Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data BackgroundVisual = BackgroundVisual
  { bv_vao :: VertexArray
  , bv_shaderProg :: ShaderProgram
  , bv_nVertices :: GLsizei
  , bv_uniformLocations :: UniformLocations (BackgroundVParams Cpu)
  }

type BackgroundVParams :: GLKind -> Type
data BackgroundVParams k = BackgroundVParams
  { bvp_aspectRatio :: GLData k Float
  , bvp_viewScale :: GLData k Float
  , bvp_viewCenter :: GLData k Float2
  , bvp_modelScale :: GLData k Float
  , bvp_modelTrans :: GLData k Float2
  , bvp_worldRadius :: GLData k Float
  , bvp_backgroundColor :: GLData k Float3
  , bvp_patternColor :: GLData k Float3
  , bvp_scoreSeconds :: GLData k Float
  , bvp_scorePos :: GLData k Float2
  , bvp_scoreColor :: GLData k Float3
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (BackgroundVParams Cpu)
deriving anyclass instance GLUniform (BackgroundVParams Cpu)
deriving anyclass instance GLSLInput (BackgroundVParams Gpu)

drawBackground :: BackgroundVisual -> BackgroundVParams Cpu -> GL ()
drawBackground background params =
  withVertexArray (bv_vao background) $
    withShaderProgram (bv_shaderProg background) $ do
      -- Set uniforms
      writeUniforms (bv_uniformLocations background) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (bv_nVertices background)

loadBackground :: TimedFastLogger -> GL BackgroundVisual
loadBackground logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    BackgroundVisual
      { bv_vao = vao
      , bv_shaderProg = shaderProg
      , bv_nVertices = fromIntegral $ P.length vertices
      , bv_uniformLocations = uniforms
      }

vertices :: [Position Cpu]
vertices = map Position $ square 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {worldPos :: Expr Float2}
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  GLUnit Gpu ->
  BackgroundVParams Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} GLUnit BackgroundVParams{..} = do
  worldPos <- bind $ bvp_modelTrans + bvp_modelScale *. position
  screenPos <-
    bind $
      bvp_viewScale *. (worldPos - bvp_viewCenter) / vec2 (bvp_aspectRatio, 1)
  pure (screenPos, PipelineVars worldPos)

data LoopState = LoopState
  { offset :: Expr Float2
  , min_dist2_edge_outer :: Expr Float
  , min_dist2_edge_inner :: Expr Float
  }

fragShader :: BackgroundVParams Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader BackgroundVParams{..} PipelineVars{worldPos} = do
  -- Pattern
  let n_circles :: Int
      n_circles = 10

  let n_circles' :: Expr Float
      n_circles' = fromIntegral n_circles
  rot_cos <- bind $ cos (2 * pi / n_circles')
  rot_sin <- bind $ sin (2 * pi / n_circles')

  let stroke_radius = 0.667
      stroke_width = 2 * stroke_radius

  let buffer = 40.0
      overlap = bvp_worldRadius / 30
  offset_magnitude <- bind $ (bvp_worldRadius - buffer) * 0.5 - overlap
  radius_outer <- bind $ (bvp_worldRadius - buffer) * 0.5 + overlap
  radius_inner <- bind $ radius_outer - stroke_width
  radius_outer2 <- bind $ radius_outer * radius_outer
  radius_inner2 <- bind $ radius_inner * radius_inner

  world_diameter2 <- bind (4 * bvp_worldRadius * bvp_worldRadius)
  initLoopState <- do
    offset <- bind (vec2 (offset_magnitude, 0.0))
    pure
      LoopState
        { offset = offset
        , min_dist2_edge_outer = world_diameter2
        , min_dist2_edge_inner = world_diameter2
        }

  LoopState{min_dist2_edge_outer, min_dist2_edge_inner} <-
    flip execStateT initLoopState $
      for_ [1 .. n_circles] $ \_ -> do
        LoopState{offset, min_dist2_edge_outer, min_dist2_edge_inner} <- get

        newLoopState <- lift $ do
          dx <- bind $ x_ worldPos - x_ offset
          dy <- bind $ y_ worldPos - y_ offset
          dist2_center <- bind $ dx * dx + dy * dy
          dist2_edge_outer <- bind $ dist2_center - radius_outer2
          dist2_edge_inner <- bind $ dist2_center - radius_inner2

          -- Comparing squares in a weird way to avoid square roots in the
          -- loop, but at the cost of some distortion (negligible for now)
          new_min_dist2_edge_outer <-
            bind $
              if abs dist2_edge_outer <# abs min_dist2_edge_outer
                then dist2_edge_outer
                else min_dist2_edge_outer
          new_min_dist2_edge_inner <-
            bind $
              if abs dist2_edge_inner <# abs min_dist2_edge_inner
                then dist2_edge_inner
                else min_dist2_edge_inner
          new_offset <-
            bind $
              vec2
                ( rot_cos * x_ offset - rot_sin * y_ offset
                , rot_sin * x_ offset + rot_cos * y_ offset
                )
          pure
            LoopState
              { offset = new_offset
              , min_dist2_edge_outer = new_min_dist2_edge_outer
              , min_dist2_edge_inner = new_min_dist2_edge_inner
              }

        put newLoopState

  min_dist_edge_outer <-
    bind $ sqrt (max' 0 (min_dist2_edge_outer + radius_outer2)) - radius_outer
  min_dist_edge_inner <-
    bind $ sqrt (max' 0 (min_dist2_edge_inner + radius_inner2)) - radius_inner

  let fuzziness = 0.5
  fill_stroke <-
    bind $
      smoothstep (-fuzziness) fuzziness (min_dist_edge_outer + stroke_radius)
        - smoothstep (-fuzziness) fuzziness (min_dist_edge_outer - stroke_radius)
  mask_stroke <-
    bind $
      1
        - smoothstep (-fuzziness) fuzziness (min_dist_edge_inner + stroke_radius)
        + smoothstep (-fuzziness) fuzziness (min_dist_edge_inner - stroke_radius)
  pattern_mask <- bind $ min' fill_stroke mask_stroke

  -- Flag capture wave effect
  score_radius <- bind $ length $ worldPos - bvp_scorePos
  let wave_speed = 6000
  fade_time_constant <- bind 0.4
  max_time <- bind $ 1 + fade_time_constant * 6
  wave_radius <- bind $ wave_speed * min' max_time bvp_scoreSeconds
  wave_front_dist <- bind $ max' 0 $ score_radius - wave_radius
  wave_intensity <- do
    let cutoff = 0.05 -- to end the effect faster
    x <-
      bind $
        exp ((score_radius - wave_radius) / wave_speed / fade_time_constant)
    bind $ (x - cutoff) / (1 - cutoff)
  pattern_wave_intensity <-
    bind $
      smoothstep (-fuzziness) fuzziness (wave_radius - score_radius)
        * wave_intensity
  bloom_wave_intensity <- bind $ min' 1 wave_intensity

  -- Bloom
  bloom_mask <- do
    bloom_radius <-
      bind $
        sqrt $
          min_dist_edge_outer * min_dist_edge_outer
            + wave_front_dist * wave_front_dist
    bloom_radius_const <- bind $ 4 * bloom_wave_intensity
    s <- bind $ bloom_radius / bloom_radius_const
    bind $ 0.4 * exp (-s * s)

  -- Blending
  score_color_intensity <-
    bind $
      max'
        (pattern_mask * pattern_wave_intensity)
        (bloom_mask * bloom_wave_intensity)

  normalColor <-
    bind $
      (1 - pattern_mask) *. bvp_backgroundColor
        + pattern_mask *. bvp_patternColor
  colorOut <-
    bind $
      (score_color_intensity *. bvp_scoreColor)
        + (1 - score_color_intensity) *. normalColor

  pure $ vec4 (colorOut, 1.0)
  where
    ifThenElse = ifThenElse'
