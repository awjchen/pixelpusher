{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Overseer entities
module Client.Visuals.Shaders.Overseer (
  loadOverseer,
  drawOverseer,
  OverseerInstance (..),
  OverseerGLInstance,
  OverseerVParams (..),
  fromOverseerRendering,
  fromOverseerDeathParticle,
) where

import Prelude hiding (atan2, length, pi)
import Prelude qualified

import Data.Bits ((.&.))
import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2 (Float2), Float3, Float4 (Float4))
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters.Test
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Time

import Client.Theme
import Client.Visuals.CoordinateSystems (CoordSystem)
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Util (fromFloat12, fromFloat22, fromRGB_A, toRGB_A)
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.MovementTrails
import Client.Visuals.SDF (isoscelesTrapezoid)
import Client.Visuals.SharedVisuals
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

-- External instance type
-- This type exists to allow specification of overseer visuals without a value
-- of type `RenderingComponent OverseerRendering` (e.g. to demonstrate how
-- overseer health is indicated in the help screen).
data OverseerInstance = OverseerInstance
  { oi_radius :: Float
  , oi_pos :: Float2
  , oi_prevPos1 :: Float2
  , oi_prevPos2 :: Float2
  , oi_prevPos3 :: Float2
  , oi_prevPos4 :: Float2
  , oi_prevPos5 :: Float2
  , oi_prevPos6 :: Float2
  , oi_prevPos7 :: Float2
  , oi_prevPos8 :: Float2
  , oi_prevPos9 :: Float2
  , oi_prevPos10 :: Float2
  , oi_healthFraction :: Float
  , oi_shieldsFraction :: Float
  , oi_shieldColor :: Float3
  , oi_color :: Float4
  , oi_recentDamage :: Float
  , oi_isInvulnerable :: Bool
  , oi_class :: PlayerClass
  , oi_angleOffset :: Float
  , oi_psiStormReadySeconds :: Float
  , oi_angleFins :: Float
  , oi_ticksSinceDash :: Maybe Ticks
  }

-- Internal instance type
type OverseerGLInstance :: GLKind -> Type
data OverseerGLInstance k = OverseerGLInstance
  { ogi_modelScaleTrans :: GLData k Float3
  -- ^ modelScale: float, modelTrans: vec2
  , ogi_healthShieldsFractions :: GLData k Float2
  , ogi_color :: GLData k Float4
  , ogi_shieldColor :: GLData k Float3
  , ogi_shadowOffsets12 :: GLData k Float4
  -- ^ offset1: vec2, offset2: vec2
  , ogi_shadowOffsets34 :: GLData k Float4
  -- ^ offset3: vec2, offset4: vec2
  , ogi_shadowOffsets56 :: GLData k Float4
  -- ^ offset5: vec2, offset6: vec2
  , ogi_shadowOffsets78 :: GLData k Float4
  -- ^ offset7: vec2, offset8: vec2
  , ogi_shadowOffsets910 :: GLData k Float4
  -- ^ offset9: vec2, offset10: vec2
  , ogi_ridgesScale :: GLData k Float
  , ogi_finParams :: GLData k Float4
  -- ^ width1: float, width2: float; height: float, curvature: float
  , ogi_angleOffset :: GLData k Float
  , ogi_psiStormReadySeconds :: GLData k Float
  , ogi_angleFins :: GLData k Float
  , ogi_secondsSinceDash :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (OverseerGLInstance Cpu)
deriving anyclass instance GLAttribute (OverseerGLInstance Cpu)
deriving anyclass instance GLSLInput (OverseerGLInstance Gpu)

loadOverseer ::
  TimedFastLogger -> GL (StandardInstancedVisual OverseerGLInstance)
loadOverseer logger =
  loadStandardVisual logger vertices compiledShaders maxInstances
  where
    maxInstances = fromIntegral C.maxActorsPerGame

drawOverseer ::
  StandardInstancedVisual OverseerGLInstance -> OverseerVParams -> GL ()
drawOverseer visual params =
  drawStandardVisual visual $
    StandardVParams
      { svp_aspectRatio = ovp_aspectRatio params
      , svp_coordSystem = ovp_coordSystem params
      , svp_testParams = ovp_testParams params
      , svp_instances = map makeGLInstance $ ovp_instances params
      }

data OverseerVParams = OverseerVParams
  { ovp_aspectRatio :: Float
  , ovp_coordSystem :: CoordSystem
  , ovp_testParams :: TestParams
  , ovp_instances :: [OverseerInstance]
  }

--------------------------------------------------------------------------------

fromOverseerRendering ::
  Time ->
  Theme ->
  TeamView ->
  Maybe ActorID ->
  Float ->
  RenderingComponent OverseerRendering ->
  OverseerInstance
fromOverseerRendering time theme teamView mViewPlayer mouseAngle rc =
  OverseerInstance
    { oi_radius = rendering_radius rc
    , oi_pos = rendering_pos rc
    , oi_prevPos1 = or_prevPos1 ov
    , oi_prevPos2 = or_prevPos2 ov
    , oi_prevPos3 = or_prevPos3 ov
    , oi_prevPos4 = or_prevPos4 ov
    , oi_prevPos5 = or_prevPos5 ov
    , oi_prevPos6 = or_prevPos6 ov
    , oi_prevPos7 = or_prevPos7 ov
    , oi_prevPos8 = or_prevPos8 ov
    , oi_prevPos9 = or_prevPos9 ov
    , oi_prevPos10 = or_prevPos10 ov
    , oi_healthFraction = or_healthFraction ov
    , oi_shieldsFraction = or_shieldsFraction ov
    , oi_shieldColor =
        case viewTeamColor teamView (or_team ov) of
          TeamRed -> theme_teamRed_shieldColor theme
          TeamBlue -> theme_teamBlue_shieldColor theme
    , oi_color =
        fromRGB_A (viewPlayerColor theme mViewPlayer teamView (or_color ov)) 1
    , oi_recentDamage = or_recentDamage ov
    , oi_isInvulnerable = or_isInvulnerable ov
    , oi_class = or_playerClass ov
    , oi_angleOffset = overseerRotation time (or_entityID ov)
    , oi_psiStormReadySeconds = case mViewPlayer of
        Nothing -> noPsiStormReadySeconds
        Just actorID ->
          if actorID /= or_actorID ov
            then noPsiStormReadySeconds
            else
              fromIntegral (time `diffTime` or_psiStormCooldownUntil ov)
                / fromIntegral C.tickRate_hz
    , oi_angleFins = mouseAngle
    , oi_ticksSinceDash = or_ticksSinceDash ov
    }
  where
    ov :: OverseerRendering
    ov = rendering_entityRendering rc

fromOverseerDeathParticle ::
  Time ->
  Float ->
  Particle OverseerDeathParticle ->
  OverseerInstance
fromOverseerDeathParticle time mouseAngle particle =
  OverseerInstance
    { oi_radius = Fixed.toFloat (odp_scale ov)
    , oi_pos = pos
    , oi_prevPos1 = pos
    , oi_prevPos2 = pos
    , oi_prevPos3 = pos
    , oi_prevPos4 = pos
    , oi_prevPos5 = pos
    , oi_prevPos6 = pos
    , oi_prevPos7 = pos
    , oi_prevPos8 = pos
    , oi_prevPos9 = pos
    , oi_prevPos10 = pos
    , oi_healthFraction = 0
    , oi_shieldsFraction = 0
    , oi_shieldColor = 1 -- should be unused since `oi_shieldsFraction` is 0
    , oi_color = Float4 1 1 1 opacity
    , oi_recentDamage = 0
    , oi_isInvulnerable = False
    , oi_class = odp_killedClass ov
    , oi_angleOffset = overseerRotation time (odp_entityID ov)
    , oi_psiStormReadySeconds = noPsiStormReadySeconds
    , oi_angleFins = mouseAngle
    , oi_ticksSinceDash = Nothing
    }
  where
    ov :: OverseerDeathParticle
    ov = particle_type particle

    pos =
      Fixed.toFloats $
        odp_pos ov
          + Fixed.map (* (0.5 * fromIntegral ticksSinceDeath)) (odp_vel ov)
    ticksSinceDeath =
      getTicks $ diffTime time (particle_startTime particle)
    opacity =
      -- Not following decay rate of damage flash.
      0.5 ** max 0 (fromIntegral ticksSinceDeath - 1)

overseerRidgesScale :: PlayerClass -> Float
overseerRidgesScale = \case Zealot -> 1; Templar -> 0

overseerFinParams :: PlayerClass -> Float4
overseerFinParams = \case
  Zealot -> Float4 0.685 0.500 0.362 (-0.20)
  Templar -> Float4 0.598 0.776 0.362 0.10

noPsiStormReadySeconds :: Float
noPsiStormReadySeconds = -1

-- Have overseers rotate slowly over time. Vary drones by their `EntityID`
-- (arbitrary choice).
overseerRotation :: Time -> Int -> Float
overseerRotation time eid =
  let ticks = getTicks $ time `diffTime` initialTime
      eidPart = (* 0.125) $ fromIntegral $ eid .&. 127
      timePart = if even eid then t else -t
        where
          t = (2 * Prelude.pi / 4096 *) $ fromIntegral $ ticks .&. 4095
  in  eidPart + timePart

makeGLInstance :: OverseerInstance -> OverseerGLInstance Cpu
makeGLInstance ovInstance =
  OverseerGLInstance
    { ogi_modelScaleTrans =
        fromFloat12 (oi_radius ovInstance) (oi_pos ovInstance)
    , ogi_healthShieldsFractions =
        let rawShieldFraction = oi_shieldsFraction ovInstance
            shieldFraction =
              if rawShieldFraction > 0
                then max 0.025 rawShieldFraction -- ensure presence of shield is visible
                else 0
        in  Float2 (oi_healthFraction ovInstance) shieldFraction
    , ogi_color =
        let (color, opacity) = toRGB_A (oi_color ovInstance)
        in  fromRGB_A (Float.map3 damageFilter' color) opacity
    , ogi_shieldColor = oi_shieldColor ovInstance
    , ogi_shadowOffsets12 =
        fromFloat22
          (oi_prevPos1 ovInstance - oi_pos ovInstance)
          (oi_prevPos2 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets34 =
        fromFloat22
          (oi_prevPos3 ovInstance - oi_pos ovInstance)
          (oi_prevPos4 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets56 =
        fromFloat22
          (oi_prevPos5 ovInstance - oi_pos ovInstance)
          (oi_prevPos6 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets78 =
        fromFloat22
          (oi_prevPos7 ovInstance - oi_pos ovInstance)
          (oi_prevPos8 ovInstance - oi_pos ovInstance)
    , ogi_shadowOffsets910 =
        fromFloat22
          (oi_prevPos9 ovInstance - oi_pos ovInstance)
          (oi_prevPos10 ovInstance - oi_pos ovInstance)
    , ogi_ridgesScale = overseerRidgesScale (oi_class ovInstance)
    , ogi_finParams = overseerFinParams (oi_class ovInstance)
    , ogi_angleOffset = oi_angleOffset ovInstance
    , ogi_psiStormReadySeconds = oi_psiStormReadySeconds ovInstance
    , ogi_angleFins = oi_angleFins ovInstance
    , ogi_secondsSinceDash =
        maybe
          noSecondsSinceDash
          ((/ fromIntegral C.tickRate_hz) . fromIntegral)
          (oi_ticksSinceDash ovInstance)
    }
  where
    damageFilter' :: Float -> Float
    damageFilter' =
      if oi_isInvulnerable ovInstance
        then invulnerabilityFilter
        else
          let w = damageFilterWeight (oi_recentDamage ovInstance)
          in  \x -> w + (1 - w) * x

noSecondsSinceDash :: Float
noSecondsSinceDash = 10

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ square 1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , invScale :: Expr Float
  , healthFraction :: Expr Float
  , shieldFraction :: Expr Float
  , baseShieldColor :: Expr Float3
  , color :: Expr Float4
  , shadowOffsets12 :: Expr Float4
  , shadowOffsets34 :: Expr Float4
  , shadowOffsets56 :: Expr Float4
  , shadowOffsets78 :: Expr Float4
  , shadowOffsets910 :: Expr Float4
  , ridgesScale :: Expr Float
  , finParams :: Expr Float4
  , angleOffset :: Expr Float
  , psiStormReadySeconds :: Expr Float
  , angleFins :: Expr Float
  , secondsSinceDash :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  OverseerGLInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} OverseerGLInstance{..} StandardUniforms{..} =
  let modelScale = x_ ogi_modelScaleTrans
      modelTrans = yz_ ogi_modelScaleTrans
      lastShadowOffset = zw_ ogi_shadowOffsets78 -- needs updating if more shadows are added
      offsetLength = length lastShadowOffset
      lineRadius = 1.8 * modelScale
      boxRadiusX = lineRadius + 0.5 * offsetLength
      boxRadiusY = lineRadius
      scaledTranslatedVertex =
        vec2
          ( boxRadiusX * x_ position + 0.5 * offsetLength
          , boxRadiusY * y_ position
          )

      offsetDir =
        ifThenElse'
          (offsetLength <# 0.01)
          (vec2 (1, 0))
          (normalize lastShadowOffset)
      rotatedVertex =
        vec2
          ( x_ offsetDir * x_ scaledTranslatedVertex - y_ offsetDir * y_ scaledTranslatedVertex
          , y_ offsetDir * x_ scaledTranslatedVertex + x_ offsetDir * y_ scaledTranslatedVertex
          )

      screenPos =
        su_viewScale
          * (rotatedVertex + modelTrans - su_viewCenter)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { pos = rotatedVertex ./ modelScale
          , invScale = recip modelScale
          , healthFraction = x_ ogi_healthShieldsFractions
          , shieldFraction = y_ ogi_healthShieldsFractions
          , baseShieldColor = ogi_shieldColor
          , color = ogi_color
          , shadowOffsets12 = ogi_shadowOffsets12 ./ modelScale
          , shadowOffsets34 = ogi_shadowOffsets34 ./ modelScale
          , shadowOffsets56 = ogi_shadowOffsets56 ./ modelScale
          , shadowOffsets78 = ogi_shadowOffsets78 ./ modelScale
          , shadowOffsets910 = ogi_shadowOffsets910 ./ modelScale
          , ridgesScale = ogi_ridgesScale
          , finParams = ogi_finParams
          , angleOffset = ogi_angleOffset
          , psiStormReadySeconds = ogi_psiStormReadySeconds
          , angleFins = ogi_angleFins
          , secondsSinceDash = ogi_secondsSinceDash
          }
  in  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader StandardUniforms{} PipelineVars{..} = do
  -- Constants
  let pi = 3.1415926535897932384626433832795
      fuzziness = 0.5 * invScale

  -- Radial divisions
  let shell_outer_radius = 1.0
      shell_inner_radius = 0.85
      core_outer_radius = 0.8
      core_inner_radius = 0.47
      shield_outer_radius = 1.075
      shield_inner_radius = 0.8

  radius <- octagon pos angleOffset ridgesScale
  body_outer_mask <-
    bind $ 1 - smoothstepCentered shell_outer_radius fuzziness radius

  -- Body
  (body_mask, shield_mask) <- do
    angle_ <- bind $ atan2 (y_ pos) (x_ pos)
    angle_frac <- bind $ abs angle_ / pi

    core_mask <- do
      core_angular_mask <- do
        inner_ring_fuzziness <-
          let extra_fuzziness_factor = 4
           in bind $ extra_fuzziness_factor * fuzziness / (pi * radius)
        ring_frac <-
          bind $
            -- So that 100% health is displayed as completely full, and 0% health
            -- as completely empty
            healthFraction * (1 + 1.5 * inner_ring_fuzziness) - 0.75 * inner_ring_fuzziness
        bind $ 1 - smoothstepCentered ring_frac inner_ring_fuzziness angle_frac

      bind $ -- core_mask
        smoothstepCentered core_inner_radius fuzziness radius
          * (1 - smoothstepCentered core_outer_radius fuzziness radius)
          * core_angular_mask

    -- Copied from core_mask
    shield_mask <- do
      shield_angular_mask <- do
        inner_ring_fuzziness <-
          let extra_fuzziness_factor = 2
           in bind $ extra_fuzziness_factor * fuzziness / (pi * radius)
        bind $ 1 - smoothstepCentered shieldFraction inner_ring_fuzziness angle_frac

      bind $ -- shield_mask
        smoothstepCentered shield_inner_radius fuzziness radius
          * (1 - smoothstepCentered shield_outer_radius fuzziness radius)
          * shield_angular_mask
          * ifThenElse' (shieldFraction ># 0) 0.775 0.0

    shell_mask <-
      bind $
        smoothstepCentered
          shell_inner_radius
          fuzziness
          radius
          * body_outer_mask

    body_mask <- bind $ core_mask + shell_mask
    pure (body_mask, shield_mask)

  -- Bloom
  bloom_mask <- do
    bloom_radius <-
      bind $
        max' 0 $
          max' (radius - shell_outer_radius) (core_inner_radius - radius)
    bind $ 0.4 * exp' (-40 * bloom_radius * bloom_radius) -- bloom_mask

  -- Unpack
  shadowOffset1 <- bind $ xy_ shadowOffsets12
  shadowOffset2 <- bind $ zw_ shadowOffsets12
  shadowOffset3 <- bind $ xy_ shadowOffsets34
  shadowOffset4 <- bind $ zw_ shadowOffsets34
  shadowOffset5 <- bind $ xy_ shadowOffsets56
  shadowOffset6 <- bind $ zw_ shadowOffsets56
  shadowOffset7 <- bind $ xy_ shadowOffsets78
  shadowOffset8 <- bind $ zw_ shadowOffsets78
  shadowOffset9 <- bind $ xy_ shadowOffsets910
  shadowOffset10 <- bind $ zw_ shadowOffsets910

  -- Fins ("spawners" in diep.io, but in this game drones don't spawn from them)
  finParams' <- do
    fp_width1 <- bind $ x_ finParams
    fp_width2 <- bind $ y_ finParams
    fp_height <- bind $ z_ finParams
    fp_curvature <- bind $ w_ finParams
    pure FinParams{..}
  fins_outer_mask <- finsOuterMask fuzziness angleFins finParams' pos
  fins_mask <- finsOpacity 1.00 1.00 fuzziness angleFins finParams' pos
  fins_shadow_mask <- do
    fins1 <- finsOpacity radiusFactor1 opacityFactor1 fuzziness angleFins finParams' (pos - shadowOffset1)
    fins2 <- finsOpacity radiusFactor2 opacityFactor2 fuzziness angleFins finParams' (pos - shadowOffset2)
    fins3 <- finsOpacity radiusFactor3 opacityFactor3 fuzziness angleFins finParams' (pos - shadowOffset3)
    fins4 <- finsOpacity radiusFactor4 opacityFactor4 fuzziness angleFins finParams' (pos - shadowOffset4)
    fins5 <- finsOpacity radiusFactor5 opacityFactor5 fuzziness angleFins finParams' (pos - shadowOffset5)
    fins6 <- finsOpacity radiusFactor6 opacityFactor6 fuzziness angleFins finParams' (pos - shadowOffset6)
    fins7 <- finsOpacity radiusFactor7 opacityFactor7 fuzziness angleFins finParams' (pos - shadowOffset7)
    fins8 <- finsOpacity radiusFactor8 opacityFactor8 fuzziness angleFins finParams' (pos - shadowOffset8)
    fins9 <- finsOpacity radiusFactor9 opacityFactor9 fuzziness angleFins finParams' (pos - shadowOffset9)
    fins10 <- finsOpacity radiusFactor10 opacityFactor10 fuzziness angleFins finParams' (pos - shadowOffset10)
    bind $
      max'
        ( max'
            (max' (max' fins1 fins2) (max' fins3 fins4))
            (max' (max' fins5 fins6) (max' fins7 fins8))
        )
        (max' fins9 fins10)
  fins_opacity <-
    bind $
      fins_mask
        + (1 - fins_outer_mask) * fins_shadow_mask

  -- Body shadows
  shadow_mask <- do
    offset1 <- bind $ pos - shadowOffset1
    shadow1_mask <- shadowOpacity fuzziness radiusFactor1 opacityFactor1 shell_outer_radius offset1 angleOffset ridgesScale
    offset2 <- bind $ pos - shadowOffset2
    shadow2_mask <- shadowOpacity fuzziness radiusFactor2 opacityFactor2 shell_outer_radius offset2 angleOffset ridgesScale
    offset3 <- bind $ pos - shadowOffset3
    shadow3_mask <- shadowOpacity fuzziness radiusFactor3 opacityFactor3 shell_outer_radius offset3 angleOffset ridgesScale
    offset4 <- bind $ pos - shadowOffset4
    shadow4_mask <- shadowOpacity fuzziness radiusFactor4 opacityFactor4 shell_outer_radius offset4 angleOffset ridgesScale
    offset5 <- bind $ pos - shadowOffset5
    shadow5_mask <- shadowOpacity fuzziness radiusFactor5 opacityFactor5 shell_outer_radius offset5 angleOffset ridgesScale
    offset6 <- bind $ pos - shadowOffset6
    shadow6_mask <- shadowOpacity fuzziness radiusFactor6 opacityFactor6 shell_outer_radius offset6 angleOffset ridgesScale
    offset7 <- bind $ pos - shadowOffset7
    shadow7_mask <- shadowOpacity fuzziness radiusFactor7 opacityFactor7 shell_outer_radius offset7 angleOffset ridgesScale
    offset8 <- bind $ pos - shadowOffset8
    shadow8_mask <- shadowOpacity fuzziness radiusFactor8 opacityFactor8 shell_outer_radius offset8 angleOffset ridgesScale
    offset9 <- bind $ pos - shadowOffset9
    shadow9_mask <- shadowOpacity fuzziness radiusFactor9 opacityFactor9 shell_outer_radius offset9 angleOffset ridgesScale
    offset10 <- bind $ pos - shadowOffset10
    shadow10_mask <- shadowOpacity fuzziness radiusFactor10 opacityFactor10 shell_outer_radius offset10 angleOffset ridgesScale
    combined_shadow_mask <-
      bind $
        max'
          ( max'
              ( max'
                  (max' shadow1_mask shadow2_mask)
                  (max' shadow3_mask shadow4_mask)
              )
              ( max'
                  (max' shadow5_mask shadow6_mask)
                  (max' shadow7_mask shadow8_mask)
              )
          )
          (max' shadow9_mask shadow10_mask)
    bind $ combined_shadow_mask * (1 - 0.35 * fins_outer_mask)

  -- Cooldown ready flash
  flashMask <- do
    progress <- bind $ 4 * psiStormReadySeconds

    let flashWidth = 0.7
    moveRadius <- bind $ 1 + flashWidth
    flashCenter <- bind $ -moveRadius + progress * 2 * moveRadius

    s <- bind $ let sqrt2 = 0.7071067811865475 in sqrt2 * (x_ pos + y_ pos)
    bind $
      step (flashCenter - flashWidth) s
        * (1 - step (flashCenter + flashWidth) s)

  -- Composition
  body_and_bloom_mask <- bind $ max' body_mask bloom_mask
  opacity_out <-
    bind $
      body_and_bloom_mask
        + min' (1 - body_and_bloom_mask) (1 - body_outer_mask)
          * max' shadow_mask fins_opacity

  color_out <- do
    dashWeight <- bind $ max' 0 $ min' 1 $ exp (-secondsSinceDash / 0.1)
    baseColor <-
      bind $
        1
          - ( (1 -. xyz_ color .* (1 + 0.12 * dashWeight))
                .* (1 - 0.2 * dashWeight)
            )
    shieldColor <-
      bind $ shield_mask *. baseShieldColor + (1 - shield_mask) *. baseColor
    flashColor <- bind $ flashMask +. (1 - flashMask) *. shieldColor
    bind $
      body_and_bloom_mask *. flashColor
        + (1 - body_and_bloom_mask) *. shieldColor
  baseOpacity <- bind $ w_ color

  pure $ vec4 (color_out, baseOpacity * opacity_out)

shadowOpacity ::
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float2 ->
  Expr Float ->
  Expr Float ->
  GLSL (Expr Float)
shadowOpacity
  fuzziness
  scale
  opacity
  shell_outer_radius
  offset
  angleOffset
  ridgesScale = do
    shadow_radius <- octagon offset angleOffset ridgesScale
    scaled_shadow_radius <- bind $ shadow_radius / scale
    shadow_mask <-
      bind $
        smoothstepCentered
          shell_outer_radius
          (4 * fuzziness)
          scaled_shadow_radius
    bind $ opacity * (1 - shadow_mask)

octagon :: Expr Float2 -> Expr Float -> Expr Float -> GLSL (Expr Float)
octagon offset angleOffset ridgesScale = do
  angle_ <- bind $ angleOffset + atan2 (y_ offset) (x_ offset)
  bind $ length offset * (1 + ridgesScale * 0.021 * sin (angle_ * 8))

finsOpacity ::
  Expr Float ->
  Expr Float ->
  Expr Float ->
  Expr Float ->
  FinParams ->
  Expr Float2 ->
  GLSL (Expr Float)
finsOpacity scale opacity fuzziness angleFins finParams pos = do
  dr <- finsSignedDist scale angleFins finParams pos
  bind $
    0.65 * opacity * (1 - smoothstepCentered finLineWidth fuzziness (abs dr))

finsOuterMask ::
  Expr Float ->
  Expr Float ->
  FinParams ->
  Expr Float2 ->
  GLSL (Expr Float)
finsOuterMask fuzziness angleFins finParams pos = do
  dr <- finsSignedDist 1 angleFins finParams pos
  opacityComplement <-
    bind $
      smoothstepCentered
        (finLineWidth + finBoundaryWidth)
        (fuzziness * finBoundaryFuzzinessFactor)
        dr
  bind $ 1 - opacityComplement

finLineWidth :: Expr Float
finLineWidth = 0.0719

finBoundaryWidth :: Expr Float
finBoundaryWidth = 0.0144

finBoundaryFuzzinessFactor :: Expr Float
finBoundaryFuzzinessFactor = 2.5

data FinParams = FinParams
  { fp_width1 :: Expr Float
  , fp_width2 :: Expr Float
  , fp_height :: Expr Float
  , fp_curvature :: Expr Float
  }

finsSignedDist ::
  Expr Float -> Expr Float -> FinParams -> Expr Float2 -> GLSL (Expr Float)
finsSignedDist scale angleFins FinParams{..} pos = do
  s <- bind $ sin angleFins
  c <- bind $ cos angleFins
  rot <- bind $ vec2 (c * x_ pos - s * y_ pos, s * x_ pos + c * y_ pos)
  rot2 <- bind $ ifThenElse' (x_ rot <# 0) (-rot) rot
  rot4 <- bind $ ifThenElse' (y_ rot2 <# 0) (vec2 (-y_ rot2, x_ rot2)) rot2
  rot4' <- bind $ vec2 (x_ rot4 - y_ rot4, x_ rot4 + y_ rot4) ./ (sqrt 2 * scale)
  x <- bind $ x_ rot4'
  curve_x <- bind $ x * fp_curvature
  curve_x2 <- bind $ curve_x * curve_x
  curve_x4 <- bind $ curve_x2 * curve_x2
  y_offset <- bind $ (0.5 * curve_x2 + 0.125 * curve_x4) / fp_curvature -- circle
  trapSD <-
    isoscelesTrapezoid
      fp_width1
      fp_width2
      fp_height
      (rot4' - vec2 (0, 0.8625 + y_offset))
  bind $ trapSD + 0.030 -- rounded/sharpened corners

-- Helper
smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius = smoothstep (cutoff - radius) (cutoff + radius)
