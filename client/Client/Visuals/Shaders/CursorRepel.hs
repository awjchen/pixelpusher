{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Cursor for repelling drones
--
-- TODO: Use uniforms rather than instances
module Client.Visuals.Shaders.CursorRepel (
  loadCursorRepel,
  drawCursorRepel,
  CursorRepelInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CursorRepelInstance :: GLKind -> Type
data CursorRepelInstance k = CursorRepelInstance
  { cri_size :: GLData k Float
  , cri_center :: GLData k Float2
  , cri_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CursorRepelInstance Cpu)
deriving anyclass instance GLAttribute (CursorRepelInstance Cpu)
deriving anyclass instance GLSLInput (CursorRepelInstance Gpu)

--------------------------------------------------------------------------------

loadCursorRepel ::
  TimedFastLogger -> GL (StandardInstancedVisual CursorRepelInstance)
loadCursorRepel logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawCursorRepel ::
  StandardInstancedVisual CursorRepelInstance ->
  StandardVParams CursorRepelInstance ->
  GL ()
drawCursorRepel = drawStandardVisual

maxInstances :: Word16
maxInstances = 2 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CursorRepelInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CursorRepelInstance{..} StandardUniforms{..} = do
  pos <- bind $ cri_size *. position
  screenPos <-
    bind $
      su_viewScale
        * (cri_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let radius = cri_size
      color = cri_color
  pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  let fuzziness = 0.5
  r <- bind $ length pos

  radialFuzziness <- bind $ fuzziness / r
  angle_ <- bind $ atan2 (y_ pos) (x_ pos)
  angleMask <-
    bind $
      1
        - smoothstepCentered
          0.3125
          radialFuzziness
          (abs (mod' (angle_ / (pi / 3)) 1 - 0.5))

  outerRing <-
    let outerRadius = radius
        innerRadius = radius - 1
     in bind $
          (1 - smoothstepCentered outerRadius fuzziness r)
            * smoothstepCentered innerRadius fuzziness r
  innerRing <-
    let outerRadius = radius - 2
        innerRadius = radius - 2.5
     in bind $
          (1 - smoothstepCentered outerRadius fuzziness r)
            * smoothstepCentered innerRadius fuzziness r
  opacity <- bind $ max' outerRing innerRing * angleMask
  pure $ vec4 (xyz_ color, opacity * w_ color)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)
