{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Powerup spawners, based on team bases
module Client.Visuals.Shaders.PowerupSpawner (
  loadPowerupSpawner,
  drawPowerupSpawner,
  PowerupSpawnerInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float3, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type PowerupSpawnerInstance :: GLKind -> Type
data PowerupSpawnerInstance k = PowerupSpawnerInstance
  { psi_modelScale :: GLData k Float
  , psi_modelTrans :: GLData k Float2
  , psi_color :: GLData k Float3
  , psi_boundaryOpacity :: GLData k Float
  , psi_outerOpacity :: GLData k Float
  , psi_innerOpacity :: GLData k Float
  , psi_innerRadiusFraction :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (PowerupSpawnerInstance Cpu)
deriving anyclass instance GLAttribute (PowerupSpawnerInstance Cpu)
deriving anyclass instance GLSLInput (PowerupSpawnerInstance Gpu)

loadPowerupSpawner ::
  TimedFastLogger -> GL (StandardInstancedVisual PowerupSpawnerInstance)
loadPowerupSpawner logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

maxInstances :: Word16
maxInstances = 32 -- arbitrary

drawPowerupSpawner ::
  StandardInstancedVisual PowerupSpawnerInstance ->
  StandardVParams PowerupSpawnerInstance ->
  GL ()
drawPowerupSpawner = drawStandardVisual

----------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float3
  , boundaryOpacity :: Expr Float
  , outerOpacity :: Expr Float
  , innerOpacity :: Expr Float
  , innerRadiusFraction :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  PowerupSpawnerInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} PowerupSpawnerInstance{..} StandardUniforms{..} = do
  pos <- bind $ psi_modelScale *. position
  screenPos <-
    bind $
      su_viewScale
        * (psi_modelTrans + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let pipelineVars =
        PipelineVars
          { radius = psi_modelScale
          , pos = pos
          , color = psi_color
          , boundaryOpacity = psi_boundaryOpacity
          , outerOpacity = psi_outerOpacity
          , innerOpacity = psi_innerOpacity
          , innerRadiusFraction = psi_innerRadiusFraction
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos

  outerMask <- bind $ smoothstep (-0.5) 0.5 (r - radius)
  innerMask <- bind $ smoothstep (-0.5) 0.5 (r - radius * innerRadiusFraction)
  fill <-
    bind $
      outerOpacity * (1 - outerMask) * innerMask
        + innerOpacity .* (1 - innerMask)

  dr <- bind $ r - radius
  boundary <- do
    line <- bind $ 1 - smoothstep 0 1 (abs dr)
    lineBloom <- bind $ 0.6 * exp' (-0.2 * dr * dr)
    bind $ boundaryOpacity * max' line lineBloom

  opacity <- bind $ max' fill boundary
  pure $ vec4 (color, opacity)
