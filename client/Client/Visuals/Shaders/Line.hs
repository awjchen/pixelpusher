{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | For debugging
module Client.Visuals.Shaders.Line (
  loadLine,
  drawLine,
  LineInstance (..),
) where

import Prelude hiding (length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type LineInstance :: GLKind -> Type
data LineInstance k = LineInstance
  { li_endpoint1 :: GLData k Float2
  , li_endpoint2 :: GLData k Float2
  , li_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (LineInstance Cpu)
deriving anyclass instance GLAttribute (LineInstance Cpu)
deriving anyclass instance GLSLInput (LineInstance Gpu)

--------------------------------------------------------------------------------

loadLine :: TimedFastLogger -> GL (StandardInstancedVisual LineInstance)
loadLine logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawLine ::
  StandardInstancedVisual LineInstance ->
  StandardVParams LineInstance ->
  GL ()
drawLine = drawStandardVisual

maxInstances :: Word16
maxInstances = 1024 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ square 1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars
  { color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  LineInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  Position{position}
  LineInstance{..}
  StandardUniforms{..} = do
    lineVec <- bind $ li_endpoint2 - li_endpoint1
    lineDir <- bind $ normalize lineVec
    linePerpDir <- bind $ vec2 (-y_ lineDir, x_ lineDir)
    lineLength <- bind $ length lineVec
    lineCenter <- bind $ li_endpoint1 + 0.5 *. lineVec
    let lineWidth = 1
    x <- bind $ 0.5 * lineLength * x_ position
    y <- bind $ 0.5 * lineWidth * y_ position
    pos <- bind $ lineCenter + x *. lineDir + y *. linePerpDir
    screenPos <-
      bind $ su_viewScale * (pos - su_viewCenter) / vec2 (su_aspectRatio, 1)
    let color = li_color
    pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = pure color
