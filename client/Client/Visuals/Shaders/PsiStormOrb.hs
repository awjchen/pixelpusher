{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Psi-storm orbs orbiting overseers
module Client.Visuals.Shaders.PsiStormOrb (
  loadPsiStormOrb,
  drawPsiStormOrb,
  PsiStormOrbInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

import Client.Visuals.Shaders.PsiStorm (psiStormWaves)

--------------------------------------------------------------------------------

type PsiStormOrbInstance :: GLKind -> Type
data PsiStormOrbInstance k = PsiStormOrbInstance
  { psoi_size :: GLData k Float
  , psoi_center :: GLData k Float2
  , psoi_color :: GLData k Float4
  , psoi_time :: GLData k Float
  , psoi_bloomRadiusFactor :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (PsiStormOrbInstance Cpu)
deriving anyclass instance GLAttribute (PsiStormOrbInstance Cpu)
deriving anyclass instance GLSLInput (PsiStormOrbInstance Gpu)

loadPsiStormOrb ::
  TimedFastLogger -> GL (StandardInstancedVisual PsiStormOrbInstance)
loadPsiStormOrb logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawPsiStormOrb ::
  StandardInstancedVisual PsiStormOrbInstance ->
  StandardVParams PsiStormOrbInstance ->
  GL ()
drawPsiStormOrb = drawStandardVisual

maxInstances :: Word16
maxInstances = C.maxNumEntities

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , time :: Expr Float
  , bloomRadiusFactor :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  PsiStormOrbInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} PsiStormOrbInstance{..} StandardUniforms{..} = do
  pos <- bind $ (psoi_size + 2 / (0.7 * psoi_bloomRadiusFactor)) *. position
  screenPos <-
    bind $
      su_viewScale
        * (psoi_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let radius = psoi_size
      color = psoi_color
      time = psoi_time
      bloomRadiusFactor = psoi_bloomRadiusFactor
  pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader StandardUniforms{} PipelineVars{..} = do
  r <- bind $ length pos
  dr <- bind $ r - radius
  frac <- bind $ r / radius

  outerMask <- bind $ smoothstep (-0.5) 0.5 dr

  body <- do
    boundedFrac <- bind $ min' 1 frac
    x <- bind $ asin boundedFrac
    scaledPos <- bind $ pos .* (x * 35)
    signedWaves <- psiStormWaves time scaledPos
    bind $ sqrt $ 0.5 * (1 + signedWaves)

  bloom <- do
    s <- bind $ bloomRadiusFactor * dr
    bind $ 0.4 * exp (-s * s)

  opacity <- bind $ outerMask * bloom + (1 - outerMask) * body
  pure $ vec4 (xyz_ color, w_ color * opacity)
