{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Dust cloud effect for dashes
module Client.Visuals.Shaders.Dash (
  loadDash,
  drawDash,
  DashInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type DashInstance :: GLKind -> Type
data DashInstance k = DashInstance
  { di_center :: GLData k Float2
  , di_initialVelocity :: GLData k Float2
  , di_scale :: GLData k Float
  , di_angle :: GLData k Float
  , di_ticksElapsed :: GLData k Float
  , di_secondsElapsed :: GLData k Float
  , di_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (DashInstance Cpu)
deriving anyclass instance GLAttribute (DashInstance Cpu)
deriving anyclass instance GLSLInput (DashInstance Gpu)

loadDash :: TimedFastLogger -> GL (StandardInstancedVisual DashInstance)
loadDash logger =
  loadStandardVisual logger vertices compiledShaders C.maxNumEntities

drawDash ::
  StandardInstancedVisual DashInstance ->
  StandardVParams DashInstance ->
  GL ()
drawDash = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.01

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , size :: Expr Float
  , secondsElapsed :: Expr Float
  , fuzziness :: Expr Float
  , baseColor :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  DashInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} DashInstance{..} StandardUniforms{..} = do
  fuzziness <- do
    let fuzzSqPerSecond = 24 * 24
    bind $ sqrt $ 1 + fuzzSqPerSecond * di_secondsElapsed

  localPos <- bind $ position .* (di_scale + fuzziness) -- game-world scale
  screenPos <- do
    let tau = 8 -- dust drag time constant
    distanceFactor <- bind $ tau * (1 - exp (-di_ticksElapsed / tau))

    pushVec <- do
      let pushVelocity = 2
      pushDist <- bind $ pushVelocity * distanceFactor
      bind $ vec2 (-pushDist * cos di_angle, -pushDist * sin di_angle)
    driftVec <- bind $ distanceFactor *. di_initialVelocity
    gameWorldPos <- bind $ localPos + pushVec + driftVec
    bind $
      su_viewScale
        * (gameWorldPos + di_center - su_viewCenter)
        / vec2 (su_aspectRatio, 1)

  let pipelineVars =
        PipelineVars
          { pos = localPos
          , size = di_scale
          , secondsElapsed = di_secondsElapsed
          , fuzziness = fuzziness
          , baseColor = di_color
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  mask <- bind $ 1 - smoothstep (-fuzziness) fuzziness (length pos - size)
  opacity <-
    bind $
      min' (0.5 * exp (-3.5 * secondsElapsed)) (secondsElapsed / 0.05)
  pure $ vec4 (xyz_ baseColor, w_ baseColor * opacity * mask)
