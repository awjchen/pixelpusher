{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Soft obstacle entities
module Client.Visuals.Shaders.SoftObstacle (
  loadSoftObstacle,
  drawSoftObstacle,
  SoftObstacleInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float3, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type SoftObstacleInstance :: GLKind -> Type
data SoftObstacleInstance k = SoftObstacleInstance
  { soi_modelScale :: GLData k Float
  , soi_modelTrans :: GLData k Float2
  , soi_color :: GLData k Float3
  , soi_maxOpacity :: GLData k Float
  , soi_minOpacity :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (SoftObstacleInstance Cpu)
deriving anyclass instance GLAttribute (SoftObstacleInstance Cpu)
deriving anyclass instance GLSLInput (SoftObstacleInstance Gpu)

loadSoftObstacle ::
  TimedFastLogger -> GL (StandardInstancedVisual SoftObstacleInstance)
loadSoftObstacle logger =
  loadStandardVisual logger vertices compiledShaders C.maxSoftObstacles

drawSoftObstacle ::
  StandardInstancedVisual SoftObstacleInstance ->
  StandardVParams SoftObstacleInstance ->
  GL ()
drawSoftObstacle = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ disk 32 1.03

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , radius :: Expr Float
  , color :: Expr Float3
  , minOpacity :: Expr Float
  , opacityRange :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  SoftObstacleInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} SoftObstacleInstance{..} StandardUniforms{..} = do
  localPos <- bind $ soi_modelScale *. position
  pure $
    let screenPos =
          su_viewScale
            * (soi_modelTrans - su_viewCenter + localPos)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { pos = localPos
            , radius = soi_modelScale
            , color = soi_color
            , minOpacity = soi_minOpacity
            , opacityRange = soi_maxOpacity - soi_minOpacity
            }
    in  (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  s <- bind $ length pos
  r <- bind $ max' 0 (s - (radius - 120)) / 120
  r2 <- bind $ r * r
  r4 <- bind $ r2 * r2
  r6 <- bind $ r4 * r2
  r8 <- bind $ r4 * r4
  r16 <- bind $ r8 * r8
  dr <- bind $ (r - 1) * radius
  outerBound <- bind $ 1 - smoothstep (-0.5) 0.5 dr
  opacity <- bind $ outerBound * (opacityRange * 0.5 * (r16 + r6) + minOpacity)
  pure $ vec4 (color, opacity)
