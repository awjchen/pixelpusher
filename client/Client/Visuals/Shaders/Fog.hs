{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Fog to obscure area beyond field of view
module Client.Visuals.Shaders.Fog (
  loadFog,
  drawFog,
  FogVisual,
  FogVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import Data.Kind (Type)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data FogVisual = FogVisual
  { fv_vao :: VertexArray
  , fv_shaderProg :: ShaderProgram
  , fv_nVertices :: GLsizei
  , fv_uniformLocations :: UniformLocations (FogVParams Cpu)
  }

type FogVParams :: GLKind -> Type
data FogVParams k = FogVParams
  { fvp_aspectRatio :: GLData k Float
  , fvp_viewScale :: GLData k Float
  , fvp_viewCenter :: GLData k Float2
  , fvp_modelScale :: GLData k Float
  , fvp_modelTrans :: GLData k Float2
  , fvp_fogRadius :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (FogVParams Cpu)
deriving anyclass instance GLUniform (FogVParams Cpu)
deriving anyclass instance GLSLInput (FogVParams Gpu)

drawFog :: FogVisual -> FogVParams Cpu -> GL ()
drawFog fog params =
  withVertexArray (fv_vao fog) $
    withShaderProgram (fv_shaderProg fog) $ do
      -- Set uniforms
      writeUniforms (fv_uniformLocations fog) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (fv_nVertices fog)

loadFog :: TimedFastLogger -> GL FogVisual
loadFog logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    FogVisual
      { fv_vao = vao
      , fv_shaderProg = shaderProg
      , fv_nVertices = fromIntegral $ P.length vertices
      , fv_uniformLocations = uniforms
      }

vertices :: [Position Cpu]
vertices = map Position $ square 5

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars
  { worldPos :: Expr Float2
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  GLUnit Gpu ->
  FogVParams Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} GLUnit FogVParams{..} = do
  worldPos <- bind $ fvp_modelTrans + fvp_modelScale *. position
  screenPos <-
    bind $
      fvp_viewScale *. (worldPos - fvp_viewCenter) / vec2 (fvp_aspectRatio, 1)
  pure
    ( screenPos
    , PipelineVars
        { worldPos = worldPos
        }
    )

fragShader :: FogVParams Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader FogVParams{..} PipelineVars{..} = do
  r <- bind $ length $ worldPos - fvp_modelTrans
  opacity <- do
    let fadeWidth = 6
    x <- bind $ max' 0 $ min' 1 $ (r - (fvp_fogRadius - fadeWidth)) / fadeWidth
    bind $ x * x
  pure $ vec4 (0, 0, 0, opacity)
