{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Shield powerup entities
module Client.Visuals.Shaders.ShieldPowerup (
  loadShieldPowerup,
  drawShieldPowerup,
  ShieldPowerupInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type ShieldPowerupInstance :: GLKind -> Type
data ShieldPowerupInstance k = ShieldPowerupInstance
  { spi_size :: GLData k Float
  , spi_center :: GLData k Float2
  , spi_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (ShieldPowerupInstance Cpu)
deriving anyclass instance GLAttribute (ShieldPowerupInstance Cpu)
deriving anyclass instance GLSLInput (ShieldPowerupInstance Gpu)

loadShieldPowerup ::
  TimedFastLogger -> GL (StandardInstancedVisual ShieldPowerupInstance)
loadShieldPowerup logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawShieldPowerup ::
  StandardInstancedVisual ShieldPowerupInstance ->
  StandardVParams ShieldPowerupInstance ->
  GL ()
drawShieldPowerup = drawStandardVisual

maxInstances :: Word16
maxInstances = C.maxNumEntities

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  ShieldPowerupInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} ShieldPowerupInstance{..} StandardUniforms{..} = do
  pos <- bind $ (spi_size + 2 / bloomRadiusFactor) *. position
  screenPos <-
    bind $
      su_viewScale
        * (spi_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let radius = spi_size
      color = spi_color
  pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader StandardUniforms{} PipelineVars{..} = do
  dr <- bind $ length pos - radius

  ringMask <- do
    let thickness = 1
    halfThickness <- bind $ 0.5 * thickness
    outerMask <- bind $ 1 - smoothstep (-0.5) 0.5 (dr - halfThickness)
    innerMask <- bind $ smoothstep (-0.5) 0.5 (dr + halfThickness)
    bind $ outerMask * innerMask

  s <- bind $ bloomRadiusFactor * dr
  bloom <- bind $ 0.4 * exp (-s * s)

  opacity <- bind $ max' bloom ringMask
  pure $ vec4 (xyz_ color, w_ color * opacity)

bloomRadiusFactor :: Expr Float
bloomRadiusFactor = 0.25
