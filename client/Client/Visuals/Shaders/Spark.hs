{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Sparks for indicating damage dealt
module Client.Visuals.Shaders.Spark (
  loadSpark,
  drawSpark,
  SparkInstance (..),
  fromCollisionParticle,
  fromPsiStormDamageParticle,
) where

import Prelude hiding (length)

import Data.Bool (bool)
import Data.Int (Int32)
import Data.Kind (Type)
import Data.List qualified as List
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4,
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Particles
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.Time

import Client.Constants qualified as CC
import Client.Theme
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SDF (unevenCapsule)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type SparkInstance :: GLKind -> Type
data SparkInstance k = SparkInstance
  { si_center :: GLData k Float2
  , si_radius :: GLData k Float
  , si_angle :: GLData k Float
  , si_tailLength :: GLData k Float
  , si_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (SparkInstance Cpu)
deriving anyclass instance GLAttribute (SparkInstance Cpu)
deriving anyclass instance GLSLInput (SparkInstance Gpu)

loadSpark :: TimedFastLogger -> GL (StandardInstancedVisual SparkInstance)
loadSpark logger =
  loadStandardVisual logger vertices compiledShaders CC.maxVisibleSparks

drawSpark ::
  StandardInstancedVisual SparkInstance ->
  StandardVParams SparkInstance ->
  GL ()
drawSpark = drawStandardVisual

--------------------------------------------------------------------------------

fromPsiStormDamageParticle ::
  Maybe ActorID ->
  Theme ->
  TeamView ->
  Time ->
  Particle PsiStormDamageParticle ->
  [SparkInstance Cpu]
fromPsiStormDamageParticle mViewPlayer theme teamView time particle =
  let psdp = particle_type particle
      damage = Fixed.toFloat $ psdp_damage psdp

      center = Fixed.toFloats $ psdp_pos psdp
      vel = Fixed.toFloats $ psdp_vel psdp
      nSparks = sparkCount (particle_startTime particle) center damage

      randAngle :: Int -> Float
      randAngle i =
        (* (2 * pi)) $
          rand $
            Float.dot2 (Float2 42.0865 63.811) center
              - 28.04 * fromIntegral i
      angles = map randAngle [1 .. nSparks]

      speeds = map (randSpeed damage center) [1 .. nSparks]
      dragFactors = map (randDragFactor center) [1 .. nSparks]

      ticksElapsed =
        fromIntegral $ getTicks $ time `diffTime` particle_startTime particle
      color = getDamageColor mViewPlayer theme teamView $ psdp_mPlayerColor psdp
  in  List.zipWith4
        (makeInstance ticksElapsed center vel)
        angles
        speeds
        dragFactors
        (repeat color)

fromCollisionParticle ::
  Maybe ActorID ->
  Theme ->
  TeamView ->
  Time ->
  Particle CollisionDamageParticle ->
  [SparkInstance Cpu]
fromCollisionParticle mViewPlayer theme teamView time particle =
  let cdp = particle_type particle
      damage = Fixed.toFloat $ cdp_damage cdp

      center = Fixed.toFloats $ cdp_pos (cdp :: CollisionDamageParticle)
      vel = Fixed.toFloats $ cdp_vel cdp
      nSparks = sparkCount (particle_startTime particle) center damage

      baseAng = (+ pi / 2) $ Float.unangle $ Fixed.toFloats $ cdp_direction cdp
      offsetAngle :: Int -> Float
      offsetAngle i =
        let random =
              rand $
                Float.dot2 (Float2 12.9898 78.233) center
                  + 91.25 * fromIntegral i
            randomSym = 2 * random - 1
            sign = signum randomSym
            mag = abs randomSym
            magShaped = mag * (0.05 + mag * (0.05 + mag * mag * 0.9))
            width = pi / 8
        in  sign * width * magShaped
      anglesHalf = map ((+ baseAng) . offsetAngle) [1 .. nSparks]
      angles = anglesHalf >>= \ang -> [ang, ang + pi]

      speeds = map (randSpeed damage center) [1 .. 2 * nSparks]
      dragFactors = map (randDragFactor center) [1 .. 2 * nSparks]

      ticksElapsed =
        fromIntegral $ getTicks $ time `diffTime` particle_startTime particle

      colors =
        cycle
          [ getDamageColor mViewPlayer theme teamView (cdp_mPlayerColor1 cdp)
          , getDamageColor mViewPlayer theme teamView (cdp_mPlayerColor2 cdp)
          , getDamageColor mViewPlayer theme teamView (cdp_mPlayerColor2 cdp)
          , getDamageColor mViewPlayer theme teamView (cdp_mPlayerColor1 cdp)
          ]
  in  List.zipWith4
        (makeInstance ticksElapsed center vel)
        angles
        speeds
        dragFactors
        colors

-- Helpers

sparkCount :: Time -> Float2 -> Float -> Int
sparkCount startTime pos damage =
  min 48 $
    let (damageFloor, damageFrac) = properFraction $ damage * 1.33
        timeFactor = 41.5016 * fromIntegral (getTime startTime `rem` 1009)
        posFactor = Float.dot2 (Float2 (-24.0516) 9.5223) pos
        x = rand $ timeFactor + posFactor
    in  damageFloor + bool 0 1 (damageFrac > x) -- Avoiding rebindable syntax

getDamageColor ::
  Maybe ActorID -> Theme -> TeamView -> Maybe PlayerColor -> Float3
getDamageColor mViewPlayer theme teamView =
  maybe (Float3 0 0 0) (viewPlayerColor theme mViewPlayer teamView)

rand :: Float -> Float
rand x =
  let r = 43758.5453123 * sin x
  in  r - fromIntegral @Int (floor r)

randSpeed :: Float -> Float2 -> Int -> Float
randSpeed damage pos i =
  let r1 =
        rand $ Float.dot2 (Float2 71.233 11.6868) pos + 3.36 * fromIntegral i
      r2 =
        rand $ Float.dot2 (Float2 (-30.561) 8.508) pos - 2.32 * fromIntegral i
  in  sqrt damage * (r1 + r2) -- triangle-shaped distribution on [0, 2]

randDragFactor :: Float2 -> Int -> Float
randDragFactor pos i =
  let r1 =
        rand $ Float.dot2 (Float2 (-19.406) (-12.915)) pos - 2.25 * fromIntegral i
      r2 =
        rand $ Float.dot2 (Float2 (-30.186) 10.575) pos - 2.25 * fromIntegral i
  in  1.1 + (r1 + r2 - 1) * 0.3

makeInstance ::
  Int32 ->
  Float2 ->
  Float2 ->
  Float ->
  Float ->
  Float ->
  Float3 ->
  SparkInstance Cpu
makeInstance ticksElapsedInt pos vel launchAngle launchSpeed dragFactor color =
  SparkInstance
    { si_center = pos + Float.map2 (* dist) initVelocityNorm
    , si_radius = 0.7 + 0.4 * opacity
    , si_angle = ang
    , si_tailLength = 6 * speed + 1.5
    , si_color = Util.fromRGB_A (brighten color) opacity
    }
  where
    initVelocity = vel + Float.map2 (* launchSpeed) (Float.angle launchAngle)
    (initSpeed, initVelocityNorm) = Float.normAndNormalized2 initVelocity
    ang = Float.unangle initVelocityNorm

    timeDilation = 2.0
    ticksElapsed = fromIntegral $ max 1 ticksElapsedInt
    t = ticksElapsed / timeDilation
    c = recip $ initSpeed * timeDilation
    dragCoeff = 0.05 * dragFactor
    dist = (log (dragCoeff * t + c) - log c) / dragCoeff
    speed = recip (dragCoeff * t + c) / timeDilation

    opacity = min 1 $ max 0 $ 2 * speed - 0.5
    brighten x = Float3 w w w + Float.map3 (* (1 - w)) x
      where
        w = 0.25 * opacity

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ square 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , kernelRadius :: Expr Float
  , tailLength :: Expr Float
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  SparkInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} SparkInstance{..} StandardUniforms{..} = do
  pos_y <- bind $ y_ position * si_radius
  pos_x <-
    bind $
      if x_ position ># 0
        then x_ position * si_radius
        else x_ position * si_tailLength

  s <- bind $ sin si_angle
  c <- bind $ cos si_angle
  pos_rotated <- bind $ vec2 (pos_x * c - pos_y * s, pos_x * s + pos_y * c)

  let screenPos =
        su_viewScale
          * (pos_rotated + si_center - su_viewCenter)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { pos = vec2 (pos_x, pos_y)
          , kernelRadius = si_radius
          , tailLength = si_tailLength
          , color = si_color
          }
  pure (screenPos, pipelineVars)
  where
    ifThenElse = ifThenElse'

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  sdf <- unevenCapsule tailLength kernelRadius 0 pos
  let fuzziness = 0.25
  opacity <- bind $ 1 - smoothstep (-fuzziness) fuzziness sdf
  pure $ vec4 (xyz_ color, opacity * w_ color)
