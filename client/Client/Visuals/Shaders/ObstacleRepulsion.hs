{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Client.Visuals.Shaders.ObstacleRepulsion (
  loadObstacleRepulsion,
  drawObstacleRepulsion,
  ObstacleRepulsionInstance (..),
) where

import Data.Coerce
import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SDF (segment)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type ObstacleRepulsionInstance :: GLKind -> Type
data ObstacleRepulsionInstance k = ObstacleRepulsionInstance
  { ori_modelScale :: GLData k Float
  , ori_modelTrans :: GLData k Float2
  , ori_direction :: GLData k Float2 -- unit vector
  , ori_opacity :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (ObstacleRepulsionInstance Cpu)
deriving anyclass instance GLAttribute (ObstacleRepulsionInstance Cpu)
deriving anyclass instance GLSLInput (ObstacleRepulsionInstance Gpu)

loadObstacleRepulsion ::
  TimedFastLogger ->
  GL (StandardInstancedVisual ObstacleRepulsionInstance)
loadObstacleRepulsion logger =
  loadStandardVisual logger vertices compiledShaders maxInstances
  where
    maxInstances = fromIntegral C.maxNumEntities

drawObstacleRepulsion ::
  StandardInstancedVisual ObstacleRepulsionInstance ->
  StandardVParams ObstacleRepulsionInstance ->
  GL ()
drawObstacleRepulsion = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 2.5

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  ObstacleRepulsionInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} ObstacleRepulsionInstance{..} StandardUniforms{..} = do
  rotatedPos <-
    bind $
      vec2
        ( y_ ori_direction * x_ position + x_ ori_direction * y_ position
        , -x_ ori_direction * x_ position + y_ ori_direction * y_ position
        )
  pure $
    let screenPos =
          su_viewScale
            * (ori_modelTrans - su_viewCenter + ori_modelScale *. rotatedPos)
            / vec2 (su_aspectRatio, 1)
        pipelineVars =
          PipelineVars
            { pos = position
            , baseOpacity = ori_opacity
            }
    in  (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , baseOpacity :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- segment (vec2 (-0.1, 0)) (vec2 (0.1, 0)) pos
  s <- bind $ r * 1.3
  opacity <- bind $ exp (-s * s)
  pure $ vec4 (1, 1, 1, opacity * baseOpacity)
