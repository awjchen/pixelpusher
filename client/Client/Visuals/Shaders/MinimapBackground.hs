{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The walls of the game world
module Client.Visuals.Shaders.MinimapBackground (
  loadMinimapBackground,
  drawMinimapBackground,
  MinimapBackgroundVisual,
  MinimapBackgroundVParams (..),
) where

import Prelude hiding (length)
import Prelude qualified as P

import Data.Coerce
import Data.Int (Int32)
import Data.Kind (Type)
import Data.Vector.Storable.Sized (Vector)
import GHC.Generics
import Graphics.GL.Types
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types (GLRecord)
import Client.Visuals.GL.Uniform
import Client.Visuals.GL.VertexArrayObject
import Client.Visuals.GL.Wrapped
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

data MinimapBackgroundVisual = MinimapBackgroundVisual
  { mbv_vao :: VertexArray
  , mbv_shaderProg :: ShaderProgram
  , mbv_nVertices :: GLsizei
  , mbv_uniformLocations :: UniformLocations (MinimapBackgroundVParams Cpu)
  }

type MinimapBackgroundVParams :: GLKind -> Type
data MinimapBackgroundVParams k = MinimapBackgroundVParams
  { mbvp_aspectRatio :: GLData k Float
  , mbvp_viewScale :: GLData k Float2
  , mbvp_viewCenter :: GLData k Float2
  , mbvp_visionCenters :: GLData k (Vector 32 Float2)
  , mbvp_visionCentersCount :: GLData k Int32
  , mbvp_visionRadius :: GLData k Float
  , mbvp_fuzziness :: GLData k Float
  , mbvp_visionColor :: GLData k Float4
  , mbvp_fogColor :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (MinimapBackgroundVParams Cpu)
deriving anyclass instance GLUniform (MinimapBackgroundVParams Cpu)
deriving anyclass instance GLSLInput (MinimapBackgroundVParams Gpu)

--------------------------------------------------------------------------------

drawMinimapBackground ::
  MinimapBackgroundVisual -> MinimapBackgroundVParams Cpu -> GL ()
drawMinimapBackground minimapBackground params =
  withVertexArray (mbv_vao minimapBackground) $
    withShaderProgram (mbv_shaderProg minimapBackground) $ do
      -- Set uniforms
      writeUniforms (mbv_uniformLocations minimapBackground) params

      -- Draw
      glDrawArrays GL_TRIANGLE_STRIP 0 (mbv_nVertices minimapBackground)

loadMinimapBackground :: TimedFastLogger -> GL MinimapBackgroundVisual
loadMinimapBackground logger = do
  vao <- setupVertexArrayObject vertices
  shaderProg <-
    makeShaderProgram
      logger
      (vertShaderGlsl compiledShaders)
      (fragShaderGlsl compiledShaders)
  uniforms <- getUniformLocations shaderProg
  pure $
    MinimapBackgroundVisual
      { mbv_vao = vao
      , mbv_shaderProg = shaderProg
      , mbv_nVertices = fromIntegral $ P.length vertices
      , mbv_uniformLocations = uniforms
      }

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.05

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {pos :: Expr Float2}
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  GLUnit Gpu ->
  MinimapBackgroundVParams Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{..} GLUnit MinimapBackgroundVParams{..} = do
  screenPos <-
    bind $
      mbvp_viewScale
        * (position - mbvp_viewCenter)
        / vec2 (mbvp_aspectRatio, 1)
  pure (screenPos, PipelineVars position)

fragShader :: MinimapBackgroundVParams Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader MinimapBackgroundVParams{..} PipelineVars{pos} = do
  fuzz <- bind $ 0.5 * mbvp_fuzziness
  let smoothstep' = smoothstep (-fuzz) fuzz
  r <- bind $ length pos

  worldBoundaryMask <- bind $ 1 - smoothstep' (r - 1)

  visionMask <- do
    let loopBody :: Expr Float -> Expr Float2 -> GLSL (Expr Float)
        loopBody minDistSq visionCenter = do
          dx <- bind $ x_ visionCenter - x_ pos
          dy <- bind $ y_ visionCenter - y_ pos
          distSq <- bind $ dx * dx + dy * dy
          bind $ min' distSq minDistSq
    visionRadiusSq <- bind $ mbvp_visionRadius * mbvp_visionRadius
    initDistSq <- bind $ 2 * visionRadiusSq
    distSq <-
      foldArrayN
        mbvp_visionCentersCount
        loopBody
        initDistSq
        mbvp_visionCenters
    dist <- bind $ sqrt distSq
    bind $ 1 - smoothstep' (dist - mbvp_visionRadius)

  color <-
    bind $ visionMask *. mbvp_visionColor + (1 - visionMask) *. mbvp_fogColor
  pure $ vec4 (xyz_ color, w_ color * worldBoundaryMask)
