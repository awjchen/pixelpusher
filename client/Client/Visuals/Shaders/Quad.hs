{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Generic rectangle
module Client.Visuals.Shaders.Quad (
  loadQuad,
  drawQuad,
  QuadInstance (..),
) where

import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type QuadInstance :: GLKind -> Type
data QuadInstance k = QuadInstance
  { qi_size :: GLData k Float2
  , qi_center :: GLData k Float2
  , qi_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (QuadInstance Cpu)
deriving anyclass instance GLAttribute (QuadInstance Cpu)
deriving anyclass instance GLSLInput (QuadInstance Gpu)

loadQuad :: TimedFastLogger -> GL (StandardInstancedVisual QuadInstance)
loadQuad logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawQuad ::
  StandardInstancedVisual QuadInstance ->
  StandardVParams QuadInstance ->
  GL ()
drawQuad = drawStandardVisual

maxInstances :: Word16
maxInstances = 16 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ square 1.0

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars {color :: Expr Float4}
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  QuadInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} QuadInstance{..} StandardUniforms{..} =
  pure $
    let screenPos =
          su_viewScale
            * (qi_center - su_viewCenter + qi_size * position)
            / vec2 (su_aspectRatio, 1)
    in  (screenPos, PipelineVars qi_color)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{color} = pure color
