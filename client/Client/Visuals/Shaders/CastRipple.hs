{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | An expanding circle
module Client.Visuals.Shaders.CastRipple (
  loadCastRipple,
  drawCastRipple,
  CastRippleInstance (..),
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Constants qualified as CC
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CastRippleInstance :: GLKind -> Type
data CastRippleInstance k = CastRippleInstance
  { cri_size :: GLData k Float
  , cri_center :: GLData k Float2
  , cri_color :: GLData k Float4
  , cri_ringWidth :: GLData k Float
  , cri_gradientWidth :: GLData k Float
  , cri_progress :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CastRippleInstance Cpu)
deriving anyclass instance GLAttribute (CastRippleInstance Cpu)
deriving anyclass instance GLSLInput (CastRippleInstance Gpu)

loadCastRipple ::
  TimedFastLogger -> GL (StandardInstancedVisual CastRippleInstance)
loadCastRipple logger =
  loadStandardVisual logger vertices compiledShaders CC.maxVisiblePings

drawCastRipple ::
  StandardInstancedVisual CastRippleInstance ->
  StandardVParams CastRippleInstance ->
  GL ()
drawCastRipple = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = map Position $ hexagon 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

vertShader ::
  Position Gpu ->
  CastRippleInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CastRippleInstance{..} StandardUniforms{..} =
  let screenPos =
        su_viewScale
          * (cri_center - su_viewCenter + cri_size *. position)
          / vec2 (su_aspectRatio, 1)
      pipelineVars =
        PipelineVars
          { size = cri_size
          , pos = position
          , color = cri_color
          , ringWidth = cri_ringWidth
          , gradientWidth = cri_gradientWidth
          , progress = cri_progress
          }
  in  pure (screenPos, pipelineVars)

data PipelineVars = PipelineVars
  { size :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  , ringWidth :: Expr Float
  , gradientWidth :: Expr Float
  , progress :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos * size

  outer_bound <- bind $ progress * size
  ring_inner_bound <- bind $ outer_bound - ringWidth
  gradient_inner_bound <- bind $ ring_inner_bound - gradientWidth

  let fuzziness = 0.25
  mask <- bind $ 1 - smoothstep (-fuzziness) fuzziness (r - outer_bound)

  widthFrac <-
    bind $ min' 1 $ max' 0 $ (r - gradient_inner_bound) / gradientWidth
  gradient <- bind $ widthFrac * widthFrac

  progress2 <- bind $ progress * progress
  opacity_factor <-
    bind $ min' 1 $ (1 - progress2) * inversesqrt (0.01 + progress * 16)

  opacity <- bind $ mask * gradient * opacity_factor
  pure $ vec4 (xyz_ color, w_ color * opacity)
