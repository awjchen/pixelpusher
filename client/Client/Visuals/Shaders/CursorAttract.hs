{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Cursor for attracting drones
--
-- TODO: Use uniforms rather than instances
module Client.Visuals.Shaders.CursorAttract (
  loadCursorAttract,
  drawCursorAttract,
  CursorAttractInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Coerce
import Data.Kind (Type)
import Data.Word (Word16)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CursorAttractInstance :: GLKind -> Type
data CursorAttractInstance k = CursorAttractInstance
  { cai_size :: GLData k Float
  , cai_center :: GLData k Float2
  , cai_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CursorAttractInstance Cpu)
deriving anyclass instance GLAttribute (CursorAttractInstance Cpu)
deriving anyclass instance GLSLInput (CursorAttractInstance Gpu)

--------------------------------------------------------------------------------

loadCursorAttract ::
  TimedFastLogger -> GL (StandardInstancedVisual CursorAttractInstance)
loadCursorAttract logger =
  loadStandardVisual logger vertices compiledShaders maxInstances

drawCursorAttract ::
  StandardInstancedVisual CursorAttractInstance ->
  StandardVParams CursorAttractInstance ->
  GL ()
drawCursorAttract = drawStandardVisual

maxInstances :: Word16
maxInstances = 2 -- arbitrary

--------------------------------------------------------------------------------

vertices :: [Position Cpu]
vertices = coerce $ hexagon 1.1

compiledShaders :: CompiledShaders Position
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { radius :: Expr Float
  , pos :: Expr Float2
  , color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  Position Gpu ->
  CursorAttractInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader Position{position} CursorAttractInstance{..} StandardUniforms{..} = do
  pos <- bind $ cai_size *. position
  screenPos <-
    bind $
      su_viewScale
        * (cai_center + pos - su_viewCenter)
        / vec2 (su_aspectRatio, 1)
  let radius = cai_size
      color = cai_color
  pure (screenPos, PipelineVars{..})

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  let fuzziness = 0.5
      sqrt34 = 0.8660254037844386
  r <- bind $ length pos

  radialFuzziness <- bind $ fuzziness / r
  angle_ <- bind $ atan2 (y_ pos) (x_ pos)
  angleMask <-
    bind $
      1
        - smoothstepCentered
          0.125
          radialFuzziness
          (abs (mod' (angle_ / (pi / 3)) 1 - 0.5))

  hexRadius <-
    bind $
      max' (abs (dot' pos (vec2 (0, 1)))) $
        max'
          (abs (dot' pos (vec2 (-sqrt34, 0.5))))
          (abs (dot' pos (vec2 (sqrt34, 0.5))))
  hexMask <-
    bind $ 1 - smoothstepCentered radius fuzziness hexRadius

  opacity <- bind $ hexMask * angleMask
  pure $ vec4 (xyz_ color, opacity * w_ color)

smoothstepCentered :: Expr Float -> Expr Float -> Expr Float -> Expr Float
smoothstepCentered cutoff radius =
  smoothstep (cutoff - radius) (cutoff + radius)
