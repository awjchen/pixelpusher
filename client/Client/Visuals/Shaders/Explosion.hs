{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | Explosion effect for overseer death and flag capture
module Client.Visuals.Shaders.Explosion (
  loadExplosion,
  drawExplosion,
  ExplosionInstance (..),
  fromOverseerDeathParticle,
  fromFlagExplodeParticle,
) where

import Prelude hiding (length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3,
  Float4,
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Time

import Client.Theme
import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.SharedVisuals (flagColors)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.TeamView
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

-- | Made of a lot of triangles
type ExplosionInstance :: GLKind -> Type
data ExplosionInstance k = ExplosionInstance
  { ei_center :: GLData k Float2
  , ei_offsetVel :: GLData k Float2
  , ei_color :: GLData k Float4
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (ExplosionInstance Cpu)
deriving anyclass instance GLAttribute (ExplosionInstance Cpu)
deriving anyclass instance GLSLInput (ExplosionInstance Gpu)

loadExplosion ::
  TimedFastLogger -> GL (StandardInstancedVisual ExplosionInstance)
loadExplosion logger =
  loadStandardVisual
    logger
    vertices
    compiledShaders
    (fromIntegral maxSparks * (nFlags + fromIntegral C.maxActorsPerGame))
  where
    nFlags = 2

drawExplosion ::
  StandardInstancedVisual ExplosionInstance ->
  StandardVParams ExplosionInstance ->
  GL ()
drawExplosion = drawStandardVisual

--------------------------------------------------------------------------------

maxSparks :: Int
maxSparks = max nOverseerSparks nFlagSparks

nOverseerSparks :: Int
nOverseerSparks = 160

nFlagSparks :: Int
nFlagSparks = 96

data ExplosionPreInstance = ExplosionPreInstance
  { epi_nSparks :: Int
  , epi_bodyRadius :: Float
  , epi_color :: Float3
  , epi_center :: Float2
  , epi_velocity :: Float2
  , epi_ticksElapsed :: Float
  , epi_dragCoeff :: Float
  }

fromOverseerDeathParticle ::
  Maybe ActorID ->
  Theme ->
  TeamView ->
  Time ->
  Particle OverseerDeathParticle ->
  [ExplosionInstance Cpu]
fromOverseerDeathParticle mViewPlayer theme teamView time particle =
  makeInstance
    ExplosionPreInstance
      { epi_nSparks = nOverseerSparks
      , epi_bodyRadius = Fixed.toFloat (odp_scale odp)
      , epi_color = viewPlayerColor theme mViewPlayer teamView $ odp_color odp
      , epi_center = Fixed.toFloats $ odp_pos odp
      , epi_velocity = Fixed.toFloats $ odp_vel odp
      , epi_ticksElapsed = fromIntegral $ diffTime time (particle_startTime particle)
      , epi_dragCoeff = 0.025
      }
  where
    odp = particle_type particle

fromFlagExplodeParticle ::
  Theme ->
  TeamView ->
  Time ->
  Particle FlagExplodeParticle ->
  [ExplosionInstance Cpu]
fromFlagExplodeParticle theme teamView time particle =
  makeInstance
    ExplosionPreInstance
      { epi_nSparks = nFlagSparks
      , epi_bodyRadius = Fixed.toFloat (fep_radius fep)
      , epi_color = flagColors theme $ viewTeamColor teamView (fep_flagTeam fep)
      , epi_center = Fixed.toFloats $ fep_pos fep
      , epi_velocity = Fixed.toFloats $ fep_vel fep
      , epi_ticksElapsed = fromIntegral $ diffTime time (particle_startTime particle)
      , epi_dragCoeff = 0.050
      }
  where
    fep = particle_type particle

makeInstance :: ExplosionPreInstance -> [ExplosionInstance Cpu]
makeInstance ExplosionPreInstance{..} =
  let secondsElapsed = epi_ticksElapsed / fromIntegral C.tickRate_hz

      timeDilation = 7
      timeDilation' = 8 -- I don't know what I was doing before, so I'm just going to duplicate this parameter
      t =
        epi_ticksElapsed / timeDilation
          + 0.001 -- avoid divison by zero
      a = 3
      s = 2 * t / (1 + a / t)
      ds_dt = let sq x = x * x in 2 * t * (t + 2 * a) / sq (t + a)
  in  flip map [1 .. epi_nSparks] $ \i ->
        let i' = fromIntegral i

            ang = 2 * pi * i' / fromIntegral epi_nSparks
            offset_ang =
              let r1 = rand (Float.map2 (subtract (11.942 * i')) epi_center)
                  r2 = rand (Float.map2 (+ (9.487 * i')) epi_center)
              in  pi / 6 * (r1 + r2 - 1)
            init_offset = Float.angle ang
            init_launch_dir = Float.angle $ ang + offset_ang

            launch_speed =
              let r1 = rand (Float.map2 (+ (20.671 * i')) epi_center)
                  r2 = rand (Float.map2 (+ (3.236 * i')) epi_center)
              in  15 * (r1 + r2)
            (init_speed, init_dir) =
              Float.normAndNormalized2 $
                epi_velocity + Float.map2 (* launch_speed) init_launch_dir

            c = recip $ init_speed * timeDilation'
            dragCoeff =
              let r1 = rand (Float.map2 (subtract (6.015 * i')) epi_center)
                  r2 = rand (Float.map2 (+ (8.110 * i')) epi_center)
              in  epi_dragCoeff * (1.0 + 0.5 * (r1 + r2 - 1))
            dist = (log (dragCoeff * s + c) - log c) / dragCoeff
            speed = recip (dragCoeff * s + c) * ds_dt / timeDilation'

            speedFloor = 0.25
            speedRange = 0.75
            speedOpacity = max 0 $ min 1 $ (speed - speedFloor) / speedRange
            baseColor = Util.fromRGB_A epi_color speedOpacity

            whiteness = exp (-7.2 * secondsElapsed)
            color = Float.map4 (\x -> whiteness + (1 - whiteness) * x) baseColor
            tailFactor = 4.5
        in  ExplosionInstance
              { ei_center =
                  epi_center
                    + Float.map2 (* epi_bodyRadius) init_offset
                    + Float.map2 (* dist) init_dir
              , ei_offsetVel = Float.map2 (* (tailFactor * speed)) init_dir
              , ei_color = color
              }

rand :: Float2 -> Float
rand (Float2 x y) =
  let r = 43758.5453123 * sin (x * 12.9898 + y * 78.233)
  in  r - fromIntegral @Int (floor r)

--------------------------------------------------------------------------------

vertices :: [LabelledPosition Cpu]
vertices =
  [ LabelledPosition
      { label = 1
      , position = Float2 1 0
      }
  , LabelledPosition
      { label = 0
      , position = Float2 1 (-1)
      }
  , LabelledPosition
      { label = 0
      , position = Float2 1 1
      }
  , LabelledPosition
      { label = 0
      , position = Float2 0 0
      }
  ]

compiledShaders :: CompiledShaders LabelledPosition
compiledShaders = compileShaders vertShader fragShader

newtype PipelineVars = PipelineVars
  { color :: Expr Float4
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  LabelledPosition Gpu ->
  ExplosionInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader
  LabelledPosition{label, position}
  ExplosionInstance{..}
  StandardUniforms{..} = do
    forwardDir <- bind $ normalize ei_offsetVel
    sideDir <- bind $ vec2 (-y_ forwardDir, x_ forwardDir)
    localPos <-
      bind $
        x_ position *. ei_offsetVel
          + y_ position *. sideDir
          + label *. forwardDir
    pure $
      let screenPos =
            su_viewScale
              * (localPos + ei_center - su_viewCenter)
              / vec2 (su_aspectRatio, 1)
          pipelineVars = PipelineVars ei_color
      in  (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{color} = pure color
