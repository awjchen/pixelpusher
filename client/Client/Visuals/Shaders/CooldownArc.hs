{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

-- | The cooldown arcs around your overseer
--
-- Note: This visual should not be used for arcs longer than pi radians
module Client.Visuals.Shaders.CooldownArc (
  loadCooldownArc,
  drawCooldownArc,
  CooldownArcInstance (..),
) where

import Prelude hiding (atan2, length)

import Data.Kind (Type)
import GHC.Generics
import System.Log.FastLogger

import Pixelpusher.Custom.Float (Float2, Float4)
import Pixelpusher.Game.Constants qualified as C

import Client.Visuals.GL.Attribute
import Client.Visuals.GL.DSL
import Client.Visuals.GL.Types
import Client.Visuals.GL.Wrapped (GL)
import Client.Visuals.StandardInstancedVisual
import Client.Visuals.Vertices

--------------------------------------------------------------------------------

type CooldownArcInstance :: GLKind -> Type
data CooldownArcInstance k = CooldownArcInstance
  { cai_center :: GLData k Float2
  , cai_arcRadius :: GLData k Float
  , cai_angleLow_degrees :: GLData k Float
  , cai_angleMid_degrees :: GLData k Float
  , cai_angleHigh_degrees :: GLData k Float
  , cai_colorLow :: GLData k Float4
  , cai_colorHigh :: GLData k Float4
  , cai_arcThickness :: GLData k Float
  , cai_bloomRadius :: GLData k Float
  }
  deriving stock (Generic)

deriving anyclass instance GLRecord (CooldownArcInstance Cpu)
deriving anyclass instance GLAttribute (CooldownArcInstance Cpu)
deriving anyclass instance GLSLInput (CooldownArcInstance Gpu)

loadCooldownArc ::
  TimedFastLogger -> GL (StandardInstancedVisual CooldownArcInstance)
loadCooldownArc logger =
  loadStandardVisual logger vertices compiledShaders C.maxNumEntities

drawCooldownArc ::
  StandardInstancedVisual CooldownArcInstance ->
  StandardVParams CooldownArcInstance ->
  GL ()
drawCooldownArc = drawStandardVisual

--------------------------------------------------------------------------------

vertices :: [LabelledPosition Cpu]
vertices = labelledAnnulus 32 1 1

compiledShaders :: CompiledShaders LabelledPosition
compiledShaders = compileShaders vertShader fragShader

data PipelineVars = PipelineVars
  { pos :: Expr Float2
  , arcRadius :: Expr Float
  , angleLow :: Expr Float
  , angleMid :: Expr Float
  , angleHigh :: Expr Float
  , colorLow :: Expr Float4
  , colorHigh :: Expr Float4
  , arcThickness :: Expr Float
  , bloomRadius :: Expr Float
  }
  deriving stock (Generic)
  deriving anyclass (GLSLStageLinkage)

vertShader ::
  LabelledPosition Gpu ->
  CooldownArcInstance Gpu ->
  StandardUniforms Gpu ->
  GLSL (Expr Float2, PipelineVars)
vertShader LabelledPosition{..} CooldownArcInstance{..} StandardUniforms{..} = do
  width <- bind $ 0.5 * cai_arcThickness + 3.5 * cai_bloomRadius
  radius <- bind $ cai_arcRadius + width * (2 * label - 1)
  localPosition <- bind $ position .* radius
  screenPos <-
    bind $
      su_viewScale
        * (localPosition + cai_center - su_viewCenter)
        / vec2 (su_aspectRatio, 1)

  degreesToRadians <- bind $ pi / 180
  let pipelineVars =
        PipelineVars
          { pos = localPosition
          , arcRadius = cai_arcRadius
          , angleLow =
              degreesToRadians * cai_angleLow_degrees
          , angleMid =
              let low = min' cai_angleLow_degrees cai_angleHigh_degrees
                  high = max' cai_angleLow_degrees cai_angleHigh_degrees
              in  degreesToRadians * clamp cai_angleMid_degrees low high
          , angleHigh = degreesToRadians * cai_angleHigh_degrees
          , colorLow = cai_colorLow
          , colorHigh = cai_colorHigh
          , arcThickness = cai_arcThickness
          , bloomRadius = cai_bloomRadius
          }
  pure (screenPos, pipelineVars)

fragShader :: StandardUniforms Gpu -> PipelineVars -> GLSL (Expr Float4)
fragShader _ PipelineVars{..} = do
  r <- bind $ length pos
  ang <- do
    ang_raw <- bind $ atan2 (y_ pos) (x_ pos)
    angleLower <- bind $ angleLow - pi
    bind $ angleLower + (ang_raw - angleLower) `mod'` (2 * pi)

  opacity <- do
    dr <- bind $ max' 0 $ abs (r - arcRadius) - 0.5 * arcThickness
    da <- do
      ang_mean <- bind $ 0.5 * (angleLow + angleHigh)
      ang_mean_radius <- bind $ 0.5 * abs (angleHigh - angleLow)
      bind $ arcRadius * max' 0 (abs (ang - ang_mean) - ang_mean_radius)
    r2 <- bind $ dr * dr + da * da
    bind $ exp $ negate $ 0.5 * r2 / (bloomRadius * bloomRadius)

  color <- do
    angleRange <- bind $ angleHigh - angleLow
    frac <- bind $ (ang - angleLow) / angleRange
    fracMid <- bind $ (angleMid - angleLow) / angleRange
    fuzziness <- bind $ 0.5 / (abs angleRange * r)
    weight <-
      bind $
        ifThenElse'
          (fracMid ># 0.999)
          0
          ( ifThenElse'
              (fracMid <# 0.001)
              1
              (smoothstep (-fuzziness) fuzziness (frac - fracMid))
          )
    bind $ mix colorLow colorHigh weight

  pure $ vec4 (xyz_ color, w_ color * opacity)
