module Client.Visuals.SceneDecorations (
  SceneDecorations (..),
  ScenePerspective (..),
  PlayerInfo (..),
  getPlayerPerspective,
  DisplayStats (..),
  DisplayHelp (..),
  ShowOSCursor (..),
  ShowMenuScene (..),
  DebugDisplayMode (..),
  nextDebugDisplayMode,
  prevDebugDisplayMode,
) where

import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Client.Menu.Pure
import Client.Theme
import Client.Visuals.TeamView

-- | The part of the rendering specification that captures information from
-- outside of the game state.
data SceneDecorations = SceneDecorations
  { sceneDeco_theme :: Theme
  , sceneDeco_perspective :: ScenePerspective
  , sceneDeco_teamPerspective :: Maybe Team -- TODO: rename
  , sceneDeco_teamView :: TeamView
  , sceneDeco_renderStatsOverlay :: DisplayStats
  , sceneDeco_renderHelpOverlay :: DisplayHelp
  , sceneDeco_roomID :: Maybe Int
  , sceneDeco_networkLatency :: Maybe Int
  , sceneDeco_elaspedClientTime :: Ticks
  , sceneDeco_showTutorial :: Bool
  , sceneDeco_showMenu :: ShowMenuScene
  , sceneDeco_showOSCursor :: ShowOSCursor
  , sceneDeco_isFramePredicted :: Bool
  , sceneDeco_enableScreenShake :: Bool
  , sceneDeco_enableTutorialMode :: Bool
  , sceneDeco_showDebugInfo :: DebugDisplayMode
  , sceneDeco_spatialAudioStatus :: Bool
  , sceneDeco_showUI :: Bool
  , sceneDeco_showHUD :: Bool
  , sceneDeco_showPlayerTags :: Bool
  , sceneDeco_showCursor :: Bool
  , sceneDeco_showFlagCaptureFlash :: Bool
  }

data ScenePerspective
  = PlayerPerspective PlayerInfo
  | FreePerspective
  | GlobalPerspective

data PlayerInfo = PlayerInfo
  { pi_actorID :: ActorID
  , pi_player :: Player
  , pi_playerView :: PlayerView
  }

getPlayerPerspective :: ScenePerspective -> Maybe PlayerInfo
getPlayerPerspective = \case
  PlayerPerspective p -> Just p
  FreePerspective -> Nothing
  GlobalPerspective -> Nothing

data DisplayStats = DisplayStats | HideStats

data DisplayHelp
  = HideHelp
  | ShowHelpInstructions
  | ShowHelpVisuals
  | ShowHelpDetailedMechanics
  | ShowHelpPlayerClasses
  | ShowHelpThemes

data ShowOSCursor = ShowOSCursor | DoNotShowOSCursor

data ShowMenuScene where
  NoMenuScene :: ShowMenuScene
  ShowMenuScene :: model -> MenuState widgets model -> ShowMenuScene

data DebugDisplayMode
  = DDM_None
  | DDM_Overseers
  | DDM_Drones
  | DDM_PsiStorm
  | DDM_Goals
  | DDM_MovementPotentials
  | DDM_DroneMovementPotentials
  deriving stock (Eq, Ord, Enum, Bounded)

nextDebugDisplayMode :: DebugDisplayMode -> DebugDisplayMode
nextDebugDisplayMode x = if x == maxBound then minBound else succ x

prevDebugDisplayMode :: DebugDisplayMode -> DebugDisplayMode
prevDebugDisplayMode x = if x == minBound then maxBound else pred x
