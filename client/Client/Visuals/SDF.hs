{-# LANGUAGE RebindableSyntax #-}

module Client.Visuals.SDF (
  unevenCapsule,
  isoscelesTrapezoid,
  segment,
) where

import Prelude hiding (length)

import Pixelpusher.Custom.Float (Float2)

import Client.Visuals.GL.DSL

-- The signed distance function for this uneven capsule is taken from
-- https://iquilezles.org/articles/distfunctions2d/, except this implementation
-- extends to the left (-x), rather than up (+y).
unevenCapsule ::
  Expr Float -> Expr Float -> Expr Float -> Expr Float2 -> GLSL (Expr Float)
unevenCapsule h r1 r2 pos = do
  p <- bind $ vec2 (x_ pos, abs (y_ pos)) -- vertical symmetry
  y <- bind $ (r1 - r2) / h
  x <- bind $ sqrt $ 1 - y * y
  k <- bind $ dot' p (vec2 (-x, -y))
  bind $
    if k <# 0
      then length p - r1 -- around the kernel
      else
        if k <# (x * h)
          then dot' p (vec2 (-y, x)) - r1 -- from the line
          else length (p + vec2 (h, 0)) - r2 -- around the tip of the tail
  where
    ifThenElse = ifThenElse'

-- The signed distance function for this uneven capsule is taken from
-- https://iquilezles.org/articles/distfunctions2d/.
isoscelesTrapezoid ::
  Expr Float -> Expr Float -> Expr Float -> Expr Float2 -> GLSL (Expr Float)
isoscelesTrapezoid width1 width2 height rawPos = do
  k1 <- bind $ vec2 (width2, height)
  k2 <- bind $ vec2 (width2 - width1, 2 * height)
  pos <- bind $ vec2 (abs (x_ rawPos), y_ rawPos)
  ca <-
    bind $
      vec2
        ( x_ pos - min' (x_ pos) (if y_ pos <# 0 then width1 else width2)
        , abs (y_ pos) - height
        )
  cb <- bind $ pos - k1 + k2 .* clamp (dot' (k1 - pos) k2 / dot' k2 k2) 0 1
  s <- bind $ if (x_ cb <# 0) `and'` (y_ ca <# 0) then -1 else 1
  pure $ s * sqrt (min' (dot' ca ca) (dot' cb cb))
  where
    ifThenElse = ifThenElse'

segment :: Expr Float2 -> Expr Float2 -> Expr Float2 -> GLSL (Expr Float)
segment p1 p2 pos = do
  v1p <- bind $ pos - p1
  v12 <- bind $ p2 - p1
  h <- bind $ clamp (dot' v1p v12 / dot' v12 v12) 0 1
  bind $ length $ v1p - v12 .* h
