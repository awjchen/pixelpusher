module Client.Visuals.CameraInterpolation (
  overseerCameraInterpolation,
) where

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.Float qualified as Float

import Pixelpusher.Game.PlayerStatus

-- | Update the camera to track an overseer. Should be called once per frame.
overseerCameraInterpolation ::
  Float -> OverseerView -> (Float2, Float2) -> (Float2, Float2)
overseerCameraInterpolation ticksPerFrame overseerView =
  case overseerView of
    OV_Alive OverseerViewAlive{ova_pos, ova_vel} ->
      cameraInterpolation
        ticksPerFrame
        (Fixed.toFloats ova_pos)
        (Fixed.toFloats ova_vel)
    OV_Dead OverseerViewDead{ovd_lastPos} ->
      cameraInterpolation
        ticksPerFrame
        (Fixed.toFloats ovd_lastPos)
        0

-- | Update the camera to track a target. Should be called once per frame.
cameraInterpolation ::
  Float -> Float2 -> Float2 -> (Float2, Float2) -> (Float2, Float2)
cameraInterpolation ticksPerFrame targetPos targetVel (cameraPos, cameraVel)
  -- Immediately jump to target if it is too far away
  | Float.distance2 targetPos cameraPos > cutoff = (targetPos, targetVel)
  | otherwise =
      let vel = w * targetVel + w_compl * cameraVel
          pos =
            w * targetPos
              + w_compl * (cameraPos + Float.map2 (* ticksPerFrame) vel)
      in  (pos, vel)
  where
    w = Float.map2 (* ticksPerFrame) 1.5625e-2 -- a lazy approximation
    w_compl = 1.0 - w
    cutoff = 320.0
