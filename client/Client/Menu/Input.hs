module Client.Menu.Input (
  MenuInputEvent (..),
  MenuInputQueue,
  newQueue,
  pushQueue,
  flushQueue,
) where

import Data.IORef
import Data.Text (Text)

data MenuInputEvent
  = MenuEnter
  | MenuEscape
  | MenuUp
  | MenuDown
  | MenuLeft
  | MenuRight
  | MenuCharInput Char
  | MenuCharDelete
  | MenuPasteText Text

-- | Simple unbounded FIFO queue for menu inputs
--
-- TODO: use bounded queue
newtype MenuInputQueue = MenuInputQueue (IORef [MenuInputEvent])

newQueue :: IO MenuInputQueue
newQueue = MenuInputQueue <$> newIORef []

pushQueue :: MenuInputQueue -> MenuInputEvent -> IO ()
pushQueue (MenuInputQueue queueRef) event =
  atomicModifyIORef' queueRef $ \events ->
    (event : events, ())

flushQueue :: MenuInputQueue -> IO [MenuInputEvent]
flushQueue (MenuInputQueue queueRef) =
  reverse <$> atomicModifyIORef' queueRef ([],)
