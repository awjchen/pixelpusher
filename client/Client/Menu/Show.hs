module Client.Menu.Show (
  MenuShow (..),
) where

import Data.Text (Text)

import Pixelpusher.Game.PlayerClass

class MenuShow a where
  menuShow :: a -> Text

-- Instances that would otherwise be orphan instances

instance MenuShow PlayerClass where
  menuShow = \case
    Templar -> templarText
    Zealot -> zealotText
