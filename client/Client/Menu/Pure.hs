{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Client.Menu.Pure (
  MenuItemType (..),
  MenuItem (..),
  ButtonConfig (..),
  ToggleConfig (..),
  IntSliderConfig (..),
  EnumChoiceConfig (..),
  TextInputConfig (..),
  KeybindConfig (..),
  MenuShow (..),
  MenuState,
  initialMenuState,
  getMenuTitle,
  resetMenuNavigation,
  Selected (..),
  Visibility (..),
  renderMenu,
  MenuUpdateEvent (..),
  handleMenuInput,
) where

import Data.Bifunctor (first)
import Data.Generics.Product.Fields
import Data.Kind (Type)
import Data.List (intersperse)
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Text (Text)
import Data.Text qualified as Text
import GHC.Generics (Generic)
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Client.Config.Keybindings (keyToText)
import Client.Menu.Input (MenuInputEvent (..))
import Client.Menu.Show
import Client.Menu.Zipper (Zipper (..))
import Client.Menu.Zipper qualified as Zipper

--------------------------------------------------------------------------------
-- Configuration

data MenuItemType
  = Button
  | Toggle
  | IntSlider
  | TextInput
  | EnumChoice Type
  | Keybind

type MenuItem :: ids -> Type -> Type
data MenuItem ids model where
  ButtonWidget ::
    ids 'Button -> ButtonConfig -> MenuItem ids model
  ToggleWidget ::
    ids 'Toggle -> Lens' model Bool -> ToggleConfig -> MenuItem ids model
  IntSliderWidget ::
    ids 'IntSlider -> Lens' model Int -> IntSliderConfig -> MenuItem ids model
  -- | Displays all choices on one line of text, so only works if there are a
  -- small number of choices.
  EnumChoiceWidget ::
    (Bounded a, Enum a, Eq a, MenuShow a) =>
    ids ('EnumChoice a) ->
    Lens' model a ->
    EnumChoiceConfig ->
    MenuItem ids model
  TextInputWidget ::
    ids 'TextInput -> Lens' model Text -> TextInputConfig -> MenuItem ids model
  -- | Special-purpose widget for setting custom keybindings
  KeybindWidget ::
    ids 'Keybind ->
    (model -> GLFW.Key) ->
    KeybindConfig ->
    MenuItem ids model

data TextInputWidget' ids model
  = TextInputWidget' (ids 'TextInput) (Lens' model Text) TextInputConfig

menuItemId :: MenuItem ids model -> SomeWidgetId ids
menuItemId = \case
  ButtonWidget widgetId _ -> SomeWidgetId widgetId
  ToggleWidget widgetId _ _ -> SomeWidgetId widgetId
  IntSliderWidget widgetId _ _ -> SomeWidgetId widgetId
  EnumChoiceWidget widgetId _ _ -> SomeWidgetId widgetId
  TextInputWidget widgetId _ _ -> SomeWidgetId widgetId
  KeybindWidget widgetId _ _ -> SomeWidgetId widgetId

newtype ButtonConfig = ButtonConfig
  { bc_label :: Text
  }

newtype ToggleConfig = ToggleConfig
  { tc_label :: Text
  }

data IntSliderConfig = IntSliderConfig
  { isc_label :: Text
  , isc_min :: Int
  , isc_max :: Int
  , isc_step :: Int
  }

newtype EnumChoiceConfig = EnumChoiceConfig
  { esc_label :: Text
  }

data TextInputConfig = TextInputConfig
  { tic_label :: Text
  , tic_maxLength :: Int
  , tic_permittedChars :: Char -> Bool
  }

newtype KeybindConfig = KeybindConfig
  { kc_label :: Text
  }

--------------------------------------------------------------------------------
-- State

-- TODO: Could merge ms_zipper and ms_screen into one type for more safety

data MenuState ids model = MenuState
  { ms_title :: Text
  , ms_zipper :: Zipper (MenuItem ids model)
  , ms_screen :: MenuScreen ids model
  }
  deriving (Generic)

data MenuScreen ids model
  = MenuScreen
  | TextInputScreen (TextInputWidget' ids model)

getMenuTitle :: MenuState ids model -> Text
getMenuTitle = ms_title

initialMenuState ::
  Text -> NonEmpty (MenuItem ids model) -> MenuState ids model
initialMenuState title menuItems =
  MenuState
    { ms_title = title
    , ms_zipper = Zipper.fromNonEmpty menuItems
    , ms_screen = MenuScreen
    }

resetMenuNavigation :: MenuState ids model -> MenuState ids model
resetMenuNavigation menuState =
  menuState
    { ms_zipper = Zipper.focusFirst (ms_zipper menuState)
    , ms_screen = MenuScreen
    }

--------------------------------------------------------------------------------
-- Rendering

data Selected = Selected | NotSelected
  deriving stock (Eq)

data Visibility = Visible | Shadowed | Invisible

shadowVisibility :: Visibility -> Visibility
shadowVisibility = \case
  Visible -> Shadowed
  Shadowed -> Shadowed
  Invisible -> Invisible

renderMenu ::
  model -> MenuState ids model -> [(Selected, NonEmpty (Visibility, Text))]
renderMenu model menuState =
  case ms_screen menuState of
    MenuScreen ->
      let Zipper{..} = ms_zipper menuState
      in  map ((NotSelected,) . renderMenuItem model NotSelected) (reverse z_above)
            <> [(Selected, renderMenuItem model Selected z_focused)]
            <> map ((NotSelected,) . renderMenuItem model NotSelected) z_below
    TextInputScreen (TextInputWidget' _ lens_ TextInputConfig{..}) ->
      let Zipper{..} = ms_zipper menuState
          textInputLine =
            (Selected,) $
              let text = view lens_ model
              in  (Visible, tic_label)
                    :| [ (Shadowed, " :: ")
                       , (Invisible, "[")
                       , (Visible, text)
                       , (Shadowed, "_")
                       ]
      in  map ((NotSelected,) . fmap (first shadowVisibility) . renderMenuItem model NotSelected) (reverse z_above)
            <> [textInputLine]
            <> map ((NotSelected,) . fmap (first shadowVisibility) . renderMenuItem model NotSelected) z_below

renderMenuItem ::
  model -> Selected -> MenuItem ids model -> NonEmpty (Visibility, Text)
renderMenuItem model _selected = \case
  ButtonWidget _ buttonConfig ->
    pure (Visible, "[" <> bc_label buttonConfig <> "]")
  ToggleWidget _ lens_ toggleConfig ->
    let label = tc_label toggleConfig
        enabled = view lens_ model
        offVisibility = if enabled then Shadowed else Visible
        onVisibility = if enabled then Visible else Shadowed
    in  (Visible, label)
          :| [ (Shadowed, " :: ")
             , (offVisibility, "off")
             , (Shadowed, " | ")
             , (onVisibility, "on")
             ]
  IntSliderWidget _ lens_ IntSliderConfig{..} ->
    let value = view lens_ model
        leftArrowVisibility
          | value > isc_min = Visible
          | otherwise = Shadowed
        rightArrowVisibility
          | value < isc_max = Visible
          | otherwise = Shadowed
        renderedValue = tshow value
        padding = Text.pack $ replicate (max 0 (maxWidth - valueWidth)) '0'
          where
            valueWidth = Text.length renderedValue
            maxWidth = length $ show isc_max
    in  (Visible, isc_label)
          :| [ (Shadowed, " :: ")
             , (leftArrowVisibility, "< ")
             , (Invisible, padding)
             , (Visible, renderedValue)
             , (rightArrowVisibility, " > ")
             ]
  TextInputWidget _ lens_ TextInputConfig{..} ->
    let text = view lens_ model
    in  (Visible, tic_label)
          :| [ (Shadowed, " :: ")
             , (Visible, "[" <> text <> "]")
             ]
  EnumChoiceWidget _ lens_ EnumChoiceConfig{..} ->
    let value = view lens_ model
        values =
          flip map [minBound .. maxBound] $ \val ->
            if val == value
              then (Visible, menuShow val)
              else (Shadowed, menuShow val)
    in  (:|) (Visible, esc_label) $
          (Shadowed, " :: ")
            : intersperse (Shadowed, " | ") values
  KeybindWidget _ getter KeybindConfig{..} ->
    let key = getter model
        keyText = "[" <> keyToText key <> "]"
    in  (:|)
          (Visible, kc_label)
          [ (Shadowed, " :: ")
          , (Visible, keyText)
          ]

--------------------------------------------------------------------------------
-- Update

data MenuUpdateEvent ids where
  Escape ::
    MenuUpdateEvent ids
  Enter ::
    MenuUpdateEvent ids
  MenuScrollUp ::
    SomeWidgetId ids ->
    MenuUpdateEvent ids
  MenuScrollDown ::
    SomeWidgetId ids ->
    MenuUpdateEvent ids
  ButtonPressed ::
    ids 'Button ->
    MenuUpdateEvent ids
  ToggleSwitched ::
    ids 'Toggle ->
    Bool -> -- new value
    MenuUpdateEvent ids
  IntSliderChanged ::
    ids 'IntSlider ->
    Int -> -- new value
    MenuUpdateEvent ids
  EnumChoiceChanged ::
    ids ('EnumChoice a) ->
    a -> -- new value
    MenuUpdateEvent ids
  TextChanged ::
    ids 'TextInput ->
    Text -> -- new value
    MenuUpdateEvent ids
  KeybindTriggered ::
    ids 'Keybind ->
    MenuUpdateEvent ids
  FocusTextInput ::
    ids 'TextInput ->
    MenuUpdateEvent ids
  UnfocusTextInput ::
    ids 'TextInput ->
    MenuUpdateEvent ids

type SomeWidgetId :: ids -> Type
data SomeWidgetId ids where
  SomeWidgetId :: ids a -> SomeWidgetId ids

handleMenuInput ::
  MenuInputEvent ->
  model ->
  MenuState ids model ->
  Maybe (model, MenuState ids model, MenuUpdateEvent ids)
handleMenuInput inputEvent model menuState =
  leftToMaybe $ do
    case ms_screen menuState of
      MenuScreen -> do
        let MenuState{..} = menuState
            selectedWidget = z_focused ms_zipper
        handleInputWidgets inputEvent model menuState selectedWidget
        first (uncurry ((,,) model)) $
          handleInputMenu menuState inputEvent
        first (model,menuState,) $
          handleInputGlobal inputEvent
      TextInputScreen textInputWidget' ->
        handleTextInputScreen textInputWidget' inputEvent model menuState

leftToMaybe :: Either a b -> Maybe a
leftToMaybe = \case
  Left a -> Just a
  Right _ -> Nothing

handleInputWidgets ::
  MenuInputEvent ->
  model ->
  MenuState ids model ->
  MenuItem ids model ->
  Either (model, MenuState ids model, MenuUpdateEvent ids) ()
handleInputWidgets inputEvent model menuState = \case
  ButtonWidget buttonId _buttonConfig ->
    case inputEvent of
      MenuEnter -> Left (model, menuState, ButtonPressed buttonId)
      _ -> Right ()
  ToggleWidget toggleId lens_ _ ->
    preserveMenuState $
      case inputEvent of
        MenuEnter ->
          let newVal = not $ view lens_ model
          in  Left (set lens_ newVal model, ToggleSwitched toggleId newVal)
        MenuLeft
          | view lens_ model ->
              let newVal = False
              in  Left (set lens_ newVal model, ToggleSwitched toggleId newVal)
          | otherwise ->
              Right ()
        MenuRight
          | view lens_ model ->
              Right ()
          | otherwise ->
              let newVal = True
              in  Left (set lens_ newVal model, ToggleSwitched toggleId newVal)
        _ ->
          Right ()
  IntSliderWidget intSliderId lens_ IntSliderConfig{..} ->
    preserveMenuState $
      case inputEvent of
        MenuLeft
          | val > isc_min ->
              let newVal = max isc_min (val - isc_step)
              in  Left (set lens_ newVal model, IntSliderChanged intSliderId newVal)
          | otherwise ->
              Right ()
        MenuRight
          | val < isc_max ->
              let newVal = min isc_max (val + isc_step)
              in  Left (set lens_ newVal model, IntSliderChanged intSliderId newVal)
          | otherwise ->
              Right ()
        _ -> Right ()
    where
      val = view lens_ model
  EnumChoiceWidget enumChoiceId lens_ EnumChoiceConfig{} ->
    preserveMenuState $
      case inputEvent of
        MenuLeft
          | val == minBound ->
              Right ()
          | otherwise ->
              let newVal = pred val
                  newModel = set lens_ newVal model
                  update = EnumChoiceChanged enumChoiceId newVal
              in  Left (newModel, update)
        MenuRight
          | val == maxBound ->
              Right ()
          | otherwise ->
              let newVal = succ val
                  newModel = set lens_ newVal model
                  update = EnumChoiceChanged enumChoiceId newVal
              in  Left (newModel, update)
        _ -> Right ()
    where
      val = view lens_ model
  TextInputWidget textInputId lens_ textInputConfig ->
    case inputEvent of
      MenuEnter ->
        Left
          ( model
          , menuState
              & field @"ms_screen"
              .~ TextInputScreen (TextInputWidget' textInputId lens_ textInputConfig)
          , FocusTextInput textInputId
          )
      _ -> Right ()
  KeybindWidget keySelectId _lens_ KeybindConfig{} ->
    -- Dummy widget handler for the special-purpose keybind widget; model
    -- updates do not happen here, but in the user event handler.
    case inputEvent of
      MenuEnter ->
        Left (model, menuState, KeybindTriggered keySelectId)
      _ -> Right ()
  where
    preserveMenuState =
      first (\(newModel, updateEvent) -> (newModel, menuState, updateEvent))

handleInputMenu ::
  MenuState ids model ->
  MenuInputEvent ->
  Either (MenuState ids model, MenuUpdateEvent ids) ()
handleInputMenu menuState = \case
  MenuEnter -> Right ()
  MenuEscape -> Right ()
  MenuUp ->
    Left
      ( over (field @"ms_zipper") Zipper.scrollUp menuState
      , MenuScrollUp (menuItemId menuItem)
      )
  MenuDown ->
    Left
      ( over (field @"ms_zipper") Zipper.scrollDown menuState
      , MenuScrollDown (menuItemId menuItem)
      )
  MenuLeft -> Right ()
  MenuRight -> Right ()
  MenuCharInput{} -> Right ()
  MenuCharDelete -> Right ()
  MenuPasteText{} -> Right ()
  where
    menuItem = z_focused $ ms_zipper menuState

handleInputGlobal :: MenuInputEvent -> Either (MenuUpdateEvent ids) ()
handleInputGlobal = \case
  MenuEnter -> Left Enter
  MenuEscape -> Left Escape
  _ -> Right ()

handleTextInputScreen ::
  TextInputWidget' ids model ->
  MenuInputEvent ->
  model ->
  MenuState ids model ->
  Either (model, MenuState ids model, MenuUpdateEvent ids) ()
handleTextInputScreen textInputWidget' inputEvent model menuState =
  case inputEvent of
    MenuCharInput c ->
      preserveMenuState $
        if
          | not (tic_permittedChars c) ->
              Right ()
          | Text.length val >= tic_maxLength ->
              Right ()
          | otherwise ->
              let newVal = Text.snoc val c
              in  Left (set lens_ newVal model, TextChanged textInputId newVal)
    MenuCharDelete ->
      preserveMenuState $
        case Text.unsnoc val of
          Nothing -> Right ()
          Just (newText, _) ->
            Left (set lens_ newText model, TextChanged textInputId newText)
    MenuPasteText pastedText ->
      preserveMenuState $
        let newText = Text.take tic_maxLength $ val <> pastedText
        in  Left (set lens_ newText model, TextChanged textInputId newText)
    MenuEnter ->
      leaveTextInputScreen
    MenuEscape ->
      leaveTextInputScreen
    _ -> Right ()
  where
    TextInputWidget' textInputId lens_ TextInputConfig{..} = textInputWidget'
    val = view lens_ model
    preserveMenuState =
      first (\(newModel, updateEvent) -> (newModel, menuState, updateEvent))
    leaveTextInputScreen =
      Left
        ( model
        , menuState & field @"ms_screen" .~ MenuScreen
        , UnfocusTextInput textInputId
        )

-- Helper
tshow :: (Show a) => a -> Text
tshow = Text.pack . show
