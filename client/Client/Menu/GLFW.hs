{-# LANGUAGE MultiWayIf #-}

module Client.Menu.GLFW (
  charCallback,
  keyCallback,
) where

import Data.Text qualified as Text
import Graphics.UI.GLFW hiding (KeyCallback)
import Graphics.UI.GLFW qualified as GLFW

import Client.Config.Keybindings (Keybindings (..))
import Client.Menu.Input (MenuInputEvent (..), MenuInputQueue)
import Client.Menu.Input qualified as Menu
import Client.Visuals.GLFW.KeyCallback

charCallback :: MenuInputQueue -> GLFW.CharCallback
charCallback inputQueue _window char =
  Menu.pushQueue inputQueue (MenuCharInput char)

keyCallback :: MenuInputQueue -> Keybindings -> KeyCallback
keyCallback inputQueue keybindings =
  KeyCallback $ \window key _scanCode keySt mods ->
    let shift = GLFW.modifierKeysShift mods
        ctrl = GLFW.modifierKeysControl mods
        firstPress = keySt == GLFW.KeyState'Pressed
    in  if
          | keySt == GLFW.KeyState'Released ->
              Nothing
          -- Enter
          | key == Key'Enter && firstPress ->
              Just $ Menu.pushQueue inputQueue MenuEnter
          | key == Key'Space && firstPress ->
              Just $ Menu.pushQueue inputQueue MenuEnter
          -- Escape
          | key == Key'Escape && firstPress ->
              Just $ Menu.pushQueue inputQueue MenuEscape
          -- Up/Down
          | key == Key'Up ->
              Just $ Menu.pushQueue inputQueue MenuUp
          | key == Key'Down ->
              Just $ Menu.pushQueue inputQueue MenuDown
          | key == Key'Tab ->
              if shift
                then Just $ Menu.pushQueue inputQueue MenuUp
                else Just $ Menu.pushQueue inputQueue MenuDown
          -- Left/Right
          | key == Key'Left ->
              Just $ Menu.pushQueue inputQueue MenuLeft
          | key == Key'Right ->
              Just $ Menu.pushQueue inputQueue MenuRight
          -- Text
          | key == Key'Backspace ->
              Just $ Menu.pushQueue inputQueue MenuCharDelete
          | key == Key'V && ctrl ->
              Just $ do
                GLFW.getClipboardString window >>= \case
                  Nothing -> pure ()
                  Just pastedText ->
                    Menu.pushQueue inputQueue $
                      MenuPasteText (Text.pack pastedText)
          -- Custom keybindings, checked after all other keybindings in order
          -- to guarantee that menus are always navigable.
          | key == kb_overseerUp keybindings ->
              Just $ Menu.pushQueue inputQueue MenuUp
          | key == kb_overseerDown keybindings ->
              Just $ Menu.pushQueue inputQueue MenuDown
          | key == kb_overseerLeft keybindings ->
              Just $ Menu.pushQueue inputQueue MenuLeft
          | key == kb_overseerRight keybindings ->
              Just $ Menu.pushQueue inputQueue MenuRight
          --
          | otherwise ->
              Nothing
