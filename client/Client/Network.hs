{-# LANGUAGE OverloadedStrings #-}

module Client.Network (
  Connection,
  connect,
  ClientSendHandles (..),
  runClient,
) where

import Control.Concurrent (forkIO)
import Control.Concurrent.Async
import Control.Concurrent.Chan.Unagi qualified as UC
import Control.Concurrent.MVar
import Control.Exception
import Control.Monad (forever)
import Data.Functor (void)
import Data.IORef
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Text.Encoding (decodeUtf8)
import Network.Socket hiding (connect)
import Network.Socket qualified
import Network.Socket.ByteString qualified as BS
import System.Log.FastLogger

import Client.Logging

import Pixelpusher.Custom.Clock (getMonotonicTimeNSecInt, threadDelayNSec)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Debug
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Protocol (ClientInitialMsg (..), ClientRole (..))
import Pixelpusher.Game.Version qualified as Ver
import Pixelpusher.Network.Constants qualified as NC
import Pixelpusher.Network.Protocol
import Pixelpusher.Network.TCP.Protocol

--------------------------------------------------------------------------------

data Connection = Connection
  { conn_tcpConn :: TCPConnection
  , conn_udpSock :: Socket
  }

connect ::
  forall a.
  TimedFastLogger ->
  HostName ->
  Int -> -- Port
  ClientRole PlayerName ->
  Maybe Int -> -- Room
  (DebugMode -> Connection -> IO a) ->
  IO a
connect logger server port clientRole roomNo action = do
  writeLog logger $
    "Connecting via TCP to "
      <> toLogStr server
      <> " (port "
      <> toLogStr (show port)
      <> ")."

  withTcpClient server (show port) $ \ipVersion tcpConn -> do
    -- Send version
    tcpSendData tcpConn $ Ver.toByteString C.version

    -- Return token via UDP
    udpAuthToken <-
      either error id . Serialize.decode @Int <$> tcpRecvData tcpConn
    let payload =
          Serialize.encode @(ClientUDPMsg ()) $
            ClientUDPMsg_AuthToken udpAuthToken
    udpSocket <-
      let addrFamily =
            case ipVersion of
              Ipv4 -> AF_INET
              Ipv6 -> AF_INET6
       in connectToServerUDP addrFamily server port
    BS.sendAll udpSocket payload

    -- Receive debug mode flag
    debugMode <-
      either error id . Serialize.decode @DebugMode <$> tcpRecvData tcpConn

    -- Send client parameters
    tcpSendData tcpConn $
      Serialize.encode
        ClientInitialMsg
          { cim_role = getPlayerName <$> clientRole
          , cim_mRoomID = roomNo
          }

    action
      debugMode
      Connection
        { conn_tcpConn = tcpConn
        , conn_udpSock = udpSocket
        }

connectToServerUDP :: Family -> HostName -> Int -> IO Socket
connectToServerUDP addrFamily' server port = do
  let hints =
        defaultHints
          { addrSocketType = Datagram
          , addrFamily = addrFamily'
          }
  addr <-
    List.NonEmpty.head <$> getAddrInfo (Just hints) (Just server) (Just (show port))
  sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
  Network.Socket.connect sock (addrAddress addr)
  pure sock

data ClientSendHandles send = ClientSendHandles
  { client_sendTCP :: send -> IO ()
  , client_sendUDP :: send -> IO ()
  }

runClient ::
  forall recv send.
  (Serialize recv, Serialize send) =>
  TimedFastLogger ->
  Connection ->
  (recv -> IO ()) ->
  MVar (ClientSendHandles send) ->
  (Int -> IO ()) ->
  IO ()
runClient logger conn putRecvMsg putSendHandlesVar putLatencyMeasurement = do
  acksRef <- newIORef nullAck
  (tcpSendQueueIn, tcpSendQueueOut) <- UC.newChan @(ClientTCPMsg send)
  (udpSendQueueIn, udpSendQueueOut) <- UC.newChan @send

  let putTCP = UC.writeChan tcpSendQueueIn . ClientTCPMsg_Data
      putUDP = UC.writeChan udpSendQueueIn
  putMVar putSendHandlesVar $
    ClientSendHandles
      { client_sendTCP = putTCP
      , client_sendUDP = putUDP
      }

  asyncs <-
    traverse
      async
      [ runReceiveUDP logger (conn_udpSock conn) acksRef putRecvMsg
      , runSendUDP (conn_udpSock conn) acksRef udpSendQueueOut
      , runReceiveTCP logger (conn_tcpConn conn) putRecvMsg putLatencyMeasurement
      , runSendTCP (conn_tcpConn conn) tcpSendQueueOut
      , runStillAlive tcpSendQueueIn
      ]
  void $ waitAnyCancelAsync asyncs

--------------------------------------------------------------------------------

runReceiveUDP ::
  forall recv a.
  (Serialize recv) =>
  TimedFastLogger ->
  Socket ->
  IORef Acks ->
  (recv -> IO ()) ->
  IO a
runReceiveUDP logger udpSocket acksRef putRecvMsg = forever $ do
  payload <- BS.recv udpSocket 1024
  case Serialize.decode @(ServerUDPMsg recv) payload of
    Left _ ->
      writeLog logger $
        "Received bad UDP msg from server: " <> toLogStr (decodeUtf8 payload)
    Right (ServerUDPMsg seqNum recvMsg) -> do
      atomicModifyIORef' acksRef $ \ack -> (insertSeqNum seqNum ack, ())
      putRecvMsg recvMsg

runReceiveTCP ::
  forall recv a.
  (Serialize recv) =>
  TimedFastLogger ->
  TCPConnection ->
  (recv -> IO ()) ->
  (Int -> IO ()) ->
  IO a
runReceiveTCP logger tcpConn putRecvMsg putLatencyMeasurement = forever $ do
  payload <- tcpRecvData tcpConn
  case Serialize.decode @(ServerTCPMsg recv) payload of
    Left _ ->
      writeLog logger $
        "Recevied bad TCP message from server: " <> toLogStr (decodeUtf8 payload)
    Right serverMsg ->
      case serverMsg of
        ServerTCPMsg_Data recvMsg -> putRecvMsg recvMsg
        ServerReturnPing time0 -> do
          time1 <- getMonotonicTimeNSecInt
          let latencyMilliseconds = (time1 - time0) `div` 1000000
          putLatencyMeasurement latencyMilliseconds

runSendUDP ::
  forall a send.
  (Serialize send) =>
  Socket ->
  IORef Acks ->
  UC.OutChan send ->
  IO a
runSendUDP udpSocket acksRef udpSendQueueOut = do
  seqNumRef <- newIORef firstSeqNum
  runSendUDP' seqNumRef udpSocket acksRef udpSendQueueOut

runSendUDP' ::
  (Serialize send) =>
  IORef SeqNum ->
  Socket ->
  IORef Acks ->
  UC.OutChan send ->
  IO a
runSendUDP' seqNumRef udpSocket acksRef udpSendQueueOut = forever $ do
  msg <- UC.readChan udpSendQueueOut

  seqNum <- readIORef seqNumRef
  writeIORef seqNumRef $ incrementSeqNum seqNum

  acks <- readIORef acksRef

  BS.sendAll udpSocket $
    Serialize.encode $
      ClientUDPMsg_Data $
        ClientUDPData
          { clientUDPData_sequenceNumber = seqNum
          , clientUDPData_acks = acks
          , clientUDPData_payload = msg
          }

runSendTCP ::
  (Serialize send) =>
  TCPConnection ->
  UC.OutChan (ClientTCPMsg send) ->
  IO a
runSendTCP tcpConn sendQueueOut =
  forever $ UC.readChan sendQueueOut >>= tcpSendData tcpConn . Serialize.encode

runStillAlive :: forall send a. UC.InChan (ClientTCPMsg send) -> IO a
runStillAlive sendQueue = forever $ do
  time <- getMonotonicTimeNSecInt
  UC.writeChan sendQueue $ ClientStillAlivePing @send time
  threadDelayNSec NC.stillAliveSendInterval_ns

--------------------------------------------------------------------------------

-- | Like 'waitAnyCancel', but cancels the asynchronous operations
-- asynchronously.
--
-- This is to work around the uninterruptible system IO of GHC's Windows
-- runtime. See https://gitlab.haskell.org/ghc/ghc/-/issues/7353
waitAnyCancelAsync :: [Async a] -> IO (Async a, a)
waitAnyCancelAsync asyncs =
  waitAny asyncs `finally` mapM_ (forkIO . cancel) asyncs
