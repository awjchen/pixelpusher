{-# LANGUAGE OverloadedStrings #-}

module Client.ThemeEnv (
  ThemeEnv (..),
  newThemeEnv,
  getTheme,
  reloadCustomTheme,
  writeThemeFileIfMissing,
) where

import Control.Exception
import Control.Monad (unless)
import Data.Functor ((<&>))
import Data.IORef
import Data.List (intercalate)
import Data.Text.IO qualified as Text
import Prettyprinter qualified
import Prettyprinter.Render.Text qualified as Prettyprinter
import System.Directory (doesFileExist)
import System.Log.FastLogger
import Toml qualified

import Pixelpusher.Custom.IO (withWriteFileViaTemp)

import Client.Config.Visual (ThemeSelection (..))
import Client.Logging
import Client.Theme
import Client.Theme.Toml

--------------------------------------------------------------------------------
-- ThemeEnv

data ThemeEnv = ThemeEnv
  { te_themeRef :: IORef Theme
  , te_logger :: TimedFastLogger
  }

newThemeEnv :: TimedFastLogger -> IO ThemeEnv
newThemeEnv logger = do
  customTheme <- loadTheme logger
  themeRef <- newIORef customTheme
  pure $!
    ThemeEnv
      { te_themeRef = themeRef
      , te_logger = logger
      }

getTheme :: ThemeEnv -> ThemeSelection -> IO Theme
getTheme (ThemeEnv ref _) themeSelection =
  readIORef ref <&> \customTheme ->
    case themeSelection of
      DefaultTheme -> defaultTheme
      CustomTheme -> customTheme

reloadCustomTheme :: ThemeEnv -> IO ()
reloadCustomTheme (ThemeEnv ref logger) = do
  let suppressException e =
        writeLog logger . toLogStr $
          "Could not reload theme due to error: '" <> show (e :: SomeException)
  handle suppressException $ do
    customTheme <- loadTheme logger
    atomicWriteIORef ref customTheme

--------------------------------------------------------------------------------

themeFilePath :: FilePath
themeFilePath = "pixelpusher-theme.toml"

-- Code copied from `loadGameParameters`
loadTheme :: TimedFastLogger -> IO Theme
loadTheme logger =
  fmap fromTomlTheme $ do
    writeLog logger . toLogStr $
      "Attempting to load custom theme from " <> themeFilePath
    doesFileExist themeFilePath >>= \case
      False -> do
        writeLog logger $
          toLogStr $
            concat
              [ "Could not find/open '"
              , themeFilePath
              , "'. Using default theme."
              ]
        pure defaultTomlTheme
      True -> do
        parseResult <- Toml.decode <$> Text.readFile themeFilePath
        case parseResult of
          Toml.Failure errors -> do
            writeLog logger $
              toLogStr $
                "Error reading theme: " <> intercalate "; " errors
            writeLog logger "Using default theme"
            pure defaultTomlTheme
          Toml.Success warnings theme -> do
            writeLog logger . toLogStr $
              "Using theme from " <> themeFilePath
            unless (null warnings) $ do
              writeLog logger $
                toLogStr $
                  "Warnings encountered while reading theme: "
                    <> intercalate "; " warnings
            pure theme

writeThemeFileIfMissing :: IO ()
writeThemeFileIfMissing = do
  doesFileExist themeFilePath >>= \case
    True ->
      -- Do not write to an existing file in order to avoid clobbering
      -- someone's theme
      pure ()
    False ->
      withWriteFileViaTemp themeFilePath $ \h ->
        Prettyprinter.renderIO h $
          Prettyprinter.layoutPretty Prettyprinter.defaultLayoutOptions $
            Toml.encode defaultTomlTheme
