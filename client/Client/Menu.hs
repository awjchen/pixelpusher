{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}

module Client.Menu (
  menuScene,
  menuScene',
  customMenuScene,
  LoopContinue (..),
  drawMenu,
  uiSoundHigh,
  uiSoundLow,
) where

import Control.Monad.Morph (lift)
import Control.Monad.Trans.Cont
import Control.Monad.Trans.Reader
import Data.Foldable (for_, toList)
import Data.IORef
import Data.Text qualified as Text
import Graphics.UI.GLFW qualified as GLFW
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Clock
import Pixelpusher.Custom.Float (
  Float2 (Float2),
  Float3 (Float3),
  Float4 (Float4),
 )
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters (defaultGameParams)

import Client.Audio.Env
import Client.Config.Controls
import Client.Config.Keybindings
import Client.GlobalKeyCallback (globalKeyCallback)
import Client.Menu.GLFW qualified as Menu
import Client.Menu.Input (MenuInputQueue)
import Client.Menu.Input qualified as Menu
import Client.Menu.Pure
import Client.StateRef
import Client.ThemeEnv
import Client.Visuals.Env
import Client.Visuals.FontAtlas
import Client.Visuals.GL.Util qualified as Util
import Client.Visuals.GL.Wrapped
import Client.Visuals.GLFW.KeyCallback (toGLFWKeyCallback)
import Client.Visuals.Rendering.GameUI.HelpOverlay (
  drawCredits,
  drawDetailedMechanicsOverlay,
  drawHelpOverlay,
  drawThemeHelp,
 )
import Client.Visuals.Rendering.GameUI.PlayerClassesOverlay (
  drawPlayerClassesOverlay,
 )
import Client.Visuals.Rendering.GameUI.VisualGlossary (drawVisualGlossary)
import Client.Visuals.Shaders.Char
import Client.Visuals.Text

menuScene ::
  GLEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  [String] ->
  StateRef model ->
  MenuState widgets model ->
  (Maybe (MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  IO (MenuState widgets model, Maybe result)
menuScene glEnv themeEnv keybindings controlOpts messages modelRef menuState userHandleInput =
  customMenuScene
    glEnv
    themeEnv
    keybindings
    controlOpts
    messages
    modelRef
    menuState
    (const userHandleInput)

menuScene' ::
  GLEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  [String] ->
  StateRef model ->
  MenuState widgets model ->
  (Maybe (MenuUpdateEvent widgets) -> IO (LoopContinue result)) ->
  IO (Maybe result)
menuScene' glEnv themeEnv keybindings controlOpts messages modelRef menuState userHandleInput =
  snd <$> menuScene glEnv themeEnv keybindings controlOpts messages modelRef menuState userHandleInput

-- | Like 'menuScene', but allows for the event handler to make arbitrary
-- updates to the model.
customMenuScene ::
  GLEnv ->
  ThemeEnv ->
  Keybindings ->
  ControlOptions ->
  [String] ->
  StateRef model ->
  MenuState widgets model ->
  ( ((model -> model) -> IO ()) ->
    Maybe (MenuUpdateEvent widgets) ->
    IO (LoopContinue result)
  ) ->
  IO (MenuState widgets model, Maybe result)
customMenuScene glEnv themeEnv keybindings controlOpts messages modelRef menuState userHandleInput = do
  inputQueue <- Menu.newQueue
  menuStateRef <- newIORef menuState

  result <-
    withCharCallback glEnv (Menu.charCallback inputQueue) $ do
      let keyCallback =
            toGLFWKeyCallback $
              globalKeyCallback themeEnv
                <> Menu.keyCallback inputQueue keybindings
      withKeyCallback glEnv keyCallback $
        evalContT $
          callCC $ \k ->
            runApproximateClock C.tickDelay_ns $ do
              loopContinue <-
                lift $
                  loopBody
                    messages
                    userHandleInput
                    glEnv
                    inputQueue
                    modelRef
                    menuStateRef
                    keybindings
                    controlOpts
              case loopContinue of
                Nothing -> k Nothing
                Just (LoopFinished result) -> k (Just result)
                Just LoopContinue -> pure ()

  finalState <- readIORef menuStateRef
  pure (finalState, result)

data LoopContinue result = LoopContinue | LoopFinished result

loopBody ::
  [String] ->
  ( ((model -> model) -> IO ()) ->
    Maybe (MenuUpdateEvent widgets) ->
    IO (LoopContinue result)
  ) ->
  GLEnv ->
  MenuInputQueue ->
  StateRef model ->
  IORef (MenuState widgets model) ->
  Keybindings ->
  ControlOptions ->
  IO (Maybe (LoopContinue result))
loopBody messages userHandleInput glEnv inputQueue modelRef menuStateRef keybindings controlOpts =
  GLFW.windowShouldClose (gle_window glEnv) >>= \case
    True ->
      pure Nothing
    False ->
      Just
        <$> loopBody'
          messages
          userHandleInput
          glEnv
          inputQueue
          modelRef
          menuStateRef
          keybindings
          controlOpts

loopBody' ::
  [String] ->
  ( ((model -> model) -> IO ()) ->
    Maybe (MenuUpdateEvent widgets) ->
    IO (LoopContinue result)
  ) ->
  GLEnv ->
  MenuInputQueue ->
  StateRef model ->
  IORef (MenuState widgets model) ->
  Keybindings ->
  ControlOptions ->
  IO (LoopContinue result)
loopBody' messages userHandleInput glEnv inputQueue modelRef menuStateRef keybindings controlOpts = do
  GLFW.pollEvents
  inputEvents <- Menu.flushQueue inputQueue

  displayHelp <- do
    let keyIsPressed =
          fmap (/= GLFW.KeyState'Released) . GLFW.getKey (gle_window glEnv)
    f1 <- keyIsPressed GLFW.Key'F1
    f2 <- keyIsPressed GLFW.Key'F2
    f3 <- keyIsPressed GLFW.Key'F3
    f4 <- keyIsPressed GLFW.Key'F4
    f5 <- keyIsPressed GLFW.Key'F5
    f12 <- keyIsPressed GLFW.Key'F12
    pure $
      if
        | f1 -> DisplayHelpInstructions
        | f2 -> DisplayHelpVisuals
        | f3 -> DisplayHelpDetailedMechanics
        | f4 -> DisplayHelpPlayerClasses
        | f5 -> DisplayHelpThemes
        | f12 -> ShowCredits
        | otherwise -> HideHelp

  result <-
    evalContT . callCC $ \k -> do
      for_ inputEvents $ \inputEvent -> do
        model0 <- lift $ readStateRef modelRef
        menuState0 <- lift $ readIORef menuStateRef
        result <-
          case handleMenuInput inputEvent model0 menuState0 of
            Nothing ->
              lift $ userHandleInput (modifyStateRef modelRef) Nothing
            Just (model1, menuState1, updateEvent) ->
              lift $ do
                writeStateRef modelRef model1
                writeIORef menuStateRef menuState1
                userHandleInput
                  (modifyStateRef modelRef)
                  (Just updateEvent)
        case result of
          LoopContinue -> pure ()
          finished@(LoopFinished _) -> k finished
      pure LoopContinue

  case result of
    LoopFinished _ ->
      -- Avoid flashing the menu when closng the window
      pure ()
    LoopContinue -> do
      -- Handle framebuffer size changes
      gle_pollFramebufferSizeUpdate glEnv >>= \case
        Nothing -> pure ()
        Just (w, h) -> setViewport (fromIntegral w) (fromIntegral h)

      model <- readStateRef modelRef
      menuState <- readIORef menuStateRef

      (screenSize, aspectRatio) <- getFramebufferDims $ gle_window glEnv

      runGL $ do
        glClearColor 0.0 0.0 0.0 1.0
        glClear GL_COLOR_BUFFER_BIT
        flip runReaderT (glEnv, screenSize, aspectRatio) $ do
          case displayHelp of
            HideHelp -> do
              drawMenu messages model menuState
              drawStatuses
            DisplayHelpInstructions ->
              drawHelpOverlay keybindings controlOpts
            DisplayHelpVisuals ->
              drawVisualGlossary defaultGameParams
            DisplayHelpDetailedMechanics ->
              drawDetailedMechanicsOverlay
            DisplayHelpPlayerClasses ->
              drawPlayerClassesOverlay defaultGameParams
            ShowCredits ->
              drawCredits
            DisplayHelpThemes ->
              drawThemeHelp

      GLFW.swapBuffers $ gle_window glEnv

  pure result

data DisplayHelp
  = HideHelp
  | DisplayHelpInstructions
  | DisplayHelpVisuals
  | DisplayHelpDetailedMechanics
  | DisplayHelpPlayerClasses
  | ShowCredits
  | DisplayHelpThemes

drawMenu ::
  [String] ->
  model ->
  MenuState widgets model ->
  ReaderT (GLEnv, Int, Float) GL ()
drawMenu messages model menuState =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let title = getMenuTitle menuState
        menuLines = renderMenu model menuState

    let fontAtlasRegular = gle_fontAtlasRegular glEnv
        fontSizeRegular = fa_fontSize fontAtlasRegular
        lineSpacingRegular = fromIntegral $ (fontSizeRegular * 3) `div` 2
    let fontAtlasLarge = gle_fontAtlasLarge glEnv
        fontSizeLarge = fa_fontSize fontAtlasLarge
        lineSpacingLarge = fromIntegral $ (fontSizeLarge * 3) `div` 2

    let x_center = lineSpacingRegular
        offWhite = Float4 0.85 0.85 0.85 1
        lightGrey = Float4 0.70 0.70 0.70 1
        darkGrey = Float4 0.30 0.30 0.30 1

    -- Title lines
    let y_top =
          fromIntegral (screenSize * 3 `div` 4)
            - fromIntegral fontSizeLarge
        (pos_menu, titleLines) =
          shadowText (0.5 * fromIntegral fontSizeLarge) darkGrey -- multiplier is a hack
            <$> stackLinesDownward
              CharAlignLeft
              (Float2 x_center y_top)
              lineSpacingLarge
              [(offWhite, Text.unpack title)]

    -- Menu lines
    let (messages_pos, prePreCharLines) =
          stackCompoundLinesDownward CharAlignLeft pos_menu lineSpacingRegular $
            flip map menuLines $ \(selected, textFragments) ->
              toList $
                textFragments <&> \(visibility, text) ->
                  ((visibility, selected), Text.unpack text)
        preCharLines =
          let horizontalIndent = 0.04 * fromIntegral fontSizeRegular
              shiftSelected charLine =
                if any ((== Selected) . snd . fst) (charLine ^. _3)
                  then charLine & _2 +~ Float2 horizontalIndent 0
                  else charLine
          in  map shiftSelected prePreCharLines
        charLines =
          let faceColor visibility selected =
                case visibility of
                  Invisible ->
                    Float4 0 0 0 0
                  Shadowed ->
                    Float4 0 0 0 0
                  Visible ->
                    case selected of
                      Selected -> Float4 0.96 0.69 0.00 1
                      NotSelected -> lightGrey
          in  map (mapCharLine (uncurry faceColor)) preCharLines
        shadowedCharLines =
          let shadowColor visibility selected =
                let baseColor =
                      case selected of
                        NotSelected ->
                          darkGrey
                        Selected ->
                          Float4 0.65 0.00 0.43 1
                in  case visibility of
                      Visible -> baseColor
                      Shadowed ->
                        let (rgb, a) = Util.toRGB_A baseColor
                        in  Util.fromRGB_A (Float.map3 (* 0.7) rgb) a
                      Invisible -> Float4 0 0 0 0
          in  map
                ( mapCharLine (uncurry shadowColor)
                    . shadowOffsetText (0.5 * fromIntegral fontSizeRegular) -- multiplier is a hack
                )
                preCharLines

    -- Status message lines
    let (_, messageLines) =
          shadowText (0.5 * fromIntegral fontSizeLarge) darkGrey -- multiplier is a hack
            <$> stackLinesDownward
              CharAlignLeft
              messages_pos
              lineSpacingRegular
              (map (offWhite,) ("" : messages)) -- empty line for spacing
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = gle_fontAtlasLarge glEnv
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines = titleLines
        }
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = gle_fontAtlasRegular glEnv
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines = shadowedCharLines ++ charLines
        }
    drawChar (gle_char glEnv) $
      CharVParams
        { cvp_aspectRatio = aspectRatio
        , cvp_fontAtlas = gle_fontAtlasRegular glEnv
        , cvp_screenSize = fromIntegral screenSize
        , cvp_lines = messageLines
        }

leftLines :: [(Float3, String)]
leftLines =
  reverse
    [ (grey, "Show help :: hold F1-F5")
    , (grey, "Show credits :: hold F12")
    ]

rightLines :: [(Float3, String)]
rightLines =
  reverse
    [ (grey, "v" <> C.versionString)
    ]

grey :: Float3
grey = let x = 0.5 in Float3 x x x

-- modified from `Client.Graphics.Rendering.GameUI.Bottom.drawStatuses`
drawStatuses :: ReaderT (GLEnv, Int, Float) GL ()
drawStatuses =
  ReaderT $ \(glEnv, screenSize, aspectRatio) -> do
    let screenSize' = fromIntegral screenSize

    let fontAtlasSmall = gle_fontAtlasSmall glEnv
        fontSizeSmall = fa_fontSize fontAtlasSmall

        smallSpacing = fromIntegral $ fontSizeSmall `div` 2
        lineSpacing = fromIntegral $ 5 * fontSizeSmall `div` 4

    let makeDisplayLines ::
          CharAlignment ->
          [(Float3, String)] ->
          [(CharAlignment, Float2, [(Float4, String)])]
        makeDisplayLines align = zipWith (makeDisplayLine align) [0 ..]

        makeDisplayLine ::
          CharAlignment ->
          Int ->
          (Float3, String) ->
          (CharAlignment, Float2, [(Float4, String)])
        makeDisplayLine align lineNo (color, text) =
          ( align
          , Float2 pos_x (smallSpacing + fromIntegral lineNo * lineSpacing)
          , [(Util.fromRGB_A color 1, text)]
          )
          where
            pos_x = case align of
              CharAlignRight -> screenSize' - smallSpacing
              CharAlignLeft -> smallSpacing
              CharAlignCenter -> 0.5 * screenSize'
     in drawChar (gle_char glEnv) $
          CharVParams
            { cvp_aspectRatio = aspectRatio
            , cvp_fontAtlas = fontAtlasSmall
            , cvp_screenSize = fromIntegral screenSize
            , cvp_lines =
                let displayLines =
                      makeDisplayLines CharAlignLeft leftLines
                        <> makeDisplayLines CharAlignRight rightLines
                in  shadowText
                      (fromIntegral (fa_fontSize fontAtlasSmall))
                      (Float4 0 0 0 1)
                      displayLines
            }

--------------------------------------------------------------------------------
-- Standard sounds

uiSoundHigh :: ALEnv -> IO ()
uiSoundHigh alEnv = playBonkMuffled alEnv BonkHigh 0.40 (0, 0)

uiSoundLow :: ALEnv -> IO ()
uiSoundLow alEnv = playBonkMuffled alEnv BonkLow 0.25 (0, 0)
