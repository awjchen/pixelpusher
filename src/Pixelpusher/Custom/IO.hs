module Pixelpusher.Custom.IO (
  withWriteFileViaTemp,
) where

import Control.Exception
import System.Directory
import System.FilePath
import System.IO

-- | Open a temporary file in read-write mode, run the action on it, then move
-- the temporary file to the specified filepath.
withWriteFileViaTemp :: FilePath -> (Handle -> IO a) -> IO a
withWriteFileViaTemp filePath action = do
  let (dir, _) = splitFileName filePath
  withTempFile dir $ \tempFilePath h -> do
    r <- action h
    hClose h
    renameFile tempFilePath filePath
    pure r

withTempFile :: FilePath -> (FilePath -> Handle -> IO a) -> IO a
withTempFile dir action = do
  bracket
    (openTempFile dir "pixelpusher-tempfile")
    (\(filepath, h) -> hClose h >> ignoreExceptions (removeFile filepath)) -- TODO: Refine exception handling?
    (uncurry action)

ignoreExceptions :: IO () -> IO ()
ignoreExceptions = handle (\(_ :: SomeException) -> pure ())
