{-# OPTIONS_GHC -Wno-orphans #-}

module Pixelpusher.Custom.Serialize (
  Serialized,
  serialize,
) where

import Data.ByteString qualified as BS
import Data.List.NonEmpty
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize

-- | A serialization of a value that remembers the type of the value
newtype Serialized a = Serialized BS.ByteString

serialize :: (Serialize a) => a -> Serialized a
serialize a = Serialized $ Serialize.encode a
{-# INLINE serialize #-}

instance (Serialize a) => Serialize (Serialized a) where
  get = serialize <$> Serialize.get -- Avoid this method
  {-# INLINE get #-}

  put (Serialized bs) = Serialize.putByteString bs
  {-# INLINE put #-}

-- Orphan instance
instance (Serialize a) => Serialize (NonEmpty a)
