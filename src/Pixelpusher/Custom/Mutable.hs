{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Custom.Mutable (
  Mutable (..),
  Copyable (..),
) where

-- I want to do something similar to Data.Mutable
-- (http://hackage.haskell.org/package/mutable), but in the opposite direction:
-- instead of starting with immutable data types and deriving mutable versions,
-- I want to start with mutable data types and derive immutable versions.

-- For now I'll just define this class and fill in the instances by hand
-- and later learn from "Data.Mutable" how to derive the instances.

import Control.Monad.ST
import Data.Kind
import Data.STRef
import Data.Vector qualified as V
import Data.Vector.Mutable qualified as VM
import Data.Vector.Storable qualified as VS
import Data.Vector.Storable.Mutable qualified as VSM
import Data.Vector.Unboxed qualified as VU
import Data.Vector.Unboxed.Mutable qualified as VUM
import Data.Word (Word32)
import GHC.Generics
import System.Random.MWC qualified as MWC
import Unsafe.Coerce (unsafeCoerce)

--------------------------------------------------------------------------------
-- Classes

-- | A class for taking snapshots of mutable data structures.
--
-- Used for (de)serializing the game state.

-- The `freeze` and `thaw` functions should be inverses. That is, they should
-- satisfy `thaw >=> freeze == pure`, and `freeze >=> thaw` should return an
-- "equivalent" mutable reference.
class Mutable s snapshot mutableRef | mutableRef -> snapshot where
  freeze :: mutableRef -> ST s snapshot
  thaw :: snapshot -> ST s mutableRef

-- | A class for copying the data of one mutable data structure into another.
--
-- Used for copying the current game state for as a starting point for
-- client-side prediction.

-- Note: Take care to ensure that `copy` is type-safe. For example, `MVector`
-- should not be an instance of `Copyable` because a copy between vectors of
-- different lenghts is undefined behaviour.
class Copyable s f where
  copy :: f -> f -> ST s ()
  -- It is worth deriving `copy` for records because its implementation is
  -- error-prone: you will not be warned if you leave out a field of a record
  -- in the implementation of `copy`.
  --
  -- Well, one can pattern-match on a record so that GHC will emit warnings if
  -- any fields are unused.
  default copy ::
    (Generic f, GCopyable s (Rep f)) =>
    f ->
    f ->
    ST s ()
  copy target source = gCopy (from target) (from source)
  {-# INLINE copy #-}

--------------------------------------------------------------------------------
-- Instances

-- STRef
instance Mutable s a (STRef s a) where
  freeze = readSTRef
  thaw = newSTRef

instance Copyable s (STRef s a) where
  copy target source = readSTRef source >>= writeSTRef target

-- Vector
instance Mutable s (V.Vector a) (VM.MVector s a) where
  freeze = V.freeze
  thaw = V.thaw

instance (VSM.Storable a) => Mutable s (VS.Vector a) (VSM.MVector s a) where
  freeze = VS.freeze
  thaw = VS.thaw

-- Not providing an instance of `Copyable` for `MVector`s

-- MWC.Gen
instance Mutable s (VU.Vector Word32) (MWC.Gen s) where
  freeze = fmap MWC.fromSeed . MWC.save
  thaw = MWC.restore . MWC.toSeed

instance Copyable s (MWC.Gen s) where
  copy target source =
    -- Warning: This implementation is unsafe! We are depending on the internal
    -- representation of `MWC.Gen`.
    VUM.copy @_ @Word32 (unsafeCoerce target) (unsafeCoerce source)

--------------------------------------------------------------------------------
-- Deriving `Copyable` with GHC Generics

class GCopyable s (f :: Type -> Type) where
  gCopy :: f x -> f x -> ST s ()

instance (GCopyable s f) => GCopyable s (M1 i c f) where
  gCopy (M1 x) (M1 y) = gCopy x y
  {-# INLINE gCopy #-}

instance (GCopyable s f, GCopyable s g) => GCopyable s (f :*: g) where
  gCopy (x1 :*: x2) (y1 :*: y2) = gCopy x1 y1 >> gCopy x2 y2
  {-# INLINE gCopy #-}

instance (Copyable s c) => GCopyable s (K1 i c) where
  gCopy (K1 x) (K1 y) = copy x y
  {-# INLINE gCopy #-}

instance GCopyable s U1 where
  gCopy _ _ = pure ()
  {-# INLINE gCopy #-}
