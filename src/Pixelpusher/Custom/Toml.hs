module Pixelpusher.Custom.Toml (
  withTable,
  genericParseTableDefaults,
) where

import Data.Text qualified as Text
import GHC.Generics
import Toml (Value' (Table'))
import Toml.Schema.FromValue (FromValue, optKey)
import Toml.Schema.Matcher (Matcher)
import Toml.Schema.ParseTable (ParseTable, parseTable, warnTable)

withTable :: String -> ParseTable l a -> Value' l -> Matcher l a
withTable label f = \case
  Table' l table ->
    parseTable f l table
  _ ->
    fail $
      "Error parsing "
        <> label
        <> ": expected table value, encountered non-table value"

-- | Match a 'Table' using the field names in a record. Supply values from a
-- record of defaults if a field name is not present.
genericParseTableDefaults ::
  (Generic a, GParseTableDef (Rep a)) => a -> ParseTable l a
genericParseTableDefaults = fmap to . gParseTableDef . from
{-# INLINE genericParseTableDefaults #-}

class GParseTableDef f where
  -- | Convert a value and apply the continuation to the result.
  gParseTableDef :: f a -> ParseTable l (f a)

instance (GParseTableDef f) => GParseTableDef (D1 c f) where
  gParseTableDef (M1 defaults) = M1 <$> gParseTableDef defaults
  {-# INLINE gParseTableDef #-}

instance (GParseTableDef f) => GParseTableDef (C1 c f) where
  gParseTableDef (M1 defaults) = M1 <$> gParseTableDef defaults
  {-# INLINE gParseTableDef #-}

instance (GParseTableDef f, GParseTableDef g) => GParseTableDef (f :*: g) where
  gParseTableDef (defaults1 :*: defaults2) =
    (:*:) <$> gParseTableDef defaults1 <*> gParseTableDef defaults2
  {-# INLINE gParseTableDef #-}

instance (Selector s, FromValue a) => GParseTableDef (S1 s (K1 i a)) where
  gParseTableDef x@(M1 (K1 defaultVal)) =
    fmap (M1 . K1) $ do
      optKey (Text.pack (selName x)) >>= \case
        Just val ->
          pure $! val
        Nothing -> do
          warnTable $
            "Using default value for missing field '" <> selName x <> "'"
          pure $! defaultVal
  {-# INLINE gParseTableDef #-}

instance GParseTableDef U1 where
  gParseTableDef U1 = pure U1
  {-# INLINE gParseTableDef #-}
