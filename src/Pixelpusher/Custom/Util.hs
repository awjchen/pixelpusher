-- | Assorted utility functions
module Pixelpusher.Custom.Util (
  listSetBits,
  mapMaybeM,
  expectJust,
  ceilDiv,
  uniformUnitDisc,
  eitherToMaybe,
  withSTRef',
) where

import Control.Monad.ST
import Control.Monad.Trans.State.Strict (StateT, runStateT)
import Data.Bits
import Data.STRef (STRef, readSTRef, writeSTRef)
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed

-- | Returns the indices of set bits in increasing order
listSetBits :: (FiniteBits b) => b -> [Int]
listSetBits bits = go bits 0
  where
    go !b !shiftAcc =
      if b == zeroBits
        then []
        else
          let trailingZeros = countTrailingZeros b
              !setBitNum = shiftAcc + trailingZeros
          in  setBitNum : go (shiftR b (trailingZeros + 1)) (setBitNum + 1)

mapMaybeM :: (Monad m) => (a -> m (Maybe b)) -> [a] -> m [b]
mapMaybeM f (x : xs) =
  f x >>= \case
    Nothing -> mapMaybeM f xs
    Just y -> (:) y <$> mapMaybeM f xs
mapMaybeM _ [] = pure []
{-# INLINE mapMaybeM #-}

expectJust :: String -> Maybe a -> a
expectJust _ (Just a) = a
expectJust errMsg Nothing = error errMsg

ceilDiv :: (Integral a) => a -> a -> a
ceilDiv n d = (n - 1) `div` d + 1

uniformUnitDisc :: MWC.Gen s -> ST s Fixed2
uniformUnitDisc gen = do
  posAngle <- MWC.uniformR (0, 2 * pi) gen
  posRadius <- sqrt <$> MWC.uniformR (0, 1) gen
  pure $ Fixed.map (* posRadius) $ Fixed.angle posAngle

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe (Right b) = Just b
eitherToMaybe (Left _) = Nothing

-- | Mutate the contents of an STRef `ref` using the provided action `f`, where
-- `ref` must not be used in `f`
withSTRef' :: STRef s a -> StateT a (ST s) b -> ST s b
withSTRef' ref f = do
  initialState <- readSTRef ref
  (result, finalState) <- runStateT f initialState
  finalState `seq` writeSTRef ref finalState
  pure result
