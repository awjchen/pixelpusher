-- | A wrapper around "Data.IntSet" for newtypes of `Int`.
--
-- This module is intended to be imported qualified.
module Pixelpusher.Custom.WrappedIntSet (
  IsInt (..),
  Identity,
  makeIdentity,
  IntSet,
  -- Construction
  empty,
  fromList,
  fromAscList,
  singleton,
  -- Insertion
  insert,
  -- Deletion
  delete,
  -- Query
  member,
  -- Combine
  intersection,
  union,
  -- Min/Max
  minView,
  -- Conversion
  toList,
  toAscList,
) where

import Data.Bifunctor
import Data.IntSet qualified as IS
import Data.Serialize (Serialize)
import Data.Word (Word16)

--------------------------------------------------------------------------------

-- | Show how `a` injects into `Int`, but show it only for this module.
class IsInt a where
  toInt :: a -> Identity Int
  fromInt :: Int -> Identity a

newtype Identity a = Identity {runIdentity :: a}

makeIdentity :: a -> Identity a
makeIdentity = Identity

toInt' :: (IsInt a) => a -> Int
toInt' = runIdentity . toInt

fromInt' :: (IsInt a) => Int -> a
fromInt' = runIdentity . fromInt

instance IsInt Int where
  toInt = makeIdentity
  {-# INLINE toInt #-}
  fromInt = makeIdentity
  {-# INLINE fromInt #-}

instance IsInt Word16 where
  toInt = makeIdentity . fromIntegral
  {-# INLINE toInt #-}
  fromInt = makeIdentity . fromIntegral
  {-# INLINE fromInt #-}

--------------------------------------------------------------------------------

newtype IntSet k = IntSet IS.IntSet
  deriving newtype (Serialize)

-- Construction

empty :: IntSet k
empty = IntSet IS.empty
{-# INLINE empty #-}

fromList :: (IsInt k) => [k] -> IntSet k
fromList xs = IntSet $ IS.fromList $ map toInt' xs
{-# INLINE fromList #-}

fromAscList :: (IsInt k) => [k] -> IntSet k
fromAscList xs = IntSet $ IS.fromAscList $ map toInt' xs
{-# INLINE fromAscList #-}

singleton :: (IsInt k) => k -> IntSet k
singleton x = IntSet $ IS.singleton $ toInt' x
{-# INLINE singleton #-}

-- Insertion

insert :: (IsInt k) => k -> IntSet k -> IntSet k
insert k (IntSet s) = IntSet $ IS.insert (toInt' k) s
{-# INLINE insert #-}

-- Deletion

delete :: (IsInt k) => k -> IntSet k -> IntSet k
delete k (IntSet s) = IntSet $ IS.delete (toInt' k) s
{-# INLINE delete #-}

-- Query

member :: (IsInt k) => k -> IntSet k -> Bool
member k (IntSet s) = IS.member (toInt' k) s
{-# INLINE member #-}

-- Combine

intersection :: IntSet k -> IntSet k -> IntSet k
intersection (IntSet s1) (IntSet s2) = IntSet $ IS.intersection s1 s2
{-# INLINE intersection #-}

union :: IntSet k -> IntSet k -> IntSet k
union (IntSet s1) (IntSet s2) = IntSet $ IS.union s1 s2
{-# INLINE union #-}

-- Min/Max

minView :: (IsInt k) => IntSet k -> Maybe (k, IntSet k)
minView (IntSet s) = bimap fromInt' IntSet <$> IS.minView s
{-# INLINE minView #-}

-- Conversion

toList :: (IsInt k) => IntSet k -> [k]
toList (IntSet s) = map fromInt' $ IS.toList s
{-# INLINE toList #-}

toAscList :: (IsInt k) => IntSet k -> [k]
toAscList (IntSet s) = map fromInt' $ IS.toAscList s
{-# INLINE toAscList #-}
