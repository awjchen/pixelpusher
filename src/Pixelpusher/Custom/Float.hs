-- | Small, strict, monomorphic vectors of 'Float's
module Pixelpusher.Custom.Float (
  Float2 (..),
  Float3 (..),
  Float4 (..),
  toList2,
  toList3,
  toList4,
  fromVec2,
  fromVec3,
  fromVec4,
  toVec2,
  toVec3,
  toVec4,
  dot2,
  map2,
  map3,
  map4,
  quadrance2,
  norm2,
  distance2,
  angle,
  unangle,
  normalize2,
  normAndNormalized2,
) where

import Data.Functor ((<&>))
import Foreign (castPtr)
import Foreign.Storable
import Lens.Micro.Internal (Field1 (..), Field2 (..), Field3 (..), Field4 (..))

import Pixelpusher.Custom.Linear (V2 (V2), V3 (V3), V4 (V4), nearZero)

--------------------------------------------------------------------------------
-- Data declarations

data Float2 = Float2 Float Float
  deriving stock (Eq, Ord, Show)

data Float3 = Float3 Float Float Float
  deriving stock (Eq, Ord, Show)

data Float4 = Float4 Float Float Float Float
  deriving stock (Eq, Ord, Show)

--------------------------------------------------------------------------------
-- Storable instances

instance Storable Float2 where
  sizeOf _ = 2 * sizeOf (undefined :: Float)
  {-# INLINE sizeOf #-}
  alignment _ = alignment (undefined :: Float)
  {-# INLINE alignment #-}
  poke ptr (Float2 x y) = poke ptr' x >> pokeElemOff ptr' 1 y
    where
      ptr' = castPtr ptr
  {-# INLINE poke #-}
  peek ptr = Float2 <$> peek ptr' <*> peekElemOff ptr' 1
    where
      ptr' = castPtr ptr
  {-# INLINE peek #-}

instance Storable Float3 where
  sizeOf _ = 3 * sizeOf (undefined :: Float)
  {-# INLINE sizeOf #-}
  alignment _ = alignment (undefined :: Float)
  {-# INLINE alignment #-}
  poke ptr (Float3 x y z) = do
    poke ptr' x
    pokeElemOff ptr' 1 y
    pokeElemOff ptr' 2 z
    where
      ptr' = castPtr ptr
  {-# INLINE poke #-}
  peek ptr = Float3 <$> peek ptr' <*> peekElemOff ptr' 1 <*> peekElemOff ptr' 2
    where
      ptr' = castPtr ptr
  {-# INLINE peek #-}

instance Storable Float4 where
  sizeOf _ = 4 * sizeOf (undefined :: Float)
  {-# INLINE sizeOf #-}
  alignment _ = alignment (undefined :: Float)
  {-# INLINE alignment #-}
  poke ptr (Float4 x y z w) = do
    poke ptr' x
    pokeElemOff ptr' 1 y
    pokeElemOff ptr' 2 z
    pokeElemOff ptr' 3 w
    where
      ptr' = castPtr ptr
  {-# INLINE poke #-}
  peek ptr =
    Float4
      <$> peek ptr'
      <*> peekElemOff ptr' 1
      <*> peekElemOff ptr' 2
      <*> peekElemOff ptr' 3
    where
      ptr' = castPtr ptr
  {-# INLINE peek #-}

--------------------------------------------------------------------------------
-- Num instances

instance Num Float2 where
  (+) (Float2 x y) (Float2 x' y') = Float2 (x + x') (y + y')
  {-# INLINE (+) #-}
  (-) (Float2 x y) (Float2 x' y') = Float2 (x - x') (y - y')
  {-# INLINE (-) #-}
  (*) (Float2 x y) (Float2 x' y') = Float2 (x * x') (y * y')
  {-# INLINE (*) #-}
  negate (Float2 x y) = Float2 (negate x) (negate y)
  {-# INLINE negate #-}
  abs (Float2 x y) = Float2 (abs x) (abs y)
  {-# INLINE abs #-}
  signum (Float2 x y) = Float2 (signum x) (signum y)
  {-# INLINE signum #-}
  fromInteger i = let f = fromIntegral i in Float2 f f
  {-# INLINE fromInteger #-}

instance Num Float3 where
  (+) (Float3 x y z) (Float3 x' y' z') = Float3 (x + x') (y + y') (z + z')
  {-# INLINE (+) #-}
  (-) (Float3 x y z) (Float3 x' y' z') = Float3 (x - x') (y - y') (z - z')
  {-# INLINE (-) #-}
  (*) (Float3 x y z) (Float3 x' y' z') = Float3 (x * x') (y * y') (z * z')
  {-# INLINE (*) #-}
  negate (Float3 x y z) = Float3 (negate x) (negate y) (negate z)
  {-# INLINE negate #-}
  abs (Float3 x y z) = Float3 (abs x) (abs y) (abs z)
  {-# INLINE abs #-}
  signum (Float3 x y z) = Float3 (signum x) (signum y) (signum z)
  {-# INLINE signum #-}
  fromInteger i = let f = fromIntegral i in Float3 f f f
  {-# INLINE fromInteger #-}

instance Num Float4 where
  (+) (Float4 x y z w) (Float4 x' y' z' w') =
    Float4 (x + x') (y + y') (z + z') (w + w')
  {-# INLINE (+) #-}
  (-) (Float4 x y z w) (Float4 x' y' z' w') =
    Float4 (x - x') (y - y') (z - z') (w - w')
  {-# INLINE (-) #-}
  (*) (Float4 x y z w) (Float4 x' y' z' w') =
    Float4 (x * x') (y * y') (z * z') (w * w')
  {-# INLINE (*) #-}
  negate (Float4 x y z w) = Float4 (negate x) (negate y) (negate z) (negate w)
  {-# INLINE negate #-}
  abs (Float4 x y z w) = Float4 (abs x) (abs y) (abs z) (abs w)
  {-# INLINE abs #-}
  signum (Float4 x y z w) = Float4 (signum x) (signum y) (signum z) (signum w)
  {-# INLINE signum #-}
  fromInteger i = let f = fromIntegral i in Float4 f f f f
  {-# INLINE fromInteger #-}

--------------------------------------------------------------------------------
-- Fractional instances

instance Fractional Float2 where
  recip = map2 recip
  {-# INLINE recip #-}
  (/) (Float2 x y) (Float2 x' y') = Float2 (x / x') (y / y')
  {-# INLINE (/) #-}
  fromRational r = let f = fromRational r in Float2 f f
  {-# INLINE fromRational #-}

instance Fractional Float3 where
  recip = map3 recip
  {-# INLINE recip #-}
  (/) (Float3 x y z) (Float3 x' y' z') = Float3 (x / x') (y / y') (z / z')
  {-# INLINE (/) #-}
  fromRational r = let f = fromRational r in Float3 f f f
  {-# INLINE fromRational #-}

instance Fractional Float4 where
  recip = map4 recip
  {-# INLINE recip #-}
  (/) (Float4 x y z w) (Float4 x' y' z' w') =
    Float4 (x / x') (y / y') (z / z') (w / w')
  {-# INLINE (/) #-}
  fromRational r = let f = fromRational r in Float4 f f f f
  {-# INLINE fromRational #-}

--------------------------------------------------------------------------------
-- To list

toList2 :: Float2 -> [Float]
toList2 (Float2 x y) = [x, y]
{-# INLINE toList2 #-}

toList3 :: Float3 -> [Float]
toList3 (Float3 x y z) = [x, y, z]
{-# INLINE toList3 #-}

toList4 :: Float4 -> [Float]
toList4 (Float4 x y z w) = [x, y, z, w]
{-# INLINE toList4 #-}

--------------------------------------------------------------------------------
-- From vector

fromVec2 :: V2 Float -> Float2
fromVec2 (V2 x y) = Float2 x y
{-# INLINE fromVec2 #-}

fromVec3 :: V3 Float -> Float3
fromVec3 (V3 x y z) = Float3 x y z
{-# INLINE fromVec3 #-}

fromVec4 :: V4 Float -> Float4
fromVec4 (V4 x y z w) = Float4 x y z w
{-# INLINE fromVec4 #-}

toVec2 :: Float2 -> V2 Float
toVec2 (Float2 x y) = V2 x y
{-# INLINE toVec2 #-}

toVec3 :: Float3 -> V3 Float
toVec3 (Float3 x y z) = V3 x y z
{-# INLINE toVec3 #-}

toVec4 :: Float4 -> V4 Float
toVec4 (Float4 x y z w) = V4 x y z w
{-# INLINE toVec4 #-}

--------------------------------------------------------------------------------
-- Field access lenses

instance Field1 Float2 Float2 Float Float where
  _1 f (Float2 x y) = f x <&> \x' -> Float2 x' y
  {-# INLINE _1 #-}

instance Field2 Float2 Float2 Float Float where
  _2 f (Float2 x y) = f y <&> \y' -> Float2 x y'
  {-# INLINE _2 #-}

instance Field1 Float3 Float3 Float Float where
  _1 f (Float3 x y z) = f x <&> \x' -> Float3 x' y z
  {-# INLINE _1 #-}

instance Field2 Float3 Float3 Float Float where
  _2 f (Float3 x y z) = f y <&> \y' -> Float3 x y' z
  {-# INLINE _2 #-}

instance Field3 Float3 Float3 Float Float where
  _3 f (Float3 x y z) = f z <&> \z' -> Float3 x y z'
  {-# INLINE _3 #-}

instance Field4 Float4 Float4 Float Float where
  _4 f (Float4 x y z w) = f w <&> \w' -> Float4 x y z w'
  {-# INLINE _4 #-}

--------------------------------------------------------------------------------
-- Vector operations

dot2 :: Float2 -> Float2 -> Float
dot2 (Float2 x y) (Float2 x' y') = x * x' + y * y'
{-# INLINE dot2 #-}

map2 :: (Float -> Float) -> Float2 -> Float2
map2 f (Float2 x y) = Float2 (f x) (f y)
{-# INLINE map2 #-}

map3 :: (Float -> Float) -> Float3 -> Float3
map3 f (Float3 x y z) = Float3 (f x) (f y) (f z)
{-# INLINE map3 #-}

map4 :: (Float -> Float) -> Float4 -> Float4
map4 f (Float4 x y z w) = Float4 (f x) (f y) (f z) (f w)
{-# INLINE map4 #-}

quadrance2 :: Float2 -> Float
quadrance2 (Float2 ax ay) = ax * ax + ay * ay
{-# INLINE quadrance2 #-}

norm2 :: Float2 -> Float
norm2 v = sqrt $ quadrance2 v
{-# INLINE norm2 #-}

distance2 :: Float2 -> Float2 -> Float
distance2 v w = norm2 $ v - w
{-# INLINE distance2 #-}

-- Copied from "Linear.V2"
unangle :: Float2 -> Float
unangle a@(Float2 ax ay) =
  let alpha = asin $ ay / norm2 a
  in  if ax < 0
        then pi - alpha
        else alpha
{-# INLINE unangle #-}

angle :: Float -> Float2
angle a = Float2 (cos a) (sin a)
{-# INLINE angle #-}

-- Copied from "Linear.V2.normalize"
normalize2 :: Float2 -> Float2
normalize2 v = if nearZero l || nearZero (1 - l) then v else map2 (/ sqrt l) v
  where
    l = quadrance2 v
{-# INLINE normalize2 #-}

-- Based on "Linear.V2.normalize"
normAndNormalized2 :: Float2 -> (Float, Float2)
normAndNormalized2 v =
  let normalized =
        if nearZero l || nearZero (1 - l) then v else map2 (/ norm) v
  in  (norm, normalized)
  where
    l = quadrance2 v
    norm = sqrt l
{-# INLINE normAndNormalized2 #-}
