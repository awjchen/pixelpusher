{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

-- | Implementation of the sparse set data structure.
--
-- Operations are implemented as needed.
module Pixelpusher.Store.DenseStore (
  DenseStore,
  DenseStoreSnapshot,
  newDenseStore,
  copyDenseStore,
  -- Lookup
  lookup_1,
  sureLookup_1,
  sureLookup_2,
  sureLookup_3,
  -- Set
  sureSet_1,
  sureSet_2,
  -- Modify
  modify,
  modifyReturn,
  sureModify,
  sureModifyReturn,
  sureModifyPair,
  sureModifyPairReturn,
  sureModifyPair_2_1,
  sureModify_1,
  sureModify_1_1,
  sureModify_2,
  sureModify_3_1,
  sureModifyReturn_1,
  sureModifyReturn_1_1,
  sureModifyReturn_2,
  sureModifyReturn_3,
  -- Map
  map,
  mapWithIndexReturn,
  map_1_3,
  mapReturn_idx_3_2,
  mapM_idx_1_2,
  mapMReturn_idx_1,
  -- Copy
  unsafeCopy,
  -- Folds
  foldM_idx_1,
  foldM_idx_2,
  -- To list
  toList,
  toList_1,
  toList_idx_2,
  toListM_idx_2,
  toListMaybe_idx_2,
  toListPred_idx_2,
  indices,
  indicesPred_1,
) where

import Prelude hiding (init, lookup, map, read)

import Control.Monad (filterM, foldM, forM, forM_)
import Control.Monad.ST
import Data.STRef
import Data.Serialize (Serialize)
import Data.Strict.Tuple (Pair ((:!:)))
import Data.Traversable (for)
import Data.Vector.Generic qualified as VG
import Data.Vector.Generic.Mutable qualified as VGM
import Data.Vector.Storable qualified as VS
import Data.Vector.Storable.Mutable qualified as VSM
import Data.Word (Word16)
import Foreign.Storable
import GHC.Generics

import Pixelpusher.Custom.Mutable qualified as Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (VecStore)
import Pixelpusher.Store.VecStore qualified as VecSt

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  CoreM,
  ReadStore (..),
  Store (..),
  SureReadStore (..),
  coreM,
 )

--------------------------------------------------------------------------------

-- | Sparse set data structure for a struct of arrays (`vecStore` parameter)
-- with typed indices (`index` and `sureIndex` parameters).
--
-- `index` is the type of "ordinary" indices to the store. `sureIndex` is
-- the type of indices for entries that must exist in the store if the
-- corresponding value of type `sureIndex` exists. This invariant is not
-- maintained by the store, but by the "Pixelpusher.Game.EntityStore.Core"
-- module, which has the exclusive ability to add or remove indices from the
-- store via the `CoreM` type.
data DenseStore s index sureIndex vecStore = DenseStore
  { dvs_capacity :: Word16
  , dvs_count :: {-# UNPACK #-} STRef s Word16
  , dvs_sparse2Dense :: {-# UNPACK #-} VSM.MVector s Word16
  -- ^ Uses `emptySignifier` to denote missing values
  , dvs_dense2Sparse :: {-# UNPACK #-} VSM.MVector s index
  -- ^ Values at indices greater or equal to the current count are undefined
  , dvs_values :: vecStore s
  -- ^ Values at indices greater or equal to the current count are undefined
  }

emptySignifier :: Word16
emptySignifier = maxBound

newDenseStore ::
  (VecStore elem vs, Storable idx) => Word16 -> ST s (DenseStore s idx sidx vs)
newDenseStore capacity = do
  count <- newSTRef 0
  sparse <- VSM.replicate (fromIntegral C.maxNumEntities) emptySignifier
  dense <- VSM.unsafeNew (fromIntegral capacity)
  values <- VecSt.init (VGM.unsafeNew (fromIntegral capacity))
  pure $
    DenseStore
      { dvs_capacity = capacity
      , dvs_count = count
      , dvs_sparse2Dense = sparse
      , dvs_dense2Sparse = dense
      , dvs_values = values
      }
{-# INLINEABLE newDenseStore #-}

--------------------------------------------------------------------------------
-- `Mutable` instance

data DenseStoreSnapshot idx vs = DenseStoreSnapshot
  { dvss_capacity :: Word16
  , dvss_count :: Word16
  , dvss_sparse :: VS.Vector Word16
  , dvss_dense :: VS.Vector idx
  , dvss_values :: VecSt.Snapshot vs
  }
  deriving stock (Generic)

instance
  (Serialize (VecSt.Snapshot vs), Serialize idx, Storable idx) =>
  Serialize (DenseStoreSnapshot idx vs)

instance
  (Storable idx, VecStore elem vs) =>
  Mutable.Mutable s (DenseStoreSnapshot idx vs) (DenseStore s idx sidx vs)
  where
  freeze :: DenseStore s idx sidx vs -> ST s (DenseStoreSnapshot idx vs)
  freeze store = do
    count <- readSTRef (dvs_count store)
    let freezeCount :: (VG.Vector v a) => VG.Mutable v s a -> ST s (v a)
        freezeCount = fmap (VG.slice 0 (fromIntegral count)) . VG.freeze
    sparse <- VS.freeze (dvs_sparse2Dense store)
    dense <- freezeCount (dvs_dense2Sparse store)
    values <- VecSt.freeze freezeCount (dvs_values store)
    pure $
      DenseStoreSnapshot
        { dvss_capacity = dvs_capacity store
        , dvss_count = count
        , dvss_sparse = sparse
        , dvss_dense = dense
        , dvss_values = values
        }

  thaw :: DenseStoreSnapshot idx vs -> ST s (DenseStore s idx sidx vs)
  thaw snapshot = do
    let snapshotCapacity = dvss_capacity snapshot
        snapshotCount = dvss_count snapshot
    let thawCount :: (VG.Vector v a) => v a -> ST s (VG.Mutable v s a)
        thawCount v = do
          vm <- VG.thaw v
          wm <- VGM.unsafeNew (fromIntegral snapshotCapacity)
          VGM.unsafeCopy (VGM.slice 0 (fromIntegral snapshotCount) wm) vm
          pure wm
    count <- newSTRef snapshotCount
    sparse <- VS.thaw (dvss_sparse snapshot)
    dense <- thawCount (dvss_dense snapshot)
    values <- VecSt.thaw thawCount (dvss_values snapshot)
    pure $
      DenseStore
        { dvs_capacity = snapshotCapacity
        , dvs_count = count
        , dvs_sparse2Dense = sparse
        , dvs_dense2Sparse = dense
        , dvs_values = values
        }

-- Only well-defined for stores of equal capacity.
copyDenseStore ::
  (Storable idx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  DenseStore s idx sidx vs ->
  ST s ()
copyDenseStore target source = do
  count <- readSTRef (dvs_count source)
  writeSTRef (dvs_count target) count

  VSM.copy (dvs_sparse2Dense target) (dvs_sparse2Dense source)

  let sliceCopy :: (VGM.MVector v a) => v s a -> v s a -> ST s ()
      sliceCopy trgt src =
        let count' = fromIntegral count
        in  VGM.copy (VGM.slice 0 count' trgt) (VGM.slice 0 count' src)

  sliceCopy (dvs_dense2Sparse target) (dvs_dense2Sparse source)
  VecSt.copy sliceCopy (dvs_values target) (dvs_values source)

--------------------------------------------------------------------------------
-- Lookup

-- Internal
lookupDenseIdx ::
  (IsEntityID idx) => DenseStore s idx sidx vs -> idx -> ST s (Maybe Word16)
lookupDenseIdx store (getIndex -> sparseIdx) = do
  denseIdx <- VSM.read (dvs_sparse2Dense store) (fromIntegral sparseIdx)
  pure $ if denseIdx == emptySignifier then Nothing else Just denseIdx

-- | Internal. Like `lookupDenseIdx`, but does not check whether the provided
-- `idx` has a valid entry.
unsafeLookupDenseIdx ::
  (IsEntityID sidx) => DenseStore s idx sidx vs -> sidx -> ST s Word16
unsafeLookupDenseIdx store (getIndex -> sparseIdx) =
  VSM.read (dvs_sparse2Dense store) (fromIntegral sparseIdx)

instance
  (IsEntityID idx, VecStore elem vs) =>
  ReadStore s idx elem (DenseStore s idx sidx vs)
  where
  lookup ::
    (SubEntityID i idx) =>
    DenseStore s idx sidx vs ->
    i ->
    ST s (Maybe elem)
  lookup store eid =
    lookupDenseIdx store (subEntityID eid) >>= traverse \denseIdx ->
      VecSt.read (`VGM.unsafeRead` fromIntegral denseIdx) (dvs_values store)
  {-# INLINEABLE lookup #-}

instance
  (IsEntityID sidx, VecStore elem vs) =>
  SureReadStore s sidx elem (DenseStore s idx sidx vs)
  where
  sureLookup ::
    (SubEntityID i sidx) => DenseStore s idx sidx vs -> i -> ST s elem
  sureLookup store eid = do
    denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
    VecSt.read (`VGM.unsafeRead` denseIdx) (dvs_values store)
  {-# INLINE sureLookup #-}

lookup_1 ::
  (IsEntityID idx, SubEntityID i idx, VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  i ->
  ST s (Maybe a)
lookup_1 store acc1 eid =
  lookupDenseIdx store (subEntityID eid)
    >>= traverse (VGM.unsafeRead vec1 . fromIntegral)
  where
    values = dvs_values store
    vec1 = acc1 values
{-# INLINEABLE lookup_1 #-}

sureLookup_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  i ->
  ST s a
sureLookup_1 store accessor eid =
  fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
    >>= VGM.unsafeRead (accessor values)
  where
    values = dvs_values store
{-# INLINE sureLookup_1 #-}

sureLookup_2 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a, VGM.MVector v2 b) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a) ->
  (vs s -> v2 s b) ->
  i ->
  ST s (a, b)
sureLookup_2 store acc1 acc2 eid = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (,)
    <$> VGM.unsafeRead vec1 denseIdx
    <*> VGM.unsafeRead vec2 denseIdx
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINEABLE sureLookup_2 #-}

sureLookup_3 ::
  ( IsEntityID sidx
  , SubEntityID i sidx
  , VGM.MVector v1 a1
  , VGM.MVector v2 a2
  , VGM.MVector v3 a3
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (vs s -> v3 s a3) ->
  i ->
  ST s (a1, a2, a3)
sureLookup_3 store acc1 acc2 acc3 eid = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (,,)
    <$> VGM.unsafeRead vec1 denseIdx
    <*> VGM.unsafeRead vec2 denseIdx
    <*> VGM.unsafeRead vec3 denseIdx
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
{-# INLINEABLE sureLookup_3 #-}

--------------------------------------------------------------------------------
-- Insertion, deletion

instance
  (IsEntityID idx, Storable idx, VecStore elem vs) =>
  Store s idx elem (DenseStore s idx sidx vs)
  where
  insert ::
    (SubEntityID i idx) => DenseStore s idx sidx vs -> i -> elem -> CoreM s ()
  insert store (subEntityID -> eid) value =
    coreM $
      lookupDenseIdx store eid >>= \case
        Just denseIdx ->
          -- Key exists. Overwrite the value
          VecSt.write (`VGM.write` fromIntegral denseIdx) (dvs_values store) value
        Nothing -> do
          -- Add to end of dense arrays
          denseIdx <- readSTRef (dvs_count store)
          modifySTRef' (dvs_count store) succ
          VSM.write (dvs_sparse2Dense store) (getIndex eid) denseIdx
          VSM.write (dvs_dense2Sparse store) (fromIntegral denseIdx) eid
          VecSt.write (`VGM.write` fromIntegral denseIdx) (dvs_values store) value
  {-# INLINEABLE insert #-}

  delete :: (SubEntityID i idx) => DenseStore s idx sidx vs -> i -> CoreM s ()
  delete store (subEntityID -> eid) =
    coreM $
      lookupDenseIdx store eid >>= \case
        Nothing ->
          -- Key does not exist. Nothing to do.
          pure ()
        Just denseIdx -> do
          -- Get last dense index and its corresponding sparse index
          modifySTRef' (dvs_count store) pred
          lastDenseIdx <- fromIntegral <$> readSTRef (dvs_count store)
          lastSparseIdx <-
            getIndex <$> VSM.unsafeRead (dvs_dense2Sparse store) lastDenseIdx

          -- Update sparse indices
          -- Note: We must write to `sparseIdx` after `lastSparseIdx` in case they
          -- are equal.
          VSM.unsafeWrite (dvs_sparse2Dense store) lastSparseIdx denseIdx
          VSM.unsafeWrite (dvs_sparse2Dense store) (getIndex eid) emptySignifier

          -- Move last elements of dense arrays to index of removed element
          VSM.unsafeRead (dvs_dense2Sparse store) lastDenseIdx
            >>= VSM.unsafeWrite (dvs_dense2Sparse store) (fromIntegral denseIdx)
          VecSt.read (`VGM.unsafeRead` lastDenseIdx) (dvs_values store)
            >>= VecSt.write (`VGM.unsafeWrite` fromIntegral denseIdx) (dvs_values store)
  {-# INLINEABLE delete #-}

  clear :: DenseStore s idx sidx vs -> CoreM s ()
  clear store =
    coreM $ do
      writeSTRef (dvs_count store) 0
      VSM.set (dvs_sparse2Dense store) emptySignifier

--------------------------------------------------------------------------------
-- Set

sureSet_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  i ->
  a ->
  ST s ()
sureSet_1 store acc eid a =
  fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
    >>= \i -> VGM.unsafeWrite (acc values) i a
  where
    values = dvs_values store
{-# INLINEABLE sureSet_1 #-}

sureSet_2 ::
  ( IsEntityID sidx
  , SubEntityID i sidx
  , VGM.MVector v1 a1
  , VGM.MVector v2 a2
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  i ->
  a1 ->
  a2 ->
  ST s ()
sureSet_2 store acc1 acc2 eid a1 a2 =
  fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid) >>= \i -> do
    VGM.unsafeWrite vec1 i a1
    VGM.unsafeWrite vec2 i a2
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINEABLE sureSet_2 #-}

--------------------------------------------------------------------------------
-- Modify

modify ::
  (IsEntityID idx, SubEntityID i idx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  (elem -> elem) ->
  ST s ()
modify store eid f =
  fmap fromIntegral <$> lookupDenseIdx store (subEntityID eid) >>= \case
    Nothing -> pure ()
    Just denseIdx ->
      VecSt.read (`VGM.unsafeRead` denseIdx) values
        >>= VecSt.write (`VGM.unsafeWrite` denseIdx) values . f
  where
    values = dvs_values store
{-# INLINEABLE modify #-}

modifyReturn ::
  (IsEntityID idx, SubEntityID i idx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  (elem -> (r, elem)) ->
  ST s (Maybe r)
modifyReturn store eid f =
  fmap fromIntegral <$> lookupDenseIdx store (subEntityID eid) >>= \case
    Nothing -> pure Nothing
    Just denseIdx -> do
      let values = dvs_values store
      i <- VecSt.read (`VGM.unsafeRead` denseIdx) values
      let (r, o) = f i
      VecSt.write (`VGM.unsafeWrite` denseIdx) values o
      pure $ Just r
{-# INLINEABLE modifyReturn #-}

sureModify ::
  (IsEntityID sidx, SubEntityID i sidx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  (elem -> elem) ->
  ST s ()
sureModify store eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  i <- VecSt.read (`VGM.unsafeRead` denseIdx) values
  VecSt.write (`VGM.unsafeWrite` denseIdx) values (f i)
  where
    values = dvs_values store
{-# INLINEABLE sureModify #-}

sureModifyReturn ::
  (IsEntityID sidx, SubEntityID i sidx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  (elem -> (r, elem)) ->
  ST s r
sureModifyReturn store eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  i <- VecSt.read (`VGM.unsafeRead` denseIdx) values
  let (r, o) = f i
  VecSt.write (`VGM.unsafeWrite` denseIdx) values o
  pure r
  where
    values = dvs_values store
{-# INLINEABLE sureModifyReturn #-}

sureModifyPair ::
  (IsEntityID sidx, SubEntityID i sidx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  i ->
  (elem -> elem -> (elem, elem)) ->
  ST s ()
sureModifyPair store eid1 eid2 f = do
  denseIdx1 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid1)
  denseIdx2 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid2)
  (o1, o2) <-
    f
      <$> VecSt.read (`VGM.unsafeRead` denseIdx1) values
      <*> VecSt.read (`VGM.unsafeRead` denseIdx2) values
  VecSt.write (`VGM.unsafeWrite` denseIdx1) values o1
  VecSt.write (`VGM.unsafeWrite` denseIdx2) values o2
  where
    values = dvs_values store
{-# INLINEABLE sureModifyPair #-}

sureModifyPairReturn ::
  (IsEntityID sidx, SubEntityID i sidx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  i ->
  i ->
  (elem -> elem -> (r, (elem, elem))) ->
  ST s r
sureModifyPairReturn store eid1 eid2 f = do
  denseIdx1 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid1)
  denseIdx2 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid2)
  i1 <- VecSt.read (`VGM.unsafeRead` denseIdx1) values
  i2 <- VecSt.read (`VGM.unsafeRead` denseIdx2) values
  let (r, (o1, o2)) = f i1 i2
  VecSt.write (`VGM.unsafeWrite` denseIdx1) values o1
  VecSt.write (`VGM.unsafeWrite` denseIdx2) values o2
  pure r
  where
    values = dvs_values store
{-# INLINEABLE sureModifyPairReturn #-}

sureModifyPair_2_1 ::
  (IsEntityID sidx, SubEntityID i sidx, Storable a, Storable b, Storable c) =>
  DenseStore s idx sidx vs ->
  (vs s -> VSM.MVector s a) ->
  (vs s -> VSM.MVector s b) ->
  (vs s -> VSM.MVector s c) ->
  i ->
  i ->
  ((a, b, c) -> (a, b, c) -> (c, c)) ->
  ST s ()
sureModifyPair_2_1 store acc1 acc2 acc3 eid1 eid2 f = do
  denseIdx1 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid1)
  denseIdx2 <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid2)
  i1 <-
    (,,)
      <$> VSM.unsafeRead vec1 denseIdx1
      <*> VSM.unsafeRead vec2 denseIdx1
      <*> VSM.unsafeRead vec3 denseIdx1
  i2 <-
    (,,)
      <$> VSM.unsafeRead vec1 denseIdx2
      <*> VSM.unsafeRead vec2 denseIdx2
      <*> VSM.unsafeRead vec3 denseIdx2
  let (o1, o2) = f i1 i2
  VSM.unsafeWrite vec3 denseIdx1 o1
  VSM.unsafeWrite vec3 denseIdx2 o2
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
{-# INLINEABLE sureModifyPair_2_1 #-}

sureModify_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  i ->
  (a -> a) ->
  ST s ()
sureModify_1 store acc eid f =
  fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
    >>= VGM.unsafeModify (acc values) f
  where
    values = dvs_values store
{-# INLINE sureModify_1 #-}

sureModify_1_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  i ->
  (a1 -> a2 -> a2) ->
  ST s ()
sureModify_1_1 store acc1 acc2 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  o2 <-
    f <$> VGM.unsafeRead vec1 denseIdx <*> VGM.unsafeRead vec2 denseIdx
  VGM.unsafeWrite vec2 denseIdx o2
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINE sureModify_1_1 #-}

sureModify_2 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  i ->
  (a1 -> a2 -> (a1, a2)) ->
  ST s ()
sureModify_2 store acc1 acc2 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (o1, o2) <-
    f <$> VGM.unsafeRead vec1 denseIdx <*> VGM.unsafeRead vec2 denseIdx
  VGM.unsafeWrite vec1 denseIdx o1
  VGM.unsafeWrite vec2 denseIdx o2
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINE sureModify_2 #-}

sureModify_3_1 ::
  ( IsEntityID sidx
  , SubEntityID i sidx
  , VGM.MVector v1 a1
  , VGM.MVector v2 a2
  , VGM.MVector v3 a3
  , VGM.MVector v4 a4
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (vs s -> v3 s a3) ->
  (vs s -> v4 s a4) ->
  i ->
  (a1 -> a2 -> a3 -> a4 -> a4) ->
  ST s ()
sureModify_3_1 store acc1 acc2 acc3 acc4 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  o4 <-
    f
      <$> VGM.unsafeRead vec1 denseIdx
      <*> VGM.unsafeRead vec2 denseIdx
      <*> VGM.unsafeRead vec3 denseIdx
      <*> VGM.unsafeRead vec4 denseIdx
  VGM.unsafeWrite vec4 denseIdx o4
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
    vec4 = acc4 values
{-# INLINE sureModify_3_1 #-}

sureModifyReturn_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a1) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  i ->
  (a1 -> (r, a1)) ->
  ST s r
sureModifyReturn_1 store acc1 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (r, o1) <- f <$> VGM.unsafeRead vec1 denseIdx
  VGM.unsafeWrite vec1 denseIdx o1
  pure r
  where
    values = dvs_values store
    vec1 = acc1 values
{-# INLINE sureModifyReturn_1 #-}

sureModifyReturn_1_1 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  i ->
  (a1 -> a2 -> (r, a2)) ->
  ST s r
sureModifyReturn_1_1 store acc1 acc2 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (r, o2) <-
    f
      <$> VGM.unsafeRead vec1 denseIdx
      <*> VGM.unsafeRead vec2 denseIdx
  VGM.unsafeWrite vec2 denseIdx o2
  pure r
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINE sureModifyReturn_1_1 #-}

sureModifyReturn_2 ::
  (IsEntityID sidx, SubEntityID i sidx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  i ->
  (a1 -> a2 -> (r, a1, a2)) ->
  ST s r
sureModifyReturn_2 store acc1 acc2 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (r, o1, o2) <-
    f
      <$> VGM.unsafeRead vec1 denseIdx
      <*> VGM.unsafeRead vec2 denseIdx
  VGM.unsafeWrite vec1 denseIdx o1
  VGM.unsafeWrite vec2 denseIdx o2
  pure r
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINE sureModifyReturn_2 #-}

sureModifyReturn_3 ::
  ( IsEntityID sidx
  , SubEntityID i sidx
  , VGM.MVector v1 a1
  , VGM.MVector v2 a2
  , VGM.MVector v3 a3
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (vs s -> v3 s a3) ->
  i ->
  (a1 -> a2 -> a3 -> (r, a1, a2, a3)) ->
  ST s r
sureModifyReturn_3 store acc1 acc2 acc3 eid f = do
  denseIdx <- fromIntegral <$> unsafeLookupDenseIdx store (subEntityID eid)
  (r, o1, o2, o3) <-
    f
      <$> VGM.unsafeRead vec1 denseIdx
      <*> VGM.unsafeRead vec2 denseIdx
      <*> VGM.unsafeRead vec3 denseIdx
  VGM.unsafeWrite vec1 denseIdx o1
  VGM.unsafeWrite vec2 denseIdx o2
  VGM.unsafeWrite vec3 denseIdx o3
  pure r
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
{-# INLINE sureModifyReturn_3 #-}

--------------------------------------------------------------------------------
-- Map

map ::
  (VecStore elem vs) => DenseStore s idx sidx vs -> (elem -> elem) -> ST s ()
map store f = do
  let vs = dvs_values store
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM_ [0 .. n - 1] $ \i ->
    VecSt.read (`VGM.unsafeRead` i) vs
      >>= VecSt.write (`VGM.unsafeWrite` i) vs . f
{-# INLINEABLE map #-}

mapWithIndexReturn ::
  (Storable idx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  (idx -> elem -> (elem, r)) ->
  ST s [r]
mapWithIndexReturn store f = do
  let values = dvs_values store
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \i -> do
    (o, r) <-
      f
        <$> VGM.unsafeRead eids i
        <*> VecSt.read (`VGM.unsafeRead` i) values
    VecSt.write (`VGM.unsafeWrite` i) values o
    pure r
  where
    eids = dvs_dense2Sparse store
{-# INLINEABLE mapWithIndexReturn #-}

map_1_3 ::
  ( VGM.MVector v1 a1
  , VGM.MVector v2 a2
  , VGM.MVector v3 a3
  , VGM.MVector v4 a4
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (vs s -> v3 s a3) ->
  (vs s -> v4 s a4) ->
  (a1 -> a2 -> a3 -> a4 -> (a2, a3, a4)) ->
  ST s ()
map_1_3 store acc1 acc2 acc3 acc4 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM_ [0 .. n - 1] $ \i -> do
    (o2, o3, o4) <-
      f
        <$> VGM.unsafeRead vec1 i
        <*> VGM.unsafeRead vec2 i
        <*> VGM.unsafeRead vec3 i
        <*> VGM.unsafeRead vec4 i
    VGM.unsafeWrite vec2 i o2
    VGM.unsafeWrite vec3 i o3
    VGM.unsafeWrite vec4 i o4
  where
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
    vec4 = acc4 values
{-# INLINEABLE map_1_3 #-}

mapReturn_idx_3_2 ::
  ( Storable idx
  , VGM.MVector v1 a
  , VGM.MVector v2 b
  , VGM.MVector v3 c
  , VGM.MVector v4 d
  , VGM.MVector v5 e
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a) ->
  (vs s -> v2 s b) ->
  (vs s -> v3 s c) ->
  (vs s -> v4 s d) ->
  (vs s -> v5 s e) ->
  (idx -> a -> b -> c -> d -> e -> (d, e, r)) ->
  ST s [r]
mapReturn_idx_3_2 store acc1 acc2 acc3 acc4 acc5 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \i -> do
    (o4, o5, r) <-
      f
        <$> VGM.unsafeRead eids i
        <*> VGM.unsafeRead vec1 i
        <*> VGM.unsafeRead vec2 i
        <*> VGM.unsafeRead vec3 i
        <*> VGM.unsafeRead vec4 i
        <*> VGM.unsafeRead vec5 i
    VGM.unsafeWrite vec4 i o4
    VGM.unsafeWrite vec5 i o5
    pure r
  where
    eids = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
    vec4 = acc4 values
    vec5 = acc5 values
{-# INLINEABLE mapReturn_idx_3_2 #-}

mapM_idx_1_2 ::
  ( Storable idx
  , VGM.MVector v1 a1
  , VGM.MVector v2 a2
  , VGM.MVector v3 a3
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (vs s -> v3 s a3) ->
  (idx -> a1 -> a2 -> a3 -> ST s (Pair a2 a3)) ->
  ST s ()
mapM_idx_1_2 store acc1 acc2 acc3 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM_ [0 .. n - 1] $ \i -> do
    sparseIdx <- VSM.unsafeRead ds i
    i1 <- VGM.unsafeRead vec1 i
    i2 <- VGM.unsafeRead vec2 i
    i3 <- VGM.unsafeRead vec3 i
    o2 :!: o3 <- f sparseIdx i1 i2 i3
    VGM.unsafeWrite vec2 i o2
    VGM.unsafeWrite vec3 i o3
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
    vec3 = acc3 values
{-# INLINEABLE mapM_idx_1_2 #-}

mapMReturn_idx_1 ::
  ( Storable idx
  , VGM.MVector v1 a1
  ) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (idx -> a1 -> ST s (a1, r)) ->
  ST s [r]
mapMReturn_idx_1 store acc1 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \i -> do
    sparseIdx <- VSM.unsafeRead ds i
    i1 <- VGM.unsafeRead vec1 i
    (o1, r) <- f sparseIdx i1
    VGM.unsafeWrite vec1 i o1
    pure r
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
{-# INLINEABLE mapMReturn_idx_1 #-}

--------------------------------------------------------------------------------
-- Copy

unsafeCopy ::
  (VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  (vs s -> v s a) ->
  ST s ()
unsafeCopy store destAcc srcAcc = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  VGM.unsafeCopy (VGM.slice 0 n dest_vec) (VGM.slice 0 n src_vec)
  where
    values = dvs_values store
    dest_vec = destAcc values
    src_vec = srcAcc values
{-# INLINEABLE unsafeCopy #-}

--------------------------------------------------------------------------------
-- Folds

foldM_idx_1 ::
  (Storable idx, VGM.MVector v1 a1) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (b -> idx -> a1 -> ST s b) ->
  b ->
  ST s b
foldM_idx_1 store acc1 f z = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  foldM
    ( \b denseIdx -> do
        eid <- VSM.unsafeRead ds denseIdx
        i1 <- VGM.unsafeRead vec1 denseIdx
        r <- f b eid i1
        pure $! r
    )
    z
    [0 .. n - 1]
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
{-# INLINEABLE foldM_idx_1 #-}

foldM_idx_2 ::
  (Storable idx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (b -> idx -> a1 -> a2 -> ST s b) ->
  b ->
  ST s b
foldM_idx_2 store acc1 acc2 f z = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  foldM
    ( \b denseIdx -> do
        eid <- VSM.unsafeRead ds denseIdx
        i1 <- VGM.unsafeRead vec1 denseIdx
        i2 <- VGM.unsafeRead vec2 denseIdx
        r <- f b eid i1 i2
        pure $! r
    )
    z
    [0 .. n - 1]
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINEABLE foldM_idx_2 #-}

--------------------------------------------------------------------------------
-- To lists

toList ::
  (Storable idx, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  ST s [(idx, elem)]
toList store = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \i -> do
    (,)
      <$> VSM.unsafeRead (dvs_dense2Sparse store) i
      <*> VecSt.read (`VGM.unsafeRead` i) (dvs_values store)
{-# INLINEABLE toList #-}

toList_1 ::
  forall s idx sidx vs a1 v1 r.
  (VGM.MVector v1 a1) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (a1 -> r) ->
  ST s [r]
toList_1 store acc1 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \denseIdx -> do
    i1 <- VGM.unsafeRead vec1 denseIdx
    pure $! f i1
  where
    values = dvs_values store
    vec1 = acc1 values
{-# INLINEABLE toList_1 #-}

toList_idx_2 ::
  forall s idx sidx vs a1 a2 v1 v2 r.
  (Storable idx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (idx -> a1 -> a2 -> r) ->
  ST s [r]
toList_idx_2 store acc1 acc2 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \denseIdx -> do
    eid <- VGM.unsafeRead ds denseIdx
    i1 <- VGM.unsafeRead vec1 denseIdx
    i2 <- VGM.unsafeRead vec2 denseIdx
    pure $! f eid i1 i2
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINEABLE toList_idx_2 #-}

toListM_idx_2 ::
  forall s idx sidx vs a1 a2 v1 v2 r.
  (Storable idx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (idx -> a1 -> a2 -> ST s r) ->
  ST s [r]
toListM_idx_2 store acc1 acc2 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  forM [0 .. n - 1] $ \denseIdx -> do
    eid <- VGM.unsafeRead ds denseIdx
    i1 <- VGM.unsafeRead vec1 denseIdx
    i2 <- VGM.unsafeRead vec2 denseIdx
    f eid i1 i2
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values
{-# INLINEABLE toListM_idx_2 #-}

toListMaybe_idx_2 ::
  forall s idx sidx vs a1 a2 v1 v2 r.
  (Storable idx, VGM.MVector v1 a1, VGM.MVector v2 a2) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (idx -> a1 -> a2 -> Maybe r) ->
  ST s [r]
toListMaybe_idx_2 store acc1 acc2 f = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  mapMaybeHelper [0 .. n - 1]
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values

    mapMaybeHelper :: [Int] -> ST s [r]
    mapMaybeHelper [] = pure []
    mapMaybeHelper (i : is) = do
      f
        <$> VSM.unsafeRead ds i
        <*> VGM.unsafeRead vec1 i
        <*> VGM.unsafeRead vec2 i
        >>= \case
          Nothing -> mapMaybeHelper is
          Just r -> (:) r <$> mapMaybeHelper is
{-# INLINEABLE toListMaybe_idx_2 #-}

toListPred_idx_2 ::
  forall s idx sidx vs elem a1 a2 v1 v2.
  (Storable idx, VGM.MVector v1 a1, VGM.MVector v2 a2, VecStore elem vs) =>
  DenseStore s idx sidx vs ->
  (vs s -> v1 s a1) ->
  (vs s -> v2 s a2) ->
  (idx -> a1 -> a2 -> Bool) ->
  ST s [(idx, elem)]
toListPred_idx_2 store acc1 acc2 predicate = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  filterMHelper [0 .. n - 1]
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
    vec2 = acc2 values

    filterMHelper :: [Int] -> ST s [(idx, elem)]
    filterMHelper [] = pure []
    filterMHelper (i : is) = do
      sparseIdx <- VSM.unsafeRead ds i
      a1 <- VGM.unsafeRead vec1 i
      a2 <- VGM.unsafeRead vec2 i
      if predicate sparseIdx a1 a2
        then do
          e <- VecSt.read (`VGM.unsafeRead` i) (dvs_values store)
          (:) (sparseIdx, e) <$> filterMHelper is
        else filterMHelper is
{-# INLINEABLE toListPred_idx_2 #-}

indices ::
  (Storable idx) =>
  DenseStore s idx sidx vs ->
  ST s [idx]
indices store = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  for [0 .. n - 1] $ VSM.unsafeRead ds
  where
    ds = dvs_dense2Sparse store
{-# INLINEABLE indices #-}

indicesPred_1 ::
  (Storable idx, VGM.MVector v a) =>
  DenseStore s idx sidx vs ->
  (vs s -> v s a) ->
  (a -> Bool) ->
  ST s [idx]
indicesPred_1 store acc1 predicate = do
  n <- fromIntegral <$> readSTRef (dvs_count store)
  denseIndices <-
    flip filterM [0 .. n - 1] $ fmap predicate . VGM.unsafeRead vec1
  traverse (VGM.unsafeRead ds) denseIndices
  where
    ds = dvs_dense2Sparse store
    values = dvs_values store
    vec1 = acc1 values
{-# INLINEABLE indicesPred_1 #-}
