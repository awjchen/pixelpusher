{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

-- | Each game entity is associated with a unique 'EntityID' with which the
-- entity can be identified. This is acheived by having each 'EntityID' contain
-- a unique index (an `Int`) that acts as a key for inserting or retrieving
-- data components from various data stores.
--
-- 'EntityID's also contain a tag indicating the type of its entity (e.g.
-- overseer, drone, ...).
--
-- Having tags embedded within 'EntityID's lets us discover the type of an
-- entity at any time. We also annotate our 'EntityID' types with type-level
-- tags to preserve this this information, once we discover it.
--
-- This module defines entity types and subsets of entity types that can be
-- used to define and track entity invariants (e.g. overseer entities should
-- always have a data component for "dynamics"). However, this module alone
-- does not enforce meaningful type-safety; we ultimately depend on the data
-- stores and their operations to preserve invariants.
module Pixelpusher.Store.EntityID (
  -- Types of game entities
  EntityType (..),
  -- EntityID types
  EntityID,
  SEntityID,
  IsEntityID (..),
  -- Subsets of entity types
  EntityTypeSet (..),
  IsEntityTypeSet,
  refineEntityID,
  SubEntityID,
  subEntityID,
  ViewEntityID (..),
  -- EntityID pools
  EntityIDPool,
  EntityIDPoolSnapshot,
  newEntityIDPool,
  clearEntityIDPool,
  borrowEntityID,
  returnEntityID,
  -- Packing pairs of 'EntityID's into a word
  EntityIDPair,
  entityIDPair,
  getEntityIDPair,
) where

import Control.Monad.ST
import Data.Bits (shiftL, shiftR, (.&.), (.|.))
import Data.Coerce (coerce)
import Data.Generics.Product.Fields
import Data.Hashable (Hashable)
import Data.Kind (Constraint, Type)
import Data.STRef
import Data.Serialize (Serialize)
import Data.Type.Bool (type (&&))
import Data.Word (Word16)
import Foreign.Storable
import GHC.Generics (Generic)
import GHC.TypeError (ErrorMessage (..), Unsatisfiable)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS

--------------------------------------------------------------------------------
-- Types of game entities

-- | An enumeration of all entity types.
type data EntityType
  = Overseer
  | Drone
  | SoftObstacle
  | PsiStorm
  | Flag
  | TeamBase
  | BaseFlag
  | Explosion
  | SpawnArea
  | Powerup

-- | How many bits of an 'EntityID' to use for tags. The rest of the bits are
-- used for the index. Must be consistent with 'tagMask', and must be large
-- enough to represent all values of 'EntityType'.
tagBits :: Int
tagBits = 4
{-# INLINE tagBits #-}

-- | A bitmask with 1s at the tag bits. Must be consistent with 'tagShift'.
tagMask :: Word16
tagMask = 0xf
{-# INLINE tagMask #-}

-- | Internal type
newtype Tag = Tag Word16
  deriving newtype (Eq)

-- Tag patterns

{-# COMPLETE
  OverseerTag
  , DroneTag
  , SoftObstacleTag
  , PsiStormTag
  , FlagTag
  , TeamBaseTag
  , BaseFlagTag
  , ExplosionTag
  , SpawnAreaTag
  , PowerupTag
  #-}
pattern OverseerTag :: Tag
pattern OverseerTag = Tag 0
pattern DroneTag :: Tag
pattern DroneTag = Tag 1
pattern SoftObstacleTag :: Tag
pattern SoftObstacleTag = Tag 2
pattern PsiStormTag :: Tag
pattern PsiStormTag = Tag 3
pattern FlagTag :: Tag
pattern FlagTag = Tag 4
pattern TeamBaseTag :: Tag
pattern TeamBaseTag = Tag 5
pattern BaseFlagTag :: Tag
pattern BaseFlagTag = Tag 6
pattern ExplosionTag :: Tag
pattern ExplosionTag = Tag 7
pattern SpawnAreaTag :: Tag
pattern SpawnAreaTag = Tag 8
pattern PowerupTag :: Tag
pattern PowerupTag = Tag 9

-- | Mapping type-level entity types to concrete tags. Used only in
-- constructing new 'EntityID's.
class KnownEntityType (entity :: EntityType) where
  entityTypeTag :: Tag

instance KnownEntityType Overseer where entityTypeTag = OverseerTag
instance KnownEntityType Drone where entityTypeTag = DroneTag
instance KnownEntityType SoftObstacle where entityTypeTag = SoftObstacleTag
instance KnownEntityType PsiStorm where entityTypeTag = PsiStormTag
instance KnownEntityType Flag where entityTypeTag = FlagTag
instance KnownEntityType TeamBase where entityTypeTag = TeamBaseTag
instance KnownEntityType BaseFlag where entityTypeTag = BaseFlagTag
instance KnownEntityType Explosion where entityTypeTag = ExplosionTag
instance KnownEntityType SpawnArea where entityTypeTag = SpawnAreaTag
instance KnownEntityType Powerup where entityTypeTag = PowerupTag

--------------------------------------------------------------------------------
-- EntityID types

-- | An 'EntityID' tagged with its entity type.
newtype EntityID (t :: EntityType) = EntityID {unEntityID :: Word16}
  deriving newtype
    (Eq, Ord, Hashable, WIM.IsInt, WIS.IsInt, Serialize, Storable)

-- | An 'EntityID' with an unknown entity type belonging to some set of entity
-- types. Mnemonic: S for set.
newtype SEntityID (s :: EntityTypeSet) = SEntityID {unSEntityID :: Word16}
  deriving newtype
    (Eq, Ord, Hashable, WIM.IsInt, WIS.IsInt, Serialize, Storable)

-- | Internal constructor
makeEntityID :: Tag -> Word16 -> EntityID t
makeEntityID (Tag tag) index = EntityID $ shiftL index tagBits .|. tag

-- | Internal accessor
getEntityIndex :: EntityID t -> Word16
getEntityIndex = (`shiftR` tagBits) . unEntityID

-- | Internal accessor
getEntityTag :: EntityID t -> Tag
getEntityTag = Tag . (.&. tagMask) . unEntityID

-- | Internal accessor
getSEntityTag :: SEntityID s -> Tag
getSEntityTag = getEntityTag . coerce

-- | Functions on 'EntityID' and its variants (only 'SEntityID' at the moment).
class IsEntityID a where
  -- | Get the index associated with an 'EntityID'
  getIndex :: a -> Int

  -- | Test whether two 'EntityID's have the same entity type.
  eqEntityType :: a -> a -> Bool

instance IsEntityID (EntityID t) where
  getIndex = fromIntegral . getEntityIndex
  {-# INLINE getIndex #-}
  eqEntityType eid1 eid2 = getEntityTag eid1 == getEntityTag eid2
  {-# INLINE eqEntityType #-}

instance IsEntityID (SEntityID s) where
  getIndex = fromIntegral . getEntityIndex . coerce
  {-# INLINE getIndex #-}
  eqEntityType eid1 eid2 =
    getEntityTag (coerce eid1) == getEntityTag (coerce eid2)
  {-# INLINE eqEntityType #-}

--------------------------------------------------------------------------------
-- Sets of entity types

-- | An enumeration of sets of entity types.
type data EntityTypeSet
  = -- | All entity types
    AnyEntity
  | -- | Entities guaranteed to have dynamics components
    DynamicsEntity
  | -- | Entities guaranteed to have combat components
    CombatEntity
  | -- | Entities guaranteed to have flag components
    FlagEntity
  | -- | Entities that /may/ have actor components
    MaybeActorEntity
  | -- | Entities guaranteed to have actor components
    ActorEntity
  | -- | Entities guaranteed to have transitory components
    TransitoryEntity
  | -- | Entities that /may/ have transitory components
    MaybeTransitoryEntity
  | -- | Entities guaranteed to have team components
    TeamEntity
  | -- | Entities guaranteed to have player class components
    PlayerClassEntity

-- | A class for defining sets of entity types.
class IsEntityTypeSet (set :: EntityTypeSet) where
  -- | Internal. A list of entity types representing the members of the sets.
  type SetMembers (set :: EntityTypeSet) :: [EntityType]

  -- | Internal. Refine a 'SEntityID' to discover its entity type.
  _refineEntityID :: SEntityID set -> ViewEntityID set

-- | Refine a 'SEntityID' to discover its entity type.
refineEntityID :: (IsEntityTypeSet set) => SEntityID set -> ViewEntityID set
refineEntityID = _refineEntityID
{-# INLINE refineEntityID #-}

-- | The result of refining an 'SEntityID' to discover its entity type.
--
-- This type has a constructor for each entity type. These constructors are
-- "accessible" exactly when the corresponding entity type belongs to the
-- entity type subset indicated by the type parameter.
data ViewEntityID :: EntityTypeSet -> Type where
  OverseerID ::
    (IsSetMember Overseer subsetType) =>
    EntityID Overseer ->
    ViewEntityID subsetType
  DroneID ::
    (IsSetMember Drone subsetType) =>
    EntityID Drone ->
    ViewEntityID subsetType
  SoftObstacleID ::
    (IsSetMember SoftObstacle subsetType) =>
    EntityID SoftObstacle ->
    ViewEntityID subsetType
  PsiStormID ::
    (IsSetMember PsiStorm subsetType) =>
    EntityID PsiStorm ->
    ViewEntityID subsetType
  FlagID ::
    (IsSetMember Flag subsetType) =>
    EntityID Flag ->
    ViewEntityID subsetType
  TeamBaseID ::
    (IsSetMember TeamBase subsetType) =>
    EntityID TeamBase ->
    ViewEntityID subsetType
  BaseFlagID ::
    (IsSetMember BaseFlag subsetType) =>
    EntityID BaseFlag ->
    ViewEntityID subsetType
  ExplosionID ::
    (IsSetMember Explosion subsetType) =>
    EntityID Explosion ->
    ViewEntityID subsetType
  SpawnAreaID ::
    (IsSetMember SpawnArea subsetType) =>
    EntityID SpawnArea ->
    ViewEntityID subsetType
  PowerupID ::
    (IsSetMember Powerup subsetType) =>
    EntityID Powerup ->
    ViewEntityID subsetType

-- | Subtype relationship between entity IDs.
class SubEntityID (entityID1 :: Type) (entityID2 :: Type) where
  -- Internal
  _subEntityID :: entityID1 -> entityID2

instance SubEntityID (EntityID entityType) (EntityID entityType) where
  _subEntityID = id
  {-# INLINE _subEntityID #-}

instance
  (IsSetMember entityType subsetType) =>
  SubEntityID (EntityID entityType) (SEntityID subsetType)
  where
  _subEntityID = coerce
  {-# INLINE _subEntityID #-}

instance
  (IsSubset subsetType supersetType) =>
  SubEntityID (SEntityID subsetType) (SEntityID supersetType)
  where
  _subEntityID = coerce
  {-# INLINE _subEntityID #-}

-- | Weaken an entity ID, converting it to one of its supertypes.
subEntityID :: (SubEntityID entityID1 entityID2) => entityID1 -> entityID2
subEntityID = _subEntityID
{-# INLINE subEntityID #-}

type IsSetMember :: EntityType -> EntityTypeSet -> Constraint
type family IsSetMember entityType subsetType where
  IsSetMember entityType subsetType =
    IsTrue
      ( 'Text "Entity type "
          ':<>: 'ShowType entityType
          ':<>: 'Text " not a member of "
          ':<>: 'ShowType subsetType
      )
      (Elem entityType (SetMembers subsetType))

type IsSubset :: EntityTypeSet -> EntityTypeSet -> Constraint
type family IsSubset subset superset where
  IsSubset subset superset =
    IsTrue
      ( 'Text "Entity type set "
          ':<>: 'ShowType subset
          ':<>: 'Text " not a subset of "
          ':<>: 'ShowType superset
      )
      (Subset (SetMembers subset) (SetMembers superset))

-- Helper
type IsTrue :: ErrorMessage -> Bool -> Constraint
type family IsTrue errMsg bool where
  IsTrue _errMsg 'True = ()
  IsTrue errMsg 'False = Unsatisfiable errMsg

-- Helper. 'Data.List.elem' at the type-level.
type Elem :: EntityType -> [EntityType] -> Bool
type family Elem a as where
  Elem a '[] = 'False
  Elem a (a : as) = 'True
  Elem a (_ : as) = Elem a as

-- Helper
type Subset :: [EntityType] -> [EntityType] -> Bool
type family Subset xs ys where
  Subset '[] ys = 'True
  Subset (x ': xs) ys = Elem x ys && Subset xs ys

----------------------------------------------------------------------------------
-- Definitions of entity type subsets

instance IsEntityTypeSet AnyEntity where
  type
    SetMembers AnyEntity =
      '[ Overseer
       , Drone
       , SoftObstacle
       , PsiStorm
       , Flag
       , TeamBase
       , BaseFlag
       , Explosion
       , SpawnArea
       , Powerup
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    SoftObstacleTag -> SoftObstacleID (coerce eid)
    PsiStormTag -> PsiStormID (coerce eid)
    FlagTag -> FlagID (coerce eid)
    TeamBaseTag -> TeamBaseID (coerce eid)
    BaseFlagTag -> BaseFlagID (coerce eid)
    ExplosionTag -> ExplosionID (coerce eid)
    SpawnAreaTag -> SpawnAreaID (coerce eid)
    PowerupTag -> PowerupID (coerce eid)

instance IsEntityTypeSet DynamicsEntity where
  type
    SetMembers DynamicsEntity =
      '[ Overseer
       , Drone
       , SoftObstacle
       , PsiStorm
       , Flag
       , TeamBase
       , BaseFlag
       , Explosion
       , SpawnArea
       , Powerup
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    SoftObstacleTag -> SoftObstacleID (coerce eid)
    PsiStormTag -> PsiStormID (coerce eid)
    FlagTag -> FlagID (coerce eid)
    TeamBaseTag -> TeamBaseID (coerce eid)
    BaseFlagTag -> BaseFlagID (coerce eid)
    ExplosionTag -> ExplosionID (coerce eid)
    SpawnAreaTag -> SpawnAreaID (coerce eid)
    PowerupTag -> PowerupID (coerce eid)
    where
      _err = error "unexpected tag when matching SEntityID DynamicsEntity"

instance IsEntityTypeSet CombatEntity where
  type
    SetMembers CombatEntity =
      '[ Overseer
       , Drone
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID CombatEntity"

instance IsEntityTypeSet FlagEntity where
  type
    SetMembers FlagEntity =
      '[ Overseer
       , Flag
       , TeamBase
       , BaseFlag
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    FlagTag -> FlagID (coerce eid)
    TeamBaseTag -> TeamBaseID (coerce eid)
    BaseFlagTag -> BaseFlagID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID FlagEntity"

instance IsEntityTypeSet MaybeActorEntity where
  type
    SetMembers MaybeActorEntity =
      '[ Overseer
       , Drone
       , PsiStorm
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    PsiStormTag -> PsiStormID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID MaybeActorEntity"

instance IsEntityTypeSet ActorEntity where
  type
    SetMembers ActorEntity =
      '[ Overseer
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID ActorEntity"

instance IsEntityTypeSet TransitoryEntity where
  type
    SetMembers TransitoryEntity =
      '[ PsiStorm
       , Explosion
       ]
  _refineEntityID eid = case getSEntityTag eid of
    PsiStormTag -> PsiStormID (coerce eid)
    ExplosionTag -> ExplosionID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID TransitoryEntity"

instance IsEntityTypeSet MaybeTransitoryEntity where
  type
    SetMembers MaybeTransitoryEntity =
      '[ PsiStorm
       , Explosion
       , Powerup
       ]
  _refineEntityID eid = case getSEntityTag eid of
    PsiStormTag -> PsiStormID (coerce eid)
    ExplosionTag -> ExplosionID (coerce eid)
    PowerupTag -> PowerupID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID MaybeTransitoryEntity"

instance IsEntityTypeSet TeamEntity where
  type
    SetMembers TeamEntity =
      '[ Overseer
       , Drone
       , PsiStorm
       , Flag
       , TeamBase
       , BaseFlag
       , SpawnArea
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    FlagTag -> FlagID (coerce eid)
    TeamBaseTag -> TeamBaseID (coerce eid)
    BaseFlagTag -> BaseFlagID (coerce eid)
    SpawnAreaTag -> SpawnAreaID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID TeamEntity"

instance IsEntityTypeSet PlayerClassEntity where
  type
    SetMembers PlayerClassEntity =
      '[ Overseer
       , Drone
       ]
  _refineEntityID eid = case getSEntityTag eid of
    OverseerTag -> OverseerID (coerce eid)
    DroneTag -> DroneID (coerce eid)
    _ -> err
    where
      err = error "unexpected tag when matching SEntityID PlayerClassEntity"

----------------------------------------------------------------------------------
-- EntityID pools

-- | A mutable data structure for handing out unique 'EntityID's.
newtype EntityIDPool s = EntityIDPool (STRef s EntityIDPoolSnapshot)
  deriving newtype (Copyable s, Mutable s EntityIDPoolSnapshot)

data EntityIDPoolSnapshot = EntityIDPoolSnapshot
  { pool_nDistinct :: Word16
  -- ^ The number of distinct indices given out
  , pool_returnedIndices :: WIS.IntSet Word16
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

emptySnapshot :: EntityIDPoolSnapshot
emptySnapshot =
  EntityIDPoolSnapshot
    { pool_nDistinct = 0
    , pool_returnedIndices = WIS.empty
    }

newEntityIDPool :: ST s (EntityIDPool s)
newEntityIDPool = EntityIDPool <$> newSTRef emptySnapshot

clearEntityIDPool :: EntityIDPool s -> ST s ()
clearEntityIDPool (EntityIDPool ref) = writeSTRef ref emptySnapshot

borrowEntityID ::
  forall entity s.
  (KnownEntityType entity) =>
  EntityIDPool s ->
  ST s (EntityID entity)
borrowEntityID (EntityIDPool ref) = do
  pool <- readSTRef ref
  case WIS.minView (pool_returnedIndices pool) of
    Nothing -> do
      let !pool' = pool & field @"pool_nDistinct" +~ 1
      writeSTRef ref pool'
      pure $ makeEntityID (entityTypeTag @entity) (pool_nDistinct pool)
    Just (idx, returnedIndices') -> do
      let !pool' = pool & field @"pool_returnedIndices" .~ returnedIndices'
      writeSTRef ref pool'
      pure $ makeEntityID (entityTypeTag @entity) idx

returnEntityID :: EntityIDPool s -> SEntityID AnyEntity -> ST s ()
returnEntityID (EntityIDPool ref) eid =
  modifySTRef' ref $
    over (field @"pool_returnedIndices") $
      WIS.insert $
        getEntityIndex $
          coerce eid

----------------------------------------------------------------------------------
-- Packing (unordered) pairs of 'EntityID's into a word

-- | Internal helper function
packEntityPair :: SEntityID s -> SEntityID s -> Int
packEntityPair
  (fromIntegral . unSEntityID -> eid1)
  (fromIntegral . unSEntityID -> eid2)
    | eid1 <= eid2 = eid1 + shiftL eid2 16
    | otherwise = eid2 + shiftL eid1 16

-- | Internal helper function
unpackEntityPair :: Int -> (SEntityID a, SEntityID b)
unpackEntityPair i =
  ( SEntityID $ fromIntegral $ i .&. 65535
  , SEntityID $ fromIntegral $ shiftR i 16
  )

-- | Unordered pairs of entity IDs.
newtype EntityIDPair (store :: EntityTypeSet) = EntityIDPair Int
  deriving stock (Show)

instance WIS.IsInt (EntityIDPair a) where
  toInt (EntityIDPair i) = WIS.makeIdentity i
  {-# INLINE toInt #-}
  fromInt = WIS.makeIdentity . EntityIDPair
  {-# INLINE fromInt #-}

entityIDPair :: SEntityID s -> SEntityID s -> EntityIDPair s
entityIDPair eid1 eid2 = EntityIDPair (packEntityPair eid1 eid2)

getEntityIDPair :: EntityIDPair s -> (SEntityID s, SEntityID s)
getEntityIDPair (EntityIDPair i) = unpackEntityPair i
