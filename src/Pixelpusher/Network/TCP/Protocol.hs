-- | Protocol for sending messages over TCP
module Pixelpusher.Network.TCP.Protocol (
  TCPConnection,
  ReconnectFlag (..),
  TCPConnectionClosedException (..),
  tcpSendData,
  tcpSendClose,
  tcpRecvData,
  withTcpClient,
  IpVersion (..),
  runTCPServer,
) where

import Control.Applicative
import Control.Concurrent (forkFinally, forkIO)
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Exception
import Control.Monad (forever, when)
import Data.Bits (finiteBitSize)
import Data.ByteString qualified as BS
import Data.Functor (void, ($>))
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Serialize.Text ()
import Data.Text (Text)
import Data.Word (Word32)
import GHC.Generics
import Network.Socket
import Network.Socket.ByteString

--------------------------------------------------------------------------------

-- We use a trivial protocol for sending send messages over TCP: we prefix each
-- message by its MsgLenRep. We also try to work around the use of blocking
-- network IO by the Windows runtime.

newtype TCPConnection = TCPConnection {getSocket :: Socket}

type MsgLenRep = Word32

lengthBytes :: Int
lengthBytes = fromIntegral $ finiteBitSize @MsgLenRep 0 `div` 8

-- | Message types
data Message
  = DataMsg BS.ByteString
  | CloseMsg ReconnectFlag Text
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ReconnectFlag = Reconnect | DoNotReconnect
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

-- | A exception signalling the closure of the connection. May provide a reason
-- for the closure, and whether your peer intends for you to recononect.
data TCPConnectionClosedException
  = ClosedBecause ReconnectFlag Text
  | UnexpectedClose
  deriving stock (Show)

instance Exception TCPConnectionClosedException

tcpSendData :: TCPConnection -> BS.ByteString -> IO ()
tcpSendData tcpConn = tcpSendMessage tcpConn . DataMsg

tcpSendClose :: TCPConnection -> ReconnectFlag -> Text -> IO ()
tcpSendClose tcpConn reconnect reason =
  tcpSendMessage tcpConn $ CloseMsg reconnect reason

tcpSendMessage :: TCPConnection -> Message -> IO ()
tcpSendMessage tcpConn msg = do
  let payload = Serialize.encode msg
  sendAll (getSocket tcpConn) $
    Serialize.encode @MsgLenRep (fromIntegral $ BS.length payload) <> payload

-- Throws a `TCPConnectionClosedException` when either a close message is
-- received or when TCP the connection is closed.
tcpRecvData :: TCPConnection -> IO BS.ByteString
tcpRecvData tcpConn = do
  lenPrefixBytes <- recvAll tcpConn lengthBytes
  let lenPrefix =
        fromIntegral $
          either error id $
            Serialize.decode @MsgLenRep lenPrefixBytes

  msgBytes <- recvAll tcpConn lenPrefix

  case either error id $ Serialize.decode @Message msgBytes of
    CloseMsg reconnect reason -> throwIO $ ClosedBecause reconnect reason
    DataMsg bs -> pure bs

-- Throws a `TCPConnectionClosedException` when either a close message is
-- received or when TCP the connection is closed.
recvAll :: TCPConnection -> Int -> IO BS.ByteString
recvAll tcpConn size
  | size <= 0 = pure BS.empty
  | otherwise = do
      bytes <- recv (getSocket tcpConn) size
      when (BS.null bytes) $ throwIO UnexpectedClose
      if BS.length bytes < size
        then do
          let missing = size - BS.length bytes
          moreBytes <- recvAll tcpConn missing
          pure $ bytes <> moreBytes
        else pure bytes

--------------------------------------------------------------------------------
-- Client and server

-- | Attempt to connect to the given server over both IPv4 and IPv6, using
-- whichever connection succeeds first and closing the other. Returns a
-- connected TCP socket (`TCPConnection`) and the address family used by the
-- socket (which will be one of `AF_INET` or `AF_INET6`).
--
-- This function opens and closes TCP sockets asynchronously to work around the
-- uninterruptible system IO of GHC's Windows runtime. See
-- https://gitlab.haskell.org/ghc/ghc/-/issues/7353
withTcpClient ::
  HostName -> ServiceName -> (IpVersion -> TCPConnection -> IO a) -> IO a
withTcpClient hostName serviceName action = do
  let getTcpAddrInfo :: Family -> IO (Either SomeException AddrInfo)
      getTcpAddrInfo family =
        try @SomeException $ do
          let hints = defaultHints{addrSocketType = Stream, addrFamily = family}
          List.NonEmpty.head
            <$> getAddrInfo (Just hints) (Just hostName) (Just serviceName)
  addrInfo4Either <- getTcpAddrInfo AF_INET
  addrInfo6Either <- getTcpAddrInfo AF_INET6

  case (addrInfo4Either, addrInfo6Either) of
    (Left e4, Left _e6) ->
      throwIO e4
    (Left _, Right addrInfo6) ->
      withClientTcpSocket addrInfo6 $ \sock6 -> do
        asyncConnect sock6 addrInfo6
        action Ipv6 (TCPConnection sock6)
    (Right addrInfo4, Left _) ->
      withClientTcpSocket addrInfo4 $ \sock4 -> do
        asyncConnect sock4 addrInfo4
        action Ipv4 (TCPConnection sock4)
    (Right addrInfo4, Right addrInfo6) ->
      withClientTcpSocket addrInfo6 $ \sock6 ->
        withClientTcpSocket addrInfo4 $ \sock4 -> do
          (family, tcpConn, socketToClose) <-
            firstToSucceed
              ( asyncConnect sock6 addrInfo6
                  $> (Ipv6, TCPConnection sock6, sock4)
              )
              ( asyncConnect sock4 addrInfo4
                  $> (Ipv4, TCPConnection sock4, sock6)
              )
          asyncClose socketToClose
          action family tcpConn

data IpVersion = Ipv4 | Ipv6

withClientTcpSocket :: AddrInfo -> (Socket -> IO a) -> IO a
withClientTcpSocket addrInfo action =
  bracket (openSocket addrInfo) asyncClose $ \sock -> do
    setSocketOption sock NoDelay 1
    action sock

-- | Waits for the first action to return without throwing an exception and
-- cancels the other action. If both actions throw an exception, the last
-- exception thrown is rethrown.
firstToSucceed :: IO a -> IO a -> IO a
firstToSucceed ioLeft ioRight =
  withAsync ioLeft $ \asyncLeft ->
    withAsync ioRight $ \asyncRight -> do
      resultFirst <-
        atomically $
          Left <$> waitCatchSTM asyncLeft <|> Right <$> waitCatchSTM asyncRight
      case resultFirst of
        Left (Left _) -> wait asyncRight
        Left (Right valueLeft) -> pure valueLeft
        Right (Left _) -> wait asyncLeft
        Right (Right valueRight) -> pure valueRight

-- | Call `connect` asynchronously to work around the uninterruptible system IO
-- of GHC's Windows runtime. See
-- https://gitlab.haskell.org/ghc/ghc/-/issues/7353
asyncConnect :: Socket -> AddrInfo -> IO ()
asyncConnect sock addrInfo = do
  connectAsync <- async $ connect sock (addrAddress addrInfo)
  wait connectAsync `onException` forkIO (cancel connectAsync)

-- | Call `gracefulClose` asynchronously to work around the uninterruptible
-- system IO of GHC's Windows runtime. See
-- https://gitlab.haskell.org/ghc/ghc/-/issues/7353
asyncClose :: Socket -> IO ()
asyncClose sock = void $ forkIO (gracefulClose sock 5000)

runTCPServer ::
  Maybe HostName -> ServiceName -> (TCPConnection -> IO a) -> IO S
runTCPServer mHostName serviceName server = do
  let hints =
        defaultHints
          { addrFlags = [AI_PASSIVE]
          , addrSocketType = Stream
          , addrFamily = AF_INET6
          }
  addr <-
    List.NonEmpty.head <$> getAddrInfo (Just hints) mHostName (Just serviceName)
  bracket (open addr) close loop
  where
    open :: AddrInfo -> IO Socket
    open addr = do
      sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
      setSocketOption sock ReuseAddr 1
      setSocketOption sock NoDelay 1
      withFdSocket sock setCloseOnExecIfNeeded
      bind sock $ addrAddress addr
      listen sock 1024
      pure sock

    loop :: Socket -> IO a
    loop sock = forever $ do
      (conn, _peer) <- accept sock
      void $
        forkFinally
          (server (TCPConnection conn))
          (const $ handle printException $ gracefulClose conn 5000)

-- | A catch-all handler
printException :: SomeException -> IO ()
printException = print
