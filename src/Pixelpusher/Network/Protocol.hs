-- | Data-agnostic protocol for communication between client and server
module Pixelpusher.Network.Protocol (
  ServerTCPMsg (..),
  ServerUDPMsg (..),
  ClientTCPMsg (..),
  ClientUDPMsg (..),
  ClientUDPData (..),
  SeqNum,
  getSeqNum,
  compareSeqNum,
  firstSeqNum,
  incrementSeqNum,
  shiftSeqNum,
  Acks,
  nullAck,
  insertSeqNum,
  diffAck,
  betweenAcks,
) where

import Data.Bits
import Data.Coerce (coerce)
import Data.Hashable (Hashable)
import Data.Int (Int16)
import Data.List (unfoldr)
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Word (Word64)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Util (listSetBits)

--------------------------------------------------------------------------------
-- Messages from the server to the client

data ServerTCPMsg a
  = ServerReturnPing Int
  | ServerTCPMsg_Data a
  deriving stock (Generic)

instance (Serialize a) => Serialize (ServerTCPMsg a)

data ServerUDPMsg a = ServerUDPMsg SeqNum a
  deriving stock (Generic)

instance (Serialize a) => Serialize (ServerUDPMsg a) where
  get = ServerUDPMsg <$> Serialize.get <*> Serialize.get
  put (ServerUDPMsg seqNum payload) =
    Serialize.put seqNum >> Serialize.put payload

--------------------------------------------------------------------------------
-- Messages from the client to the server

data ClientTCPMsg a
  = -- | For latency measurement
    ClientStillAlivePing Int
  | ClientTCPMsg_Data a
  deriving stock (Generic)

instance (Serialize a) => Serialize (ClientTCPMsg a)

data ClientUDPMsg a
  = ClientUDPMsg_AuthToken Int
  | ClientUDPMsg_Data (ClientUDPData a)
  deriving stock (Generic)

instance (Serialize a) => Serialize (ClientUDPMsg a)

data ClientUDPData a = ClientUDPData
  { clientUDPData_sequenceNumber :: SeqNum
  , clientUDPData_acks :: Acks
  , clientUDPData_payload :: a
  }
  deriving stock (Generic)

instance (Serialize a) => Serialize (ClientUDPData a)

--------------------------------------------------------------------------------
-- Message sequence numbers

newtype SeqNum = SeqNum Int16
  deriving newtype (Eq, Serialize, Hashable)

getSeqNum :: SeqNum -> Int16
getSeqNum (SeqNum n) = n

subtractSeqNum :: SeqNum -> SeqNum -> Int16
subtractSeqNum (SeqNum m) (SeqNum n) = m - n

compareSeqNum :: SeqNum -> SeqNum -> Ordering
compareSeqNum m n = 0 `compare` (n `subtractSeqNum` m)

-- | The sequence number that should be used for the first message of a
-- sequence
firstSeqNum :: SeqNum
firstSeqNum = SeqNum 1

-- | Used to by `Acks` to acknowledge the reception of no messages in the early
-- stages of communication
nullSeqNum :: SeqNum
nullSeqNum = let (SeqNum n) = firstSeqNum in SeqNum (n - 1)

incrementSeqNum :: SeqNum -> SeqNum
incrementSeqNum (SeqNum n) = SeqNum (n + 1)

shiftSeqNum :: Int16 -> SeqNum -> SeqNum
shiftSeqNum m (SeqNum n) = SeqNum (m + n)

--------------------------------------------------------------------------------
-- Acknowledgments

-- | Acknowledgments of the reception of messages, which are identified by
-- sequence numbers. This type provides acknowledgments of a contiguous
-- interval of sequence numbers.
data Acks = Acks
  { ackLatestSeqNum :: SeqNum
  -- ^ The highest received sequence number
  , ackPrevSeqNums :: Word64
  -- ^ Acknowledgments for the preceeding sequence numbers
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | An empty acknowledgment, one that acknowledges no messages. Because
-- sequence numbers wrap around, this acknowledgment only conveys its intended
-- meaning in at the beginning of communication when no messages have been
-- received, and thus should only be sent at such times.
nullAck :: Acks
nullAck = Acks nullSeqNum zeroBits

-- | Update an acknowledgment by adding a new sequence number
insertSeqNum :: SeqNum -> Acks -> Acks
insertSeqNum seqNum acks
  | diff > 0 =
      Acks
        { ackLatestSeqNum = seqNum
        , ackPrevSeqNums = setBit (shiftL (ackPrevSeqNums acks) diff) (diff - 1)
        }
  | diff < 0 = acks{ackPrevSeqNums = setBit (ackPrevSeqNums acks) (negate diff - 1)}
  | otherwise = acks
  where
    diff = fromIntegral $ seqNum `subtractSeqNum` ackLatestSeqNum acks

-- | `diffAck oldAck newAck` returns a list of sequence numbers acknowledged by
-- `newAck` but not by `oldAck`. The returned list of sequence numbers is
-- strictly decreasing.
diffAck :: Acks -> Acks -> [SeqNum]
diffAck oldAck newAck
  | diff >= 0 =
      let SeqNum latest = ackLatestSeqNum newAck
          oldBits = ackPrevSeqNums oldAck
          (prependLatest, oldBitsShifted) =
            if diff == 0
              then (id, oldBits)
              else ((latest :), setBit (shiftL oldBits diff) (diff - 1))
          newMasked = ackPrevSeqNums newAck .&. complement oldBitsShifted
      in  coerce $
            prependLatest $
              map (\i -> latest - (fromIntegral i + 1)) (listSetBits newMasked)
  -- Under the current protocol (April 27 2021), monotonicty of the latest
  -- sequence number and that of the acknowledgments for each individual
  -- sequence number imply that, in this case (that is, when `diff < 0`), the
  -- `newAck` cannot contain any information not already represented in
  -- `oldAck` (except for old, out-of-scope sequence numbers). Therefore, it
  -- suffices to return an empty diff. Though, it still might be worth
  -- implementing for future-proofing.
  | otherwise = []
  where
    diff = fromIntegral $ ackLatestSeqNum newAck `subtractSeqNum` ackLatestSeqNum oldAck

-- | Returns all the sequence numbers that could have been acknowledged by the
-- new `Acks` but not by the old `Acks`, based on their highest acknowledged
-- sequence numbers.
betweenAcks :: Acks -> Acks -> [SeqNum]
betweenAcks oldAck newAck
  | new - old > 0 = coerce $
      flip unfoldr old $ \i ->
        if i == new then Nothing else let i1 = i + 1 in Just (i1, i1)
  | otherwise = []
  where
    SeqNum old = ackLatestSeqNum oldAck
    SeqNum new = ackLatestSeqNum newAck
