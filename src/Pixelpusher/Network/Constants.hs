module Pixelpusher.Network.Constants where

stillAliveSendInterval_ns :: Int
stillAliveSendInterval_ns = stillAliveCheckInterval_ns `div` 2

stillAliveCheckInterval_ns :: Int
stillAliveCheckInterval_ns = 20_000_000_000
