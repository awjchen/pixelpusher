module Pixelpusher.Game.Dynamics.Collisions.Entities (
  CollisionEntity (..),
  CollisionEntityPair (..),
) where

import Pixelpusher.Game.BotID
import Pixelpusher.Store.EntityID

data CollisionEntity
  = CE_DynEntity (SEntityID DynamicsEntity)
  | -- | Recovery area around non-base flags
    CE_FlagArea (EntityID Flag)
  | CE_BotViewArea BotID
  | CE_PsiStormDamage (EntityID PsiStorm)

-- | Just a strict, monomorphic pair
data CollisionEntityPair = CollisionEntityPair CollisionEntity CollisionEntity
