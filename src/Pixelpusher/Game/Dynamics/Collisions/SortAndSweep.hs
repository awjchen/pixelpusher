{-# LANGUAGE RecordWildCards #-}

-- | Broad-phase collision detection on circular entities
module Pixelpusher.Game.Dynamics.Collisions.SortAndSweep (
  SortAndSweepComponent (..),
  sortAndSweep,
) where

import Control.Monad.ST (runST)
import Data.Bits
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.Vector (Vector)
import Data.Vector qualified as V

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Radix (Radix)
import Pixelpusher.Custom.Radix qualified as Radix
import Pixelpusher.Game.Dynamics.Collisions.Entities

--------------------------------------------------------------------------------

data SortAndSweepComponent = SortAndSweepComponent
  { ssc_entity :: CollisionEntity
  , ssc_radius :: Fixed
  , ssc_pos :: {-# UNPACK #-} Fixed2
  }

-- | Find exact collisions between the given components
sortAndSweep :: Fixed -> [SortAndSweepComponent] -> [CollisionEntityPair]
sortAndSweep worldRadius = sweep . sort worldRadius

--------------------------------------------------------------------------------
-- Sort

-- | Given a list of entities, return a sorted list of their axis-aligned
-- boundaries in the x dimension
sort :: Fixed -> [SortAndSweepComponent] -> Vector Boundary
sort worldRadius =
  radixSort
    . catPairs
    . zipWith
      (makeAxisAlignedBoundsX (makeDiscretization worldRadius))
      [0 ..] -- assign a unique ID to each entity

radixSort :: (Radix a) => [a] -> Vector a
radixSort xs = runST $ do
  vm <- V.unsafeThaw $ V.fromList xs
  Radix.sort vm
  V.unsafeFreeze vm

catPairs :: [(a, a)] -> [a]
catPairs = foldr (\(x, y) xs -> x : y : xs) []

-- | Axis-aligned entity bounds to be sorted by discretized x position by radix
-- sort
data Boundary = Boundary
  { boundary_id_isMinBound_position :: Int
  -- ^ The bit structure of this field is as follows:
  -- unique id (n) | isMinBound (1) | discretized x position (14)
  , boundary_component :: SortAndSweepComponent
  }

isMinBound :: Boundary -> Bool
isMinBound boundary =
  boundary_id_isMinBound_position boundary .&. 16384 /= 0 -- 16384 = 2^14

boundaryId :: Boundary -> Int
boundaryId boundary = boundary_id_isMinBound_position boundary `shiftR` 15

instance Radix Boundary where
  passes _ = 2
  {-# INLINE passes #-}
  size _ = 128
  {-# INLINE size #-}
  radix 0 boundary = boundary_id_isMinBound_position boundary .&. 127
  radix _ boundary = (boundary_id_isMinBound_position boundary `shiftR` 7) .&. 127
  {-# INLINE radix #-}

-- | Constants for mapping game-world coordinates to a smaller number of
-- discrete buckets
data Discretization = Discretization
  { discr_bucketsMinusOne :: Int
  , discr_lowerBound :: Fixed
  , discr_scale :: Fixed
  }

makeDiscretization :: Fixed -> Discretization
makeDiscretization worldRadius =
  let bufferSize = 64
      lowerBound = -worldRadius - bufferSize
      upperBound = worldRadius + bufferSize
      bufferedWorldLength = upperBound - lowerBound
      buckets = 16384 -- 2^14; needs to be less than 2^15 to prevent overflow
      scale = fromIntegral buckets / bufferedWorldLength
  in  Discretization
        { discr_bucketsMinusOne = buckets - 1
        , discr_lowerBound = lowerBound
        , discr_scale = scale
        }

discretize :: Discretization -> Fixed -> Int
discretize Discretization{..} x =
  max 0 . min discr_bucketsMinusOne $
    floor $
      discr_scale * (x - discr_lowerBound)

makeAxisAlignedBoundsX ::
  Discretization -> Int -> SortAndSweepComponent -> (Boundary, Boundary)
makeAxisAlignedBoundsX discr uniqueId component =
  let (SortAndSweepComponent _ radius pos) = component
      Fixed2 x _ = pos
      lowerBound =
        Boundary
          { boundary_id_isMinBound_position =
              uniqueId `shiftL` 15 .|. 16384 .|. discretize discr (x - radius) -- 16384 = 2^14
          , boundary_component = component
          }
      upperBound =
        Boundary
          { boundary_id_isMinBound_position =
              uniqueId `shiftL` 15 .|. discretize discr (x + radius)
          , boundary_component = component
          }
  in  (lowerBound, upperBound)

--------------------------------------------------------------------------------
-- Sweep

data SweepState = SweepState
  { sweep_activeComponents :: IntMap SortAndSweepComponent
  -- ^ The entities for which we have encountered their left axis-aligned
  -- boundary and not their right axis-aligned boundary
  , sweep_collisions :: [CollisionEntityPair]
  -- ^ Output collisions
  }

initialSweepState :: SweepState
initialSweepState =
  SweepState
    { sweep_activeComponents = IntMap.empty
    , sweep_collisions = []
    }

-- | Find exact collisions between entities given a sorted list of their
-- axis-aligned boundaries
sweep :: Vector Boundary -> [CollisionEntityPair]
sweep = sweep_collisions . V.foldl' sweepStep initialSweepState

sweepStep :: SweepState -> Boundary -> SweepState
sweepStep SweepState{..} boundary@Boundary{boundary_component = component}
  | isMinBound boundary =
      SweepState
        { sweep_activeComponents =
            IntMap.insert boundaryId' component sweep_activeComponents
        , sweep_collisions =
            let addCollision collisions activeComponent =
                  if areOverlapping activeComponent component
                    then
                      let collision =
                            CollisionEntityPair
                              (ssc_entity component)
                              (ssc_entity activeComponent)
                      in  collision `seq` (collision : collisions)
                    else collisions
            in  IntMap.foldr' (flip addCollision) sweep_collisions sweep_activeComponents
        }
  | otherwise =
      SweepState
        { sweep_activeComponents =
            IntMap.delete boundaryId' sweep_activeComponents
        , sweep_collisions = sweep_collisions
        }
  where
    boundaryId' = boundaryId boundary

-- | Exact collision detection of circles
areOverlapping :: SortAndSweepComponent -> SortAndSweepComponent -> Bool
areOverlapping comp1 comp2 =
  let SortAndSweepComponent _ r1 pos1 = comp1
      SortAndSweepComponent _ r2 pos2 = comp2
      radius = r1 + r2
      displacement = pos1 - pos2
  in  Fixed.normLeq displacement radius
