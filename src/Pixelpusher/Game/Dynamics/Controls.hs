module Pixelpusher.Game.Dynamics.Controls (
  DynamicsControls (..),
  zeroAccelerationControls,
  boundPlayerControls,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Store.DenseStore qualified as DS

import Pixelpusher.Game.State.Store.Dynamics.Types (DynamicsStore')

--------------------------------------------------------------------------------

-- ** Input bounding

-- | The player controls, but in a form more convenient for the dynamics
-- computations.
data DynamicsControls = DynamicsControls
  { dynCtrl_arrowKeyDir :: {-# UNPACK #-} Fixed2
  , dynCtrl_mousePos :: {-# UNPACK #-} Fixed2
  , dynCtrl_droneAccelScale :: Fixed
  }

zeroAccelerationControls :: DynamicsControls
zeroAccelerationControls =
  DynamicsControls
    { dynCtrl_arrowKeyDir = Fixed2 0 0
    , dynCtrl_mousePos = Fixed2 0 0
    , dynCtrl_droneAccelScale = 0
    }

-- | Pre-process player controls for dynamics computations. In particular,
-- bound positions of player mouse cursors to be within a radius of their
-- overseer.
boundPlayerControls ::
  DynamicsParams ->
  DynamicsStore' s ->
  PlayerStatus ->
  PlayerControls ->
  ST s DynamicsControls
boundPlayerControls params dynComps playerStatus playerCtrls =
  maybe zeroAccelerationControls (readControls params playerClass playerCtrls)
    <$> lookupOverseer dynComps playerStatus
  where
    playerClass = ps_playerClass playerStatus

lookupOverseer ::
  DynamicsStore' s -> PlayerStatus -> ST s (Maybe Fixed2)
lookupOverseer dynComps playerStatus =
  case ps_overseerStatus playerStatus of
    OverseerDead{} -> pure Nothing
    OverseerAlive overseerID _ ->
      Just <$> DS.sureLookup_1 dynComps vec_dyn_pos overseerID

readControls ::
  DynamicsParams -> PlayerClass -> PlayerControls -> Fixed2 -> DynamicsControls
readControls params playerClass playerCtrls overseerPos =
  DynamicsControls
    { dynCtrl_arrowKeyDir = arrowKeyDirection playerCtrls
    , dynCtrl_mousePos =
        boundedMousePos params playerClass overseerPos playerCtrls
    , dynCtrl_droneAccelScale = droneAccelScale playerCtrls
    }

-- | Bound the position of a player's mouse cursor to within a radius of their
-- overseer.
boundedMousePos ::
  DynamicsParams -> PlayerClass -> Fixed2 -> PlayerControls -> Fixed2
boundedMousePos params playerClass overseerPos controls =
  boundPosition overseerPos controlRadius pos
  where
    pos = control_worldPos controls
    controlRadius = getClassParam playerClass (gp_controlRadius params)

boundPosition :: Fixed2 -> Fixed -> Fixed2 -> Fixed2
boundPosition center radius pos
  | normDiff <= radius = pos
  | otherwise = center + Fixed.map (* (radius / normDiff)) diff
  where
    diff = pos - center
    normDiff = Fixed.norm diff

-- | Represent the direction of drone acceleration as a scaling factor.
droneAccelScale :: PlayerControls -> Fixed
droneAccelScale controls =
  case controls ^. to control_buttons . control_drone of
    DroneNeutral -> 0
    DroneAttraction -> 1
    DroneRepulsion -> -1
