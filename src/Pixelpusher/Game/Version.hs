module Pixelpusher.Game.Version (
  toByteString,
  fromByteString,
  areCompatible,
) where

import Data.ByteString qualified as BS
import Data.SemVer qualified as SV
import Data.Text.Encoding qualified as Text
import Lens.Micro.Platform.Custom

--------------------------------------------------------------------------------

toByteString :: SV.Version -> BS.ByteString
toByteString = Text.encodeUtf8 . SV.toText

fromByteString :: BS.ByteString -> Either String SV.Version
fromByteString = SV.fromText . Text.decodeUtf8

-- | Check whether two versions (below 1.0.0) are compatible.
areCompatible :: SV.Version -> SV.Version -> Bool
areCompatible v1 v2 =
  v1 ^. SV.major == v2 ^. SV.major
    && v1 ^. SV.minor == v2 ^. SV.minor
