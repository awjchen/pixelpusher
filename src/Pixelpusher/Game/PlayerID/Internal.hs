module Pixelpusher.Game.PlayerID.Internal (
  PlayerID (..),
  unsafeDebugPlayerID,
) where

import Data.Serialize (Serialize)
import Data.Word (Word8)
import Foreign.Storable (Storable)

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS

-- | Identifiers for players within a specific game.
newtype PlayerID = PlayerID {getPlayerID :: Word8}
  deriving newtype (Eq, Ord, Storable, Serialize)

-- | An arbitrary `PlayerID` for debugging purposes. Do not use in serious code.
unsafeDebugPlayerID :: PlayerID
unsafeDebugPlayerID = PlayerID 0

instance Show PlayerID where
  show (PlayerID uid) = show uid

instance WIS.IsInt PlayerID where
  toInt (PlayerID i) = WIS.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIS.makeIdentity . PlayerID . fromIntegral
  {-# INLINE fromInt #-}

instance WIM.IsInt PlayerID where
  toInt (PlayerID i) = WIM.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . PlayerID . fromIntegral
  {-# INLINE fromInt #-}
