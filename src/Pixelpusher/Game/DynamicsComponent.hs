{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.DynamicsComponent (
  DynamicsComponent (..),
  DynamicsComponentVec (..),
  ProtoDynamicsComponent (..),
  newDynamicsComponent,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed
import Pixelpusher.Store.VecStore

--------------------------------------------------------------------------------

-- | Entities that have a spatial presence
data DynamicsComponent = DynamicsComponent
  { dyn_radius :: Fixed
  , dyn_mass :: Fixed
  , dyn_pos :: Fixed2
  , dyn_vel :: Fixed2
  , dyn_pos_prev :: Fixed2
  , dyn_pos_prev2 :: Fixed2
  , dyn_pos_prev3 :: Fixed2
  , dyn_pos_prev4 :: Fixed2
  , dyn_pos_prev5 :: Fixed2
  , dyn_pos_prev6 :: Fixed2
  , dyn_pos_prev7 :: Fixed2
  , dyn_pos_prev8 :: Fixed2
  , dyn_pos_prev9 :: Fixed2
  , dyn_pos_prev10 :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeVecStore ''DynamicsComponent

-- | For making new dynamics components
data ProtoDynamicsComponent = ProtoDynamicsComponent
  { pdyn_radius :: Fixed
  , pdyn_mass :: Fixed
  , pdyn_pos :: Fixed2
  , pdyn_vel :: Fixed2
  }

newDynamicsComponent :: ProtoDynamicsComponent -> DynamicsComponent
newDynamicsComponent ProtoDynamicsComponent{..} =
  DynamicsComponent
    { dyn_radius = pdyn_radius
    , dyn_mass = pdyn_mass
    , dyn_pos = pdyn_pos
    , dyn_vel = pdyn_vel
    , dyn_pos_prev = pdyn_pos
    , dyn_pos_prev2 = pdyn_pos
    , dyn_pos_prev3 = pdyn_pos
    , dyn_pos_prev4 = pdyn_pos
    , dyn_pos_prev5 = pdyn_pos
    , dyn_pos_prev6 = pdyn_pos
    , dyn_pos_prev7 = pdyn_pos
    , dyn_pos_prev8 = pdyn_pos
    , dyn_pos_prev9 = pdyn_pos
    , dyn_pos_prev10 = pdyn_pos
    }
