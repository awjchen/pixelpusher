module Pixelpusher.Game.PlayerStats (
  PlayerStats,
  newPlayerStats,
  AggregatePlayerStats,
  plusAggregateStats,
  -- Update
  incrementKills,
  incrementDeaths,
  incrementAssists,
  addDamageDealt,
  addDamageTaken,
  incrementFlagsCaptured,
  incrementFlagsRecovered,
  -- Access
  getAggregateStats,
  getKills,
  getDeaths,
  getAssists,
  getStreakCurrent,
  getStreakBest,
  getDamageDealt,
  getDamageTaken,
  getFlagsCaptured,
  getFlagsRecovered,
  getDamageTakenTimestamps,
) where

import Control.Monad.Trans.State.Strict (execState)
import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import Data.Word (Word64)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

data PlayerStats = PlayerStats
  { ps_aggregated :: {-# UNPACK #-} AggregatePlayerStats
  , ps_killStreak_current :: Word64
  , -- TODO: Remove actors from this map when they leave the game?
    ps_damageTakenTimestamps :: WIM.IntMap ActorID Time
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | The subset of player statistics for which a (monoidal) combining action
-- makes sense. Only this subset will be preserved across rounds.
data AggregatePlayerStats = AggregatePlayerStats
  { aps_kills :: Word64
  , aps_killStreak_best :: Word64
  , aps_deaths :: Word64
  , aps_assists :: Word64
  , aps_damageDealt :: Word64
  , aps_damageTaken :: Word64
  , aps_flagsCaptured :: Word64
  , aps_flagsRecovered :: Word64
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------

newPlayerStats :: PlayerStats
newPlayerStats =
  PlayerStats
    { ps_aggregated = mempty
    , ps_killStreak_current = 0
    , ps_damageTakenTimestamps = mempty
    }

instance Semigroup AggregatePlayerStats where
  (<>) s t =
    AggregatePlayerStats
      { aps_kills =
          aps_kills s + aps_kills t
      , aps_killStreak_best =
          max (aps_killStreak_best s) (aps_killStreak_best t)
      , aps_deaths =
          aps_deaths s + aps_deaths t
      , aps_assists =
          aps_assists s + aps_assists t
      , aps_damageDealt =
          aps_damageDealt s + aps_damageDealt t
      , aps_damageTaken =
          aps_damageTaken s + aps_damageTaken t
      , aps_flagsCaptured =
          aps_flagsCaptured s + aps_flagsCaptured t
      , aps_flagsRecovered =
          aps_flagsRecovered s + aps_flagsRecovered t
      }

instance Monoid AggregatePlayerStats where
  mempty =
    AggregatePlayerStats
      { aps_kills = 0
      , aps_killStreak_best = 0
      , aps_deaths = 0
      , aps_assists = 0
      , aps_damageDealt = 0
      , aps_damageTaken = 0
      , aps_flagsCaptured = 0
      , aps_flagsRecovered = 0
      }

plusAggregateStats :: PlayerStats -> AggregatePlayerStats -> PlayerStats
plusAggregateStats stats aggStats =
  stats & field @"ps_aggregated" <>~ aggStats

--------------------------------------------------------------------------------
-- Update

incrementKills :: PlayerStats -> PlayerStats
incrementKills = execState $ do
  field @"ps_aggregated" . field @"aps_kills" += 1
  currentStreak <- field @"ps_killStreak_current" <%= (+ 1)
  field @"ps_aggregated" . field @"aps_killStreak_best" %= max currentStreak

incrementDeaths :: PlayerStats -> PlayerStats
incrementDeaths playerStats =
  playerStats
    & (field @"ps_aggregated" . field @"aps_deaths" +~ 1)
    & (field @"ps_killStreak_current" .~ 0)

incrementAssists :: PlayerStats -> PlayerStats
incrementAssists =
  field @"ps_aggregated" . field @"aps_assists" +~ 1

addDamageDealt :: Fixed -> PlayerStats -> PlayerStats
addDamageDealt dmg =
  field @"ps_aggregated" . field @"aps_damageDealt" +~ fromDamage dmg

addDamageTaken :: Fixed -> PlayerStats -> PlayerStats
addDamageTaken dmg =
  field @"ps_aggregated" . field @"aps_damageTaken" +~ fromDamage dmg

incrementFlagsCaptured :: PlayerStats -> PlayerStats
incrementFlagsCaptured =
  field @"ps_aggregated" . field @"aps_flagsCaptured" +~ 1

incrementFlagsRecovered :: PlayerStats -> PlayerStats
incrementFlagsRecovered =
  field @"ps_aggregated" . field @"aps_flagsRecovered" +~ 1

--------------------------------------------------------------------------------
-- Access

getAggregateStats :: PlayerStats -> AggregatePlayerStats
getAggregateStats = ps_aggregated

getKills :: AggregatePlayerStats -> Int
getKills = fromIntegral . aps_kills

getDeaths :: AggregatePlayerStats -> Int
getDeaths = fromIntegral . aps_deaths

getAssists :: AggregatePlayerStats -> Int
getAssists = fromIntegral . aps_assists

getStreakCurrent :: PlayerStats -> Int
getStreakCurrent = fromIntegral . ps_killStreak_current

getStreakBest :: AggregatePlayerStats -> Int
getStreakBest = fromIntegral . aps_killStreak_best

getDamageDealt :: AggregatePlayerStats -> Int
getDamageDealt = toDamage . aps_damageDealt

getDamageTaken :: AggregatePlayerStats -> Int
getDamageTaken = toDamage . aps_damageTaken

getFlagsCaptured :: AggregatePlayerStats -> Int
getFlagsCaptured = fromIntegral . aps_flagsCaptured

getFlagsRecovered :: AggregatePlayerStats -> Int
getFlagsRecovered = fromIntegral . aps_flagsRecovered

getDamageTakenTimestamps :: PlayerStats -> WIM.IntMap ActorID Time
getDamageTakenTimestamps = ps_damageTakenTimestamps

--------------------------------------------------------------------------------
-- Helpers

fromDamage :: Fixed -> Word64
fromDamage dmg = round (damageResolutionFloat * Fixed.toFloat dmg)

toDamage :: Word64 -> Int
toDamage w = fromIntegral $ w `div` damageResolutionWord

--------------------------------------------------------------------------------
-- Constants

damageResolutionWord :: Word64
damageResolutionWord = 256

damageResolutionFloat :: Float
damageResolutionFloat = fromIntegral damageResolutionWord
