{-# LANGUAGE PatternSynonyms #-}

-- | Player teams and team power-scaling
module Pixelpusher.Game.Team (
  Team (TeamW, TeamE),
  teamNameCapitalized,
  teamNameAllCaps,
  teamAdjective,
  smallerTeam,
  oppositeTeam,
  playerTeam,
  botTeam,
) where

import Data.Bifunctor (bimap)
import Data.Foldable (toList)
import Data.List (partition)
import Data.Serialize (Serialize)
import Data.Word (Word8)
import Foreign.Storable

--------------------------------------------------------------------------------

newtype Team = Team Word8
  deriving newtype (Eq, Serialize, Storable)

{-# COMPLETE TeamW, TeamE :: Team #-}
pattern TeamW :: Team
pattern TeamW = Team 0
pattern TeamE :: Team
pattern TeamE = Team 1

teamNameCapitalized :: Team -> String
teamNameCapitalized = \case
  TeamW -> "Formalists"
  TeamE -> "Purists"

teamNameAllCaps :: Team -> String
teamNameAllCaps = \case
  TeamW -> "FORMALISTS"
  TeamE -> "PURISTS"

teamAdjective :: Team -> String
teamAdjective = \case
  TeamW -> "Formalist"
  TeamE -> "Purist"

oppositeTeam :: Team -> Team
oppositeTeam TeamW = TeamE
oppositeTeam TeamE = TeamW

-- | Returns (n_west, n_east)
countMembers :: [Team] -> (Int, Int)
countMembers = bimap length length . partition (== TeamW)

smallerTeam :: (Foldable f) => f Team -> Maybe Team
smallerTeam teams
  | n_west > n_east = Just TeamE
  | n_east > n_west = Just TeamW
  | otherwise = Nothing
  where
    (n_west, n_east) = countMembers $ toList teams

-- | For players-versus-bots mode
playerTeam :: Team
playerTeam = TeamW

-- | For players-versus-bots mode
botTeam :: Team
botTeam = TeamE
