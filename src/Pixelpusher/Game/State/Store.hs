{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RecordWildCards #-}

-- | This module encapsulates the game logic that acts on the part of the
-- game state (the "store") that is stored in mutable arrays ("stores") and
-- indexed by integers ('EntityID's).
--
-- Note that an 'EntityID' obtained from one step of the game state should not
-- be used in a later frame, as the 'EntityID' may be invalidated by then. This
-- is why we take care not to expose `EntityID`s in the interface of this
-- module.
module Pixelpusher.Game.State.Store (
  -- * State
  GameStateStore,
  initialGameStateStore,
  resetGameStateStore,
  EntityStepEvents (..),
  stepEntities,

  -- ** State snapshot
  GameStateStoreSnapshot,

  -- * Query
  getRenderingComponents,
  viewPlayers,
  getFlags,
  getMapPowerups,

  -- * Operations

  -- ** Players
  addPlayer,
  removePlayer,

  -- ** Game entities
  addSoftObstacle,
  spawnTeamBase,
  spawnBaseFlag,
  spawnSpawnArea,

  -- * Debug
  debugDamageDrones,
  debugDamageOverseer,
  debugSwitchTeams,
) where

import Control.Arrow ((&&&))
import Control.Monad (join, void, when)
import Control.Monad.ST (ST)
import Data.Bifunctor (bimap, first)
import Data.Either (partitionEithers)
import Data.Foldable (for_, toList, traverse_)
import Data.IntMap.Merge.Strict qualified as IntMap
import Data.IntMap.Strict qualified as IntMap
import Data.List (foldl')
import Data.Maybe (catMaybes)
import Data.Serialize (Serialize)
import Data.Strict.Tuple (Pair ((:!:)))
import Data.Strict.Tuple qualified as Pair
import Data.Traversable (for)
import Lens.Micro.Platform.Custom
import System.Random.MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable (Copyable (..), Mutable (..))
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIntSet
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View (BotView, makeBotViews)
import Pixelpusher.Game.CastEvents (CastEvent (..), DashCastEvent (..), PsiStormCastEvent (..))
import Pixelpusher.Game.CollisionEvents (
  CollisionEvents (..),
  FlagCollisionEvent,
  NeutralCollision (..),
  ObstacleOverlap (..),
  PowerupGet (..),
 )
import Pixelpusher.Game.CombatComponent (CombatComponent)
import Pixelpusher.Game.CombatEvents (
  CollisionDamage (..),
  CombatDeathEvent (..),
  CombatEvents (..),
  OverseerPsiStormDamage (..),
  PsiStormEffect (..),
 )
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.DynamicsComponent (DynamicsComponent (..))
import Pixelpusher.Game.FlagComponent (FlagComponent (..))
import Pixelpusher.Game.FlagEvents (FlagCapture (..), FlagEvents (..), FlagRecovery (..), FlagType (..))
import Pixelpusher.Game.GameEvents (
  DroneDeathEvent (..),
  FlagDropEvent (..),
  OverseerDeathEvent (..),
  OverseerHitEvent (..),
  ShieldPickupEvent (..),
 )
import Pixelpusher.Game.GamePhase (GamePhase (..))
import Pixelpusher.Game.GamePhase qualified as GamePhase
import Pixelpusher.Game.MapPowerups (MapPowerupStatuses)
import Pixelpusher.Game.Parameters (
  CombatParams (..),
  DynamicsParams (..),
  FlagParams (..),
  GameParams (..),
 )
import Pixelpusher.Game.Player (Player (..), player_joinTime)
import Pixelpusher.Game.PlayerClass (PlayerClass)
import Pixelpusher.Game.PlayerControls (
  FlagCommand (FlagDrop),
  PlayerControls,
  control_buttons,
  control_flag,
 )
import Pixelpusher.Game.PlayerStatus (
  OverseerStatus (OverseerAlive, OverseerDead),
  OverseerView (OV_Alive, OV_Dead),
  OverseerViewAlive (..),
  OverseerViewDead (..),
  PlayerStatus (..),
  PlayerView (..),
 )
import Pixelpusher.Game.RenderingComponent (RenderingComponents)
import Pixelpusher.Game.Team (Team, oppositeTeam)
import Pixelpusher.Game.TeamLocations (teamBaseLocation)
import Pixelpusher.Game.TeamScore (TeamScore, incrementTeamScore)
import Pixelpusher.Game.Time (Ticks (..), Time, diffTime)
import Pixelpusher.Game.Time qualified as Time
import Pixelpusher.Store.EntityID (
  EntityID,
  EntityIDPair,
  EntityType (Drone, Overseer),
  EntityTypeSet (AnyEntity, CombatEntity),
  FlagEntity,
  SEntityID,
  ViewEntityID (BaseFlagID, DroneID, FlagID, OverseerID, TeamBaseID),
  getEntityIDPair,
  getIndex,
  refineEntityID,
  subEntityID,
 )

-- The next layer of the game state:
import Pixelpusher.Game.State.Core (
  GameStateCore,
  GameStateCoreSnapshot,
  ReadOnly,
  readOnly,
 )
import Pixelpusher.Game.State.Core qualified as Core

import Pixelpusher.Game.State.Store.ActorEntity (
  ActorEntityStore,
  getActorEntities,
 )
import Pixelpusher.Game.State.Store.Casting (runPlayerCasts)
import Pixelpusher.Game.State.Store.Combat (CombatReadStore)
import Pixelpusher.Game.State.Store.Combat qualified as Combat
import Pixelpusher.Game.State.Store.Dynamics (DynamicsReadStore)
import Pixelpusher.Game.State.Store.Dynamics qualified as Dynamics
import Pixelpusher.Game.State.Store.Flag (PartitionedFlags (..))
import Pixelpusher.Game.State.Store.Flag qualified as Flag
import Pixelpusher.Game.State.Store.Minion (MinionStore)
import Pixelpusher.Game.State.Store.Minion qualified as Minion
import Pixelpusher.Game.State.Store.PlayerClass (PlayerClassStore)
import Pixelpusher.Game.State.Store.Powerup qualified as Powerup
import Pixelpusher.Game.State.Store.Rendering qualified as Rendering
import Pixelpusher.Game.State.Store.Team (TeamStore)
import Pixelpusher.Game.State.Store.Team qualified as Team
import Pixelpusher.Game.State.Store.Transitory qualified as Transitory

--------------------------------------------------------------------------------
-- Store type

-- | An abstract wrapper around 'GameStateCore', for encapsulation
newtype GameStateStore s = GameStateStore (GameStateCore s)
  deriving newtype (Copyable s)

initialGameStateStore :: Time -> ST s (GameStateStore s)
initialGameStateStore time = GameStateStore <$> Core.initialGameStateCore time

resetGameStateStore :: Time -> GameStateStore s -> ST s ()
resetGameStateStore time (GameStateStore core) =
  Core.resetGameStateCore time core

--------------------------------------------------------------------------------
-- Serialization

-- | An abstract wrapper around 'GameStateCoreSnapshot', for encapsulation
newtype GameStateStoreSnapshot = GameStateStoreSnapshot GameStateCoreSnapshot
  deriving newtype (Serialize)

instance Mutable s GameStateStoreSnapshot (GameStateStore s) where
  freeze (GameStateStore core) =
    GameStateStoreSnapshot <$> freeze core
  thaw (GameStateStoreSnapshot coreSnapshot) =
    GameStateStore <$> thaw coreSnapshot

--------------------------------------------------------------------------------
-- Queries

viewPlayers :: GameStateStore s -> ST s (WIM.IntMap ActorID PlayerView)
viewPlayers (GameStateStore core) =
  Core.getPlayerStatuses core >>= traverse (viewPlayer core)

viewPlayer :: GameStateCore s -> PlayerStatus -> ST s PlayerView
viewPlayer core PlayerStatus{..} = do
  let Core.Stores{store_dynamics, store_combat, store_flag} =
        Core.dataStores core
  overseerView <-
    case ps_overseerStatus of
      OverseerDead deathTime respawnTime deathPos ->
        pure $
          OV_Dead
            OverseerViewDead
              { ovd_deathTime = deathTime
              , ovd_respawnTime = respawnTime
              , ovd_lastPos = deathPos
              }
      OverseerAlive overseerID _ -> do
        (pos, vel) <- Dynamics.readPosVel store_dynamics overseerID
        hasFlag <- flag_hasFlag <$> Core.sureLookup store_flag overseerID
        healthFrac <-
          min 1 . max 0 <$> Combat.getHealthFraction store_combat overseerID
        pure $
          OV_Alive
            OverseerViewAlive
              { ova_pos = pos
              , ova_vel = vel
              , ova_hasFlag = hasFlag
              , ova_healthFraction = healthFrac
              }
  pure
    PlayerView
      { pv_team = ps_team
      , pv_playerClass = ps_playerClass
      , pv_overseerView = overseerView
      , pv_controlState = ps_controlState
      }

getMapPowerups :: GameStateStore s -> ST s MapPowerupStatuses
getMapPowerups (GameStateStore core) = Core.getMapPowerups core

getFlags :: GameStateStore s -> ST s [(Team, Fixed2, FlagType)]
getFlags (GameStateStore core) = do
  Flag.getFlags store_flag store_team' >>= traverse \(flagsID, team) -> do
    pos <- dyn_pos <$> Core.sureLookup store_dynamics flagsID
    pure (team, pos, flagType flagsID)
  where
    Core.Stores{store_flag, store_dynamics, store_team} = Core.dataStores core
    store_team' = readOnly store_team

    flagType :: SEntityID FlagEntity -> FlagType
    flagType flagsID =
      case refineEntityID flagsID of
        BaseFlagID _ -> BaseFlagType
        FlagID _ -> DroppedFlagType
        OverseerID _ -> OverseerFlagType
        TeamBaseID _ -> error "getFlags: team bases should not carry flags"

--------------------------------------------------------------------------------
-- Operations

addPlayer ::
  GameParams ->
  GameStateStore s ->
  Time ->
  Team ->
  ActorID ->
  PlayerClass ->
  ST s ()
addPlayer params (GameStateStore core) = Core.addPlayer params core

removePlayer :: GameParams -> GameStateStore s -> Time -> ActorID -> ST s ()
removePlayer params (GameStateStore core) = Core.removePlayer params core

addSoftObstacle ::
  GameParams ->
  GameStateStore s ->
  Maybe Fixed ->
  Fixed2 ->
  Maybe Fixed ->
  ST s ()
addSoftObstacle params (GameStateStore core) = Core.addSoftObstacle params core

spawnTeamBase :: GameParams -> GameStateStore s -> Team -> ST s ()
spawnTeamBase params (GameStateStore core) team =
  let pos = teamBaseLocation (gp_dynamics params) team
  in  void $ Core.addTeamBase params core team pos

spawnBaseFlag :: GameParams -> GameStateStore s -> Team -> ST s ()
spawnBaseFlag params (GameStateStore core) = spawnBaseFlag' params core

spawnBaseFlag' :: GameParams -> GameStateCore s -> Team -> ST s ()
spawnBaseFlag' params core team =
  let pos = teamBaseLocation (gp_dynamics params) team
  in  void $ Core.addBaseFlag params core team pos

spawnSpawnArea :: GameParams -> GameStateStore s -> Team -> ST s ()
spawnSpawnArea params (GameStateStore core) = Core.addSpawnArea params core

debugDamageDrones :: GameStateStore s -> Time -> ActorID -> ST s ()
debugDamageDrones (GameStateStore core) time actorID = do
  playerStatusMaybe <- Core.getPlayerStatus core actorID
  for_ playerStatusMaybe $ \playerStatus ->
    case ps_overseerStatus playerStatus of
      OverseerDead{} -> pure ()
      OverseerAlive overseerID _spawnTime -> do
        let Core.Stores{..} = Core.dataStores core
        droneIDs <- Minion.getMinions store_masterMinions overseerID
        for_ (WIntSet.toList droneIDs) $ \droneID ->
          Combat.debugApplyDamageFraction
            store_combat
            time
            droneID
            0.20 -- arbitrary

debugDamageOverseer :: GameStateStore s -> Time -> ActorID -> ST s ()
debugDamageOverseer (GameStateStore core) time actorID = do
  playerStatusMaybe <- Core.getPlayerStatus core actorID
  for_ playerStatusMaybe $ \playerStatus ->
    case ps_overseerStatus playerStatus of
      OverseerDead{} -> pure ()
      OverseerAlive overseerID _spawnTime -> do
        let Core.Stores{store_combat} = Core.dataStores core
        Combat.debugApplyDamageFraction
          store_combat
          time
          overseerID
          0.20 -- arbitrary

-- | Returns whether or not the switch was a success
debugSwitchTeams :: GameStateStore s -> ActorID -> ST s Bool
debugSwitchTeams (GameStateStore core) actorID = do
  let Core.Stores{..} = Core.dataStores core
  -- Only proceed if the player's overseer is not carrying a flag.
  -- Note: This also requires that the player is alive, though that currently
  -- makes no difference because the if the player is dead then they should not
  -- have any entities that would need updating.
  maybeOverseerID <-
    Core.getPlayerStatus core actorID <&> \maybePlayerStatus -> do
      playerStatus <- maybePlayerStatus
      case ps_overseerStatus playerStatus of
        OverseerDead{} -> Nothing
        OverseerAlive overseerID _ -> Just overseerID
  case maybeOverseerID of
    Nothing -> pure False
    Just overseerID -> do
      overseerHasFlag <- fst <$> Flag.readOverseerHasFlag store_flag overseerID
      if not overseerHasFlag
        then do
          -- Read
          actorEntityIDs <- getActorEntities store_actorEntity actorID

          -- Update
          Core.debugSwitchPlayerTeam core actorID
          for_ actorEntityIDs $ \actorEntityID ->
            case refineEntityID actorEntityID of
              OverseerID overseerID' ->
                Team.debugChangeTeam store_team (subEntityID overseerID')
              DroneID droneID ->
                Team.debugChangeTeam store_team (subEntityID droneID)
              _ ->
                pure ()
          pure True
        else pure False

--------------------------------------------------------------------------------
-- Game state integration

data EntityStepEvents = EntityStepEvents
  { ese_overseerDeaths :: [OverseerDeathEvent]
  , ese_droneDeaths :: [DroneDeathEvent]
  , ese_overseerHits :: [OverseerHitEvent]
  , ese_collisions :: [CollisionDamage]
  , ese_neutralCollisions :: [NeutralCollision]
  , ese_dashCasts :: [DashCastEvent]
  , ese_psiStormCasts :: [PsiStormCastEvent]
  , ese_psiStormEffects :: [PsiStormEffect]
  , ese_flagEvents :: FlagEvents
  , ese_flagDropEvents :: [FlagDropEvent]
  , ese_scoreIncrement :: TeamScore
  , ese_botViews :: WIM.IntMap BotID BotView
  , ese_obstacleOverlaps :: [ObstacleOverlap]
  , ese_shieldPickups :: [ShieldPickupEvent]
  }

-- | Advance the simulation by one step. This function runs the core game logic.
stepEntities ::
  GameParams ->
  GameStateStore s ->
  Gen s ->
  Time ->
  WIM.IntMap ActorID Player ->
  GamePhase ->
  ST s EntityStepEvents
stepEntities params (GameStateStore core) gen time players gamePhase =
  Core.finallyRemoveEntities params time core $ \markForRemoval -> do
    Core.updatePlayerClasses params core time $
      flip WIM.map players $ \p ->
        ( player_prevControls p
        , player_controls p
        , view player_joinTime p
        )

    Core.respawnOverseers params gen core time gamePhase
    spawnDrones params core gen time

    castEvents <-
      runPlayerCasts
        (gp_cast params)
        core
        time
        (WIM.map (player_prevControls &&& player_controls) players)

    playerStatuses <- Core.getPlayerStatuses core
    let playersAndStatuses =
          WIM.merge
            (IntMap.mapMissing (\_k _x -> error errMsg))
            (IntMap.mapMissing (\_k _x -> error errMsg))
            (IntMap.zipWithMatched (\_k a b -> (a, b)))
            players
            playerStatuses
          where
            errMsg = "Error: stepEntities: Inconsistency in player data"
        players' = WIM.map (first player_controls) playersAndStatuses

    -- TODO: merge with `FlagEvents`
    flagDropEvents <- runDropFlags params core time players'

    when (gp_enableShieldPowerupSpawners params) $
      case gamePhase of
        GamePhase_Game{} ->
          Core.respawnBoundPowerups params time core
        GamePhase_Intermission{} ->
          pure ()

    let Core.Stores{..} = Core.dataStores core
        readStore_team = readOnly store_team
        readStore_playerEntity = readOnly store_actorEntity
        readStore_combat = Combat.readCombatStore store_combat
        readStore_dynamics = Dynamics.readDynamicsStore store_dynamics
        readStore_powerup = Powerup.readPowerupStore store_powerup
        readStore_playerClass = readOnly store_playerClass

    collisionEvents <-
      Dynamics.stepDynamics
        params
        time
        players'
        readStore_team
        readStore_playerEntity
        readStore_powerup
        store_masterMinions
        readStore_combat
        store_psiStorm
        store_dynamics

    -- Consume powerups
    shieldPickupEvents <-
      fmap catMaybes $
        for (ce_powerupGet collisionEvents) $ \(PowerupGet overseerID powerupID) -> do
          shieldHealthMaybe <- Powerup.consumePowerup store_powerup powerupID
          case shieldHealthMaybe of
            Nothing -> pure Nothing
            Just shieldHealth -> do
              Combat.addShields store_combat overseerID shieldHealth
              markForRemoval (subEntityID powerupID)
              actorID <- Core.sureLookup readStore_playerEntity overseerID
              (pos, radius) <-
                Dynamics.readPosRadius readStore_dynamics powerupID
              pure $! Just $! ShieldPickupEvent actorID pos radius

    let shouldSmiteOverseers =
          case gamePhase of
            GamePhase_Game{} -> Combat.DoNotSmiteOverseers
            GamePhase_Intermission intermissionStart _ _ _ ->
              if time `diffTime` intermissionStart > Ticks (C.tickRate_hz * 6)
                then Combat.SmiteOverseers
                else Combat.DoNotSmiteOverseers

    combatEvents <-
      Combat.runCombat
        gen
        store_combat
        readStore_playerEntity
        (gp_combat params)
        time
        shouldSmiteOverseers
        (ce_collision collisionEvents)
        (ce_psiStorm collisionEvents)
        (ce_spawn collisionEvents)

    (droneDeathEvents, overseerDeathEvents) <-
      -- deduplicate drone and overseer death events by EntityID
      bimap
        (IntMap.elems . IntMap.fromList . map (\dde -> (dde_eid dde, dde)))
        (IntMap.elems . IntMap.fromList . map (\val@(overseerID, _) -> (getIndex overseerID, val)))
        . partitionEithers
        . concat
        <$> mapM
          ( lookupDeaths
              readStore_dynamics
              readStore_combat
              store_masterMinions
              readStore_playerEntity
              readStore_team
              readStore_playerClass
              time
          )
          (ce_deaths combatEvents)

    overseerCollisionHits <-
      concat
        <$> mapM
          (lookupCollisionDamage readStore_playerEntity)
          (ce_collisions combatEvents)
    overseerPsiStormHits <-
      catMaybes
        <$> mapM
          (lookupPsiStormDamage readStore_playerEntity)
          (ce_psiStormDamage combatEvents)
    let overseerHitEvents = overseerCollisionHits ++ overseerPsiStormHits

    flagEvents <-
      runFlags' params core time markForRemoval (ce_flag collisionEvents)

    -- Remove entities killed in combat
    for_ (ce_deaths combatEvents) $ markForRemoval . subEntityID . killed_eid

    -- Remove expired transitory entities
    Transitory.getExpiredEntities store_transitory time
      >>= traverse_ (markForRemoval . subEntityID)

    -- Create overseer death explosions
    for_ overseerDeathEvents $ \(overseerID, _) -> do
      pos <-
        dyn_pos <$> Core.sureLookup (readOnly store_dynamics) overseerID
      Core.addExplosion (gp_dynamics params) core time pos

    -- Create shield powerups from overseer deaths
    when (gp_enableOverseerDeathShieldPowerupDrops params && GamePhase.inGame gamePhase) $
      for_ overseerDeathEvents $ \(_, overseerDeathEvent) ->
        let pos = ode_pos overseerDeathEvent
            health = gp_overseerDeathShieldPowerupHealth params
            lifetime = gp_overseerDeathPowerupLifetimeTicks params
        in  Core.addFreePowerup params time core pos health lifetime

    -- Bot logic
    botViews <- do
      flags <- getFlags (GameStateStore core)
      mapPowerups <- Core.getMapPowerups core
      let playerControlsStatuses =
            WIM.intersectionWith
              (,)
              (WIM.map player_controls players)
              playerStatuses
          teamScore =
            case gamePhase of
              GamePhase_Game _ score -> score
              GamePhase_Intermission{} -> mempty
      makeBotViews
        (Dynamics.readDynamicsStore store_dynamics)
        (Combat.readCombatStore store_combat)
        store_masterMinions
        (readOnly store_team)
        (readOnly store_actorEntity)
        (Flag.readFlagStore store_flag)
        (Powerup.readPowerupStore store_powerup)
        store_psiStorm
        params
        time
        flags
        mapPowerups
        playerControlsStatuses
        teamScore
        (ce_botView collisionEvents)

    -- Partition cast events for output
    let (droneDashes, psiStormCasts) =
          partitionEithers $
            map
              ( \case
                  DashCastEvent_ dash -> Left dash
                  PsiStormCastEvent_ psiStorm -> Right psiStorm
              )
              castEvents

    pure
      EntityStepEvents
        { ese_overseerDeaths = map snd overseerDeathEvents
        , ese_droneDeaths = droneDeathEvents
        , ese_overseerHits = overseerHitEvents
        , ese_collisions = map Pair.snd $ ce_collisions combatEvents
        , ese_neutralCollisions = ce_neutralCollision collisionEvents
        , ese_dashCasts = droneDashes
        , ese_psiStormCasts = psiStormCasts
        , ese_psiStormEffects = ce_psiStormEffects combatEvents
        , ese_flagEvents = flagEvents
        , ese_flagDropEvents = flagDropEvents
        , ese_scoreIncrement =
            foldl' (flip incrementTeamScore) mempty $
              map (\(FlagCapture{flagCapture_team}) -> flagCapture_team) $
                flagEvents_captures flagEvents
        , ese_botViews = botViews
        , ese_obstacleOverlaps = ce_obstacleOverlap collisionEvents
        , ese_shieldPickups = shieldPickupEvents
        }

--------------------------------------------------------------------------------

spawnDrones :: GameParams -> GameStateCore s -> Gen s -> Time -> ST s ()
spawnDrones params core gen time = do
  let checkInterval = gp_droneRespawnCheckInterval $ gp_combat params
  when (time `Time.atMultipleOf` checkInterval) $ do
    let Core.Stores{store_masterMinions} = Core.dataStores core
    recruitingMasterIDs <-
      Minion.recruitingMasters store_masterMinions (gp_combat params) time
    let invulnTicks = Ticks (-1)
        mDeathTime = Nothing
    for_ recruitingMasterIDs $ \overseerID ->
      Core.addDrone
        params
        time
        invulnTicks
        core
        gen
        overseerID
        mDeathTime
        Core.SingleDroneSpawn

--------------------------------------------------------------------------------

runDropFlags ::
  (Traversable f) =>
  GameParams ->
  GameStateCore s ->
  Time ->
  f (PlayerControls, PlayerStatus) ->
  ST s [FlagDropEvent]
runDropFlags params core time players
  | gp_enableFlagDrops (gp_flags params) =
      fmap catMaybes $
        for (toList players) $ \(controls, playerStatus) ->
          case ps_overseerStatus playerStatus of
            OverseerDead{} ->
              pure Nothing
            OverseerAlive overseerID _ ->
              if controls ^. to control_buttons . control_flag == FlagDrop
                then do
                  let Core.Stores
                        { store_actorEntity
                        , store_dynamics
                        , store_flag
                        , store_team
                        } =
                          Core.dataStores core
                  Flag.tryDropOverseerFlag (gp_flags params) store_flag time overseerID
                    >>= \case
                      Flag.HadNoFlag -> pure Nothing
                      Flag.FlagDropped -> do
                        actorID <- Core.sureLookup store_actorEntity overseerID
                        flagTeam <- oppositeTeam <$> Core.sureLookup store_team overseerID
                        (pos, vel) <-
                          Dynamics.readPosVel store_dynamics overseerID
                        Core.addFlag params core time flagTeam pos
                        pure $
                          Just
                            FlagDropEvent
                              { fde_actor = actorID
                              , fde_pos = pos
                              , fde_vel = vel
                              }
                else pure Nothing
  | otherwise =
      pure []

--------------------------------------------------------------------------------

-- | Note: A drone death event may be created by either the drone itself or its
-- overseer, so we must (elsewhere) account for the possibility of duplicates.
lookupDeaths ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MinionStore s ->
  ReadOnly (ActorEntityStore s) ->
  ReadOnly (TeamStore s) ->
  ReadOnly (PlayerClassStore s) ->
  Time ->
  CombatDeathEvent ->
  ST s [Either DroneDeathEvent (EntityID Overseer, OverseerDeathEvent)]
lookupDeaths
  dynamicsStore
  combatStore
  masterMinionStore
  playerStore
  teamStore
  playerClassStore
  time
  combatEvent =
    case refineEntityID (killed_eid combatEvent) of
      OverseerID overseerID -> do
        team <- Core.sureLookup teamStore overseerID
        playerClass <- Core.sureLookup playerClassStore overseerID
        overseerDeath <- do
          pid <- Core.sureLookup playerStore overseerID
          (pos, vel, radius) <-
            Dynamics.readPosVelRadius dynamicsStore overseerID
          pure
            OverseerDeathEvent
              { ode_time = time
              , ode_pos = pos
              , ode_vel = vel
              , ode_radius = radius
              , ode_owner = pid
              , ode_owner_team = team
              , ode_owner_class = playerClass
              , ode_killer = killer_mPid combatEvent
              , ode_entityID = getIndex overseerID
              }
        droneDeaths <- do
          droneMinionIDs <-
            WIntSet.toList <$> Minion.getMinions masterMinionStore overseerID
          for droneMinionIDs $ \droneID -> do
            posVelRadius <- Dynamics.readPosVelRadius dynamicsStore droneID
            combatComp <- Core.sureLookup combatStore droneID
            pure $
              makeDroneDeath time team droneID posVelRadius combatComp playerClass
        pure $ (:) (Right (overseerID, overseerDeath)) (map Left droneDeaths)
      DroneID droneID -> do
        team <- Core.sureLookup teamStore droneID
        playerClass <- Core.sureLookup playerClassStore droneID
        posVelRadius <- Dynamics.readPosVelRadius dynamicsStore droneID
        combatComp <- Core.sureLookup combatStore droneID
        pure $
          singleton $
            Left $
              makeDroneDeath time team droneID posVelRadius combatComp playerClass
    where
      singleton x = [x]

makeDroneDeath ::
  Time ->
  Team ->
  EntityID Drone ->
  (Fixed2, Fixed2, Fixed) ->
  CombatComponent ->
  PlayerClass ->
  DroneDeathEvent
makeDroneDeath time team eid (pos, vel, radius) combatComp playerClass =
  DroneDeathEvent
    { dde_time = time
    , dde_eid = getIndex eid
    , dde_radius = radius
    , dde_pos = pos
    , dde_vel = vel
    , dde_team = team
    , dde_class = playerClass
    , dde_recentDamage = Combat.recentDamage time combatComp
    }

lookupPsiStormDamage ::
  ReadOnly (ActorEntityStore s) ->
  OverseerPsiStormDamage ->
  ST s (Maybe OverseerHitEvent)
lookupPsiStormDamage playerStore psiStormDamage =
  Core.sureLookup playerStore (opsd_overseerID psiStormDamage) <&> \pid ->
    Just $
      OverseerHitEvent
        { ohe_attacker = opsd_mPsiStormActorID psiStormDamage
        , ohe_defender = pid
        , ohe_damage = opsd_damage psiStormDamage
        }

lookupCollisionDamage ::
  ReadOnly (ActorEntityStore s) ->
  Pair (EntityIDPair CombatEntity) CollisionDamage ->
  ST s [OverseerHitEvent]
lookupCollisionDamage actorComps collision = do
  let combatIDPair :!: collisionDamage = collision
      (combatID1, combatID2) = getEntityIDPair combatIDPair
      dmg = cd_damage collisionDamage
  case refineEntityID combatID1 of
    OverseerID overseerID1 -> do
      case refineEntityID combatID2 of
        OverseerID overseerID2 -> do
          pid1 <- Core.sureLookup actorComps overseerID1
          pid2 <- Core.sureLookup actorComps overseerID2
          pure
            [ OverseerHitEvent
                { ohe_attacker = Just pid1
                , ohe_defender = pid2
                , ohe_damage = dmg
                }
            , OverseerHitEvent
                { ohe_attacker = Just pid2
                , ohe_defender = pid1
                , ohe_damage = dmg
                }
            ]
        DroneID droneID2 -> do
          (: []) <$> makeDroneHit actorComps dmg droneID2 overseerID1
    DroneID droneID1 -> do
      case refineEntityID combatID2 of
        OverseerID overseerID2 ->
          (: []) <$> makeDroneHit actorComps dmg droneID1 overseerID2
        DroneID _droneID2 ->
          pure []

makeDroneHit ::
  ReadOnly (ActorEntityStore s) ->
  Fixed ->
  EntityID Drone ->
  EntityID Overseer ->
  ST s OverseerHitEvent
makeDroneHit actorComps dmg droneID overseerID = do
  attackerPID <- Core.lookup actorComps droneID
  defenderPID <- Core.sureLookup actorComps overseerID
  pure
    OverseerHitEvent
      { ohe_attacker = attackerPID
      , ohe_defender = defenderPID
      , ohe_damage = dmg
      }

--------------------------------------------------------------------------------

runFlags' ::
  GameParams ->
  GameStateCore s ->
  Time ->
  (SEntityID AnyEntity -> ST s ()) ->
  [FlagCollisionEvent] ->
  ST s FlagEvents
runFlags' params core time markForRemoval collisions = do
  let Core.Stores{store_flag, store_dynamics, store_team, store_actorEntity} =
        Core.dataStores core
      store_dynamics' = Dynamics.readDynamicsStore store_dynamics
      store_team' = readOnly store_team
      store_playerEntity' = readOnly store_actorEntity

  (flagEvents, partitionedFlags) <-
    Flag.runFlags
      store_flag
      store_dynamics'
      store_team'
      store_playerEntity'
      (gp_flags params)
      time
      collisions

  let PartitionedFlags{pf_flagCarriers, pf_emptySourceFlags} = partitionedFlags

  -- Slow flag carriers
  for_ pf_flagCarriers $ \overseerID -> do
    Dynamics.modifyVelocityWithMass store_dynamics overseerID $ \mass vel ->
      let speed2 = Fixed.unsafeNormSq vel
          factor =
            max 0 . min 1 $
              1.0 - speed2 * gp_flagDrag (gp_dynamics params) / mass
      in  Fixed.map (* factor) vel

  -- Remove empty source flags
  traverse_ (markForRemoval . subEntityID) pf_emptySourceFlags

  -- Spawn base flags from captures and reclamations
  for_ (flagEvents_captures flagEvents) $ \(FlagCapture{flagCapture_team}) ->
    spawnBaseFlag' params core (oppositeTeam flagCapture_team)
  for_ (flagEvents_recoveries flagEvents) $ \(FlagRecovery team _ _) ->
    spawnBaseFlag' params core team

  pure flagEvents

--------------------------------------------------------------------------------
-- Export entity data for rendering

getRenderingComponents ::
  GameParams ->
  GameStateStore s ->
  Time ->
  Maybe ActorID ->
  WIM.IntMap ActorID Player ->
  ST s RenderingComponents
getRenderingComponents params (GameStateStore core) time mActorID playerMap = do
  mViewPos <- join <$> traverse (getPlayerPosition core) mActorID
  playerStatusMap <- Core.getPlayerStatuses core
  let playerPlayerStatusMap =
        WIM.merge
          (IntMap.mapMissing (\_k _x -> error errMsg))
          (IntMap.mapMissing (\_k _x -> error errMsg))
          (IntMap.zipWithMatched (\_k a b -> (a, b)))
          playerMap
          playerStatusMap
  Rendering.getRenderingComponents
    params
    (Core.dataStores core)
    time
    mViewPos
    playerPlayerStatusMap
  where
    errMsg = "Error: getRenderingComponents: Inconsistency in player data"

getPlayerPosition :: GameStateCore s -> ActorID -> ST s (Maybe Fixed2)
getPlayerPosition core actorID = do
  let Core.Stores{store_dynamics} = Core.dataStores core
  Core.getPlayerStatus core actorID >>= \case
    Nothing -> pure Nothing
    Just playerStatus ->
      fmap Just $
        case ps_overseerStatus playerStatus of
          OverseerDead _ _ deathPos ->
            pure deathPos
          OverseerAlive overseerID _ ->
            Dynamics.readPos store_dynamics overseerID
