{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}

-- | This module encapsulates the entirety of the "proper" game state and logic
-- (that is, the state and logic of the game itself, which exludes that related
-- to debugging).
module Pixelpusher.Game.State.Outer (
  -- * State
  GameStateOuter,
  initializeGameStateOuter,
  stepGameStateOuter,

  -- ** State snapshot
  GameStateOuterSnapshot,

  -- * Query
  GameSceneOuter (..),
  getGameSceneOuter,
  getPlayers,

  -- * Debug
  debugDamageDrones,
  debugDamageOverseer,
  debugSwitchTeams,
) where

import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.ST
import Control.Monad.State.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict (StateT (StateT), runState)
import Data.Bifunctor (bimap, second)
import Data.Either (partitionEithers)
import Data.Foldable (foldl', for_, toList, traverse_)
import Data.Generics.Product.Fields
import Data.Generics.Sum.Constructors
import Data.IntMap.Merge.Strict qualified as IntMap
import Data.Maybe
import Data.STRef
import Data.Serialize (Serialize)
import Data.Vector qualified as V
import Data.Vector.Unboxed qualified as VU
import Data.Word (Word32)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC
import System.Random.MWC.Distributions qualified as MWC

import Pixelpusher.Custom.DelayQueue qualified as DelayQueue
import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable (Copyable (..), Mutable (..))
import Pixelpusher.Custom.Util qualified as Util
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID (BotActor, PlayerActor))
import Pixelpusher.Game.ActorID qualified as ActorID
import Pixelpusher.Game.BotID (BotID, BotIDPool)
import Pixelpusher.Game.BotID qualified as BotID
import Pixelpusher.Game.Bots.AI qualified as Bots.AI
import Pixelpusher.Game.Bots.View (BotView)
import Pixelpusher.Game.CastEvents (DashCastEvent (..), PsiStormCastEvent (..))
import Pixelpusher.Game.CollisionEvents (ObstacleOverlap)
import Pixelpusher.Game.CombatEvents (
  CollisionDamage (..),
  CollisionDamageParticipants (..),
  PsiStormEffect (..),
 )
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents (
  FlagCapture (..),
  FlagEvents (..),
  FlagPickup (..),
  FlagRecovery (..),
  FlagType (BaseFlagType),
 )
import Pixelpusher.Game.GameCommand (GameCommand (..))
import Pixelpusher.Game.GameEvents (
  DroneDeathEvent (..),
  FlagDropEvent (..),
  GameEvents (..),
  GamePhaseTransition (
    GameRestart,
    IntermissionEnding,
    MatchEnd,
    PlayMatchEndSfx
  ),
  KillEvent (..),
  OverseerDeathEvent (..),
  ShieldPickupEvent (..),
 )
import Pixelpusher.Game.GamePhase (GamePhase (..))
import Pixelpusher.Game.GamePhase qualified as GamePhase
import Pixelpusher.Game.MapPowerups (MapPowerupStatuses)
import Pixelpusher.Game.Parameters (
  BaseGameParams,
  BotParams (..),
  CombatParams (..),
  DynamicsParams (..),
  GameParams (..),
  makeGameParams,
  rebaseGameParams,
  resizeGameParams,
 )
import Pixelpusher.Game.Particles
import Pixelpusher.Game.Player (
  Player (..),
  PlayerAttributes (..),
  player_actorID,
  player_color,
  player_initialClass,
  player_name,
  player_team,
 )
import Pixelpusher.Game.PlayerClass (PlayerClass)
import Pixelpusher.Game.PlayerClass qualified as PlayerClass
import Pixelpusher.Game.PlayerColors (PlayerColor (..), leastCommonColorVariant)
import Pixelpusher.Game.PlayerControls (
  DroneCommand (DroneAttraction, DroneNeutral),
  MoveCommand (MoveNeutral),
  PlayerControls (..),
  control_drone,
  control_move,
  defaultButtonStates,
  defaultControls,
 )
import Pixelpusher.Game.PlayerID (PlayerID)
import Pixelpusher.Game.PlayerName (PlayerName, makeBotName)
import Pixelpusher.Game.PlayerStats (
  AggregatePlayerStats,
  getAggregateStats,
  newPlayerStats,
 )
import Pixelpusher.Game.PlayerStatus (
  OverseerView (OV_Alive, OV_Dead),
  OverseerViewAlive (..),
  OverseerViewDead (..),
  PlayerView (..),
 )
import Pixelpusher.Game.RenderingComponent (RenderingComponents)
import Pixelpusher.Game.Stats (recordStats)
import Pixelpusher.Game.Team (
  Team (TeamE, TeamW),
  oppositeTeam,
 )
import Pixelpusher.Game.Team qualified as Team
import Pixelpusher.Game.TeamScore (TeamScore (..), zeroTeamScore)
import Pixelpusher.Game.Time (
  Ticks (..),
  Time,
  addTicks,
  diffTime,
  getTime,
  initialTime,
  nextTick,
 )
import Pixelpusher.Game.Time qualified as Time
import Pixelpusher.Game.Tutorial (
  TutorialEffect (..),
  TutorialPhase (..),
  TutorialState (..),
 )
import Pixelpusher.Game.Tutorial qualified as Tutorial

-- The next layer of the game state:
import Pixelpusher.Game.State.Store (GameStateStore)
import Pixelpusher.Game.State.Store qualified as Store

import Pixelpusher.Game.State.Outer.ParticleStore (ParticleStore)
import Pixelpusher.Game.State.Outer.ParticleStore qualified as ParticleStore

--------------------------------------------------------------------------------
-- Types

-- | The entirety of the game state
data GameStateOuter s = GameStateOuter
  { gso_pure :: STRef s GameStatePure
  , gso_inner :: GameStateStore s
  , gso_prng :: MWC.Gen s
  }
  deriving stock (Generic)
  deriving anyclass (Copyable s)

data GameStatePure = GameStatePure
  { gsp_params :: GameParams
  , gsp_time :: Time
  , gsp_roundStart :: Time
  , gsp_phase :: GamePhase
  , gsp_players :: WIM.IntMap ActorID Player
  , gsp_particles :: ParticleStore
  , gsp_botIDPool :: BotIDPool
  , gsp_tutorialState :: TutorialState
  , gsp_obstacleOverlap :: [ObstacleOverlap]
  -- ^ Like a particle in that it has no effect on the evolution of the game
  -- state, but it lasts just one frame and must be continually refreshed.
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- Extra `GameStatePure` lenses

gsp_status_players ::
  Lens' GameStatePure (GamePhase, WIM.IntMap ActorID Player)
gsp_status_players =
  unsafeLensProduct (field @"gsp_phase") (field @"gsp_players")

unsafeLensProduct :: Lens' s a -> Lens' s b -> Lens' s (a, b)
unsafeLensProduct l r = lens getter setter
  where
    getter = view l &&& view r
    setter s (a, b) = s & set l a . set r b

--------------------------------------------------------------------------------
-- Initialization

initializeGameStateOuter :: BaseGameParams -> MWC.Gen s -> ST s (GameStateOuter s)
initializeGameStateOuter baseParams gen = do
  let params = makeGameParams 0 baseParams
  let initialState = initialGameStatePure params
  entityStore <- Store.initialGameStateStore (gsp_time initialState)
  ref <- newSTRef initialState
  unless (gp_enableTutorialMode params) $
    spawnInitialEntities params gen entityStore
  pure $!
    GameStateOuter
      { gso_pure = ref
      , gso_inner = entityStore
      , gso_prng = gen
      }

initialGameStatePure :: GameParams -> GameStatePure
initialGameStatePure params =
  GameStatePure
    { gsp_params = params
    , gsp_time = t0
    , gsp_roundStart = initialTime
    , gsp_phase = GamePhase_Game t0 zeroTeamScore
    , gsp_players = WIM.empty
    , gsp_particles = ParticleStore.new
    , gsp_botIDPool = BotID.newBotIDPool
    , gsp_tutorialState = Tutorial.initTutorialState initialTime
    , gsp_obstacleOverlap = []
    }
  where
    t0 = initialTime

spawnInitialEntities :: GameParams -> MWC.Gen s -> GameStateStore s -> ST s ()
spawnInitialEntities params gen store = do
  spawnDriftingSoftObstacles params gen store
  spawnTeamBasesAndFlags params store

spawnDriftingSoftObstacles ::
  GameParams -> MWC.Gen s -> GameStateStore s -> ST s ()
spawnDriftingSoftObstacles params gen store = do
  let (nPairs, nCenterObstacles) = gp_numSoftObstacles params `divMod` 2
      buffer = 1.5 * gp_softObstacleRadius (gp_dynamics params)
      spawnRadius = max 1 $ gp_worldRadius (gp_dynamics params) - buffer
  replicateM_ nCenterObstacles $ do
    let pos = 0
        velAngle = Nothing
    void $ Store.addSoftObstacle params store velAngle pos Nothing
  replicateM_ nPairs $ do
    pos <- Fixed.map (* spawnRadius) <$> Util.uniformUnitDisc gen
    velAngle <- MWC.uniformR (0, 2 * pi) gen
    void $ Store.addSoftObstacle params store (Just velAngle) pos Nothing
    void $ Store.addSoftObstacle params store (Just (velAngle + pi)) (-pos) Nothing

spawnTeamBasesAndFlags :: GameParams -> GameStateStore s -> ST s ()
spawnTeamBasesAndFlags params store = do
  Store.spawnTeamBase params store TeamW
  Store.spawnTeamBase params store TeamE
  Store.spawnBaseFlag params store TeamW
  Store.spawnBaseFlag params store TeamE
  Store.spawnSpawnArea params store TeamW
  Store.spawnSpawnArea params store TeamE

--------------------------------------------------------------------------------
-- Query

getPlayers :: GameStateOuter s -> ST s (WIM.IntMap ActorID Player)
getPlayers = fmap gsp_players . readSTRef . gso_pure

-- | A projection of 'GameStateOuter' for presenting to players
data GameSceneOuter = GameSceneOuter
  { scene_gameParams :: GameParams
  , scene_time :: Time
  , scene_roundStart :: Time
  , scene_renderingComps :: RenderingComponents
  , scene_gamePhase :: GamePhase
  , scene_particles :: Particles
  , scene_players :: WIM.IntMap ActorID (Player, PlayerView)
  , scene_flags :: [(Team, Fixed2, FlagType)]
  , scene_tutorialPhase :: TutorialPhase
  , scene_obstacleOverlaps :: [ObstacleOverlap]
  , scene_mapPowerups :: MapPowerupStatuses
  }

getGameSceneOuter :: GameStateOuter s -> Maybe ActorID -> ST s GameSceneOuter
getGameSceneOuter gameState mActorID = do
  gameStatePure <- readSTRef $ gso_pure gameState
  mapPowerups <- Store.getMapPowerups $ gso_inner gameState
  flags <- Store.getFlags $ gso_inner gameState
  renderingComps <-
    Store.getRenderingComponents
      (gsp_params gameStatePure)
      (gso_inner gameState)
      (gsp_time gameStatePure)
      mActorID
      (gsp_players gameStatePure)
  playerViews <- Store.viewPlayers $ gso_inner gameState
  pure $
    GameSceneOuter
      { scene_gameParams = gsp_params gameStatePure
      , scene_time = gsp_time gameStatePure
      , scene_roundStart = gsp_roundStart gameStatePure
      , scene_renderingComps = renderingComps
      , scene_gamePhase = gsp_phase gameStatePure
      , scene_particles =
          ParticleStore.getParticles $ gsp_particles gameStatePure
      , scene_players =
          let errMsg = "getGameScene: Players and PlayerViews do not align"
          in  WIM.merge
                (IntMap.mapMissing (const (const (error errMsg))))
                (IntMap.mapMissing (const (const (error errMsg))))
                (IntMap.zipWithMatched (const (,)))
                (gsp_players gameStatePure)
                playerViews
      , scene_flags = flags
      , scene_tutorialPhase = ts_phase $ gsp_tutorialState gameStatePure
      , scene_obstacleOverlaps = gsp_obstacleOverlap gameStatePure
      , scene_mapPowerups = mapPowerups
      }

--------------------------------------------------------------------------------
-- Serialization

data GameStateOuterSnapshot = GameStateOuterSnapshot
  { gsos_pure :: GameStatePure
  , gsos_store :: Store.GameStateStoreSnapshot
  , gsos_prng :: VU.Vector Word32
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s GameStateOuterSnapshot (GameStateOuter s) where
  freeze GameStateOuter{..} =
    GameStateOuterSnapshot
      <$> freeze gso_pure
      <*> freeze gso_inner
      <*> freeze gso_prng
  thaw GameStateOuterSnapshot{..} =
    GameStateOuter
      <$> thaw gsos_pure
      <*> thaw gsos_store
      <*> thaw gsos_prng

--------------------------------------------------------------------------------
-- Debug

debugDamageDrones :: GameStateOuter s -> ActorID -> ST s ()
debugDamageDrones GameStateOuter{..} actorID = do
  time <- gsp_time <$> readSTRef gso_pure
  Store.debugDamageDrones gso_inner time actorID

debugDamageOverseer :: GameStateOuter s -> ActorID -> ST s ()
debugDamageOverseer GameStateOuter{..} actorID = do
  time <- gsp_time <$> readSTRef gso_pure
  Store.debugDamageOverseer gso_inner time actorID

debugSwitchTeams :: GameStateOuter s -> ActorID -> ST s ()
debugSwitchTeams GameStateOuter{..} actorID = do
  switchSuccessful <- Store.debugSwitchTeams gso_inner actorID
  when switchSuccessful $
    modifySTRef' gso_pure $
      field @"gsp_players" . ix actorID . player_team %~ oppositeTeam

--------------------------------------------------------------------------------
-- State integration

stepGameStateOuter ::
  GameStateOuter s ->
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  ST s GameEvents
stepGameStateOuter GameStateOuter{..} newPlayerControls gameCmds =
  Util.withSTRef' gso_pure $
    stepGameStateOuterStateT gso_prng gso_inner newPlayerControls gameCmds

stepGameStateOuterStateT ::
  MWC.Gen s ->
  Store.GameStateStore s ->
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  StateT GameStatePure (ST s) GameEvents
stepGameStateOuterStateT gen store newPlayerControls gameCmds = do
  -- Increment time
  time <- field @"gsp_time" <%= nextTick

  -- Run game commands
  paramsChangedEvents <-
    catMaybes <$> mapM (runGameCommand gen store time) gameCmds

  -- Get the game parameters after running game commands, since there's a
  -- command to change the game parameters (for playtests)
  params <- gets gsp_params

  -- Add and remove bots
  when (time `Time.atMultipleOf` Ticks 60 && not (gp_enableTutorialMode params)) $
    maintainBotNumbers params gen store time

  -- Update player controls
  zoom (field @"gsp_players") $
    modify' $
      let updateControls _key newControls player =
            player
              & set (field @"player_controls") newControls
              . set (field @"player_prevControls") (player_controls player)
      in  WIM.merge
            IntMap.dropMissing
            IntMap.preserveMissing
            (IntMap.zipWithMatched updateControls)
            (WIM.mapKeys PlayerActor newPlayerControls)

  -- Step inner game state
  Store.EntityStepEvents{..} <- do
    gamePhase <- gets gsp_phase
    players <- gets gsp_players
    lift $
      Store.stepEntities
        params
        store
        gen
        time
        players
        gamePhase

  -- Update bot controls
  zoom (field @"gsp_players") $ do
    playerViews <- lift $ Store.viewPlayers store
    modify' $ flushBotControlQueuesOnDeath ese_overseerDeaths
    StateT $
      fmap ((),)
        . updateBotControls params gen time ese_botViews playerViews

  -- Store obstacle overlaps
  field @"gsp_obstacleOverlap" .= ese_obstacleOverlaps

  -- Update score
  field @"gsp_phase" . _Ctor @"GamePhase_Game" . _2 %= (ese_scoreIncrement <>)

  -- Record stats
  killEvents <- do
    players <- gets gsp_players
    let (killEvents', newPlayers) =
          flip runState players $
            recordStats
              time
              ese_overseerDeaths
              ese_overseerHits
              ese_flagEvents
    gets gsp_phase >>= \case
      GamePhase_Intermission{} ->
        -- Discard player state updates during intermission
        pure ()
      GamePhase_Game{} ->
        field @"gsp_players" .= newPlayers
    pure $! killEvents'

  -- Create new particles
  do
    gamePhase <- gets gsp_phase
    players <- gets gsp_players
    zoom (field @"gsp_particles") $
      updateParticles
        params
        gamePhase
        time
        players
        killEvents
        ese_droneDeaths
        ese_collisions
        ese_dashCasts
        ese_psiStormCasts
        ese_psiStormEffects
        ese_flagEvents
        ese_flagDropEvents
        ese_shieldPickups
        paramsChangedEvents

  -- Game status transitions
  -- Note: Since game status transitions may change the game parameters, we
  -- perform at the very end of the step to avoid inconsistency related to changing
  -- game parameters part way through a step.
  mGamePhaseTransition <-
    if gp_enableTutorialMode params
      then do
        -- Tutorial phase transitions
        players' <- gets gsp_players
        tutorialEffects <-
          zoom (field @"gsp_tutorialState") $
            state $
              Tutorial.updateTutorialPhase
                time
                players'
                ese_overseerDeaths
                ese_droneDeaths
                ese_psiStormEffects
                ese_psiStormCasts
                (flagEvents_pickups ese_flagEvents)
                (flagEvents_captures ese_flagEvents)
        traverse_ (runTutorialEffect params gen store) tutorialEffects
        pure Nothing
      else do
        -- Regular game phase transitions
        mGamePhaseTransition <-
          zoom gsp_status_players $ transitionGamePhase params time
        for_ mGamePhaseTransition $ \case
          MatchEnd{} -> pure ()
          PlayMatchEndSfx -> pure ()
          IntermissionEnding -> pure ()
          GameRestart -> newRound gen store
        pure mGamePhaseTransition

  pure
    GameEvents
      { ge_gamePhaseTransitions = maybeToList mGamePhaseTransition
      , ge_collisions = ese_collisions
      , ge_neutralCollisions = ese_neutralCollisions
      , ge_overseerDeaths = ese_overseerDeaths
      , ge_dashes = ese_dashCasts
      , ge_psiStormCasts = ese_psiStormCasts
      , ge_psiStormEffects = ese_psiStormEffects
      , ge_flagCaptures = flagEvents_captures ese_flagEvents
      , ge_flagPickups = flagEvents_pickups ese_flagEvents
      , ge_flagRecoveries = flagEvents_recoveries ese_flagEvents
      , ge_shieldPickups = ese_shieldPickups
      , ge_flagDrops = ese_flagDropEvents
      }

--------------------------------------------------------------------------------
-- Game status transitions

transitionGamePhase ::
  (Monad m) =>
  GameParams ->
  Time ->
  StateT (GamePhase, WIM.IntMap ActorID Player) m (Maybe GamePhaseTransition)
transitionGamePhase params time =
  use _1 >>= \case
    GamePhase_Game roundStartTime teamScore -> do
      let scoreNW = teamScore_w teamScore
          scoreSE = teamScore_e teamScore
          requiredScore = gp_flagsToWin params
      let mWinningTeam
            | scoreNW >= requiredScore = Just TeamW
            | scoreSE >= requiredScore = Just TeamE
            | otherwise = Nothing
      case mWinningTeam of
        Nothing -> pure Nothing
        Just winningTeam ->
          let minIntermissionTicks =
                fromIntegral (gp_minIntermissionTicks params) :: Fixed
              maxIntermissionTicks =
                fromIntegral (gp_maxIntermissionTicks params)
              shortMatchSeconds = 90 -- 1:30
              longMatchSeconds = 60 * 6 -- 6:00
              matchDurationSeconds =
                fromIntegral (time `diffTime` roundStartTime)
                  `div` C.tickRate_hz
              lengthFactor =
                max 0
                  . min 1
                  $ (fromIntegral matchDurationSeconds - shortMatchSeconds)
                    / (longMatchSeconds - shortMatchSeconds)
              intermissionTicks =
                round $
                  minIntermissionTicks
                    + lengthFactor
                      * (maxIntermissionTicks - minIntermissionTicks)
              nextRoundTime = addTicks (Ticks intermissionTicks) time
          in  do
                _1
                  .= GamePhase_Intermission
                    time
                    nextRoundTime
                    winningTeam
                    teamScore
                pure $ Just $ MatchEnd winningTeam
    GamePhase_Intermission roundEndTime nextRoundTime _ _ -> do
      let -- Emit an end-of-intermission event some time before the actual end
          -- of intermission to allow the game manager to act before the start
          -- of the next round
          intermissionTicks = nextRoundTime `diffTime` roundEndTime
          endOfIntermission =
            addTicks (4 * intermissionTicks `div` 5) roundEndTime
          matchEndSfxTime = addTicks C.matchEndSfxDelayTicks roundEndTime
      if
        | time == nextRoundTime -> do
            _2 . traverse %= \player ->
              player
                & field @"player_aggregateStats"
                <>~ getAggregateStats (player_stats player)
            pure $ Just GameRestart
        | time == endOfIntermission ->
            pure $ Just IntermissionEnding
        | time == matchEndSfxTime ->
            pure $ Just PlayMatchEndSfx
        | otherwise ->
            pure Nothing

--------------------------------------------------------------------------------
-- Game commands

data GameCommandEvent = ParamsChanged

-- | Run a game command.
--
-- There are probably situations where switching game params causes some
-- inconsistency. This is okay for now, since we only plan to switch game
-- parameters during playtests for experimentation purposes.
runGameCommand ::
  MWC.Gen s ->
  GameStateStore s ->
  Time ->
  GameCommand ->
  StateT GameStatePure (ST s) (Maybe GameCommandEvent)
runGameCommand gen store time cmd = case cmd of
  PlayerJoin pid name playerClass mTeam -> do
    params <- gets gsp_params
    let actorID = PlayerActor pid
        emptyStats = mempty
    zoom (field @"gsp_players") $ do
      let mTeam' =
            case mTeam of
              Just team -> Just team
              Nothing ->
                if gp_enablePlayersVersusBotsMode params
                  then Just Team.playerTeam
                  else Nothing
      addPlayer params store gen time mTeam' actorID name playerClass emptyStats
    pure Nothing
  PlayerLeave pid -> do
    params <- gets gsp_params
    let actorID = PlayerActor pid
    removePlayer params store time actorID
    pure Nothing
  SwitchGameParams newRawParams -> do
    field @"gsp_params" %= rebaseGameParams newRawParams
    pure $ Just ParamsChanged

--------------------------------------------------------------------------------
-- Add/remove players

addPlayer ::
  GameParams ->
  GameStateStore s ->
  MWC.Gen s ->
  Time ->
  Maybe Team ->
  ActorID ->
  PlayerName ->
  PlayerClass ->
  AggregatePlayerStats ->
  StateT (WIM.IntMap ActorID Player) (ST s) ()
addPlayer params store gen time mTeam pid name playerClass aggStats = do
  -- Assign new players to the smaller team
  team <- case mTeam of
    Just team -> pure team
    Nothing ->
      get >>= \actors ->
        let players =
              map snd $
                filter (\(actorID, _) -> ActorID.isActorPlayer actorID) $
                  WIM.toList actors
        in  case Team.smallerTeam (map (view player_team) players) of
              Just team -> pure team
              Nothing -> do
                bool <- MWC.uniform gen
                if bool then pure TeamW else pure TeamE

  -- Choose player color to be the least common color on the team
  color <-
    gets $
      leastCommonColorVariant
        . map (view player_color)
        . filter ((== team) . view player_team)
        . WIM.elems

  let attrs = PlayerAttributes pid name team color time playerClass
      player =
        Player
          attrs
          defaultControls
          defaultControls
          Nothing
          DelayQueue.emptyDelayQueue
          defaultControls
          defaultControls
          newPlayerStats
          aggStats
  -- Add player record
  at pid .= Just player
  -- Add store data
  lift $ Store.addPlayer params store time team pid playerClass

removePlayer ::
  GameParams ->
  GameStateStore s ->
  Time ->
  ActorID ->
  StateT GameStatePure (ST s) ()
removePlayer params store time pid = do
  -- Remove player record
  field @"gsp_players" . at pid .= Nothing
  -- Return BotID
  case pid of
    PlayerActor _ -> pure ()
    BotActor botID ->
      field @"gsp_botIDPool" %= BotID.returnBotID botID
  -- Remove store data
  lift $ Store.removePlayer params store time pid

--------------------------------------------------------------------------------
-- Particles

updateParticles ::
  GameParams ->
  GamePhase ->
  Time ->
  WIM.IntMap ActorID Player ->
  [KillEvent] ->
  [DroneDeathEvent] ->
  [CollisionDamage] ->
  [DashCastEvent] ->
  [PsiStormCastEvent] ->
  [PsiStormEffect] ->
  FlagEvents ->
  [FlagDropEvent] ->
  [ShieldPickupEvent] ->
  [GameCommandEvent] ->
  StateT ParticleStore (ST s) ()
updateParticles
  params
  gamePhase
  time
  players
  kills
  droneDeaths
  collisions
  dashCasts
  psiStormCasts
  groupedPsiStormEffectEvents
  flagEvents
  flagDropEvents
  shieldPickupEvents
  paramsChangedEvents = do
    let dynamicsParams = gp_dynamics params

    -- Remove old particles
    modify' $ ParticleStore.trim (addTicks (Ticks (-1)) time)

    -- Create overseer death particles
    forM_ kills $ \KillEvent{ke_death = ovDeathEvent} ->
      case WIM.lookup (ode_owner ovDeathEvent) players of
        Nothing ->
          error "Cannot create overseer death particle: Non-existent ActorID"
        Just player -> do
          let teamColor =
                PlayerColor
                  (player ^. player_team)
                  (player ^. player_color)
                  (player ^. player_actorID)
          modify' . ParticleStore.insert time $
            OverseerDeathParticle
              teamColor
              (ode_pos ovDeathEvent)
              (ode_vel ovDeathEvent)
              (ode_radius ovDeathEvent)
              (ode_entityID ovDeathEvent)
              (ode_owner ovDeathEvent)
              (ode_owner_team ovDeathEvent)
              (ode_owner_class ovDeathEvent)
          modify' . ParticleStore.insert time $
            OverseerRemainsParticle
              teamColor
              (ode_pos ovDeathEvent)
              (ode_radius ovDeathEvent)

    -- Create drone death particles
    modify' . ParticleStore.insertMany time $
      flip map droneDeaths $ \drDeathEvent ->
        DroneDeathParticle
          (dde_eid drDeathEvent)
          (dde_pos drDeathEvent)
          (dde_vel drDeathEvent)
          (dde_radius drDeathEvent)
          (dde_team drDeathEvent)
          (dde_class drDeathEvent)
          (dde_recentDamage drDeathEvent)

    -- Create collision damage particles
    modify' . ParticleStore.insertMany time $
      flip map collisions $ \collision ->
        CollisionDamageParticle
          (cd_position collision)
          (cd_velocity collision)
          (cd_damage collision)
          (cd_direction collision)
          (lookupColor $ cd_mActorID1 collision)
          (lookupColor $ cd_mActorID2 collision)

    -- Create kill/death notification particles
    when (GamePhase.inGame gamePhase) $
      modify' . ParticleStore.insertMany time $
        flip map kills $
          \KillEvent{ke_death = ovDeathEvent, ke_assisters = assisters} ->
            case WIM.lookup (ode_owner ovDeathEvent) players of
              Nothing ->
                error "Cannot create kill notification particle: Non-existent ActorID"
              Just killedPlayer ->
                let killedName = killedPlayer ^. player_name
                    killedTeam = killedPlayer ^. player_team
                    mKillerName = do
                      killerPid <- ode_killer ovDeathEvent
                      killerPlayer <- WIM.lookup killerPid players
                      guard $ view player_team killerPlayer /= killedTeam
                      pure
                        ( view player_actorID killerPlayer
                        , view player_name killerPlayer
                        )
                in  KillNotificationParticle
                      killedName
                      killedTeam
                      mKillerName
                      assisters

    -- Create psi-storm cast particles
    modify' . ParticleStore.insertMany time $
      flip map psiStormCasts $ \psiStormCastEvent ->
        CastRippleParticle (psce_pos psiStormCastEvent)

    -- Create dash cast particles
    modify' . ParticleStore.insertMany time $
      flip map dashCasts $ \dashEvent ->
        DashParticle
          { dp_pos = dce_pos dashEvent
          , dp_vel = dce_vel dashEvent
          , dp_radius = dce_entityRadius dashEvent
          , dp_direction = dce_direction dashEvent
          , dp_magnitudeFraction = dce_effectsMagnitudeFrac dashEvent
          , dp_dashType = dce_dashType dashEvent
          , dp_team = dce_team dashEvent
          }

    -- Create psi-storm damage particles
    modify' . ParticleStore.insertMany time $
      flip map groupedPsiStormEffectEvents $ \PsiStormEffect{..} ->
        PsiStormDamageParticle
          pse_entityPos
          pse_entityVel
          (lookupColor pse_mEntityActorID)
          pse_damage

    -- Note: The game phase restriction was intended to only apply to
    -- notifications, but since the overseers won't have much time to live
    -- after the game ends, it doesn't really matter that we also "silence"
    -- the other flag-related particles.
    when (GamePhase.inGame gamePhase) $ do
      -- Create flag pickup particles
      forM_ (flagEvents_pickups flagEvents) $ \pickup -> do
        let team = flagPickup_carrierTeam pickup
            flagPos = flagPickup_flagPosition pickup
            name =
              view player_name $
                Util.expectJust "updateParticles: flagPickup: no actorID" $
                  WIM.lookup (flagPickup_actor pickup) players
            particle =
              FlagPickupParticle
                { fpp_carrierTeam = team
                , fpp_flagPos = flagPos
                , fpp_carrierName = name
                }
        when (flagPickup_type pickup == BaseFlagType) $ do
          modify' $ ParticleStore.insert time $ LogMsg_FlagPickup particle
        modify' $ ParticleStore.insert time $ FlagFade_Pickup particle

      -- Create flag score particles
      forM_ (flagEvents_captures flagEvents) $
        \(FlagCapture scoringTeam actorID pos vel) -> do
          modify' . ParticleStore.insert time $
            let name =
                  view player_name $
                    Util.expectJust "updateParticles: flagPickup: no actorID" $
                      WIM.lookup actorID players
            in  LogMsg_FlagCapture $
                  FlagCaptureParticle scoringTeam name

          modify' . ParticleStore.insert time $
            let flagTeam = oppositeTeam scoringTeam
                radius = gp_flagRadius dynamicsParams
            in  FlagExplodeParticle flagTeam pos vel radius

          modify' . ParticleStore.insert time $
            let flagTeam = oppositeTeam scoringTeam
            in  FlagFade_Respawn flagTeam

      -- Create flag recovery particles
      forM_ (flagEvents_recoveries flagEvents) $
        \(FlagRecovery team mActorID pos) -> do
          let mName =
                mActorID <&> \actorID ->
                  view player_name $
                    Util.expectJust "updateParticles: flagPickup: no actorID" $
                      WIM.lookup actorID players
              particle = FlagRecoverParticle team mName pos
          modify' . ParticleStore.insert time $ FlagFade_Recover particle
          modify' . ParticleStore.insert time $ FlagFade_Respawn team
          modify' . ParticleStore.insert time $ LogMsg_FlagRecover particle

    -- Create flag drop particles
    modify' . ParticleStore.insertMany time $
      flip map flagDropEvents $ \(FlagDropEvent _ pos vel) ->
        FlagFade_Drop (FlagDropParticle pos vel)

    -- Create params changed notification particles
    modify' . ParticleStore.insertMany time $
      flip map paramsChangedEvents $ \ParamsChanged ->
        LogMsg_AnyMsg "Game parameters updated"

    -- Create screenshake particles
    modify' . ParticleStore.insertMany time $
      flip concatMap collisions $ \CollisionDamage{..} ->
        case cd_participants of
          CollisionDamageOverseers actorID1 actorID2 ->
            [ ScreenshakeParticle
                { sp_damage = cd_damage
                , sp_direction = cd_direction
                , sp_actorID = actorID1
                }
            , ScreenshakeParticle
                { sp_damage = cd_damage
                , sp_direction = cd_direction
                , sp_actorID = actorID2
                }
            ]
          CollisionDamageDrones ->
            []
          CollisionDamageOverseerDrone actorID ->
            pure $
              ScreenshakeParticle
                { sp_damage = cd_damage
                , sp_direction = cd_direction
                , sp_actorID = actorID
                }

    -- Create shield pickup particles
    modify' . ParticleStore.insertMany time $
      flip map shieldPickupEvents $ \ShieldPickupEvent{..} ->
        ShieldPickupParticle
          { spp_pos = spe_pos
          , spp_radius = spe_radius
          }
    where
      lookupColor :: Maybe ActorID -> Maybe PlayerColor
      lookupColor mPid = do
        pid <- mPid
        player <- WIM.lookup pid players
        pure $
          PlayerColor
            (player ^. player_team)
            (player ^. player_color)
            (player ^. player_actorID)

--------------------------------------------------------------------------------
-- Round reset

newRound ::
  MWC.Gen s ->
  GameStateStore s ->
  StateT GameStatePure (ST s) ()
newRound gen store = do
  -- Save IDs, names, and (aggregated) stats of players.
  -- Only player information is saved; bots are excluded.
  pidNameClassStatsTeam <- do
    players <-
      gets gsp_players <&> \actors ->
        V.fromList $
          flip mapMaybe (WIM.elems actors) $
            \actor ->
              case actor ^. player_actorID of
                BotActor _ -> Nothing
                PlayerActor playerID ->
                  Just
                    ( playerID
                    , actor ^. player_name
                    , actor ^. player_initialClass
                    , player_aggregateStats actor
                    , actor ^. player_team
                    )
    MWC.uniformShuffle players gen -- randomize order to randomize teams

  -- Determine new parameter set from number of players
  let nPlayers = length pidNameClassStatsTeam
  newParams <- field @"gsp_params" <%= resizeGameParams nPlayers

  -- Reset
  time <- gets gsp_time
  field @"gsp_roundStart" .= time
  field @"gsp_phase" .= GamePhase_Game time zeroTeamScore
  field @"gsp_players" .= WIM.empty
  field @"gsp_botIDPool" .= BotID.newBotIDPool
  field @"gsp_particles" .= ParticleStore.new

  -- Add initial entities, re-add players
  lift $ do
    Store.resetGameStateStore time store
    spawnInitialEntities newParams gen store

  zoom (field @"gsp_players") $
    for_ pidNameClassStatsTeam $
      \(pid, name, playerClass, aggStats, oldTeam) -> do
        let mTeam
              | gp_enablePlayersVersusBotsMode newParams = Just Team.playerTeam
              | gp_newRound_randomizeTeams newParams = Nothing
              | otherwise = Just oldTeam
        addPlayer
          newParams
          store
          gen
          time
          mTeam
          (PlayerActor pid)
          name
          playerClass
          aggStats

--------------------------------------------------------------------------------
-- Bot control

flushBotControlQueuesOnDeath ::
  [OverseerDeathEvent] ->
  WIM.IntMap ActorID Player ->
  WIM.IntMap ActorID Player
flushBotControlQueuesOnDeath overseerDeaths players =
  foldl'
    ( \playersAcc overseerDeath ->
        let actorID = ode_owner overseerDeath
            f player = player{player_botControlQueue = DelayQueue.emptyDelayQueue}
        in  WIM.adjust f actorID playersAcc
    )
    players
    overseerDeaths

updateBotControls ::
  GameParams ->
  MWC.Gen s ->
  Time ->
  WIM.IntMap BotID BotView ->
  WIM.IntMap ActorID PlayerView ->
  WIM.IntMap ActorID Player ->
  ST s (WIM.IntMap ActorID Player)
updateBotControls params gen time botViews playerViews players = do
  flip WIM.traverseWithKey players $ \actorID player ->
    case actorID of
      PlayerActor _ -> pure player
      BotActor botID ->
        let playerView =
              fromMaybe (error "updateBotControls: lookup player view") $
                WIM.lookup (BotActor botID) playerViews
        in  case pv_overseerView playerView of
              OV_Dead OverseerViewDead{ovd_deathTime} ->
                let respawnTicks = gp_overseerRespawnDelay (gp_combat params)
                    respawnMoveCommand =
                      if addTicks respawnTicks ovd_deathTime `diffTime` time
                        > PlayerClass.respawnClassSelectionDeadTimeFinal
                        && even (getTime time) -- class selection requires changing inputs
                        then
                          PlayerClass.playerClassToMoveCommand $
                            if gp_botAllowNonTemplarClasses (gp_bots params)
                              then PlayerClass.deathTimeToPlayerClass ovd_deathTime
                              else PlayerClass.Templar
                        else MoveNeutral
                in  pure
                      player
                        { player_controls =
                            player_controls player
                              & field @"control_buttons"
                              . control_move
                              .~ respawnMoveCommand
                        , player_prevControls =
                            player_controls player
                        }
              OV_Alive{} ->
                -- Bots should have a bot view exactly when `shouldControlBot` is true
                -- for the current frame and their overseer is alive
                case WIM.lookup botID botViews of
                  Nothing ->
                    pure $
                      player
                        { player_controls =
                            updateControlWorldPos
                              (player_controls player)
                              (player_prevBotTargetControls player)
                              (player_botTargetControls player)
                        , player_prevControls =
                            player_controls player
                        }
                  Just botView -> do
                    let botState =
                          fromMaybe Bots.AI.defaultBotState $
                            player_botState player
                    (!controls, !newBotState) <-
                      let delayedControls =
                            toList $ player_botControlQueue player
                       in Bots.AI.botControl params gen time botID botView botState delayedControls
                    let (delayedTargetControlMaybe, newControlQueue) =
                          DelayQueue.pushDelayQueue controlQueueSize controls $
                            player_botControlQueue player
                        newTargetControls =
                          fromMaybe
                            (player_controls player)
                              { control_buttons = defaultButtonStates
                              }
                            delayedTargetControlMaybe
                        prevTargetControls = player_botTargetControls player
                    pure $
                      player
                        { player_botState = Just newBotState
                        , player_botControlQueue = newControlQueue
                        , player_controls =
                            updateControlWorldPos
                              (player_controls player)
                              prevTargetControls
                              newTargetControls
                        , player_prevControls = player_controls player
                        , player_botTargetControls = newTargetControls
                        , player_prevBotTargetControls = prevTargetControls
                        }
  where
    controlQueueSize = gp_botControlQueueLength (gp_bots params)

-- Update old controls by (1) taking buttons of target control and (2)
-- interpolating mouse position towards that of target controls
updateControlWorldPos ::
  PlayerControls -> PlayerControls -> PlayerControls -> PlayerControls
updateControlWorldPos controlsOld prevControlsTarget controlsTarget =
  let newMousePos =
        Bots.AI.interpolateMouse
          (control_worldPos controlsOld)
          (control_worldPos prevControlsTarget)
          (control_worldPos controlsTarget)
  in  PlayerControls
        { control_buttons = control_buttons controlsTarget
        , control_worldPos = newMousePos
        }

--------------------------------------------------------------------------------
-- Spawning and despawning bots

spawnBot ::
  MWC.Gen s ->
  GameStateStore s ->
  Team ->
  Maybe PlayerClass ->
  StateT GameStatePure (ST s) ()
spawnBot gen store team playerClassMaybe = do
  params <- gets gsp_params
  time <- gets gsp_time
  botIDMaybe <- zoom (field @"gsp_botIDPool") $ state BotID.borrowBotID
  playerClass <-
    case playerClassMaybe of
      Just playerClass -> pure playerClass
      Nothing ->
        if gp_botAllowNonTemplarClasses (gp_bots params)
          then MWC.uniformM gen
          else pure PlayerClass.Templar

  for_ botIDMaybe $ \botID ->
    zoom (field @"gsp_players") $
      addPlayer
        params
        store
        gen
        time
        (Just team)
        (BotActor botID)
        (makeBotName (BotID.getBotID botID))
        playerClass
        mempty

maintainBotNumbers ::
  GameParams ->
  MWC.Gen s ->
  GameStateStore s ->
  Time ->
  StateT GameStatePure (ST s) ()
maintainBotNumbers params gen store time =
  if gp_enableBots (gp_bots params)
    then maintainBotNumbers' params gen store time
    else removeAllBots params store time

removeAllBots ::
  GameParams -> GameStateStore s -> Time -> StateT GameStatePure (ST s) ()
removeAllBots params store time = do
  players <- lift $ Store.viewPlayers store
  let botIDs =
        flip mapMaybe (WIM.keys players) $ \case
          PlayerActor _ -> Nothing
          BotActor botID -> Just botID
  for_ botIDs $ removePlayer params store time . BotActor

maintainBotNumbers' ::
  GameParams ->
  MWC.Gen s ->
  GameStateStore s ->
  Time ->
  StateT GameStatePure (ST s) ()
maintainBotNumbers' params gen store time = do
  UnimportantBots{..} <- lift $ findUnimportantBots <$> Store.viewPlayers store
  PlayerCounts{..} <- gets (countPlayers . gsp_players)
  let BotParams{gp_minBotsPerTeam, gp_minTeamSize} = gp_bots params
      westActorsTotal = pc_westHumans + pc_westBots
      eastActorsTotal = pc_eastHumans + pc_eastBots
      minWestBots = max gp_minBotsPerTeam $ gp_minTeamSize - pc_westHumans
      minEastBots = max gp_minBotsPerTeam $ gp_minTeamSize - pc_eastHumans
      minWestTeamSize = pc_westHumans + minWestBots
      minEastTeamSize = pc_eastHumans + minEastBots
      targetTeamSize = max minWestTeamSize minEastTeamSize
      targetWestBots = targetTeamSize - pc_westHumans
      targetEastBots = targetTeamSize - pc_eastHumans
      totalBots = pc_westBots + pc_eastBots
      maxBots = fromIntegral C.maxBotsPerGame
  -- Strategy:
  -- 1. Always maintain sufficient bot numbers
  -- 2. Quickly balance teams by spawning bots if necessary
  -- 3. Prune pairs of excess bots only when their removal would have low impact on the game
  if
    | (pc_westBots < targetWestBots || pc_eastBots < targetEastBots) && totalBots < maxBots -> do
        -- Maintain minimum bot numbers on each team by spawning new bots.
        -- The check on the total number of bots prevents this logic from
        -- becoming stuck attempting to spawn new bots while being unable to
        -- due to the bot limit.
        let numNwSpawns = targetWestBots - pc_westBots
            numSeSpawns = targetEastBots - pc_eastBots
            -- Interleave bot spawns so that one team's bots aren't
            -- guaranteed to have a big latency advantage over the other.
            -- If there are two bots, A and B, on opposing teams, and bot A's
            -- logic always runs the tick after that of bot B, then bot A has
            -- an advantage over bot B; this is because bot A can more
            -- quickly respond to the moves of bot B than bot B can respond
            -- to bot A's moves.
            --
            -- This situation can arise because (1) we stagger the computation
            -- of the bot logic for different bots across different ticks in
            -- order to reduce the maximum computation required in a tick,
            -- (2) the current way we choose which frame to run a bot's logic
            -- is by taking the bot's id modulo the control delay, in ticks,
            -- and (3) bots spawned at the start of the game are assigned bot
            -- ids in ascending order.
            spawns =
              if numNwSpawns < numSeSpawns
                then
                  interleave
                    (replicate numSeSpawns TeamE)
                    (replicate numNwSpawns TeamW)
                else
                  interleave
                    (replicate numNwSpawns TeamW)
                    (replicate numSeSpawns TeamE)
        for_ spawns $ \team ->
          spawnBot gen store team Nothing
    | westActorsTotal < eastActorsTotal -> do
        -- Try to remove SE team bots
        let difference = eastActorsTotal - westActorsTotal
            toRemove = min difference (pc_eastBots - targetEastBots)
            eastBots = take toRemove ub_eastUnimportant
        for_ eastBots $ \eastBotID ->
          removePlayer params store time (BotActor eastBotID)
        -- Spawn bots for NW team
        let remainingDifference = difference - length eastBots
        replicateM_ remainingDifference $ spawnBot gen store TeamW Nothing
    | westActorsTotal > eastActorsTotal -> do
        -- Try to remove NW team bots
        let difference = westActorsTotal - eastActorsTotal
            toRemove = min difference (pc_westBots - targetWestBots)
            westBots = take toRemove ub_westUnimportant
        for_ westBots $ \westBotID ->
          removePlayer params store time (BotActor westBotID)
        -- Spawn bots for SE team
        let remainingDifference = difference - length westBots
        replicateM_ remainingDifference $ spawnBot gen store TeamE Nothing
    | pc_westBots > targetWestBots && pc_eastBots > targetEastBots -> do
        -- Remove excess bots
        let numPairsToRemove =
              min (pc_westBots - targetWestBots) (pc_eastBots - targetEastBots)
            botPairsToRemove =
              take numPairsToRemove $ zip ub_westUnimportant ub_eastUnimportant
        for_ botPairsToRemove $ \(botID1, botID2) -> do
          removePlayer params store time (BotActor botID1)
          removePlayer params store time (BotActor botID2)
    | otherwise -> pure ()

-- | Interleave the elements of two lists, starting with the list on the left.
interleave :: [a] -> [a] -> [a]
interleave (x : xs) (y : ys) = x : y : interleave xs ys
interleave xs ys = xs ++ ys

data UnimportantBots = UnimportantBots
  { ub_westUnimportant :: [BotID]
  , ub_eastUnimportant :: [BotID]
  }

findUnimportantBots :: WIM.IntMap ActorID PlayerView -> UnimportantBots
findUnimportantBots playerViews =
  let -- Filter to bots
      bots = (mapMaybe . _1) ActorID.matchBotID (WIM.toList playerViews)

      -- Partition by team
      (westBots, eastBots) =
        let teamOverseerView playerView =
              case pv_team playerView of
                TeamW -> Left $ pv_overseerView playerView
                TeamE -> Right $ pv_overseerView playerView
        in  partitionEithers $ map (distribute . second teamOverseerView) bots

      -- Partition by liveness
      partitionByLiveness overseerViews =
        let flagLivenessView = \case
              OV_Alive OverseerViewAlive{ova_pos, ova_hasFlag} ->
                Left (ova_pos, ova_hasFlag)
              OV_Dead{} ->
                Right ()
        in  partitionEithers $
              map (distribute . second flagLivenessView) overseerViews
      (westLiveBots, map fst -> westDeadBots) = partitionByLiveness westBots
      (eastLiveBots, map fst -> eastDeadBots) = partitionByLiveness eastBots

      -- Filter by flags
      filterByFlag (pos, hasFlag) = if hasFlag then Nothing else Just pos
      westFlaglessBots = (mapMaybe . _2) filterByFlag westLiveBots
      eastFlaglessBots = (mapMaybe . _2) filterByFlag eastLiveBots

      -- Positions
      westPositions = map (fst . snd) westLiveBots
      eastPositions = map (fst . snd) eastLiveBots

      -- Proximities to enemies
      enemyProximity enemyPositions pos =
        foldl' min Fixed.maxBoundFixedSq $
          map (\pos' -> Fixed.normSq (pos' - pos)) enemyPositions
      westProximitiesSq =
        (map . second) (enemyProximity eastPositions) westFlaglessBots
      eastProximitiesSq =
        (map . second) (enemyProximity westPositions) eastFlaglessBots

      -- Partition by enemy visibility
      partitionByVisibility proximitiesSq =
        let visibility (botID, distSq) =
              if distSq >= Fixed.toFixedSq C.viewRadius
                then Left botID
                else Right botID
        in  partitionEithers $ map visibility proximitiesSq
      (westNotInViewBots, _westVisibleBots) = partitionByVisibility westProximitiesSq
      (eastNotInViewBots, _eastVisibleBots) = partitionByVisibility eastProximitiesSq
  in  -- TODO: Could try to assign priorities to not-in-view bots
      UnimportantBots
        { ub_westUnimportant = westDeadBots ++ westNotInViewBots
        , ub_eastUnimportant = eastDeadBots ++ eastNotInViewBots
        }

-- Helper
distribute :: (a, Either b c) -> Either (a, b) (a, c)
distribute (a, e) = bimap (a,) (a,) e

-- Helper
data PlayerCounts = PlayerCounts
  { pc_westHumans :: Int
  , pc_eastHumans :: Int
  , pc_westBots :: Int
  , pc_eastBots :: Int
  }

emptyPlayerCounts :: PlayerCounts
emptyPlayerCounts =
  PlayerCounts
    { pc_westHumans = 0
    , pc_eastHumans = 0
    , pc_westBots = 0
    , pc_eastBots = 0
    }

-- Helper
countPlayers :: (Foldable f) => f Player -> PlayerCounts
countPlayers = foldl' f emptyPlayerCounts
  where
    f counts player =
      case (view player_team player, view player_actorID player) of
        (TeamW, PlayerActor _) -> counts{pc_westHumans = pc_westHumans counts + 1}
        (TeamW, BotActor _) -> counts{pc_westBots = pc_westBots counts + 1}
        (TeamE, PlayerActor _) -> counts{pc_eastHumans = pc_eastHumans counts + 1}
        (TeamE, BotActor _) -> counts{pc_eastBots = pc_eastBots counts + 1}

--------------------------------------------------------------------------------
-- Tutorial

runTutorialEffect ::
  GameParams ->
  MWC.Gen s ->
  GameStateStore s ->
  TutorialEffect ->
  StateT GameStatePure (ST s) ()
runTutorialEffect params gen store = \case
  TutorialSpawnEnemyBot ->
    spawnBot gen store Tutorial.botTutorialTeam (Just PlayerClass.Templar)
  TutorialRemoveEnemyBots -> do
    time <- gets gsp_time
    players <- gets gsp_players
    let botIDs =
          mapMaybe (\case BotActor botID -> Just botID; _ -> Nothing)
            . WIM.keys
            $ players
    for_ botIDs $ \botID ->
      removePlayer params store time (BotActor botID)
  TutorialSpawnEnemyFlag ->
    lift $ do
      Store.spawnTeamBase params store Tutorial.botTutorialTeam
      Store.spawnBaseFlag params store Tutorial.botTutorialTeam
  TutorialSpawnAlliedFlag ->
    lift $ do
      Store.spawnTeamBase params store Tutorial.playerTutorialTeam
      Store.spawnBaseFlag params store Tutorial.playerTutorialTeam
  TutorialSetBotCommandsForPsiStorm -> do
    -- Tell bot to cover themselves with their drones
    playerViews <- lift $ Store.viewPlayers store
    field @"gsp_players" %= \players ->
      flip WIM.mapWithKey players $ \actorID player ->
        case actorID of
          PlayerActor _ -> player
          BotActor _ ->
            let botPosition =
                  fromMaybe 0 $ do
                    overseerView <-
                      pv_overseerView <$> WIM.lookup actorID playerViews
                    case overseerView of
                      OV_Dead{} -> Nothing
                      OV_Alive OverseerViewAlive{ova_pos} -> Just ova_pos
            in  player
                  & field @"player_controls"
                  . field @"control_buttons"
                  . control_move
                  .~ MoveNeutral
                  & field @"player_controls"
                  . field @"control_buttons"
                  . control_drone
                  .~ DroneAttraction
                  & field @"player_controls"
                  . field @"control_worldPos"
                  .~ botPosition
  TutorialFreezeBots -> do
    -- Don't let bots move
    field @"gsp_players" %= \players ->
      flip WIM.mapWithKey players $ \actorID player ->
        case actorID of
          PlayerActor _ -> player
          BotActor _ ->
            player
              & field @"player_controls"
              . field @"control_buttons"
              . control_move
              .~ MoveNeutral
              & field @"player_controls"
              . field @"control_buttons"
              . control_drone
              .~ DroneNeutral
