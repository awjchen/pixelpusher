module Pixelpusher.Game.State.Outer.ParticleStore (
  ParticleStore,
  new,
  insert,
  insertMany,
  trim,
  getParticles,
  Particles (..),
) where

import Data.Foldable (toList)
import Data.List.NonEmpty (NonEmpty ((:|)), (<|))
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Map
import Data.Serialize (Serialize)

import Pixelpusher.Custom.Serialize ()
import Pixelpusher.Game.Time

import Pixelpusher.Game.Particles

--------------------------------------------------------------------------------

-- | A collection of "live" particles. Expired particles must be manually
-- removed using 'trim'.
newtype ParticleStore
  = ParticleStore (Map Time (NonEmpty (Particle SomeParticle)))
  deriving newtype (Serialize)

new :: ParticleStore
new = ParticleStore Map.empty

-- | Insert a particle to the store with the given start time
insert ::
  forall p. (IsParticle p) => Time -> p -> ParticleStore -> ParticleStore
insert startTime particleData (ParticleStore m) =
  let endTime = addTicks (lifetimeTicks @p) startTime
      particle =
        Particle
          { particle_startTime = startTime
          , particle_endTime = endTime
          , particle_type = toSomeParticle particleData
          }
      addParticle = \case
        Nothing -> Just $ particle :| []
        Just particles -> Just $ particle <| particles
  in  ParticleStore $ Map.alter addParticle endTime m

-- | Insert many particles with the same type and start time
insertMany ::
  forall p. (IsParticle p) => Time -> [p] -> ParticleStore -> ParticleStore
insertMany startTime particleData (ParticleStore m) =
  case List.NonEmpty.nonEmpty particleData of
    Nothing -> ParticleStore m
    Just particleDataNonEmpty ->
      let endTime = addTicks (lifetimeTicks @p) startTime
          particles =
            flip fmap particleDataNonEmpty $ \p ->
              Particle
                { particle_startTime = startTime
                , particle_endTime = endTime
                , particle_type = toSomeParticle p
                }
          addParticle = \case
            Nothing -> Just particles
            Just existingParticles -> Just $ particles <> existingParticles
      in  ParticleStore $ Map.alter addParticle endTime m

-- | Remove particles that end at or before the provided time
trim :: Time -> ParticleStore -> ParticleStore
trim time (ParticleStore m) = ParticleStore $ snd $ Map.split time m

-- | Get all particles in the store
getParticles :: ParticleStore -> Particles
getParticles (ParticleStore m) =
  partitionParticles $ concatMap toList $ Map.elems m
