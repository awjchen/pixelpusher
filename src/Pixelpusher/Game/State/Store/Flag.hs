{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Pixelpusher.Game.State.Store.Flag (
  FlagStore,
  newFlagStore,
  FlagReadStore,
  readFlagStore,
  ReadFlag (..),
  PartitionedFlags (..),
  runFlags,
  accumulatedFlagTicks,
  getFlags,
  tryDropOverseerFlag,
  FlagDropResult (..),
) where

import Control.Monad (when)
import Control.Monad.ST
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.Maybe (catMaybes)
import Data.Traversable (for)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.CollisionEvents (FlagCollisionEvent (..))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.Parameters.Flag
import Pixelpusher.Game.State.Store.Dynamics
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadOnly,
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Team

--------------------------------------------------------------------------------
-- Store type and boilerplate

type FlagStore' s =
  DenseStore s (SEntityID FlagEntity) (SEntityID FlagEntity) FlagComponentVec

-- | Tracks the flags carried by entities.
newtype FlagStore s = FlagStore (FlagStore' s)
  deriving newtype
    ( ReadStore s (SEntityID FlagEntity) FlagComponent
    , Store s (SEntityID FlagEntity) FlagComponent
    , Mutable s (DenseStoreSnapshot (SEntityID FlagEntity) FlagComponentVec)
    , SureReadStore s (SEntityID FlagEntity) FlagComponent
    , ReadFlag s
    )

instance Copyable s (FlagStore s) where
  copy (FlagStore target) (FlagStore source) =
    DS.copyDenseStore target source

newFlagStore :: ST s (FlagStore s)
newFlagStore = FlagStore <$> DS.newDenseStore C.maxNumEntities

newtype FlagReadStore s = FlagReadStore (FlagStore s)
  deriving newtype
    ( ReadStore s (SEntityID FlagEntity) FlagComponent
    , SureReadStore s (SEntityID FlagEntity) FlagComponent
    , ReadFlag s
    )

readFlagStore :: FlagStore s -> FlagReadStore s
readFlagStore = FlagReadStore

--------------------------------------------------------------------------------
-- Query

class ReadFlag s store where
  readHasFlag ::
    (SubEntityID i (SEntityID FlagEntity)) =>
    store ->
    i ->
    ST s Bool

  -- | Read overseer flag status. Returns (1) whether an overseer has a flag
  -- and (2) the last time at which the overseer picked up or dropped a flag.
  readOverseerHasFlag ::
    store ->
    EntityID Overseer ->
    ST s (Bool, Time)

instance ReadFlag s (FlagStore' s) where
  readHasFlag comps = DS.sureLookup_1 comps vec_flag_hasFlag
  {-# INLINE readHasFlag #-}

  readOverseerHasFlag comps overseerID = do
    (hasFlag, conditionStart, conditionLast) <-
      DS.sureLookup_3
        comps
        vec_flag_hasFlag
        vec_flag_conditionStart
        vec_flag_conditionLast
        overseerID
    pure
      if hasFlag
        then (hasFlag, conditionStart)
        else (hasFlag, conditionLast)
  {-# INLINE readOverseerHasFlag #-}

--------------------------------------------------------------------------------
-- Integration

runFlags ::
  FlagStore s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  FlagParams ->
  Time ->
  [FlagCollisionEvent] ->
  ST s (FlagEvents, PartitionedFlags)
runFlags
  (FlagStore flagComps)
  dynComps
  teamComps
  actorComps
  params
  time
  collisions = do
    baseFlagsCount <- countBaseFlags flagComps teamComps
    transferEvents <-
      transferFlags
        params
        flagComps
        dynComps
        teamComps
        actorComps
        time
        baseFlagsCount
        collisions
    autoReturnEvents <-
      autoReturnFlags params time dynComps teamComps flagComps
    partitionedFlags <- getFlagsPartitioned flagComps
    pure (transferEvents <> autoReturnEvents, partitionedFlags)

--------------------------------------------------------------------------------

countBaseFlags ::
  forall s. FlagStore' s -> ReadOnly (TeamStore s) -> ST s BaseFlagsCount
countBaseFlags flagComps teamComps =
  DS.foldM_idx_1 flagComps vec_flag_hasFlag f (BaseFlagsCount 0 0)
  where
    f :: BaseFlagsCount -> SEntityID FlagEntity -> Bool -> ST s BaseFlagsCount
    f count flagID hasFlag =
      case refineEntityID flagID of
        BaseFlagID _ ->
          if not hasFlag
            then pure count
            else
              sureLookup teamComps flagID <&> \case
                TeamW -> count{bfc_teamW = bfc_teamW count + 1}
                TeamE -> count{bfc_teamE = bfc_teamE count + 1}
        _ ->
          pure count

data BaseFlagsCount = BaseFlagsCount
  { bfc_teamW :: Int
  , bfc_teamE :: Int
  }

transferFlags ::
  FlagParams ->
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  BaseFlagsCount ->
  [FlagCollisionEvent] ->
  ST s FlagEvents
transferFlags
  params
  flagComps
  dynComps
  teamComps
  actorComps
  time
  baseFlagsCount
  collisions =
    flip execStateT mempty $
      for_ collisions $ \case
        FlagOverlap eid1 eid2 -> do
          -- Note: If you change the interacting pairs of flag entities below, you
          -- will also need to change pairs of entities for which the collision
          -- detection emits flag events.
          case (refineEntityID eid1, refineEntityID eid2) of
            --
            (OverseerID overseerID, FlagID flagID) ->
              overseer_flagBody' overseerID flagID
            (FlagID flagID, OverseerID overseerID) ->
              overseer_flagBody' overseerID flagID
            --
            (OverseerID overseerID, BaseFlagID baseFlagID) ->
              overseer_baseFlag' overseerID baseFlagID
            (BaseFlagID baseFlagID, OverseerID overseerID) ->
              overseer_baseFlag' overseerID baseFlagID
            --
            (OverseerID overseerID, TeamBaseID teamBaseID) ->
              overseer_teamBase' overseerID teamBaseID
            (TeamBaseID teamBaseID, OverseerID overseerID) ->
              overseer_teamBase' overseerID teamBaseID
            --
            _ -> pure ()
        FlagAreaOverlap flagID overseerID ->
          overseer_flagArea' overseerID flagID
    where
      overseer_baseFlag' =
        overseer_baseFlag flagComps dynComps teamComps actorComps time
      overseer_flagArea' =
        overseer_flagArea params flagComps dynComps teamComps actorComps time
      overseer_flagBody' =
        overseer_flagBody flagComps dynComps teamComps actorComps time
      overseer_teamBase' =
        overseer_teamBase flagComps dynComps teamComps actorComps time baseFlagsCount

overseer_teamBase ::
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  BaseFlagsCount ->
  EntityID Overseer ->
  EntityID TeamBase ->
  StateT FlagEvents (ST s) ()
overseer_teamBase
  flagComps
  dynComps
  teamComps
  actorComps
  time
  baseFlagsCount
  overseerID
  teamBaseID =
    do
      overseerTeam <- lift $ sureLookup teamComps overseerID
      overseerHasFlag <-
        lift $ DS.sureLookup_1 flagComps vec_flag_hasFlag overseerID
      baseTeam <- lift $ sureLookup teamComps teamBaseID
      when (overseerTeam == baseTeam && overseerHasFlag) $ do
        lift $ runMarkFlagConditionSatisfied flagComps time teamBaseID
        let hasBaseFlag = case overseerTeam of
              TeamW -> bfc_teamW baseFlagsCount > 0
              TeamE -> bfc_teamE baseFlagsCount > 0
        when hasBaseFlag $ do
          lift $
            DS.sureSet_2
              flagComps
              vec_flag_hasFlag
              vec_flag_conditionLast
              overseerID
              False
              time
          actorID <- lift $ sureLookup actorComps overseerID
          (pos, vel) <- lift $ readPosVel dynComps overseerID
          let event =
                FlagCapture
                  { flagCapture_team = overseerTeam
                  , flagCapture_actorID = actorID
                  , flagCapture_position = pos
                  , flagCapture_velocity = vel
                  }
          field @"flagEvents_captures" %= (:) event

overseer_baseFlag ::
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  EntityID Overseer ->
  EntityID BaseFlag ->
  StateT FlagEvents (ST s) ()
overseer_baseFlag
  flagComps
  dynComps
  teamComps
  actorComps
  time
  overseerID
  baseFlagID = do
    overseerTeam <- lift $ sureLookup teamComps overseerID
    flagTeam <- lift $ sureLookup teamComps baseFlagID
    when (overseerTeam /= flagTeam) $
      zoom (field @"flagEvents_pickups") $
        -- Picking up an enemy flag
        pickupEnemyFlag
          flagComps
          dynComps
          actorComps
          time
          overseerTeam
          overseerID
          (PickupBaseFlag baseFlagID)

overseer_flagBody ::
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  EntityID Overseer ->
  EntityID Flag ->
  StateT FlagEvents (ST s) ()
overseer_flagBody
  flagComps
  dynComps
  teamComps
  actorComps
  time
  overseerID
  flagID = do
    overseerTeam <- lift $ sureLookup teamComps overseerID
    flagTeam <- lift $ sureLookup teamComps flagID
    when (overseerTeam /= flagTeam) $ do
      -- Picking up an enemy flag
      zoom (field @"flagEvents_pickups") $
        pickupEnemyFlag
          flagComps
          dynComps
          actorComps
          time
          overseerTeam
          overseerID
          (PickupNonBaseFlag flagID)

overseer_flagArea ::
  FlagParams ->
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  EntityID Overseer ->
  EntityID Flag ->
  StateT FlagEvents (ST s) ()
overseer_flagArea
  params
  flagComps
  dynComps
  teamComps
  actorComps
  time
  overseerID
  flagID = do
    overseerTeam <- lift $ sureLookup teamComps overseerID
    flagTeam <- lift $ sureLookup teamComps flagID
    when (overseerTeam == flagTeam) $ do
      -- Recovering an allied flag
      recoverFlag <- lift $ do
        runMarkFlagConditionSatisfied flagComps time flagID
        DS.sureModifyReturn flagComps flagID $
          \(field @"flag_ticks" +~ 1 -> flagComp) ->
            tryRecoverFlag params time flagComp
      when recoverFlag $ do
        actorID <- lift $ sureLookup actorComps overseerID
        pos <- lift $ readPos dynComps flagID
        field @"flagEvents_recoveries"
          %= (:) (FlagRecovery overseerTeam (Just actorID) pos)

pickupEnemyFlag ::
  FlagStore' s ->
  DynamicsReadStore s ->
  ReadOnly (ActorEntityStore s) ->
  Time ->
  Team ->
  EntityID Overseer ->
  FlagPickupType ->
  StateT [FlagPickup] (ST s) ()
pickupEnemyFlag
  flagComps
  dynComps
  actorComps
  time
  overseerTeam
  overseerID
  flagPickup = do
    let flagID :: SEntityID FlagEntity
        flagID =
          case flagPickup of
            PickupNonBaseFlag flagID_ -> subEntityID flagID_
            PickupBaseFlag baseFlagID -> subEntityID baseFlagID
    overseerFlag <- lift $ sureLookup flagComps overseerID
    flagFlag <- lift $ sureLookup flagComps flagID
    let condition =
          flag_hasFlag flagFlag
            && not (flag_hasFlag overseerFlag)
            && time >= flag_time overseerFlag -- Is the overseer allowed to pick up flags?
    when condition $ do
      lift $ do
        DS.sureSet_2
          flagComps
          vec_flag_hasFlag
          vec_flag_conditionStart
          overseerID
          True
          time
        DS.sureSet_1 flagComps vec_flag_hasFlag flagID False
      actorID <- lift $ sureLookup actorComps overseerID
      flagPos <- lift $ readPos dynComps flagID
      -- Only notify players when a base flag is picked up
      let flagType =
            case flagPickup of
              PickupNonBaseFlag{} -> DroppedFlagType
              PickupBaseFlag{} -> BaseFlagType
      modify' $
        (:)
          FlagPickup
            { flagPickup_carrierTeam = overseerTeam
            , flagPickup_actor = actorID
            , flagPickup_flagPosition = flagPos
            , flagPickup_type = flagType
            }

data FlagPickupType
  = PickupNonBaseFlag (EntityID Flag)
  | PickupBaseFlag (EntityID BaseFlag)

autoReturnFlags ::
  FlagParams ->
  Time ->
  DynamicsReadStore s ->
  ReadOnly (TeamStore s) ->
  FlagStore' s ->
  ST s FlagEvents
autoReturnFlags params time dynComps teamComps flagComps = do
  returnedFlagIDs <-
    fmap catMaybes $
      DS.mapWithIndexReturn flagComps $ \flagEntityID flagComp ->
        case refineEntityID flagEntityID of
          FlagID flagID ->
            let (flagRecovered, flagComp') =
                  tryRecoverFlag params time flagComp
            in  (flagComp', if flagRecovered then Just flagID else Nothing)
          _ -> (flagComp, Nothing)
  flagRecoveries <-
    for returnedFlagIDs $ \flagID -> do
      flagTeam <- sureLookup teamComps flagID
      pos <- readPos dynComps flagID
      pure $ FlagRecovery flagTeam Nothing pos
  pure
    mempty
      { flagEvents_recoveries = flagRecoveries
      }

tryRecoverFlag :: FlagParams -> Time -> FlagComponent -> (Bool, FlagComponent)
tryRecoverFlag params time flagComp =
  let ticksAccumulated = accumulatedFlagTicks params time flagComp
      recoverFlag = ticksAccumulated >= gp_flagRecoveryTicks params
  in  if
        | not recoverFlag ->
            -- Recovery conditions not met, just increment ticks
            (False, flagComp)
        | not (flag_hasFlag flagComp) ->
            -- Flag already taken: do nothing
            (False, flagComp)
        | otherwise ->
            -- All conditions met: take flag
            (True, flagComp & field @"flag_hasFlag" .~ False)

accumulatedFlagTicks :: FlagParams -> Time -> FlagComponent -> Ticks
accumulatedFlagTicks params time flagComp =
  let ticksSinceDropped = time `diffTime` flag_time flagComp
      ticksPresent = flag_ticks flagComp
      autoRecoveryFactor = gp_flagAutoRecoveryFactor params
  in  ticksPresent + ticksSinceDropped `div` fromIntegral autoRecoveryFactor

--------------------------------------------------------------------------------

getFlagsPartitioned :: FlagStore' s -> ST s PartitionedFlags
getFlagsPartitioned flagComps =
  foldr partitionFlag emptyPartitionedFlags <$> DS.toList flagComps

data PartitionedFlags = PartitionedFlags
  { pf_emptySourceFlags :: [SEntityID FlagEntity]
  , pf_flagCarriers :: [EntityID Overseer]
  , pf_otherFlags :: [SEntityID FlagEntity]
  }

emptyPartitionedFlags :: PartitionedFlags
emptyPartitionedFlags =
  PartitionedFlags
    { pf_emptySourceFlags = []
    , pf_flagCarriers = []
    , pf_otherFlags = []
    }

partitionFlag ::
  (SEntityID FlagEntity, FlagComponent) ->
  PartitionedFlags ->
  PartitionedFlags
partitionFlag (flagID, flagComp) ps =
  case refineEntityID flagID of
    OverseerID overseerID ->
      if flag_hasFlag flagComp
        then ps{pf_flagCarriers = overseerID : pf_flagCarriers ps}
        else ps{pf_otherFlags = flagID : pf_otherFlags ps}
    FlagID _ ->
      if not (flag_hasFlag flagComp)
        then ps{pf_emptySourceFlags = flagID : pf_emptySourceFlags ps}
        else ps{pf_otherFlags = flagID : pf_otherFlags ps}
    BaseFlagID _ ->
      if not (flag_hasFlag flagComp)
        then ps{pf_emptySourceFlags = flagID : pf_emptySourceFlags ps}
        else ps{pf_otherFlags = flagID : pf_otherFlags ps}
    TeamBaseID _ ->
      ps{pf_otherFlags = flagID : pf_otherFlags ps}

--------------------------------------------------------------------------------
-- Flag conditions

runMarkFlagConditionSatisfied ::
  (SubEntityID i (SEntityID FlagEntity)) =>
  FlagStore' s ->
  Time ->
  i ->
  ST s ()
runMarkFlagConditionSatisfied flagComps time flagID =
  DS.sureModify_2
    flagComps
    vec_flag_conditionStart
    vec_flag_conditionLast
    flagID
    (markFlagConditionSatisfied time)

--------------------------------------------------------------------------------
-- Query

getFlags ::
  FlagStore s -> ReadOnly (TeamStore s) -> ST s [(SEntityID FlagEntity, Team)]
getFlags (FlagStore flagComps) teamComps =
  DS.indicesPred_1 flagComps vec_flag_hasFlag id
    >>= traverse \flagID -> do
      team <- sureLookup teamComps flagID
      pure $
        let flagTeam = case refineEntityID flagID of
              OverseerID _ -> oppositeTeam team -- Overseers carry flags of the opposite team
              FlagID _ -> team
              BaseFlagID _ -> team
              TeamBaseID _ -> team
        in  (flagID, flagTeam)

--------------------------------------------------------------------------------
-- Operations

tryDropOverseerFlag ::
  FlagParams ->
  FlagStore s ->
  Time ->
  EntityID Overseer ->
  ST s FlagDropResult
tryDropOverseerFlag params (FlagStore flagComps) time overseerID =
  DS.sureModifyReturn_3
    flagComps
    vec_flag_hasFlag
    vec_flag_time
    vec_flag_conditionLast
    overseerID
    \hasFlag noPickupUntilTime conditionLast ->
      if hasFlag
        then (FlagDropped, False, addTicks delay time, time)
        else (HadNoFlag, hasFlag, noPickupUntilTime, conditionLast)
  where
    delay = gp_flagPickupDisableTicks params

-- | Return value of 'overseerDropFlag'
data FlagDropResult = FlagDropped | HadNoFlag
