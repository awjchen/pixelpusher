{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.State.Store.Combat (
  CombatStore,
  newCombatStore,
  CombatReadStore,
  readCombatStore,
  ReadCombat (..),
  setHealthRegenType,
  addShields,
  debugApplyDamageFraction,
  CombatDeathEvent (..),
  SmiteOverseers (..),
  runCombat,
  recentDamage,
  recentDamageDecayFactorPerTick,
) where

import Control.Monad (when)
import Control.Monad.ST
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Writer.CPS
import Control.Monad.Writer.Class ()
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields
import Data.Maybe (catMaybes)
import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom
import System.Random.MWC

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.CombatEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )
import {-# SOURCE #-} Pixelpusher.Game.State.Core qualified as Store (
  ReadOnly,
  lookup,
  sureLookup,
 )

import Pixelpusher.Game.State.Store.ActorEntity

--------------------------------------------------------------------------------
-- Store

type CombatStore' s =
  DS.DenseStore
    s
    (SEntityID CombatEntity)
    (SEntityID CombatEntity)
    CombatComponentVec

newtype CombatStore s = CombatStore (CombatStore' s)
  deriving newtype
    ( ReadStore s (SEntityID CombatEntity) CombatComponent
    , Store s (SEntityID CombatEntity) CombatComponent
    , Mutable s (DS.DenseStoreSnapshot (SEntityID CombatEntity) CombatComponentVec)
    , SureReadStore s (SEntityID CombatEntity) CombatComponent
    , ReadCombat s
    )

instance Copyable s (CombatStore s) where
  copy (CombatStore target) (CombatStore source) =
    DS.copyDenseStore target source

newCombatStore :: ST s (CombatStore s)
newCombatStore = CombatStore <$> DS.newDenseStore C.maxNumEntities

newtype CombatReadStore s = CombatReadStore (CombatStore s)
  deriving newtype
    ( ReadStore s (SEntityID CombatEntity) CombatComponent
    , SureReadStore s (SEntityID CombatEntity) CombatComponent
    , ReadCombat s
    )

readCombatStore :: CombatStore s -> CombatReadStore s
readCombatStore = CombatReadStore

--------------------------------------------------------------------------------
-- Query

class ReadCombat s store where
  getHealth ::
    (SubEntityID i (SEntityID CombatEntity)) => store -> i -> ST s Fixed
  getHealthFraction ::
    (SubEntityID i (SEntityID CombatEntity)) => store -> i -> ST s Fixed
  getHealthShields ::
    (SubEntityID i (SEntityID CombatEntity)) => store -> i -> ST s (Fixed, Fixed)

instance ReadCombat s (CombatStore' s) where
  getHealth combatComps = DS.sureLookup_1 combatComps vec_com_currentHealth
  {-# INLINE getHealth #-}
  getHealthFraction combatComps =
    fmap (uncurry (/))
      . DS.sureLookup_2 combatComps vec_com_currentHealth vec_com_maxHealth
  {-# INLINE getHealthFraction #-}
  getHealthShields combatComps =
    DS.sureLookup_2 combatComps vec_com_currentHealth vec_com_shields
  {-# INLINE getHealthShields #-}

--------------------------------------------------------------------------------
-- Operations

setHealthRegenType ::
  (SubEntityID i (SEntityID CombatEntity)) =>
  CombatStore s ->
  i ->
  HealthRegenType ->
  ST s ()
setHealthRegenType (CombatStore combatComps) =
  DS.sureSet_1 combatComps vec_com_healthRegenType

addShields ::
  (SubEntityID i (SEntityID CombatEntity)) =>
  CombatStore s ->
  i ->
  Fixed ->
  ST s ()
addShields (CombatStore combatComps) combatID extraShields =
  DS.sureModify_1_1 combatComps vec_com_maxHealth vec_com_shields combatID $
    \maxHealth shields -> min maxHealth (shields + extraShields)

debugApplyDamageFraction ::
  (SubEntityID i (SEntityID CombatEntity)) =>
  CombatStore s ->
  Time ->
  i ->
  Fixed ->
  ST s ()
debugApplyDamageFraction (CombatStore combatComps) time combatID damageFrac = do
  DS.sureModify combatComps combatID $ \combatComp ->
    let damage =
          -- Don't kill the entity so that we don't have to emit proper death
          -- events
          min (com_currentHealth combatComp - 0.01) $
            com_maxHealth combatComp * damageFrac
    in  snd $ forceApplyDamage time damage combatComp

--------------------------------------------------------------------------------
-- System

data SmiteOverseers = SmiteOverseers | DoNotSmiteOverseers

runCombat ::
  Gen s ->
  CombatStore s ->
  Store.ReadOnly (ActorEntityStore s) ->
  CombatParams ->
  Time ->
  SmiteOverseers ->
  [Pair (EntityIDPair CombatEntity) Collision] ->
  [PsiStormOverlap] ->
  [SpawnOverlap] ->
  ST s CombatEvents
runCombat
  gen
  (CombatStore combatComps)
  actorComps
  params
  time
  shouldSmiteOverseers
  collisions
  psiStormOverlaps
  spawnOverlaps =
    execWriterT $ do
      lift $
        when (gp_unlimitedSpawnInvulnerability params) $
          applySpawnInvulnerability combatComps time spawnOverlaps
      lift (regenerateHealth combatComps params time)
        >>= scribe (field @"ce_deaths")
      for_ psiStormOverlaps $
        applyPsiStormDamage params combatComps actorComps time
      traverse_ (tradeBlows combatComps actorComps time) collisions
      case shouldSmiteOverseers of
        DoNotSmiteOverseers ->
          pure ()
        SmiteOverseers ->
          lift (smiteOverseers gen combatComps)
            >>= scribe (field @"ce_deaths")

data IsKilled = Killed | NotKilled
  deriving stock (Eq)

data IsInvulnerable = Invulnerable | NotInvulnerable
  deriving stock (Eq)

tradeBlows ::
  CombatStore' s ->
  Store.ReadOnly (ActorEntityStore s) ->
  Time ->
  Pair (EntityIDPair CombatEntity) Collision ->
  WriterT CombatEvents (ST s) ()
tradeBlows combatComps actorComps time collision = do
  let eidPair :!: collision' = collision
      (eid1, eid2) = getEntityIDPair eidPair

  currentHealth1 <-
    lift $
      uncurry (+)
        <$> DS.sureLookup_2 combatComps vec_com_currentHealth vec_com_shields eid1
  currentHealth2 <-
    lift $
      uncurry (+)
        <$> DS.sureLookup_2 combatComps vec_com_currentHealth vec_com_shields eid2

  let damage = min currentHealth1 $ min currentHealth2 $ col_damage collision'
  when (damage > 0) $ do
    let applyDamage' = applyDamage time damage
    (isKilled1, isInvulnerable1) <-
      lift $
        DS.sureModifyReturn combatComps eid1 applyDamage'
    (isKilled2, isInvulnerable2) <-
      lift $
        DS.sureModifyReturn combatComps eid2 applyDamage'

    when (isKilled1 == Killed) $ do
      mKillerPid2 <- lift $ Store.lookup actorComps eid2
      scribe (field @"ce_deaths") $
        pure $
          CombatDeathEvent{killed_eid = eid1, killer_mPid = mKillerPid2}
    when (isKilled2 == Killed) $ do
      mKillerPid1 <- lift $ Store.lookup actorComps eid1
      scribe (field @"ce_deaths") $
        pure $
          CombatDeathEvent{killed_eid = eid2, killer_mPid = mKillerPid1}

    participants <-
      lift $
        case (refineEntityID eid1, refineEntityID eid2) of
          (OverseerID overseerID1, OverseerID overseerID2) -> do
            CollisionDamageOverseers
              <$> Store.sureLookup actorComps overseerID1
              <*> Store.sureLookup actorComps overseerID2
          (OverseerID overseerID, DroneID _) -> do
            CollisionDamageOverseerDrone
              <$> Store.sureLookup actorComps overseerID
          (DroneID _, OverseerID overseerID) -> do
            CollisionDamageOverseerDrone
              <$> Store.sureLookup actorComps overseerID
          (DroneID _, DroneID _) ->
            pure CollisionDamageDrones

    let collisionDamage = flip fmap collision $ \Collision{..} ->
          CollisionDamage
            { cd_position = col_position
            , cd_velocity = col_velocity
            , cd_damage = damage
            , cd_direction = col_direction
            , cd_mActorID1 =
                if isInvulnerable1 == Invulnerable
                  then Nothing
                  else col_mActorID1
            , cd_mActorID2 =
                if isInvulnerable2 == Invulnerable
                  then Nothing
                  else col_mActorID2
            , cd_participants = participants
            , cd_isKillingBlow =
                isKilled1 == Killed || isKilled2 == Killed
            }
    scribe (field @"ce_collisions") $ pure collisionDamage

-- | Apply damage, but not to invulnerable entities
applyDamage ::
  Time ->
  Fixed ->
  CombatComponent ->
  ((IsKilled, IsInvulnerable), CombatComponent)
applyDamage time rawDamage combatComp
  | isInvulnerable time combatComp = ((NotKilled, Invulnerable), combatComp)
  | otherwise =
      let (isKilled, combatComp') = forceApplyDamage time rawDamage combatComp
      in  ((isKilled, NotInvulnerable), combatComp')

-- | Apply damage regardless of invulnerability
--
-- TODO: Add strictness annotations? Look at core first.
forceApplyDamage ::
  Time ->
  Fixed ->
  CombatComponent ->
  (IsKilled, CombatComponent)
forceApplyDamage time rawDamage combatComp =
  let currentHealth = com_currentHealth combatComp
      currentShields = com_shields combatComp
      shieldDamage = min currentShields rawDamage
      healthDamage = min currentHealth (rawDamage - shieldDamage)
      totalDamage = shieldDamage + healthDamage
      newShields = currentShields - shieldDamage
      newHealth = currentHealth - healthDamage
      oldHealthDamage = recentDamage time combatComp
      updateRegenInterruptTime oldTime =
        if totalDamage > 0 then time else oldTime
      combatComp' =
        combatComp
          & field @"com_currentHealth"
          .~ newHealth
          & field @"com_shields"
          .~ newShields
          & field @"com_lastDamageTime"
          .~ time
          & field @"com_lastDamageAmount"
          .~ totalDamage
          + oldHealthDamage
            & field @"com_lastRegenInterruptTime"
            %~ updateRegenInterruptTime
      isKilled =
        if newHealth <= 0 && currentHealth > 0
          then Killed
          else NotKilled
  in  (isKilled, combatComp')

-- | Returns total damage taken weighted by an exponential decay with time
recentDamage :: Time -> CombatComponent -> Fixed
recentDamage time combatComp =
  let oldTime = com_lastDamageTime combatComp
      ticksSinceDamage =
        -- 32767 = 2^15 - 1
        fromIntegral $ min 32767 $ getTicks $ time `diffTime` oldTime
      oldDamage = com_lastDamageAmount combatComp
  in  oldDamage * exp (-ticksSinceDamage * recentDamageDecayFactorPerTick)

{-# INLINE recentDamageDecayFactorPerTick #-}
recentDamageDecayFactorPerTick :: (Fractional a) => a
recentDamageDecayFactorPerTick = 0.175 -- half life of 4 ticks

regenerateHealth ::
  CombatStore' s -> CombatParams -> Time -> ST s [CombatDeathEvent]
regenerateHealth combatComps params time =
  if not (time `atMultipleOf` C.healthRegenInterval)
    then pure []
    else fmap catMaybes
      $ DS.mapReturn_idx_3_2
        combatComps
        vec_com_healthRegenType
        vec_com_maxHealth
        vec_com_lastRegenInterruptTime
        vec_com_currentHealth
        vec_com_shields
      $ \combatID regenType maxHP regenInterruptTime hp shields ->
        let newHP = case regenType of
              -- Overseers
              HealthRegenQuadratic fullRegenTicks ->
                let ticksSinceInterrupt =
                      -- `min` necessary to prevent fixed-point overflow
                      min fullRegenTicks $ time `diffTime` regenInterruptTime
                    periodsSinceInterrupt =
                      fromIntegral ticksSinceInterrupt
                        / fromIntegral C.healthRegenInterval
                    rateDivisor =
                      let regenPeriods =
                            fromIntegral fullRegenTicks
                              / fromIntegral C.healthRegenInterval
                          numHealUnits = 0.5 * regenPeriods * (regenPeriods + 1)
                      in  numHealUnits
                    hp' = hp + (maxHP / rateDivisor) * periodsSinceInterrupt
                    hpClamp = max hp maxHP
                in  min hpClamp hp'
              -- Drones
              HealthRegenExponential expRate ->
                maxHP - expRate * (maxHP - hp)
              -- Decaying drones
              HealthRegenLinear linearRate ->
                hp + maxHP * linearRate

            newShields = shields * gp_shieldDecayPeriodFactor params

            mDeathEvent =
              if newHP > 0
                then Nothing
                else
                  Just
                    CombatDeathEvent
                      { killed_eid = combatID
                      , killer_mPid = Nothing
                      }
        in  (newHP, newShields, mDeathEvent)

smiteOverseers :: Gen s -> CombatStore' s -> ST s [CombatDeathEvent]
smiteOverseers gen combatComps =
  catMaybes
    <$> DS.mapMReturn_idx_1
      combatComps
      vec_com_currentHealth
      \combatID hp ->
        case refineEntityID combatID of
          OverseerID _
            | hp > 0 ->
                uniformRM (1, C.tickRate_hz `div` 6) gen <&> \x ->
                  if x == 1
                    then
                      let combatDeathEvent =
                            CombatDeathEvent
                              { killed_eid = combatID
                              , killer_mPid = Nothing
                              }
                      in  (0, Just combatDeathEvent)
                    else (hp, Nothing)
          _ ->
            pure (hp, Nothing)

applyPsiStormDamage ::
  CombatParams ->
  CombatStore' s ->
  Store.ReadOnly (ActorEntityStore s) ->
  Time ->
  PsiStormOverlap ->
  WriterT CombatEvents (ST s) ()
applyPsiStormDamage params combatComps actorComps time pso = do
  let combatID = pso_entityID pso
      damage = gp_psiStormDamagePerHit params * pso_overlap pso
  (isKilled, isInvulnerable') <-
    lift $
      DS.sureModifyReturn combatComps combatID $
        applyDamage time damage
  when (isKilled == Killed) $ do
    mKillerPid <-
      lift $ Store.lookup actorComps $ pso_psiStormEntityID pso
    scribe (field @"ce_deaths") $
      pure
        CombatDeathEvent
          { killed_eid = combatID
          , killer_mPid = mKillerPid
          }
  when (isInvulnerable' == NotInvulnerable) $ do
    scribe (field @"ce_psiStormEffects") $
      pure
        PsiStormEffect
          { pse_mEntityActorID = pso_mEntityActorID pso
          , pse_entityPos = pso_entityPos pso
          , pse_entityVel = pso_entityVel pso
          , pse_damage = damage
          }
    case refineEntityID combatID of
      OverseerID overseerID ->
        scribe (field @"ce_psiStormDamage") $
          pure
            OverseerPsiStormDamage
              { opsd_overseerID = overseerID
              , opsd_damage = damage
              , opsd_mPsiStormActorID = pso_mPsiStormActorID pso
              }
      DroneID _ -> pure ()

applySpawnInvulnerability :: CombatStore' s -> Time -> [SpawnOverlap] -> ST s ()
applySpawnInvulnerability store time spawnOverlaps = do
  let extendedInvulnerabilityTime =
        addTicks C.spawnInvulnerabilityCheckPeriod time
  for_ spawnOverlaps $ \(SpawnOverlap overseerID) ->
    DS.sureModify_1 store vec_com_invulnerableUntil overseerID $
      max extendedInvulnerabilityTime
