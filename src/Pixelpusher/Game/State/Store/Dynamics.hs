{-# LANGUAGE BlockArguments #-}

module Pixelpusher.Game.State.Store.Dynamics (
  DynamicsStore,
  newDynamicsStore,
  DynamicsReadStore,
  readDynamicsStore,
  ReadDynamics (..),
  modifyVelocity,
  modifyVelocityWithMass,
  stepDynamics,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Data.Bifunctor (second)
import Data.Foldable (traverse_)
import Data.Traversable (for)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Dynamics.Controls (boundPlayerControls)
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
  readOnly,
 )
import {-# SOURCE #-} Pixelpusher.Game.State.Core qualified as Store (
  ReadOnly,
 )

import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Combat
import Pixelpusher.Game.State.Store.Minion
import Pixelpusher.Game.State.Store.Powerup
import Pixelpusher.Game.State.Store.PsiStorm
import Pixelpusher.Game.State.Store.Team

import Pixelpusher.Game.State.Store.Dynamics.Collisions (runCollisions)
import Pixelpusher.Game.State.Store.Dynamics.Independent (stepIndependentKinematics)
import Pixelpusher.Game.State.Store.Dynamics.Thermostat (driftingSoftObstacleThermostat)
import Pixelpusher.Game.State.Store.Dynamics.Types (DynamicsStore')

--------------------------------------------------------------------------------
-- Newtype wrapping and boilerplate

newtype DynamicsStore s = DynamicsStore (DynamicsStore' s)
  deriving newtype
    ( ReadStore s (SEntityID DynamicsEntity) DynamicsComponent
    , Store s (SEntityID DynamicsEntity) DynamicsComponent
    , Mutable s (DenseStoreSnapshot (SEntityID DynamicsEntity) DynamicsComponentVec)
    , SureReadStore s (SEntityID DynamicsEntity) DynamicsComponent
    , ReadDynamics s
    )

instance Copyable s (DynamicsStore s) where
  copy (DynamicsStore target) (DynamicsStore source) =
    DS.copyDenseStore target source

newDynamicsStore :: ST s (DynamicsStore s)
newDynamicsStore = DynamicsStore <$> DS.newDenseStore C.maxNumEntities

newtype DynamicsReadStore s = DynamicsReadStore (DynamicsStore s)
  deriving newtype
    ( ReadStore s (SEntityID DynamicsEntity) DynamicsComponent
    , SureReadStore s (SEntityID DynamicsEntity) DynamicsComponent
    , ReadDynamics s
    )

readDynamicsStore :: DynamicsStore s -> DynamicsReadStore s
readDynamicsStore = DynamicsReadStore

--------------------------------------------------------------------------------
-- Query

class ReadDynamics s store where
  -- | Returns a list of all the dynamics components and their EntityIDs,
  -- filtered by EntityID, position, and radius.
  getDynamicsComponentsFiltered ::
    store ->
    (SEntityID DynamicsEntity -> Fixed -> Fixed2 -> Bool) ->
    ST s [(SEntityID DynamicsEntity, DynamicsComponent)]

  readPos ::
    (SubEntityID i (SEntityID DynamicsEntity)) =>
    store ->
    i ->
    ST s Fixed2
  readPosVel ::
    (SubEntityID i (SEntityID DynamicsEntity)) =>
    store ->
    i ->
    ST s (Fixed2, Fixed2)
  readPosRadius ::
    (SubEntityID i (SEntityID DynamicsEntity)) =>
    store ->
    i ->
    ST s (Fixed2, Fixed)
  readPosVelRadius ::
    (SubEntityID i (SEntityID DynamicsEntity)) =>
    store ->
    i ->
    ST s (Fixed2, Fixed2, Fixed)

instance ReadDynamics s (DynamicsStore' s) where
  getDynamicsComponentsFiltered comps =
    DS.toListPred_idx_2 comps vec_dyn_radius vec_dyn_pos
  readPos comps = DS.sureLookup_1 comps vec_dyn_pos
  readPosVel comps = DS.sureLookup_2 comps vec_dyn_pos vec_dyn_vel
  readPosRadius comps = DS.sureLookup_2 comps vec_dyn_pos vec_dyn_radius
  readPosVelRadius comps =
    DS.sureLookup_3 comps vec_dyn_pos vec_dyn_vel vec_dyn_radius
  {-# INLINE getDynamicsComponentsFiltered #-}
  {-# INLINE readPos #-}
  {-# INLINE readPosVel #-}
  {-# INLINE readPosRadius #-}
  {-# INLINE readPosVelRadius #-}

--------------------------------------------------------------------------------
-- Operations

modifyVelocity ::
  (SubEntityID i (SEntityID DynamicsEntity)) =>
  DynamicsStore s ->
  i ->
  (Fixed2 -> Fixed2) ->
  ST s ()
modifyVelocity (DynamicsStore dynComps) = DS.sureModify_1 dynComps vec_dyn_vel

modifyVelocityWithMass ::
  (SubEntityID i (SEntityID DynamicsEntity)) =>
  DynamicsStore s ->
  i ->
  (Fixed -> Fixed2 -> Fixed2) ->
  ST s ()
modifyVelocityWithMass (DynamicsStore dynComps) =
  DS.sureModify_1_1 dynComps vec_dyn_mass vec_dyn_vel

--------------------------------------------------------------------------------
-- Integration

-- | Integrate entity dynamics.
stepDynamics ::
  GameParams ->
  Time ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  PowerupReadStore s ->
  MinionStore s ->
  CombatReadStore s ->
  PsiStormStore s ->
  DynamicsStore s ->
  ST s CollisionEvents
stepDynamics
  params
  time
  playerCtrlsStatus
  teamStore
  actorEntityStore
  powerupStore
  masterMinionStore
  combatStore
  psiStormStore
  (DynamicsStore dynComps) = do
    let dynParams = gp_dynamics params

    -- Soft obstacle thermostat
    driftingSoftObstacleThermostat dynParams time dynComps

    -- Bound player inputs
    controlsClasses <-
      fmap (WIM.map (second ps_playerClass)) $
        for playerCtrlsStatus $ \(playerControls, playerStatus) ->
          (,playerStatus)
            <$> boundPlayerControls dynParams dynComps playerStatus playerControls

    -- Record positions
    -- TODO: read depending on game time instead of copying
    DS.unsafeCopy dynComps vec_dyn_pos_prev10 vec_dyn_pos_prev9
    DS.unsafeCopy dynComps vec_dyn_pos_prev9 vec_dyn_pos_prev8
    DS.unsafeCopy dynComps vec_dyn_pos_prev8 vec_dyn_pos_prev7
    DS.unsafeCopy dynComps vec_dyn_pos_prev7 vec_dyn_pos_prev6
    DS.unsafeCopy dynComps vec_dyn_pos_prev6 vec_dyn_pos_prev5
    DS.unsafeCopy dynComps vec_dyn_pos_prev5 vec_dyn_pos_prev4
    DS.unsafeCopy dynComps vec_dyn_pos_prev4 vec_dyn_pos_prev3
    DS.unsafeCopy dynComps vec_dyn_pos_prev3 vec_dyn_pos_prev2
    DS.unsafeCopy dynComps vec_dyn_pos_prev2 vec_dyn_pos_prev
    DS.unsafeCopy dynComps vec_dyn_pos_prev vec_dyn_pos

    -- Run independent kinematics
    DS.mapM_idx_1_2
      dynComps
      vec_dyn_radius
      vec_dyn_pos
      vec_dyn_vel
      (stepIndependentKinematics dynParams controlsClasses actorEntityStore powerupStore masterMinionStore)

    -- Grow psi-storms
    getPsiStorms psiStormStore >>= traverse_ \psiStormID ->
      DS.sureModify_1 dynComps vec_dyn_radius psiStormID (growPsiStorm dynParams)

    -- Collisions
    partitionCollisionEvents
      <$> runCollisions
        params
        teamStore
        actorEntityStore
        (readOnly psiStormStore)
        combatStore
        time
        dynComps

growPsiStorm :: DynamicsParams -> Fixed -> Fixed
growPsiStorm dynParams radius =
  let targetRadius = gp_psiStormRadius dynParams
      initFrac = gp_psiStormInitialGrowthFraction dynParams
      currentFraction = radius / targetRadius
      scaling =
        1
          - gp_psiStormGrowthConstant dynParams
            * (initFrac + (1 - initFrac) * currentFraction)
      radius' = targetRadius - (targetRadius - radius) * scaling
  in  radius'
