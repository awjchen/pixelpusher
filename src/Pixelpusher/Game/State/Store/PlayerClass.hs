{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.PlayerClass (
  PlayerClassStore,
  newPlayerClassStore,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (BoxedVecStore (..))

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the team to which entity belongs
newtype PlayerClassStore s
  = PlayerClassStore
      ( DenseStore
          s
          (SEntityID PlayerClassEntity)
          (SEntityID PlayerClassEntity)
          (BoxedVecStore PlayerClass)
      )
  deriving newtype
    ( ReadStore s (SEntityID PlayerClassEntity) PlayerClass
    , Store s (SEntityID PlayerClassEntity) PlayerClass
    , Mutable s (DenseStoreSnapshot (SEntityID PlayerClassEntity) (BoxedVecStore PlayerClass))
    , SureReadStore s (SEntityID PlayerClassEntity) PlayerClass
    )

instance Copyable s (PlayerClassStore s) where
  copy (PlayerClassStore target) (PlayerClassStore source) =
    DS.copyDenseStore target source

newPlayerClassStore :: ST s (PlayerClassStore s)
newPlayerClassStore = PlayerClassStore <$> DS.newDenseStore C.maxNumEntities
