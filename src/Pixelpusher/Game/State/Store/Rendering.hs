-- | Export entity data for rendering
module Pixelpusher.Game.State.Store.Rendering (
  getRenderingComponents,
) where

import Control.Monad.ST (ST)
import Data.List qualified as List
import Data.Maybe (fromMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.RenderingComponent
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.State.Core (Stores (..))
import Pixelpusher.Game.State.Core qualified as Core

import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Combat
import Pixelpusher.Game.State.Store.Dynamics
import Pixelpusher.Game.State.Store.Flag hiding (getFlags)
import Pixelpusher.Game.State.Store.Minion
import Pixelpusher.Game.State.Store.PlayerClass
import Pixelpusher.Game.State.Store.Team
import Pixelpusher.Game.State.Store.Transitory

--------------------------------------------------------------------------------

-- | Export entity data for rendering.
--
-- Filters data by entity type and view position.
getRenderingComponents ::
  GameParams ->
  Stores s ->
  Time ->
  Maybe Fixed2 ->
  WIM.IntMap ActorID (Player, PlayerStatus) ->
  ST s RenderingComponents
getRenderingComponents params stores time mViewPos playersMap =
  let viewPredicate' =
        case mViewPos of
          Nothing -> \_ _ _ -> True
          Just viewPos -> \entityID radius pos ->
            entityViewPredicate entityID
              || isInOverseerView C.viewRadius viewPos radius pos
  in  fmap (partitionRenderingComponents . concat) $
        traverse renderingComponent'
          =<< getDynamicsComponentsFiltered
            (store_dynamics stores)
            viewPredicate'
  where
    renderingComponent' =
      renderingComponent
        params
        (Core.readOnly (store_team stores))
        (Core.readOnly (store_combat stores))
        (Core.readOnly (store_transitory stores))
        (readFlagStore (store_flag stores))
        (Core.readOnly (store_actorEntity stores))
        (store_masterMinions stores)
        (Core.readOnly (store_playerClass stores))
        time
        playersMap

-- | For filtering entities by overseer position before exporting their
-- rendering data.
entityViewPredicate :: SEntityID DynamicsEntity -> Bool
entityViewPredicate entityID =
  case refineEntityID entityID of
    OverseerID _ -> True -- expose overseers for minimap
    TeamBaseID _ -> True -- expose team bases for minimap
    SpawnAreaID _ -> True -- expose spawn areas for minimap
    FlagID _ -> True -- for flag capture area
    _ -> False

isInOverseerView :: Fixed -> Fixed2 -> Fixed -> Fixed2 -> Bool
isInOverseerView viewRadius overseerPos objRadius objPos =
  Fixed.normLeq (overseerPos - objPos) (viewRadius + objRadius)

renderingComponent ::
  GameParams ->
  Core.ReadOnly (TeamStore s) ->
  Core.ReadOnly (CombatStore s) ->
  Core.ReadOnly (TransitoryStore s) ->
  FlagReadStore s ->
  Core.ReadOnly (ActorEntityStore s) ->
  MinionStore s ->
  Core.ReadOnly (PlayerClassStore s) ->
  Time ->
  WIM.IntMap ActorID (Player, PlayerStatus) ->
  (SEntityID DynamicsEntity, DynamicsComponent) ->
  ST s [RenderingComponent EntityRendering]
renderingComponent
  params
  teamComps
  combatComps
  transitoryComps
  flagComps
  actorComps
  masterMinionComps
  playerClassComps
  time
  playersMap
  (dynID, dynamicsComp) =
    case refineEntityID dynID of
      OverseerID overseerID -> do
        actorID <- Core.sureLookup actorComps overseerID
        combatComp <- Core.sureLookup combatComps overseerID
        playerClass <- Core.sureLookup playerClassComps overseerID
        (hasFlag, flagPickupDropTime) <-
          readOverseerHasFlag flagComps overseerID
        minionRespawnCooldownTimes <-
          getMinionRespawnCooldownTimes
            masterMinionComps
            (gp_combat params)
            time
            overseerID
        let (player, playerStatus) =
              expectJust "renderingComponent: actorID not found" $
                WIM.lookup actorID playersMap
            playerColor =
              PlayerColor
                (player ^. player_team)
                (player ^. player_color)
                actorID
            ticksSinceDash =
              let timeOfLastDash =
                    lastDashTime $
                      pcs_overseerDashStamina (ps_controlState playerStatus)
              in  time `diffTime` timeOfLastDash
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_Overseer $
            OverseerRendering
              { or_team = view player_team player
              , or_playerClass = playerClass
              , or_color = playerColor
              , or_healthFraction = Fixed.toFloat $ healthFraction combatComp
              , or_shieldsFraction = Fixed.toFloat $ shieldsFraction combatComp
              , or_recentDamage = Fixed.toFloat $ recentDamage time combatComp
              , or_actorID = actorID
              , or_playerName = view player_name player
              , or_prevPos1 = Fixed.toFloats $ dyn_pos_prev dynamicsComp
              , or_prevPos2 = Fixed.toFloats $ dyn_pos_prev2 dynamicsComp
              , or_prevPos3 = Fixed.toFloats $ dyn_pos_prev3 dynamicsComp
              , or_prevPos4 = Fixed.toFloats $ dyn_pos_prev4 dynamicsComp
              , or_prevPos5 = Fixed.toFloats $ dyn_pos_prev5 dynamicsComp
              , or_prevPos6 = Fixed.toFloats $ dyn_pos_prev6 dynamicsComp
              , or_prevPos7 = Fixed.toFloats $ dyn_pos_prev7 dynamicsComp
              , or_prevPos8 = Fixed.toFloats $ dyn_pos_prev8 dynamicsComp
              , or_prevPos9 = Fixed.toFloats $ dyn_pos_prev9 dynamicsComp
              , or_prevPos10 = Fixed.toFloats $ dyn_pos_prev10 dynamicsComp
              , or_hasFlag = hasFlag
              , or_flagPickupDropTime = flagPickupDropTime
              , or_psiStormCooldownUntil =
                  pcs_psiStormCooldownUntil $ ps_controlState playerStatus
              , or_dashStamina =
                  pcs_overseerDashStamina (ps_controlState playerStatus)
              , or_isInvulnerable = isInvulnerable time combatComp
              , or_entityID = getIndex overseerID
              , or_ticksSinceDash = Just ticksSinceDash
              , or_droneRespawnCooldowns = minionRespawnCooldownTimes
              }
      DroneID droneID -> do
        mActorID <- Core.lookup actorComps droneID
        combatComp <- Core.sureLookup combatComps droneID
        team <- Core.sureLookup teamComps droneID
        playerClass <- Core.sureLookup playerClassComps droneID
        mOverseerID <- lookupMaster masterMinionComps droneID
        maxDroneChargeTicks <-
          maybe 0 (time `diffTime`)
            <$> lookupMinionCreationTime masterMinionComps droneID
        let mPlayerAndStatus =
              mActorID <&> \actorID ->
                expectJust "renderingComponent: actorID not found" $
                  WIM.lookup actorID playersMap
            colorRep =
              fromMaybe (Left team) $ do
                _ <- mOverseerID -- Use generic team color if drone has no overseer
                (player, _) <- mPlayerAndStatus
                pure $
                  Right $
                    PlayerColor
                      (player ^. player_team)
                      (player ^. player_color)
                      (player ^. player_actorID)
            mLastDash =
              mPlayerAndStatus <&> \(_, playerStatus) ->
                let (castTime, sharedDroneChargeTicks') =
                      pcs_lastDroneDash (ps_controlState playerStatus)
                    ticksSinceDash = time `diffTime` castTime
                    droneChargeTicks =
                      min maxDroneChargeTicks sharedDroneChargeTicks'
                    droneChargeIntensity =
                      fromIntegral droneChargeTicks
                        / fromIntegral (gp_maxDroneDashChargeTicks (gp_cast params))
                in  (ticksSinceDash, droneChargeIntensity)
            chargeFraction =
              case mPlayerAndStatus of
                Nothing -> 0
                Just (_, playerStatus) ->
                  let sharedDroneChargeTicks' =
                        fromMaybe 0 $
                          sharedDroneChargeTicks
                            (gp_cast params)
                            time
                            (ps_controlState playerStatus)
                      droneChargeTicks =
                        min maxDroneChargeTicks sharedDroneChargeTicks'
                      droneChargeFraction =
                        fromIntegral droneChargeTicks
                          / fromIntegral (gp_maxDroneDashChargeTicks (gp_cast params))
                  in  droneChargeFraction
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_Drone $
            DroneRendering
              { dr_color = colorRep
              , dr_healthFraction = Fixed.toFloat $ healthFraction combatComp
              , dr_recentDamage = Fixed.toFloat $ recentDamage time combatComp
              , dr_actorID = mActorID
              , dr_prevPos1 = Fixed.toFloats $ dyn_pos_prev dynamicsComp
              , dr_prevPos2 = Fixed.toFloats $ dyn_pos_prev2 dynamicsComp
              , dr_prevPos3 = Fixed.toFloats $ dyn_pos_prev3 dynamicsComp
              , dr_prevPos4 = Fixed.toFloats $ dyn_pos_prev4 dynamicsComp
              , dr_prevPos5 = Fixed.toFloats $ dyn_pos_prev5 dynamicsComp
              , dr_prevPos6 = Fixed.toFloats $ dyn_pos_prev6 dynamicsComp
              , dr_prevPos7 = Fixed.toFloats $ dyn_pos_prev7 dynamicsComp
              , dr_prevPos8 = Fixed.toFloats $ dyn_pos_prev8 dynamicsComp
              , dr_prevPos9 = Fixed.toFloats $ dyn_pos_prev9 dynamicsComp
              , dr_prevPos10 = Fixed.toFloats $ dyn_pos_prev10 dynamicsComp
              , dr_isInvulnerable = isInvulnerable time combatComp
              , dr_entityID = getIndex droneID
              , dr_team = team
              , dr_class = playerClass
              , dr_ticksSinceDash = mLastDash
              , dr_chargeFraction = chargeFraction
              }
      SoftObstacleID _softObstacleID ->
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_SoftObstacle SoftObstacleRendering
      PsiStormID psiStormID -> do
        endTime <- Core.sureLookup transitoryComps psiStormID
        mActorID <- Core.lookup actorComps psiStormID
        let mPlayerColor = do
              actorID <- mActorID
              (player, _playerStatus) <- WIM.lookup actorID playersMap
              pure $
                PlayerColor
                  (player ^. player_team)
                  (player ^. player_color)
                  actorID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_PsiStorm $
            PsiStormRendering endTime mPlayerColor
      FlagID flagID -> do
        team <- Core.sureLookup teamComps flagID
        flagComp <- Core.sureLookup flagComps flagID
        pure $
          let flagParams = gp_flags params
              flagRecoveryTicks = gp_flagRecoveryTicks flagParams
              progressTicks =
                min flagRecoveryTicks $
                  accumulatedFlagTicks flagParams time flagComp
              progressFraction =
                fromIntegral progressTicks / fromIntegral flagRecoveryTicks
              flagRendering =
                defaultRenderingComponent dynamicsComp $
                  ER_Flag $
                    FlagRendering{fr_team = team}
              flagCaptureAreaRendering =
                RenderingComponent
                  { rendering_entityRendering =
                      ER_FlagCaptureArea $
                        FlagCaptureAreaRendering
                          { fcar_team = team
                          , fcar_progress = progressFraction
                          , fcar_recoveringFlag =
                              flagConditionStatus time flagComp
                          }
                  , rendering_pos = Fixed.toFloats $ dyn_pos dynamicsComp
                  , rendering_radius =
                      Fixed.toFloat $
                        gp_flagRecoveryRadius $
                          gp_dynamics params
                  }
          in  [flagRendering, flagCaptureAreaRendering]
      TeamBaseID teamBaseID -> do
        flagComp <- Core.sureLookup flagComps teamBaseID
        team <- Core.sureLookup teamComps teamBaseID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_TeamBase $
            TeamBaseRendering
              { tbr_team = team
              , tbr_flagDepositPending = flagConditionStatus time flagComp
              }
      BaseFlagID baseFlagID ->
        Core.sureLookup teamComps baseFlagID <&> \team ->
          List.singleton . defaultRenderingComponent dynamicsComp $
            ER_BaseFlag (BaseFlagRendering team)
      ExplosionID _explosionID ->
        pure $ List.singleton $ defaultRenderingComponent dynamicsComp ER_NoVisual
      SpawnAreaID spawnAreaID -> do
        team <- Core.sureLookup teamComps spawnAreaID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_SpawnArea (SpawnAreaRendering team)
      PowerupID powerupID -> do
        expiryTimeMaybe <- Core.lookup transitoryComps powerupID
        pure . List.singleton . defaultRenderingComponent dynamicsComp $
          ER_ShieldPowerup ShieldPowerupRendering{spr_expiryTime = expiryTimeMaybe}

defaultRenderingComponent :: DynamicsComponent -> a -> RenderingComponent a
defaultRenderingComponent dynamicsComp a =
  RenderingComponent
    { rendering_entityRendering = a
    , rendering_pos = Fixed.toFloats $ dyn_pos dynamicsComp
    , rendering_radius = Fixed.toFloat $ dyn_radius dynamicsComp
    }
