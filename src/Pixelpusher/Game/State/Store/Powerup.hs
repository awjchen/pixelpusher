{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.Powerup (
  PowerupStore,
  newPowerupStore,
  PowerupReadStore,
  readPowerupStore,
  readPowerupLocation,
  readPowerupHealth,
  consumePowerup,
  mapPowerupSpawnLocations,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.PowerupComponent
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

type PowerupStore' s =
  DenseStore
    s
    (EntityID Powerup)
    (EntityID Powerup)
    PowerupComponentVec

-- | Stores the team to which entity belongs
newtype PowerupStore s = PowerupStore (PowerupStore' s)
  deriving newtype
    ( ReadStore s (EntityID Powerup) PowerupComponent
    , Store s (EntityID Powerup) PowerupComponent
    , Mutable
        s
        (DenseStoreSnapshot (EntityID Powerup) PowerupComponentVec)
    , SureReadStore s (EntityID Powerup) PowerupComponent
    , ReadPowerup s
    )

instance Copyable s (PowerupStore s) where
  copy (PowerupStore target) (PowerupStore source) =
    DS.copyDenseStore target source

newPowerupStore :: ST s (PowerupStore s)
newPowerupStore = PowerupStore <$> DS.newDenseStore C.maxNumEntities

newtype PowerupReadStore s = PowerupReadStore (PowerupStore s)
  deriving newtype
    ( ReadStore s (EntityID Powerup) PowerupComponent
    , SureReadStore s (EntityID Powerup) PowerupComponent
    , ReadPowerup s
    )

readPowerupStore :: PowerupStore s -> PowerupReadStore s
readPowerupStore = PowerupReadStore

--------------------------------------------------------------------------------
-- Query

class ReadPowerup s store where
  readPowerupLocation ::
    store -> EntityID Powerup -> ST s (Maybe MapPowerupLocation)
  readPowerupHealth :: store -> EntityID Powerup -> ST s Fixed

instance ReadPowerup s (PowerupStore' s) where
  readPowerupLocation store = DS.sureLookup_1 store vec_pc_location
  readPowerupHealth store = DS.sureLookup_1 store vec_pc_shieldHealth
  {-# INLINE readPowerupLocation #-}
  {-# INLINE readPowerupHealth #-}

--------------------------------------------------------------------------------

-- | Returns whether the powerup was already consumed
consumePowerup :: PowerupStore s -> EntityID Powerup -> ST s (Maybe Fixed)
consumePowerup (PowerupStore store) powerupID =
  DS.sureModifyReturn_1_1
    store
    vec_pc_shieldHealth
    vec_pc_consumed
    powerupID
    ( \shieldHealth isConsumed ->
        let result = if isConsumed then Nothing else Just shieldHealth
        in  (result, True)
    )
