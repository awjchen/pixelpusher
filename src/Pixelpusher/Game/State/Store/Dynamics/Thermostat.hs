{-# LANGUAGE ViewPatterns #-}

-- | Regulation of soft obstacles
module Pixelpusher.Game.State.Store.Dynamics.Thermostat (
  driftingSoftObstacleThermostat,
) where

import Prelude hiding (lookup)

import Control.Monad (when)
import Control.Monad.ST
import Data.Foldable (for_)
import Data.List (foldl', tails)

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.State.Store.Dynamics.Types (DynamicsStore')

--------------------------------------------------------------------------------

driftingSoftObstacleThermostat ::
  DynamicsParams -> Time -> DynamicsStore' s -> ST s ()
driftingSoftObstacleThermostat params time dynComps =
  when (time `atMultipleOf` Ticks 61) $
    driftingSoftObstacleThermostat' params dynComps

-- | Regulate the total energy of the soft obstacles, and compensate for (what
-- I think is) fixed-point drift caused by always rounding down after
-- multiplications.
driftingSoftObstacleThermostat' :: DynamicsParams -> DynamicsStore' s -> ST s ()
driftingSoftObstacleThermostat' params dynComps = do
  let isSoftObstacle dynID =
        case refineEntityID dynID of
          SoftObstacleID _ -> True
          _ -> False
  (eids, positions, velocities) <-
    unzip3
      <$> DS.toListMaybe_idx_2
        dynComps
        vec_dyn_pos
        vec_dyn_vel
        ( \eid pos vel ->
            if isSoftObstacle eid then Just (eid, pos, vel) else Nothing
        )

  let nObstacles = length positions
  when (nObstacles > 0) $ do
    let nObstacles' = fromIntegral nObstacles

    let kineticEnergy = (* 0.5) $ foldl' (+) 0 $ map Fixed.unsafeNormSq velocities
        pairwisePositions = concatMap headZip $ tails positions
        potentialEnergy =
          (* (gp_softObstacleReplusion params * 0.5)) $
            foldl' (+) 0 $
              map (sq . (/ collisionRadius) . subtract collisionRadius . Fixed.norm) $
                filter (`Fixed.normLeq` collisionRadius) $
                  map (uncurry subtract) pairwisePositions
        totalEnergy = kineticEnergy + potentialEnergy
        targetAvgEnergy = (* 0.5) $ sq $ gp_softObstacleAverageSpeed params
        targetTotalEnergy = targetAvgEnergy * nObstacles'
        velScale =
          if totalEnergy == 0
            then 1
            else sqrt $ targetTotalEnergy / totalEnergy

    let avgPositionFrac =
          Fixed.map (/ (nObstacles' * gp_worldRadius params)) $
            foldl' (+) 0 positions
        restoringAccel = Fixed.map (* (-0.015625)) avgPositionFrac
        avgVelocity = Fixed.map (/ nObstacles') $ foldl' (+) 0 velocities
        dampingAccel = Fixed.map (* (-0.015625)) avgVelocity
        applyCenteringAccel = (+) (restoringAccel + dampingAccel)

    -- Artificially limit speed (to prevent the movement soft obstacles from
    -- becoming too relevant in combat)
    for_ eids $ \eid -> do
      DS.sureModify_1_1 dynComps vec_dyn_pos vec_dyn_vel eid $
        \pos (applyCenteringAccel -> vel) ->
          let worldPotentialDeltaV = worldGradient params pos
              maxSpeed = 0.05
              scale' =
                if Fixed.normLeq vel maxSpeed then velScale else velScale * 0.95
          in  Fixed.map (* scale') (vel + worldPotentialDeltaV)
  where
    collisionRadius = 2 * gp_softObstacleRadius params

    headZip :: [a] -> [(a, a)]
    headZip [] = []
    headZip (x : xs) = map (x,) xs

    sq x = x * x

-- | Push soft obstacles towards the center of the world, since they are
-- usually pushed out towards the world boundary by overseers and drones.
worldGradient :: DynamicsParams -> Fixed2 -> Fixed2
worldGradient params pos =
  let (dist, dir) = Fixed.normAndNormalize pos
      distFrac = dist / gp_worldRadius params
      magnitude = 6.25e-3 * distFrac * gp_softObstacleAverageSpeed params
  in  Fixed.map (* magnitude) (-dir)
