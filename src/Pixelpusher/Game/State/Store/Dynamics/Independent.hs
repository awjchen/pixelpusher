-- | Independent movement of entities
module Pixelpusher.Game.State.Store.Dynamics.Independent (
  stepIndependentKinematics,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Data.Maybe (fromMaybe)
import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Dynamics.Controls (
  DynamicsControls (..),
  zeroAccelerationControls,
 )
import Pixelpusher.Game.MapPowerups (mapPowerupSpawnLocation)
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core qualified as Store (
  ReadOnly,
  lookup,
  sureLookup,
 )

import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Minion
import Pixelpusher.Game.State.Store.Powerup

import Pixelpusher.Game.State.Store.Dynamics.Types (
  flipPositiveVelocity,
  projected,
  repulsionImpulse,
 )

--------------------------------------------------------------------------------

-- ** Independent dynamics

-- | Run movements of entities that depend only the entity itself.
stepIndependentKinematics ::
  DynamicsParams ->
  WIM.IntMap ActorID (DynamicsControls, PlayerClass) ->
  Store.ReadOnly (ActorEntityStore s) ->
  PowerupReadStore s ->
  MinionStore s ->
  SEntityID DynamicsEntity ->
  Fixed ->
  Fixed2 ->
  Fixed2 ->
  ST s (Pair Fixed2 Fixed2)
stepIndependentKinematics
  params
  playerCtrlsClass
  actorComps
  powerupComps
  masterMinionComps
  dynID
  radius
  pos
  vel = case refineEntityID dynID of
    OverseerID overseerID -> do
      (dynControls, playerClass) <-
        fromMaybe (zeroAccelerationControls, defaultPlayerClass)
          . (`WIM.lookup` playerCtrlsClass)
          <$> Store.sureLookup actorComps overseerID
      pure $!
        applyWorldBounds' $
          standardVelocityIntegration $
            overseerVelocityControl params playerClass dynControls
    DroneID droneID -> do
      (dynCtrls, playerClass) <-
        lookupMaster masterMinionComps droneID >>= \case
          Nothing -> pure (zeroAccelerationControls, defaultPlayerClass)
          Just _ ->
            fromMaybe (zeroAccelerationControls, defaultPlayerClass)
              . ((`WIM.lookup` playerCtrlsClass) =<<)
              <$> Store.lookup actorComps droneID
      pure $!
        applyWorldBounds' $
          standardVelocityIntegration $
            droneVelocityControl params playerClass dynCtrls pos
    SoftObstacleID _ ->
      pure $! applyWorldBounds' $ standardVelocityIntegration id
    FlagID _ ->
      pure $!
        applyWorldBounds' $
          standardVelocityIntegration flagVelocityControl
    TeamBaseID _ -> fixed
    PsiStormID _ -> fixed
    BaseFlagID _ -> fixed
    ExplosionID _ -> fixed
    SpawnAreaID _ -> fixed
    PowerupID powerupID -> do
      powerupLocMaybe <- readPowerupLocation powerupComps powerupID
      let velocityControl =
            case powerupLocMaybe of
              Nothing -> flagVelocityControl
              Just powerupLoc ->
                tetheredPowerupVelocityControl
                  (mapPowerupSpawnLocation params powerupLoc)
                  pos
      pure $! applyWorldBounds' $ standardVelocityIntegration velocityControl
    where
      -- Use the given velocity update function to obtain a new position and
      -- velocity from the current position and velocity.
      standardVelocityIntegration :: (Fixed2 -> Fixed2) -> Pair Fixed2 Fixed2
      standardVelocityIntegration f_vel =
        let vel' = f_vel vel
            pos' = pos + Fixed.map (* 0.5) (vel + vel')
        in  pos' :!: vel'

      applyWorldBounds' :: Pair Fixed2 Fixed2 -> Pair Fixed2 Fixed2
      applyWorldBounds' (pos' :!: vel') =
        (:!:) pos' $ applyWorldBounds (gp_worldRadius params) radius pos' vel'

      fixed :: (Applicative f) => f (Pair Fixed2 Fixed2)
      fixed = pure $! pos :!: 0

-- | Accelerate in the direction specified by the arrow keys. Apply drag.
overseerVelocityControl ::
  DynamicsParams -> PlayerClass -> DynamicsControls -> Fixed2 -> Fixed2
overseerVelocityControl params playerClass dynCtrls =
  (+ accel) . applyLinearDrag
  where
    accelMagnitude = getClassParam playerClass (gp_overseerAcceleration params)
    accel = Fixed.map (* accelMagnitude) $ dynCtrl_arrowKeyDir dynCtrls
    overseerDrag = getClassParam playerClass (gp_overseerDrag params)
    applyLinearDrag = Fixed.map (* (1 - overseerDrag))

-- | Accelerate towards/away from the position of the mouse cursor. Apply drag.
droneVelocityControl ::
  DynamicsParams -> PlayerClass -> DynamicsControls -> Fixed2 -> Fixed2 -> Fixed2
droneVelocityControl params playerClass dynCtrls position =
  (+ accel) . applyLinearDrag
  where
    accel =
      Fixed.map (* accelSize) $
        Fixed.normalize $
          dynCtrl_mousePos dynCtrls - position
    accelSize =
      dynCtrl_droneAccelScale dynCtrls
        * getClassParam playerClass (gp_droneAcceleration params)
    droneDrag = getClassParam playerClass (gp_droneDrag params)
    applyLinearDrag = Fixed.map (* (1 - droneDrag))

-- | Accelerate towards the spawn position. Apply drag.
tetheredPowerupVelocityControl :: Fixed2 -> Fixed2 -> Fixed2 -> Fixed2
tetheredPowerupVelocityControl spawnPosition position =
  (+ accel) . applyLinearDrag
  where
    accelDir = Fixed.normalize $ spawnPosition - position
    accelSize = 0.004
    accel = Fixed.map (* accelSize) accelDir
    drag = 0.1
    applyLinearDrag = Fixed.map (* (1 - drag))

-- | Apply strong drag
flagVelocityControl :: Fixed2 -> Fixed2
flagVelocityControl = Fixed.map (* (1 - 0.1))

-- World boundary forces

-- | The world is a disc centered at the origin.
applyWorldBounds :: Fixed -> Fixed -> Fixed2 -> Fixed2 -> Fixed2
applyWorldBounds worldRadius entityRadius pos vel
  | Fixed.normLeq pos (worldRadius - entityRadius) = vel
  | otherwise =
      let r = Fixed.norm pos
      in  vel & projected pos %~ applyWorldBounds1D worldRadius entityRadius r

applyWorldBounds1D :: Fixed -> Fixed -> Fixed -> Fixed -> Fixed
applyWorldBounds1D worldRadius entityRadius pos
  | overlap > 0 = repulsionImpulse overlap . flipPositiveVelocity
  | otherwise = id
  where
    overlap = pos + entityRadius - worldRadius
