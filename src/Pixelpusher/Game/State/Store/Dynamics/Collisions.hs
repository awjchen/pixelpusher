{-# LANGUAGE MultiWayIf #-}

-- | Detection and handling of collisions between entities
module Pixelpusher.Game.State.Store.Dynamics.Collisions (
  runCollisions,
  softObstacleReplusion,
) where

import Prelude hiding (lookup)

import Control.Monad.ST
import Data.Bifunctor (bimap, first)
import Data.Generics.Product.Fields
import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Util (mapMaybeM)
import Pixelpusher.Game.ActorID (ActorID (..))
import Pixelpusher.Game.BotID
import Pixelpusher.Game.Bots.ControlStagger (shouldControlBot)
import Pixelpusher.Game.CollisionEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Dynamics.Collisions.Entities
import Pixelpusher.Game.Dynamics.Collisions.SortAndSweep (
  SortAndSweepComponent (..),
  sortAndSweep,
 )
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID

import {-# SOURCE #-} Pixelpusher.Game.State.Core qualified as Store (
  ReadOnly,
  lookup,
  sureLookup,
 )

import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Combat
import Pixelpusher.Game.State.Store.PsiStorm
import Pixelpusher.Game.State.Store.Team

import Pixelpusher.Game.State.Store.Dynamics.Types (
  DynamicsStore',
  flipPositiveVelocity,
  projected,
  repulsionImpulse,
 )

--------------------------------------------------------------------------------

-- ** Collisions

-- Note: The current collision detection naively considers only positions and
-- not velocities.

runCollisions ::
  forall s.
  GameParams ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  Store.ReadOnly (PsiStormStore s) ->
  CombatReadStore s ->
  Time ->
  DynamicsStore' s ->
  ST s [CollisionEvent]
runCollisions params teamStore playerStore psiStormStore combatStore time dynComps =
  detectCollisions' >>= mapMaybeM handleCollision'
  where
    detectCollisions' =
      sortAndSweep (gp_worldRadius (gp_dynamics params))
        <$> DS.foldM_idx_2
          dynComps
          vec_dyn_radius
          vec_dyn_pos
          (collectCollisionEntities params time playerStore psiStormStore)
          []
    handleCollision' =
      handleCollision
        (gp_dynamics params)
        dynComps
        teamStore
        playerStore
        combatStore

collectCollisionEntities ::
  GameParams ->
  Time ->
  Store.ReadOnly (ActorEntityStore s) ->
  Store.ReadOnly (PsiStormStore s) ->
  [SortAndSweepComponent] ->
  SEntityID DynamicsEntity ->
  Fixed ->
  Fixed2 ->
  ST s [SortAndSweepComponent]
collectCollisionEntities params time actorEntityStore psiStormStore out dynId radius pos =
  case refineEntityID dynId of
    OverseerID overseerID ->
      Store.sureLookup actorEntityStore overseerID <&> \case
        PlayerActor _ -> standardComp : out
        BotActor botID ->
          if shouldControlBot time botID
            then
              let botViewComp =
                    SortAndSweepComponent
                      (CE_BotViewArea botID)
                      C.botViewRadius
                      pos
              in  standardComp : botViewComp : out
            else standardComp : out
    PsiStormID psiStormID ->
      Store.sureLookup psiStormStore psiStormID <&> \castTime ->
        let hitPeriodTicks =
              view
                (field @"gp_psiStormHitPeriodTicks") -- avoiding ambiguity occurence error
                (gp_dynamics params)
            ticksSinceCast = time `diffTime` castTime
            psiStormDamage =
              SortAndSweepComponent
                (CE_PsiStormDamage psiStormID)
                radius
                pos
        in  if ticksSinceCast `mod` hitPeriodTicks == 0
              then standardComp : psiStormDamage : out
              else standardComp : out
    SpawnAreaID _ ->
      pure $
        if time `atMultipleOf` C.spawnInvulnerabilityCheckPeriod
          then standardComp : out
          else out
    FlagID flagID ->
      pure $
        let flagAreaComp =
              SortAndSweepComponent
                (CE_FlagArea flagID)
                (gp_flagRecoveryRadius (gp_dynamics params))
                pos
        in  standardComp : flagAreaComp : out
    _ ->
      pure $ standardComp : out
  where
    standardComp = SortAndSweepComponent (CE_DynEntity dynId) radius pos

handleCollision ::
  forall s.
  DynamicsParams ->
  DynamicsStore' s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  CombatReadStore s ->
  CollisionEntityPair ->
  ST s (Maybe CollisionEvent)
handleCollision params dynComps teamStore playerStore combatStore collision = do
  let CollisionEntityPair entity1 entity2 = collision
  case entity1 of
    CE_DynEntity dynID1 ->
      case entity2 of
        CE_DynEntity dynID2 ->
          case refineEntityID dynID1 of
            OverseerID overseerID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runHardCollision' (subEntityID overseerID1) (subEntityID overseerID2)
              DroneID droneID2 -> runHardCollision' (subEntityID overseerID1) (subEntityID droneID2)
              SoftObstacleID softObstacleID2 -> runSoftObstacleRepulsion' softObstacleID2 (subEntityID overseerID1)
              PsiStormID _ -> pure Nothing
              FlagID flagID2 -> flagPair (subEntityID overseerID1) (subEntityID flagID2)
              TeamBaseID teamBaseID2 -> flagPair (subEntityID overseerID1) (subEntityID teamBaseID2)
              BaseFlagID baseFlagID2 -> flagPair (subEntityID overseerID1) (subEntityID baseFlagID2)
              ExplosionID explosionID2 -> runExplosionRepulsion' explosionID2 (subEntityID overseerID1)
              SpawnAreaID spawnAreaID2 -> runSpawnOverlap spawnAreaID2 overseerID1
              PowerupID powerupID2 -> runPowerupGet powerupID2 overseerID1
            DroneID droneID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runHardCollision' (subEntityID droneID1) (subEntityID overseerID2)
              DroneID droneID2 -> runHardCollision' (subEntityID droneID1) (subEntityID droneID2)
              SoftObstacleID softObstacleID2 -> runSoftObstacleRepulsion' softObstacleID2 (subEntityID droneID1)
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID explosionID2 -> runExplosionRepulsion' explosionID2 (subEntityID droneID1)
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            SoftObstacleID softObstacleID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runSoftObstacleRepulsion' softObstacleID1 (subEntityID overseerID2)
              DroneID droneID2 -> runSoftObstacleRepulsion' softObstacleID1 (subEntityID droneID2)
              SoftObstacleID softObstacleID2 -> runSoftObstacleCollision' softObstacleID1 softObstacleID2
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID teamBaseID2 -> runTeamBaseRepulsion (subEntityID teamBaseID2) softObstacleID1
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID spawnAreaID2 -> runTeamBaseRepulsion (subEntityID spawnAreaID2) softObstacleID1
              PowerupID powerupID2 -> runSoftObstacleRepulsion' softObstacleID1 (subEntityID powerupID2)
            PsiStormID _ ->
              pure Nothing
            FlagID flagID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> flagPair (subEntityID overseerID2) (subEntityID flagID1)
              DroneID _ -> pure Nothing
              SoftObstacleID _ -> pure Nothing
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            TeamBaseID teamBaseID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> flagPair (subEntityID overseerID2) (subEntityID teamBaseID1)
              DroneID _ -> pure Nothing
              SoftObstacleID softObstacleID2 -> runTeamBaseRepulsion (subEntityID teamBaseID1) softObstacleID2
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            BaseFlagID baseFlagID -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> flagPair (subEntityID overseerID2) (subEntityID baseFlagID)
              DroneID _ -> pure Nothing
              SoftObstacleID _ -> pure Nothing
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            ExplosionID explosionID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runExplosionRepulsion' explosionID1 (subEntityID overseerID2)
              DroneID droneID2 -> runExplosionRepulsion' explosionID1 (subEntityID droneID2)
              SoftObstacleID _ -> pure Nothing
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            SpawnAreaID spawnAreaID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runSpawnOverlap spawnAreaID1 overseerID2
              DroneID _ -> pure Nothing
              SoftObstacleID softObstacleID2 -> runTeamBaseRepulsion (subEntityID spawnAreaID1) softObstacleID2
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
            PowerupID powerupID1 -> case refineEntityID dynID2 of
              OverseerID overseerID2 -> runPowerupGet powerupID1 overseerID2
              DroneID _ -> pure Nothing
              SoftObstacleID softObstacleID2 -> runSoftObstacleRepulsion' softObstacleID2 (subEntityID powerupID1)
              PsiStormID _ -> pure Nothing
              FlagID _ -> pure Nothing
              TeamBaseID _ -> pure Nothing
              BaseFlagID _ -> pure Nothing
              ExplosionID _ -> pure Nothing
              SpawnAreaID _ -> pure Nothing
              PowerupID _ -> pure Nothing
        CE_FlagArea flagID2 -> case refineEntityID dynID1 of
          OverseerID overseerID1 -> flagArea flagID2 overseerID1
          DroneID _ -> pure Nothing
          SoftObstacleID softObstacleID1 -> Nothing <$ runSoftObstacleRepulsion' softObstacleID1 (subEntityID flagID2)
          PsiStormID _ -> pure Nothing
          FlagID _ -> pure Nothing
          TeamBaseID _ -> pure Nothing
          BaseFlagID _ -> pure Nothing
          ExplosionID _ -> pure Nothing
          SpawnAreaID _ -> pure Nothing
          PowerupID _ -> pure Nothing
        CE_BotViewArea botID2 -> botView botID2 dynID1
        CE_PsiStormDamage psiStormID2 -> case refineEntityID dynID1 of
          OverseerID overseerID1 -> psiStormOverlap' psiStormID2 (subEntityID overseerID1)
          DroneID droneID1 -> psiStormOverlap' psiStormID2 (subEntityID droneID1)
          SoftObstacleID _ -> pure Nothing
          PsiStormID _ -> pure Nothing
          FlagID _ -> pure Nothing
          TeamBaseID _ -> pure Nothing
          BaseFlagID _ -> pure Nothing
          ExplosionID _ -> pure Nothing
          SpawnAreaID _ -> pure Nothing
          PowerupID _ -> pure Nothing
    CE_FlagArea flagID1 ->
      case entity2 of
        CE_DynEntity dynID2 -> case refineEntityID dynID2 of
          OverseerID overseerID2 -> flagArea flagID1 overseerID2
          DroneID _ -> pure Nothing
          SoftObstacleID softObstacleID2 -> Nothing <$ runSoftObstacleRepulsion' softObstacleID2 (subEntityID flagID1)
          PsiStormID _ -> pure Nothing
          FlagID _ -> pure Nothing
          TeamBaseID _ -> pure Nothing
          BaseFlagID _ -> pure Nothing
          ExplosionID _ -> pure Nothing
          SpawnAreaID _ -> pure Nothing
          PowerupID _ -> pure Nothing
        CE_FlagArea _ -> pure Nothing
        CE_BotViewArea _ -> pure Nothing
        CE_PsiStormDamage _ -> pure Nothing
    CE_BotViewArea botID1 ->
      case entity2 of
        CE_DynEntity dynID2 -> botView botID1 dynID2
        CE_FlagArea _ -> pure Nothing
        CE_BotViewArea _ -> pure Nothing
        CE_PsiStormDamage _ -> pure Nothing
    CE_PsiStormDamage psiStormID1 ->
      case entity2 of
        CE_DynEntity dynID2 -> case refineEntityID dynID2 of
          OverseerID overseerID2 -> psiStormOverlap' psiStormID1 (subEntityID overseerID2)
          DroneID droneID2 -> psiStormOverlap' psiStormID1 (subEntityID droneID2)
          SoftObstacleID _ -> pure Nothing
          PsiStormID _ -> pure Nothing
          FlagID _ -> pure Nothing
          TeamBaseID _ -> pure Nothing
          BaseFlagID _ -> pure Nothing
          ExplosionID _ -> pure Nothing
          SpawnAreaID _ -> pure Nothing
          PowerupID _ -> pure Nothing
        CE_FlagArea _ -> pure Nothing
        CE_BotViewArea _ -> pure Nothing
        CE_PsiStormDamage _ -> pure Nothing
  where
    runHardCollision' ::
      SEntityID CombatEntity ->
      SEntityID CombatEntity ->
      ST s (Maybe CollisionEvent)
    runHardCollision' =
      runHardCollision params dynComps teamStore playerStore combatStore

    runSoftObstacleRepulsion' ::
      EntityID SoftObstacle ->
      SEntityID DynamicsEntity ->
      ST s (Maybe CollisionEvent)
    runSoftObstacleRepulsion' obstacleID dynID =
      Just . CE_ObstacleOverlap
        <$> runSoftObstacleRepulsion params dynComps obstacleID dynID

    runSoftObstacleCollision' ::
      EntityID SoftObstacle ->
      EntityID SoftObstacle ->
      ST s (Maybe CollisionEvent)
    runSoftObstacleCollision' obstacleID1 obstacleID2 =
      Nothing <$ runSoftObstacleCollision params dynComps obstacleID1 obstacleID2

    runTeamBaseRepulsion ::
      SEntityID DynamicsEntity ->
      EntityID SoftObstacle ->
      ST s (Maybe CollisionEvent)
    runTeamBaseRepulsion obstacleID softObstacleID =
      Nothing
        <$ teamBaseRepulsion dynComps obstacleID softObstacleID

    psiStormOverlap' ::
      EntityID PsiStorm ->
      SEntityID CombatEntity ->
      ST s (Maybe CollisionEvent)
    psiStormOverlap' psiStormID entityCombatID = do
      mEntityActorID <- Store.lookup playerStore entityCombatID
      mPsiStormActorID <- Store.lookup playerStore psiStormID
      (pos, vel) <-
        DS.sureLookup_2 dynComps vec_dyn_pos vec_dyn_vel entityCombatID
      mOverlap <- psiStormOverlap dynComps psiStormID entityCombatID
      pure $
        flip fmap mOverlap $ \(eid, overlap) ->
          CE_PsiStorm $
            PsiStormOverlap
              { pso_entityID = eid
              , pso_psiStormEntityID = psiStormID
              , pso_mEntityActorID = mEntityActorID
              , pso_overlap = overlap
              , pso_entityPos = pos
              , pso_entityVel = vel
              , pso_mPsiStormActorID = mPsiStormActorID
              }

    flagPair ::
      SEntityID FlagEntity ->
      SEntityID FlagEntity ->
      ST s (Maybe CollisionEvent)
    flagPair flagID1 flagID2 =
      pure $ Just $ CE_Flag $ FlagOverlap flagID1 flagID2

    flagArea ::
      EntityID Flag ->
      EntityID Overseer ->
      ST s (Maybe CollisionEvent)
    flagArea flagID overseerID =
      pure $ Just $ CE_Flag $ FlagAreaOverlap flagID overseerID

    runExplosionRepulsion' ::
      EntityID Explosion ->
      SEntityID DynamicsEntity ->
      ST s (Maybe CollisionEvent)
    runExplosionRepulsion' explosionID dynID =
      Nothing <$ runExplosionRepulsion params dynComps explosionID dynID

    runSpawnOverlap ::
      EntityID SpawnArea -> EntityID Overseer -> ST s (Maybe CollisionEvent)
    runSpawnOverlap spawnAreaID overseerID = do
      sameTeam <-
        (==)
          <$> Store.sureLookup teamStore spawnAreaID
          <*> Store.sureLookup teamStore overseerID
      pure $
        if sameTeam
          then Just $ CE_Spawn $ SpawnOverlap overseerID
          else Nothing

    runPowerupGet ::
      EntityID Powerup -> EntityID Overseer -> ST s (Maybe CollisionEvent)
    runPowerupGet powerupID overseerID =
      pure $! Just $! CE_PowerupGet $! PowerupGet overseerID powerupID

    botView :: BotID -> SEntityID DynamicsEntity -> ST s (Maybe CollisionEvent)
    botView botID dynID =
      pure $ Just $ CE_BotView $ BotViewCollisionEvent botID dynID

--------------------------------------------------------------------------------

-- *** Soft obstacle collisions

runSoftObstacleCollision ::
  DynamicsParams ->
  DynamicsStore' s ->
  EntityID SoftObstacle ->
  EntityID SoftObstacle ->
  ST s ()
runSoftObstacleCollision params dynComps eid1 eid2 =
  DS.sureModifyPair_2_1
    dynComps
    vec_dyn_radius
    vec_dyn_pos
    vec_dyn_vel
    eid1
    eid2
    (softObstacleCollision params)

-- | Compute the new velocities of soft obstacles resulting from their mutual
-- repulsion.
softObstacleCollision ::
  DynamicsParams ->
  (Fixed, Fixed2, Fixed2) ->
  (Fixed, Fixed2, Fixed2) ->
  (Fixed2, Fixed2)
softObstacleCollision params (r1, p1, v1) (r2, p2, v2) =
  let dp = p2 - p1
      (r, direction) = Fixed.normAndNormalize dp
      collisionRadius = r1 + r2
      forceMag =
        gp_softObstacleReplusion params * ((collisionRadius - r) / 240)
      force = Fixed.map (* forceMag) direction
  in  (v1 - force, v2 + force)

--------------------------------------------------------------------------------

-- *** Soft obstacle repulsion

runSoftObstacleRepulsion ::
  DynamicsParams ->
  DynamicsStore' s ->
  EntityID SoftObstacle ->
  SEntityID DynamicsEntity ->
  ST s ObstacleOverlap
runSoftObstacleRepulsion params dynComps obstacleID dynID = do
  let lookupPosRadiusMass =
        DS.sureLookup_3
          dynComps
          vec_dyn_pos
          vec_dyn_radius
          vec_dyn_mass
  (obstaclePos, obstacleRadius, obstacleMass) <-
    lookupPosRadiusMass (subEntityID obstacleID)
  (entityPos, entityRadius, entityMass) <-
    lookupPosRadiusMass dynID
  let relPos = entityPos - obstaclePos
      (dist, dir) = Fixed.normAndNormalize relPos
      -- Why is `accelFraction` called a fraction??
      accelFraction =
        softObstacleReplusion params obstacleRadius entityRadius dist
  -- Could avoid square roots here if we had a variation of `projected` that
  -- assumed a unit vector
  DS.sureModify_1 dynComps vec_dyn_vel dynID $
    projected relPos +~ accelFraction
  DS.sureModify_1 dynComps vec_dyn_vel obstacleID $
    projected (-relPos) +~ accelFraction * (entityMass / obstacleMass)
  pure
    ObstacleOverlap
      { oo_entityPosition = entityPos
      , oo_entityRadius = entityRadius
      , oo_entityMass = entityMass
      , oo_repulsionDirection = dir
      , oo_repulsionAccel = accelFraction
      }

-- The magnitude of the per-frame change in velocity of an entity repulsed by a
-- soft obstacle.
softObstacleReplusion :: DynamicsParams -> Fixed -> Fixed -> Fixed -> Fixed
softObstacleReplusion params obstacleRadius entityRadius dist
  | dist < obstacleRadius - entityRadius =
      -- The entity is completely covered by the obstacle
      slope * (obstacleRadius - dist)
  | dist < obstacleRadius + entityRadius =
      -- The entity is partially covered by the obstacle
      (slope *) $
        Fixed.sqDiv (obstacleRadius + entityRadius - dist) (4 * entityRadius)
  | otherwise =
      0
  where
    slope = gp_softObstacleCurvature params

--------------------------------------------------------------------------------

-- *** Team base repulsion of soft obstacles

teamBaseRepulsion ::
  DynamicsStore' s ->
  SEntityID DynamicsEntity ->
  EntityID SoftObstacle ->
  ST s ()
teamBaseRepulsion dynComps dynID obstacleID = do
  (basePos, baseRadius) <-
    DS.sureLookup_2 dynComps vec_dyn_pos vec_dyn_radius dynID
  DS.modify dynComps obstacleID $ hardObstacleRepulsion basePos baseRadius

-- | Apply the change in velocity caused by the force field of a soft obstacle.
hardObstacleRepulsion ::
  Fixed2 ->
  Fixed ->
  DynamicsComponent ->
  DynamicsComponent
hardObstacleRepulsion obstacleCenter obstacleRadius dynComp =
  dynComp
    & field @"dyn_vel"
    . projected relPos
    %~ negate
    . repulsionImpulse overlap
    . flipPositiveVelocity
    . negate
  where
    relPos = dyn_pos dynComp - obstacleCenter
    dist = Fixed.norm relPos
    entityRadius = dyn_radius dynComp
    overlap = obstacleRadius + entityRadius - dist

--------------------------------------------------------------------------------

-- *** Hard collisions

runHardCollision ::
  forall s.
  DynamicsParams ->
  DynamicsStore' s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  CombatReadStore s ->
  SEntityID CombatEntity ->
  SEntityID CombatEntity ->
  ST s (Maybe CollisionEvent)
runHardCollision params dynComps teamStore playerStore combatStore combatID1 combatID2 = do
  teamBasedHardCollision params teamStore playerStore combatID1 combatID2
    >>= \case
      NoInteraction -> pure Nothing
      FriendlyInteraction -> do
        let collisionParticipants =
              case (refineEntityID combatID1, refineEntityID combatID2) of
                (OverseerID _, OverseerID _) -> CollisionOverseers
                (DroneID _, DroneID _) -> CollisionDrones
                _ -> CollisionOverseerDrone

        Just . CE_NeutralCollision . ($ collisionParticipants)
          <$> DS.sureModifyPairReturn
            dynComps
            combatID1
            combatID2
            (curry (friendlyCollision (gp_damageEnergyRatio params)))
      AdversarialInteraction -> do
        -- Using `iSubset` here might be overly opportunistic, since it may not
        -- always be the case that combat entities have ActorIDs
        mPid1 <- Store.lookup playerStore combatID1
        mPid2 <- Store.lookup playerStore combatID2

        -- Compute limits for collision based on the health of the weakest
        -- participant
        maxDamage <-
          min
            <$> getHealth combatStore combatID1
            <*> getHealth combatStore combatID2
        let dmgFactor = combatDamageFactor params (combatID1, combatID2)
            maxEnergyLoss =
              max 0 maxDamage * gp_energyDamageRatio params / dmgFactor

        let makeCollision f =
              entityIDPair combatID1 combatID2
                :!: updateCollisionDamage (* dmgFactor) (f mPid1 mPid2)
        Just . CE_Collision . makeCollision
          <$> DS.sureModifyPairReturn
            dynComps
            combatID1
            combatID2
            (curry (adversarialCollision params maxEnergyLoss))

-- | Damage scaling factors for specific entity type pairs.
combatDamageFactor ::
  DynamicsParams -> (SEntityID CombatEntity, SEntityID CombatEntity) -> Fixed
combatDamageFactor params (combatID1, combatID2) =
  case (refineEntityID combatID1, refineEntityID combatID2) of
    --
    (DroneID _, DroneID _) ->
      gp_droneDroneCollisionDamageFactor params
    --
    (DroneID _, OverseerID _) ->
      gp_overseerDroneCollisionDamageFactor params
    (OverseerID _, DroneID _) ->
      gp_overseerDroneCollisionDamageFactor params
    --
    (OverseerID _, OverseerID _) ->
      gp_overseerOverseerCollisionDamageFactor params

-- | Perform the velocity updates associated with a friendly, rigid collision
-- of entities.
friendlyCollision ::
  Fixed ->
  (DynamicsComponent, DynamicsComponent) ->
  ( CollisionParticipants -> NeutralCollision
  , (DynamicsComponent, DynamicsComponent)
  )
friendlyCollision damageEnergyRatio dynComps =
  -- Using a lens directly, specializing to the functor @(,) a@
  alongside
    (field @"dyn_vel" . projected relPos)
    (field @"dyn_vel" . projected relPos)
    (first makeCollision . friendlyCollision1D dynComps)
    dynComps
  where
    -- Note: The sign of `relPos` follows an (arbitrary) convention
    positions = bimap dyn_pos dyn_pos dynComps
    relPos = uncurry subtract positions
    midPos = Fixed.map (* 0.5) $ uncurry (+) positions

    makeCollision :: Fixed -> CollisionParticipants -> NeutralCollision
    makeCollision energyLost collisionParticipants =
      NeutralCollision
        { nce_position = midPos
        , nce_damageEquivalent = energyLost * damageEnergyRatio
        , nce_participants = collisionParticipants
        }

-- | Compute the resulting velocities of a 1D collision between non-enemy
-- entities.
friendlyCollision1D ::
  (DynamicsComponent, DynamicsComponent) ->
  (Fixed, Fixed) ->
  (Fixed, (Fixed, Fixed))
friendlyCollision1D dynComps vels@(v1, v2) =
  let (v1', v2') =
        if v2 - v1 >= 0 -- Note: sign depends on convention
          then vels -- Do nothing if the entities are moving away from each other
          else collision1D coeffRestitution masses vels
      !v1'' = v1' + dv1
      !v2'' = v2' + dv2

      !energyChange =
        0.5 * (m1 * (sq v1' - sq v1) + m2 * (sq v2' - sq v2))
      !energyLost = max 0 (-energyChange)
  in  (energyLost, (v1'', v2''))
  where
    coeffRestitution = 0.9
    (dv1, dv2) = occlusionDeltaV dynComps
    masses@(m1, m2) = over both dyn_mass dynComps

-- | Apply a 1D impulse to separate overlapping entities. Returns the change in
-- velocities for the entities due to this impulse.
occlusionDeltaV :: (DynamicsComponent, DynamicsComponent) -> (Fixed, Fixed)
occlusionDeltaV dynComps =
  let impulse = (* C.density) $ min 16 overlap
  in  (-impulse / m1, impulse / m2)
  where
    both' l = over both l dynComps
    (r1, r2) = both' dyn_radius
    (p1, p2) = bimap dyn_pos dyn_pos dynComps
    (m1, m2) = both' dyn_mass
    overlap = max 0 $ r1 + r2 - Fixed.norm (p2 - p1)

-- | Perform the velocity updates associated with an adversarial, rigid
-- collision of entities. Also returns the energy lost in the collision, which
-- is proportional to the combat damage.
adversarialCollision ::
  DynamicsParams ->
  Fixed ->
  (DynamicsComponent, DynamicsComponent) ->
  ( Maybe ActorID -> Maybe ActorID -> Collision
  , (DynamicsComponent, DynamicsComponent)
  )
adversarialCollision params maxEnergyLoss dynComps =
  -- Using a lens directly, specializing to the functor @(,) a@
  alongside
    (field @"dyn_vel" . projected relPos)
    (field @"dyn_vel" . projected relPos)
    ( first makeCollision
        . adversarialCollision1D (gp_combatDeltaV params) maxEnergyLoss masses
    )
    dynComps
  where
    -- Note: The sign of `relPos` follows an (arbitrary) convention
    relPos = uncurry subtract $ bimap dyn_pos dyn_pos dynComps
    masses = over both dyn_mass dynComps

    -- Record extra information about the collision for player feedback
    makeCollision :: Fixed -> Maybe ActorID -> Maybe ActorID -> Collision
    makeCollision energyLost mPid1 mPid2 =
      Collision
        { col_position = collisionPosition
        , col_velocity = collisionVelocity
        , col_damage = energyLost * gp_damageEnergyRatio params
        , col_direction = relPos
        , col_mActorID1 = mPid1
        , col_mActorID2 = mPid2
        }
      where
        (pos1, pos2) = over both dyn_pos dynComps
        (radius1, radius2) = over both dyn_radius dynComps
        totalRadius = radius1 + radius2
        radiusFrac1 = radius1 / totalRadius
        radiusFrac2 = radius2 / totalRadius
        collisionPosition =
          Fixed.map (* radiusFrac2) pos1 + Fixed.map (* radiusFrac1) pos2

        (vel1, vel2) = over both dyn_vel dynComps
        (mass1, mass2) = masses
        totalMass = mass1 + mass2
        massFrac1 = mass1 / totalMass
        massFrac2 = mass2 / totalMass
        collisionVelocity =
          Fixed.map (* massFrac1) vel1 + Fixed.map (* massFrac2) vel2

-- | Compute the energy lost and resulting velocities of a 1D collision between
-- enemy entities.
adversarialCollision1D ::
  Fixed ->
  Fixed ->
  (Fixed, Fixed) ->
  (Fixed, Fixed) ->
  (Fixed, (Fixed, Fixed))
adversarialCollision1D combatDeltaV maxEnergyLoss masses@(m1, m2) (v1, v2) =
  if v2 - v1 < 0 -- Note: sign depends on convention
    then -- Entities are moving towards each other

      let -- Apply an impulse to force together enemy entities. This is intended
          -- to be applied before collision resolution to increase the combat
          -- damage in a specific way. Returns the change in velocities for the
          -- entities due to this impulse.
          v1' = v1 + combatImpulse / m1
          v2' = v2 - combatImpulse / m2

          (v1'', v2'') = collision1D coeffRestitution masses (v1', v2')

          energyChange =
            0.5
              * ( m1 * (sq v1'' - sq v1')
                    + m2 * (sq v2'' - sq v2')
                )
          energyLost = max 0 (-energyChange)

          -- Limit impulse by health of weakest entity
          frac = sqrt $ min 1 $ maxEnergyLoss / energyLost
          v1''' = v1 + (v1'' - v1) * frac
          v2''' = v2 + (v2'' - v2) * frac
      in  (energyLost, (v1''', v2'''))
    else -- Entities are moving away from each other

      let dv1 = combatImpulse / m1
          dv2 = combatImpulse / m2
          v1' = v1 - coeffRestitution * dv1
          v2' = v2 + coeffRestitution * dv2
          -- Faking the energy lost to be that resulting from a head-on collision
          -- where each entity has momentum equal to `combatImpulse`
          energyLost = 0.5 * (sq coeffRestitution - 1) * dv1 * dv2 * (m1 + m2)
      in  (energyLost, (v1', v2'))
  where
    coeffRestitution = 0.5
    combatImpulse = combatDeltaV * min m1 m2 -- Rationale: the less massive participant pushes and the more massive simply pushes back

-- | Compute the resultant velocities of a 1D collision.
collision1D ::
  Fixed -> (Fixed, Fixed) -> (Fixed, Fixed) -> (Fixed, Fixed)
collision1D coeffRestitution (m1, m2) (v1, v2) =
  let p_total = m1 * v1 + m2 * v2
      m_total = m1 + m2
      v1' = (p_total + (v2 - v1) * m2 * coeffRestitution) / m_total
      v2' = (p_total + (v1 - v2) * m1 * coeffRestitution) / m_total
  in  (v1', v2')

-- *** Determining interactions of entities

data TeamBasedHardCollision
  = NoInteraction
  | FriendlyInteraction
  | AdversarialInteraction
  deriving stock (Eq)

-- | Determine whether or not a pair of entities are allowed to collide.
teamBasedHardCollision ::
  DynamicsParams ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  SEntityID CombatEntity ->
  SEntityID CombatEntity ->
  ST s TeamBasedHardCollision
teamBasedHardCollision params teamStore actorEntityStore combatID1 combatID2 = do
  team1 <- Store.sureLookup teamStore combatID1
  team2 <- Store.sureLookup teamStore combatID2
  if
    -- Enemy units always collide
    | team1 /= team2 ->
        pure AdversarialInteraction
    -- Allied units of different entity type do not collide
    | not (eqEntityType combatID1 combatID2) ->
        pure NoInteraction
    -- Allied units of the same type ...
    | gp_enableFriendlyDroneCollison params ->
        pure FriendlyInteraction
    | otherwise ->
        case refineEntityID combatID1 of
          OverseerID _ ->
            pure FriendlyInteraction
          DroneID _droneID1 -> do
            actorIDMaybe1 <- Store.lookup actorEntityStore combatID1
            actorIDMaybe2 <- Store.lookup actorEntityStore combatID2
            pure $
              if actorIDMaybe1 == actorIDMaybe2
                then FriendlyInteraction
                else NoInteraction

--------------------------------------------------------------------------------

-- *** Psi-storm overlap

psiStormOverlap ::
  DynamicsStore' s ->
  EntityID PsiStorm ->
  SEntityID CombatEntity ->
  ST s (Maybe (SEntityID CombatEntity, Fixed))
psiStormOverlap dynComps psiStormID combatID = do
  (psiStormPos, psiStormRadius) <-
    DS.sureLookup_2 dynComps vec_dyn_pos vec_dyn_radius psiStormID
  (entityPos, entityRadius) <-
    DS.sureLookup_2 dynComps vec_dyn_pos vec_dyn_radius combatID
  let distance = Fixed.norm $ psiStormPos - entityPos
      overlap = psiStormRadius + entityRadius - distance
      overlapFraction = min 1 $ overlap / (2 * entityRadius)
  pure $
    if overlapFraction > 0 then Just (combatID, overlapFraction) else Nothing

--------------------------------------------------------------------------------

-- *** Explosion repulsion

runExplosionRepulsion ::
  DynamicsParams ->
  DynamicsStore' s ->
  EntityID Explosion ->
  SEntityID DynamicsEntity ->
  ST s ()
runExplosionRepulsion params dynComps explosionID dynID = do
  explosionPos <- DS.sureLookup_1 dynComps vec_dyn_pos explosionID
  DS.sureModify_3_1
    dynComps
    vec_dyn_pos
    vec_dyn_radius
    vec_dyn_mass
    vec_dyn_vel
    dynID
    (explosionRepulsion params explosionPos)

explosionRepulsion ::
  DynamicsParams -> Fixed2 -> Fixed2 -> Fixed -> Fixed -> Fixed2 -> Fixed2
explosionRepulsion params explosionPos pos radius mass vel =
  let displacement = pos - explosionPos
      (rawDistance, unitDir) = Fixed.normAndNormalize displacement
      distance = max 20 $ rawDistance - radius
      accel =
        gp_overseerExplosionForceConstant params * (radius / mass / distance)
  in  vel + Fixed.map (* accel) unitDir

--------------------------------------------------------------------------------
-- Helpers

sq :: (Num a) => a -> a
sq x = x * x
