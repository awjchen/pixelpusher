{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.Team (
  TeamStore,
  newTeamStore,
  debugChangeTeam,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Team (Team, oppositeTeam)
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore (..))

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the team to which entity belongs
newtype TeamStore s
  = TeamStore
      ( DenseStore
          s
          (SEntityID TeamEntity)
          (SEntityID TeamEntity)
          (StorableVecStore Team)
      )
  deriving newtype
    ( ReadStore s (SEntityID TeamEntity) Team
    , Store s (SEntityID TeamEntity) Team
    , Mutable s (DenseStoreSnapshot (SEntityID TeamEntity) (StorableVecStore Team))
    , SureReadStore s (SEntityID TeamEntity) Team
    )

instance Copyable s (TeamStore s) where
  copy (TeamStore target) (TeamStore source) =
    DS.copyDenseStore target source

newTeamStore :: ST s (TeamStore s)
newTeamStore = TeamStore <$> DS.newDenseStore C.maxNumEntities

debugChangeTeam :: TeamStore s -> SEntityID TeamEntity -> ST s ()
debugChangeTeam (TeamStore comps) entityID =
  DS.sureModify comps entityID oppositeTeam
