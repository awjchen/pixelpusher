{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.Minion (
  MasterComponent,
  MinionComponent (..),
  MinionStore,
  newMinionStore,
  clearMinionStore,
  MinionStoreSnapshot,
  registerMaster,
  registerMinion,
  unregisterMinion,
  unregisterMaster,
  recruitingMasters,
  getMinions,
  getMinionRespawnCooldownTimes,
  lookupMaster,
  lookupMinionCreationTime,
) where

import Control.Monad.ST
import Data.Foldable (toList)
import Data.Generics.Product.Fields
import Data.Sequence qualified as S
import Data.Serialize (Serialize)
import Data.Void (Void)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Mutable as Mutable
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (BoxedVecStore, makeVecStore)

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  CoreM,
  coreM,
 )
import {-# SOURCE #-} Pixelpusher.Game.State.Core qualified as Store (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------
-- Components

-- | Entities with references to other entities
data MasterComponent = MasterComponent
  { master_minionIDs :: WIS.IntSet (EntityID Drone)
  , master_minionDeathTimes :: S.Seq Time
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

newMasterComponent :: Int -> MasterComponent
newMasterComponent minionLimit =
  MasterComponent
    { master_minionIDs = WIS.empty
    , master_minionDeathTimes = S.replicate minionLimit initialTime
    }

addMinion :: EntityID Drone -> MasterComponent -> MasterComponent
addMinion minionID =
  over (field @"master_minionIDs") (WIS.insert minionID)
    . over (field @"master_minionDeathTimes") dropLeft
  where
    dropLeft :: S.Seq a -> S.Seq a
    dropLeft s = case S.viewl s of
      S.EmptyL -> s
      _ S.:< s' -> s'

removeMinion ::
  EntityID Drone -> Time -> MasterComponent -> MasterComponent
removeMinion minionID time =
  over (field @"master_minionIDs") (WIS.delete minionID)
    . over (field @"master_minionDeathTimes") (S.|> time)

hasReadyMinionRespawn :: CombatParams -> Time -> MasterComponent -> Bool
hasReadyMinionRespawn params time masterComp =
  case master_minionDeathTimes masterComp of
    deathTime S.:<| _ ->
      diffTime time deathTime >= gp_droneRespawnDelay params
    _ -> False

-- | Entities that serve other entities
data MinionComponent = MinionComponent
  { minion_masterEID :: EntityID Overseer
  , minion_creationTime :: Time
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeVecStore ''MinionComponent

--------------------------------------------------------------------------------
-- Store

-- Invariant: The master of each minion exists in the masters store.

data MinionStore s = MinionStore
  { ms_masters ::
      DenseStore
        s
        (EntityID Overseer)
        (EntityID Overseer)
        (BoxedVecStore MasterComponent)
  , ms_minions ::
      DenseStore
        s
        (EntityID Drone)
        Void
        MinionComponentVec
  }

-- TODO: rename
newMinionStore :: ST s (MinionStore s)
newMinionStore =
  MinionStore
    <$> DS.newDenseStore (fromIntegral C.maxActorsPerGame)
    <*> DS.newDenseStore C.maxNumEntities

-- TODO: rename

clearMinionStore :: MinionStore s -> CoreM s ()
clearMinionStore store = do
  Store.clear $ ms_masters store
  Store.clear $ ms_minions store

data MinionStoreSnapshot = MinionStoreSnapshot
  -- TODO: rename
  { snapshot_masters ::
      DenseStoreSnapshot (EntityID Overseer) (BoxedVecStore MasterComponent)
  , snapshot_minions ::
      DenseStoreSnapshot (EntityID Drone) MinionComponentVec
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s MinionStoreSnapshot (MinionStore s) where
  freeze :: MinionStore s -> ST s MinionStoreSnapshot
  freeze (MinionStore masters minions) =
    MinionStoreSnapshot <$> freeze masters <*> freeze minions

  thaw :: MinionStoreSnapshot -> ST s (MinionStore s)
  thaw (MinionStoreSnapshot masters minions) =
    MinionStore <$> thaw masters <*> Mutable.thaw minions

instance Copyable s (MinionStore s) where
  copy target source = do
    DS.copyDenseStore (ms_masters target) (ms_masters source)
    DS.copyDenseStore (ms_minions target) (ms_minions source)

registerMaster ::
  MinionStore s -> EntityID Overseer -> Int -> CoreM s ()
registerMaster store masterID minionLimit =
  Store.insert (ms_masters store) masterID (newMasterComponent minionLimit)

registerMinion ::
  MinionStore s -> EntityID Drone -> MinionComponent -> CoreM s ()
registerMinion store minionID minionComp = do
  Store.insert (ms_minions store) minionID minionComp
  let masterID = minion_masterEID minionComp
  coreM $ DS.modify (ms_masters store) masterID (addMinion minionID)

unregisterMinion ::
  MinionStore s -> Time -> EntityID Drone -> CoreM s ()
unregisterMinion store time minionID =
  coreM (Store.lookup (ms_minions store) minionID) >>= \case
    Nothing -> pure ()
    Just minionComp -> do
      let masterID = minion_masterEID minionComp
      Store.delete (ms_minions store) minionID
      coreM $
        DS.modify (ms_masters store) masterID (removeMinion minionID time)

unregisterMaster ::
  MinionStore s -> EntityID Overseer -> CoreM s [EntityID Drone]
unregisterMaster store masterID =
  coreM (Store.lookup (ms_masters store) masterID) >>= \case
    Nothing -> pure []
    Just masterComponent -> do
      Store.delete (ms_masters store) masterID
      let minionIDs = WIS.toList $ master_minionIDs masterComponent
      mapM_ (Store.delete (ms_minions store)) minionIDs
      pure minionIDs

--------------------------------------------------------------------------------
-- Query

recruitingMasters ::
  MinionStore s ->
  CombatParams ->
  Time ->
  ST s [EntityID Overseer]
recruitingMasters store params time =
  map fst . filter (hasReadyMinionRespawn params time . snd)
    <$> DS.toList (ms_masters store)

getMinions ::
  MinionStore s ->
  EntityID Overseer ->
  ST s (WIS.IntSet (EntityID Drone))
getMinions store masterEID =
  master_minionIDs <$> Store.sureLookup (ms_masters store) masterEID

-- | Returns respawn times in increasing order. Some minions may have zero
-- respawn cooldown even if they have not respawned. This is because we limit
-- the rate at which overseers may respawn drones; though, this limit may be
-- removed in the future.
getMinionRespawnCooldownTimes ::
  MinionStore s ->
  CombatParams ->
  Time ->
  EntityID Overseer ->
  ST s [Ticks]
getMinionRespawnCooldownTimes store params time overseerID =
  Store.sureLookup (ms_masters store) overseerID <&> \masterComp ->
    let deathTimes = toList $ master_minionDeathTimes masterComp
        respawnDelay = gp_droneRespawnDelay params
        timeToRespawn deathTime =
          let respawnTime = addTicks respawnDelay deathTime
          in  respawnTime `diffTime` time
    in  map (max 0 . timeToRespawn) deathTimes

lookupMaster ::
  MinionStore s -> EntityID Drone -> ST s (Maybe (EntityID Overseer))
lookupMaster store = DS.lookup_1 (ms_minions store) vec_minion_masterEID

lookupMinionCreationTime ::
  MinionStore s -> EntityID Drone -> ST s (Maybe Time)
lookupMinionCreationTime store =
  DS.lookup_1 (ms_minions store) vec_minion_creationTime
