{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.Transitory (
  TransitoryStore,
  newTransitoryStore,
  getExpiredEntities,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore)

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the time at which entities should expire.
newtype TransitoryStore s
  = TransitoryStore
      ( DenseStore
          s
          (SEntityID MaybeTransitoryEntity)
          (SEntityID TransitoryEntity)
          (StorableVecStore Time)
      )
  deriving newtype
    ( ReadStore s (SEntityID MaybeTransitoryEntity) Time
    , Store s (SEntityID MaybeTransitoryEntity) Time
    , Mutable s (DenseStoreSnapshot (SEntityID MaybeTransitoryEntity) (StorableVecStore Time))
    , SureReadStore s (SEntityID TransitoryEntity) Time
    )

instance Copyable s (TransitoryStore s) where
  copy (TransitoryStore target) (TransitoryStore source) =
    DS.copyDenseStore target source

newTransitoryStore :: ST s (TransitoryStore s)
newTransitoryStore = TransitoryStore <$> DS.newDenseStore C.maxNumEntities

--------------------------------------------------------------------------------

-- TODO: Improve asymptotics of this function
getExpiredEntities ::
  TransitoryStore s -> Time -> ST s [SEntityID MaybeTransitoryEntity]
getExpiredEntities (TransitoryStore store) time =
  map fst . filter ((<= time) . snd) <$> DS.toList store
