{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.ActorEntity (
  ActorEntityStore,
  newActorEntityStore,
  getActorEntities,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore (..))

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the player that owns the entity
newtype ActorEntityStore s
  = ActorEntityStore
      ( DenseStore
          s
          (SEntityID MaybeActorEntity)
          (SEntityID ActorEntity)
          (StorableVecStore ActorID)
      )
  deriving newtype
    ( ReadStore s (SEntityID MaybeActorEntity) ActorID
    , Store s (SEntityID MaybeActorEntity) ActorID
    , Mutable s (DenseStoreSnapshot (SEntityID MaybeActorEntity) (StorableVecStore ActorID))
    , SureReadStore s (SEntityID ActorEntity) ActorID
    )

instance Copyable s (ActorEntityStore s) where
  copy (ActorEntityStore target) (ActorEntityStore source) =
    DS.copyDenseStore target source

newActorEntityStore :: ST s (ActorEntityStore s)
newActorEntityStore = ActorEntityStore <$> DS.newDenseStore C.maxNumEntities

--------------------------------------------------------------------------------

getActorEntities ::
  ActorEntityStore s -> ActorID -> ST s [SEntityID MaybeActorEntity]
getActorEntities (ActorEntityStore store) actorID =
  DS.indicesPred_1 store getStorableMVec (== actorID)
