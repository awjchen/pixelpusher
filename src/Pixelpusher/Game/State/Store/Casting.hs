module Pixelpusher.Game.State.Store.Casting (
  runPlayerCasts,
) where

import Control.Monad (guard, void, when)
import Control.Monad.ST (ST)
import Control.Monad.State.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State.Strict (runStateT)
import Data.Generics.Product.Fields
import Data.Maybe (fromMaybe, maybeToList)
import Data.Traversable (for)
import Data.Tuple (swap)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.State.Core (GameStateCore)
import Pixelpusher.Game.State.Core qualified as Core
import Pixelpusher.Game.State.Store.Dynamics
import Pixelpusher.Game.State.Store.Minion
import Pixelpusher.Game.State.Store.Team

-- TODO: Separate cast events into PsiStormCast and DroneDashCast events
runPlayerCasts ::
  CastParams ->
  GameStateCore s ->
  Time ->
  WIM.IntMap ActorID (PlayerControls, PlayerControls) ->
  ST s [CastEvent]
runPlayerCasts params core time playerControls =
  Core.traversePlayerControlState core $ \actorID playerStatus -> do
    let controlState = ps_controlState playerStatus
        noop = pure (controlState, mempty)
    case ps_overseerStatus playerStatus of
      OverseerDead{} -> noop
      OverseerAlive overseerID _ ->
        case WIM.lookup actorID playerControls of
          Nothing -> noop
          Just controls ->
            runPlayerCast
              params
              core
              time
              actorID
              (ps_playerClass playerStatus)
              controls
              controlState
              overseerID

runPlayerCast ::
  CastParams ->
  GameStateCore s ->
  Time ->
  ActorID ->
  PlayerClass ->
  (PlayerControls, PlayerControls) ->
  PlayerControlState ->
  EntityID Overseer ->
  ST s (PlayerControlState, [CastEvent])
runPlayerCast
  params
  core
  time
  actorID
  playerClass
  (_prevControls, controls)
  controlState
  overseerID = do
    let Core.Stores{store_dynamics, store_masterMinions, store_team} =
          Core.dataStores core
        buttons = control_buttons controls
    fmap swap $
      flip runStateT controlState $ do
        mPsiStorm <- zoom (field @"pcs_psiStormCooldownUntil") $
          -- TODO: cleanup
          runMaybeT $ do
            guard $ playerClass == Templar
            guard $ buttons ^. control_psiStorm == PsiStormOn
            cooldownUntil <- get
            guard $ time >= cooldownUntil
            put $! gp_psiStormCastCooldownTicks params `addTicks` time
            lift . lift . fmap PsiStormCastEvent_ $
              runPsiStorm params core time actorID playerClass controls overseerID
        overseerDashes <- do
          -- TODO: cleanup
          (dashReady, dashReset) <- do
            dashStamina <- gets pcs_overseerDashStamina
            pure
              ( isDashReady params playerClass time dashStamina
              , isDashReset dashStamina
              )
          let shouldDash =
                dashReady
                  && dashReset
                  && buttons ^. control_overseerDash == OverseerDashOn
              updateCooldown =
                field @"pcs_overseerDashStamina"
                  %= decrementDashStamina params playerClass time
          if not shouldDash
            then do
              when (buttons ^. control_overseerDash == OverseerDashOff) $
                field @"pcs_overseerDashStamina" %= resetDash time
              pure []
            else do
              mOverseerDash <-
                runMaybeT $ do
                  guard $ gp_enableOverseerDash params
                  guard $ (MoveNeutral /=) $ view control_move buttons
                  updateCooldown
                  lift . lift $
                    runOverseerDash
                      params
                      store_dynamics
                      store_team
                      controls
                      overseerID
                      playerClass
              pure $ maybeToList mOverseerDash
        mDroneDashes <-
          -- TODO: cleanup
          runMaybeT $ do
            let currentDroneCmd = controls ^. to control_buttons . control_drone
                lastDroneCmd = pcs_lastDroneCommand controlState
            guard $ lastDroneCmd /= currentDroneCmd
            lift $ field @"pcs_lastDroneCommand" .= currentDroneCmd
            lift $ field @"pcs_lastDroneCommandInitialTime" .= time
            chargeTicks <-
              hoistMaybe $ sharedDroneChargeTicks params time controlState
            lift $ do
              field @"pcs_lastDroneDash" .= (time, chargeTicks)
              lift $
                runDroneDash
                  params
                  store_dynamics
                  store_masterMinions
                  store_team
                  time
                  playerClass
                  controls
                  overseerID
                  chargeTicks
        pure $
          maybeToList mPsiStorm ++ overseerDashes ++ fromMaybe [] mDroneDashes

runPsiStorm ::
  CastParams ->
  GameStateCore s ->
  Time ->
  ActorID ->
  PlayerClass ->
  PlayerControls ->
  EntityID Overseer ->
  ST s PsiStormCastEvent
runPsiStorm params core time actorID playerClass controls overseerID = do
  let Core.Stores{store_dynamics} = Core.dataStores core
  overseerPos <- readPos store_dynamics overseerID
  let castRange = getClassParam playerClass (gp_castRange params)
      cursorPos = control_worldPos controls
      deltaPos = cursorPos - overseerPos
      normDeltaPos = Fixed.norm deltaPos
      scale =
        if normDeltaPos <= castRange
          then 1.0
          else castRange / normDeltaPos
      targetPos = Fixed.map (* scale) deltaPos + overseerPos
  void $ Core.addPsiStorm params core time (Just actorID) targetPos
  pure $ PsiStormCastEvent targetPos

runDroneDash ::
  CastParams ->
  DynamicsStore s ->
  MinionStore s ->
  TeamStore s ->
  Time ->
  PlayerClass ->
  PlayerControls ->
  EntityID Overseer ->
  Ticks ->
  ST s [CastEvent]
runDroneDash
  params
  dynamicsStore
  masterMinionStore
  teamStore
  time
  playerClass
  controls
  overseerID
  chargeTicks = do
    team <- Core.sureLookup teamStore overseerID
    overseerPos <- readPos dynamicsStore overseerID
    let droneAccelScale =
          case controls ^. to control_buttons . control_drone of
            DroneNeutral -> 0
            DroneAttraction -> 1
            DroneRepulsion -> (-1)
        mousePos = control_worldPos controls
        range = getClassParam playerClass (gp_castRange params)
        diff = mousePos - overseerPos
        boundedMousePos =
          if Fixed.normLeq diff range
            then mousePos
            else
              let scale = range / Fixed.norm diff
              in  Fixed.map (* scale) diff + overseerPos
    minions <- getMinions masterMinionStore overseerID
    for (WIS.toList minions) $ \droneID -> do
      maxChargeTicks <-
        maybe 0 (time `diffTime`)
          <$> lookupMinionCreationTime masterMinionStore droneID
      let droneChargeTicks = min chargeTicks maxChargeTicks
          baseDeltaV = droneDashDeltaV params droneChargeTicks
      (pos, vel, radius) <- readPosVelRadius dynamicsStore droneID
      let direction =
            Fixed.normalize $
              Fixed.map (* droneAccelScale) $
                boundedMousePos - pos
          deltaV = Fixed.map (* baseDeltaV) direction
      modifyVelocity dynamicsStore droneID $ (+) deltaV
      let effectIntensityFrac =
            baseDeltaV
              / droneDashDeltaV params (gp_maxDroneDashChargeTicks params)
      pure $
        DashCastEvent_
          DashCastEvent
            { dce_pos = pos
            , dce_vel = vel
            , dce_entityRadius = radius
            , dce_direction = direction
            , dce_dashDeltaV = baseDeltaV
            , dce_effectsMagnitudeFrac = effectIntensityFrac
            , dce_dashType = DroneDash
            , dce_team = team
            }

runOverseerDash ::
  CastParams ->
  DynamicsStore s ->
  TeamStore s ->
  PlayerControls ->
  EntityID Overseer ->
  PlayerClass ->
  ST s CastEvent
runOverseerDash params dynamicsStore teamStore controls overseerID playerClass = do
  team <- Core.sureLookup teamStore overseerID
  (pos, vel, radius) <- readPosVelRadius dynamicsStore overseerID
  let direction = arrowKeyDirection controls
      baseDeltaV = getClassParam playerClass (gp_overseerDashDeltaV params)
      deltaV = Fixed.map (* baseDeltaV) direction
  modifyVelocity dynamicsStore overseerID $ (+) deltaV
  pure $
    DashCastEvent_
      DashCastEvent
        { dce_pos = pos
        , dce_vel = vel
        , dce_entityRadius = radius
        , dce_direction = direction
        , dce_dashDeltaV = baseDeltaV
        , dce_effectsMagnitudeFrac = 1
        , dce_dashType = OverseerDash
        , dce_team = team
        }
