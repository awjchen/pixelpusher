{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.State.Store.PsiStorm (
  PsiStormStore,
  newPsiStormStore,
  getPsiStorms,
) where

import Control.Monad.ST

import Pixelpusher.Custom.Mutable
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time
import Pixelpusher.Store.DenseStore (DenseStore, DenseStoreSnapshot)
import Pixelpusher.Store.DenseStore qualified as DS
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (StorableVecStore)

import {-# SOURCE #-} Pixelpusher.Game.State.Core (
  ReadStore (..),
  Store (..),
  SureReadStore (..),
 )

--------------------------------------------------------------------------------

-- | Stores the time at which a psi-storm was cast
newtype PsiStormStore s
  = PsiStormStore
      ( DenseStore
          s
          (EntityID PsiStorm)
          (EntityID PsiStorm)
          (StorableVecStore Time)
      )
  deriving newtype
    ( ReadStore s (EntityID PsiStorm) Time
    , Store s (EntityID PsiStorm) Time
    , Mutable s (DenseStoreSnapshot (EntityID PsiStorm) (StorableVecStore Time))
    , SureReadStore s (EntityID PsiStorm) Time
    )

instance Copyable s (PsiStormStore s) where
  copy (PsiStormStore target) (PsiStormStore source) =
    DS.copyDenseStore target source

newPsiStormStore :: ST s (PsiStormStore s)
newPsiStormStore = PsiStormStore <$> DS.newDenseStore C.maxNumEntities

--------------------------------------------------------------------------------

getPsiStorms :: PsiStormStore s -> ST s [EntityID PsiStorm]
getPsiStorms (PsiStormStore store) = DS.indices store
