{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RoleAnnotations #-}
{-# LANGUAGE UndecidableInstances #-}

-- | This module encapsulates the "core" part of the game state, which is
-- (mostly) stored in mutable arrays ("stores") and indexed by integers
-- ('EntityID's).
--
-- Game entities consist of (1) an 'EntityID' that identifies the game entity,
-- and (2) a collection of data components distributed across multiple
-- component stores. The component stores are indexed by 'EntityID's, which
-- relates data components to their entities.
--
-- The logic in this module is responsible creating and destroying game
-- entities and their data components while maintaining the consistency of the
-- data components across stores.
--
-- Specifically, we aim to maintain the following relationships:
--
-- * The status of each player's overseer is tracked by their 'OverseerStatus'.
-- * Each entity has the data components prescribed by its type.
module Pixelpusher.Game.State.Core (
  -- * Generic store interface
  Store (..),
  CoreM,
  coreM,

  -- ** Read-only store interface
  ReadStore (..),
  SureReadStore (..),
  ReadOnly,
  readOnly,

  -- * State
  GameStateCore,
  dataStores,
  Stores (..),
  initialGameStateCore,
  resetGameStateCore,

  -- ** State snapshot
  GameStateCoreSnapshot,

  -- * Query
  getPlayerStatuses,
  getPlayerStatus,
  getMapPowerups,

  -- * Operations

  -- ** Players
  addPlayer,
  removePlayer,
  traversePlayerControlState,
  updatePlayerClasses,

  -- ** Game entities
  finallyRemoveEntities,
  respawnOverseers,
  DroneSpawnType (..),
  addDrone,
  addSoftObstacle,
  addPsiStorm,
  addFlag,
  addBaseFlag,
  addTeamBase,
  addExplosion,
  addSpawnArea,
  respawnBoundPowerups,
  addFreePowerup,

  -- * Debug
  debugSwitchPlayerTeam,
) where

import Prelude hiding (lookup)

import Control.Monad (replicateM_, void, when)
import Control.Monad.ST (ST)
import Control.Monad.Trans.Writer
import Data.Foldable (for_, traverse_)
import Data.Generics.Product.Fields
import Data.Maybe (fromMaybe)
import Data.STRef (STRef, modifySTRef', newSTRef, readSTRef, writeSTRef)
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Mutable
import Pixelpusher.Custom.Util (
  expectJust,
  uniformUnitDisc,
 )
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.ClassParameter (getClassParam)
import Pixelpusher.Game.CombatComponent
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.DynamicsComponent
import Pixelpusher.Game.FlagComponent
import Pixelpusher.Game.GamePhase
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Parameters.Dynamics (shieldRadius)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControlState (PlayerControlState, initialControlState)
import Pixelpusher.Game.PlayerControls (PlayerControls, control_buttons, control_move)
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.PowerupComponent
import Pixelpusher.Game.Team (Team, oppositeTeam)
import Pixelpusher.Game.TeamLocations (spawnAreaLocation)
import Pixelpusher.Game.Time (Ticks (..), Time, addTicks, diffTime, initialTime)
import Pixelpusher.Game.Time qualified as Time
import Pixelpusher.Store.DenseStore (DenseStoreSnapshot)
import Pixelpusher.Store.EntityID
import Pixelpusher.Store.VecStore (BoxedVecStore, StorableVecStore)

import Pixelpusher.Game.State.Store.ActorEntity (ActorEntityStore)
import Pixelpusher.Game.State.Store.ActorEntity qualified as ActorEntity
import Pixelpusher.Game.State.Store.Combat (CombatStore)
import Pixelpusher.Game.State.Store.Combat qualified as Combat
import Pixelpusher.Game.State.Store.Dynamics (DynamicsStore)
import Pixelpusher.Game.State.Store.Dynamics qualified as Dynamics
import Pixelpusher.Game.State.Store.Flag (FlagStore)
import Pixelpusher.Game.State.Store.Flag qualified as Flag
import Pixelpusher.Game.State.Store.Minion (
  MinionComponent (..),
  MinionStore,
  MinionStoreSnapshot,
 )
import Pixelpusher.Game.State.Store.Minion qualified as Minion
import Pixelpusher.Game.State.Store.PlayerClass (PlayerClassStore)
import Pixelpusher.Game.State.Store.PlayerClass qualified as PlayerClass
import Pixelpusher.Game.State.Store.Powerup (PowerupStore)
import Pixelpusher.Game.State.Store.Powerup qualified as Powerup
import Pixelpusher.Game.State.Store.PsiStorm (PsiStormStore)
import Pixelpusher.Game.State.Store.PsiStorm qualified as PsiStorm
import Pixelpusher.Game.State.Store.Team (TeamStore)
import Pixelpusher.Game.State.Store.Team qualified as Team
import Pixelpusher.Game.State.Store.Transitory (TransitoryStore)
import Pixelpusher.Game.State.Store.Transitory qualified as Transitory

--------------------------------------------------------------------------------
-- Store interfaces

-- | An interface through which a data store can allow for the creation and
-- deletion of data entries for a specific entity. All such operations should
-- be restricted to the 'CoreM' context.
class Store s idx elem store | store -> idx, store -> elem where
  -- | Add a data component to the store at the given index. This function
  -- should be the only way to add entries to a store.
  insert :: (SubEntityID i idx) => store -> i -> elem -> CoreM s ()

  -- | Remove the data component in the store at the given index. This function
  -- should be the only way to remove entries to a store, other than 'clear'.
  delete :: (SubEntityID i idx) => store -> i -> CoreM s ()

  -- | A convenience function for removing all data from the store.
  clear :: store -> CoreM s ()

-- | An interface through which one can retrieve data about an entity from a
-- data store.
class ReadStore s idx elem store | store -> idx, store -> elem where
  lookup :: (SubEntityID i idx) => store -> i -> ST s (Maybe elem)

-- | An interface for data stores through which data about an entity can be
-- retrieved when the target data is known to be present in the store.
class SureReadStore s sidx elem store | store -> sidx, store -> elem where
  -- | Like 'lookup', but we know that the store is has an entry at the given
  -- index.
  sureLookup :: (SubEntityID i sidx) => store -> i -> ST s elem

--------------------------------------------------------------------------------
-- Read-only store wrapper

-- | A wrapper for entity stores that exposes only their 'ReadStore' instances
newtype ReadOnly store = ReadOnly store

instance
  (ReadStore s idx elem store) =>
  ReadStore s idx elem (ReadOnly store)
  where
  lookup :: (SubEntityID i idx) => ReadOnly store -> i -> ST s (Maybe elem)
  lookup (ReadOnly store) = lookup store

instance
  (SureReadStore s sidx elem store) =>
  SureReadStore s sidx elem (ReadOnly store)
  where
  sureLookup :: (SubEntityID i sidx) => ReadOnly store -> i -> ST s elem
  sureLookup (ReadOnly store) = sureLookup store

-- | Wrap an entity store, exposing only its 'ReadStore' instance.
readOnly :: store -> ReadOnly store
readOnly = ReadOnly

--------------------------------------------------------------------------------
-- Store action context

-- | 'CoreM' is a wrapper around 'ST' that can only be unwrapped by the code
-- in this module. We want all operations on entity stores that create or
-- delete entries to be encapsulated in this module so that we can more easily
-- maintain invariants across data stores. To achieve encapsulation, the
-- implementation of each individual entity store /must/ ensure that they use
-- 'CoreM' to wrap all operations that create or delete entries.
newtype CoreM s a = CoreM {runCoreM :: ST s a}
  deriving newtype (Functor, Applicative, Monad)

type role CoreM nominal representational

-- | Wrap an 'ST' action, labelling it as a store action.
coreM :: ST s a -> CoreM s a
coreM = CoreM

--------------------------------------------------------------------------------
-- Collection of all stores

-- | An opaque type containing the collection of all stores.
data GameStateCore s = GameStateCore
  { gsc_entityIDPool :: EntityIDPool s
  , gsc_actors :: STRef s (WIM.IntMap ActorID PlayerStatus)
  , gsc_mapPowerups :: STRef s MapPowerupStatuses
  , gsc_dataStores :: Stores s
  }
  deriving stock (Generic)

dataStores :: GameStateCore s -> Stores s
dataStores = gsc_dataStores

-- | All the stores accessible to the game logic.
data Stores s = Stores
  { store_team :: TeamStore s
  , store_dynamics :: DynamicsStore s
  , store_combat :: CombatStore s
  , store_masterMinions :: MinionStore s
  , store_transitory :: TransitoryStore s
  , store_flag :: FlagStore s
  , store_actorEntity :: ActorEntityStore s
  , store_psiStorm :: PsiStormStore s
  , store_powerup :: PowerupStore s
  , store_playerClass :: PlayerClassStore s
  }
  deriving stock (Generic)

initialGameStateCore :: Time -> ST s (GameStateCore s)
initialGameStateCore initTime =
  GameStateCore
    <$> newEntityIDPool
    <*> newSTRef WIM.empty
    <*> newSTRef (initMapPowerupStatuses initTime)
    <*> initialStores

initialStores :: ST s (Stores s)
initialStores =
  Stores
    <$> Team.newTeamStore
    <*> Dynamics.newDynamicsStore
    <*> Combat.newCombatStore
    <*> Minion.newMinionStore
    <*> Transitory.newTransitoryStore
    <*> Flag.newFlagStore
    <*> ActorEntity.newActorEntityStore
    <*> PsiStorm.newPsiStormStore
    <*> Powerup.newPowerupStore
    <*> PlayerClass.newPlayerClassStore

data GameStateCoreSnapshot = GameStateCoreSnapshot
  { gscs_entityIDPool :: EntityIDPoolSnapshot
  , gscs_players :: WIM.IntMap ActorID PlayerStatus
  , gscs_mapPowerups :: MapPowerupStatuses
  , gscs_stores :: StoresSnapshot
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data StoresSnapshot = StoresSnapshot
  { storeSnapshot_team ::
      DenseStoreSnapshot (SEntityID TeamEntity) (StorableVecStore Team)
  , storeSnapshot_dynamics ::
      DenseStoreSnapshot (SEntityID DynamicsEntity) DynamicsComponentVec
  , storeSnapshot_combat ::
      DenseStoreSnapshot (SEntityID CombatEntity) CombatComponentVec
  , storeSnapshot_masterMinions ::
      MinionStoreSnapshot
  , storeSnapshot_transitory ::
      DenseStoreSnapshot (SEntityID MaybeTransitoryEntity) (StorableVecStore Time)
  , storeSnapshot_flag ::
      DenseStoreSnapshot (SEntityID FlagEntity) FlagComponentVec
  , storeSnapshot_playerEntity ::
      DenseStoreSnapshot (SEntityID MaybeActorEntity) (StorableVecStore ActorID)
  , storeSnapshot_psiStorm ::
      DenseStoreSnapshot (EntityID PsiStorm) (StorableVecStore Time)
  , storeSnapshot_powerup ::
      DenseStoreSnapshot (EntityID Powerup) PowerupComponentVec
  , storeSnapshot_playerClass ::
      DenseStoreSnapshot (SEntityID PlayerClassEntity) (BoxedVecStore PlayerClass)
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s GameStateCoreSnapshot (GameStateCore s) where
  freeze GameStateCore{..} =
    GameStateCoreSnapshot
      <$> freeze gsc_entityIDPool
      <*> readSTRef gsc_actors
      <*> readSTRef gsc_mapPowerups
      <*> freeze gsc_dataStores
  thaw GameStateCoreSnapshot{..} =
    GameStateCore
      <$> thaw gscs_entityIDPool
      <*> newSTRef gscs_players
      <*> newSTRef gscs_mapPowerups
      <*> thaw gscs_stores

instance Mutable s StoresSnapshot (Stores s) where
  freeze Stores{..} =
    StoresSnapshot
      <$> freeze store_team
      <*> freeze store_dynamics
      <*> freeze store_combat
      <*> freeze store_masterMinions
      <*> freeze store_transitory
      <*> freeze store_flag
      <*> freeze store_actorEntity
      <*> freeze store_psiStorm
      <*> freeze store_powerup
      <*> freeze store_playerClass
  thaw StoresSnapshot{..} =
    Stores
      <$> thaw storeSnapshot_team
      <*> thaw storeSnapshot_dynamics
      <*> thaw storeSnapshot_combat
      <*> thaw storeSnapshot_masterMinions
      <*> thaw storeSnapshot_transitory
      <*> thaw storeSnapshot_flag
      <*> thaw storeSnapshot_playerEntity
      <*> thaw storeSnapshot_psiStorm
      <*> thaw storeSnapshot_powerup
      <*> thaw storeSnapshot_playerClass

instance Copyable s (GameStateCore s)
instance Copyable s (Stores s)

-- | Clear all entities and `EntityID`s from the store, setting it a state
-- equivalent to that of `initialGameStateCore`. This can be used to reset the
-- game state between game rounds.
--
-- Note: We perform a pattern match on the constructor to make it harder to
-- forget a component.
resetGameStateCore :: Time -> GameStateCore s -> ST s ()
resetGameStateCore time (GameStateCore eidPool playerOverseers powerups stores) = do
  clearEntityIDPool eidPool
  writeSTRef playerOverseers WIM.empty
  writeSTRef powerups (initMapPowerupStatuses time)
  resetStores stores

-- Note: We perform a pattern match on the constructor to make it harder to
-- forget a component.
resetStores :: Stores s -> ST s ()
resetStores
  ( Stores
      team
      dynamics
      combat
      minion
      transitory
      flag
      playerEntity
      psiStorm
      powerup
      playerClass
    ) =
    runCoreM $ do
      clear team
      clear dynamics
      clear combat
      Minion.clearMinionStore minion
      clear transitory
      clear flag
      clear playerEntity
      clear psiStorm
      clear powerup
      clear playerClass

--------------------------------------------------------------------------------
-- Queries

getPlayerStatuses :: GameStateCore s -> ST s (WIM.IntMap ActorID PlayerStatus)
getPlayerStatuses core = readSTRef $ gsc_actors core

getPlayerStatus :: GameStateCore s -> ActorID -> ST s (Maybe PlayerStatus)
getPlayerStatus core actorID =
  fmap (WIM.lookup actorID) $ readSTRef $ gsc_actors core

getMapPowerups :: GameStateCore s -> ST s MapPowerupStatuses
getMapPowerups core = readSTRef $ gsc_mapPowerups core

--------------------------------------------------------------------------------
-- Mutations

traversePlayerControlState ::
  (Monoid w) =>
  GameStateCore s ->
  (ActorID -> PlayerStatus -> ST s (PlayerControlState, w)) ->
  ST s w
traversePlayerControlState core f = do
  players <- readSTRef playersRef
  (newPlayers, w) <-
    runWriterT $
      flip WIM.traverseWithKey players $
        \actorID playerStatus -> do
          newControlState <- WriterT $ f actorID playerStatus
          pure playerStatus{ps_controlState = newControlState}
  writeSTRef playersRef newPlayers
  pure w
  where
    playersRef = gsc_actors core

updatePlayerClasses ::
  GameParams ->
  GameStateCore s ->
  Time ->
  WIM.IntMap ActorID (PlayerControls, PlayerControls, Time) ->
  ST s ()
updatePlayerClasses params core time playerControlsJoinTime =
  modifySTRef' (gsc_actors core) $ \players ->
    flip WIM.mapWithKey players $ \actorID playerStatus ->
      case ps_overseerStatus playerStatus of
        OverseerAlive{} -> playerStatus
        OverseerDead deathTime respawnTime _ ->
          if (time `diffTime` deathTime <= respawnClassSelectionDeadTimeInitial)
            || (respawnTime `diffTime` time <= respawnClassSelectionDeadTimeFinal)
            then playerStatus
            else case WIM.lookup actorID playerControlsJoinTime of
              Nothing -> playerStatus
              Just (oldControls, controls, joinTime) ->
                let getMoveCmd = view control_move . control_buttons
                in  if time <= addTicks (gp_startSpawnDelayTicks params) joinTime
                      || getMoveCmd oldControls == getMoveCmd controls
                      then playerStatus
                      else
                        playerStatus
                          { ps_playerClass =
                              fromMaybe (ps_playerClass playerStatus) $
                                moveCommandToPlayerClass $
                                  getMoveCmd controls
                          }

debugSwitchPlayerTeam :: GameStateCore s -> ActorID -> ST s ()
debugSwitchPlayerTeam core actorID =
  modifySTRef' (gsc_actors core) $
    flip WIM.adjust actorID $ \playerStatus ->
      playerStatus{ps_team = oppositeTeam (ps_team playerStatus)}

--------------------------------------------------------------------------------
-- Coordinated store operations

-- Adding and removing players

addPlayer ::
  GameParams ->
  GameStateCore s ->
  Time ->
  Team ->
  ActorID ->
  PlayerClass ->
  ST s ()
addPlayer params core time team aid playerClass = do
  let respawnTime = addTicks (gp_startSpawnDelayTicks params) time
      fakeDeathTime =
        addTicks (-gp_overseerRespawnDelay (gp_combat params)) respawnTime
      playerStatus =
        PlayerStatus
          { ps_team = team
          , ps_playerClass = playerClass
          , ps_overseerStatus =
              OverseerDead
                fakeDeathTime
                respawnTime
                (spawnAreaLocation params team)
          , ps_controlState = initialControlState time
          }
  modifySTRef' (gsc_actors core) $ at aid ?~ playerStatus

-- Should be idempotent
removePlayer :: GameParams -> GameStateCore s -> Time -> ActorID -> ST s ()
removePlayer params core time actorID = do
  let Stores{store_actorEntity} = gsc_dataStores core

  -- Dissociate actor entities
  ActorEntity.getActorEntities store_actorEntity actorID
    >>= traverse_ (dissociateActorEntity params core time)

  modifySTRef' (gsc_actors core) $ at actorID .~ Nothing

-- Entity removal

-- | Run an ST action with the ability to mark entities for removal from the
-- given `GameStateCore`. The marked entities will be removed at the end of the
-- action. This function is the only way to remove entities from an
-- 'GameStateCore`.
--
-- 'EntityID's act as references to entities. If the entity referred to by an
-- 'EntityID' is removed, continued use of the 'EntityID' will likely yield
-- nonsensical results. Our goal is to prevent such "invalidation" of
-- 'EntityID's; to this end, we aim to maintain the following invariants, which
-- should be sufficient:
--
-- (1) We do not retain any 'EntityID's between iterations of the game loop.
-- (2) We never remove entities within an iteration of the game loop; instead,
-- they are removed at the very end of each iteration, after all uses of any
-- 'EntityID's.
--
-- We manually enforce the first invariant. This function helps us enforce the
-- second invariant. In the game loop, this function should be called exactly
-- once, and all uses of 'EntityID's should take place in the action given to
-- this function.
finallyRemoveEntities ::
  GameParams ->
  Time ->
  GameStateCore s ->
  ((SEntityID AnyEntity -> ST s ()) -> ST s a) ->
  ST s a
finallyRemoveEntities params time core action = do
  -- Using a set ensures that each entity is removed at most once (assuming
  -- that the above-mentioned invariants hold).
  toDelete <- newSTRef WIS.empty
  let markForRemoval = modifySTRef' toDelete . WIS.insert
  result <- action markForRemoval
  readSTRef toDelete
    >>= traverse_ (removeEntity params core time) . WIS.toList
  pure result

removeEntity ::
  GameParams -> GameStateCore s -> Time -> SEntityID AnyEntity -> ST s ()
removeEntity params core time eid =
  case refineEntityID eid of
    OverseerID overseerID -> removeOverseer params core time overseerID
    DroneID droneID -> removeDrone core time droneID
    SoftObstacleID softObstacleID -> removeSoftObstacle core softObstacleID
    PsiStormID psiStormID -> removePsiStorm core psiStormID
    FlagID flagID -> removeFlag core flagID
    TeamBaseID teamBaseID -> removeTeamBase core teamBaseID
    BaseFlagID baseFlagID -> removeBaseFlag core baseFlagID
    ExplosionID explosionID -> removeExplosion core explosionID
    SpawnAreaID spawnAreaID -> removeSpawnArea core spawnAreaID
    PowerupID powerupID -> removePowerup core time powerupID

-- | Remove all `ActorID`s from the data associated with an entity, either by
-- modifying its data or removing the entity outright. Should be idempotent.
dissociateActorEntity ::
  GameParams ->
  GameStateCore s ->
  Time ->
  SEntityID MaybeActorEntity ->
  ST s ()
dissociateActorEntity params core time playerEntityID =
  case refineEntityID playerEntityID of
    OverseerID overseerID ->
      -- Dissociation not implemented yet, but we need to get rid of the
      -- `ActorID` reference, so overseers currently cannot exist without a
      -- controlling actor
      removeOverseer params core time overseerID
    DroneID droneID -> dissociateActorDrone params core time droneID
    PsiStormID psiStormID -> dissociateActorPsiStorm core psiStormID

-- Overseer entities

-- | This function should be the only way to create overseer entities.
respawnOverseers ::
  GameParams ->
  MWC.Gen s ->
  GameStateCore s ->
  Time ->
  GamePhase ->
  ST s ()
respawnOverseers params gen core time gamePhase =
  -- Check for respawns only periodically
  when (time `Time.atMultipleOf` Ticks 6) $ do
    case gamePhase of
      GamePhase_Intermission{} ->
        pure ()
      GamePhase_Game{} -> do
        let ovRef = gsc_actors core
        pids <- WIM.keys <$> readSTRef ovRef
        traverse_ (addOverseer params time core gen) pids

-- This function should only be used with 'respawnOverseers'.
addOverseer ::
  GameParams -> Time -> GameStateCore s -> Gen s -> ActorID -> ST s ()
addOverseer params time core gen actorID = do
  PlayerStatus team playerClass overseerStatus _cooldonws <-
    expectJust "addOverseer: ActorID not found" . WIM.lookup actorID
      <$> readSTRef (gsc_actors core)
  case overseerStatus of
    OverseerAlive{} -> pure ()
    OverseerDead deathTime respawnTime _ ->
      when (time >= respawnTime) $ do
        overseerID <- borrowEntityID @Overseer (gsc_entityIDPool core)

        modifySTRef' (gsc_actors core) $
          ix actorID
            . field @"ps_overseerStatus"
            .~ OverseerAlive overseerID time

        let droneLimit = getClassParam playerClass (gp_overseerDroneLimit params)

        runCoreM $ do
          let Stores{..} = gsc_dataStores core
          insert store_team overseerID team
          CoreM (randomOverseerSpawnPosition params gen playerClass team)
            >>= \pos ->
              insert store_dynamics overseerID $
                overseer_newDynamicsComponent params playerClass pos
          insert store_combat overseerID $
            overseer_newCombatComponent params playerClass time deathTime
          Minion.registerMaster store_masterMinions overseerID droneLimit
          insert store_flag overseerID $ overseer_newFlagComponent time
          insert store_actorEntity overseerID actorID
          insert store_playerClass overseerID playerClass

        -- Create drones
        replicateM_ droneLimit $
          addDrone
            params
            time
            (gp_playerRespawnDroneInvulnerabilityTicks (gp_combat params))
            core
            gen
            overseerID
            (Just deathTime)
            GroupDroneSpawn

-- Should be idempotent
removeOverseer ::
  GameParams -> GameStateCore s -> Time -> EntityID Overseer -> ST s ()
removeOverseer params core time overseerID = do
  let Stores{..} = gsc_dataStores core
  -- We must check for the absence of a 'ActorID', since the overseer may
  -- already be deleted.
  mActorID <- lookup store_actorEntity overseerID
  for_ mActorID $ \pid -> do
    -- Get components before removal
    hasFlag <- Flag.readHasFlag store_flag overseerID
    position <- Dynamics.readPos store_dynamics overseerID
    team <- sureLookup store_team overseerID

    -- Remove components
    droneIDs <- runCoreM $ do
      delete store_team overseerID
      delete store_dynamics overseerID
      delete store_combat overseerID
      droneIDs <- Minion.unregisterMaster store_masterMinions overseerID
      delete store_flag overseerID
      delete store_actorEntity overseerID
      delete store_playerClass overseerID
      pure droneIDs

    -- Drop flag
    when hasFlag $ do
      -- Overseers carry the flags of the opposite team
      let flagTeam = oppositeTeam team
      void $ addFlag params core time flagTeam position

    -- Set overseer status
    modifySTRef' (gsc_actors core) $
      over (ix pid . field @"ps_overseerStatus") $ \case
        OverseerAlive{} ->
          let respawnDelay = gp_overseerRespawnDelay (gp_combat params)
              respawnTime = addTicks respawnDelay time
          in  OverseerDead time respawnTime position
        ovDead@OverseerDead{} -> ovDead

    -- Destroy drones
    traverse_ (dissociateOverseerDrone params core time) droneIDs

    returnEntityID (gsc_entityIDPool core) (subEntityID overseerID)

overseer_newDynamicsComponent ::
  GameParams -> PlayerClass -> Fixed2 -> DynamicsComponent
overseer_newDynamicsComponent params playerClass pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius =
          getClassParam playerClass (gp_overseerRadius (gp_dynamics params))
      , pdyn_mass =
          C.massPerMaxHealth
            * getClassParam playerClass (gp_overseerMaxHealth (gp_combat params))
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }

overseer_newCombatComponent ::
  GameParams -> PlayerClass -> Time -> Time -> CombatComponent
overseer_newCombatComponent params playerClass time deathTime =
  CombatComponent
    { com_currentHealth = min maxHealth $ maxHealth * respawnHealthFraction
    , com_maxHealth = maxHealth
    , com_healthRegenType = HealthRegenQuadratic fullRegenTicks
    , com_invulnerableUntil =
        flip addTicks time $
          gp_playerRespawnOverseerInvulnerabilityTicks (gp_combat params)
    , com_lastRegenInterruptTime = deathTime
    , com_lastDamageTime = deathTime
    , com_lastDamageAmount = 0
    , com_shields = 0
    }
  where
    maxHealth =
      getClassParam playerClass (gp_overseerMaxHealth (gp_combat params))
    fullRegenTicks =
      getClassParam
        playerClass
        (gp_quadraticHealthFullRegenTicks (gp_combat params))
    respawnHealthFraction =
      let baseFraction =
            gp_playerRespawnOverseerBonusHealthFraction (gp_combat params)
          regenFraction =
            sq $
              fromIntegral (time `diffTime` deathTime)
                / fromIntegral fullRegenTicks
          sq x = x * x
      in  min 1 $ baseFraction + regenFraction

overseer_newFlagComponent :: Time -> FlagComponent
overseer_newFlagComponent time =
  FlagComponent
    { flag_hasFlag = False
    , flag_time = time
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = prevTime
    , flag_conditionLast = prevTime
    }
  where
    -- an arbitrary time not too far in the past
    prevTime = addTicks (-10 * fromIntegral C.tickRate_hz) time

-- Drone entities

data DroneSpawnType = SingleDroneSpawn | GroupDroneSpawn

addDrone ::
  GameParams ->
  Time ->
  Ticks ->
  GameStateCore s ->
  Gen s ->
  EntityID Overseer ->
  Maybe Time ->
  DroneSpawnType ->
  ST s ()
addDrone params time invulnTicks core gen overseerID mDeathTime spawnType = do
  let Stores{..} = gsc_dataStores core
  -- Inherit overseer attributes
  overseerTeam <- sureLookup store_team overseerID
  overseerPlayerClass <- sureLookup store_playerClass overseerID
  (overseerPos, overseerVel) <- Dynamics.readPosVel store_dynamics overseerID
  actorID <- sureLookup store_actorEntity overseerID
  droneClass <-
    ps_playerClass
      . fromMaybe (error "addDrone: missing player status for actorID")
      . WIM.lookup actorID
      <$> readSTRef (gsc_actors core)

  -- Get new EntityID
  droneID <- borrowEntityID @Drone (gsc_entityIDPool core)

  -- Register team component
  runCoreM $ insert store_team droneID overseerTeam

  -- Register dynamics component
  offsetDir <- Fixed.angle . Fixed.fromDouble <$> uniformR @Double (0, 2 * pi) gen
  let spawnRadius = 8 -- smaller spawn radius --> more drone overlap --> more repulsion at overseer spawn
      pos = overseerPos + Fixed.map (* spawnRadius) offsetDir
      vel =
        case spawnType of
          SingleDroneSpawn -> overseerVel
          GroupDroneSpawn -> overseerVel + Fixed.map (* 2.5) offsetDir
  runCoreM $
    insert
      store_dynamics
      droneID
      (drone_newDynamicsComponent params droneClass pos vel)

  runCoreM $ do
    -- Register combat component
    insert store_combat droneID $
      drone_newCombatComponent
        params
        droneClass
        time
        invulnTicks
        mDeathTime

    -- Register minion component
    Minion.registerMinion
      store_masterMinions
      droneID
      (MinionComponent overseerID time)

    -- Register actor entity component
    insert store_actorEntity droneID actorID

    -- Register player classs component
    insert store_playerClass droneID overseerPlayerClass

-- Should be idempotent
removeDrone :: GameStateCore s -> Time -> EntityID Drone -> ST s ()
removeDrone core time droneID = do
  let Stores{..} = gsc_dataStores core
      eid = subEntityID droneID
  runCoreM $ do
    delete store_team droneID
    delete store_dynamics droneID
    delete store_combat droneID
    Minion.unregisterMinion store_masterMinions time droneID
    delete store_actorEntity droneID
    delete store_playerClass droneID

  returnEntityID (gsc_entityIDPool core) eid

-- Modify a drone when its actor leaves the game. Should be idempotent.
dissociateActorDrone ::
  GameParams -> GameStateCore s -> Time -> EntityID Drone -> ST s ()
dissociateActorDrone params core time droneID = do
  dissociateOverseerDrone params core time droneID
  let Stores{..} = gsc_dataStores core
  -- Remove actor entity component
  runCoreM $ delete store_actorEntity droneID

-- Modify a drone when its overseer dies. Should be idempotent.
dissociateOverseerDrone ::
  GameParams -> GameStateCore s -> Time -> EntityID Drone -> ST s ()
dissociateOverseerDrone params core time droneID = do
  let Stores{..} = gsc_dataStores core
  -- Unregister as a minion
  runCoreM $ Minion.unregisterMinion store_masterMinions time droneID
  -- Set health decay
  Combat.setHealthRegenType store_combat droneID $
    HealthRegenLinear $
      negate $
        gp_disassociatedDroneDecayFactor params -- set negative health regen

drone_newDynamicsComponent ::
  GameParams -> PlayerClass -> Fixed2 -> Fixed2 -> DynamicsComponent
drone_newDynamicsComponent params playerClass pos vel =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius =
          getClassParam playerClass (gp_droneRadius (gp_dynamics params))
      , pdyn_mass =
          C.massPerMaxHealth
            * getClassParam playerClass (gp_droneMaxHealth (gp_combat params))
      , pdyn_pos = pos
      , pdyn_vel = vel
      }

drone_newCombatComponent ::
  GameParams ->
  PlayerClass ->
  Time ->
  Ticks ->
  Maybe Time ->
  CombatComponent
drone_newCombatComponent params playerClass time invulnTicks mDeathTime =
  CombatComponent
    { com_currentHealth = min maxHealth respawnHealth
    , com_maxHealth = maxHealth
    , com_healthRegenType =
        HealthRegenExponential $
          gp_droneHealthRegenPeriodFactor (gp_combat params)
    , com_invulnerableUntil = addTicks invulnTicks time
    , com_lastRegenInterruptTime = initialTime
    , com_lastDamageTime = initialTime
    , com_lastDamageAmount = 0
    , com_shields = 0
    }
  where
    combatParams = gp_combat params
    maxHealth = getClassParam playerClass (gp_droneMaxHealth combatParams)
    respawnHealth = case mDeathTime of
      Nothing -> gp_droneRespawnHealth (gp_combat params)
      Just deathTime ->
        let timeConstantTicks = gp_droneHealthRegenTimeConstantTicks combatParams
            ticksSinceDeath = fromIntegral $ time `diffTime` deathTime
        in  maxHealth * (1 - exp (ticksSinceDeath / timeConstantTicks))

-- Soft obstacle entities

addSoftObstacle ::
  GameParams ->
  GameStateCore s ->
  Maybe Fixed ->
  Fixed2 ->
  Maybe Fixed ->
  ST s ()
addSoftObstacle params core velAngle pos mRadius = do
  let Stores{..} = gsc_dataStores core
  obstacleID <- borrowEntityID @SoftObstacle (gsc_entityIDPool core)

  runCoreM $
    insert store_dynamics obstacleID $
      softObstacle_newDynamicsComponent params pos velAngle mRadius

-- Should be idempotent
removeSoftObstacle :: GameStateCore s -> EntityID SoftObstacle -> ST s ()
removeSoftObstacle core obstacleID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ delete store_dynamics obstacleID

  returnEntityID (gsc_entityIDPool core) (subEntityID obstacleID)

softObstacle_newDynamicsComponent ::
  GameParams ->
  Fixed2 ->
  Maybe Fixed ->
  Maybe Fixed ->
  DynamicsComponent
softObstacle_newDynamicsComponent params pos mVelAngle mRadius =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = obstacleRadius
      , pdyn_mass =
          square (obstacleRadius * C.sqrtDensity) * softObstacleDenstyFactor
      , pdyn_pos = pos
      , pdyn_vel =
          case mVelAngle of
            Nothing -> 0
            Just velAngle ->
              Fixed.map (* gp_softObstacleAverageSpeed (gp_dynamics params)) $
                Fixed2 (cos velAngle) (sin velAngle)
      }
  where
    obstacleRadius =
      fromMaybe (gp_softObstacleRadius (gp_dynamics params)) mRadius
    softObstacleDenstyFactor =
      gp_softObstacleDensityFactor (gp_dynamics params)

-- Flag entities

addFlag :: GameParams -> GameStateCore s -> Time -> Team -> Fixed2 -> ST s ()
addFlag params core time team pos = do
  flagID <- borrowEntityID @Flag (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_team flagID team
    insert store_dynamics flagID $
      flag_newDynamicsComponent params pos
    insert store_flag flagID $ flag_newFlagComponent time

-- Should be idempotent
removeFlag :: GameStateCore s -> EntityID Flag -> ST s ()
removeFlag core flagID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_team flagID
    delete store_dynamics flagID
    delete store_flag flagID

  returnEntityID (gsc_entityIDPool core) (subEntityID flagID)

flag_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
flag_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_flagRadius (gp_dynamics params)
      , pdyn_mass = square $ gp_flagRadius (gp_dynamics params) * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }

flag_newFlagComponent :: Time -> FlagComponent
flag_newFlagComponent time =
  FlagComponent
    { flag_hasFlag = True
    , flag_time = time
    , flag_ticks = 0
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- Base flag entities

addBaseFlag :: GameParams -> GameStateCore s -> Team -> Fixed2 -> ST s ()
addBaseFlag params core team pos = do
  flagID <- borrowEntityID @BaseFlag (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_team flagID team
    insert store_dynamics flagID $
      baseFlag_newDynamicsComponent params pos
    insert store_flag flagID baseFlag_newFlagComponent

-- Should be idempotent
removeBaseFlag :: GameStateCore s -> EntityID BaseFlag -> ST s ()
removeBaseFlag core flagID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_team flagID
    delete store_dynamics flagID
    delete store_flag flagID

  returnEntityID (gsc_entityIDPool core) (subEntityID flagID)

baseFlag_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
baseFlag_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = radius
      , pdyn_mass = square $ radius * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
  where
    radius = gp_flagRadius $ gp_dynamics params

baseFlag_newFlagComponent :: FlagComponent
baseFlag_newFlagComponent =
  FlagComponent
    { flag_hasFlag = True
    , flag_time = initialTime -- dummy value
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- TeamBase entities

addTeamBase :: GameParams -> GameStateCore s -> Team -> Fixed2 -> ST s ()
addTeamBase params core team pos = do
  teamBaseID <- borrowEntityID @TeamBase (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_team teamBaseID team
    insert store_dynamics teamBaseID $
      teamBase_newDynamicsComponent params pos
    insert store_flag teamBaseID teamBase_newFlagComponent

-- Should be idempotent
removeTeamBase :: GameStateCore s -> EntityID TeamBase -> ST s ()
removeTeamBase core teamBaseID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_team teamBaseID
    delete store_dynamics teamBaseID
    delete store_flag teamBaseID

  returnEntityID (gsc_entityIDPool core) (subEntityID teamBaseID)

teamBase_newDynamicsComponent :: GameParams -> Fixed2 -> DynamicsComponent
teamBase_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = baseRadius
      , pdyn_mass = square $ baseRadius * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
  where
    baseRadius = gp_teamBaseRadius (gp_dynamics params)

teamBase_newFlagComponent :: FlagComponent
teamBase_newFlagComponent =
  FlagComponent
    { flag_hasFlag = False
    , flag_time = initialTime -- dummy value
    , flag_ticks = 0 -- dummy value
    , flag_conditionStart = initialTime
    , flag_conditionLast = initialTime
    }

-- Psi storm entities

addPsiStorm ::
  CastParams ->
  GameStateCore s ->
  Time ->
  Maybe ActorID ->
  Fixed2 ->
  ST s ()
addPsiStorm params core time mActorID targetPos = do
  psiStormID <- borrowEntityID @PsiStorm (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_dynamics psiStormID $ psiStorm_newDynamicsComponent targetPos
    insert store_transitory psiStormID $
      addTicks (gp_psiStormLifetimeTicks params) time
    for_ mActorID $ insert store_actorEntity psiStormID
    insert store_psiStorm psiStormID time

-- Should be idempotent
removePsiStorm :: GameStateCore s -> EntityID PsiStorm -> ST s ()
removePsiStorm core psiStormID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_dynamics psiStormID
    delete store_transitory psiStormID
    delete store_actorEntity psiStormID
    delete store_psiStorm psiStormID

  returnEntityID (gsc_entityIDPool core) (subEntityID psiStormID)

-- Should be idempotent
dissociateActorPsiStorm :: GameStateCore s -> EntityID PsiStorm -> ST s ()
dissociateActorPsiStorm core psiStormID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ delete store_actorEntity psiStormID

psiStorm_newDynamicsComponent :: Fixed2 -> DynamicsComponent
psiStorm_newDynamicsComponent pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = 0 -- initial radius
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }

-- Explosion entities

addExplosion ::
  DynamicsParams ->
  GameStateCore s ->
  Time ->
  Fixed2 ->
  ST s ()
addExplosion params core time pos = do
  explosionID <- borrowEntityID @Explosion (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_dynamics explosionID $
      explosion_newDynamicsComponent params pos
    insert store_transitory explosionID $
      addTicks (gp_overseerExplosionDuration params) time

-- Should be idempotent
removeExplosion :: GameStateCore s -> EntityID Explosion -> ST s ()
removeExplosion core explosionID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_dynamics explosionID
    delete store_transitory explosionID

  returnEntityID (gsc_entityIDPool core) (subEntityID explosionID)

explosion_newDynamicsComponent :: DynamicsParams -> Fixed2 -> DynamicsComponent
explosion_newDynamicsComponent params pos =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_overseerExplosionRadius params
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }

-- Team spawn entities

addSpawnArea ::
  GameParams ->
  GameStateCore s ->
  Team ->
  ST s ()
addSpawnArea params core team = do
  spawnAreaID <- borrowEntityID @SpawnArea (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_dynamics spawnAreaID $
      spawnArea_newDynamicsComponent params team
    insert store_team spawnAreaID team

-- Should be idempotent
removeSpawnArea :: GameStateCore s -> EntityID SpawnArea -> ST s ()
removeSpawnArea core spawnAreaID = do
  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    delete store_dynamics spawnAreaID
    delete store_team spawnAreaID

  returnEntityID (gsc_entityIDPool core) (subEntityID spawnAreaID)

spawnArea_newDynamicsComponent :: GameParams -> Team -> DynamicsComponent
spawnArea_newDynamicsComponent params team =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = gp_spawnAreaRadius (gp_dynamics params)
      , pdyn_mass = 1 -- dummy value; not used
      , pdyn_pos = pos
      , pdyn_vel = 0
      }
  where
    pos = spawnAreaLocation params team

-- Powerup entities

respawnBoundPowerups :: GameParams -> Time -> GameStateCore s -> ST s ()
respawnBoundPowerups params time core =
  when (gp_enableShieldPowerupSpawners params) $ do
    initMapPowerups <- readSTRef (gsc_mapPowerups core)
    let powerupSpawnLocation' = mapPowerupSpawnLocation (gp_dynamics params)
    !finalMapPowerups <-
      flip traverseWithKeyMapPowerupStatuses initMapPowerups $
        \powerupLoc powerupStatus ->
          case powerupStatus of
            MapPowerupSpawned _ ->
              pure powerupStatus -- unchanged
            MapPowerupConsumed consumedTime -> do
              let respawnTime =
                    addTicks (gp_shieldPowerupRespawnPeriod params) consumedTime
              if time >= respawnTime
                then do
                  let pos = powerupSpawnLocation' powerupLoc
                      shieldHealth = gp_shieldPowerupHealth params
                  addPowerup' params time core (Just powerupLoc) pos shieldHealth Nothing
                  pure $! MapPowerupSpawned time
                else pure powerupStatus -- unchanged
    writeSTRef (gsc_mapPowerups core) finalMapPowerups

addFreePowerup ::
  GameParams ->
  Time ->
  GameStateCore s ->
  Fixed2 ->
  Fixed ->
  Ticks ->
  ST s ()
addFreePowerup params time core pos shieldHealth lifetime =
  addPowerup' params time core Nothing pos shieldHealth (Just lifetime)

addPowerup' ::
  GameParams ->
  Time ->
  GameStateCore s ->
  Maybe MapPowerupLocation ->
  Fixed2 ->
  Fixed ->
  Maybe Ticks ->
  ST s ()
addPowerup' params time core locationMaybe pos shieldHealth lifetimeMaybe = do
  powerupID <- borrowEntityID @Powerup (gsc_entityIDPool core)

  let Stores{..} = gsc_dataStores core
  runCoreM $ do
    insert store_dynamics powerupID $
      powerup_newDynamicsComponent params pos shieldHealth
    insert
      store_powerup
      powerupID
      PowerupComponent
        { pc_location = locationMaybe
        , pc_consumed = False
        , pc_shieldHealth = shieldHealth
        }
    for_ lifetimeMaybe $ \lifetime ->
      insert store_transitory powerupID $
        addTicks lifetime time

-- Should be idempotent
removePowerup :: GameStateCore s -> Time -> EntityID Powerup -> ST s ()
removePowerup core time powerupID = do
  let Stores{..} = gsc_dataStores core

  Powerup.readPowerupLocation store_powerup powerupID >>= traverse_ \loc ->
    modifySTRef'
      (gsc_mapPowerups core)
      (setMapPowerupStatus loc (MapPowerupConsumed time))

  runCoreM $ do
    delete store_dynamics powerupID
    delete store_powerup powerupID
    delete store_transitory powerupID

  returnEntityID (gsc_entityIDPool core) (subEntityID powerupID)

powerup_newDynamicsComponent ::
  GameParams -> Fixed2 -> Fixed -> DynamicsComponent
powerup_newDynamicsComponent params pos shieldHealth =
  newDynamicsComponent
    ProtoDynamicsComponent
      { pdyn_radius = radius
      , pdyn_mass = square $ radius * C.sqrtDensity
      , pdyn_pos = pos
      , pdyn_vel = Fixed2 0 0
      }
  where
    radius = shieldRadius (gp_dynamics params) shieldHealth

--------------------------------------------------------------------------------
-- Overseer respawn helpers

-- | Return a random spawn position for a new overseer based on its team.
randomOverseerSpawnPosition ::
  GameParams -> MWC.Gen s -> PlayerClass -> Team -> ST s Fixed2
randomOverseerSpawnPosition params gen playerClass team =
  (+ spawnAreaLocation params team) . Fixed.map (* bufferedSpawnAreaRadius)
    <$> uniformUnitDisc gen
  where
    spawnAreaRadius = gp_spawnAreaRadius (gp_dynamics params)
    overseerRadius =
      getClassParam playerClass (gp_overseerRadius (gp_dynamics params))
    buffer = 1 + overseerRadius
    bufferedSpawnAreaRadius = spawnAreaRadius - buffer

--------------------------------------------------------------------------------
-- Misc. helpers

square :: (Num a) => a -> a
square x = x * x
