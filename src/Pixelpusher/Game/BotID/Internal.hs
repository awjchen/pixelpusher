module Pixelpusher.Game.BotID.Internal (
  BotID (..),
) where

import Data.Hashable (Hashable)
import Data.Serialize (Serialize)
import Data.Word (Word8)
import Foreign.Storable (Storable)

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS

-- | Identifiers for bot actors within a specific game.
newtype BotID = BotID {_getBotID :: Word8}
  deriving newtype (Eq, Ord, Hashable, Storable, Serialize)

instance Show BotID where
  show (BotID bid) = show bid

instance WIS.IsInt BotID where
  toInt (BotID i) = WIS.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIS.makeIdentity . BotID . fromIntegral
  {-# INLINE fromInt #-}

instance WIM.IsInt BotID where
  toInt (BotID i) = WIM.makeIdentity (fromIntegral i)
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . BotID . fromIntegral
  {-# INLINE fromInt #-}
