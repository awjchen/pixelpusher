{-# LANGUAGE PatternSynonyms #-}

module Pixelpusher.Game.PlayerControls (
  PlayerControls (..),
  defaultControls,
  ButtonStates,
  defaultButtonStates,
  PlayerControlsTransport,
  defaultControlsTransport,
  getControlsTransportButtons,
  makeControlsTransport,
  fromControlsTransport,
  control_flag,
  control_move,
  control_drone,
  control_psiStorm,
  control_overseerDash,
  FlagCommand (FlagNeutral, FlagDrop),
  MoveCommand (
    MoveNeutral,
    MoveE,
    MoveNE,
    MoveN,
    MoveNW,
    MoveW,
    MoveSW,
    MoveS,
    MoveSE
  ),
  arrowKeyDirection,
  arrowKeyDirection',
  DroneCommand (DroneNeutral, DroneAttraction, DroneRepulsion),
  PsiStormCommand (PsiStormOff, PsiStormOn),
  OverseerDashCommand (OverseerDashOff, OverseerDashOn),
) where

import Data.Generics.Wrapped
import Data.Int (Int16)
import Data.Serialize (Serialize)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Float (Float2)
import Pixelpusher.Custom.Float qualified as Float
import Pixelpusher.Custom.Linear (V2 (V2))
import Pixelpusher.Custom.WrappedIntMap qualified as WIM (IsInt)
import Pixelpusher.Game.PlayerControls.Internal (
  ButtonStates (..),
  defaultButtonStates,
 )

--------------------------------------------------------------------------------

-- | The controls available to a player at each tick
data PlayerControls = PlayerControls
  { control_buttons :: ButtonStates
  , control_worldPos :: Fixed2
  -- ^ Mouse position in the game world
  }
  deriving stock (Generic, Eq)
  deriving anyclass (Serialize)

defaultControls :: PlayerControls
defaultControls =
  PlayerControls
    { control_buttons = defaultButtonStates
    , control_worldPos = Fixed2 0 0
    }

arrowKeyDirection :: PlayerControls -> Fixed2
arrowKeyDirection control =
  arrowKeyDirection' $ control ^. to control_buttons . control_move

arrowKeyDirection' :: MoveCommand -> Fixed2
arrowKeyDirection' = \case
  MoveNeutral -> Fixed2 0 0
  MoveE -> Fixed2 1 0
  MoveNE -> Fixed2 invSqrt2 invSqrt2
  MoveN -> Fixed2 0 1
  MoveNW -> Fixed2 (-invSqrt2) invSqrt2
  MoveW -> Fixed2 (-1) 0
  MoveSW -> Fixed2 (-invSqrt2) (-invSqrt2)
  MoveS -> Fixed2 0 (-1)
  MoveSE -> Fixed2 invSqrt2 (-invSqrt2)

-- Helper
invSqrt2 :: Fixed
invSqrt2 = Fixed.fromDouble $ recip $ sqrt 2

--------------------------------------------------------------------------------

-- | A version of the player controls to be serialized and sent over the
-- network
data PlayerControlsTransport = PlayerControlsTransport
  { controlTransport_buttons :: ButtonStates
  , controlTransport_worldPos4 :: V2 Int16
  -- ^ Four times the mouse position in the game world
  }
  deriving stock (Generic, Eq, Show)
  deriving anyclass (Serialize)

defaultControlsTransport :: PlayerControlsTransport
defaultControlsTransport =
  PlayerControlsTransport
    { controlTransport_buttons = defaultButtonStates
    , controlTransport_worldPos4 = V2 0 0
    }

getControlsTransportButtons :: PlayerControlsTransport -> ButtonStates
getControlsTransportButtons = controlTransport_buttons

-- | Smart constructor
makeControlsTransport :: ButtonStates -> Float2 -> PlayerControlsTransport
makeControlsTransport buttons worldPos =
  PlayerControlsTransport
    { controlTransport_buttons = buttons
    , controlTransport_worldPos4 =
        round . (* 4) <$> Float.toVec2 worldPos
    }

fromControlsTransport :: PlayerControlsTransport -> PlayerControls
fromControlsTransport controlsTransport =
  PlayerControls
    { control_buttons = controlTransport_buttons controlsTransport
    , control_worldPos =
        Fixed.fromV2Floats $
          (* 0.25) . fromIntegral
            <$> controlTransport_worldPos4 controlsTransport
    }

--------------------------------------------------------------------------------

control_flag :: Lens' ButtonStates FlagCommand
control_flag = control_generic flagDivisor numFlagCommands . _Wrapped

control_move :: Lens' ButtonStates MoveCommand
control_move = control_generic moveDivisor numMoveCommands . _Wrapped

control_drone :: Lens' ButtonStates DroneCommand
control_drone =
  control_generic droneDivisor numDroneCommands . _Wrapped

control_psiStorm :: Lens' ButtonStates PsiStormCommand
control_psiStorm =
  control_generic psiStormDivisor numPsiStormCommands . _Wrapped

control_overseerDash :: Lens' ButtonStates OverseerDashCommand
control_overseerDash =
  control_generic overseerDashDivisor numOverseerDashCommands . _Wrapped

control_generic :: Word8 -> Word8 -> Lens' ButtonStates Word8
control_generic divisor range f (ButtonStates x) =
  let (quot1, rem1) = x `quotRem` divisor
      (quot2, rem2) = quot1 `quotRem` range
  in  f rem2 <&> \rem2' ->
        ButtonStates $ (quot2 * range + rem2') * divisor + rem1

flagDivisor :: Word8
flagDivisor = 1
{-# INLINE flagDivisor #-}

moveDivisor :: Word8
moveDivisor = flagDivisor * numFlagCommands
{-# INLINE moveDivisor #-}

droneDivisor :: Word8
droneDivisor = moveDivisor * numMoveCommands
{-# INLINE droneDivisor #-}

psiStormDivisor :: Word8
psiStormDivisor = droneDivisor * numDroneCommands
{-# INLINE psiStormDivisor #-}

overseerDashDivisor :: Word8
overseerDashDivisor = psiStormDivisor * numOverseerDashCommands
{-# INLINE overseerDashDivisor #-}

--------------------------------------------------------------------------------

newtype FlagCommand = FlagCommand Word8
  deriving stock (Generic)
  deriving newtype (Eq)

numFlagCommands :: Word8
numFlagCommands = 2
{-# INLINE numFlagCommands #-}

{-# COMPLETE FlagNeutral, FlagDrop :: FlagCommand #-}

pattern FlagNeutral :: FlagCommand
pattern FlagNeutral = FlagCommand 0
pattern FlagDrop :: FlagCommand
pattern FlagDrop = FlagCommand 1

instance Show FlagCommand where
  show FlagNeutral = "FlagNeutral"
  show FlagDrop = "FlagDrop"

--------------------------------------------------------------------------------

newtype MoveCommand = MoveCommand Word8
  deriving stock (Generic)
  deriving newtype (Eq, Serialize, Show, WIM.IsInt)

numMoveCommands :: Word8
numMoveCommands = 9
{-# INLINE numMoveCommands #-}

{-# COMPLETE MoveNeutral, MoveE, MoveNE, MoveN, MoveNW, MoveW, MoveSW, MoveS, MoveSE :: MoveCommand #-}
pattern MoveNeutral :: MoveCommand
pattern MoveNeutral = MoveCommand 0
pattern MoveE :: MoveCommand
pattern MoveE = MoveCommand 1
pattern MoveNE :: MoveCommand
pattern MoveNE = MoveCommand 2
pattern MoveN :: MoveCommand
pattern MoveN = MoveCommand 3
pattern MoveNW :: MoveCommand
pattern MoveNW = MoveCommand 4
pattern MoveW :: MoveCommand
pattern MoveW = MoveCommand 5
pattern MoveSW :: MoveCommand
pattern MoveSW = MoveCommand 6
pattern MoveS :: MoveCommand
pattern MoveS = MoveCommand 7
pattern MoveSE :: MoveCommand
pattern MoveSE = MoveCommand 8

instance MWC.Uniform MoveCommand where
  uniformM gen = MoveCommand <$> MWC.uniformRM (0, numMoveCommands - 1) gen

--------------------------------------------------------------------------------

newtype DroneCommand = DroneCommand Word8
  deriving stock (Generic)
  deriving newtype (Eq, Serialize)

numDroneCommands :: Word8
numDroneCommands = 3
{-# INLINE numDroneCommands #-}

{-# COMPLETE DroneNeutral, DroneAttraction, DroneRepulsion :: DroneCommand #-}
pattern DroneNeutral :: DroneCommand
pattern DroneNeutral = DroneCommand 0
pattern DroneAttraction :: DroneCommand
pattern DroneAttraction = DroneCommand 1
pattern DroneRepulsion :: DroneCommand
pattern DroneRepulsion = DroneCommand 2

--------------------------------------------------------------------------------

newtype PsiStormCommand = PsiStormCommand Word8
  deriving stock (Generic)
  deriving newtype (Eq)

numPsiStormCommands :: Word8
numPsiStormCommands = 2
{-# INLINE numPsiStormCommands #-}

{-# COMPLETE PsiStormOff, PsiStormOn :: PsiStormCommand #-}
pattern PsiStormOff :: PsiStormCommand
pattern PsiStormOff = PsiStormCommand 0
pattern PsiStormOn :: PsiStormCommand
pattern PsiStormOn = PsiStormCommand 1

--------------------------------------------------------------------------------

newtype OverseerDashCommand = OverseerDashCommand Word8
  deriving stock (Generic)
  deriving newtype (Eq, Serialize)

numOverseerDashCommands :: Word8
numOverseerDashCommands = 2
{-# INLINE numOverseerDashCommands #-}

{-# COMPLETE OverseerDashOff, OverseerDashOn :: OverseerDashCommand #-}
pattern OverseerDashOff :: OverseerDashCommand
pattern OverseerDashOff = OverseerDashCommand 0
pattern OverseerDashOn :: OverseerDashCommand
pattern OverseerDashOn = OverseerDashCommand 1
