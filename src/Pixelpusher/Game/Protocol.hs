-- | Game data transferred between server and client
module Pixelpusher.Game.Protocol (
  ServerGameMsg (..),
  ServerGameData (..),
  GameStateUpdate (..),
  ClientGameMsg (..),
  ClientInitialMsg (..),
  ClientRole (..),
  PlayerIDMap32,
  getPlayerIDMap32,
  makePlayerIDMap32,
) where

import Control.Monad (replicateM)
import Data.Bits (popCount, setBit, zeroBits)
import Data.ByteString (ByteString)
import Data.List (foldl')
import Data.Serialize (Serialize)
import Data.Serialize qualified as Serialize
import Data.Word (Word32)
import GHC.Generics

import Pixelpusher.Custom.SmallList (SmallList)
import Pixelpusher.Custom.Util (listSetBits)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Debug (DebugCommand)
import Pixelpusher.Game.GameCommand (GameCommand)
import Pixelpusher.Game.PlayerClass (PlayerClass)
import Pixelpusher.Game.PlayerControls (PlayerControlsTransport)
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..))
import Pixelpusher.Game.State (GameStateSnapshot)
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

-- | Messages from servers to clients
newtype ServerGameMsg = ServerGameData {unServerGameMsg :: ServerGameData}
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ServerGameData
  = ServerSnapshot
      (Maybe PlayerID) -- `Just PlayerID` for players, `Nothing` for observers
      Int -- Room ID
      GameStateSnapshot
  | ServerDeltaUpdate GameStateUpdate
  deriving stock (Generic)
  deriving anyclass (Serialize)

data GameStateUpdate = GameStateUpdate
  { deltaUpdate_time :: SyncTime
  , deltaUpdate_playerControls :: PlayerIDMap32 PlayerControlsTransport
  , deltaUpdate_gameCommands :: SmallList GameCommand
  , deltaUpdate_debugCommand :: Maybe DebugCommand
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Messages from clients to the servers
data ClientGameMsg
  = ClientControls PlayerControlsTransport
  | ClientDebugCommand DebugCommand
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Parameters requested by clients
data ClientInitialMsg = ClientInitialMsg
  { cim_role :: ClientRole ByteString
  , cim_mRoomID :: Maybe Int
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ClientRole playerName
  = ClientPlayer playerName PlayerClass
  | ClientObserver
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------

-- | A newtype for defining a Binary instance on maps of PlayerIDs that takes
-- advantage of the small number of players to be more compact. Only allows
-- `PlayerID`s from 0 to 31 as keys.
newtype PlayerIDMap32 a = PlayerIDMap32
  {getPlayerIDMap32 :: WIM.IntMap PlayerID a}
  deriving stock (Eq, Show)

-- | Filters out `PlayerID` keys larger than `PlayerID 31`
makePlayerIDMap32 :: WIM.IntMap PlayerID a -> PlayerIDMap32 a
makePlayerIDMap32 =
  PlayerIDMap32 . WIM.filterWithKey (\k _ -> k <= PlayerID 31)

-- | Uses a bitfield to represent the `PlayerID` keys
instance (Serialize a) => Serialize (PlayerIDMap32 a) where
  put :: PlayerIDMap32 a -> Serialize.Put
  put (PlayerIDMap32 m) = do
    let (uids, as) = unzip $ WIM.toAscList m
        bitfield =
          foldl' setBit zeroBits $
            map (fromIntegral . getPlayerID) uids
    Serialize.put @Word32 bitfield
    mapM_ Serialize.put as

  get :: Serialize.Get (PlayerIDMap32 a)
  get = do
    bitfield <- Serialize.get @Word32
    let size = popCount bitfield
        uids = map (PlayerID . fromIntegral) $ listSetBits bitfield
    as <- replicateM size Serialize.get
    pure $ PlayerIDMap32 $ WIM.fromAscList $ zip uids as
