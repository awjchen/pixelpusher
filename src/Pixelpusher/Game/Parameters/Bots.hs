{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Bots (
  BotParams (..),
  makeBotParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Bots.AI.OverseerMoveCandidates
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters.Base.Bots
import Pixelpusher.Game.Parameters.Base.Drone
import Pixelpusher.Game.Parameters.Base.DroneDash
import Pixelpusher.Game.Parameters.Base.Overseer
import Pixelpusher.Game.Parameters.Base.OverseerDash
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time (Ticks)

data BotParams = BotParams
  { gp_enableBots :: Bool
  , gp_minBotsPerTeam :: Int
  , gp_minTeamSize :: Int
  , gp_botControlQueueLength :: Int
  , gp_botMaxPredictionTicks :: Ticks
  , gp_botAllowNonTemplarClasses :: Bool
  , gp_botAllowOverseerDash :: Bool
  , gp_botAllowPsiStorm :: Bool
  , gp_botAllowDroneDash :: Bool
  , gp_botAllowDroneRepel :: Bool
  , gp_botAllowOverseerPursuit :: Bool
  , gp_botDeprioritizeEnemyFlag :: Bool
  , gp_botLongRangeMovementPotentialScaling :: Fixed
  , gp_botDroneControlUpdateWeight :: Fixed
  , gp_overseerDragParams :: ClassParameter LinearDragParams
  , gp_droneDragParams :: ClassParameter LinearDragParams
  , gp_protoOverseerMoveCandidates ::
      ClassParameter (OverseerMoveCandidates, OverseerMoveCandidates)
  , gp_protoLongRangeOverseerMoveCandidates ::
      ClassParameter (WIM.IntMap MoveCommand LongRangeOverseerMoveCandidate)
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeBotParams ::
  CastParams ->
  BaseBotParams Fixed ->
  BaseOverseerDashParams Fixed ->
  BaseDroneDashParams Fixed ->
  BaseDroneParams Fixed ->
  BaseOverseerParams Fixed ->
  BotParams
makeBotParams
  castParams
  BaseBotParams{..}
  BaseOverseerDashParams{..}
  BaseDroneDashParams{..}
  BaseDroneParams{..}
  BaseOverseerParams{..} =
    let -- Bot level skill progression
        -- Intended level range: 1 to 9
        -- All levels: Steadily decrease latency
        controlQueueLength =
          let baseLength = max 0 (11 - bots_level)
              extraLenth = max 0 (3 - bots_level)
          in  baseLength + extraLenth
        longRangeMovementPotentialScaling =
          Fixed.linearStep 1 5 (fromIntegral bots_level)
        droneControlUpdateWeight' =
          let averageDroneLatency_controlSteps =
                1 + 2 * Fixed.linearStep 5 1 (fromIntegral bots_level)
          in  droneControlUpdateWeight averageDroneLatency_controlSteps
        allowOverseerPursuit = bots_level >= 1 || bots_forceEnableAllTechniques
        allowOverseerDash = bots_level >= 2 || bots_forceEnableAllTechniques
        allowPsiStorm = bots_level >= 3 || bots_forceEnableAllTechniques
        -- Psi-storm is the major differentiator of the templar from the zealot,
        -- so let's introduce the zealot when the templar gets it's special
        -- ability
        allowNonTemplarClasses = allowPsiStorm || bots_forceEnableAllTechniques
        allowDroneRepel = bots_level >= 4 || bots_forceEnableAllTechniques
        allowDroneDash = bots_level >= 4 || bots_forceEnableAllTechniques

        overseerDragParams_templar =
          makeLinearDragParams
            (overseer_acceleration * overseer_acceleration_factor_templar)
            (overseer_drag * overseer_drag_factor_templar)
        overseerDragParams_zealot =
          makeLinearDragParams
            (overseer_acceleration * overseer_acceleration_factor_zealot)
            (overseer_drag * overseer_drag_factor_zealot)
    in  BotParams
          { gp_enableBots = bots_enable
          , gp_minBotsPerTeam = bots_minBotsPerTeam
          , gp_minTeamSize = bots_minTeamSize
          , gp_botControlQueueLength = controlQueueLength
          , gp_botMaxPredictionTicks =
              let botLatencyTicks =
                    fromIntegral controlQueueLength * C.botControlFrequencyTicks
                  minPredictionTicksForHighLatency =
                    -- To prevent high-latency bots from circling flags and other
                    -- small targets
                    botLatencyTicks - 30
              in  max minPredictionTicksForHighLatency $
                    min C.botMaxPredictionTicks botLatencyTicks
          , gp_botAllowNonTemplarClasses = allowNonTemplarClasses
          , gp_botAllowOverseerDash = overseerDash_enable && allowOverseerDash
          , gp_botAllowPsiStorm = allowPsiStorm
          , gp_botAllowDroneDash = droneDash_enable && allowDroneDash
          , gp_botAllowDroneRepel = allowDroneRepel
          , gp_botAllowOverseerPursuit = allowOverseerPursuit
          , gp_botDeprioritizeEnemyFlag = bots_deprioritizeEnemyFlag
          , gp_botLongRangeMovementPotentialScaling = longRangeMovementPotentialScaling
          , gp_botDroneControlUpdateWeight = droneControlUpdateWeight'
          , gp_overseerDragParams =
              ClassParameter
                { cp_templar = overseerDragParams_templar
                , cp_zealot = overseerDragParams_zealot
                }
          , gp_droneDragParams =
              ClassParameter
                { cp_templar =
                    makeLinearDragParams
                      (drone_acceleration * drone_acceleration_factor_templar)
                      (drone_drag * drone_drag_factor_templar)
                , cp_zealot =
                    makeLinearDragParams
                      (drone_acceleration * drone_acceleration_factor_zealot)
                      (drone_drag * drone_drag_factor_zealot)
                }
          , gp_protoOverseerMoveCandidates =
              ClassParameter
                { cp_templar =
                    makeProtoOverseerMoveCandidates
                      overseerDragParams_templar
                      (getClassParam Templar (gp_overseerDashDeltaV castParams))
                , cp_zealot =
                    makeProtoOverseerMoveCandidates
                      overseerDragParams_zealot
                      (getClassParam Zealot (gp_overseerDashDeltaV castParams))
                }
          , gp_protoLongRangeOverseerMoveCandidates =
              ClassParameter
                { cp_templar =
                    makeProtoLongRangeOverseerMoveCandidates
                      overseerDragParams_templar
                , cp_zealot =
                    makeProtoLongRangeOverseerMoveCandidates
                      overseerDragParams_zealot
                }
          }

-- | Weight w of new inputs, where old inputs have weight (1 - w). "Average"
-- latency is proportional to (1 - w) / (w^2).
--
-- t = (1 - w) / (w^2) <==> w = (sqrt(4t + 1) - 1) / (2t)
droneControlUpdateWeight :: Fixed -> Fixed
droneControlUpdateWeight averageLatency_controlSteps =
  let t = averageLatency_controlSteps
  in  (sqrt (4 * t + 1) - 1) / (2 * t)
