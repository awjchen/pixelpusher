{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Combat (
  CombatParams (..),
  makeCombatParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Drone
import Pixelpusher.Game.Parameters.Base.Overseer
import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.ShieldPowerup
import Pixelpusher.Game.Parameters.Base.SpawnArea

data CombatParams = CombatParams
  { gp_quadraticHealthFullRegenTicks :: ClassParameter Ticks
  , gp_droneMaxHealth_base :: Fixed
  , gp_droneMaxHealth :: ClassParameter Fixed
  , gp_droneHealthRegenPeriodFactor :: Fixed
  , gp_droneHealthRegenTimeConstantTicks :: Fixed
  , gp_overseerMaxHealth_base :: Fixed
  , gp_overseerMaxHealth :: ClassParameter Fixed
  , gp_droneRespawnCheckInterval :: Ticks
  , gp_droneRespawnDelay :: Ticks
  , gp_droneRespawnHealth :: Fixed
  , gp_overseerRespawnDelay :: Ticks
  , gp_psiStormDamagePerHit :: Fixed
  , gp_playerRespawnOverseerInvulnerabilityTicks :: Ticks
  , gp_playerRespawnDroneInvulnerabilityTicks :: Ticks
  , gp_playerRespawnOverseerBonusHealthFraction :: Fixed
  , gp_unlimitedSpawnInvulnerability :: Bool
  , gp_shieldDecayPeriodFactor :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeCombatParams ::
  BaseDroneParams Fixed ->
  BaseOverseerParams Fixed ->
  BasePowerupParams Fixed ->
  BasePsiStormParams Fixed ->
  BaseSpawnAreaParams Fixed ->
  CombatParams
makeCombatParams
  BaseDroneParams{..}
  BaseOverseerParams{..}
  BasePowerupParams{..}
  BasePsiStormParams{..}
  BaseSpawnAreaParams{..} =
    CombatParams
      { gp_quadraticHealthFullRegenTicks =
          ClassParameter
            { cp_templar =
                fromIntegral overseer_health_fullRegenTime_ticks_templar
            , cp_zealot =
                fromIntegral overseer_health_fullRegenTime_ticks_zealot
            }
      , gp_droneMaxHealth_base = drone_health_max
      , gp_droneMaxHealth =
          ClassParameter
            { cp_templar =
                drone_health_max
                  * drone_health_max_factor_templar
                  * ( fromIntegral overseer_droneLimit_base
                        / fromIntegral overseer_droneLimit_templar
                    )
            , cp_zealot =
                drone_health_max
                  * drone_health_max_factor_zealot
                  * ( fromIntegral overseer_droneLimit_base
                        / fromIntegral overseer_droneLimit_zealot
                    )
            }
      , gp_droneHealthRegenPeriodFactor =
          let halfLife =
                fromIntegral drone_health_regenHalfLife_ticks
              t = fromIntegral C.healthRegenInterval
          in  exp (log 0.5 * (t / halfLife))
      , gp_droneHealthRegenTimeConstantTicks =
          let halfLife = fromIntegral drone_health_regenHalfLife_ticks
          in  halfLife / log 0.5
      , gp_overseerMaxHealth_base = overseer_health_max
      , gp_overseerMaxHealth =
          ClassParameter
            { cp_templar =
                overseer_health_max * overseer_health_max_factor_templar
            , cp_zealot =
                overseer_health_max * overseer_health_max_factor_zealot
            }
      , gp_droneRespawnCheckInterval =
          fromIntegral drone_respawn_interval_ticks
      , gp_droneRespawnDelay = fromIntegral drone_respawn_delay_ticks
      , gp_droneRespawnHealth =
          let halfLife =
                fromIntegral drone_health_regenHalfLife_ticks
              t = fromIntegral drone_respawn_delay_ticks
          in  (1.0 - exp (log 0.5 * (t / halfLife))) * drone_health_max
      , gp_overseerRespawnDelay = fromIntegral overseer_respawn_delay_ticks
      , gp_psiStormDamagePerHit = psiStorm_damagePerWave
      , gp_playerRespawnOverseerInvulnerabilityTicks =
          fromIntegral overseer_respawn_invulnerabilityTime_overseer_ticks
      , gp_playerRespawnDroneInvulnerabilityTicks =
          fromIntegral overseer_respawn_invulnerabilityTime_drone_ticks
      , gp_playerRespawnOverseerBonusHealthFraction =
          overseer_respawn_bonusHealthFraction
      , gp_unlimitedSpawnInvulnerability = spawnArea_enableUnlimitedSpawnInvulnerability
      , gp_shieldDecayPeriodFactor =
          let halfLife = fromIntegral shieldPowerup_decayHalfLife_ticks
              t = fromIntegral C.healthRegenInterval
          in  exp (log 0.5 * (t / halfLife))
      }
