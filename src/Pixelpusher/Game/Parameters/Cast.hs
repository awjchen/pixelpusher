{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Cast (
  CastParams (..),
  makeCastParams,
  droneDashDeltaV,
  psiStormLastDamageTime,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Drone
import Pixelpusher.Game.Parameters.Base.DroneDash
import Pixelpusher.Game.Parameters.Base.Overseer
import Pixelpusher.Game.Parameters.Base.OverseerDash
import Pixelpusher.Game.Parameters.Base.PsiStorm

data CastParams = CastParams
  { gp_psiStormLifetimeTicks :: Ticks
  , gp_psiStormCastCooldownTicks :: Ticks
  , gp_psiStormHitPeriodTicks :: Ticks
  , gp_castRange :: ClassParameter Fixed
  , gp_overseerDashDeltaV :: ClassParameter Fixed
  , gp_overseerDashCooldownTicks :: ClassParameter Ticks
  , gp_overseerDashMinDelayTicks :: ClassParameter Ticks
  , gp_overseerDashMaxCharges :: ClassParameter Int
  , gp_enableOverseerDash :: Bool
  , gp_enableDroneDash :: Bool
  , gp_droneDashEfficiencyFactor :: Fixed
  , gp_droneDashFullEfficiencyTicks :: Ticks
  , gp_droneAccel :: Fixed
  , gp_maxDroneDashChargeTicks :: Ticks
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeCastParams ::
  BaseDroneParams Fixed ->
  BaseDroneDashParams Fixed ->
  BaseOverseerParams Fixed ->
  BaseOverseerDashParams Fixed ->
  BasePsiStormParams Fixed ->
  CastParams
makeCastParams
  BaseDroneParams{..}
  BaseDroneDashParams{..}
  BaseOverseerParams{..}
  BaseOverseerDashParams{..}
  BasePsiStormParams{..} =
    CastParams
      { gp_psiStormLifetimeTicks =
          fromIntegral $
            (psiStorm_damageInterval_ticks * (2 * psiStorm_numWaves + 1)) `div` 2
      , gp_psiStormCastCooldownTicks = fromIntegral psiStorm_castCooldown_ticks
      , gp_psiStormHitPeriodTicks = fromIntegral psiStorm_damageInterval_ticks
      , gp_castRange =
          ClassParameter
            { cp_templar = overseer_controlRadius_templar
            , cp_zealot = overseer_controlRadius_zealot
            }
      , gp_overseerDashDeltaV =
          ClassParameter
            { cp_templar =
                overseerDash_power_base
                  * overseerDash_power_factor_templar
                  * overseer_acceleration
                  * overseer_acceleration_factor_templar
            , cp_zealot =
                overseerDash_power_base
                  * overseerDash_power_factor_zealot
                  * overseer_acceleration
                  * overseer_acceleration_factor_zealot
            }
      , gp_overseerDashCooldownTicks =
          ClassParameter
            { cp_templar = fromIntegral overseerDash_cooldown_ticks_templar
            , cp_zealot = fromIntegral overseerDash_cooldown_ticks_zealot
            }
      , gp_overseerDashMinDelayTicks =
          ClassParameter
            { cp_templar = fromIntegral overseerDash_min_delay_ticks_templar
            , cp_zealot = fromIntegral overseerDash_min_delay_ticks_zealot
            }
      , gp_overseerDashMaxCharges =
          ClassParameter
            { cp_templar = overseerDash_max_charges_templar
            , cp_zealot = overseerDash_max_charges_zealot
            }
      , gp_enableOverseerDash = overseerDash_enable
      , gp_enableDroneDash = droneDash_enable
      , gp_droneDashEfficiencyFactor = droneDash_chargeEfficiency
      , gp_droneDashFullEfficiencyTicks = fromIntegral droneDash_chargeFullEfficiencyDuration_ticks
      , gp_droneAccel = drone_acceleration
      , gp_maxDroneDashChargeTicks = fromIntegral droneDash_chargeMaxDuration_ticks
      }

-- TODO: Apply gp_maxDroneDashChargeTicks within this function?
droneDashDeltaV :: CastParams -> Ticks -> Fixed
droneDashDeltaV CastParams{..} chargeTicks =
  let accelTicks
        | chargeTicks <= gp_droneDashFullEfficiencyTicks =
            fromIntegral chargeTicks
        | otherwise =
            fromIntegral gp_droneDashFullEfficiencyTicks
              + fromIntegral (chargeTicks - gp_droneDashFullEfficiencyTicks)
                * gp_droneDashEfficiencyFactor
  in  accelTicks * gp_droneAccel

psiStormLastDamageTime :: CastParams -> Time -> Time
psiStormLastDamageTime CastParams{..} castTime =
  let numHits = gp_psiStormLifetimeTicks `div` gp_psiStormHitPeriodTicks
  in  addTicks
        (numHits * gp_psiStormHitPeriodTicks)
        castTime
