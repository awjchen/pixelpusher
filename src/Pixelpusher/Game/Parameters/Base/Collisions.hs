module Pixelpusher.Game.Parameters.Base.Collisions (
  BaseDamageParams (..),
  defaultDamageParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseDamageParams a = BaseDamageParams
  { collisions_damageMultiplier_droneDrone :: a
  , collisions_damageMultiplier_overseerDrone :: a
  , collisions_damageMultiplier_overseerOverseer :: a
  , collisions_damageEnergyRatio :: a
  , collisions_adversarialCollisionAddedVelocity :: a
  , collisions_enableFriendlyDroneCollisons :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDamageParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDamageParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDamageParams Double) where
  toTable = Toml.genericToTable

defaultDamageParams :: BaseDamageParams Double
defaultDamageParams =
  BaseDamageParams
    { collisions_damageMultiplier_droneDrone = 1.2
    , collisions_damageMultiplier_overseerDrone = 1.0
    , collisions_damageMultiplier_overseerOverseer = 1.0
    , collisions_damageEnergyRatio = 7.5e-2
    , collisions_adversarialCollisionAddedVelocity = 0.8
    , collisions_enableFriendlyDroneCollisons = False
    }
