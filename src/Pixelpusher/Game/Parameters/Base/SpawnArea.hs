module Pixelpusher.Game.Parameters.Base.SpawnArea (
  BaseSpawnAreaParams (..),
  defaultSpawnAreaParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseSpawnAreaParams a = BaseSpawnAreaParams
  { spawnArea_radius :: a
  , spawnArea_location_degrees :: a
  , spawnArea_enableUnlimitedSpawnInvulnerability :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseSpawnAreaParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseSpawnAreaParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseSpawnAreaParams Double) where
  toTable = Toml.genericToTable

defaultSpawnAreaParams :: BaseSpawnAreaParams Double
defaultSpawnAreaParams =
  BaseSpawnAreaParams
    { spawnArea_radius = 100
    , spawnArea_location_degrees = -45
    , spawnArea_enableUnlimitedSpawnInvulnerability = False
    }
