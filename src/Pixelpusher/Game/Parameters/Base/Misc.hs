module Pixelpusher.Game.Parameters.Base.Misc (
  BaseMiscParams (..),
  defaultMiscParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseMiscParams a = BaseMiscParams
  { misc_worldSize_lengthPerSqrtPlayer :: a
  , misc_worldSize_minPlayers :: Int
  , misc_intermission_minDuration_ticks :: Int
  , misc_intermission_maxDuration_ticks :: Int
  , misc_enableFogOfWar :: Bool
  , misc_initialJoinDelay_ticks :: Int
  , misc_requiredFlagCapturesForVictory :: Int
  , misc_enablePlayersVersusBotsMode :: Bool
  , misc_overseerExplosion_radius :: a
  , misc_overseerExplosion_force :: a
  , misc_overseerExplosion_lifetime_ticks :: Int
  , misc_newRound_randomizeTeams :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseMiscParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseMiscParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseMiscParams Double) where
  toTable = Toml.genericToTable

defaultMiscParams :: BaseMiscParams Double
defaultMiscParams =
  BaseMiscParams
    { misc_worldSize_lengthPerSqrtPlayer = 542
    , misc_worldSize_minPlayers = 16
    , misc_intermission_minDuration_ticks = 15 * fromIntegral C.tickRate_hz
    , misc_intermission_maxDuration_ticks = 45 * fromIntegral C.tickRate_hz
    , misc_enableFogOfWar = True
    , misc_initialJoinDelay_ticks = fromIntegral $ C.tickRate_hz * 4
    , misc_requiredFlagCapturesForVictory = 3
    , misc_enablePlayersVersusBotsMode = False
    , misc_overseerExplosion_radius = 128
    , misc_overseerExplosion_force = 0.1
    , misc_overseerExplosion_lifetime_ticks = 16
    , misc_newRound_randomizeTeams = True
    }
