-- | Parameters for quickly iterating on constants during development
module Pixelpusher.Game.Parameters.Base.Dev (
  BaseTestParams (..),
  defaultTestParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseTestParams a = BaseTestParams
  { dev_param1 :: a
  , dev_param2 :: a
  , dev_param3 :: a
  , dev_param4 :: a
  , dev_param5 :: a
  , dev_param6 :: a
  , dev_enableTutorialMode :: Bool -- TODO: Should this be set elsewhere? It would never be used outside the tutorial
  , dev_enableDebugData :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseTestParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseTestParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseTestParams Double) where
  toTable = Toml.genericToTable

defaultTestParams :: BaseTestParams Double
defaultTestParams =
  BaseTestParams
    { dev_param1 = 0
    , dev_param2 = 0
    , dev_param3 = 0
    , dev_param4 = 0
    , dev_param5 = 0
    , dev_param6 = 0
    , dev_enableTutorialMode = False
    , dev_enableDebugData = False
    }
