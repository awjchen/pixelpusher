module Pixelpusher.Game.Parameters.Base.Overseer (
  BaseOverseerParams (..),
  defaultOverseerParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseOverseerParams a = BaseOverseerParams
  { overseer_acceleration :: a
  , overseer_drag :: a
  , overseer_controlRadius_templar :: a
  , overseer_controlRadius_zealot :: a
  , overseer_radius :: a
  , overseer_droneLimit_base :: Int
  , overseer_droneLimit_templar :: Int
  , overseer_droneLimit_zealot :: Int
  , overseer_health_fullRegenTime_ticks_templar :: Int
  , overseer_health_fullRegenTime_ticks_zealot :: Int
  , overseer_health_max :: a
  , overseer_respawn_invulnerabilityTime_overseer_ticks :: Int
  , overseer_respawn_invulnerabilityTime_drone_ticks :: Int
  , overseer_respawn_bonusHealthFraction :: a
  , overseer_respawn_delay_ticks :: Int
  , overseer_acceleration_factor_templar :: a
  , overseer_acceleration_factor_zealot :: a
  , overseer_drag_factor_templar :: a
  , overseer_drag_factor_zealot :: a
  , overseer_health_max_factor_templar :: a
  , overseer_health_max_factor_zealot :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseOverseerParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseOverseerParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseOverseerParams Double) where
  toTable = Toml.genericToTable

defaultOverseerParams :: BaseOverseerParams Double
defaultOverseerParams =
  BaseOverseerParams
    { overseer_acceleration = 0.0448
    , overseer_drag = 0.02625
    , overseer_controlRadius_templar = 375
    , overseer_controlRadius_zealot = 200
    , overseer_radius = 14
    , overseer_droneLimit_base = 6
    , overseer_droneLimit_templar = 6
    , overseer_droneLimit_zealot = 5
    , overseer_health_fullRegenTime_ticks_templar = 12 * fromIntegral C.tickRate_hz
    , overseer_health_fullRegenTime_ticks_zealot = (21 * fromIntegral C.tickRate_hz) `div` 2 -- 10.5 seconds
    , overseer_health_max = 56.0
    , overseer_respawn_invulnerabilityTime_overseer_ticks = 2 * fromIntegral C.tickRate_hz
    , overseer_respawn_invulnerabilityTime_drone_ticks = fromIntegral C.tickRate_hz
    , overseer_respawn_bonusHealthFraction = 0.25
    , overseer_respawn_delay_ticks = 8 * fromIntegral C.tickRate_hz
    , overseer_acceleration_factor_templar = 1.0
    , overseer_acceleration_factor_zealot = 0.85
    , overseer_drag_factor_templar = 1.0
    , overseer_drag_factor_zealot = 0.78
    , overseer_health_max_factor_templar = 1.0
    , overseer_health_max_factor_zealot = 1.2
    }
