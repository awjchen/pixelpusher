module Pixelpusher.Game.Parameters.Base.Bots (
  BaseBotParams (..),
  defaultBotParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseBotParams a = BaseBotParams
  { bots_enable :: Bool
  , bots_minBotsPerTeam :: Int
  , bots_minTeamSize :: Int
  , bots_level :: Int
  , bots_deprioritizeEnemyFlag :: Bool
  , bots_forceEnableAllTechniques :: Bool
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseBotParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseBotParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseBotParams Double) where
  toTable = Toml.genericToTable

defaultBotParams :: BaseBotParams Double
defaultBotParams =
  BaseBotParams
    { bots_enable = True
    , bots_minBotsPerTeam = 0
    , bots_minTeamSize = 3
    , bots_level = 4
    , bots_deprioritizeEnemyFlag = False
    , bots_forceEnableAllTechniques = False
    }
