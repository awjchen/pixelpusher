module Pixelpusher.Game.Parameters.Base.Obstacle (
  BaseSoftObstacleParams (..),
  defaultSoftObstacleParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseSoftObstacleParams a = BaseSoftObstacleParams
  { obstacle_radius :: Int
  , obstacle_entityRepulsion :: a
  , obstacle_obstacleReplusion :: a
  , obstacle_averageDriftSpeed :: a
  , obstacle_density :: a
  , obstacle_obstaclesPerPlayer :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseSoftObstacleParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseSoftObstacleParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseSoftObstacleParams Double) where
  toTable = Toml.genericToTable

defaultSoftObstacleParams :: BaseSoftObstacleParams Double
defaultSoftObstacleParams =
  BaseSoftObstacleParams
    { obstacle_radius = 120
    , obstacle_entityRepulsion = 3.0e-3
    , obstacle_obstacleReplusion = 1e-4
    , obstacle_averageDriftSpeed = 4.0e-2
    , obstacle_density = 4
    , obstacle_obstaclesPerPlayer = 0.675
    }
