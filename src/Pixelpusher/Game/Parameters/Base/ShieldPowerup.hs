module Pixelpusher.Game.Parameters.Base.ShieldPowerup (
  BasePowerupParams (..),
  defaultPowerupParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BasePowerupParams a = BasePowerupParams
  { shieldPowerup_spawned_health :: a
  , shieldPowerup_spawned_spawningTime_ticks :: Int
  , shieldPowerup_spawned_location_worldRadiusFraction :: a
  , shieldPowerup_spawned_enable :: Bool
  , shieldPowerup_dropped_enable :: Bool
  , shieldPowerup_dropped_health :: a
  , shieldPowerup_dropped_lifetime_ticks :: Int
  , shieldPowerup_radiusFactor :: a
  , shieldPowerup_decayHalfLife_ticks :: Int
  , shieldPowerup_maxHealth :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BasePowerupParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BasePowerupParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BasePowerupParams Double) where
  toTable = Toml.genericToTable

defaultPowerupParams :: BasePowerupParams Double
defaultPowerupParams =
  BasePowerupParams
    { shieldPowerup_spawned_health = 28.0
    , shieldPowerup_spawned_spawningTime_ticks = fromIntegral $ C.tickRate_hz * 10
    , shieldPowerup_spawned_location_worldRadiusFraction = 0.25
    , shieldPowerup_spawned_enable = True
    , shieldPowerup_dropped_enable = True
    , shieldPowerup_dropped_health = 14.0
    , shieldPowerup_dropped_lifetime_ticks = fromIntegral $ C.tickRate_hz * 5
    , shieldPowerup_radiusFactor = 1.3363
    , shieldPowerup_decayHalfLife_ticks = 10 * fromIntegral C.tickRate_hz
    , shieldPowerup_maxHealth = 56.0
    }
