module Pixelpusher.Game.Parameters.Base.PsiStorm (
  BasePsiStormParams (..),
  defaultPsiStormParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BasePsiStormParams a = BasePsiStormParams
  { psiStorm_radius :: Int
  , psiStorm_castCooldown_ticks :: Int
  , psiStorm_damagePerWave :: a
  , psiStorm_numWaves :: Int
  , psiStorm_damageInterval_ticks :: Int
  , psiStorm_expansionInitialRate :: a
  , psiStorm_expansionAcceleration :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BasePsiStormParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BasePsiStormParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BasePsiStormParams Double) where
  toTable = Toml.genericToTable

defaultPsiStormParams :: BasePsiStormParams Double
defaultPsiStormParams =
  BasePsiStormParams
    { psiStorm_radius = 144
    , psiStorm_castCooldown_ticks = fromIntegral C.tickRate_hz * 7
    , psiStorm_damagePerWave = 3.0
    , psiStorm_numWaves = 7
    , psiStorm_damageInterval_ticks = fromIntegral C.tickRate_hz `div` 3
    , psiStorm_expansionInitialRate = 0.045
    , psiStorm_expansionAcceleration = 0.041667
    }
