module Pixelpusher.Game.Parameters.Base.Flag (
  BaseFlagParams (..),
  defaultFlagParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseFlagParams a = BaseFlagParams
  { flag_drop_enable :: Bool
  , flag_drop_flagPickupCooldown_ticks :: Int
  , flag_radius :: a
  , flag_recovery_radius :: a
  , flag_recovery_time_ticks :: Int
  , flag_recovery_autoRecoverTimeMultiplier :: Int
  , flag_carrier_quadraticDrag :: a
  }
  deriving stock (Functor, Generic)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseFlagParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseFlagParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseFlagParams Double) where
  toTable = Toml.genericToTable

defaultFlagParams :: BaseFlagParams Double
defaultFlagParams =
  BaseFlagParams
    { flag_drop_enable = True
    , flag_drop_flagPickupCooldown_ticks = fromIntegral C.tickRate_hz
    , flag_radius = 8
    , flag_recovery_radius = 90
    , flag_recovery_time_ticks = 5 * fromIntegral C.tickRate_hz
    , flag_recovery_autoRecoverTimeMultiplier = 3
    , flag_carrier_quadraticDrag = 0.01
    }
