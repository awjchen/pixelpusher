module Pixelpusher.Game.Parameters.Base.DroneDash (
  BaseDroneDashParams (..),
  defaultDroneDashParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseDroneDashParams a = BaseDroneDashParams
  { droneDash_enable :: Bool
  , droneDash_chargeEfficiency :: a
  , droneDash_chargeFullEfficiencyDuration_ticks :: Int
  , droneDash_chargeMaxDuration_ticks :: Int
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDroneDashParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDroneDashParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDroneDashParams Double) where
  toTable = Toml.genericToTable

defaultDroneDashParams :: BaseDroneDashParams Double
defaultDroneDashParams =
  BaseDroneDashParams
    { droneDash_enable = True
    , droneDash_chargeEfficiency = 0.55
    , droneDash_chargeFullEfficiencyDuration_ticks = 3
    , droneDash_chargeMaxDuration_ticks = 120
    }
