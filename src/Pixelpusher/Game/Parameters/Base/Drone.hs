module Pixelpusher.Game.Parameters.Base.Drone (
  BaseDroneParams (..),
  defaultDroneParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseDroneParams a = BaseDroneParams
  { drone_acceleration :: a
  , drone_drag :: a
  , drone_radius :: a
  , drone_maxDissociatedLifetime_ticks :: Int
  , drone_health_max :: a
  , drone_health_regenHalfLife_ticks :: Int
  , drone_respawn_delay_ticks :: Int
  , drone_respawn_interval_ticks :: Int
  , drone_acceleration_factor_templar :: a
  , drone_acceleration_factor_zealot :: a
  , drone_drag_factor_templar :: a
  , drone_drag_factor_zealot :: a
  , drone_health_max_factor_templar :: a
  , drone_health_max_factor_zealot :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseDroneParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseDroneParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseDroneParams Double) where
  toTable = Toml.genericToTable

defaultDroneParams :: BaseDroneParams Double
defaultDroneParams =
  BaseDroneParams
    { drone_acceleration = 0.0464
    , drone_drag = 0.0169221
    , drone_radius = 10
    , drone_maxDissociatedLifetime_ticks = 6 * fromIntegral C.tickRate_hz
    , drone_health_max = 20.25
    , drone_health_regenHalfLife_ticks = 5 * fromIntegral C.tickRate_hz
    , drone_respawn_delay_ticks = 4 * fromIntegral C.tickRate_hz
    , drone_respawn_interval_ticks = fromIntegral C.tickRate_hz `div` 5
    , drone_acceleration_factor_templar = 1.0
    , drone_acceleration_factor_zealot = 0.85
    , drone_drag_factor_templar = 1.0
    , drone_drag_factor_zealot = 0.78
    , drone_health_max_factor_templar = 1.0
    , drone_health_max_factor_zealot = 1.0
    }
