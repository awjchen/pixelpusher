module Pixelpusher.Game.Parameters.Base.OverseerDash (
  BaseOverseerDashParams (..),
  defaultOverseerDashParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C

--------------------------------------------------------------------------------

data BaseOverseerDashParams a = BaseOverseerDashParams
  { overseerDash_power_base :: a
  , overseerDash_cooldown_ticks_templar :: Int
  , overseerDash_cooldown_ticks_zealot :: Int
  , overseerDash_min_delay_ticks_templar :: Int
  , overseerDash_min_delay_ticks_zealot :: Int
  , overseerDash_max_charges_templar :: Int
  , overseerDash_max_charges_zealot :: Int
  , overseerDash_enable :: Bool
  , overseerDash_power_factor_templar :: a
  , overseerDash_power_factor_zealot :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseOverseerDashParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseOverseerDashParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseOverseerDashParams Double) where
  toTable = Toml.genericToTable

defaultOverseerDashParams :: BaseOverseerDashParams Double
defaultOverseerDashParams =
  BaseOverseerDashParams
    { overseerDash_power_base = 40
    , overseerDash_cooldown_ticks_templar = 4 * fromIntegral C.tickRate_hz
    , overseerDash_cooldown_ticks_zealot = 4 * fromIntegral C.tickRate_hz
    , overseerDash_min_delay_ticks_templar = fromIntegral C.tickRate_hz `div` 4
    , overseerDash_min_delay_ticks_zealot = fromIntegral C.tickRate_hz `div` 4
    , overseerDash_max_charges_templar = 2
    , overseerDash_max_charges_zealot = 2
    , overseerDash_enable = True
    , overseerDash_power_factor_templar = 1.0
    , overseerDash_power_factor_zealot = 1.3
    }
