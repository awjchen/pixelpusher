module Pixelpusher.Game.Parameters.Base.Audio (
  BaseAudioParams (..),
  defaultAudioParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseAudioParams a = BaseAudioParams
  { audio_falloff_start_viewRadiusFraction :: a
  , audio_falloff_end_viewRadiusFraction :: a
  }
  deriving stock (Functor, Generic)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseAudioParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseAudioParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseAudioParams Double) where
  toTable = Toml.genericToTable

defaultAudioParams :: BaseAudioParams Double
defaultAudioParams =
  BaseAudioParams
    { audio_falloff_start_viewRadiusFraction = 0.75
    , audio_falloff_end_viewRadiusFraction = 1.5
    }
