module Pixelpusher.Game.Parameters.Base.TeamBase (
  BaseTeamBaseParams (..),
  defaultTeamBaseParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

--------------------------------------------------------------------------------

data BaseTeamBaseParams a = BaseTeamBaseParams
  { teamBase_radius :: a
  , teamBase_location_worldRadiusFraction :: a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

instance Toml.FromValue (BaseTeamBaseParams Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseTeamBaseParams Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseTeamBaseParams Double) where
  toTable = Toml.genericToTable

defaultTeamBaseParams :: BaseTeamBaseParams Double
defaultTeamBaseParams =
  BaseTeamBaseParams
    { teamBase_radius = 100
    , teamBase_location_worldRadiusFraction = 0.6273
    }
