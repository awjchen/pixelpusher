{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Dynamics (
  DynamicsParams (..),
  makeDynamicsParams,
  shieldRadius,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Collisions
import Pixelpusher.Game.Parameters.Base.Drone
import Pixelpusher.Game.Parameters.Base.Flag
import Pixelpusher.Game.Parameters.Base.Misc
import Pixelpusher.Game.Parameters.Base.Obstacle
import Pixelpusher.Game.Parameters.Base.Overseer
import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.ShieldPowerup
import Pixelpusher.Game.Parameters.Base.SpawnArea
import Pixelpusher.Game.Parameters.Base.TeamBase

data DynamicsParams = DynamicsParams
  -- World
  { gp_worldRadius :: Fixed
  , gp_shieldPowerupWorldRadiusFrac :: Fixed
  , -- Drag and acceleration
    gp_overseerAcceleration_base :: Fixed
  , gp_overseerAcceleration :: ClassParameter Fixed
  , gp_overseerDrag_base :: Fixed
  , gp_overseerDrag :: ClassParameter Fixed
  , gp_droneAcceleration :: ClassParameter Fixed
  , gp_droneDrag :: ClassParameter Fixed
  , gp_flagDrag :: Fixed
  , -- Radii
    gp_overseerRadius_base :: Fixed
  , gp_overseerRadius :: ClassParameter Fixed
  , gp_droneRadius_base :: Fixed
  , gp_droneRadius :: ClassParameter Fixed
  , gp_flagRadius :: Fixed
  , gp_flagRecoveryRadius :: Fixed
  , -- Density
    gp_softObstacleDensityFactor :: Fixed
  , -- Collisions
    gp_droneDroneCollisionDamageFactor :: Fixed
  , gp_overseerDroneCollisionDamageFactor :: Fixed
  , gp_overseerOverseerCollisionDamageFactor :: Fixed
  , gp_combatDeltaV :: Fixed
  , gp_enableFriendlyDroneCollison :: Bool
  , -- Soft obstacles
    gp_softObstacleRadius :: Fixed
  , gp_softObstacleCurvature :: Fixed
  , gp_softObstacleReplusion :: Fixed
  , gp_softObstacleAverageSpeed :: Fixed
  , -- Psi-storms
    gp_psiStormRadius :: Fixed
  , gp_psiStormHitPeriodTicks :: Ticks
  , gp_psiStormInitialGrowthFraction :: Fixed
  , gp_psiStormGrowthConstant :: Fixed
  , gp_psiStormFadeTicks :: Ticks
  , -- Control radius
    gp_controlRadius :: ClassParameter Fixed
  , -- Damage rate
    gp_damageEnergyRatio :: Fixed
  , gp_energyDamageRatio :: Fixed
  , -- Explosions
    gp_overseerExplosionRadius :: Fixed
  , gp_overseerExplosionForceConstant :: Fixed
  , gp_overseerExplosionDuration :: Ticks
  , -- Misc
    gp_teamBaseRadius :: Fixed
  , gp_teamBaseDistanceFromCenter :: Fixed
  , gp_spawnAreaRadius :: Fixed
  , -- TODO: this parameter is never used on its own
    gp_powerupRadiusFactor :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeDynamicsParams ::
  Int ->
  BaseDroneParams Fixed ->
  BaseMiscParams Fixed ->
  BaseDamageParams Fixed ->
  BaseFlagParams Fixed ->
  BaseOverseerParams Fixed ->
  BaseSoftObstacleParams Fixed ->
  BasePsiStormParams Fixed ->
  BasePowerupParams Fixed ->
  BaseSpawnAreaParams Fixed ->
  BaseTeamBaseParams Fixed ->
  DynamicsParams
makeDynamicsParams
  gameSize
  BaseDroneParams{..}
  BaseMiscParams{..}
  BaseDamageParams{..}
  BaseFlagParams{..}
  BaseOverseerParams{..}
  BaseSoftObstacleParams{..}
  BasePsiStormParams{..}
  BasePowerupParams{..}
  BaseSpawnAreaParams{..}
  BaseTeamBaseParams{..} =
    DynamicsParams
      { -- World
        gp_worldRadius = worldRadius
      , gp_shieldPowerupWorldRadiusFrac =
          shieldPowerup_spawned_location_worldRadiusFraction
      , -- Drag and acceleration
        gp_overseerAcceleration_base = overseer_acceleration
      , gp_overseerAcceleration =
          ClassParameter
            { cp_templar =
                overseer_acceleration * overseer_acceleration_factor_templar
            , cp_zealot =
                overseer_acceleration * overseer_acceleration_factor_zealot
            }
      , gp_overseerDrag_base = overseer_drag
      , gp_overseerDrag =
          ClassParameter
            { cp_templar = overseer_drag * overseer_drag_factor_templar
            , cp_zealot = overseer_drag * overseer_drag_factor_zealot
            }
      , gp_droneAcceleration =
          ClassParameter
            { cp_templar =
                drone_acceleration * drone_acceleration_factor_templar
            , cp_zealot =
                drone_acceleration * drone_acceleration_factor_zealot
            }
      , gp_droneDrag =
          ClassParameter
            { cp_templar = drone_drag * drone_drag_factor_templar
            , cp_zealot = drone_drag * drone_drag_factor_zealot
            }
      , gp_flagDrag = flag_carrier_quadraticDrag
      , -- Radii
        gp_overseerRadius_base = overseer_radius
      , gp_overseerRadius =
          ClassParameter
            { cp_templar =
                overseer_radius * sqrt overseer_health_max_factor_templar
            , cp_zealot =
                overseer_radius * sqrt overseer_health_max_factor_zealot
            }
      , gp_droneRadius_base = drone_radius
      , gp_droneRadius =
          ClassParameter
            { cp_templar =
                let droneHealthFactor =
                      drone_health_max_factor_templar
                        * ( fromIntegral overseer_droneLimit_base
                              / fromIntegral overseer_droneLimit_templar
                          )
                in  drone_radius * sqrt droneHealthFactor
            , cp_zealot =
                let droneHealthFactor =
                      drone_health_max_factor_zealot
                        * ( fromIntegral overseer_droneLimit_base
                              / fromIntegral overseer_droneLimit_zealot
                          )
                in  drone_radius * sqrt droneHealthFactor
            }
      , gp_flagRadius = flag_radius
      , gp_flagRecoveryRadius = flag_recovery_radius
      , -- Density
        gp_softObstacleDensityFactor = obstacle_density
      , -- Collisions
        gp_droneDroneCollisionDamageFactor = collisions_damageMultiplier_droneDrone
      , gp_overseerDroneCollisionDamageFactor = collisions_damageMultiplier_overseerDrone
      , gp_overseerOverseerCollisionDamageFactor = collisions_damageMultiplier_overseerOverseer
      , gp_combatDeltaV = collisions_adversarialCollisionAddedVelocity
      , gp_enableFriendlyDroneCollison = collisions_enableFriendlyDroneCollisons
      , -- Soft obstacles
        gp_softObstacleRadius = fromIntegral obstacle_radius
      , gp_softObstacleCurvature = obstacle_entityRepulsion
      , gp_softObstacleReplusion = obstacle_obstacleReplusion
      , gp_softObstacleAverageSpeed = obstacle_averageDriftSpeed
      , -- Psi-storms
        gp_psiStormRadius = fromIntegral psiStorm_radius
      , gp_psiStormHitPeriodTicks = fromIntegral psiStorm_damageInterval_ticks
      , gp_psiStormInitialGrowthFraction = psiStorm_expansionInitialRate
      , gp_psiStormGrowthConstant = psiStorm_expansionAcceleration
      , gp_psiStormFadeTicks = fromIntegral psiStorm_damageInterval_ticks
      , -- Control radius
        gp_controlRadius =
          ClassParameter
            { cp_templar = overseer_controlRadius_templar
            , cp_zealot = overseer_controlRadius_zealot
            }
      , -- Damage rate
        gp_damageEnergyRatio = collisions_damageEnergyRatio / C.density
      , gp_energyDamageRatio = C.density / collisions_damageEnergyRatio
      , -- Explosions
        gp_overseerExplosionRadius = misc_overseerExplosion_radius
      , gp_overseerExplosionForceConstant = misc_overseerExplosion_force
      , gp_overseerExplosionDuration = fromIntegral misc_overseerExplosion_lifetime_ticks
      , -- Misc
        gp_teamBaseRadius = teamBase_radius
      , gp_teamBaseDistanceFromCenter =
          worldRadius * teamBase_location_worldRadiusFraction
      , gp_spawnAreaRadius = spawnArea_radius
      , gp_powerupRadiusFactor = shieldPowerup_radiusFactor
      }
    where
      worldRadius =
        0.5 -- (0.5*) converts the width to a radius
          * sqrt (2 * fromIntegral gameSize)
          * misc_worldSize_lengthPerSqrtPlayer

--------------------------------------------------------------------------------
-- Helpers

shieldRadius :: DynamicsParams -> Fixed -> Fixed
shieldRadius dynParams shieldHealth =
  gp_powerupRadiusFactor dynParams * sqrt shieldHealth
