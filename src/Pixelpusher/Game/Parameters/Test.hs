{-# LANGUAGE RecordWildCards #-}

-- | Parameters for quickly iterating on constants during development
module Pixelpusher.Game.Parameters.Test (
  TestParams (..),
  makeTestParams,
  emptyTestParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

import Pixelpusher.Game.Parameters.Base.Dev

data TestParams = TestParams
  { gp_testFloat1 :: Fixed
  , gp_testFloat2 :: Fixed
  , gp_testFloat3 :: Fixed
  , gp_testFloat4 :: Fixed
  , gp_testFloat5 :: Fixed
  , gp_testFloat6 :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeTestParams :: BaseTestParams Fixed -> TestParams
makeTestParams BaseTestParams{..} =
  TestParams
    { gp_testFloat1 = dev_param1
    , gp_testFloat2 = dev_param2
    , gp_testFloat3 = dev_param3
    , gp_testFloat4 = dev_param4
    , gp_testFloat5 = dev_param5
    , gp_testFloat6 = dev_param6
    }

emptyTestParams :: TestParams
emptyTestParams =
  TestParams
    { gp_testFloat1 = 0
    , gp_testFloat2 = 0
    , gp_testFloat3 = 0
    , gp_testFloat4 = 0
    , gp_testFloat5 = 0
    , gp_testFloat6 = 0
    }
