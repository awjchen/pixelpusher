{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Flag (
  FlagParams (..),
  makeFlagParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Flag

data FlagParams = FlagParams
  { gp_enableFlagDrops :: Bool
  , gp_flagPickupDisableTicks :: Ticks
  , gp_flagRecoveryTicks :: Ticks
  , gp_flagAutoRecoveryFactor :: Int
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeFlagParams :: BaseFlagParams Fixed -> FlagParams
makeFlagParams BaseFlagParams{..} =
  FlagParams
    { gp_enableFlagDrops = flag_drop_enable
    , gp_flagPickupDisableTicks = fromIntegral flag_drop_flagPickupCooldown_ticks
    , gp_flagRecoveryTicks = fromIntegral flag_recovery_time_ticks
    , gp_flagAutoRecoveryFactor = flag_recovery_autoRecoverTimeMultiplier
    }
