{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Parameters.Audio (
  AudioParams (..),
  makeAudioParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

import Pixelpusher.Game.Parameters.Base.Audio

data AudioParams = AudioParams
  { gp_audioFalloffStart_viewRadiusFraction :: Fixed
  , gp_audioFalloffEnd_viewRadiusFraction :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeAudioParams :: BaseAudioParams Fixed -> AudioParams
makeAudioParams BaseAudioParams{..} =
  AudioParams
    { gp_audioFalloffStart_viewRadiusFraction = audio_falloff_start_viewRadiusFraction
    , gp_audioFalloffEnd_viewRadiusFraction = audio_falloff_end_viewRadiusFraction
    }
