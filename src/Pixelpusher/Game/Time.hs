-- | In-game time
module Pixelpusher.Game.Time (
  Ticks (..),
  Time,
  getTime,
  initialTime,
  diffTime,
  nextTick,
  prevTick,
  addTicks,
  subtractTicks,
  atMultipleOf,
  SyncTime,
  getSyncTime,
  initialSyncTime,
  addSyncTicks,
  diffSyncTime,
  nextSyncTick,
  prevSyncTick,
  atMultipleOfSync,
  syncTimeRange,
) where

import Data.Int (Int32)
import Data.List (unfoldr)
import Data.Serialize (Serialize)
import Foreign.Storable

import Pixelpusher.Custom.WrappedIntMap qualified as WIM

--------------------------------------------------------------------------------

-- | Shared unit of duration
newtype Ticks = Ticks {getTicks :: Int32}
  deriving newtype (Eq, Ord, Bounded, Num, Real, Enum, Integral, Serialize)

--------------------------------------------------------------------------------

-- | Absolute time for the game logic
--
-- At 120 ticks per second, an Int32 would take over half a year to overflow.
newtype Time = Time Int32
  deriving newtype (Eq, Ord, Serialize, Show, Storable)

getTime :: Time -> Int32
getTime (Time i) = i

instance WIM.IsInt Time where
  toInt (Time n) = WIM.makeIdentity $ fromIntegral n
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . Time . fromIntegral
  {-# INLINE fromInt #-}

initialTime :: Time
initialTime = Time 0

diffTime :: Time -> Time -> Ticks
diffTime (Time n1) (Time n2) = Ticks $ n1 - n2

nextTick :: Time -> Time
nextTick (Time n) = Time $ n + 1

prevTick :: Time -> Time
prevTick (Time n) = Time $ n - 1

addTicks :: Ticks -> Time -> Time
addTicks (Ticks ticks) (Time t) = Time $ t + ticks
{-# INLINE addTicks #-}

subtractTicks :: Ticks -> Time -> Time
subtractTicks (Ticks ticks) (Time t) = Time $ t - ticks
{-# INLINE subtractTicks #-}

atMultipleOf :: Time -> Ticks -> Bool
atMultipleOf (Time t) (Ticks ticks) = t `rem` ticks == 0
{-# INLINE atMultipleOf #-}

--------------------------------------------------------------------------------

-- | Absolute time for game state synchronization between client and server
--
-- Using an `Int32` rather than an `Int` to save a bit of bandwidth. At 120
-- ticks per second, an Int32 would take over half a year to overflow.
newtype SyncTime = SyncTime Int32
  deriving newtype (Eq, Ord, Serialize, Show, Storable)

getSyncTime :: SyncTime -> Int32
getSyncTime (SyncTime i) = i

instance WIM.IsInt SyncTime where
  toInt (SyncTime n) = WIM.makeIdentity $ fromIntegral n
  {-# INLINE toInt #-}
  fromInt = WIM.makeIdentity . SyncTime . fromIntegral
  {-# INLINE fromInt #-}

initialSyncTime :: SyncTime
initialSyncTime = SyncTime 0

addSyncTicks :: Ticks -> SyncTime -> SyncTime
addSyncTicks (Ticks ticks) (SyncTime t) = SyncTime $ t + ticks
{-# INLINE addSyncTicks #-}

diffSyncTime :: SyncTime -> SyncTime -> Ticks
diffSyncTime (SyncTime n1) (SyncTime n2) = Ticks $ n1 - n2

nextSyncTick :: SyncTime -> SyncTime
nextSyncTick (SyncTime n) = SyncTime $ n + 1

prevSyncTick :: SyncTime -> SyncTime
prevSyncTick (SyncTime n) = SyncTime $ n - 1

atMultipleOfSync :: SyncTime -> Ticks -> Bool
atMultipleOfSync (SyncTime t) (Ticks ticks) = t `rem` ticks == 0
{-# INLINE atMultipleOfSync #-}

-- | Helper function
syncTimeRange :: SyncTime -> SyncTime -> [SyncTime]
syncTimeRange firstTime lastTime =
  unfoldr
    (\t -> if t > lastTime then Nothing else Just (t, nextSyncTick t))
    firstTime
