module Pixelpusher.Game.Player (
  Player (..),
  player_name,
  player_team,
  player_actorID,
  player_color,
  player_joinTime,
  player_initialClass,
  PlayerAttributes (..),
) where

import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.DelayQueue (DelayQueue)
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Bots.AI (BotState)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.PlayerStats
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

-- | Data associated with a player
data Player = Player
  { player_attrs :: PlayerAttributes
  , player_controls :: PlayerControls
  -- ^ Most recent control message
  , player_prevControls :: PlayerControls
  , player_botState :: Maybe BotState
  -- ^ Persistent state for bot actors; unused for non-bot actors
  , player_botControlQueue :: DelayQueue PlayerControls
  , player_botTargetControls :: PlayerControls
  , player_prevBotTargetControls :: PlayerControls
  , player_stats :: PlayerStats
  , player_aggregateStats :: AggregatePlayerStats
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Invariant: `PlayerAttributes` should only be updated between rounds, and
-- should otherwise remain fixed.
data PlayerAttributes = PlayerAttributes
  { playerAttrs_pid :: ActorID
  , playerAttrs_name :: PlayerName
  , playerAttrs_team :: Team
  , playerAttrs_color :: ColorVariant
  , playerAttrs_joinTime :: Time
  , playerAttrs_initialClass :: PlayerClass
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- Extra `Player` lenses

player_name :: Lens' Player PlayerName
player_name = field @"player_attrs" . field @"playerAttrs_name"

player_team :: Lens' Player Team
player_team = field @"player_attrs" . field @"playerAttrs_team"

player_actorID :: Lens' Player ActorID
player_actorID = field @"player_attrs" . field @"playerAttrs_pid"

player_color :: Lens' Player ColorVariant
player_color = field @"player_attrs" . field @"playerAttrs_color"

player_joinTime :: Lens' Player Time
player_joinTime = field @"player_attrs" . field @"playerAttrs_joinTime"

player_initialClass :: Lens' Player PlayerClass
player_initialClass = field @"player_attrs" . field @"playerAttrs_initialClass"
