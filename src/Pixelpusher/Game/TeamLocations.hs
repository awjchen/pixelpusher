module Pixelpusher.Game.TeamLocations (
  teamBaseLocation,
  spawnAreaLocation,
) where

import Pixelpusher.Custom.Fixed (Fixed2 (Fixed2))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Team

teamBaseLocation :: DynamicsParams -> Team -> Fixed2
teamBaseLocation params = \case
  TeamW -> Fixed2 (-r) 0
  TeamE -> Fixed2 r 0
  where
    r = gp_teamBaseDistanceFromCenter params - gp_teamBaseRadius params

spawnAreaLocation :: GameParams -> Team -> Fixed2
spawnAreaLocation params = \case
  TeamW -> westLoc
  TeamE -> eastLoc
  where
    spawnAreaRadius = gp_spawnAreaRadius (gp_dynamics params)
    r = gp_worldRadius (gp_dynamics params) - buffer - spawnAreaRadius
    buffer = 8
    angle = gp_spawnLocationAngle params
    eastLoc = Fixed2 (r * cos angle) (r * sin angle)
    westLoc = -eastLoc
