{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

-- |
-- We represent the game parameters in the following three ways:
-- 1. 'TomlGameParams'
-- 2. 'BaseGameParams'
-- 3. 'GameParams'
--
-- 'TomlGameParams'
-- * User-facing, exported as TOML
-- * Organized for editing by users
-- * Uses 'Double's for their nice textual representation
--
-- 'BaseGameParams'
-- * Internal
-- * Variant of 'TomlGameParams' that uses 'Fixed' for cross-platform
-- compatibility
--
-- 'GameParams'
-- * Internal
-- * Organized for consumption by the game logic
-- * Uses 'Fixed' for cross-platform compatibility
--
-- 'BaseGameParams' is derived from 'TomlGameParams' by converting the
-- 'Double's to 'Fixed'. 'GameParams' is derived from the combination of
-- 'BaseGameParams' and the number of players.
--
-- Currently, the number of players is represented by the number of "control
-- points", an old game mechanic that no longer exists.
module Pixelpusher.Game.Parameters (
  TomlGameParams,
  defaultTomlGameParams,
  BaseGameParams,
  BaseGameParams' (..),
  GameParams (..),
  makeBaseGameParams,
  makeGameParams,
  defaultBaseGameParams,
  resizeGameParams,
  rebaseGameParams,
  defaultGameParams,
  -- Re-exports
  BotParams (..),
  CastParams (..),
  CombatParams (..),
  DynamicsParams (..),
  FlagParams (..),
  AudioParams (..),
  TestParams (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.Generic.FromValue qualified as Toml
import Toml.Schema.Generic.ToValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

import Pixelpusher.Game.Parameters.Base.Audio
import Pixelpusher.Game.Parameters.Base.Bots
import Pixelpusher.Game.Parameters.Base.Collisions
import Pixelpusher.Game.Parameters.Base.Dev
import Pixelpusher.Game.Parameters.Base.Drone
import Pixelpusher.Game.Parameters.Base.DroneDash
import Pixelpusher.Game.Parameters.Base.Flag
import Pixelpusher.Game.Parameters.Base.Misc
import Pixelpusher.Game.Parameters.Base.Obstacle
import Pixelpusher.Game.Parameters.Base.Overseer
import Pixelpusher.Game.Parameters.Base.OverseerDash
import Pixelpusher.Game.Parameters.Base.PsiStorm
import Pixelpusher.Game.Parameters.Base.ShieldPowerup
import Pixelpusher.Game.Parameters.Base.SpawnArea
import Pixelpusher.Game.Parameters.Base.TeamBase

import Pixelpusher.Game.Parameters.Audio
import Pixelpusher.Game.Parameters.Bots
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.Parameters.Combat
import Pixelpusher.Game.Parameters.Dynamics
import Pixelpusher.Game.Parameters.Flag
import Pixelpusher.Game.Parameters.Test

--------------------------------------------------------------------------------

-- | User-specified game parameters. The type parameter 'a' is intended to be
-- substituted with either 'Double' or 'Fixed'.
data BaseGameParams' a = BaseGameParams'
  { overseers :: BaseOverseerParams a
  , drones :: BaseDroneParams a
  , obstacles :: BaseSoftObstacleParams a
  , collisions :: BaseDamageParams a
  , spawnAreas :: BaseSpawnAreaParams a
  , teamBases :: BaseTeamBaseParams a
  , overseerDash :: BaseOverseerDashParams a
  , droneDash :: BaseDroneDashParams a
  , shieldPowerups :: BasePowerupParams a
  , psiStorm :: BasePsiStormParams a
  , flags :: BaseFlagParams a
  , audio :: BaseAudioParams a
  , bots :: BaseBotParams a
  , misc :: BaseMiscParams a
  , z_dev :: BaseTestParams a
  }
  deriving stock (Generic, Functor)
  deriving anyclass (Serialize)

-- | User-specified game parameters, suitable for representation in a .toml
-- file.
--
-- This form of the parameters represents non-integral numbers using 'Double'
-- (instead of 'Fixed') in order to obtain a nicer textual representation for
-- the numbers in the .toml file. The textual representations of 'Fixed' values
-- are long (e.g. 0.4 becomes "0.3999939"), making the parameters file hard to
-- read, while those of 'Double' values are nicely rounded.
newtype TomlGameParams = TomlGameParams (BaseGameParams' Double)
  deriving newtype (Toml.ToTable, Toml.ToValue)

-- Manual instance since newtype deriving doesn't work
instance Toml.FromValue TomlGameParams where
  fromValue =
    fmap TomlGameParams . Toml.parseTableFromValue Toml.genericParseTable

-- | User-specified game parameters, suitable for transmission to clients on
-- different platforms.
--
-- This form of the parameters represents non-integral numbers using 'Fixed'
-- (instead of 'Double') in order to prevent inconsistency between platforms
-- (e.g. operating systems and CPUs) due to differences in floating-point
-- arithmetic. Strictly speaking, transmitting values as 'Fixed' may not be
-- necessary for cross-platform consistency, but it is sufficient, and I just
-- want to a simple way to avoid any cross-platform issues.
type BaseGameParams = BaseGameParams' Fixed

-- | Convert a 'TomlGameParams' into a 'BaseGameParams', making it
-- cross-platform compatible.
makeBaseGameParams :: TomlGameParams -> BaseGameParams
makeBaseGameParams (TomlGameParams params) =
  fmap Fixed.fromDouble params

defaultBaseGameParams :: BaseGameParams
defaultBaseGameParams = makeBaseGameParams defaultTomlGameParams

instance Toml.FromValue (BaseGameParams' Double) where
  fromValue = Toml.parseTableFromValue Toml.genericParseTable

instance Toml.ToValue (BaseGameParams' Double) where
  toValue = Toml.Table . Toml.toTable

instance Toml.ToTable (BaseGameParams' Double) where
  toTable = Toml.genericToTable

defaultTomlGameParams :: TomlGameParams
defaultTomlGameParams =
  TomlGameParams $
    BaseGameParams'
      { overseers = defaultOverseerParams
      , drones = defaultDroneParams
      , obstacles = defaultSoftObstacleParams
      , collisions = defaultDamageParams
      , spawnAreas = defaultSpawnAreaParams
      , teamBases = defaultTeamBaseParams
      , overseerDash = defaultOverseerDashParams
      , droneDash = defaultDroneDashParams
      , psiStorm = defaultPsiStormParams
      , flags = defaultFlagParams
      , audio = defaultAudioParams
      , bots = defaultBotParams
      , shieldPowerups = defaultPowerupParams
      , misc = defaultMiscParams
      , z_dev = defaultTestParams
      }

--------------------------------------------------------------------------------

-- | Game parameters used by the game logic and also for effects. Derived from
-- the combination of `BaseGameParams` and the number of players.
data GameParams = GameParams
  -- The inputs from which the rest are derived
  { gp_gameParamsRaw :: BaseGameParams' Fixed
  , gp_numPlayers :: Int
  , -- Derived parameters (structured)
    gp_dynamics :: DynamicsParams
  , gp_cast :: CastParams
  , gp_combat :: CombatParams
  , gp_flags :: FlagParams
  , gp_audio :: AudioParams
  , gp_bots :: BotParams
  , gp_test :: TestParams
  , -- Derived parameters (miscellaneous)
    gp_gameSize :: Int
  , gp_numSoftObstacles :: Int
  , gp_minIntermissionTicks :: Ticks
  , gp_maxIntermissionTicks :: Ticks
  , gp_flagsToWin :: Int
  , gp_requestFogOfWar :: Bool
  , gp_disassociatedDroneDecayFactor :: Fixed
  , gp_startSpawnDelayTicks :: Ticks
  , gp_overseerDroneLimit :: ClassParameter Int
  , gp_spawnLocationAngle :: Fixed
  , gp_enableTutorialMode :: Bool
  , gp_shieldPowerupHealth :: Fixed
  , gp_shieldPowerupRespawnPeriod :: Ticks
  , gp_enableShieldPowerupSpawners :: Bool
  , gp_enableOverseerDeathShieldPowerupDrops :: Bool
  , gp_overseerDeathShieldPowerupHealth :: Fixed
  , gp_overseerDeathPowerupLifetimeTicks :: Ticks
  , gp_enableDebugData :: Bool
  , gp_enablePlayersVersusBotsMode :: Bool
  , gp_newRound_randomizeTeams :: Bool
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | Derive game parameters from 'BaseGameParams' and the desired number of
-- control points. Any number of control points is acceptable because the value
-- is internally clamped.
makeGameParams :: Int -> BaseGameParams -> GameParams
makeGameParams (max 1 -> nPlayers) gpr@BaseGameParams'{..} =
  GameParams
    { -- The inputs from which the rest are derived
      gp_gameParamsRaw = gpr
    , gp_numPlayers = nPlayers
    , -- Derived parameters (structured)
      gp_dynamics = dynamicsParams'
    , gp_cast = castParams'
    , gp_combat =
        makeCombatParams
          drones
          overseers
          shieldPowerups
          psiStorm
          spawnAreas
    , gp_flags = makeFlagParams flags
    , gp_audio = makeAudioParams audio
    , gp_bots = botParams'
    , gp_test = makeTestParams z_dev
    , -- Derived parameters (miscellaneous)
      gp_gameSize = gameSize
    , gp_numSoftObstacles =
        min (fromIntegral C.maxSoftObstacles) $
          round $
            obstacle_obstaclesPerPlayer * 2 * fromIntegral gameSize
    , gp_minIntermissionTicks = fromIntegral misc_intermission_minDuration_ticks
    , gp_maxIntermissionTicks = fromIntegral misc_intermission_maxDuration_ticks
    , gp_flagsToWin = misc_requiredFlagCapturesForVictory
    , gp_requestFogOfWar = misc_enableFogOfWar
    , gp_disassociatedDroneDecayFactor =
        let ticksUntilDeath =
              fromIntegral drone_maxDissociatedLifetime_ticks
                / fromIntegral C.healthRegenInterval
        in  recip ticksUntilDeath
    , gp_startSpawnDelayTicks = fromIntegral misc_initialJoinDelay_ticks
    , gp_overseerDroneLimit =
        ClassParameter
          { cp_templar = overseer_droneLimit_templar
          , cp_zealot = overseer_droneLimit_zealot
          }
    , gp_spawnLocationAngle = spawnArea_location_degrees * (pi / 180)
    , gp_enableTutorialMode = dev_enableTutorialMode
    , gp_shieldPowerupHealth =
        min shieldPowerup_maxHealth $
          shieldPowerup_spawned_health
            * (fromIntegral teamSize / baseTeamSize)
    , gp_shieldPowerupRespawnPeriod =
        fromIntegral shieldPowerup_spawned_spawningTime_ticks
    , gp_enableShieldPowerupSpawners = shieldPowerup_spawned_enable
    , gp_enableOverseerDeathShieldPowerupDrops = shieldPowerup_dropped_enable
    , gp_overseerDeathShieldPowerupHealth = shieldPowerup_dropped_health
    , gp_overseerDeathPowerupLifetimeTicks = fromIntegral shieldPowerup_dropped_lifetime_ticks
    , gp_enableDebugData = dev_enableDebugData
    , gp_enablePlayersVersusBotsMode = misc_enablePlayersVersusBotsMode
    , gp_newRound_randomizeTeams = misc_newRound_randomizeTeams
    }
  where
    BaseBotParams{..} = bots
    BaseDroneParams{..} = drones
    BaseOverseerParams{..} = overseers
    BasePowerupParams{..} = shieldPowerups
    BaseSoftObstacleParams{..} = obstacles
    BaseSpawnAreaParams{..} = spawnAreas
    BaseTestParams{..} = z_dev
    BaseMiscParams{..} = misc

    baseTeamSize = 8 -- for some parameters that scale with game size
    teamSize =
      max (gp_minTeamSize botParams') $
        (nPlayers + 1) `div` 2 + gp_minBotsPerTeam botParams'
    gameSize =
      max bots_minTeamSize . min (fromIntegral C.maxGameSize) $
        max (misc_worldSize_minPlayers `div` 2) teamSize
    castParams' = makeCastParams drones droneDash overseers overseerDash psiStorm
    botParams' = makeBotParams castParams' bots overseerDash droneDash drones overseers
    dynamicsParams' =
      makeDynamicsParams
        gameSize
        drones
        misc
        collisions
        flags
        overseers
        obstacles
        psiStorm
        shieldPowerups
        spawnAreas
        teamBases

-- | Re-derive game parameters using a different number of control points.
resizeGameParams :: Int -> GameParams -> GameParams
resizeGameParams nPlayers params =
  makeGameParams nPlayers $ gp_gameParamsRaw params

-- | Re-derive game parameters using different base parameters.
rebaseGameParams :: BaseGameParams -> GameParams -> GameParams
rebaseGameParams baseParams oldParams =
  makeGameParams (gp_numPlayers oldParams) baseParams

defaultGameParams :: GameParams
defaultGameParams = makeGameParams 1 defaultBaseGameParams
