{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.PlayerControlState (
  PlayerControlState (..),
  initialControlState,
  sharedDroneChargeTicks,
  DashStamina,
  initialDashStamina,
  lastDashTime,
  isDashReady,
  isDashReset,
  dashReadyTime,
  getCurrentStamina,
  decrementDashStamina,
  resetDash,
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time

data PlayerControlState = PlayerControlState
  { pcs_psiStormCooldownUntil :: Time
  , pcs_overseerDashStamina :: DashStamina
  , pcs_lastDroneCommand :: DroneCommand
  , pcs_lastDroneCommandInitialTime :: Time
  -- ^ Time since the last move command has been held
  , pcs_lastDroneDash :: (Time, Ticks)
  -- ^ Time and shared drone charge ticks of the last drone dash
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- All cooldowns ready
initialControlState :: Time -> PlayerControlState
initialControlState time =
  PlayerControlState
    { pcs_psiStormCooldownUntil = time
    , pcs_overseerDashStamina = initialDashStamina time
    , pcs_lastDroneCommand = DroneNeutral
    , pcs_lastDroneCommandInitialTime = time
    , pcs_lastDroneDash = (initialTime, 0)
    }

sharedDroneChargeTicks ::
  CastParams -> Time -> PlayerControlState -> Maybe Ticks
sharedDroneChargeTicks params time PlayerControlState{..} =
  let chargeTicks =
        min (gp_maxDroneDashChargeTicks params) $
          time `diffTime` pcs_lastDroneCommandInitialTime
  in  if pcs_lastDroneCommand == DroneNeutral && gp_enableDroneDash params
        then Just $! chargeTicks
        else Nothing

data DashStamina = DashStamina
  { ds_lastDashTime :: Time
  , ds_lastReleaseTime :: Time
  -- ^ Last time at which the dash key was not pressed
  , ds_fullStaminaCharges :: Int
  -- ^ At the time of the last dash
  , ds_partialStaminaCharge :: Ticks
  -- ^ At the time of the last dash
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

initialDashStamina :: Time -> DashStamina
initialDashStamina time =
  DashStamina
    { ds_lastDashTime = time
    , ds_lastReleaseTime = time
    , ds_fullStaminaCharges = 0
    , ds_partialStaminaCharge = 0
    }

lastDashTime :: DashStamina -> Time
lastDashTime = ds_lastDashTime

isDashReady :: CastParams -> PlayerClass -> Time -> DashStamina -> Bool
isDashReady castParams playerClass time DashStamina{..}
  | time `diffTime` ds_lastDashTime
      < getClassParam playerClass (gp_overseerDashMinDelayTicks castParams) =
      False
  | ds_fullStaminaCharges >= 1 = True
  | otherwise =
      (time `diffTime` ds_lastDashTime) + ds_partialStaminaCharge
        >= getClassParam playerClass (gp_overseerDashCooldownTicks castParams)

isDashReset :: DashStamina -> Bool
isDashReset DashStamina{..} = ds_lastDashTime <= ds_lastReleaseTime

dashReadyTime :: CastParams -> PlayerClass -> DashStamina -> Time
dashReadyTime castParams playerClass DashStamina{..}
  | ds_fullStaminaCharges >= 1 =
      addTicks
        (getClassParam playerClass (gp_overseerDashMinDelayTicks castParams))
        ds_lastDashTime
  | otherwise =
      let cooldownTicks =
            getClassParam playerClass (gp_overseerDashCooldownTicks castParams)
          waitTicks =
            max
              (getClassParam playerClass (gp_overseerDashMinDelayTicks castParams))
              (cooldownTicks - ds_partialStaminaCharge)
      in  addTicks waitTicks ds_lastDashTime

getCurrentStamina ::
  CastParams -> PlayerClass -> Time -> DashStamina -> (Int, Ticks)
getCurrentStamina castParams playerClass time DashStamina{..} =
  let (accumulatedFullStaminaCharges, accumulatedPartialStaminaCharge) =
        ((time `diffTime` ds_lastDashTime) + ds_partialStaminaCharge)
          `divMod` getClassParam playerClass (gp_overseerDashCooldownTicks castParams)
      maxCharges =
        getClassParam playerClass (gp_overseerDashMaxCharges castParams)
      fullStaminaCharges =
        min maxCharges $
          ds_fullStaminaCharges + fromIntegral accumulatedFullStaminaCharges
      partialStaminaCharge =
        if fullStaminaCharges == maxCharges
          then 0
          else accumulatedPartialStaminaCharge
  in  (fullStaminaCharges, partialStaminaCharge)

decrementDashStamina ::
  CastParams -> PlayerClass -> Time -> DashStamina -> DashStamina
decrementDashStamina params playerClass time dashStamina =
  let (fullStaminaCharges, partialStaminaCharge) =
        getCurrentStamina params playerClass time dashStamina
  in  if fullStaminaCharges >= 1
        then
          DashStamina
            { ds_lastDashTime = time
            , ds_lastReleaseTime = ds_lastReleaseTime dashStamina
            , ds_fullStaminaCharges = fullStaminaCharges - 1
            , ds_partialStaminaCharge = partialStaminaCharge
            }
        else
          DashStamina
            { ds_lastDashTime = time
            , ds_lastReleaseTime = ds_lastReleaseTime dashStamina
            , ds_fullStaminaCharges = 0
            , ds_partialStaminaCharge = 0
            }

-- | Signal that the dash key has been released, allowing the dash to be used
-- again.
resetDash :: Time -> DashStamina -> DashStamina
resetDash time dashStamina = dashStamina{ds_lastReleaseTime = time}
