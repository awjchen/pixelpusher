module Pixelpusher.Game.CombatEvents (
  CombatEvents (..),
  CollisionDamage (..),
  CollisionDamageParticipants (..),
  CombatDeathEvent (..),
  OverseerPsiStormDamage (..),
  PsiStormEffect (..),
) where

import Data.Strict.Tuple (Pair)
import GHC.Generics (Generic, Generically (..))

import Pixelpusher.Custom.Fixed
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Store.EntityID

data CombatEvents = CombatEvents
  { ce_collisions :: [Pair (EntityIDPair CombatEntity) CollisionDamage]
  , ce_deaths :: [CombatDeathEvent]
  , ce_psiStormDamage :: [OverseerPsiStormDamage]
  , ce_psiStormEffects :: [PsiStormEffect]
  }
  deriving stock (Generic)
  deriving (Semigroup, Monoid) via Generically CombatEvents

data CollisionDamage = CollisionDamage
  { cd_position :: Fixed2
  , cd_velocity :: Fixed2
  , cd_damage :: Fixed
  , cd_direction :: Fixed2
  , cd_mActorID1 :: Maybe ActorID
  , cd_mActorID2 :: Maybe ActorID
  , cd_participants :: CollisionDamageParticipants
  , cd_isKillingBlow :: Bool
  }
  deriving stock (Generic)

data CollisionDamageParticipants
  = CollisionDamageOverseers ActorID ActorID
  | CollisionDamageDrones
  | CollisionDamageOverseerDrone ActorID

data CombatDeathEvent = CombatDeathEvent
  { killed_eid :: SEntityID CombatEntity
  , killer_mPid :: Maybe ActorID
  }

-- | For player statistics
data OverseerPsiStormDamage = OverseerPsiStormDamage
  { opsd_overseerID :: EntityID Overseer
  -- ^ The entity in the psi-storm
  , opsd_damage :: Fixed
  , opsd_mPsiStormActorID :: Maybe ActorID
  -- ^ The owner of the psi-storm, if any
  }

-- | For VFX and SFX
data PsiStormEffect = PsiStormEffect
  { pse_mEntityActorID :: Maybe ActorID
  -- ^ The owner of the entity, if any
  , pse_entityPos :: Fixed2
  -- ^ Position of the entity
  , pse_entityVel :: Fixed2
  -- ^ Velocity of the entity
  , pse_damage :: Fixed
  -- ^ Total psi-storm damage
  }
