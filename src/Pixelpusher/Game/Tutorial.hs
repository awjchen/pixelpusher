{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Tutorial (
  TutorialState (..),
  TutorialPhase (..),
  initTutorialState,
  TutorialEffect (..),
  updateTutorialPhase,
  playerTutorialTeam,
  botTutorialTeam,
) where

import Data.Foldable (find)
import Data.Int (Int32)
import Data.Maybe
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.CombatEvents (PsiStormEffect (..))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

playerTutorialTeam :: Team
playerTutorialTeam = TeamE

botTutorialTeam :: Team
botTutorialTeam = oppositeTeam playerTutorialTeam

data TutorialState = TutorialState
  { ts_phase :: TutorialPhase
  , ts_phaseStart :: Time
  , ts_conditionMet :: Bool
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data TutorialPhase
  = Tutorial_Start
  | Tutorial_MoveOverseer
  | Tutorial_MoveOverseerComplete
  | Tutorial_MoveDrones
  | Tutorial_MoveDronesComplete
  | Tutorial_DronesAreHeavy
  | Tutorial_DronesAreHeavyComplete
  | Tutorial_OverseerDash
  | Tutorial_OverseerDashComplete
  | Tutorial_AttackOverseer
  | Tutorial_AttackOverseerComplete
  | Tutorial_CastPsiStorm
  | Tutorial_CastPsiStormComplete
  | Tutorial_KillDroneWithPsiStorm
  | Tutorial_KillDroneWithPsiStormComplete
  | Tutorial_UsePsiStormDefensively
  | Tutorial_UsePsiStormDefensivelyComplete
  | Tutorial_TakeFlag
  | Tutorial_TakeFlagComplete
  | Tutorial_FlagSlow
  | Tutorial_FlagSlowComplete
  | Tutorial_DropFlag
  | Tutorial_DropFlagComplete
  | Tutorial_CaptureFlag
  | Tutorial_CaptureFlagComplete
  | Tutorial_FullControls
  | Tutorial_FullControlsComplete
  | Tutorial_End
  deriving stock (Bounded, Enum, Eq, Generic)
  deriving anyclass (Serialize)

-- TODO: the tutorial should never be run in multiplayer, so this Serialize
-- instance would be unnecessary if not for technical reasons.

initTutorialState :: Time -> TutorialState
initTutorialState time =
  TutorialState
    { ts_phase = Tutorial_Start
    , ts_phaseStart = time
    , ts_conditionMet = False
    }

data TutorialEffect
  = TutorialSpawnEnemyBot
  | TutorialRemoveEnemyBots
  | TutorialSpawnEnemyFlag
  | TutorialSpawnAlliedFlag
  | TutorialSetBotCommandsForPsiStorm
  | TutorialFreezeBots

updateTutorialPhase ::
  Time ->
  WIM.IntMap ActorID Player ->
  [OverseerDeathEvent] ->
  [DroneDeathEvent] ->
  [PsiStormEffect] ->
  [PsiStormCastEvent] ->
  [FlagPickup] ->
  [FlagCapture] ->
  TutorialState ->
  (Maybe TutorialEffect, TutorialState)
updateTutorialPhase
  time
  players
  overseerDeaths
  droneDeaths
  psiStormEffects
  psiStormCasts
  flagPickups
  flagCaptures
  tutorialState =
    transition time tutorialState
    where
      transition :: Time -> TutorialState -> (Maybe TutorialEffect, TutorialState)
      transition =
        case ts_phase tutorialState of
          Tutorial_Start ->
            updateDelay_ (seconds 4)
          Tutorial_MoveOverseer ->
            let hasMovedOverseer =
                  maybe
                    False
                    ( (/= MoveNeutral)
                        . view control_move
                        . control_buttons
                        . player_controls
                        . snd
                    )
                    . find (isActorPlayer . fst)
                    . WIM.toList
                    $ players
            in  updateDelayConditional_ (seconds 3) hasMovedOverseer
          Tutorial_MoveOverseerComplete ->
            updateDelay_ (seconds 1)
          Tutorial_MoveDrones ->
            let hasMovedDrones =
                  maybe
                    False
                    ( (/= DroneNeutral)
                        . view control_drone
                        . control_buttons
                        . player_controls
                        . snd
                    )
                    . find (isActorPlayer . fst)
                    . WIM.toList
                    $ players
            in  updateDelayConditional_ (seconds 3) hasMovedDrones
          Tutorial_MoveDronesComplete ->
            updateDelay_ (seconds 1)
          Tutorial_DronesAreHeavy ->
            updateDelay_ (seconds 4)
          Tutorial_DronesAreHeavyComplete ->
            updateDelay_ (seconds 1)
          Tutorial_OverseerDash ->
            let hasDashed =
                  maybe
                    False
                    ( ( (&&)
                          <$> ((/= MoveNeutral) . view control_move)
                          <*> ((== OverseerDashOn) . view control_overseerDash)
                      )
                        . control_buttons
                        . player_controls
                        . snd
                    )
                    . find (isActorPlayer . fst)
                    . WIM.toList
                    $ players
            in  updateDelayConditional_ (seconds 3) hasDashed
          Tutorial_OverseerDashComplete ->
            updateDelay (seconds 3) TutorialSpawnEnemyBot
          Tutorial_AttackOverseer ->
            let hasDefeatedOverseer =
                  isJust $
                    find ((== botTutorialTeam) . ode_owner_team) overseerDeaths
            in  update
                  (seconds 3)
                  (Just TutorialFreezeBots)
                  (Just TutorialRemoveEnemyBots)
                  hasDefeatedOverseer
          Tutorial_AttackOverseerComplete ->
            updateDelay_ (seconds 3)
          Tutorial_CastPsiStorm ->
            let hasCastPsiStorm = not $ null psiStormCasts
            in  updateDelayConditional_ (seconds 3) hasCastPsiStorm
          Tutorial_CastPsiStormComplete ->
            updateDelay (seconds 3) TutorialSpawnEnemyBot
          Tutorial_KillDroneWithPsiStorm ->
            let psiStormHitOnBotDrone =
                  isJust $
                    find
                      (maybe False isActorBot . pse_mEntityActorID)
                      psiStormEffects
                botDroneDied =
                  isJust $
                    find
                      ((== botTutorialTeam) . dde_team)
                      droneDeaths
                hasStormKilledDrone =
                  psiStormHitOnBotDrone && botDroneDied
            in  update
                  (seconds 3)
                  (Just TutorialSetBotCommandsForPsiStorm)
                  (Just TutorialRemoveEnemyBots)
                  hasStormKilledDrone
          Tutorial_KillDroneWithPsiStormComplete ->
            updateDelay_ (seconds 1)
          Tutorial_UsePsiStormDefensively ->
            updateDelay_ (seconds 5)
          Tutorial_UsePsiStormDefensivelyComplete ->
            updateDelay (seconds 1) TutorialSpawnEnemyFlag
          Tutorial_TakeFlag ->
            let hasPickedUpFlag = not $ null flagPickups
            in  updateDelayConditional_ (seconds 3) hasPickedUpFlag
          Tutorial_TakeFlagComplete ->
            updateDelay_ (seconds 1)
          Tutorial_FlagSlow ->
            updateDelay_ (seconds 3)
          Tutorial_FlagSlowComplete ->
            updateDelay_ (seconds 1)
          Tutorial_DropFlag ->
            let hasDroppedFlag =
                  maybe
                    False
                    ( (== FlagDrop)
                        . view control_flag
                        . control_buttons
                        . player_controls
                        . snd
                    )
                    . find (isActorPlayer . fst)
                    . WIM.toList
                    $ players
            in  updateDelayConditional_ (seconds 3) hasDroppedFlag
          Tutorial_DropFlagComplete ->
            updateDelay (seconds 1) TutorialSpawnAlliedFlag
          Tutorial_CaptureFlag ->
            let hasCapturedFlag = not $ null flagCaptures
            in  updateDelayConditional_ (seconds 3) hasCapturedFlag
          Tutorial_CaptureFlagComplete ->
            updateDelay_ (seconds 1)
          Tutorial_FullControls ->
            updateDelay_ (seconds 4)
          Tutorial_FullControlsComplete ->
            updateDelay_ (seconds 1)
          Tutorial_End ->
            const (Nothing,)

seconds :: Int32 -> Ticks
seconds n = Ticks $ n * C.tickRate_hz

update ::
  Ticks ->
  Maybe TutorialEffect ->
  Maybe TutorialEffect ->
  Bool ->
  Time ->
  TutorialState ->
  (Maybe TutorialEffect, TutorialState)
update
  delay
  nonTransitionEventMaybe
  transitionEventMaybe
  conditionMet
  time
  state@TutorialState{..} =
    let conditionMet' = ts_conditionMet || conditionMet
    in  if time >= addTicks delay ts_phaseStart && conditionMet'
          then (transitionEventMaybe, advancePhase time ts_phase)
          else (nonTransitionEventMaybe, state{ts_conditionMet = conditionMet'})

advancePhase :: Time -> TutorialPhase -> TutorialState
advancePhase time tutorialPhase =
  TutorialState
    { ts_phase =
        if tutorialPhase == maxBound
          then tutorialPhase
          else succ tutorialPhase
    , ts_phaseStart = time
    , ts_conditionMet = False
    }

updateDelayConditional ::
  Ticks ->
  TutorialEffect ->
  Bool ->
  Time ->
  TutorialState ->
  (Maybe TutorialEffect, TutorialState)
updateDelayConditional delay event = update delay Nothing (Just event)

updateDelayConditional_ ::
  Ticks ->
  Bool ->
  Time ->
  TutorialState ->
  (Maybe TutorialEffect, TutorialState)
updateDelayConditional_ delay = update delay Nothing Nothing

updateDelay ::
  Ticks ->
  TutorialEffect ->
  Time ->
  TutorialState ->
  (Maybe TutorialEffect, TutorialState)
updateDelay delay event = updateDelayConditional delay event True

updateDelay_ ::
  Ticks -> Time -> TutorialState -> (Maybe TutorialEffect, TutorialState)
updateDelay_ delay = updateDelayConditional_ delay True
