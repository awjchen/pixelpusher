-- | Scorekeeping
module Pixelpusher.Game.TeamScore (
  TeamScore (..),
  zeroTeamScore,
  incrementTeamScore,
  scaleTeamScore,
) where

import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Game.Team

--------------------------------------------------------------------------------

data TeamScore = TeamScore
  { teamScore_e :: Int
  , teamScore_w :: Int
  }
  deriving stock (Generic, Show)
  deriving anyclass (Serialize)

instance Semigroup TeamScore where
  (<>) (TeamScore a1 b1) (TeamScore a2 b2) = TeamScore (a1 + a2) (b1 + b2)

instance Monoid TeamScore where
  mempty = TeamScore 0 0

zeroTeamScore :: TeamScore
zeroTeamScore = TeamScore 0 0

incrementTeamScore :: Team -> TeamScore -> TeamScore
incrementTeamScore team score = case team of
  TeamW -> score & field @"teamScore_w" +~ 1
  TeamE -> score & field @"teamScore_e" +~ 1

scaleTeamScore :: Int -> TeamScore -> TeamScore
scaleTeamScore factor score =
  TeamScore
    { teamScore_e = factor * teamScore_e score
    , teamScore_w = factor * teamScore_w score
    }
