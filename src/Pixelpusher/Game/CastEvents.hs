module Pixelpusher.Game.CastEvents (
  CastEvent (..),
  PsiStormCastEvent (..),
  DashCastEvent (..),
  DashType (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Game.Team (Team)

data CastEvent
  = PsiStormCastEvent_ PsiStormCastEvent
  | DashCastEvent_ DashCastEvent

newtype PsiStormCastEvent = PsiStormCastEvent
  { psce_pos :: Fixed2
  }

data DashCastEvent = DashCastEvent
  { dce_pos :: Fixed2
  , dce_vel :: Fixed2
  , dce_entityRadius :: Fixed
  , dce_direction :: Fixed2
  , dce_dashDeltaV :: Fixed
  , dce_effectsMagnitudeFrac :: Fixed
  -- ^ VFX, SFX
  , dce_dashType :: DashType
  , dce_team :: Team
  }

data DashType = OverseerDash | DroneDash
  deriving stock (Generic)
  deriving anyclass (Serialize)
