module Pixelpusher.Game.GameEvents (
  GameEvents (..),
  GamePhaseTransition (..),
  OverseerDeathEvent (..),
  OverseerHitEvent (..),
  DroneDeathEvent (..),
  ShieldPickupEvent (..),
  FlagDropEvent (..),
  KillEvent (..),
) where

import GHC.Generics (Generic, Generically (..))
import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CastEvents (DashCastEvent, PsiStormCastEvent)
import Pixelpusher.Game.CollisionEvents (NeutralCollision)
import Pixelpusher.Game.CombatEvents (CollisionDamage, PsiStormEffect)
import Pixelpusher.Game.FlagEvents (FlagCapture, FlagPickup, FlagRecovery)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

data GameEvents = GameEvents
  { ge_gamePhaseTransitions :: [GamePhaseTransition]
  -- ^ For the server (game manager)
  , ge_collisions :: [CollisionDamage]
  , ge_neutralCollisions :: [NeutralCollision]
  , ge_overseerDeaths :: [OverseerDeathEvent]
  , ge_dashes :: [DashCastEvent]
  , ge_psiStormCasts :: [PsiStormCastEvent]
  , ge_psiStormEffects :: [PsiStormEffect]
  , ge_flagCaptures :: [FlagCapture]
  , ge_flagPickups :: [FlagPickup]
  , ge_flagRecoveries :: [FlagRecovery]
  , ge_shieldPickups :: [ShieldPickupEvent]
  , ge_flagDrops :: [FlagDropEvent]
  }
  deriving stock (Generic)
  deriving (Semigroup, Monoid) via Generically GameEvents

data OverseerDeathEvent = OverseerDeathEvent
  { ode_time :: Time
  , ode_pos :: Fixed2
  , ode_vel :: Fixed2
  , ode_radius :: Fixed
  , ode_owner :: ActorID
  , ode_owner_team :: Team
  , ode_owner_class :: PlayerClass
  , ode_killer :: Maybe ActorID
  , ode_entityID :: Int
  }

data DroneDeathEvent = DroneDeathEvent
  { dde_time :: Time
  , dde_eid :: Int
  -- ^ Weakened `EntityID`
  , dde_radius :: Fixed
  , dde_pos :: Fixed2
  , dde_vel :: Fixed2
  , dde_team :: Team
  , dde_class :: PlayerClass
  , dde_recentDamage :: Fixed
  }

-- | An event for when an overseer gets hit, for the purposes of tracking
-- player stats
data OverseerHitEvent = OverseerHitEvent
  { ohe_attacker :: Maybe ActorID
  , ohe_defender :: ActorID
  , ohe_damage :: Fixed
  }

data GamePhaseTransition
  = -- | The match has ended
    MatchEnd
      -- | Winner
      Team
  | -- | Timing for match end sfx (hack)
    PlayMatchEndSfx
  | -- | The game intermission is about to end
    IntermissionEnding
  | -- | The game is restarting now
    GameRestart
  deriving stock (Eq)

data ShieldPickupEvent = ShieldPickupEvent
  { spe_overseerOwner :: ActorID
  , spe_pos :: Fixed2
  , spe_radius :: Fixed
  }

data FlagDropEvent = FlagDropEvent
  { fde_actor :: ActorID
  , fde_pos :: Fixed2
  , fde_vel :: Fixed2
  }

data KillEvent = KillEvent
  { ke_death :: OverseerDeathEvent
  , ke_assisters :: WIM.IntMap ActorID ()
  }
