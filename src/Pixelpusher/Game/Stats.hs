{-# LANGUAGE GADTs #-}

module Pixelpusher.Game.Stats (
  recordStats,
) where

import Control.Monad (guard)
import Control.Monad.State.Class
import Control.Monad.Trans.State.Strict (State)
import Data.Foldable (foldl')
import Data.Generics.Product.Fields
import Data.Traversable (for)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.GameEvents
import Pixelpusher.Game.Player
import Pixelpusher.Game.PlayerStats
import Pixelpusher.Game.Time

recordStats ::
  Time ->
  [OverseerDeathEvent] ->
  [OverseerHitEvent] ->
  FlagEvents ->
  State (WIM.IntMap ActorID Player) [KillEvent]
recordStats time overseerDeaths overseerHits flagEvents = do
  -- Overseer hits
  -- Must be recorded before overseers deaths for proper assist attributions
  modify' $ \players ->
    foldl' (flip (recordOverseerHit time)) players overseerHits

  -- Overseer deaths
  killAssistEvents <-
    for overseerDeaths $ \overseerDeath ->
      state $ recordKill time overseerDeath

  -- Capture-the-flag contributions
  modify' $ recordFlagStats flagEvents

  -- Return
  pure killAssistEvents

-- | Update player kills, deaths, and assists resulting from an overseer death.
recordKill ::
  Time ->
  OverseerDeathEvent ->
  WIM.IntMap ActorID Player ->
  (KillEvent, WIM.IntMap ActorID Player)
recordKill time death players =
  case definiteKillerMaybe of
    Nothing ->
      let assisters = recentEnemyDamagers
          killAssistEvent =
            KillEvent
              { ke_death = death
              , ke_assisters = assisters
              }
          players' =
            players
              & addDeath
              & flip (foldl' (flip addAssist)) (WIM.keys assisters)
      in  -- TODO: Instead of an assist, assign a kill to the enemy that most
          -- recently damaged the killed overseer, if any
          (killAssistEvent, players')
    Just killerID ->
      let assisters =
            WIM.filterWithKey
              (\damagerID () -> damagerID /= killerID)
              recentEnemyDamagers
          killAssistEvent =
            KillEvent
              { ke_death = death
              , ke_assisters = assisters
              }
          players' =
            players
              & addKill killerID
              & addDeath
              & flip (foldl' (flip addAssist)) (WIM.keys assisters)
      in  (killAssistEvent, players')
  where
    definiteKillerMaybe :: Maybe ActorID
    definiteKillerMaybe = do
      killerID <- ode_killer death
      killerPlayer <- WIM.lookup killerID players
      guard $ view player_team killerPlayer /= ode_owner_team death
      pure killerID

    recentEnemyDamagers :: WIM.IntMap ActorID ()
    recentEnemyDamagers =
      WIM.map (const ()) $
        maybe
          mempty
          ( WIM.filterWithKey
              ( \actorID lastDamageTime ->
                  let isRecent =
                        time `diffTime` lastDamageTime
                          <= Ticks (C.tickRate_hz * 4) -- 4 seconds (arbitrary)
                      isEnemy =
                        maybe
                          False
                          (\player -> view player_team player /= ode_owner_team death)
                          (WIM.lookup actorID players)
                  in  isRecent && isEnemy
              )
              . getDamageTakenTimestamps
              . player_stats
          )
          (WIM.lookup (ode_owner death) players)

    addKill ::
      ActorID -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
    addKill killerID =
      ix killerID . field @"player_stats" %~ incrementKills

    addDeath ::
      WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
    addDeath =
      ix (ode_owner death) . field @"player_stats" %~ incrementDeaths

    addAssist :: ActorID -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
    addAssist actorID = ix actorID . field @"player_stats" %~ incrementAssists

recordOverseerHit ::
  Time ->
  OverseerHitEvent ->
  WIM.IntMap ActorID Player ->
  WIM.IntMap ActorID Player
recordOverseerHit
  time
  OverseerHitEvent
    { ohe_attacker = mAttacker
    , ohe_defender = defender
    , ohe_damage = damage
    } =
    if mAttacker == Just defender
      then addDmgTaken
      else addDmgTaken . recordHitTime . addDmgDealt
    where
      addDmgTaken :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
      addDmgTaken = ix defender . field @"player_stats" %~ addDamageTaken damage

      recordHitTime :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
      recordHitTime =
        case mAttacker of
          Nothing -> id
          Just attacker ->
            ix defender
              . field @"player_stats"
              . field @"ps_damageTakenTimestamps"
              . at attacker
              ?~ time

      addDmgDealt :: WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
      addDmgDealt = case mAttacker of
        Nothing -> id
        Just attacker -> ix attacker . field @"player_stats" %~ addDamageDealt damage

recordFlagStats ::
  FlagEvents -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagStats flagEvents =
  recordFlagRecoveries (flagEvents_recoveries flagEvents)
    . recordFlagCaptures (flagEvents_captures flagEvents)

recordFlagCaptures ::
  [FlagCapture] -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagCaptures flagCaptures players =
  foldl' (flip recordFlagCapture) players flagCaptures
  where
    recordFlagCapture (FlagCapture{flagCapture_actorID}) =
      ix flagCapture_actorID . field @"player_stats" %~ incrementFlagsCaptured

recordFlagRecoveries ::
  [FlagRecovery] -> WIM.IntMap ActorID Player -> WIM.IntMap ActorID Player
recordFlagRecoveries flagRecoveries players =
  foldl' (flip recordFlagRecovery) players flagRecoveries
  where
    recordFlagRecovery (FlagRecovery _ mPid _) =
      maybe
        id
        (\pid -> ix pid . field @"player_stats" %~ incrementFlagsRecovered)
        mPid
