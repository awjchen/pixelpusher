{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.MapPowerups (
  MapPowerupLocation (..),
  mapPowerupSpawnLocation,
  mapPowerupSpawnLocations,
  mapPowerupSpawnLocationsNE,
  MapPowerupStatus (..),
  MapPowerupStatuses (..),
  initMapPowerupStatuses,
  readMapPowerupStatus,
  setMapPowerupStatus,
  traverseWithKeyMapPowerupStatuses,
) where

import Data.Foldable (toList)
import Data.Hashable (Hashable)
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed2 (Fixed2))
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Time (Time)

--------------------------------------------------------------------------------

data MapPowerupLocation
  = MapPowerupLoc_North
  | MapPowerupLoc_South
  deriving stock (Eq, Ord, Generic)
  deriving anyclass (Hashable, Serialize)

mapPowerupSpawnLocation :: DynamicsParams -> MapPowerupLocation -> Fixed2
mapPowerupSpawnLocation params =
  let northPos = mapPowerupSpawnLocationNorth params
  in  \case
        MapPowerupLoc_North -> northPos
        MapPowerupLoc_South -> -northPos

mapPowerupSpawnLocations :: DynamicsParams -> [(MapPowerupLocation, Fixed2)]
mapPowerupSpawnLocations params = toList (mapPowerupSpawnLocationsNE params)

mapPowerupSpawnLocationsNE :: DynamicsParams -> NonEmpty (MapPowerupLocation, Fixed2)
mapPowerupSpawnLocationsNE params =
  let northPos = mapPowerupSpawnLocationNorth params
  in  (MapPowerupLoc_North, northPos) :| [(MapPowerupLoc_South, -northPos)]

mapPowerupSpawnLocationNorth :: DynamicsParams -> Fixed2
mapPowerupSpawnLocationNorth dynParams =
  let worldRadius = gp_worldRadius dynParams
      radiusFrac = gp_shieldPowerupWorldRadiusFrac dynParams
      r = radiusFrac * worldRadius
  in  Fixed2 0 r

--------------------------------------------------------------------------------

data MapPowerupStatus
  = MapPowerupConsumed
      -- | Time consumed
      Time
  | MapPowerupSpawned
      -- | Time spawned
      Time
  deriving stock (Generic)
  deriving anyclass (Serialize)

data MapPowerupStatuses = MapPowerupStatuses
  { mps_north :: MapPowerupStatus
  , mps_south :: MapPowerupStatus
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

initMapPowerupStatuses :: Time -> MapPowerupStatuses
initMapPowerupStatuses time =
  MapPowerupStatuses
    { mps_north = MapPowerupConsumed time
    , mps_south = MapPowerupConsumed time
    }

readMapPowerupStatus ::
  MapPowerupLocation -> MapPowerupStatuses -> MapPowerupStatus
readMapPowerupStatus loc MapPowerupStatuses{..} =
  case loc of
    MapPowerupLoc_North -> mps_north
    MapPowerupLoc_South -> mps_south

setMapPowerupStatus ::
  MapPowerupLocation ->
  MapPowerupStatus ->
  MapPowerupStatuses ->
  MapPowerupStatuses
setMapPowerupStatus loc status locMap =
  case loc of
    MapPowerupLoc_North -> locMap{mps_north = status}
    MapPowerupLoc_South -> locMap{mps_south = status}

traverseWithKeyMapPowerupStatuses ::
  (Applicative f) =>
  (MapPowerupLocation -> MapPowerupStatus -> f MapPowerupStatus) ->
  MapPowerupStatuses ->
  f MapPowerupStatuses
traverseWithKeyMapPowerupStatuses f mps = do
  mps_north' <- f MapPowerupLoc_North (mps_north mps)
  mps_south' <- f MapPowerupLoc_South (mps_south mps)
  pure
    MapPowerupStatuses
      { mps_north = mps_north'
      , mps_south = mps_south'
      }
