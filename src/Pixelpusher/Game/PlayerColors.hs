-- | Colors of player entities
module Pixelpusher.Game.PlayerColors (
  PlayerColor (..),
  ColorVariant (..),
  defaultColorVariant,
  leastCommonColorVariant,
) where

import Data.Foldable (foldl', minimumBy, toList)
import Data.Function (on)
import Data.List.NonEmpty qualified as NonEmpty
import Data.Map.Strict qualified as Map
import Data.Maybe (fromMaybe)
import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Team

--------------------------------------------------------------------------------

-- | Abstract colors. Mapped to concrete colors in the game client, taking into
-- context.
data PlayerColor = PlayerColor Team ColorVariant ActorID
  deriving stock (Generic)
  deriving anyclass (Serialize)

data ColorVariant = Color1 | Color2 | Color3 | Color4 | Color5 | Color6
  deriving stock (Bounded, Enum, Eq, Generic, Ord)
  deriving anyclass (Serialize)

defaultColorVariant :: ColorVariant
defaultColorVariant = Color1

--------------------------------------------------------------------------------

leastCommonColorVariant :: (Foldable f) => f ColorVariant -> ColorVariant
leastCommonColorVariant = fromMaybe defaultColorVariant . leastCommon
{-# INLINEABLE leastCommonColorVariant #-}

leastCommon :: (Foldable f, Ord a, Bounded a, Enum a) => f a -> Maybe a
leastCommon xs =
  fmap (fst . minimumBy (compare `on` snd)) $
    NonEmpty.nonEmpty $
      Map.toList $
        foldl' (flip (Map.alter increment)) Map.empty $
          [minBound .. maxBound] ++ toList xs
  where
    increment :: Maybe Int -> Maybe Int
    increment = Just . maybe 0 (+ 1)
