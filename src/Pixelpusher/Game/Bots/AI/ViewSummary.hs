{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.ViewSummary (
  ViewSummary (..),
  AllyInfo (..),
  makeViewSummary,
) where

import Data.Foldable (foldl', toList)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Parameters

import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.Threats

--------------------------------------------------------------------------------

data ViewSummary = ViewSummary
  { vs_myInfo :: AllyInfo
  , vs_allyInfo :: WIM.IntMap ActorID AllyInfo
  , vs_totalAlliedHealth :: Fixed
  , vs_totalEnemyHealth :: Fixed
  , vs_enemyAlliedHealthRatio :: Fixed
  , vs_objectivePotentialScaling :: Fixed
  -- ^ Reciprocal of vs_avoidancePotentialScaling
  , vs_avoidancePotentialScaling :: Fixed
  -- ^ Reciprocal of vs_objectivePotentialScaling
  }

data AllyInfo = AllyInfo
  { ai_overseer :: OverseerBotView
  , ai_restingPosition :: Fixed2
  , ai_threateningOverseers :: [OverseerBotView]
  , ai_threateningOverseersTotalHealth :: Fixed
  , ai_threateningDrones :: [DroneBotView]
  , ai_threateningDronesTotalHealth :: Fixed
  }

makeViewSummary :: GameParams -> RefinedBotView -> ViewSummary
makeViewSummary params botView@RefinedBotView{..} =
  let botParams = gp_bots params
      vs_myInfo = makeAllyInfo botParams botView rbv_myOverseer
      vs_allyInfo =
        WIM.fromList $
          map
            (\obv -> (obv_actorID obv, makeAllyInfo botParams botView obv))
            rbv_alliedOverseers

      vs_totalAlliedHealth =
        foldl' (+) 0 (map obv_totalOverseerHealth (rbv_myOverseer : rbv_alliedOverseers))
          + foldl' (+) 0 (map dbv_health (maybe [] (toList . rbdv_myDrones) rbv_myDrones ++ rbv_alliedDrones))
      vs_totalEnemyHealth =
        foldl' (+) 0 (map obv_totalOverseerHealth rbv_enemyOverseers)
          + foldl' (+) 0 (map dbv_health rbv_enemyDrones)

      baseHealth = gp_overseerMaxHealth_base (gp_combat params)

      vs_enemyAlliedHealthRatio =
        (vs_totalEnemyHealth + baseHealth)
          / (vs_totalAlliedHealth + baseHealth)

      (vs_objectivePotentialScaling, vs_avoidancePotentialScaling) =
        let logEnemyAlliedHealthRatio =
              log (vs_totalEnemyHealth + baseHealth)
                - log (vs_totalAlliedHealth + baseHealth)
            logThreateningDronesHealthRatio =
              0.5
                * ( log (max 1 (ai_threateningDronesTotalHealth vs_myInfo))
                      - log (max 1 (obv_totalOverseerHealth rbv_myOverseer))
                  )
            logObjScaling =
              (* (-1.5)) $ -- arbitrary
                max (-1) . min 1 $
                  if logThreateningDronesHealthRatio > 0
                    then max logEnemyAlliedHealthRatio logThreateningDronesHealthRatio
                    else logEnemyAlliedHealthRatio
            logAvoidScaling = -logObjScaling
        in  (exp logObjScaling, exp logAvoidScaling)
  in  ViewSummary{..}

makeAllyInfo :: BotParams -> RefinedBotView -> OverseerBotView -> AllyInfo
makeAllyInfo botParams RefinedBotView{..} alliedOverseer =
  let ai_overseer = alliedOverseer
      ai_restingPosition =
        overseerRestingPosition
          (getClassParam (obv_class alliedOverseer) (gp_overseerDragParams botParams))
          alliedOverseer
      ai_threateningOverseers =
        filter
          (isOverseerThreatening botParams ai_restingPosition)
          rbv_enemyOverseers
      ai_threateningDrones =
        filter
          (isDroneThreatening botParams ai_restingPosition)
          rbv_enemyDrones
      ai_threateningDronesTotalHealth =
        foldl' (+) 0 $ map dbv_health ai_threateningDrones
      ai_threateningOverseersTotalHealth =
        foldl' (+) 0 $ map obv_totalOverseerHealth ai_threateningOverseers
  in  AllyInfo{..}
