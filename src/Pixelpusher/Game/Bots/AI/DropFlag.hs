{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.DropFlag (
  computeFlagCommand,
) where

import Data.Foldable (foldl')

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.TeamLocations

import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.Threats (
  isDroneThreatening,
  overseerRestingPosition,
 )
import Pixelpusher.Game.Bots.AI.ViewSummary

computeFlagCommand ::
  GameParams -> RefinedBotView -> ViewSummary -> FlagCommand
computeFlagCommand params RefinedBotView{..} ViewSummary{..} =
  let dynParams = gp_dynamics params
      botParams = gp_bots params

      canScore =
        case fbv_type rbv_alliedFlag of
          BaseFlagType -> True
          DroppedFlagType -> False
          OverseerFlagType -> False

      myPos = obv_pos rbv_myOverseer
      myProjectedPos =
        overseerRestingPosition
          (getClassParam (obv_class rbv_myOverseer) (gp_overseerDragParams botParams))
          rbv_myOverseer

      myTeam = obv_team rbv_myOverseer
      alliedBasePos = teamBaseLocation dynParams myTeam
      myClass = obv_class rbv_myOverseer
      myRadius = getClassParam myClass (gp_overseerRadius dynParams)
      captureRadius = gp_teamBaseRadius dynParams + myRadius

      distFromScore =
        max 0 $ Fixed.norm (myPos - alliedBasePos) - captureRadius
      noReturnDistance = 120
      noReturn = canScore && distFromScore < noReturnDistance

      lethalThreat =
        let myHealth = obv_totalOverseerHealth rbv_myOverseer
            isAlliedDroneNearby dbv =
              Fixed.normLeq (dbv_pos dbv - myPos) 100 -- arbitrary, needs adjustment
            nearbyAlliedDroneHealth =
              foldl' (+) 0 $
                map dbv_health $
                  filter
                    ( \dbv ->
                        isAlliedDroneNearby dbv
                          || isDroneThreatening botParams myProjectedPos dbv
                    )
                    rbv_alliedDrones
            threateningHealth =
              ai_threateningDronesTotalHealth vs_myInfo
                + ai_threateningOverseersTotalHealth vs_myInfo
                - nearbyAlliedDroneHealth
            boundHealthFactor = if canScore then min 1 else id
            healthFactor =
              boundHealthFactor $
                let scoreFrac =
                      Fixed.linearStep
                        (noReturnDistance + 170)
                        noReturnDistance
                        distFromScore
                    generalProximityFrac =
                      let width = 2 * gp_teamBaseDistanceFromCenter dynParams
                      in  Fixed.linearStep
                            (noReturnDistance + width)
                            noReturnDistance
                            distFromScore
                in  0.6
                      + 0.4 * generalProximityFrac
                      + 1.4 * scoreFrac
        in  (myHealth * healthFactor) < threateningHealth
  in  if not noReturn && lethalThreat
        then FlagDrop
        else FlagNeutral
