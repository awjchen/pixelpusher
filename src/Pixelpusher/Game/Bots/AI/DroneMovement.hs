{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.DroneMovement (
  droneMovement,
  DroneTargetMode (..),
  DroneMoveResult (..),
  DroneMoveCandidate (..),
) where

import Control.Monad.ST
import Data.Foldable (foldl', minimumBy)
import Data.Function (on)
import Data.Generics.Product.Fields
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.List qualified as List
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe
import Data.Serialize (Serialize)
import Data.Strict.Tuple (Pair ((:!:)))
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Parameters.Cast (droneDashDeltaV, psiStormLastDamageTime)
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControls (DroneCommand (..))
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.Bots.AI.DroneTarget
import Pixelpusher.Game.Bots.AI.Kinematics
import Pixelpusher.Game.Bots.AI.Pathfinding
import Pixelpusher.Game.Bots.AI.RefinedView

--------------------------------------------------------------------------------
-- Drone movement
--
data DroneTargetMode = DirectTargeting | LeadTargeting
  deriving stock (Generic)
  deriving anyclass (Serialize)

data DroneMoveResult = DroneMoveResult
  { dmr_moveCandidate :: DroneMoveCandidate
  , dmr_staticPotential :: Fixed
  , dmr_targetingPotential :: Fixed
  }
  deriving stock (Generic)

dmr_totalPotential :: DroneMoveResult -> Fixed
dmr_totalPotential dmr = dmr_staticPotential dmr + dmr_targetingPotential dmr

droneMovement ::
  GameParams ->
  MWC.Gen s ->
  RefinedBotView ->
  DroneTargetMode ->
  DroneTarget ->
  Ticks ->
  IntMap Fixed ->
  ST
    s
    ( Maybe (DroneMoveCandidate, PathfindingResult, [(Fixed2, Fixed)])
    , IntMap Fixed
    )
droneMovement params gen botView@RefinedBotView{..} targetMode droneTarget chargeTicks oldTargetingPotentials =
  case rbv_myDrones of
    Nothing ->
      let updateWeight = gp_botDroneControlUpdateWeight (gp_bots params)
      in  pure (Nothing, IntMap.map (* (1 - updateWeight)) oldTargetingPotentials)
    Just RefinedBotDronesView{..} -> do
      let myClass = obv_class rbv_myOverseer
          myDroneParams = getClassParam myClass (gp_droneDragParams (gp_bots params))
      let -- Weigh velolcities by proximity to median position
          aggregateVel =
            let f :: (Fixed2, Fixed) -> DroneBotView -> (Fixed2, Fixed)
                f (!velAcc, !weightAcc) drone =
                  let scaling dist
                        | dist < 0.01 = 100
                        | otherwise = recip dist
                      weight' = scaling $ Fixed.norm $ dbv_pos drone - rbdv_medianPos
                      vel = Fixed.map (* weight') (dbv_vel drone)
                  in  (velAcc + vel, weightAcc + weight')
            in  (\(totalVel, totalWeight) -> Fixed.map (/ totalWeight) totalVel) $
                  foldl' f (0, 0) rbdv_myDrones
          moveCandidates =
            droneMoveCandidates
              params
              myClass
              myDroneParams
              chargeTicks
              (obv_pos rbv_myOverseer)
              rbdv_medianPos
              aggregateVel
      targetDronePos <-
        targetDronePosition params gen botView targetMode droneTarget

      let obstacles =
            let softObstacles =
                  flip map rbv_softObstacles $ \sobv ->
                    Obstacle
                      { obs_id = getIndex (sobv_obstacleID sobv)
                      , obs_pos = sobv_pos sobv
                      , obs_radius = gp_softObstacleRadius (gp_dynamics params)
                      , obs_intersectionsTraversable = True
                      }
                psiStorms =
                  flip mapMaybe rbv_psiStorms $ \ps ->
                    let fullRadius = gp_psiStormRadius (gp_dynamics params)
                        lastDamageTime = psiStormLastDamageTime (gp_cast params) (psbv_castTime ps)
                        stillExistsAtControlTime = rbv_controlTime <= lastDamageTime
                        diff = rbdv_restingMedianPos - psbv_pos ps
                        dronesInside = Fixed.normLeq diff (0.7 * fullRadius) -- arbitrary
                        reachableInLifetime =
                          let remainingLifeTimeTicks =
                                lastDamageTime `diffTime` rbv_predictionTime
                              speed = ldp_terminalVelocity myDroneParams
                              moveRange = speed * fromIntegral remainingLifeTimeTicks
                          in  Fixed.normLeq diff (moveRange + fullRadius)
                    in  if stillExistsAtControlTime
                          && not dronesInside
                          && reachableInLifetime
                          then
                            Just
                              Obstacle
                                { obs_id = getIndex (psbv_id ps)
                                , obs_pos = psbv_pos ps
                                , obs_radius = fullRadius
                                , obs_intersectionsTraversable = False
                                }
                          else Nothing
            in  softObstacles ++ psiStorms
          dronePathfinding =
            pathfind
              (gp_dynamics params)
              obstacles
              rbdv_restingMedianPos
              targetDronePos
          immediateTargetDronePos =
            let path = fromMaybe [] (pr_path dronePathfinding)
                farPath =
                  flip List.dropWhile path $ \pos ->
                    Fixed.normLeq (pos - rbdv_medianPos) 84 -- arbitrary radius
            in  case farPath of
                  [] -> targetDronePos
                  firstWaypoint : _ -> firstWaypoint

          targetingPotential' = targetingPotential immediateTargetDronePos

          staticPotential pos =
            let filteredPsiStorms =
                  filter
                    (not . ignorePsiStorm params botView droneTarget)
                    rbv_psiStorms
            in  psiStormAvoidancePotential params filteredPsiStorms pos
                  + obstacleAvoidancePotential params rbv_softObstacles pos

          potentials :: IntMap (Either Fixed DroneMoveResult)
          potentials =
            let newPotentials =
                  IntMap.map
                    ( \moveCandidate ->
                        DroneMoveResult
                          { dmr_moveCandidate = moveCandidate
                          , dmr_staticPotential =
                              staticPotential (dmc_pos moveCandidate)
                          , dmr_targetingPotential =
                              targetingPotential' (dmc_pos moveCandidate)
                          }
                    )
                    moveCandidates
                updateWeight = gp_botDroneControlUpdateWeight (gp_bots params)
            in  IntMap.mergeWithKey
                  ( \_k oldTargetingPotential dmr ->
                      let combinedTargetingPotential =
                            (1 - updateWeight) * oldTargetingPotential
                              + updateWeight * dmr_targetingPotential dmr
                      in  Just . Right $
                            dmr & field @"dmr_targetingPotential" .~ combinedTargetingPotential
                  )
                  (IntMap.map (Left . (* (1 - updateWeight))))
                  ( IntMap.map
                      (Right . over (field @"dmr_targetingPotential") (* updateWeight))
                  )
                  oldTargetingPotentials
                  newPotentials

          newTargetingPOtentials =
            IntMap.map (either id dmr_targetingPotential) potentials
          moveResults =
            IntMap.mapMaybe (either (const Nothing) Just) potentials

          chosenMove =
            dmr_moveCandidate $
              minimumBy (compare `on` dmr_totalPotential) moveResults

          debugMovePotentials =
            map
              (\dmr -> (dmc_pos (dmr_moveCandidate dmr), dmr_totalPotential dmr))
              (IntMap.elems moveResults)

      pure
        ( Just
            ( chosenMove
            , dronePathfinding
            , debugMovePotentials
            )
        , newTargetingPOtentials
        )

--------------------------------------------------------------------------------

targetingPotential :: Fixed2 -> Fixed2 -> Fixed
targetingPotential targetPos testPos = Fixed.norm (testPos - targetPos)

-- Heuristic for choosing a drone position for attacking an overseer target
targetDronePosition ::
  GameParams ->
  MWC.Gen s ->
  RefinedBotView ->
  DroneTargetMode ->
  DroneTarget ->
  ST s Fixed2
targetDronePosition params gen botView targetMode droneTarget = do
  let target = targetDronePosition' params botView targetMode droneTarget
  offset <- randDroneTargetOffset gen
  pure $! target + offset

targetDronePosition' ::
  GameParams -> RefinedBotView -> DroneTargetMode -> DroneTarget -> Fixed2
targetDronePosition' params RefinedBotView{..} targetMode droneTarget =
  let dynParams = gp_dynamics params
      (ovTarget, targetingOffsetDir) =
        case droneTarget of
          DroneTarget_Enemy ov ->
            (ov, 0)
          DroneTarget_Ally ov overseerThreats droneThreats ->
            (ov, threatOffsetDir ov overseerThreats droneThreats)
      ovRadius =
        getClassParam (obv_class ovTarget) (gp_overseerRadius dynParams)
      targetingOffset =
        let spacing = gp_droneRadius_base (gp_dynamics params)
        in  Fixed.map (* (ovRadius + spacing)) targetingOffsetDir

      predictionTicks = 20
      stepTicks =
        if
          | obv_hasFlag ovTarget -> predictionTicks * 0.7
          | obv_team ovTarget == obv_team rbv_myOverseer ->
              predictionTicks
          | otherwise ->
              case targetMode of
                DirectTargeting -> predictionTicks
                LeadTargeting -> 2 * predictionTicks
      projOverseerPos :!: projOverseerVel =
        forwardSimulateKinematics
          (getClassParam (obv_class ovTarget) (gp_overseerDragParams (gp_bots params)))
          (obv_accelDir ovTarget)
          stepTicks
          (obv_pos ovTarget :!: obv_vel ovTarget)
      ovLeadingPos =
        projOverseerPos + Fixed.map (* predictionTicks) projOverseerVel
  in  ovLeadingPos + targetingOffset

threatOffsetDir ::
  OverseerBotView -> [OverseerBotView] -> [DroneBotView] -> Fixed2
threatOffsetDir threatTarget overseerThreats droneThreats =
  let targetPos = obv_pos threatTarget
      (totalThreatHealth, totalThreatHealthDir) =
        let f (!healthAcc, !healthDirAcc) (health, pos) =
              ( healthAcc + health
              , healthDirAcc
                  + Fixed.map (* health) (Fixed.normalize (pos - targetPos))
              )
        in  foldl' f (0, 0) $
              map (\obv -> (obv_health obv, obv_pos obv)) overseerThreats
                ++ map (\dbv -> (dbv_health dbv, dbv_pos dbv)) droneThreats
  in  if totalThreatHealth == 0
        then 0
        else Fixed.map (/ totalThreatHealth) totalThreatHealthDir

randDroneTargetOffset :: MWC.Gen s -> ST s Fixed2
randDroneTargetOffset gen = do
  offsetDir <- Fixed.angle . Fixed.fromDouble <$> MWC.uniformR @Double (0, 2 * pi) gen
  offsetSize <- Fixed.fromDouble <$> MWC.uniformR @Double (0, 16) gen -- arbitrary
  pure $ Fixed.map (* offsetSize) offsetDir

--------------------------------------------------------------------------------

-- | Whether drones should ignore a psi-storm when pursuing a target
--
-- TODO: This does not handle overlapping psi-storms
ignorePsiStorm ::
  GameParams -> RefinedBotView -> DroneTarget -> PsiStormBotView -> Bool
ignorePsiStorm params RefinedBotView{..} droneTarget psiStorm =
  case rbv_myDrones of
    Nothing -> True
    Just RefinedBotDronesView{..} ->
      case droneTarget of
        DroneTarget_Ally{} -> False
        DroneTarget_Enemy targetOverseer ->
          let predictionTicks = 48
              projectedTargetPos =
                obv_pos targetOverseer
                  + Fixed.map (* predictionTicks) (obv_vel targetOverseer)
              targetInPsiStorm =
                Fixed.normLeq
                  (psbv_pos psiStorm - projectedTargetPos)
                  (gp_psiStormRadius (gp_dynamics params))
              availableDroneHealth =
                foldl' (+) 0 $
                  rbdv_myDrones <&> \dbv ->
                    max 0 $
                      dbv_health dbv
                        - 2 * gp_psiStormDamagePerHit (gp_combat params)
              targetOverwhelmedByDrones =
                let buffer = 0.1 * gp_overseerMaxHealth_base (gp_combat params)
                in  obv_totalOverseerHealth targetOverseer
                      < availableDroneHealth - buffer
          in  targetInPsiStorm && targetOverwhelmedByDrones

-- TODO: take into account drone health
psiStormAvoidancePotential ::
  GameParams -> [PsiStormBotView] -> Fixed2 -> Fixed
psiStormAvoidancePotential params psiStorms pos =
  let -- Compute psi-storm damage to cluster of drones centered at position
      psiStormRadius = gp_psiStormRadius (gp_dynamics params)
      droneRadius4 = 4 * gp_droneRadius_base (gp_dynamics params)
      (overlapFracs, distances) =
        unzip $
          flip map psiStorms $ \psiStorm ->
            let distance = Fixed.norm (psbv_pos psiStorm - pos)
                overlap =
                  max 0 . min 1 $
                    (psiStormRadius + droneRadius4 - distance)
                      / (2 * droneRadius4)
            in  (overlap, distance)
      overlapsSqSum = sqrt $ foldl' (+) 0 $ map (\x -> x * x) overlapFracs
      damage = gp_psiStormDamagePerHit (gp_combat params) * overlapsSqSum

      damagePotential = damage * 144
      movementIncentivePotential =
        case List.NonEmpty.nonEmpty distances of
          Nothing -> 0
          Just distances' ->
            -min (psiStormRadius + droneRadius4) (minimum distances')
              * damage
  in  damagePotential + movementIncentivePotential

--------------------------------------------------------------------------------

obstacleAvoidancePotential ::
  GameParams -> [SoftObstacleBotView] -> Fixed2 -> Fixed
obstacleAvoidancePotential params obstacles pos =
  let softObstacleRadius = gp_softObstacleRadius (gp_dynamics params)
      droneRadius3 = 3 * gp_droneRadius_base (gp_dynamics params)
      avoidanceRadius = max 0 $ softObstacleRadius - droneRadius3
      minDistance =
        Fixed.sqrtFixedSq $
          foldl' min Fixed.maxBoundFixedSq $
            map (\obstacle -> Fixed.normSq (sobv_pos obstacle - pos)) obstacles
      maxPenalty = softObstacleRadius * 2 -- arbitrary multiplier
  in  if
        | minDistance > softObstacleRadius -> 0
        | minDistance > avoidanceRadius ->
            ( (minDistance - avoidanceRadius)
                / (softObstacleRadius - avoidanceRadius)
            )
              * maxPenalty
        | otherwise ->
            maxPenalty

--------------------------------------------------------------------------------

droneMoveCandidates ::
  GameParams ->
  PlayerClass ->
  LinearDragParams ->
  Ticks ->
  Fixed2 ->
  Fixed2 ->
  Fixed2 ->
  IntMap DroneMoveCandidate
droneMoveCandidates
  params
  playerClass
  droneParams
  chargeTicks
  overseerPos
  initialPos
  initialVel =
    let posNeutral :!: velNeutral = step 0 (initialPos :!: initialVel)
        deltaV = droneDashDeltaV (gp_cast params) chargeTicks
        posOffset :!: velOffset =
          step (Fixed2 1 0) (Fixed2 0 0 :!: Fixed2 deltaV 0)
        moveCandidates = flip map moveDirections $ \(moveIdx, moveDir) ->
          let (dmc_mousePos, attractRepel) =
                mouseTarget params overseerPos controlRadius initialPos moveDir
              (dmc_droneCmd, actualMoveDir) =
                let mouseDir = Fixed.normalize (dmc_mousePos - initialPos)
                in  case attractRepel of
                      Attract -> (DroneAttraction, mouseDir)
                      Repel -> (DroneRepulsion, -mouseDir)
              Fixed2 dx dy = actualMoveDir
              px = -dy
              py = dx -- perpendicular direction
              rotate (Fixed2 x y) = Fixed2 (dx * x + px * y) (dy * x + py * y)
              dmc_pos = posNeutral + rotate posOffset
              dmc_vel = velNeutral + rotate velOffset
          in  (moveIdx, DroneMoveCandidate{..})
        neutralCandidate =
          DroneMoveCandidate
            { dmc_pos = posNeutral
            , dmc_vel = velNeutral
            , dmc_mousePos = posNeutral
            , dmc_droneCmd = DroneNeutral
            }
        candidates =
          if gp_botAllowDroneDash (gp_bots params)
            then (neutralMoveIndex, neutralCandidate) : moveCandidates
            else moveCandidates
    in  IntMap.fromList candidates
    where
      controlRadius =
        getClassParam playerClass (gp_controlRadius (gp_dynamics params))
      step dir =
        let duration = 45
        in  forwardSimulateKinematics droneParams dir duration

data AttractRepel = Attract | Repel

mouseTarget ::
  GameParams -> Fixed2 -> Fixed -> Fixed2 -> Fixed2 -> (Fixed2, AttractRepel)
mouseTarget params controlCenter controlRadius currentPos moveDir
  | Fixed.normLeq (targetPos - controlCenter) controlRadius =
      (targetPos, Attract)
  | otherwise =
      let controlToTarget = targetPos - controlCenter
          boundedTargetPos =
            controlCenter
              + Fixed.map
                (* (controlRadius / Fixed.norm controlToTarget))
                controlToTarget
          boundedMoveDir = Fixed.normalize (boundedTargetPos - currentPos)
          allowDroneRepel = gp_botAllowDroneRepel (gp_bots params)
      in  if not allowDroneRepel || Fixed.dot boundedMoveDir moveDir > 0.9 -- TODO: adjust
            then (boundedTargetPos, Attract)
            else
              let antiTargetPos =
                    currentPos - Fixed.map (* controlRadius) moveDir
                  controlToAntiTarget = antiTargetPos - controlCenter
                  boundedAntiTargetPos =
                    controlCenter
                      + Fixed.map
                        (* (controlRadius / Fixed.norm controlToAntiTarget))
                        controlToAntiTarget
              in  (boundedAntiTargetPos, Repel)
  where
    targetPos = currentPos + Fixed.map (* 150) moveDir

neutralMoveIndex :: Int
neutralMoveIndex = 0

moveDirections :: [(Int, Fixed2)]
moveDirections =
  flip map [1 :: Int .. nPoints] $ \n ->
    let moveIdx = n
    in  (moveIdx, Fixed.angle $ 2 * pi * fromIntegral n / nPoints')
  where
    nPoints = 24
    nPoints' = fromIntegral nPoints

data DroneMoveCandidate = DroneMoveCandidate
  { dmc_pos :: Fixed2
  , dmc_vel :: Fixed2
  , dmc_mousePos :: Fixed2
  , dmc_droneCmd :: DroneCommand
  }
