module Pixelpusher.Game.Bots.AI.GeometryUtil (
  lineDistance,
  isWithinDistanceOfLine,
  pointToCircleTangents,
  circleToCircleOuterTangents,
  circleToCircleCrossTangents,
  perpendicularCCW,
  projectToCircle,
  geometricMedian,
) where

import Data.Foldable (foldl')
import Data.Foldable1 (Foldable1)
import Data.Tuple (swap)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed

-- | Distance of a point from a line segment defined by two endpoints
lineDistance :: Fixed2 -> Fixed2 -> Fixed2 -> Fixed
lineDistance endpoint1 endpoint2 point =
  let frac = Fixed.nearestPointOnLineFrac endpoint1 endpoint2 point
      linePoint = endpoint1 + Fixed.map (* frac) (endpoint2 - endpoint1)
  in  Fixed.norm (linePoint - point)

isWithinDistanceOfLine :: Fixed2 -> Fixed2 -> Fixed -> Fixed2 -> Bool
isWithinDistanceOfLine endpoint1 endpoint2 radius point =
  let frac = Fixed.nearestPointOnLineFrac endpoint1 endpoint2 point
      linePoint = endpoint1 + Fixed.map (* frac) (endpoint2 - endpoint1)
  in  Fixed.normLeq (linePoint - point) radius

-- | Given a circle C and a point P outside of that circle, finds the two
-- points on the circle whose tangent lines intersect P.
--
-- Facing the circle from P, the first returned position is on the left, and
-- the second is on the right.
pointToCircleTangents :: Fixed2 -> Fixed -> Fixed2 -> Maybe (Fixed2, Fixed2)
pointToCircleTangents circlePos circleRadius testPos
  | Fixed.normLeq (testPos - circlePos) circleRadius = Nothing
  | otherwise = Just $ pointToCircleTangents' circlePos circleRadius testPos

-- | Like pointToCircleTangents, but the precondition that P lies outside the
-- circle is not checked.
pointToCircleTangents' :: Fixed2 -> Fixed -> Fixed2 -> (Fixed2, Fixed2)
pointToCircleTangents' circlePos circleRadius testPos =
  let (dist, dir) = Fixed.normAndNormalize (circlePos - testPos)
      x = Fixed.sqDiv circleRadius dist
      y =
        -- Preventing overflow
        Fixed.fromDouble $
          let r = Fixed.toDouble circleRadius
              x' = Fixed.toDouble x
          in  sqrt $ r * r - x' * x'
      base = testPos + Fixed.map (* (dist - x)) dir
      offset = Fixed.map (* y) (perpendicularCCW dir)
      pos1 = base + offset
      pos2 = base - offset
  in  (pos1, pos2)

-- | Returns ordered pairs, where the first point lies on the first circle, and
-- the second point lies on the second circle.
circleToCircleOuterTangents ::
  Fixed -> Fixed2 -> Fixed -> Fixed2 -> Maybe ((Fixed2, Fixed2), (Fixed2, Fixed2))
circleToCircleOuterTangents circleRadius1 circlePos1 circleRadius2 circlePos2
  | circleRadius1 >= circleRadius2 =
      circleToCircleOuterTangents' circleRadius1 circlePos1 circleRadius2 circlePos2
  | otherwise =
      over both swap
        <$> circleToCircleOuterTangents' circleRadius2 circlePos2 circleRadius1 circlePos1

-- | Assumes the first circle is larger than the second.
--
-- Returns ordered pairs, where the first point lies on the first circle, and
-- the second point lies on the second circle.
circleToCircleOuterTangents' ::
  Fixed -> Fixed2 -> Fixed -> Fixed2 -> Maybe ((Fixed2, Fixed2), (Fixed2, Fixed2))
circleToCircleOuterTangents' circleRadius1 circlePos1 circleRadius2 circlePos2 =
  let diff = circlePos2 - circlePos1
      dist = Fixed.norm diff
  in  if circleRadius2 - circleRadius1 >= dist
        then Nothing
        else
          Just $
            let cos' = (circleRadius1 - circleRadius2) / dist
                sin' = sqrt $ 1 - cos' * cos'
                p1 = Fixed.map (* (circleRadius1 / dist)) diff
                p2 = Fixed.map (* (circleRadius2 / dist)) diff
                !p11 = Fixed.rotateSinCos sin' cos' p1 + circlePos1
                !p12 = Fixed.rotateSinCos sin' cos' p2 + circlePos2
                !p21 = Fixed.rotateSinCos (-sin') cos' p1 + circlePos1
                !p22 = Fixed.rotateSinCos (-sin') cos' p2 + circlePos2
            in  ((p11, p12), (p21, p22))

-- | Returns ordered pairs, where the first point lies on the first circle, and
-- the second point lies on the second circle.
circleToCircleCrossTangents ::
  Fixed ->
  Fixed2 ->
  Fixed ->
  Fixed2 ->
  Maybe ((Fixed2, Fixed2), (Fixed2, Fixed2))
circleToCircleCrossTangents circleRadius1 circlePos1 circleRadius2 circlePos2
  | circleRadius1 >= circleRadius2 =
      circleToCircleCrossTangents' circleRadius1 circlePos1 circleRadius2 circlePos2
  | otherwise =
      over both swap
        <$> circleToCircleCrossTangents' circleRadius2 circlePos2 circleRadius1 circlePos1

-- | Assumes the first circle is larger than the second.
--
-- Returns ordered pairs, where the first point lies on the first circle, and
-- the second point lies on the second circle.
circleToCircleCrossTangents' ::
  Fixed ->
  Fixed2 ->
  Fixed ->
  Fixed2 ->
  Maybe ((Fixed2, Fixed2), (Fixed2, Fixed2))
circleToCircleCrossTangents' circleRadius1 circlePos1 circleRadius2 circlePos2 =
  let diff = circlePos2 - circlePos1
      dist = Fixed.norm diff
      radiusSum = circleRadius1 + circleRadius2
  in  if dist <= radiusSum
        then Nothing
        else
          Just $
            let cos' = radiusSum / dist
                sin' = sqrt $ 1 - cos' * cos'
                p1 = Fixed.map (* (circleRadius1 / dist)) diff
                !crossTangentPt1 = Fixed.rotateSinCos sin' cos' p1 + circlePos1
                !crossTangentPt2 = Fixed.rotateSinCos (-sin') cos' p1 + circlePos1
                crossPoint =
                  Fixed.map (* (circleRadius1 / radiusSum)) diff + circlePos1
                ratio = circleRadius2 / circleRadius1
                !oppositeTangent1 =
                  crossPoint + Fixed.map (* ratio) (crossPoint - crossTangentPt1)
                !oppositeTangent2 =
                  crossPoint + Fixed.map (* ratio) (crossPoint - crossTangentPt2)
            in  ( (crossTangentPt1, oppositeTangent1)
                , (crossTangentPt2, oppositeTangent2)
                )

-- | Rotate a 2D vector 90 degrees anti-clockwise
perpendicularCCW :: Fixed2 -> Fixed2
perpendicularCCW (Fixed2 x y) = Fixed2 (-y) x

projectToCircle :: Fixed2 -> Fixed -> Fixed2 -> Maybe Fixed2
projectToCircle circleCenter circleRadius pos
  | circleCenter == pos = Nothing
  | otherwise =
      Just $
        let dir = Fixed.normalize (pos - circleCenter)
        in  Fixed.map (* circleRadius) dir + circleCenter

--------------------------------------------------------------------------------

-- Approximate
geometricMedian :: (Foldable1 t) => t Fixed2 -> Fixed2
geometricMedian positions =
  let (posSum, posCount) =
        let f (!sumAcc, !countAcc) pos = (sumAcc + pos, countAcc + 1)
        in  foldl' f (0, 0 :: Int) positions
      avgPos = Fixed.map (/ fromIntegral posCount) posSum
  in  geometricMedianIter positions $
        geometricMedianIter positions $
          geometricMedianIter positions avgPos
{-# INLINEABLE geometricMedian #-}

-- https://en.wikipedia.org/wiki/Geometric_median
-- Weiszfeld's algorithm
geometricMedianIter :: (Foldable t) => t Fixed2 -> Fixed2 -> Fixed2
geometricMedianIter positions candidatePos =
  let f (!numAcc, !denomAcc) pos =
        let dist = Fixed.norm (pos - candidatePos)
            recipDist = if dist < 0.04 then 25 else recip dist
        in  (numAcc + Fixed.map (* recipDist) pos, denomAcc + recipDist)
      (num, denom) = foldl' f (0, 0) positions
  in  if denom < 1e-3 then candidatePos else Fixed.map (/ denom) num
{-# INLINEABLE geometricMedianIter #-}
