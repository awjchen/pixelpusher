module Pixelpusher.Game.Bots.AI.Intentions (
  CastedPsiStorm (..),
  CastedOverseerDash (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Game.Time

-- | For allowing the bot to react to its own psi-storm faster than the bot
-- delay
data CastedPsiStorm = CastedPsiStorm
  { cps_time :: Time
  -- ^ Time at which the psi-storm command was issued. The actual cast will
  -- occur after the bot delay.
  , cps_pos :: Fixed2
  -- ^ The intended position of the psi-storm. The actual position will
  -- depend on the bot's mouse position at the time of the actual cast, which
  -- we have made inaccurate to be more fair to humans.
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

newtype CastedOverseerDash = CastedOverseerDash {cod_time :: Time}
  deriving newtype (Serialize)
