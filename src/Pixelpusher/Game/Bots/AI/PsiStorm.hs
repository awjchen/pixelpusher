{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.PsiStorm (
  PsiStormCast (..),
  castPsiStorm,
  CastedPsiStorm (..),
) where

import Control.Monad (unless, when)
import Data.Foldable (foldl')
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe (mapMaybe)

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.Time

import Pixelpusher.Game.Bots.AI.GeometryUtil (geometricMedian)
import Pixelpusher.Game.Bots.AI.Intentions
import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.ViewSummary

--------------------------------------------------------------------------------

data PsiStormCast
  = PSC_DoNotCast
  | PSC_DelayCast Fixed2
  | PSC_Cast Fixed2

instance Semigroup PsiStormCast where
  -- Left-biased and lazy in the second argument, intended for short-circuiting
  (<>) :: PsiStormCast -> PsiStormCast -> PsiStormCast
  (<>) (PSC_Cast pos) _ = PSC_Cast pos
  (<>) (PSC_DelayCast _) (PSC_Cast pos) = PSC_Cast pos
  (<>) (PSC_DelayCast pos) _ = PSC_DelayCast pos
  (<>) PSC_DoNotCast psiStormCast = psiStormCast

instance Monoid PsiStormCast where
  mempty = PSC_DoNotCast

castPsiStorm ::
  GameParams -> Time -> RefinedBotView -> ViewSummary -> PsiStormCast
castPsiStorm params time botView viewSummary =
  let psiStormEnabled = gp_botAllowPsiStorm (gp_bots params)
      isTemplar = obv_class (rbv_myOverseer botView) == Templar
      psiStormOnCooldown = time < rbv_myPsiStormCooldownTime botView
  in  if not psiStormEnabled || not isTemplar || psiStormOnCooldown
        then PSC_DoNotCast
        else castPsiStorm' params time botView viewSummary

castPsiStorm' ::
  GameParams -> Time -> RefinedBotView -> ViewSummary -> PsiStormCast
castPsiStorm' params time botView ViewSummary{..} =
  let castDefensivePsiStorm' = castDefensivePsiStorm params time botView vs_allyInfo
  in  foldMap (uncurry castDefensivePsiStorm') $
        (TargetSelf, vs_myInfo)
          : map (TargetAlly,) (WIM.elems vs_allyInfo)

data DefensivePsiStormTarget = TargetSelf | TargetAlly
  deriving stock (Eq)

castDefensivePsiStorm ::
  GameParams ->
  Time ->
  RefinedBotView ->
  WIM.IntMap ActorID AllyInfo ->
  DefensivePsiStormTarget ->
  AllyInfo ->
  PsiStormCast
castDefensivePsiStorm params time RefinedBotView{..} visibleAllies targetType targetAlly =
  let psiStormRadius = gp_psiStormRadius (gp_dynamics params)
      botDelayTicks =
        let stepTicks = C.botControlFrequencyTicks
            steps = gp_botControlQueueLength (gp_bots params)
        in  fromIntegral stepTicks * steps
      targetAllyOv = ai_overseer targetAlly
      targetRestingPos = ai_restingPosition targetAlly

      psiStormCast =
        either id PSC_Cast $ do
          -- Note: All returns of PSC_DoNotCast must come before returns of PSC_DelayCast
          -- TODO: Use a different error-type monad

          -- For psi-storms on behalf of allies ...
          when (targetType == TargetAlly) $ do
            -- Don't cast for allies that can do it themselves
            let hasPsiStormCharge obv =
                  obv_psiStormCooldownUntil obv <= addTicks rbv_queueDelayTicks time
            when (hasPsiStormCharge targetAllyOv) $ Left PSC_DoNotCast

            -- Don't cast if we're not the closest ally with a psi-storm charge
            let alliesWithPsiStormCharge =
                  flip mapMaybe rbv_alliedOverseerPositions $
                    \(actorID, pos) ->
                      if actorID == obv_actorID rbv_myOverseer
                        then Nothing
                        else case WIM.lookup actorID visibleAllies of
                          Nothing ->
                            -- Say that allies outside our view as have a charge
                            Just pos
                          Just allyInfo ->
                            let alliedOverseer = ai_overseer allyInfo
                            in  if obv_class alliedOverseer == Templar
                                  && hasPsiStormCharge alliedOverseer
                                  then -- Using non-predicted position for consistency
                                  -- with positions of allies not in view
                                    Just pos
                                  else Nothing
                distToAlly = Fixed.normSq . subtract targetRestingPos
                myDistToAlly = distToAlly rbv_myRawPosition
            when (any ((< myDistToAlly) . distToAlly) alliesWithPsiStormCharge) $
              Left PSC_DoNotCast

          -- Ensure there is enough of a threat
          threateningDrones' <-
            maybe
              (Left PSC_DoNotCast)
              Right
              (List.NonEmpty.nonEmpty (ai_threateningDrones targetAlly))

          do
            let damageRatio =
                  min 4 $
                    ai_threateningDronesTotalHealth targetAlly
                      / max 1 (obv_totalOverseerHealth rbv_myOverseer)
                requiredThreats
                  | damageRatio > 1 =
                      -- Require at least two entities in case of lethal damage
                      -- so that psi-storming would actually reduce the damage
                      -- taken, assuming that we also take psi-storm damage
                      floor $ max 2 $ min 5 $ recip (damageRatio - 1) + 1
                  | otherwise = 5
                numThreats = length (ai_threateningDrones targetAlly)
                isThreatened = numThreats >= requiredThreats
            unless isThreatened (Left PSC_DoNotCast)

          -- Compute psi-storm target
          let psiStormTarget =
                let medianPos = geometricMedian $ fmap dbv_pos threateningDrones'
                    droneTerminalVelocity drone =
                      ldp_terminalVelocity $
                        getClassParam
                          (dbv_class drone)
                          (gp_droneDragParams (gp_bots params))
                    totalDroneTerminalVelocity =
                      foldl' (+) 0 $
                        fmap droneTerminalVelocity threateningDrones'
                    averageDroneTerminalVelocity =
                      -- TODO: come up with something better than using an
                      -- average
                      totalDroneTerminalVelocity
                        / fromIntegral (length threateningDrones')
                    displacement =
                      fromIntegral botDelayTicks -- Note: this is in addition to the base prediction delay
                        * averageDroneTerminalVelocity
                        * 0.8 -- arbitrary
                    dronesToTarget = targetRestingPos - medianPos
                    scaledDisplacement =
                      Fixed.map
                        (* (displacement / Fixed.norm dronesToTarget))
                        dronesToTarget
                in  targetRestingPos + scaledDisplacement

          -- Don't cast psi-storm if our projected position or psi-storm target
          -- is inside an obstacle
          let inObstacle =
                flip any rbv_softObstacles $ \obstacle ->
                  let projPosInObstacle =
                        Fixed.normLeq
                          (targetRestingPos - sobv_pos obstacle)
                          (gp_softObstacleRadius (gp_dynamics params))
                      targetPosInObstacle =
                        Fixed.normLeq
                          (psiStormTarget - sobv_pos obstacle)
                          (gp_softObstacleRadius (gp_dynamics params))
                      targetPosOutsideWorldBoundary =
                        not $
                          Fixed.normLeq
                            psiStormTarget
                            (gp_worldRadius (gp_dynamics params))
                  in  projPosInObstacle
                        || targetPosInObstacle
                        || targetPosOutsideWorldBoundary
          when inObstacle (Left (PSC_DelayCast psiStormTarget))

          -- Avoid casting redundant psi-storms
          let nearbyExistingPsiStorms =
                foldl'
                  ( \count psiStorm ->
                      if Fixed.normLeq
                        (psiStormTarget - psbv_pos psiStorm)
                        (0.7 * psiStormRadius)
                        then count + 1
                        else count :: Int
                  )
                  0
                  rbv_psiStorms
          unless (nearbyExistingPsiStorms == 0) (Left (PSC_DelayCast psiStormTarget))

          -- Avoid storming too many allies
          let predictionTicks = 20 -- arbitrary
              nearbyAllies =
                foldl'
                  ( \count ov ->
                      if Fixed.normLeq
                        ( psiStormTarget
                            - (obv_pos ov + obv_vel ov * predictionTicks)
                        )
                        psiStormRadius
                        then count + 1
                        else count :: Int
                  )
                  0
                  rbv_alliedOverseers
          unless (nearbyAllies <= 1) (Left (PSC_DelayCast psiStormTarget))

          pure psiStormTarget
  in  psiStormCast
