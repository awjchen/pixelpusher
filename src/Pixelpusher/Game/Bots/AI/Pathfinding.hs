{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.Pathfinding (
  PathfindingResult (..),
  emptyPathfindingResult,
  Obstacle (..),
  pathfind,
) where

import Algorithm.Search
import Control.Monad (guard, unless, when, zipWithM_)
import Control.Monad.Trans.State.Strict
import Data.Foldable (for_)
import Data.Function (on, (&))
import Data.Functor ((<&>))
import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict qualified as HashMap
import Data.Hashable
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.List qualified as List
import Data.Maybe
import Data.Serialize (Serialize)
import Data.Traversable (for)
import Data.Tuple (swap)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.AI.GeometryUtil
import Pixelpusher.Game.Parameters

--------------------------------------------------------------------------------

type Distance = Fixed

data BuildGraphState = BuildGraphState
  { bgs_nextVertexID :: Int
  , bgs_obstacleVertices :: IntMap (IntMap ObstacleVertex)
  -- ^ Vertices that are tangent to obstacles. The set of vertices tangent
  -- to an obstacle will be joined in a loop around the perimeter of the
  -- obstacle, obstructions permitting.
  , bgs_targetVertices :: IntMap TargetVertex
  -- ^ Points of interest that should be linked with each other and all
  -- obstacles in line-of-sight.
  , bgs_neighbours :: IntMap [Vertex]
  , bgs_edgeDistances :: HashMap UnorderedInts Distance
  , bgs_drawEdges :: [(Fixed2, Fixed2)]
  }

-- | Invariant: UnorderedInts x y implies x <= y
data UnorderedInts = UnorderedInts Int Int
  deriving stock (Generic, Eq)
  deriving anyclass (Hashable)

makeUnorderedInts :: Int -> Int -> UnorderedInts
makeUnorderedInts x y
  | x < y = UnorderedInts x y
  | otherwise = UnorderedInts y x

data Vertex = Vertex
  { v_id :: Int
  -- ^ This identifier is supposed to be unique
  , v_pos :: Fixed2
  }

-- | Compare identifiers only
instance Eq Vertex where
  (==) = (==) `on` v_id

-- | Compare identifiers only
instance Ord Vertex where
  (<=) = (<=) `on` v_id
  compare = compare `on` v_id

data TargetVertex = TargetVertex
  { tv_vertex :: Vertex
  , tv_tangentObstacleIDs :: [Int]
  , tv_validOutwardsDirections :: Fixed2 -> Bool
  }

initialBuildGraphState :: BuildGraphState
initialBuildGraphState =
  BuildGraphState
    { bgs_nextVertexID = 0
    , bgs_obstacleVertices = IntMap.empty
    , bgs_targetVertices = IntMap.empty
    , bgs_neighbours = IntMap.empty
    , bgs_edgeDistances = HashMap.empty
    , bgs_drawEdges = []
    }

newVertex ::
  [(Int, Vertex -> ObstacleVertex)] ->
  Fixed2 ->
  State BuildGraphState Vertex
newVertex obstacleConstructors pos = do
  st <- get
  let vertexID = bgs_nextVertexID st
      vertex = Vertex{v_id = vertexID, v_pos = pos}
  put $!
    st
      { bgs_nextVertexID = succ vertexID
      , bgs_obstacleVertices =
          let insert obstacleVertices (obstacleID, constructor) =
                let singleton' = IntMap.singleton vertexID (constructor vertex)
                    insert' = IntMap.insert vertexID (constructor vertex)
                in  IntMap.alter
                      (Just . maybe singleton' insert')
                      obstacleID
                      obstacleVertices
          in  List.foldl' insert (bgs_obstacleVertices st) obstacleConstructors
      }
  pure vertex

addBoundaryWaypoint :: Int -> Int -> State BuildGraphState ()
addBoundaryWaypoint obstacleID vertexID =
  modify' $ \st ->
    st
      { bgs_obstacleVertices =
          IntMap.adjust
            (IntMap.adjust addWaypointToObstacleVertex vertexID)
            obstacleID
            (bgs_obstacleVertices st)
      }

registerTargetVertex :: TargetVertex -> State BuildGraphState ()
registerTargetVertex targetVertex =
  modify' $ \st ->
    st
      { bgs_targetVertices =
          IntMap.insert
            (v_id (tv_vertex targetVertex))
            targetVertex
            (bgs_targetVertices st)
      }

-- | For internal use
newVertex' :: Fixed2 -> State BuildGraphState Vertex
newVertex' pos = do
  st <- get
  let vertexID = bgs_nextVertexID st
      vertex = Vertex{v_id = vertexID, v_pos = pos}
  put $!
    st
      { bgs_nextVertexID = succ vertexID
      }
  pure vertex

addEdge :: Vertex -> Vertex -> Fixed -> State BuildGraphState ()
addEdge v1 v2 distance = do
  modify' $ \st ->
    st
      { bgs_neighbours =
          bgs_neighbours st
            & IntMap.insertWith (++) (v_id v1) [v2]
            & IntMap.insertWith (++) (v_id v2) [v1]
      , bgs_edgeDistances =
          bgs_edgeDistances st
            & HashMap.insert (makeUnorderedInts (v_id v1) (v_id v2)) distance
      , bgs_drawEdges = (v_pos v1, v_pos v2) : bgs_drawEdges st
      }

addDirectEdge :: Vertex -> Vertex -> State BuildGraphState ()
addDirectEdge v1 v2 = addEdge v1 v2 (directDistance v1 v2)

directDistance :: Vertex -> Vertex -> Fixed
directDistance v1 v2 = Fixed.norm (v_pos v1 - v_pos v2)

--------------------------------------------------------------------------------

data PathfindingResult = PathfindingResult
  { pr_path :: Maybe [Fixed2]
  , pr_requestedStartPos :: Maybe Fixed2
  -- ^ For debugging
  , pr_startPos :: Maybe Fixed2
  -- ^ For debugging
  , pr_requestedEndPos :: Maybe Fixed2
  -- ^ For debugging
  , pr_endPos :: Maybe Fixed2
  -- ^ For debugging
  , pr_edges :: [(Fixed2, Fixed2)]
  -- ^ For debugging
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

emptyPathfindingResult :: PathfindingResult
emptyPathfindingResult =
  PathfindingResult
    { pr_path = Nothing
    , pr_requestedStartPos = Nothing
    , pr_startPos = Nothing
    , pr_requestedEndPos = Nothing
    , pr_endPos = Nothing
    , pr_edges = []
    }

pathfind ::
  DynamicsParams -> [Obstacle] -> Fixed2 -> Fixed2 -> PathfindingResult
pathfind params inputObstacles startPos endPos =
  let obstacles = relevantObstacles startPos endPos inputObstacles
      (startEndVerticesMaybe, st) =
        flip runState initialBuildGraphState $
          pathfindGraph params obstacles startPos endPos
      routeMaybe =
        startEndVerticesMaybe >>= \(startVertex, endVertex) ->
          map v_pos . snd
            <$> shortestRoute
              (bgs_neighbours st)
              (bgs_edgeDistances st)
              startVertex
              endVertex
  in  PathfindingResult
        { pr_path = routeMaybe
        , pr_requestedStartPos = Just startPos
        , pr_startPos = fmap (v_pos . fst) startEndVerticesMaybe
        , pr_requestedEndPos = Just endPos
        , pr_endPos = fmap (v_pos . snd) startEndVerticesMaybe
        , pr_edges = bgs_drawEdges st
        }

pathfindGraph ::
  DynamicsParams ->
  [Obstacle] ->
  Fixed2 ->
  Fixed2 ->
  State BuildGraphState (Maybe (Vertex, Vertex))
pathfindGraph params obstacles startPos endPos = do
  obstacleIntersections <-
    findObstacleIntersections params obstacles
      <&> \obstacleIntersections' ->
        HashMap.fromList $
          flip map obstacleIntersections' $ \oi ->
            (makeUnorderedInts (oi_obstacleID1 oi) (oi_obstacleID2 oi), oi)
  worldBoundsObstacleIntersections <-
    findWorldBoundsObstacleIntersections params obstacles
      <&> \worldBoundsObstacleIntersections' ->
        IntMap.fromList $
          flip map worldBoundsObstacleIntersections' $ \wboi ->
            (wboi_obstacleID wboi, wboi)

  startVertexMaybe <-
    movePointOutOfObstacles
      params
      startPos
      obstacles
      obstacleIntersections
      worldBoundsObstacleIntersections
  endVertexMaybe <-
    movePointOutOfObstacles
      params
      endPos
      obstacles
      obstacleIntersections
      worldBoundsObstacleIntersections

  for ((,) <$> startVertexMaybe <*> endVertexMaybe) $ \(startVertex, endVertex) -> do
    addObstacleTangents params obstacles
    linkTargetVertices params obstacles

    do
      obstacleVertices' <-
        gets bgs_obstacleVertices <&> \vertices ->
          IntMap.mergeWithKey
            (\_key a b -> Just (a, b))
            (const IntMap.empty)
            (const IntMap.empty)
            (IntMap.fromList $ map (\obs -> (obs_id obs, obs)) obstacles)
            vertices
      for_ (IntMap.elems obstacleVertices') $
        \(obstacle, obstacleVertices) ->
          obstacleRingEdges
            (obs_radius obstacle)
            (obs_pos obstacle)
            (IntMap.elems obstacleVertices)

    pure (startVertex, endVertex)

shortestRoute ::
  IntMap [Vertex] ->
  HashMap UnorderedInts Distance ->
  Vertex ->
  Vertex ->
  Maybe (Distance, [Vertex])
shortestRoute neighbours edgeDistances startVertex endVertex =
  aStar
    (\v -> fromMaybe [] $ IntMap.lookup (v_id v) neighbours)
    ( \v1 v2 ->
        fromJust $
          HashMap.lookup (makeUnorderedInts (v_id v1) (v_id v2)) edgeDistances
    )
    (directDistance endVertex)
    ((== v_id endVertex) . v_id)
    startVertex

--------------------------------------------------------------------------------

data Obstacle = Obstacle
  { obs_id :: Int
  , obs_pos :: Fixed2
  , obs_radius :: Fixed
  , obs_intersectionsTraversable :: Bool
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

relevantObstacles :: Fixed2 -> Fixed2 -> [Obstacle] -> [Obstacle]
relevantObstacles endpoint1 endpoint2 obstacles =
  let (intersectingObstacles, otherObstacles) =
        List.partition
          ( \Obstacle{obs_pos, obs_radius} ->
              isWithinDistanceOfLine endpoint1 endpoint2 obs_radius obs_pos
          )
          obstacles
      initState =
        RelevantObstacleState
          { ros_includedTested = []
          , ros_includedUntested = intersectingObstacles
          , ros_other = otherObstacles
          }
  in  ros_includedTested $
        applyUntil
          (extendRelevantObstacles endpoint1 endpoint2)
          (List.null . ros_includedUntested)
          initState

data RelevantObstacleState = RelevantObstacleState
  { ros_includedTested :: [Obstacle]
  , ros_includedUntested :: [Obstacle]
  , ros_other :: [Obstacle]
  }

extendRelevantObstacles ::
  Fixed2 -> Fixed2 -> RelevantObstacleState -> RelevantObstacleState
extendRelevantObstacles endpoint1 endpoint2 st =
  case ros_includedUntested st of
    [] -> st
    testObstacle : untestedObstacles ->
      let Obstacle{obs_pos = posTesting, obs_radius = radiusTesting} = testObstacle
          (intersectingObstacles, otherObstacles) =
            flip List.partition (ros_other st) $
              \Obstacle{obs_pos = posOther, obs_radius = radiusOther} ->
                let intersectsTestObstacle =
                      Fixed.normLeq
                        (posTesting - posOther)
                        (radiusTesting + radiusOther)
                    intersectsObstacleToObstacle =
                      flip any (ros_includedTested st) $
                        \Obstacle{obs_pos = posTested, obs_radius = radiusTested} ->
                          -- TODO: Test distance against an uneven capsule,
                          -- rather than a overly large even capsule
                          isWithinDistanceOfLine
                            posTesting
                            posTested
                            (radiusOther + max radiusTested radiusTesting) -- too conservative
                            posOther
                    intersectsPointToObstacle =
                      flip any [endpoint1, endpoint2] $ \endpoint ->
                        case pointToCircleTangents posTesting radiusTesting endpoint of
                          Nothing -> False
                          Just (tangent1, tangent2) ->
                            isWithinDistanceOfLine endpoint tangent1 radiusOther posOther
                              || isWithinDistanceOfLine endpoint tangent2 radiusOther posOther
                in  intersectsTestObstacle
                      || intersectsObstacleToObstacle
                      || intersectsPointToObstacle
      in  RelevantObstacleState
            { ros_includedTested = testObstacle : ros_includedTested st
            , ros_includedUntested = intersectingObstacles ++ untestedObstacles
            , ros_other = otherObstacles
            }

-- Helper
applyUntil :: (a -> a) -> (a -> Bool) -> a -> a
applyUntil f test x = if test x then x else applyUntil f test (f x)

--------------------------------------------------------------------------------

data ObstacleIntersection = ObstacleIntersection
  { oi_obstacleID1 :: Int
  , oi_obstacleID2 :: Int
  , oi_intersectionVertex1 :: Vertex
  -- ^ First intersection on obstacle 1, going anti-clockwise
  , oi_intersectionVertex2 :: Vertex
  -- ^ Second intersection on obstacle 1, going anti-clockwise
  , oi_validOutwardsDirections1 :: Fixed2 -> Bool
  , oi_validOutwardsDirections2 :: Fixed2 -> Bool
  , oi_traversable :: Bool
  , oi_vertexFree1 :: Bool
  , oi_vertexFree2 :: Bool
  }

-- For now, assume that there are no triplets of obstacles where a
-- point exists inside of all three.
findObstacleIntersections ::
  DynamicsParams -> [Obstacle] -> State BuildGraphState [ObstacleIntersection]
findObstacleIntersections params obstacles = do
  let intersections = do
        obstacle1@(Obstacle obstacleID1 pos1 radius1 _) <- obstacles
        obstacle2@(Obstacle obstacleID2 pos2 radius2 _) <- obstacles
        guard $
          obstacleID1 < obstacleID2
            && Fixed.normLeq (pos1 - pos2) (radius1 + radius2)
        pure (obstacle1, obstacle2)
  for intersections $ \(obstacle1, obstacle2) -> do
    let Obstacle obstacleID1 pos1 radius1 traversable1 = obstacle1
        Obstacle obstacleID2 pos2 radius2 traversable2 = obstacle2
        dist = Fixed.norm (pos2 - pos1)
        (intersectionPos1, intersectionPos2) =
          circleIntersections radius1 pos1 radius2 pos2

    let isPosOccluded pos =
          let occludedByObstacles =
                flip any obstacles $ \Obstacle{..} ->
                  obs_id /= obstacleID1
                    && obs_id /= obstacleID2
                    && Fixed.normLeq (obs_pos - pos) obs_radius
              occludedByWorldBounds =
                not (Fixed.normLeq pos (gp_worldRadius params))
          in  occludedByObstacles || occludedByWorldBounds
        posFree1 = not $ isPosOccluded intersectionPos1
        posFree2 = not $ isPosOccluded intersectionPos2

    let overlap = radius1 + radius2 - dist
        traversable =
          traversable1
            && traversable2
            && overlap < obstacleOverlapThreshold
            && posFree1
            && posFree2

    let validOutwardsDirections1 outDir =
          Fixed.dotI64 outDir (intersectionPos1 - pos1) >= 0
            && Fixed.dotI64 outDir (intersectionPos1 - pos2) >= 0
        validOutwardsDirections2 outDir =
          Fixed.dotI64 outDir (intersectionPos2 - pos1) >= 0
            && Fixed.dotI64 outDir (intersectionPos2 - pos2) >= 0

    (vertex1, vertex2) <-
      if traversable
        then do
          vertex1 <-
            newVertex
              [(obstacleID1, OV_StartBoundary True), (obstacleID2, OV_EndBoundary True)]
              intersectionPos1
          vertex2 <-
            newVertex
              [(obstacleID2, OV_StartBoundary True), (obstacleID1, OV_EndBoundary True)]
              intersectionPos2
          addDirectEdge vertex1 vertex2
          let obstacleIDs = [obstacleID1, obstacleID2]
          registerTargetVertex
            TargetVertex
              { tv_vertex = vertex1
              , tv_tangentObstacleIDs = obstacleIDs
              , tv_validOutwardsDirections = validOutwardsDirections1
              }
          registerTargetVertex
            TargetVertex
              { tv_vertex = vertex2
              , tv_tangentObstacleIDs = obstacleIDs
              , tv_validOutwardsDirections = validOutwardsDirections2
              }
          pure (vertex1, vertex2)
        else do
          vertex1 <-
            newVertex
              [(obstacleID1, OV_StartBoundary False), (obstacleID2, OV_EndBoundary False)]
              intersectionPos1
          vertex2 <-
            newVertex
              [(obstacleID2, OV_StartBoundary False), (obstacleID1, OV_EndBoundary False)]
              intersectionPos2
          pure (vertex1, vertex2)

    pure
      ObstacleIntersection
        { oi_obstacleID1 = obstacleID1
        , oi_obstacleID2 = obstacleID2
        , oi_intersectionVertex1 = vertex1
        , oi_intersectionVertex2 = vertex2
        , oi_validOutwardsDirections1 = validOutwardsDirections1
        , oi_validOutwardsDirections2 = validOutwardsDirections2
        , oi_traversable = traversable
        , oi_vertexFree1 = posFree1
        , oi_vertexFree2 = posFree2
        }

-- | Upper bound for traversable obstacle overlaps
obstacleOverlapThreshold :: Fixed
obstacleOverlapThreshold = 40 -- lowMaxOverlap from OverseerMovement module

-- Intersection vertices assumed to be non-free and non-traversable
data WorldBoundsObstacleIntersection = WorldBoundsObstacleIntersection
  { wboi_obstacleID :: Int
  , wboi_intersectionVertex1 :: Vertex
  -- ^ First intersection on obstacle, going anti-clockwise
  , wboi_intersectionVertex2 :: Vertex
  -- ^ Second intersection on obstacle, going anti-clockwise
  , wboi_validOutwardsDirections1 :: Fixed2 -> Bool
  , wboi_validOutwardsDirections2 :: Fixed2 -> Bool
  , wboi_vertexFree1 :: Bool
  , wboi_vertexFree2 :: Bool
  }

-- For now, assume that there are no triplets of obstacles where a
-- point exists inside of all three.
findWorldBoundsObstacleIntersections ::
  DynamicsParams ->
  [Obstacle] ->
  State BuildGraphState [WorldBoundsObstacleIntersection]
findWorldBoundsObstacleIntersections params obstacles = do
  let worldRadius = gp_worldRadius params
      intersections =
        flip filter obstacles $ \obs ->
          not $ Fixed.normLeq (obs_pos obs) (worldRadius - obs_radius obs)
  for intersections $ \obstacle -> do
    let Obstacle obstacleID1 pos1 radius1 _ = obstacle
        pos2 = 0
        radius2 = worldRadius
        (intersectionPos1, intersectionPos2) =
          -- Swap boundaries because the world bounds occlude _outside_ the
          -- circle, not inside of it.
          swap $
            circleIntersections radius1 pos1 radius2 pos2

    let isPosOccluded pos =
          flip any obstacles $ \Obstacle{..} ->
            obs_id /= obstacleID1
              && Fixed.normLeq (obs_pos - pos) obs_radius
        posFree1 = not $ isPosOccluded intersectionPos1
        posFree2 = not $ isPosOccluded intersectionPos2

    let validOutwardsDirections1 outDir =
          Fixed.dotI64 outDir (intersectionPos1 - pos1) >= 0
            && Fixed.dotI64 outDir (intersectionPos1 - pos2) >= 0
        validOutwardsDirections2 outDir =
          Fixed.dotI64 outDir (intersectionPos2 - pos1) >= 0
            && Fixed.dotI64 outDir (intersectionPos2 - pos2) >= 0

    vertex1 <- newVertex [(obstacleID1, OV_StartBoundary False)] intersectionPos1
    vertex2 <- newVertex [(obstacleID1, OV_EndBoundary False)] intersectionPos2

    pure
      WorldBoundsObstacleIntersection
        { wboi_obstacleID = obstacleID1
        , wboi_intersectionVertex1 = vertex1
        , wboi_intersectionVertex2 = vertex2
        , wboi_validOutwardsDirections1 = validOutwardsDirections1
        , wboi_validOutwardsDirections2 = validOutwardsDirections2
        , wboi_vertexFree1 = posFree1
        , wboi_vertexFree2 = posFree2
        }

-- | Warning: Assumes that the circles intersect
--
-- Returns intersections in CCW order on the first obstacle (which is CW order
-- on the second obstacle).
circleIntersections :: Fixed -> Fixed2 -> Fixed -> Fixed2 -> (Fixed2, Fixed2)
circleIntersections radius1 pos1 radius2 pos2 =
  let (dist, dir) = Fixed.normAndNormalize (pos2 - pos1)
      (a, b) =
        -- https://math.stackexchange.com/questions/256100/how-can-i-find-the-points-at-which-two-circles-intersect
        let d = Fixed.toDouble dist
            r1 = Fixed.toDouble radius1
            r2 = Fixed.toDouble radius2
            sq x = x * x
            dSq = sq d
            rSq1 = sq r1
            rSq2 = sq r2
        in  ( Fixed.fromDouble $
                0.5 * (rSq1 - rSq2) / d
            , Fixed.fromDouble $
                (* 0.5) . sqrt $
                  2 * (rSq1 + rSq2)
                    - sq (rSq1 - rSq2) / dSq
                    - dSq
            )
      midpoint = Fixed.map (* 0.5) (pos1 + pos2) + Fixed.map (* a) dir
      offset = Fixed.map (* b) (perpendicularCCW dir)
      intersectionPos1 = midpoint - offset
      intersectionPos2 = midpoint + offset
  in  (intersectionPos1, intersectionPos2)

--------------------------------------------------------------------------------

-- | The `Int`s serve as unique identifiers for obstacles. Uniqueness is not
-- checked.
addObstacleTangents :: DynamicsParams -> [Obstacle] -> State BuildGraphState ()
addObstacleTangents params obstacles =
  let allTangents = do
        (Obstacle id1 pos1 radius1 _, Obstacle id2 pos2 radius2 _) <- unorderedDistinctPairs obstacles
        let crossTangents =
              case circleToCircleCrossTangents radius1 pos1 radius2 pos2 of
                Nothing -> []
                Just (pair1, pair2) -> [pair1, pair2]
            prependOuterTangents =
              case circleToCircleOuterTangents radius1 pos1 radius2 pos2 of
                Nothing -> id
                Just (outer1, outer2) -> \xs -> outer1 : outer2 : xs
        (tangentPos1, tangentPos2) <- prependOuterTangents crossTangents
        guard $
          Fixed.normLeq tangentPos1 (gp_worldRadius params)
            && Fixed.normLeq tangentPos2 (gp_worldRadius params)
        guard $
          not $
            flip any obstacles $ \(Obstacle id' pos' radius' _) ->
              id' /= id1
                && id' /= id2
                && isWithinDistanceOfLine tangentPos1 tangentPos2 radius' pos'
        pure (id1, tangentPos1, id2, tangentPos2)
  in  for_ allTangents $
        \(obstacleID1, tangentPos1, obstacleID2, tangentPos2) -> do
          vertex1 <- newVertex [(obstacleID1, OV_Waypoint)] tangentPos1
          vertex2 <- newVertex [(obstacleID2, OV_Waypoint)] tangentPos2
          addDirectEdge vertex1 vertex2

--------------------------------------------------------------------------------

-- | Generalized obstacles
data GenObstacle
  = GO_NormalObstacle Obstacle
  | GO_WorldBounds

movePointOutOfObstacles ::
  DynamicsParams ->
  Fixed2 ->
  [Obstacle] ->
  HashMap UnorderedInts ObstacleIntersection ->
  IntMap WorldBoundsObstacleIntersection ->
  State BuildGraphState (Maybe Vertex)
movePointOutOfObstacles params pointPos obstacles obsIntersections wbObsIntersections =
  let worldRadius = gp_worldRadius params
      obstacleOcclusions =
        map GO_NormalObstacle $
          flip filter obstacles $ \obs ->
            Fixed.normLeq (obs_pos obs - pointPos) (obs_radius obs)
      worldBoundsOcclusion =
        [ GO_WorldBounds
        | not (Fixed.normLeq pointPos worldRadius)
        ]
  in  case worldBoundsOcclusion ++ obstacleOcclusions of
        [] -> do
          vertex <- newVertex [] pointPos
          registerTargetVertex
            TargetVertex
              { tv_vertex = vertex
              , tv_tangentObstacleIDs = []
              , tv_validOutwardsDirections = const True
              }
          pure $ Just vertex
        [genObs] ->
          case genObs of
            GO_WorldBounds ->
              case projectToCircle 0 worldRadius pointPos of
                Nothing -> pure Nothing
                Just surfacePos -> do
                  let obstacleOcclusions' =
                        flip filter obstacles $ \obs' ->
                          Fixed.normLeq (obs_pos obs' - surfacePos) (obs_radius obs')
                  case obstacleOcclusions' of
                    [] -> do
                      vertex <- newVertex [] surfacePos
                      registerTargetVertex
                        TargetVertex
                          { tv_vertex = vertex
                          , tv_tangentObstacleIDs = []
                          , tv_validOutwardsDirections = const True
                          }
                      pure $ Just vertex
                    [obs2] ->
                      handleIntersection genObs (GO_NormalObstacle obs2)
                    _ ->
                      pure Nothing -- too many occlusions, give up
            GO_NormalObstacle obs -> do
              case projectToCircle (obs_pos obs) (obs_radius obs) pointPos of
                Nothing -> pure Nothing
                Just surfacePos -> do
                  let obstacleOcclusions' =
                        map GO_NormalObstacle $
                          flip filter obstacles $ \obs' ->
                            Fixed.normLeq (obs_pos obs' - surfacePos) (obs_radius obs')
                              && obs_id obs /= obs_id obs'
                      worldBoundsOcclusion' =
                        [ GO_WorldBounds
                        | not (Fixed.normLeq surfacePos worldRadius)
                        ]
                  case worldBoundsOcclusion' ++ obstacleOcclusions' of
                    [] -> do
                      vertex <- newVertex [(obs_id obs, OV_Waypoint)] surfacePos
                      registerTargetVertex
                        TargetVertex
                          { tv_vertex = vertex
                          , tv_tangentObstacleIDs = [obs_id obs]
                          , tv_validOutwardsDirections =
                              (>= 0) . Fixed.dotI64 (surfacePos - obs_pos obs)
                          }
                      pure $ Just vertex
                    [genObs2] ->
                      handleIntersection genObs genObs2
                    _ ->
                      pure Nothing -- too many occlusions, give up
        [genObs1, genObs2] ->
          handleIntersection genObs1 genObs2
        _ -> pure Nothing -- too many occlusions, give up
  where
    handleIntersection ::
      GenObstacle -> GenObstacle -> State BuildGraphState (Maybe Vertex)
    handleIntersection = \cases
      GO_WorldBounds GO_WorldBounds ->
        -- Should not happen
        error "movePointOutOfObstacles: programmer error"
      GO_WorldBounds (GO_NormalObstacle obs) ->
        handleWorldBoundsIntersection obs
      (GO_NormalObstacle obs) GO_WorldBounds ->
        handleWorldBoundsIntersection obs
      (GO_NormalObstacle obs1) (GO_NormalObstacle obs2) ->
        handleNormalIntersection obs1 obs2

    handleNormalIntersection ::
      Obstacle -> Obstacle -> State BuildGraphState (Maybe Vertex)
    handleNormalIntersection obs1 obs2 =
      case HashMap.lookup (makeUnorderedInts (obs_id obs1) (obs_id obs2)) obsIntersections of
        Nothing ->
          pure Nothing
        Just ObstacleIntersection{..} -> do
          let dist1 = Fixed.normSq (pointPos - v_pos oi_intersectionVertex1)
              dist2 = Fixed.normSq (pointPos - v_pos oi_intersectionVertex2)
              (vertex, vertexFree, isValidDir) =
                if dist1 < dist2
                  then (oi_intersectionVertex1, oi_vertexFree1, oi_validOutwardsDirections1)
                  else (oi_intersectionVertex2, oi_vertexFree2, oi_validOutwardsDirections2)
          if not vertexFree
            then pure Nothing -- too many occlusions, give up
            else do
              let vertexID = v_id vertex
              addBoundaryWaypoint oi_obstacleID1 vertexID
              addBoundaryWaypoint oi_obstacleID2 vertexID
              registerTargetVertex
                TargetVertex
                  { tv_vertex = vertex
                  , tv_tangentObstacleIDs = [oi_obstacleID1, oi_obstacleID2]
                  , tv_validOutwardsDirections = isValidDir
                  }
              pure $ Just vertex

    handleWorldBoundsIntersection ::
      Obstacle -> State BuildGraphState (Maybe Vertex)
    handleWorldBoundsIntersection obs =
      case IntMap.lookup (obs_id obs) wbObsIntersections of
        Nothing ->
          pure Nothing
        Just WorldBoundsObstacleIntersection{..} -> do
          let dist1 = Fixed.normSq (pointPos - v_pos wboi_intersectionVertex1)
              dist2 = Fixed.normSq (pointPos - v_pos wboi_intersectionVertex2)
              (vertex, vertexFree, isValidDir) =
                if dist1 < dist2
                  then
                    ( wboi_intersectionVertex1
                    , wboi_vertexFree1
                    , wboi_validOutwardsDirections1
                    )
                  else
                    ( wboi_intersectionVertex2
                    , wboi_vertexFree2
                    , wboi_validOutwardsDirections2
                    )

          -- old stuff below
          if not vertexFree
            then pure Nothing -- too many occlusions, give up
            else do
              let vertexID = v_id vertex
              addBoundaryWaypoint wboi_obstacleID vertexID
              registerTargetVertex
                TargetVertex
                  { tv_vertex = vertex
                  , tv_tangentObstacleIDs = [wboi_obstacleID]
                  , tv_validOutwardsDirections = isValidDir
                  }
              pure $ Just vertex

--------------------------------------------------------------------------------

linkTargetVertices :: DynamicsParams -> [Obstacle] -> State BuildGraphState ()
linkTargetVertices params obstacles = do
  targetVertices <- gets $ IntMap.elems . bgs_targetVertices
  -- vertex to vertex
  for_ (unorderedDistinctPairs targetVertices) $ \(tv1, tv2) -> do
    let v1 = tv_vertex tv1
        v2 = tv_vertex tv2
        pos1 = v_pos v1
        pos2 = v_pos v2
        validDir1 = tv_validOutwardsDirections tv1 (pos2 - pos1)
        validDir2 = tv_validOutwardsDirections tv2 (pos1 - pos2)
    when (validDir1 && validDir2) $ do
      let obstructedByObstacles =
            flip any obstacles $ \(Obstacle obstacleID obstaclePos obstacleRadius _) ->
              obstacleID `notElem` tv_tangentObstacleIDs tv1
                && obstacleID `notElem` tv_tangentObstacleIDs tv2
                && isWithinDistanceOfLine pos1 pos2 obstacleRadius obstaclePos
      unless obstructedByObstacles (addDirectEdge v1 v2)

  -- vertex to obstacle
  for_ targetVertices $ \TargetVertex{..} -> do
    let pos = v_pos tv_vertex
        tangentCandidates =
          flip concatMap obstacles $ \(Obstacle obstacleID obstaclePos obstacleRadius _) -> do
            guard $ obstacleID `notElem` tv_tangentObstacleIDs
            case pointToCircleTangents obstaclePos obstacleRadius pos of
              Nothing ->
                [] -- should not happen, but no harm
              Just (tangentPos1, tangentPos2) ->
                [(obstacleID, tangentPos1), (obstacleID, tangentPos2)]
    for_ tangentCandidates $ \(tangentObstacleID, tangentPos) -> do
      let validDir = tv_validOutwardsDirections (tangentPos - pos)
          obstructedByObstacles =
            flip any obstacles $ \(Obstacle obstacleID obstaclePos obstacleRadius _) ->
              obstacleID `notElem` tv_tangentObstacleIDs
                && obstacleID /= tangentObstacleID
                && isWithinDistanceOfLine pos tangentPos obstacleRadius obstaclePos
          inWorldBounds = Fixed.normLeq tangentPos (gp_worldRadius params)
      when (validDir && not obstructedByObstacles && inWorldBounds) $ do
        tangentVertex <-
          newVertex [(tangentObstacleID, OV_Waypoint)] tangentPos
        addDirectEdge tv_vertex tangentVertex

--------------------------------------------------------------------------------

data ObstacleVertex
  = OV_Waypoint Vertex
  | -- | The anti-clockwise of the two intersections between two obstacles
    OV_StartBoundary
      -- | Include vertex in graph
      Bool
      Vertex
  | -- | The clockwise of the two intersections between two obstacles
    OV_EndBoundary
      -- | Include vertex in graph
      Bool
      Vertex

getObstacleVertex :: ObstacleVertex -> Vertex
getObstacleVertex (OV_Waypoint sv) = sv
getObstacleVertex (OV_StartBoundary _ sv) = sv
getObstacleVertex (OV_EndBoundary _ sv) = sv

addWaypointToObstacleVertex :: ObstacleVertex -> ObstacleVertex
addWaypointToObstacleVertex wp@(OV_Waypoint _) = wp
addWaypointToObstacleVertex (OV_StartBoundary _ v) = OV_StartBoundary True v
addWaypointToObstacleVertex (OV_EndBoundary _ v) = OV_EndBoundary True v

obstacleRingEdges ::
  Fixed ->
  Fixed2 ->
  [ObstacleVertex] ->
  State BuildGraphState ()
obstacleRingEdges obstacleRadius obstacleCenter obstacleVertices = do
  let sortedVertices =
        List.sortOn
          (\v -> Fixed.atan2Vec (v_pos (getObstacleVertex v) - obstacleCenter))
          obstacleVertices
  case splitRingVertices sortedVertices of
    RingGroup ringGroup ->
      case ringGroup of
        [] -> pure ()
        [_] -> pure ()
        v1 : v2 : vs -> do
          let vRing = v1 : v2 : vs ++ [v1]
          interpolatedGroup <-
            interpolateArc obstacleCenter obstacleRadius vRing
          zipWithM_ addArcEdge interpolatedGroup (drop 1 interpolatedGroup)
    ArcGroups arcGroups ->
      for_ arcGroups $ \arcGroup -> do
        interpolatedArcGroup <-
          interpolateArc obstacleCenter obstacleRadius arcGroup
        case interpolatedArcGroup of
          [] -> pure ()
          v1 : vs -> zipWithM_ addArcEdge (v1 : vs) vs
  where
    addArcEdge v1 v2 =
      addEdge v1 v2 (arcLength obstacleCenter obstacleRadius v1 v2)

arcLength :: Fixed2 -> Fixed -> Vertex -> Vertex -> Fixed
arcLength obstacleCenter obstacleRadius w1 w2 =
  let ang =
        abs $
          Fixed.vectorAngle
            (v_pos w1 - obstacleCenter)
            (v_pos w2 - obstacleCenter)
  in  ang * obstacleRadius

-- Assumes vertices sorted in CCW order around the obstacle center
interpolateArc ::
  Fixed2 -> Fixed -> [Vertex] -> State BuildGraphState [Vertex]
interpolateArc obstacleCenter obstacleRadius = \case
  (v1 : v2 : vs) -> do
    let r1 = v_pos v1 - obstacleCenter
        r2 = v_pos v2 - obstacleCenter
    -- This function will loop infinitely if either `r1` or `r2` are zero.
    if r1 /= 0
      && r2 /= 0
      && Fixed.dot (Fixed.normalize r1) (Fixed.normalize r2) < invSqrt2
      then do
        let newPos = obstacleCenter + rotate45 r1
        v' <- newVertex' newPos
        (v1 :) <$> interpolateArc obstacleCenter obstacleRadius (v' : v2 : vs)
      else (v1 :) <$> interpolateArc obstacleCenter obstacleRadius (v2 : vs)
  vs -> pure vs

-- | Rotate a 2D vector 45 degrees anti-clockwise
rotate45 :: Fixed2 -> Fixed2
rotate45 (Fixed2 x y) =
  Fixed2 (invSqrt2 * (x - y)) (invSqrt2 * (x + y))

invSqrt2 :: Fixed
invSqrt2 = sqrt 0.5

-- TODO: Use List.NonEmpty?
data ObstaclePerimeterGroups
  = RingGroup [Vertex]
  | ArcGroups [[Vertex]]

splitRingVertices :: [ObstacleVertex] -> ObstaclePerimeterGroups
splitRingVertices [] = RingGroup []
splitRingVertices ovs@(_ : _) =
  let (groups, lastGroup) = splitRingVertices' ovs
  in  case groups of
        [] -> RingGroup lastGroup
        group1 : otherGroups -> ArcGroups $ (lastGroup ++ group1) : otherGroups

splitRingVertices' :: [ObstacleVertex] -> ([[Vertex]], [Vertex])
splitRingVertices' =
  unfoldrEither $ \ovs ->
    let (group, restMaybe) = breakRingVertices ovs
    in  case restMaybe of
          Nothing -> Left group
          Just rest -> Right (group, rest)

breakRingVertices :: [ObstacleVertex] -> ([Vertex], Maybe [ObstacleVertex])
breakRingVertices [] = ([], Nothing)
breakRingVertices (ov : ovs) =
  case ov of
    OV_Waypoint sv ->
      let (svs, ovs') = breakRingVertices ovs in (sv : svs, ovs')
    OV_StartBoundary include sv ->
      ([sv | include], Just ovs)
    OV_EndBoundary include sv ->
      ([], Just (if include then OV_Waypoint sv : ovs else ovs))

unfoldrEither :: (b -> Either r (a, b)) -> b -> ([a], r)
unfoldrEither f x =
  case f x of
    Left r -> ([], r)
    Right (a, x') -> let (as, r) = unfoldrEither f x' in (a : as, r)

--------------------------------------------------------------------------------

unorderedDistinctPairs :: [a] -> [(a, a)]
unorderedDistinctPairs xs = do
  slice <- List.tails xs
  case slice of
    [] -> []
    x : xs' -> map (x,) xs'
