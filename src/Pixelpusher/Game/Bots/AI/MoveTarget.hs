{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.MoveTarget (
  MoveTarget (..),
  BroadMoveTarget (..),
  MoveUrgency (..),
  computeMoveGoal,
) where

import Control.Monad.Trans.Reader
import Data.Bifunctor (second)
import Data.Foldable (foldl', minimumBy)
import Data.Function (on, (&))
import Data.Functor ((<&>))
import Data.List qualified as List
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe (mapMaybe)
import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamLocations
import Pixelpusher.Game.Time

import Pixelpusher.Game.Bots.AI.GeometryUtil (lineDistance)
import Pixelpusher.Game.Bots.AI.Pathfinding (Obstacle (..))
import Pixelpusher.Game.Bots.AI.RefinedView

--------------------------------------------------------------------------------
-- New bot state transition

data MoveTarget = MoveTarget
  { mt_broad :: BroadMoveTarget
  , mt_requiresPrecision :: Bool
  , mt_urgency :: MoveUrgency
  , mt_pathfindingObstacles :: [Obstacle]
  , mt_label :: String
  -- ^ For debugging
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data BroadMoveTarget = BroadMoveTarget
  { bmt_pos :: Fixed2
  , bmt_radius :: Fixed
  -- ^ Should not include the overseer radius
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data MoveUrgency
  = LowUrgency
  | MediumUrgency
  | HighUrgency
  | -- | Only used for when someone is about to score. TODO: replace with another mechanism
    ReallyHighUrgency
  deriving stock (Generic)
  deriving anyclass (Serialize)

computeMoveGoal ::
  GameParams -> Time -> RefinedBotView -> FlagCommand -> MoveTarget
computeMoveGoal params time botView@RefinedBotView{..} flagCmd
  | obv_hasFlag rbv_myOverseer =
      let moveTarget = runReader flagCarrier env
      in  tryCollectPowerups params time botView moveTarget
  | otherwise =
      let moveTarget = runReader nonFlagCarrier env
      in  tryCollectPowerups params time botView moveTarget
  where
    env = Env params botView flagCmd

-- | Hack: Replace move target with one targeting a powerup if getting the
-- powerup wouldn't be costly
tryCollectPowerups ::
  GameParams -> Time -> RefinedBotView -> MoveTarget -> MoveTarget
tryCollectPowerups params time botView@RefinedBotView{..} moveTarget@MoveTarget{..} =
  let BroadMoveTarget{..} = mt_broad
      myPos = obv_pos rbv_myOverseer
      distToTarget = max 0 $ Fixed.norm (bmt_pos - myPos) - bmt_radius

      maxExtraDistance =
        case mt_urgency of
          LowUrgency -> 480
          MediumUrgency -> 240
          HighUrgency -> 120
          ReallyHighUrgency -> 0

      isPowerupConvenient :: PowerupBotView -> Bool
      isPowerupConvenient powerupView =
        let distToPowerup = Fixed.norm (pbv_pos powerupView - myPos)
            distPowerupToTarget =
              max 0 $ Fixed.norm (bmt_pos - pbv_pos powerupView) - bmt_radius
        in  distToPowerup + distPowerupToTarget < distToTarget + maxExtraDistance

      shouldITakeThePowerup :: PowerupBotView -> Bool
      shouldITakeThePowerup PowerupBotView{..}
        | null rbv_enemyOverseers = myDistSq <= nearestAllyDistSq
        | otherwise =
            let myDist = Fixed.sqrtFixedSq myDistSq
                nearestEnemyDist = Fixed.sqrtFixedSq nearestEnemyDistSq
                nearestAllyDist = Fixed.sqrtFixedSq nearestAllyDistSq
            in  if nearestEnemyDist < nearestAllyDist + 240 -- arbitrary
                  then myDist <= nearestAllyDist + 60 -- arbitrary
                  else myDist <= nearestAllyDist
        where
          myDistSq = Fixed.normSq (pbv_pos - myPos)
          nearestEnemyDistSq =
            foldl' min Fixed.maxBoundFixedSq $
              map
                (\ov -> Fixed.normSq (pbv_pos - obv_pos ov))
                rbv_enemyOverseers
          nearestAllyDistSq =
            foldl' min Fixed.maxBoundFixedSq $
              map
                (\ov -> Fixed.normSq (pbv_pos - obv_pos ov))
                rbv_alliedOverseers

      powerupGetCost :: GenericPowerup -> Fixed
      powerupGetCost GenericPowerup{..} =
        let dist = max 0 $ Fixed.norm (gp_pos - myPos) - gp_radius
        in  dist / gp_health
      potentialPowerups =
        rbv_powerups
          & filter isPowerupConvenient
          & filter shouldITakeThePowerup
          & map powerupToGenericPowerup
      potentialPowerupSpawners =
        powerupSpawnersToGenericPowerup params time botView
      potentialPowerupsMaybe =
        List.NonEmpty.nonEmpty $
          potentialPowerupSpawners ++ potentialPowerups
  in  case potentialPowerupsMaybe of
        Nothing -> moveTarget
        Just powerups ->
          let chosenPowerup = minimumOn powerupGetCost powerups
          in  MoveTarget
                { mt_label = mt_label ++ " (collecting powerup)"
                , mt_broad =
                    BroadMoveTarget
                      { bmt_pos = gp_pos chosenPowerup
                      , bmt_radius = gp_radius chosenPowerup
                      }
                , mt_requiresPrecision = True
                , mt_urgency = mt_urgency
                , mt_pathfindingObstacles = []
                }

data GenericPowerup = GenericPowerup
  { gp_pos :: Fixed2
  , gp_radius :: Fixed
  , gp_health :: Fixed
  }

powerupToGenericPowerup :: PowerupBotView -> GenericPowerup
powerupToGenericPowerup PowerupBotView{..} =
  GenericPowerup
    { gp_pos = pbv_pos
    , gp_radius = pbv_radius
    , gp_health = pbv_health
    }

-- Implementation strategy for this hack: Remove consideration of activation
-- timing from future computations by handling it all now via filtering.
powerupSpawnersToGenericPowerup ::
  GameParams -> Time -> RefinedBotView -> [GenericPowerup]
powerupSpawnersToGenericPowerup params time botView
  | not (gp_enableShieldPowerupSpawners params) = []
  | otherwise =
      flip mapMaybe (mapPowerupSpawnLocations (gp_dynamics params)) $ \(powerupLoc, spawnerPos) ->
        case readMapPowerupStatus powerupLoc (rbv_mapPowerups botView) of
          MapPowerupSpawned{} -> Nothing
          MapPowerupConsumed consumeTime ->
            let activationTime =
                  addTicks (gp_shieldPowerupRespawnPeriod params) consumeTime
                ticksToActivation = activationTime `diffTime` time
                maxTicksToActivation = fromIntegral C.tickRate_hz
                myPos = obv_pos (rbv_myOverseer botView)
                myClass = obv_class (rbv_myOverseer botView)
                maxSpeed =
                  ldp_terminalVelocity $
                    getClassParam
                      myClass
                      (gp_overseerDragParams (gp_bots params))
                distToPowerupArea = Fixed.norm (myPos - spawnerPos)
                minTicksToActivation =
                  let buffer = 30
                      safetyFactor = 0.7
                  in  (distToPowerupArea + buffer) / (safetyFactor * maxSpeed)
            in  if ticksToActivation > maxTicksToActivation
                  || fromIntegral ticksToActivation < minTicksToActivation
                  then Nothing
                  else
                    Just
                      GenericPowerup
                        { gp_pos = spawnerPos
                        , gp_radius = 60 -- arbitrary
                        , gp_health =
                            gp_shieldPowerupHealth params * 0.333 -- hack: in case the already-spawned powerup hasn't been consumed
                        }

data Env = Env
  { params :: GameParams
  , botView :: RefinedBotView
  , flagCmd :: FlagCommand
  }

type FindMoveTarget = Reader Env MoveTarget

nonFlagCarrier :: FindMoveTarget
nonFlagCarrier = do
  Env{botView = botView@RefinedBotView{..}} <- ask

  let myPos = obv_pos rbv_myOverseer
      distAllied = Fixed.norm (myPos - fbv_pos rbv_alliedFlag)
      distEnemy = Fixed.norm (myPos - fbv_pos rbv_enemyFlag)

  case (fbv_type rbv_alliedFlag, fbv_type rbv_enemyFlag) of
    (BaseFlagType, BaseFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          captureEnemyBaseFlag 3 (Just (justFightAndStuff Nothing))
        EnemyTeamAdvantage ->
          retreat
        NeutralTeamAdvantage ->
          captureEnemyBaseFlag 1 (Just (justFightAndStuff Nothing))
    (BaseFlagType, OverseerFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          justFightAndStuff (Just (escortFlagCarrier Nothing))
        EnemyTeamAdvantage ->
          escortFlagCarrier Nothing
        NeutralTeamAdvantage ->
          escortFlagCarrier (Just (justFightAndStuff Nothing))
    (BaseFlagType, DroppedFlagType) ->
      let distanceToFlag =
            Fixed.norm $ obv_pos rbv_myOverseer - fbv_pos rbv_enemyFlag
          teamAdvantage =
            if distanceToFlag < C.viewRadius
              then localTeamAdvantage botView
              else globalTeamAdvantage botView
      in  case teamAdvantage of
            AlliedTeamAdvantage ->
              captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))
            EnemyTeamAdvantage ->
              retreat
            NeutralTeamAdvantage ->
              captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))
    (OverseerFlagType, BaseFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          if distAllied < distEnemy
            then
              pursueEnemyFlagCarrier
                (Just (captureEnemyBaseFlag 3 (Just (justFightAndStuff Nothing))))
            else
              captureEnemyBaseFlag
                3
                (Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing))))
        EnemyTeamAdvantage ->
          let localAdvantage =
                length' rbv_alliedOverseers >= length' rbv_enemyOverseers
          in  if distAllied < C.viewRadius && localAdvantage
                then pursueEnemyFlagCarrier Nothing
                else retreat
        NeutralTeamAdvantage ->
          if distAllied < distEnemy
            then
              pursueEnemyFlagCarrier
                (Just (captureEnemyBaseFlag 3 (Just (justFightAndStuff Nothing))))
            else
              captureEnemyBaseFlag
                3
                (Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing))))
    (OverseerFlagType, OverseerFlagType) ->
      -- Be more aggressive to prevent boring stalemates
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          pursueEnemyFlagCarrier
            (Just (escortFlagCarrier (Just (justFightAndStuff Nothing))))
        EnemyTeamAdvantage ->
          escortFlagCarrier Nothing
        NeutralTeamAdvantage ->
          if distAllied < distEnemy
            then
              pursueEnemyFlagCarrier
                (Just (escortFlagCarrier (Just (justFightAndStuff Nothing))))
            else escortFlagCarrier $
              case localTeamAdvantage botView of
                AlliedTeamAdvantage ->
                  Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing)))
                EnemyTeamAdvantage ->
                  Nothing
                NeutralTeamAdvantage ->
                  Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing)))
    (OverseerFlagType, DroppedFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          if distAllied < distEnemy
            then
              pursueEnemyFlagCarrier
                (Just (captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))))
            else
              captureDroppedEnemyFlag
                (Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing))))
        EnemyTeamAdvantage ->
          case localTeamAdvantage botView of
            EnemyTeamAdvantage ->
              retreat
            AlliedTeamAdvantage ->
              if
                | distAllied < C.viewRadius ->
                    pursueEnemyFlagCarrier
                      (Just (captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))))
                | distEnemy < C.viewRadius ->
                    captureDroppedEnemyFlag
                      (Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing))))
                | otherwise ->
                    retreat
            NeutralTeamAdvantage ->
              if distEnemy < C.viewRadius
                then captureDroppedEnemyFlag Nothing
                else retreat
        NeutralTeamAdvantage ->
          if distAllied < distEnemy
            then
              pursueEnemyFlagCarrier
                (Just (captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))))
            else
              captureDroppedEnemyFlag
                (Just (pursueEnemyFlagCarrier (Just (justFightAndStuff Nothing))))
    (DroppedFlagType, BaseFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          if distAllied < distEnemy
            then
              recoverDroppedAlliedFlag
                (Just (captureEnemyBaseFlag 3 (Just (justFightAndStuff Nothing))))
            else
              captureEnemyBaseFlag
                3
                (Just (recoverDroppedAlliedFlag (Just (justFightAndStuff Nothing))))
        EnemyTeamAdvantage ->
          if distAllied < C.viewRadius
            then case localTeamAdvantage botView of
              EnemyTeamAdvantage ->
                retreat
              AlliedTeamAdvantage ->
                recoverDroppedAlliedFlag (Just retreat)
              NeutralTeamAdvantage ->
                recoverDroppedAlliedFlag (Just retreat)
            else retreat
        NeutralTeamAdvantage ->
          recoverDroppedAlliedFlag
            (Just (justFightAndStuff Nothing))
    (DroppedFlagType, OverseerFlagType) ->
      case globalTeamAdvantage botView of
        AlliedTeamAdvantage ->
          if distAllied < distEnemy
            then recoverDroppedAlliedFlag (Just (justFightAndStuff Nothing))
            else justFightAndStuff (Just (recoverDroppedAlliedFlag Nothing))
        EnemyTeamAdvantage ->
          if
            | distAllied < 500 -> -- arbitrary
                recoverDroppedAlliedFlag Nothing
            | distEnemy < 500 -> -- arbitrary
                escortFlagCarrier Nothing
            | otherwise ->
                retreat
        NeutralTeamAdvantage ->
          if distAllied < distEnemy
            then
              recoverDroppedAlliedFlag
                (Just (escortFlagCarrier (Just (justFightAndStuff Nothing))))
            else
              escortFlagCarrier
                (Just (recoverDroppedAlliedFlag (Just (justFightAndStuff Nothing))))
    (DroppedFlagType, DroppedFlagType) ->
      if distAllied < distEnemy
        then
          recoverDroppedAlliedFlag
            (Just (captureDroppedEnemyFlag (Just (justFightAndStuff Nothing))))
        else
          captureDroppedEnemyFlag
            (Just (recoverDroppedAlliedFlag (Just (justFightAndStuff Nothing))))

data TeamAdvantage
  = AlliedTeamAdvantage
  | EnemyTeamAdvantage
  | NeutralTeamAdvantage

globalTeamAdvantage :: RefinedBotView -> TeamAdvantage
globalTeamAdvantage RefinedBotView{..}
  | rbv_alliesAlive * 4 < rbv_enemiesAlive * 3 = EnemyTeamAdvantage
  | rbv_alliesAlive * 3 > rbv_enemiesAlive * 4 = AlliedTeamAdvantage
  | otherwise = NeutralTeamAdvantage

localTeamAdvantage :: RefinedBotView -> TeamAdvantage
localTeamAdvantage RefinedBotView{..}
  | alliedOverseers < enemyOverseers = EnemyTeamAdvantage
  | alliedOverseers > enemyOverseers = AlliedTeamAdvantage
  | otherwise = NeutralTeamAdvantage
  where
    alliedOverseers = length' rbv_alliedOverseers + 1 -- count self
    enemyOverseers = length' rbv_enemyOverseers

flagCarrier :: FindMoveTarget
flagCarrier = do
  Env{params, botView = botView@RefinedBotView{..}} <- ask
  let dynParams = gp_dynamics params
      myPos = obv_pos rbv_myOverseer
      myTeam = obv_team rbv_myOverseer
      myFlagPos = fbv_pos rbv_alliedFlag
      localAllies = length' rbv_alliedOverseers
      localEnemies = length' rbv_enemyOverseers

  let alliedFlagWillBeAtHomeBase =
        fbv_type rbv_alliedFlag == BaseFlagType
          || alliedAdvantageAtAlliedPos botView myFlagPos

  let okayToRecoverAlliedFlag =
        -- TODO: Rename to `safeToRecoverAlliedFlag`
        let distToMe = Fixed.norm (myFlagPos - myPos)
            distToAlliedBase =
              Fixed.norm $
                myFlagPos - teamBaseLocation dynParams myTeam
            distToEnemyBase =
              Fixed.norm $
                myFlagPos - teamBaseLocation dynParams (oppositeTeam myTeam)
        in  distToAlliedBase < distToEnemyBase
              || (distToMe < 360 && localAllies > localEnemies)

  if alliedFlagWillBeAtHomeBase
    then flagCarrierTryToScore
    else
      if
        | rbv_alliesAlive * 3 > rbv_enemiesAlive * 4
        , localAllies > localEnemies ->
            -- Global and local advantage
            case fbv_type rbv_alliedFlag of
              OverseerFlagType ->
                justFightAndStuff (Just (pure occupyCenter))
              DroppedFlagType ->
                if okayToRecoverAlliedFlag
                  then
                    recoverDroppedAlliedFlag
                      (Just (justFightAndStuff (Just flagCarrierRetreat)))
                  else justFightAndStuff (Just flagCarrierRetreat)
              BaseFlagType ->
                error "flagCarrier: programmer error"
        | rbv_alliesAlive >= rbv_enemiesAlive
        , localAllies >= localEnemies ->
            -- No global or local disadvantage
            case fbv_type rbv_alliedFlag of
              OverseerFlagType ->
                pure occupyCenter
              DroppedFlagType ->
                if okayToRecoverAlliedFlag
                  then recoverDroppedAlliedFlag (Just flagCarrierRetreat)
                  else pure occupyCenter
              BaseFlagType ->
                error "flagCarrier: programmer error"
        | otherwise -> flagCarrierRetreat

alliedAdvantageAtAlliedPos :: RefinedBotView -> Fixed2 -> Bool
alliedAdvantageAtAlliedPos RefinedBotView{..} targetPos =
  let isNearTarget pos =
        Fixed.normLeq (pos - targetPos) 425 -- arbitrary
      enemiesNearFlag =
        length' $
          filter isNearTarget rbv_visibleEnemyOverseerPositions
      alliesNearFlag =
        length' (filter (isNearTarget . snd) rbv_alliedOverseerPositions)
          - if isNearTarget (obv_pos rbv_myOverseer) then 1 else 0
  in  4 * alliesNearFlag >= 5 * enemiesNearFlag + 2

flagCarrierTryToScore :: FindMoveTarget
flagCarrierTryToScore =
  ask <&> \Env{params, botView = botView@RefinedBotView{..}} ->
    let dynParams = gp_dynamics params
        myPos = obv_pos rbv_myOverseer
        myTeam = obv_team rbv_myOverseer
        myBase = teamBaseLocation dynParams myTeam

        collisionRadius =
          let myClass = obv_class rbv_myOverseer
              myRadius =
                getClassParam myClass $ gp_overseerRadius (gp_dynamics params)
          in  myRadius + gp_teamBaseRadius dynParams
        noReturn = Fixed.normLeq (myPos - myBase) (collisionRadius + 180) -- arbitrary
        noEnemyAdvantageAtAlliedBase =
          let isNearBase pos =
                Fixed.normLeq (pos - myBase) 425 -- arbitrary
              enemiesNearBase =
                length' $
                  filter isNearBase rbv_visibleEnemyOverseerPositions
              alliesNearBase =
                length' (filter (isNearBase . snd) rbv_alliedOverseerPositions)
          in  alliesNearBase >= enemiesNearBase
    in  MoveTarget
          { mt_label = "Flag carrier: Score"
          , mt_broad =
              withAllies
                botView
                BroadMoveTarget
                  { bmt_pos = myBase
                  , bmt_radius =
                      if noReturn || noEnemyAdvantageAtAlliedBase
                        then gp_teamBaseRadius dynParams
                        else 500 -- arbitrary
                  }
          , mt_requiresPrecision = True
          , mt_urgency =
              if noReturn
                then ReallyHighUrgency
                else HighUrgency
          , mt_pathfindingObstacles =
              if noReturn
                then []
                else flip map rbv_enemyOverseers $ \obv ->
                  Obstacle
                    { obs_id = obv_entityID obv
                    , obs_pos = obv_pos obv
                    , obs_radius = 240 -- arbitrary
                    , obs_intersectionsTraversable = False
                    }
          }

occupyCenter :: MoveTarget
occupyCenter =
  MoveTarget
    { mt_label = "Flag carrier: Occupy center"
    , mt_broad =
        BroadMoveTarget
          { bmt_pos = 0 -- map enter
          , bmt_radius = 480 -- arbitrary
          }
    , mt_requiresPrecision = False
    , mt_urgency = LowUrgency
    , mt_pathfindingObstacles = []
    }

flagCarrierRetreat :: FindMoveTarget
flagCarrierRetreat = reader $ \Env{params, botView = botView@RefinedBotView{..}} ->
  let dynParams = gp_dynamics params
      myPos = obv_pos rbv_myOverseer
      myTeam = obv_team rbv_myOverseer
      myBase = teamBaseLocation (gp_dynamics params) myTeam
      mySpawn = spawnAreaLocation params myTeam

      broadMoveTarget
        | lineDistance myBase mySpawn myPos < 240 -- arbitrary
            && length' rbv_alliedOverseers < length' rbv_enemyOverseers =
            BroadMoveTarget
              { bmt_pos = mySpawn
              , bmt_radius = 240 -- arbitrary
              }
        | otherwise =
            withAllies
              botView
              BroadMoveTarget
                { bmt_pos = myBase
                , bmt_radius = gp_teamBaseRadius dynParams -- Ready to score
                }
  in  MoveTarget
        { mt_label = "Flag carrier: Retreat"
        , mt_broad = broadMoveTarget
        , mt_requiresPrecision = False
        , mt_urgency = LowUrgency
        , mt_pathfindingObstacles = []
        }

retreat :: FindMoveTarget
retreat = reader $ \Env{params, botView} ->
  MoveTarget
    { mt_label = "Retreat"
    , mt_broad = retreat' params botView
    , mt_requiresPrecision = False
    , mt_urgency = LowUrgency
    , mt_pathfindingObstacles = []
    }

retreat' :: GameParams -> RefinedBotView -> BroadMoveTarget
retreat' params botView@RefinedBotView{..}
  | lineDistance myBase mySpawn myPos < radius
      && length' rbv_alliedOverseers + 1 < length' rbv_enemyOverseers =
      withAllies
        botView
        BroadMoveTarget
          { bmt_pos = mySpawn
          , bmt_radius = gp_spawnAreaRadius (gp_dynamics params)
          }
  | otherwise =
      withAllies
        botView
        BroadMoveTarget
          { bmt_pos = myBase
          , bmt_radius = radius
          }
  where
    myPos = obv_pos rbv_myOverseer
    myTeam = obv_team rbv_myOverseer
    myBase = teamBaseLocation (gp_dynamics params) myTeam
    mySpawn = spawnAreaLocation params myTeam
    radius = 240 -- arbitrary

-- Seek fights and powerup spawners. Not exactly sure how this interacts with
-- the other powerup-seeking logic.
justFightAndStuff :: Maybe FindMoveTarget -> FindMoveTarget
justFightAndStuff nextMaybe = do
  Env{params, botView = botView@RefinedBotView{..}} <- ask
  let myPos = obv_pos rbv_myOverseer
      enemyCandidates =
        filter (\pos -> pursueLimitedAdvantageObjective botView 2 pos 450) $
          take 8 $ -- Limit enemy count to reduce computation
            List.sortOn
              (Fixed.normSq . subtract myPos)
              rbv_visibleEnemyOverseerPositions
      enemyCandidatesWithScores =
        map (\pos -> (pos, Fixed.norm (pos - myPos))) enemyCandidates

  let powerupSpawnersWithScores =
        mapPowerupSpawnLocations (gp_dynamics params)
          & map snd
          & filter
            (\spawnerPos -> not $ Fixed.normLeq (myPos - spawnerPos) C.viewRadius)
          -- With the spawner in view, the other powerup-seeking logic
          -- should take over
          & map \spawnerPos ->
            let distToSpawner = Fixed.norm (spawnerPos - myPos)
            in  case List.NonEmpty.nonEmpty enemyCandidates of
                  Nothing -> (spawnerPos, distToSpawner)
                  Just enemyCandidates' ->
                    let closestEnemyPos =
                          minimumOn
                            (\enemyPos -> Fixed.normSq (enemyPos - spawnerPos))
                            enemyCandidates'
                        distanceAllowance = 480 -- arbitrary
                        distEnemySpawner = Fixed.norm (closestEnemyPos - spawnerPos)
                    in  (spawnerPos, distToSpawner + distEnemySpawner - distanceAllowance)

  nextTargetWithScore <-
    case nextMaybe of
      Nothing -> pure []
      Just next ->
        if not (List.null enemyCandidatesWithScores)
          then pure []
          else do
            nextTarget <- next
            let targetPos = bmt_pos (mt_broad nextTarget)
                distanceAllowance =
                  case mt_urgency nextTarget of
                    LowUrgency -> 0
                    MediumUrgency -> 500 -- arbitrary
                    HighUrgency -> 1000 -- arbitrary
                    ReallyHighUrgency -> 2000 -- arbitrary
            pure $
              List.singleton
                (targetPos, Fixed.norm (targetPos - myPos) - distanceAllowance)

  let targetPos =
        let mapCenter = 0
        in  maybe mapCenter (fst . minimumOn snd) $
              List.NonEmpty.nonEmpty $
                nextTargetWithScore
                  ++ powerupSpawnersWithScores
                  ++ enemyCandidatesWithScores

  pure
    MoveTarget
      { mt_label = "Fight"
      , mt_broad =
          BroadMoveTarget
            { bmt_pos = targetPos
            , bmt_radius =
                case rbv_myDrones of
                  Just _ ->
                    C.viewRadius
                  Nothing ->
                    -- Be more cautious when you don't have any drones
                    C.viewRadius + 120 -- arbitrary
            }
      , mt_requiresPrecision = False
      , mt_urgency = LowUrgency
      , mt_pathfindingObstacles = []
      }

escortFlagCarrier :: Maybe FindMoveTarget -> FindMoveTarget
escortFlagCarrier nextMaybe = do
  Env{botView = botView@RefinedBotView{..}} <- ask
  let radius = 425 -- arbitrary
      escortMoveTarget =
        MoveTarget
          { mt_label = "Protect flag carrier"
          , mt_broad =
              withAllies botView $
                BroadMoveTarget
                  { bmt_pos = fbv_pos rbv_enemyFlag
                  , bmt_radius = radius
                  }
          , mt_requiresPrecision = False
          , mt_urgency = LowUrgency
          , mt_pathfindingObstacles = []
          }

  case nextMaybe of
    Nothing -> pure escortMoveTarget
    Just next ->
      let targetPos = fbv_pos rbv_enemyFlag
      in  if pursueLimitedAdvantageObjective botView 1 targetPos radius
            then pure escortMoveTarget
            else next

captureEnemyBaseFlag :: Int -> Maybe FindMoveTarget -> FindMoveTarget
captureEnemyBaseFlag maxOverseers nextMaybe = do
  Env{params, botView = botView@RefinedBotView{..}, flagCmd} <- ask
  let shouldDropFlag' = case flagCmd of FlagNeutral -> False; FlagDrop -> True
      captureMoveTarget =
        MoveTarget
          { mt_label = "Get enemy base flag"
          , mt_broad =
              withAllies botView $
                BroadMoveTarget
                  { bmt_pos = fbv_pos rbv_enemyFlag
                  , bmt_radius =
                      if shouldDropFlag'
                        then 450 -- arbitrary
                        else gp_flagRadius (gp_dynamics params)
                  }
          , mt_requiresPrecision = not shouldDropFlag'
          , mt_urgency =
              let myTeam = obv_team rbv_myOverseer
                  flagDistToAlliedBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos rbv_alliedFlag
                          - teamBaseLocation (gp_dynamics params) myTeam
                  flagDistToEnemyBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos rbv_alliedFlag
                          - teamBaseLocation (gp_dynamics params) (oppositeTeam myTeam)
                  teamBaseRadius = gp_teamBaseRadius (gp_dynamics params)
                  myPos = obv_pos rbv_myOverseer
                  myDistToEnemyFlag =
                    Fixed.norm $ myPos - fbv_pos rbv_enemyFlag
              in  if fbv_type rbv_alliedFlag == OverseerFlagType
                    then
                      if
                        | flagDistToEnemyBase > flagDistToAlliedBase ->
                            MediumUrgency
                        | flagDistToEnemyBase > 240 + teamBaseRadius -> -- arbitrary
                            HighUrgency
                        | myDistToEnemyFlag < 240 && rbv_enemyMatchPoint -> -- TODO
                            ReallyHighUrgency
                        | otherwise ->
                            HighUrgency
                    else LowUrgency
          , mt_pathfindingObstacles = []
          }

  case nextMaybe of
    Nothing -> pure captureMoveTarget
    Just next ->
      let targetPos = fbv_pos rbv_enemyFlag
          radius = 425
          allowEnemyFlagPursuit =
            not $ gp_botDeprioritizeEnemyFlag (gp_bots params)
          belowLimit =
            pursueLimitedAdvantageObjective botView maxOverseers targetPos radius
      in  if allowEnemyFlagPursuit && belowLimit
            then pure captureMoveTarget
            else next

captureDroppedEnemyFlag :: Maybe FindMoveTarget -> FindMoveTarget
captureDroppedEnemyFlag nextMaybe = do
  Env{params, botView, flagCmd} <- ask
  let shouldDropFlag' = case flagCmd of FlagNeutral -> False; FlagDrop -> True
      captureMoveTarget =
        MoveTarget
          { mt_label = "Get dropped enemy flag"
          , mt_broad =
              withAllies botView $
                BroadMoveTarget
                  { bmt_pos = fbv_pos (rbv_enemyFlag botView)
                  , bmt_radius =
                      if shouldDropFlag'
                        then 450 -- arbitrary
                        else gp_flagRadius (gp_dynamics params)
                  }
          , mt_requiresPrecision = not shouldDropFlag'
          , mt_urgency =
              let myTeam = obv_team $ rbv_myOverseer botView
                  flagDistToAlliedBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos (rbv_alliedFlag botView)
                          - teamBaseLocation (gp_dynamics params) myTeam
                  flagDistToEnemyBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos (rbv_alliedFlag botView)
                          - teamBaseLocation (gp_dynamics params) (oppositeTeam myTeam)
                  teamBaseRadius = gp_teamBaseRadius (gp_dynamics params)
                  myPos = obv_pos $ rbv_myOverseer botView
                  myDistToEnemyFlag =
                    Fixed.norm $ myPos - fbv_pos (rbv_enemyFlag botView)
              in  if fbv_type (rbv_alliedFlag botView) == OverseerFlagType
                    then
                      if
                        | flagDistToEnemyBase > flagDistToAlliedBase ->
                            MediumUrgency
                        | flagDistToEnemyBase > 240 + teamBaseRadius -> -- arbitrary
                            HighUrgency
                        | myDistToEnemyFlag < 240 && rbv_enemyMatchPoint botView ->
                            ReallyHighUrgency
                        | otherwise ->
                            HighUrgency
                    else MediumUrgency
          , mt_pathfindingObstacles = []
          }

  case nextMaybe of
    Nothing -> pure captureMoveTarget
    Just next ->
      let targetPos = fbv_pos (rbv_enemyFlag botView)
          radius = 425 -- arbitrary
          allowEnemyFlagPursuit =
            not $ gp_botDeprioritizeEnemyFlag (gp_bots params)
          belowLimit = pursueLimitedAdvantageObjective botView 3 targetPos radius
      in  if allowEnemyFlagPursuit && belowLimit
            then pure captureMoveTarget
            else next

recoverDroppedAlliedFlag :: Maybe FindMoveTarget -> FindMoveTarget
recoverDroppedAlliedFlag nextMaybe = do
  Env{params, botView = botView@RefinedBotView{..}} <- ask
  let dynParams = gp_dynamics params

  let myFlag = fbv_pos rbv_alliedFlag
      enemyFlag = fbv_pos rbv_enemyFlag

      myTeam = obv_team rbv_myOverseer
      myBase = teamBaseLocation dynParams myTeam
      enemyBase = teamBaseLocation dynParams (oppositeTeam myTeam)

  let recoveryMoveTarget =
        MoveTarget
          { mt_label = "Recover dropped allied flag"
          , mt_broad =
              withAllies botView $
                BroadMoveTarget
                  { bmt_pos = myFlag
                  , bmt_radius = gp_flagRecoveryRadius dynParams
                  }
          , mt_requiresPrecision = True
          , mt_urgency =
              let distToAlliedBase = Fixed.norm $ myFlag - myBase
                  distToEnemyBase = Fixed.norm $ myFlag - enemyBase
              in  if
                    | distToAlliedBase < 240 -> LowUrgency
                    | distToAlliedBase < distToEnemyBase -> MediumUrgency
                    | otherwise -> HighUrgency
          , mt_pathfindingObstacles = []
          }

  case nextMaybe of
    Nothing -> pure recoveryMoveTarget
    Just next ->
      let flagNeedsRecovering =
            let flagIsBetterWhereItIs =
                  let flagTooCloseToBase = Fixed.normLeq (myBase - myFlag) 180
                      flagFurtherFromEnemyBase =
                        Fixed.normSq (myFlag - enemyBase)
                          > Fixed.normSq (myBase - enemyBase)
                      flagSaferWhereItIs =
                        alliedAdvantageAtAlliedPos botView myFlag
                          && not (alliedAdvantageAtAlliedPos botView myBase)
                  in  flagTooCloseToBase
                        || flagFurtherFromEnemyBase
                        || flagSaferWhereItIs
                enemyFlagNearAlliedBase =
                  Fixed.normLeq (enemyFlag - myBase) 360 -- arbitrary
                readyToCaptureEnemyFlag =
                  case fbv_type rbv_enemyFlag of
                    BaseFlagType ->
                      False
                    DroppedFlagType ->
                      enemyFlagNearAlliedBase
                        && alliedAdvantageAtAlliedPos botView enemyFlag
                    OverseerFlagType ->
                      enemyFlagNearAlliedBase
            in  not flagIsBetterWhereItIs || readyToCaptureEnemyFlag

          moreAssistanceNeeded =
            pursueLimitedAdvantageObjective botView 3 myFlag 425 -- arbitrary
      in  if flagNeedsRecovering && moreAssistanceNeeded
            then pure recoveryMoveTarget
            else next

pursueEnemyFlagCarrier :: Maybe FindMoveTarget -> FindMoveTarget
pursueEnemyFlagCarrier nextMaybe = do
  Env{params, botView = botView@RefinedBotView{..}} <- ask
  let pursueMoveTarget =
        MoveTarget
          { mt_label = "Pursue enemy flag carrier"
          , mt_broad =
              withAllies botView $
                let flagPos = fbv_pos rbv_alliedFlag
                    enemyBasePos =
                      teamBaseLocation (gp_dynamics params) $
                        oppositeTeam $
                          obv_team rbv_myOverseer
                    (distToEnemyBase, enemyBaseDir) =
                      Fixed.normAndNormalize (enemyBasePos - flagPos)
                    teamBaseRadius = gp_teamBaseRadius (gp_dynamics params)
                    overseerRadius = gp_overseerRadius_base (gp_dynamics params)
                    distToScore = distToEnemyBase - teamBaseRadius - overseerRadius
                in  BroadMoveTarget
                      { bmt_pos =
                          let size = max 0 $ min 200 distToScore
                          in  flagPos + Fixed.map (* size) enemyBaseDir
                      , bmt_radius = min 200 $ max 0 $ distToScore - 120
                      }
          , mt_requiresPrecision = False
          , mt_urgency =
              let myTeam = obv_team rbv_myOverseer
                  distToAlliedBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos rbv_alliedFlag
                          - teamBaseLocation (gp_dynamics params) myTeam
                  distToEnemyBase =
                    max 1 $
                      Fixed.norm $
                        fbv_pos rbv_alliedFlag
                          - teamBaseLocation (gp_dynamics params) (oppositeTeam myTeam)
              in  if
                    | fbv_type rbv_enemyFlag /= BaseFlagType -> LowUrgency
                    | distToEnemyBase < distToAlliedBase -> HighUrgency
                    | otherwise -> MediumUrgency
          , mt_pathfindingObstacles = []
          }

  case nextMaybe of
    Nothing -> pure pursueMoveTarget
    Just next ->
      let targetPos = fbv_pos rbv_alliedFlag
          radius = 425 -- arbitrary
      in  if pursueLimitedAdvantageObjective botView 3 targetPos radius
            then pure pursueMoveTarget
            else next

withAllies :: RefinedBotView -> BroadMoveTarget -> BroadMoveTarget
withAllies RefinedBotView{..} bmt@BroadMoveTarget{..}
  | distanceTo <= allyClosenessRadius = bmt
  | otherwise =
      let targetDir = Fixed.normalize $ bmt_pos - myPos
          projectionOnTargetDir v =
            Fixed.dot targetDir (Fixed.normalize v)
          forwardAllies =
            filter
              ( \allyPos ->
                  projectionOnTargetDir (allyPos - myPos) > 0.7 -- ~ 45 deg
                    && Fixed.normGt (allyPos - myPos) allyClosenessRadius
              )
              (map snd rbv_alliedOverseerPositions)
          (withAlly, targetPos) =
            minimumOn
              (Fixed.norm . subtract myPos . snd)
              ((WithoutAlly, bmt_pos) :| map (WithAlly,) forwardAllies)
          radius =
            case withAlly of
              WithAlly -> allyClosenessRadius
              WithoutAlly -> bmt_radius
      in  BroadMoveTarget
            { bmt_pos = targetPos
            , bmt_radius = radius
            }
  where
    myPos = obv_pos rbv_myOverseer
    distanceTo = Fixed.norm (myPos - bmt_pos)
    allyClosenessRadius = 360 -- arbitrary

data WithAlly = WithAlly | WithoutAlly

pursueLimitedAdvantageObjective ::
  RefinedBotView -> Int -> Fixed2 -> Fixed -> Bool
pursueLimitedAdvantageObjective RefinedBotView{..} minAllocation targetPos radius =
  let myActorID = obv_actorID rbv_myOverseer
      numEnemiesNearFlagCarrier =
        length' $
          filter
            (\pos -> Fixed.normLeq (pos - targetPos) radius)
            rbv_visibleEnemyOverseerPositions
      numAlliesNeeded =
        max minAllocation (numEnemiesNearFlagCarrier + 1) -- to have an advantage
      closestAlliesSorted =
        map fst $
          List.sortBy (compare `on` snd) $
            map
              (second (Fixed.normSq . subtract targetPos))
              rbv_alliedOverseerPositions
      assignedEscorts = take numAlliesNeeded closestAlliesSorted
  in  myActorID `elem` assignedEscorts

length' :: [a] -> Int
length' = foldl' (\z _ -> let !z' = z + 1 in z') 0

minimumOn :: (Ord b) => (a -> b) -> NonEmpty a -> a
minimumOn f xs = fst $ minimumBy (compare `on` snd) $ fmap (\x -> (x, f x)) xs
