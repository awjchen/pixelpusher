{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.RefinedView (
  RefinedBotView (..),
  RefinedBotDronesView (..),
  refineBotView,
) where

import Data.Foldable (foldl')
import Data.List qualified as List
import Data.List.NonEmpty qualified as List (NonEmpty)
import Data.List.NonEmpty qualified as List.NonEmpty
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.State.Store.Dynamics.Collisions
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

import Pixelpusher.Game.Bots.AI.GeometryUtil (geometricMedian)
import Pixelpusher.Game.Bots.AI.Intentions
import Pixelpusher.Game.Bots.AI.Kinematics

--------------------------------------------------------------------------------

-- | For factorization of the bot logic.
--
-- A refined version of 'BotView' that takes into account the bot's control
-- history for more accurate forward simulation of the kinematics of the bot's
-- entities.
data RefinedBotView = RefinedBotView
  { rbv_controlTime :: Time
  -- ^ Time at which any actions will take place, after being delayed by the
  -- control queue
  , rbv_predictionTime :: Time
  -- ^ The time to which entities positions are extrapolated
  , rbv_alliedFlag :: FlagBotView
  , rbv_enemyFlag :: FlagBotView
  , rbv_myOverseer :: OverseerBotView
  , rbv_myDrones :: Maybe RefinedBotDronesView
  , rbv_alliedOverseers :: [OverseerBotView]
  , rbv_alliedDrones :: [DroneBotView]
  , rbv_enemyOverseers :: [OverseerBotView]
  , rbv_enemyDrones :: [DroneBotView]
  , rbv_enemyOrphanedDrones :: [DroneBotView]
  , rbv_softObstacles :: [SoftObstacleBotView]
  , rbv_psiStorms :: [PsiStormBotView]
  , rbv_powerups :: [PowerupBotView]
  , rbv_alliesTotal :: Int
  , rbv_enemiesTotal :: Int
  , rbv_alliesAlive :: Int
  , rbv_enemiesAlive :: Int
  , rbv_enemyMatchPoint :: Bool
  , rbv_myRawPosition :: Fixed2
  , rbv_alliedOverseerPositions :: [(ActorID, Fixed2)]
  -- ^ All allied overseers of the bot, including the bot itself
  , rbv_visibleEnemyOverseerPositions :: [Fixed2]
  , rbv_mapPowerups :: MapPowerupStatuses
  , rbv_myPsiStormCooldownTime :: Time
  -- ^ Time before which a psi-storm command should not be queued
  , rbv_myOverseerDashCooldownTime :: Time
  -- ^ Time before which an overseer dash command should not be queued
  , rbv_queueDelayTicks :: Ticks
  }

data RefinedBotDronesView = RefinedBotDronesView
  { rbdv_medianPos :: Fixed2
  , rbdv_restingMedianPos :: Fixed2
  , rbdv_myDrones :: List.NonEmpty DroneBotView
  , rbdv_totalDroneHealth :: Fixed
  }

refineBotView ::
  GameParams ->
  Time ->
  BotID ->
  BotView ->
  [PlayerControls] ->
  Maybe CastedPsiStorm ->
  Maybe CastedOverseerDash ->
  Maybe RefinedBotView
refineBotView params time botID botView delayedControls castedPsiStorm castedOverseerDash =
  List.NonEmpty.nonEmpty myOverseers <&> \(myOverseer List.NonEmpty.:| _) ->
    let botParams = gp_bots params

        (myDrones, otherDrones) =
          List.partition
            (\dr -> Just (BotActor botID) == dbv_actorID dr)
            (bve_drones entities)
        myTeam = obv_team myOverseer
        myClass = obv_class myOverseer
        (alliedDrones, allEnemyDrones) =
          List.partition (\dr -> myTeam == dbv_team dr) otherDrones
        (enemyOrphanedDrones, enemyDrones) =
          List.partition dbv_orphaned allEnemyDrones
        (alliedOverseers, enemyOverseers) =
          List.partition (\ov -> myTeam == obv_team ov) otherOverseers
    in  RefinedBotView
          { rbv_controlTime =
              let latencyTicks =
                    C.botControlFrequencyTicks
                      * fromIntegral (gp_botControlQueueLength botParams)
              in  addTicks latencyTicks time
          , rbv_predictionTime =
              let predictionTicks =
                    max (gp_botMaxPredictionTicks botParams) $
                      C.botControlFrequencyTicks
                        * fromIntegral (gp_botControlQueueLength botParams)
              in  addTicks predictionTicks time
          , rbv_alliedFlag =
              case myTeam of
                TeamW -> bv_westTeamFlag botView
                TeamE -> bv_eastTeamFlag botView
          , rbv_enemyFlag =
              case myTeam of
                TeamW -> bv_eastTeamFlag botView
                TeamE -> bv_westTeamFlag botView
          , rbv_myOverseer =
              stepSelfOverseerKinematics
                params
                (bve_softObstacles entities)
                delayedControls
                myOverseer
          , rbv_myDrones =
              List.NonEmpty.nonEmpty
                (fmap (stepSelfDroneKinematics params delayedControls) myDrones)
                <&> \rbdv_myDrones ->
                  let myDroneParams =
                        getClassParam myClass (gp_droneDragParams botParams)
                      rbdv_medianPos =
                        geometricMedian (fmap dbv_pos rbdv_myDrones)
                      rbdv_restingMedianPos =
                        geometricMedian $
                          fmap (restingPosition myDroneParams) rbdv_myDrones
                      rbdv_totalDroneHealth =
                        foldl' (+) 0 $ fmap dbv_health rbdv_myDrones
                  in  RefinedBotDronesView{..}
          , rbv_alliedOverseers =
              fmap (stepOverseerKinematics params) alliedOverseers
          , rbv_alliedDrones =
              fmap (stepDroneKinematics params) alliedDrones
          , rbv_enemyOverseers =
              fmap (stepOverseerKinematics params) enemyOverseers
          , rbv_enemyDrones =
              fmap (stepDroneKinematics params) enemyDrones
          , rbv_enemyOrphanedDrones =
              fmap (stepDroneKinematics params) enemyOrphanedDrones
          , rbv_softObstacles =
              fmap (stepSoftObstacleKinematics params) (bve_softObstacles entities)
          , rbv_psiStorms = bve_psiStorms entities
          , rbv_powerups = bve_powerups entities
          , rbv_alliesTotal = bv_alliesTotal botView
          , rbv_enemiesTotal = bv_enemiesTotal botView
          , rbv_alliesAlive = bv_alliesAlive botView
          , rbv_enemiesAlive = bv_enemiesAlive botView
          , rbv_enemyMatchPoint =
              bv_enemyScore botView + 1 == gp_flagsToWin params
          , rbv_myRawPosition = obv_pos myOverseer
          , rbv_alliedOverseerPositions = bv_alliedOverseerPositions botView
          , rbv_visibleEnemyOverseerPositions = bv_visibleEnemyOverseerPositions botView
          , rbv_mapPowerups = bv_mapPowerups botView
          , rbv_myPsiStormCooldownTime =
              let intendedCastCooldownTime =
                    case castedPsiStorm of
                      Nothing -> initialTime -- no restriction
                      Just CastedPsiStorm{..} ->
                        let cooldownTicks =
                              -- Only needs to be enough to prevent casting
                              gp_psiStormCastCooldownTicks (gp_cast params)
                        in  addTicks cooldownTicks cps_time
                  observedCastCooldownTime =
                    addTicks
                      (-queueDelayTicks)
                      (bv_myPsiStormCooldownTime botView)
              in  max observedCastCooldownTime intendedCastCooldownTime
          , rbv_myOverseerDashCooldownTime =
              let myDashStamina = bv_myOverseerDashStamina botView
                  lastObservedDashTime = lastDashTime myDashStamina
                  lastIntendedDashTime =
                    case castedOverseerDash of
                      Nothing -> initialTime -- way earlier
                      Just CastedOverseerDash{..} -> cod_time
                  castParams = gp_cast params
              in  if lastObservedDashTime < lastIntendedDashTime
                    then -- We've inputted a dash command but it hasn't happened yet

                      let (fullDashCharges, _) =
                            getCurrentStamina
                              castParams
                              myClass
                              time
                              myDashStamina
                      in  if fullDashCharges > 1
                            then
                              addTicks
                                (getClassParam myClass (gp_overseerDashMinDelayTicks castParams))
                                lastIntendedDashTime
                            else
                              addTicks
                                (getClassParam myClass (gp_overseerDashCooldownTicks castParams))
                                lastIntendedDashTime
                    else
                      addTicks (-queueDelayTicks) $
                        dashReadyTime (gp_cast params) myClass myDashStamina
          , rbv_queueDelayTicks = queueDelayTicks
          }
  where
    entities = bv_entities botView
    (myOverseers, otherOverseers) =
      List.partition
        (\ov -> BotActor botID == obv_actorID ov)
        (bve_overseers entities)
    queueDelayTicks =
      fromIntegral (gp_botControlQueueLength (gp_bots params))
        * C.botControlFrequencyTicks

-- | Predict the future position of a bot's overseer using the bot's queued
-- controls.
--
-- Note: This function assumes that the bot does not try to use its overseer
-- dash when it is on cooldown.
stepSelfOverseerKinematics ::
  GameParams ->
  [SoftObstacleBotView] ->
  [PlayerControls] ->
  OverseerBotView ->
  OverseerBotView
stepSelfOverseerKinematics params softObstacles delayedControls overseer =
  fst $
    foldl'
      ( \(overseer', softObstacles') (stepTicks, delayedControl) ->
          let ambientAccel =
                softObstacleAccel
                  (gp_dynamics params)
                  softObstacles'
                  overseer
              softObstacles'' =
                fmap
                  (stepSoftObstacleKinematics' (fromIntegral stepTicks))
                  softObstacles'
              overseerWithDash =
                let buttons = control_buttons delayedControl
                in  case view control_overseerDash buttons of
                      OverseerDashOff -> overseer'
                      OverseerDashOn ->
                        let dir = arrowKeyDirection' (view control_move buttons)
                            magnitude =
                              getClassParam
                                (obv_class overseer)
                                (gp_overseerDashDeltaV (gp_cast params))
                            deltaV = Fixed.map (* magnitude) dir
                        in  overseer'{obv_vel = obv_vel overseer' + deltaV}
              overseer'' =
                forwardSimulateKinematicsWithAccel
                  (getClassParam (obv_class overseer) (gp_overseerDragParams (gp_bots params)))
                  ambientAccel
                  (arrowKeyDirection delayedControl)
                  (fromIntegral stepTicks)
                  overseerWithDash
          in  (overseer'', softObstacles'')
      )
      (overseer, softObstacles)
      (takePredictionControls params delayedControls)

softObstacleAccel ::
  DynamicsParams -> [SoftObstacleBotView] -> OverseerBotView -> Fixed2
softObstacleAccel params softObstacles overseer =
  let ovPos = obv_pos overseer
      obstaclePositions = map sobv_pos softObstacles
      ovRadius = getClassParam (obv_class overseer) (gp_overseerRadius params)
      obstacleRadius = gp_softObstacleRadius params
  in  foldl' (+) 0 $
        flip map obstaclePositions $ \obstaclePos ->
          let (dist, dir) = Fixed.normAndNormalize (ovPos - obstaclePos)
              magnitude =
                softObstacleReplusion params obstacleRadius ovRadius dist
          in  Fixed.map (* magnitude) dir

-- | Predict the future position of a bot's drones using the bot's queued
-- controls.
--
-- This function is only a rough approximation because it uses a fixed
-- acceleration direction for each control interval.
stepSelfDroneKinematics ::
  GameParams -> [PlayerControls] -> DroneBotView -> DroneBotView
stepSelfDroneKinematics params delayedControls drone =
  foldl'
    ( \drone' (stepTicks, delayedControl) ->
        let accelSign =
              case view control_drone (control_buttons delayedControl) of
                DroneNeutral -> 0
                DroneAttraction -> 1
                DroneRepulsion -> 1
            accelDir =
              Fixed.map (* accelSign) $
                Fixed.normalize $
                  -- To be more accurate, we would need to change the
                  -- acceleration direction as the drone position changes
                  control_worldPos delayedControl - dbv_pos drone
        in  forwardSimulateKinematics
              (getClassParam (dbv_class drone) (gp_droneDragParams (gp_bots params)))
              accelDir
              (fromIntegral stepTicks)
              drone'
    )
    drone
    (takePredictionControls params delayedControls)

-- | Take only as many queued controls as allowed by the prediction limit
takePredictionControls :: GameParams -> [a] -> [(Ticks, a)]
takePredictionControls params = go totalTicks
  where
    totalTicks = gp_botMaxPredictionTicks (gp_bots params)
    stepTicks = C.botControlFrequencyTicks
    go ticksRemaining (x : xs)
      | ticksRemaining > stepTicks =
          (stepTicks, x) : go (ticksRemaining - stepTicks) xs
      | otherwise = [(ticksRemaining, x)]
    go _ [] = []

-- | Predict the future position of an overseer using dead reckoning
stepOverseerKinematics :: GameParams -> OverseerBotView -> OverseerBotView
stepOverseerKinematics params overseer =
  let overseerParams =
        getClassParam
          (obv_class overseer)
          (gp_overseerDragParams (gp_bots params))
      accelDir = obv_accelDir overseer
      duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  forwardSimulateKinematics
        overseerParams
        accelDir
        duration
        overseer

-- | Predict the future position of a drone using dead reckoning
stepDroneKinematics :: GameParams -> DroneBotView -> DroneBotView
stepDroneKinematics params drone =
  let droneParams = getClassParam (dbv_class drone) (gp_droneDragParams (gp_bots params))
      accelDir = dbv_accelDir drone
      duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  forwardSimulateKinematics
        droneParams
        accelDir
        duration
        drone

-- | Predict the future position of a soft obstacle using dead reckoning
stepSoftObstacleKinematics ::
  GameParams -> SoftObstacleBotView -> SoftObstacleBotView
stepSoftObstacleKinematics params softObstacle =
  let duration = fromIntegral $ gp_botMaxPredictionTicks (gp_bots params)
  in  stepSoftObstacleKinematics' duration softObstacle

-- | Predict the future position of a soft obstacle using dead reckoning
stepSoftObstacleKinematics' ::
  Fixed -> SoftObstacleBotView -> SoftObstacleBotView
stepSoftObstacleKinematics' duration softObstacle =
  softObstacle
    { sobv_pos =
        sobv_pos softObstacle + Fixed.map (* duration) (sobv_vel softObstacle)
    }
