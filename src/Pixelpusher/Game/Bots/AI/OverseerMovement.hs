{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Eta reduce" #-}

module Pixelpusher.Game.Bots.AI.OverseerMovement (
  OverseerMovementResult (..),
  overseerMovement,
) where

import Control.Arrow ((&&&))
import Data.Foldable (find, foldl', toList)
import Data.Maybe (fromMaybe, mapMaybe)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2, FixedSq)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.Bots.AI.OverseerMoveCandidates
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.Parameters.Cast
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time

import Pixelpusher.Game.Bots.AI.GeometryUtil (isWithinDistanceOfLine, lineDistance)
import Pixelpusher.Game.Bots.AI.Kinematics
import Pixelpusher.Game.Bots.AI.MoveTarget
import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.Threats (overseerRestingPosition)
import Pixelpusher.Game.Bots.AI.ViewSummary

--------------------------------------------------------------------------------

data OverseerMovementResult = OverseerMovementResult
  { omr_moveCmd :: MoveCommand
  , omr_dashCmd :: OverseerDashCommand
  , omr_pos :: Fixed2
  , omr_totalPotential :: Fixed
  , omr_debugMovementPotentials :: [(Fixed2, Fixed)]
  }

overseerMovement ::
  GameParams ->
  Time ->
  RefinedBotView ->
  ViewSummary ->
  MoveTarget ->
  ( WIM.IntMap MoveCommand OverseerMovementResult
  , WIM.IntMap MoveCommand OverseerMovementResult
  , [(Fixed2, Fixed)]
  )
overseerMovement params time botView@RefinedBotView{..} viewSummary moveTarget =
  let botParams = gp_bots params

      myPos = obv_pos rbv_myOverseer
      myClass = obv_class rbv_myOverseer

      precisionTarget =
        if mt_requiresPrecision moveTarget
          then Just $ bmt_pos (mt_broad moveTarget)
          else Nothing

      (normalMoveCandidates, dashMoveCandidates) =
        let obstaclePredicate =
              let enemyProximity =
                    Fixed.sqrtFixedSq $
                      foldl' min Fixed.maxBoundFixedSq $
                        map (\pos -> Fixed.normSq (pos - myPos)) $
                          map obv_pos rbv_enemyOverseers
                            ++ map dbv_pos rbv_enemyDrones
              in  uncurry
                    ( obstructionMask
                        params
                        myClass
                        rbv_softObstacles
                        enemyProximity
                        precisionTarget
                    )
                    . (omc_pos &&& omc_vel)

            movePredicate (omc_moveCmd -> moveCmd) =
              let moveDir = arrowKeyDirection' moveCmd
                  moveTargetDir =
                    Fixed.normalize $ bmt_pos (mt_broad moveTarget) - myPos
              in  case mt_urgency moveTarget of
                    LowUrgency -> True
                    MediumUrgency -> True
                    HighUrgency -> True
                    ReallyHighUrgency ->
                      Fixed.dot moveDir moveTargetDir > 0.2 -- special case: about to score
            myVel = obv_vel rbv_myOverseer
            dashAvailable =
              if rbv_myOverseerDashCooldownTime <= time
                then DashAvailable
                else DashNotAvailable
            filter' =
              WIM.mapMaybe (find ((&&) <$> obstaclePredicate <*> movePredicate))
        in  over both filter' $
              makeOverseerMoveCandidates
                (getClassParam myClass (gp_overseerDragParams botParams))
                (getClassParam myClass (gp_protoOverseerMoveCandidates botParams))
                dashAvailable
                myPos
                myVel

      (longRangeAvoidancePotentials, longRangeAvoidanceDebugPotentials) =
        let longRangeMoveCandidates =
              makeLongRangeOverseerMoveCandidates
                (getClassParam myClass (gp_overseerDragParams botParams))
                (getClassParam myClass (gp_protoLongRangeOverseerMoveCandidates botParams))
                (obv_pos rbv_myOverseer)
                (obv_vel rbv_myOverseer)
            results =
              flip WIM.map longRangeMoveCandidates $ \lrmc ->
                let pos = lromc_pos lrmc
                    potential =
                      longRangeAvoidancePotential botParams botView pos
                        * gp_botLongRangeMovementPotentialScaling botParams
                in  (pos, potential)
        in  (WIM.map snd results, WIM.elems results)

      totalPotential moveCmd dir pos =
        let objectiveScaling = vs_objectivePotentialScaling viewSummary
            avoidanceScaling = vs_avoidancePotentialScaling viewSummary
            precisionScaling =
              case precisionTarget of
                Nothing -> 1
                Just targetPos ->
                  Fixed.linearStep 120 0 (Fixed.norm (targetPos - pos))

            movementPotential' =
              movementPotential params botView moveTarget pos
                * objectiveScaling
            (collisionAvoidanceScaling, rawCollisionAvoidancePotential', overseerHeadbuttPotential) =
              collisionAvoidancePotential params time botView viewSummary dir pos
            collisionAvoidancePotential' =
              collisionAvoidanceScaling * rawCollisionAvoidancePotential'
            overseerHeadbuttPotential' =
              overseerHeadbuttPotential * min 1 objectiveScaling
            (psiStormAvoidancePotential', psiStormObjectiveScaling) =
              psiStormAvoidancePotential params rbv_myOverseer rbv_psiStorms pos
            alliedOverseerAvoidancePotential' =
              alliedOverseerAvoidancePotential botView pos
                * min objectiveScaling avoidanceScaling
                * psiStormObjectiveScaling
                * precisionScaling
            shieldPowerupBonusPotential' =
              shieldPowerupBonusPotential params botView pos
            worldBoundsAvoidancePotential' =
              worldBoundsAvoidancePotential params pos
                * psiStormObjectiveScaling
                * precisionScaling

            longRangeAvoidancePotential' =
              let potential' =
                    fromMaybe 0 $
                      WIM.lookup moveCmd longRangeAvoidancePotentials
              in  0.15 * potential'

            totalPotential' =
              movementPotential'
                + collisionAvoidancePotential'
                + overseerHeadbuttPotential'
                + psiStormAvoidancePotential'
                + alliedOverseerAvoidancePotential'
                + shieldPowerupBonusPotential'
                + worldBoundsAvoidancePotential'
                + longRangeAvoidancePotential'
            testPotential = totalPotential'
        in  (totalPotential', testPotential)

      makeMovementResult ::
        WIM.IntMap MoveCommand OverseerMoveCandidate ->
        WIM.IntMap MoveCommand OverseerMovementResult
      makeMovementResult =
        WIM.map $ \moveCandidate ->
          let (totalPotential', testPotential') =
                totalPotential
                  (omc_moveCmd moveCandidate)
                  (arrowKeyDirection' (omc_moveCmd moveCandidate))
                  (omc_pos moveCandidate)
          in  OverseerMovementResult
                { omr_moveCmd = omc_moveCmd moveCandidate
                , omr_dashCmd = omc_dashCmd moveCandidate
                , omr_pos = omc_pos moveCandidate
                , omr_totalPotential = totalPotential'
                , omr_debugMovementPotentials =
                    [(omc_pos moveCandidate, testPotential')] -- TODO: cleanup
                }
  in  ( makeMovementResult normalMoveCandidates
      , makeMovementResult dashMoveCandidates
      , longRangeAvoidanceDebugPotentials
      )

--------------------------------------------------------------------------------

obstructionMask ::
  GameParams ->
  PlayerClass ->
  [SoftObstacleBotView] ->
  Fixed ->
  Maybe Fixed2 ->
  Fixed2 ->
  Fixed2 ->
  Bool
obstructionMask
  params
  myClass
  softObstacles
  enemyProximity
  precisionTargetMaybe
  testPos
  testVel
    | Just precisionTarget <- precisionTargetMaybe
    , Fixed.normLeq (testPos - precisionTarget) 24 =
        True
    | Fixed.normLeq testPos (gp_worldRadius - collisionDetectionBuffer) =
        -- Away from world boundary
        case softObstaclePositions of
          [] ->
            True
          [obstaclePos] ->
            let (distance, intoObstacle) =
                  Fixed.normAndNormalize $ obstaclePos - testPos
                velIntoObstacle = Fixed.dot intoObstacle testVel
                modifiedRadius =
                  gp_softObstacleRadius + velIntoObstacle * velWeight
            in  distance > modifiedRadius
          [obstaclePos1, obstaclePos2] ->
            let (distance1, intoObstacle1) =
                  Fixed.normAndNormalize $ obstaclePos1 - testPos
                (distance2, intoObstacle2) =
                  Fixed.normAndNormalize $ obstaclePos2 - testPos
                velIntoObstacle1 = Fixed.dot intoObstacle1 testVel
                velIntoObstacle2 = Fixed.dot intoObstacle2 testVel
                obstacleOverlap =
                  2 * gp_softObstacleRadius
                    - Fixed.norm (obstaclePos1 - obstaclePos2)
                modifiedRadius1 =
                  gp_softObstacleRadius
                    - radiusAttenuation obstacleOverlap
                    + velIntoObstacle1 * velWeight
                modifiedRadius2 =
                  gp_softObstacleRadius
                    - radiusAttenuation obstacleOverlap
                    + velIntoObstacle2 * velWeight
            in  distance1 > modifiedRadius1 && distance2 > modifiedRadius2
          _ ->
            False
    | otherwise =
        -- Near world boundary
        case softObstaclePositions of
          [] ->
            let (distanceFromCenter, intoWorldBoundary) =
                  Fixed.normAndNormalize testPos
                distanceFromBoundary = gp_worldRadius - distanceFromCenter
                velIntoWorldBoundary = Fixed.dot intoWorldBoundary testVel
                boundaryBufferRadius =
                  getClassParam myClass gp_overseerRadius
                    + velIntoWorldBoundary * velWeight
            in  distanceFromBoundary > boundaryBufferRadius
          [obstaclePos] ->
            let (distanceObstacle, intoObstacle) =
                  Fixed.normAndNormalize $ obstaclePos - testPos
                velIntoObstacle = Fixed.dot intoObstacle testVel
                virtualOverlap =
                  gp_softObstacleRadius
                    + gp_overseerRadius_base -- world boundary "radius"
                    - (gp_worldRadius - Fixed.norm obstaclePos)
                modifiedObstacleRadius =
                  gp_softObstacleRadius
                    - radiusAttenuation virtualOverlap
                    + velIntoObstacle * velWeight
                (distanceFromCenter, intoWorldBoundary) =
                  Fixed.normAndNormalize testPos
                distanceFromBoundary = gp_worldRadius - distanceFromCenter
                velIntoWorldBoundary = Fixed.dot intoWorldBoundary testVel
                boundaryBufferRadius =
                  getClassParam myClass gp_overseerRadius
                    - 0.5 * radiusAttenuation virtualOverlap
                    + velIntoWorldBoundary * velWeight
            in  distanceObstacle > modifiedObstacleRadius
                  && distanceFromBoundary > boundaryBufferRadius
          _ ->
            False
    where
      DynamicsParams{..} = gp_dynamics params
      terminalVelocity_base =
        gp_overseerAcceleration_base / gp_overseerDrag_base
      velWeight = 1.5 * gp_overseerRadius_base / terminalVelocity_base
      collisionDetectionBuffer = 1.5 * gp_overseerRadius_base

      softObstaclePositions :: [Fixed2]
      softObstaclePositions =
        flip mapMaybe softObstacles $ \SoftObstacleBotView{..} ->
          if Fixed.normLeq
            (sobv_pos - testPos)
            (gp_softObstacleRadius + collisionDetectionBuffer)
            then Just sobv_pos
            else Nothing

      -- Using (0.5*) because it the attenuation is applied to both obstacles.
      -- Using (+ overlap) effectively sets the the overlap to zero, which is a
      -- good reference point for conceptualizing the effective gap between the
      -- obstacles after this radius attenuation.
      radiusAttenuation :: Fixed -> Fixed
      radiusAttenuation overlap
        | overlap < maxOverlap =
            let pathWidth =
                  widePathWidth
                    - (widePathWidth - narrowPathWidth) * overlap / maxOverlap
            in  0.5 * (overlap + pathWidth)
        | otherwise =
            -- Beyond the maximum overlap, _increase_ effective radius to prevent
            -- overseers from getting stuck in the cleft between two obstacles
            (* 0.5) $
              max (-gp_overseerRadius_base) $
                narrowPathWidth + overlap - 16 * (overlap - maxOverlap)
        where
          maxOverlap
            | enemyProximity > farDist = highMaxOverlap
            | enemyProximity > nearDist =
                highMaxOverlap
                  - (highMaxOverlap - lowMaxOverlap)
                    * (enemyProximity - nearDist)
                    / (farDist - nearDist)
            | otherwise = lowMaxOverlap
          lowMaxOverlap = 40
          highMaxOverlap = 60
          farDist = 240
          nearDist = 60
          widePathWidth = 64 -- easy to maneouver through
          narrowPathWidth = 32 -- difficult to maneouver through

--------------------------------------------------------------------------------
-- Overseer movement goal

movementPotential ::
  GameParams -> RefinedBotView -> MoveTarget -> Fixed2 -> Fixed
movementPotential params RefinedBotView{..} MoveTarget{..} pos =
  let BroadMoveTarget{..} = mt_broad
      collisionRadius = myRadius + bmt_radius - 2 -- buffer
      steepness =
        case mt_urgency of
          LowUrgency -> 0.00071 -- arbitrary
          MediumUrgency -> 0.00214 -- arbitrary
          HighUrgency -> 0.00357 -- arbitrary
          ReallyHighUrgency -> 0.00571 -- arbitrary
      gradient = steepness * max 0 (Fixed.norm (bmt_pos - pos) - bmt_radius)

      myPos = obv_pos rbv_myOverseer
      myRadius =
        getClassParam (obv_class rbv_myOverseer) $
          gp_overseerRadius (gp_dynamics params)
      precisionBonus
        | mt_requiresPrecision
            && isWithinDistanceOfLine myPos pos collisionRadius bmt_pos =
            case mt_urgency of
              LowUrgency -> 1 -- arbitrary
              MediumUrgency -> 2 -- arbitrary
              HighUrgency -> 3 -- arbitrary
              ReallyHighUrgency -> 5 -- arbitrary
        | otherwise = 0
  in  gradient - precisionBonus

--------------------------------------------------------------------------------
-- Short-range overseer enemy collision avoidance

collisionAvoidancePotential ::
  GameParams ->
  Time ->
  RefinedBotView ->
  ViewSummary ->
  Fixed2 ->
  Fixed2 ->
  (Fixed, Fixed, Fixed)
collisionAvoidancePotential params time botView@RefinedBotView{..} viewSummary dir pos =
  let myClass = obv_class rbv_myOverseer
      (avoidanceRadius, avoidancePotentialScaling) =
        let threshold = 0.67
            healthFrac =
              myHealthAndShields
                / getClassParam myClass (gp_overseerMaxHealth (gp_combat params))
            baseAvoidanceRadius = 140
            avoidanceRadius' =
              if healthFrac > threshold
                then baseAvoidanceRadius
                else baseAvoidanceRadius + 220 * ((threshold - healthFrac) / threshold)
            scaling = avoidanceRadius' / baseAvoidanceRadius
        in  (avoidanceRadius', scaling)
      (healthScore, score) =
        foldl'
          (\(!accX, accY) (x, y) -> (accX + x, accY + y))
          (0, 0)
          [ (,0) $ droneAvoidancePotential params avoidanceRadius rbv_enemyDrones pos
          , (,0) $ droneAvoidancePotential params (0.25 * avoidanceRadius) rbv_enemyOrphanedDrones pos
          , enemyOverseerPotential params time botView viewSummary avoidanceRadius dir pos
          ]
      myHealthAndShields =
        max 1 $ obv_health rbv_myOverseer + obv_shields rbv_myOverseer

      -- Blunt the avoidance potential when it is above lethal damage to avoid
      -- over-focus on high concentrations of overseers/drones.
      avoidanceThreshold = 1.0
      avoidanceRescaling val
        | val <= avoidanceThreshold = val
        | otherwise =
            let excess = val - avoidanceThreshold
                a = 0.5 -- arbitrary
                excess' = sqrt (1 + 2 * excess / a) * a -- slope of 1 when excess is 0
            in  avoidanceThreshold + excess'
  in  ( avoidancePotentialScaling
      , avoidanceRescaling (healthScore / myHealthAndShields)
      , score
      )

droneAvoidancePotential ::
  GameParams -> Fixed -> [DroneBotView] -> Fixed2 -> Fixed
droneAvoidancePotential params avoidanceRadius enemyDrones myPos =
  let score drone =
        let droneParams =
              getClassParam (dbv_class drone) (gp_droneDragParams (gp_bots params))
            dist = droneDistance params droneParams myPos drone
            frac = max 0 (avoidanceRadius - dist) / avoidanceRadius
            frac2 = frac * frac
        in  dbv_health drone * frac2
  in  foldl' (+) 0 $ map score enemyDrones

droneDistance ::
  GameParams -> LinearDragParams -> Fixed2 -> DroneBotView -> Fixed
droneDistance params droneParams testPos enemyDrone =
  let dronePos = dbv_pos enemyDrone
      droneVel = dbv_vel enemyDrone
      impulseFactor =
        impulseDistanceFactor droneParams collisionAvoidancePredictionTicks
      dronePos' =
        dronePos + Fixed.map (* collisionAvoidancePredictionTicks) droneVel
      castParams = gp_cast params
      droneChargeTicks =
        min (dbv_chargeTicks enemyDrone) (gp_maxDroneDashChargeTicks castParams)
      deltaV = droneDashDeltaV castParams droneChargeTicks
      chargeRadius =
        let w = 0.85 -- arbitrary in [0, 1]
        in  w * impulseFactor * deltaV
  in  max 0 $ lineDistance dronePos dronePos' testPos - chargeRadius

enemyOverseerPotential ::
  GameParams ->
  Time ->
  RefinedBotView ->
  ViewSummary ->
  Fixed ->
  Fixed2 ->
  Fixed2 ->
  (Fixed, Fixed)
enemyOverseerPotential params time RefinedBotView{..} ViewSummary{..} avoidanceRadius dir pos =
  let dynParams = gp_dynamics params

      overseerMaxHealth = gp_overseerMaxHealth_base (gp_combat params)
      myHealthAndShields = obv_totalOverseerHealth rbv_myOverseer
      myRadius =
        getClassParam (obv_class rbv_myOverseer) (gp_overseerRadius dynParams)

      potential :: OverseerBotView -> (Fixed, Fixed) -- (health-scaled potential, potential)
      potential overseer =
        let healthDiffScaling =
              let enemyRadius =
                    getClassParam
                      (obv_class overseer)
                      (gp_overseerRadius dynParams)
                  combinedRadius = enemyRadius + myRadius
                  dist = Fixed.norm (pos - obv_pos overseer) - combinedRadius
                  range = 120 - combinedRadius
                  baseFrac = 0.25
              in  baseFrac + (1 - baseFrac) * max 0 (min 1 (dist / range))

            blockingEnemyHealth =
              -- TODO: entities don't need to be threatening us for them to get
              -- in the way of a headbutt or pursuit
              let blockingHealth health velocity =
                    let -- Assuming the entities are threatening, let's assume
                        -- that the they are headed towards us
                        proj = Fixed.dot dir (Fixed.normalize velocity) -- Reminder: `dir` could be zero
                        baseFrac = 0.25
                        frac = baseFrac + (1 - baseFrac) * (0.5 * (1 - proj))
                    in  health * frac
                  blockingDroneHealth =
                    foldl' (+) 0 $
                      flip map (ai_threateningDrones vs_myInfo) $ \dbv ->
                        blockingHealth (dbv_health dbv) (dbv_vel dbv)
                  blockingOverseerHealth =
                    foldl' (+) 0 $
                      flip map (ai_threateningOverseers vs_myInfo) $ \obv ->
                        if obv_actorID obv == obv_actorID overseer
                          then 0
                          else blockingHealth (obv_totalOverseerHealth obv) (obv_vel obv)
              in  blockingDroneHealth + blockingOverseerHealth
            avoidanceHealthThreshold =
              myHealthAndShields
                - blockingEnemyHealth * healthDiffScaling
                - 0.05 * overseerMaxHealth
            neutralHealthThreshold =
              avoidanceHealthThreshold - 0.1 * healthDiffScaling * overseerMaxHealth
            attackHealthThreshold =
              avoidanceHealthThreshold - 0.2 * healthDiffScaling * overseerMaxHealth
            totalHealth = obv_health overseer + obv_shields overseer
            distScaling radius dist =
              let frac = max 0 (radius - dist) / radius
              in  frac * frac
            allowOverseerPursuit = gp_botAllowOverseerPursuit (gp_bots params)
        in  if
              | totalHealth > avoidanceHealthThreshold ->
                  let distLine =
                        overseerDistanceWithDash params time pos overseer
                      healthScore =
                        totalHealth * distScaling avoidanceRadius distLine
                  in  (healthScore, 0)
              | totalHealth > neutralHealthThreshold ->
                  let distLine =
                        overseerDistanceWithDash params time pos overseer
                      healthScore =
                        totalHealth
                          * ( (totalHealth - neutralHealthThreshold)
                                / (avoidanceHealthThreshold - neutralHealthThreshold)
                            )
                          * distScaling avoidanceRadius distLine
                  in  (healthScore, 0)
              | allowOverseerPursuit ->
                  let fracComplement =
                        max 0 $
                          (totalHealth - attackHealthThreshold)
                            / (neutralHealthThreshold - attackHealthThreshold)
                      frac = 1 - fracComplement
                      projectedOverseerPos =
                        obv_pos overseer
                          + Fixed.map
                            (* collisionAvoidancePredictionTicks)
                            (obv_vel overseer)
                      dist = Fixed.norm (pos - projectedOverseerPos)
                      score = -frac * distScaling 240 dist * 2 -- arbitrary constants
                  in  (0, score)
              | otherwise ->
                  (0, 0)
  in  foldl'
        (\(!accX, accY) (x, y) -> (accX + x, accY + y))
        (0, 0)
        (map potential rbv_enemyOverseers)

-- | Like `overseerDistance`, but adds a distance buffer to account for the
-- possibility of an offensive overseer dash.
overseerDistanceWithDash ::
  GameParams -> Time -> Fixed2 -> OverseerBotView -> Fixed
overseerDistanceWithDash params time testPos enemyOverseer =
  let ovDragParams =
        getClassParam
          (obv_class enemyOverseer)
          (gp_overseerDragParams (gp_bots params))
      overseerDistance' = overseerDistance testPos enemyOverseer
      impulseFactor =
        impulseDistanceFactor ovDragParams collisionAvoidancePredictionTicks
      dashVelocity =
        getClassParam
          (obv_class enemyOverseer)
          (gp_overseerDashDeltaV (gp_cast params))
      dashBuffer =
        let dashTimeBufferTicks = fromIntegral C.tickRate_hz
            ticksUntilDash =
              obv_dashCooldownUntil enemyOverseer `diffTime` time
            frac =
              max 0 $ min 1 $ fromIntegral ticksUntilDash / dashTimeBufferTicks
            w = 0.667 * (1 - frac)
        in  w * impulseFactor * dashVelocity
  in  max 0 $ overseerDistance' - dashBuffer

overseerDistance :: Fixed2 -> OverseerBotView -> Fixed
overseerDistance testPos enemyOverseer =
  let overseerPos = obv_pos enemyOverseer
      overseerVel = obv_vel enemyOverseer
      overseerPos' =
        overseerPos + Fixed.map (* collisionAvoidancePredictionTicks) overseerVel
  in  lineDistance overseerPos overseerPos' testPos

--------------------------------------------------------------------------------
-- Overseer allied overseer avoidance

alliedOverseerAvoidancePotential :: RefinedBotView -> Fixed2 -> Fixed
alliedOverseerAvoidancePotential RefinedBotView{..} pos =
  let dist = alliedOverseerMinDistance rbv_alliedOverseers pos
      shortRange = min 150 dist
      midRange = min 300 dist
      longRange = min 450 dist
      weight =
        let flagFactor = if obv_hasFlag rbv_myOverseer then 0.125 else 1
        in  0.0048 * flagFactor -- arbitrary
  in  -weight * (shortRange + 0.25 * midRange + 6.25e-2 * longRange)

alliedOverseerMinDistance :: [OverseerBotView] -> Fixed2 -> Fixed
alliedOverseerMinDistance alliedOverseers myPos =
  Fixed.sqrtFixedSq $
    foldl' min Fixed.maxBoundFixedSq $
      map (alliedOverseerDistance myPos) alliedOverseers

alliedOverseerDistance :: Fixed2 -> OverseerBotView -> FixedSq
alliedOverseerDistance testPos alliedOverseer =
  let projectedPos =
        obv_pos alliedOverseer
          + Fixed.map (* collisionAvoidancePredictionTicks) (obv_vel alliedOverseer)
  in  Fixed.normSq $ projectedPos - testPos

--------------------------------------------------------------------------------
-- Overseer psi-storm avoidance

psiStormAvoidancePotential ::
  GameParams -> OverseerBotView -> [PsiStormBotView] -> Fixed2 -> (Fixed, Fixed)
psiStormAvoidancePotential params myOverseer psiStorms pos =
  let psiStormRadius = gp_psiStormRadius (gp_dynamics params)
      myRadius =
        getClassParam
          (obv_class myOverseer)
          (gp_overseerRadius (gp_dynamics params))
      buffer = 8 -- to make sure
      psiStormHitDamage = gp_psiStormDamagePerHit (gp_combat params)
      myHealthAndShields =
        max 1 $ obv_health myOverseer + obv_shields myOverseer

      score :: PsiStormBotView -> Fixed
      score PsiStormBotView{..} =
        let distance = Fixed.norm (psbv_pos - pos)
            overlapDistance = psiStormRadius + myRadius + buffer
            hits =
              2 * max 0 ((overlapDistance - distance) / overlapDistance)
                + if distance < overlapDistance then 1.5 else 0
        in  psiStormHitDamage * hits

      totalDamage = foldl' (+) 0 $ map score psiStorms
      damageRatio = totalDamage / myHealthAndShields
      potential = damageRatio
      objectiveScaling = min 1 $ recip $ max 1e-2 $ 8 * damageRatio
  in  (potential, objectiveScaling)

--------------------------------------------------------------------------------
-- Shield powerups

-- | If a move candidate would move the overseer over a shield powerup, give a
-- bonus. The collision detection is only approximate.
shieldPowerupBonusPotential :: GameParams -> RefinedBotView -> Fixed2 -> Fixed
shieldPowerupBonusPotential params RefinedBotView{..} pos =
  let myPos = obv_pos rbv_myOverseer
      myClass = obv_class rbv_myOverseer
      myRadius = getClassParam myClass (gp_overseerRadius (gp_dynamics params))
      overseerMaxHealth =
        getClassParam myClass (gp_overseerMaxHealth (gp_combat params))
      myHealthAndShields =
        max 1 $ obv_health rbv_myOverseer + obv_shields rbv_myOverseer
      emptyShields = max 0 $ overseerMaxHealth - obv_shields rbv_myOverseer

      shieldsGained :: Fixed -> PowerupBotView -> Fixed
      shieldsGained acc PowerupBotView{..} =
        let collisionRadius = pbv_radius + myRadius - 2 -- buffer
        in  if isWithinDistanceOfLine myPos pos collisionRadius pbv_pos
              then acc + pbv_health
              else acc

      totalShieldsGained =
        min emptyShields (foldl' shieldsGained 0 rbv_powerups)
          * overseerMaxHealth
  in  -totalShieldsGained / myHealthAndShields

--------------------------------------------------------------------------------
-- World bounds avoidance

worldBoundsAvoidancePotential :: GameParams -> Fixed2 -> Fixed
worldBoundsAvoidancePotential params pos =
  let worldRadius = gp_worldRadius (gp_dynamics params)
      r = Fixed.norm pos
      width = 240
      x = Fixed.linearStep (worldRadius - width) worldRadius r
  in  0.30 * x * x

--------------------------------------------------------------------------------
-- Long-range avoidance

longRangeAvoidancePotential :: BotParams -> RefinedBotView -> Fixed2 -> Fixed
longRangeAvoidancePotential botParams botView pos =
  let RefinedBotView{..} = botView
      avoidanceRadius = 360

      alliedDronePotential' =
        longDroneAvoidancePotential
          botParams
          avoidanceRadius
          (maybe [] (toList . rbdv_myDrones) rbv_myDrones ++ rbv_alliedDrones)
          pos
      enemyDronePotential' =
        longDroneAvoidancePotential
          botParams
          avoidanceRadius
          rbv_enemyDrones
          pos
      enemyOverseerPotential' =
        longOverseerAvoidancePotential
          botParams
          avoidanceRadius
          rbv_enemyOverseers
          pos
      alliedOverseerPotential' =
        longOverseerAvoidancePotential
          botParams
          avoidanceRadius
          rbv_alliedOverseers
          pos

      myHealthAndShields = max 1 $ obv_totalOverseerHealth rbv_myOverseer
      totalPotential' =
        max 0 $
          enemyDronePotential'
            + enemyOverseerPotential'
            - alliedDronePotential'
            - alliedOverseerPotential'
  in  max 0 $ totalPotential' / myHealthAndShields

longDroneAvoidancePotential ::
  BotParams -> Fixed -> [DroneBotView] -> Fixed2 -> Fixed
longDroneAvoidancePotential botParams avoidanceRadius enemyDrones myPos =
  let score drone =
        let droneParams =
              getClassParam (dbv_class drone) (gp_droneDragParams botParams)
            dist = Fixed.norm (restingPosition droneParams drone - myPos)
            frac = max 0 (avoidanceRadius - dist) / avoidanceRadius
        in  dbv_health drone * Fixed.smootherstep frac
  in  foldl' (+) 0 $ map score enemyDrones

longOverseerAvoidancePotential ::
  BotParams -> Fixed -> [OverseerBotView] -> Fixed2 -> Fixed
longOverseerAvoidancePotential botParams avoidanceRadius overseers myPos =
  let score overseer =
        let overseerParams =
              getClassParam
                (obv_class overseer)
                (gp_overseerDragParams botParams)
            dist =
              Fixed.norm $
                overseerRestingPosition overseerParams overseer - myPos
            frac = max 0 (avoidanceRadius - dist) / avoidanceRadius
        in  obv_health overseer * Fixed.smootherstep frac
  in  foldl' (+) 0 $ map score overseers
