{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.Threats (
  isDroneThreatening,
  isOverseerThreatening,
  overseerRestingPosition,
) where

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.AI.Kinematics (restingPosition)
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.Parameters

--------------------------------------------------------------------------------

isDroneThreatening :: BotParams -> Fixed2 -> DroneBotView -> Bool
isDroneThreatening botParams targetPos DroneBotView{..} =
  let droneParams = getClassParam dbv_class (gp_droneDragParams botParams)
  in  isEntityThreatening droneParams targetPos dbv_pos dbv_vel dbv_accelDir

isOverseerThreatening :: BotParams -> Fixed2 -> OverseerBotView -> Bool
isOverseerThreatening botParams targetPos OverseerBotView{..} =
  let overseerParams = getClassParam obv_class (gp_overseerDragParams botParams)
  in  isEntityThreatening overseerParams targetPos obv_pos obv_vel obv_accelDir

isEntityThreatening ::
  LinearDragParams ->
  Fixed2 ->
  Fixed2 ->
  Fixed2 ->
  Fixed2 ->
  Bool
isEntityThreatening dragParams targetPos pos vel accelDir =
  let threatSeconds = 0.75
      threatTicks = threatSeconds * fromIntegral C.tickRate_hz
      terminalVelocity = ldp_terminalVelocity dragParams
      threatRadius = threatTicks * terminalVelocity
      innerThreatRadius = threatRadius * 0.75

      entityToTarget = targetPos - pos
      (dist, droneToMeUnit) = Fixed.normAndNormalize entityToTarget

      inThreatRadius = Fixed.normLeq entityToTarget threatRadius
      innerThreatFrac = min 1 $ dist / innerThreatRadius

      inDroneVelocityProjection =
        let projThreshold = -1 + 2 * innerThreatFrac * 0.85 -- arbitrary
        in  Fixed.dot droneToMeUnit vel >= terminalVelocity * projThreshold

      inDroneAccelProjection =
        let projThreshold = -1 + 2 * innerThreatFrac * 0.95 -- arbitrary
        in  Fixed.dot droneToMeUnit (Fixed.normalize accelDir)
              >= projThreshold
  in  inThreatRadius && inDroneVelocityProjection && inDroneAccelProjection

overseerRestingPosition :: LinearDragParams -> OverseerBotView -> Fixed2
overseerRestingPosition dragParams overseer =
  if obv_hasFlag overseer
    then obv_pos overseer
    else restingPosition dragParams overseer
