{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI.DroneTarget (
  DroneTarget (..),
  chooseDroneTarget,
) where

import Data.Foldable (foldl', minimumBy)
import Data.Function (on)
import Data.Functor ((<&>))
import Data.List.NonEmpty qualified as List.NonEmpty
import Data.Maybe (fromMaybe, mapMaybe)

import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.ClassParameter
import Pixelpusher.Game.Parameters.Bots

import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.Threats

--------------------------------------------------------------------------------

data DroneTarget
  = DroneTarget_Enemy OverseerBotView
  | DroneTarget_Ally OverseerBotView [OverseerBotView] [DroneBotView]

chooseDroneTarget :: BotParams -> RefinedBotView -> DroneTarget
chooseDroneTarget botParams botView@RefinedBotView{..} =
  fromMaybe (DroneTarget_Ally rbv_myOverseer [] []) $
    rbv_myDrones >>= \myDrones ->
      let dronePos = rbdv_restingMedianPos myDrones
          distSqFromDrones overseer =
            Fixed.normSqI64 $
              let restingPosition' =
                    overseerRestingPosition
                      (getClassParam (obv_class overseer) (gp_overseerDragParams botParams))
                      overseer
              in  restingPosition' - dronePos
          candidateTargets =
            let alliedCandidates =
                  flip mapMaybe (rbv_myOverseer : rbv_alliedOverseers) $ \ov ->
                    alliedOverseerNeedsHelp botParams botView ov
                      <&> \(overseerThreats, droneThreats) ->
                        let defencePenalty = 16
                            !score = distSqFromDrones ov * defencePenalty
                            target = DroneTarget_Ally ov overseerThreats droneThreats
                        in  (target, score)
                enemyCandidates =
                  map
                    (\ov -> (DroneTarget_Enemy ov, distSqFromDrones ov))
                    rbv_enemyOverseers
            in  alliedCandidates ++ enemyCandidates
      in  fst . minimumBy (compare `on` snd)
            <$> List.NonEmpty.nonEmpty candidateTargets

-- | If help is not needed, returns `Nothing`; otherwise, returns the entities
-- threatning the allied overseer.
alliedOverseerNeedsHelp ::
  BotParams ->
  RefinedBotView ->
  OverseerBotView ->
  Maybe ([OverseerBotView], [DroneBotView])
alliedOverseerNeedsHelp botParams RefinedBotView{..} overseer =
  let overseerClass = obv_class overseer

      overseerRestingPos =
        overseerRestingPosition
          (getClassParam overseerClass (gp_overseerDragParams botParams))
          overseer

      threateningDrones =
        filter
          (isDroneThreatening botParams overseerRestingPos)
          rbv_enemyDrones
      threateningDroneHealth = foldl' (+) 0 $ map dbv_health threateningDrones

      threateningOverseers =
        filter
          (isOverseerThreatening botParams overseerRestingPos)
          rbv_enemyOverseers
      threateningOverseerHealth =
        foldl' (+) 0 $ map obv_totalOverseerHealth threateningOverseers

      threateningHealth =
        threateningDroneHealth + threateningOverseerHealth
      currentHealth = obv_totalOverseerHealth overseer
  in  if threateningHealth > currentHealth
        then Just (threateningOverseers, threateningDrones)
        else Nothing
