{-# LANGUAGE ViewPatterns #-}

module Pixelpusher.Game.Bots.AI.OverseerMoveCandidates (
  -- Collision avoidance move candidates
  collisionAvoidancePredictionTicks,
  OverseerMoveCandidate (..),
  OverseerMoveCandidates,
  DashAvailable (..),
  makeProtoOverseerMoveCandidates,
  makeOverseerMoveCandidates,
  -- Long-range avoidance move candidates
  longRangeAvoidancePredictionTicks,
  LongRangeOverseerMoveCandidate (..),
  makeProtoLongRangeOverseerMoveCandidates,
  makeLongRangeOverseerMoveCandidates,
) where

import Data.Bifunctor (bimap)
import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import Data.Strict.Tuple (Pair ((:!:)))
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (Fixed2))
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.LinearDragParams
import Pixelpusher.Game.PlayerControls

import Pixelpusher.Game.Bots.AI.Kinematics

--------------------------------------------------------------------------------
-- Collision avoidance move candidates

collisionAvoidancePredictionTicks :: Fixed
collisionAvoidancePredictionTicks = 45

data OverseerMoveCandidate = OverseerMoveCandidate
  { omc_pos :: Fixed2
  , omc_vel :: Fixed2
  , omc_moveCmd :: MoveCommand
  , omc_dashCmd :: OverseerDashCommand
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

type OverseerMoveCandidates = WIM.IntMap MoveCommand [OverseerMoveCandidate]

makeProtoOverseerMoveCandidates ::
  LinearDragParams ->
  Fixed ->
  (OverseerMoveCandidates, OverseerMoveCandidates)
makeProtoOverseerMoveCandidates dragParams overseerDashDeltaV =
  let posVelE = stepOverseer1 dragParams MoveE (0 :!: 0)
      posVelEE = stepOverseer2 dragParams MoveE posVelE
      posVelENE = stepOverseer2 dragParams MoveNE posVelE
      posVelESE = bimap invertY invertY posVelENE

      nonNeutralMoveCommands =
        [MoveE, MoveNE, MoveN, MoveNW, MoveW, MoveSW, MoveS, MoveSE]

      normalMoveCandidates =
        let offsetsE =
              -- later candidates will only be used if initial candidates are
              -- filtered out
              [posVelEE, posVelENE, posVelESE]
            neutralMoveCandidate =
              OverseerMoveCandidate
                { omc_pos = 0
                , omc_vel = 0
                , omc_moveCmd = MoveNeutral
                , omc_dashCmd = OverseerDashOff
                }
            nonNeutralMoveCandidates =
              flip map nonNeutralMoveCommands $ \moveCmd ->
                let Fixed2 dx dy = arrowKeyDirection' moveCmd
                    px = -dy
                    py = dx -- perpendicular direction
                    rotate (Fixed2 x y) = Fixed2 (dx * x + px * y) (dy * x + py * y)
                    rotatePosVel = bimap rotate rotate
                    moveCandidates =
                      flip map offsetsE $ \(rotatePosVel -> (posOffset :!: velOffset)) ->
                        OverseerMoveCandidate
                          { omc_pos = posOffset
                          , omc_vel = velOffset
                          , omc_moveCmd = moveCmd
                          , omc_dashCmd = OverseerDashOff
                          }
                in  (moveCmd, moveCandidates)
        in  WIM.fromList $
              (MoveNeutral, [neutralMoveCandidate]) : nonNeutralMoveCandidates

      dashMoveCandidates =
        WIM.fromList $
          flip map nonNeutralMoveCommands $ \moveCmd ->
            let Fixed2 dx dy = arrowKeyDirection' moveCmd
                px = -dy
                py = dx -- perpendicular direction
                rotate (Fixed2 x y) = Fixed2 (dx * x + px * y) (dy * x + py * y)
                rotatePosVel = bimap rotate rotate

                posOffset :!: velOffset = rotatePosVel posVelEE

                posDashOffset :!: velDashOffset =
                  rotatePosVel $
                    stepOverseer2 dragParams MoveNeutral $
                      stepOverseer1
                        dragParams
                        MoveNeutral
                        (Fixed2 0 0 :!: Fixed2 overseerDashDeltaV 0)
                candidate =
                  OverseerMoveCandidate
                    { omc_pos = posOffset + posDashOffset
                    , omc_vel = velOffset + velDashOffset
                    , omc_moveCmd = moveCmd
                    , omc_dashCmd = OverseerDashOn
                    }
            in  (moveCmd, [candidate])
  in  (normalMoveCandidates, dashMoveCandidates)

data DashAvailable = DashAvailable | DashNotAvailable

makeOverseerMoveCandidates ::
  LinearDragParams ->
  (OverseerMoveCandidates, OverseerMoveCandidates) ->
  DashAvailable ->
  Fixed2 ->
  Fixed2 ->
  (OverseerMoveCandidates, OverseerMoveCandidates)
makeOverseerMoveCandidates dragParams protoCandidates dashAvailable initialPos initialVel =
  let (protoNormalCandidates, protoDashCandidates) = protoCandidates
      posNeutral :!: velNeutral =
        stepOverseer2 dragParams MoveNeutral $
          stepOverseer1 dragParams MoveNeutral (initialPos :!: initialVel)
      addNeutralPosVel omc =
        omc & field @"omc_pos" +~ posNeutral & field @"omc_vel" +~ velNeutral

      normalMoveCandidates =
        (WIM.map . map) addNeutralPosVel protoNormalCandidates

      dashMoveCandidates =
        case dashAvailable of
          DashNotAvailable ->
            mempty
          DashAvailable ->
            (WIM.map . map) addNeutralPosVel protoDashCandidates
  in  (normalMoveCandidates, dashMoveCandidates)

stepOverseer ::
  Fixed ->
  LinearDragParams ->
  MoveCommand ->
  Pair Fixed2 Fixed2 ->
  Pair Fixed2 Fixed2
stepOverseer time dragParams moveCommand =
  forwardSimulateKinematics dragParams (arrowKeyDirection' moveCommand) time

stepOverseer1 ::
  LinearDragParams -> MoveCommand -> Pair Fixed2 Fixed2 -> Pair Fixed2 Fixed2
stepOverseer1 = stepOverseer 21.5

stepOverseer2 ::
  LinearDragParams -> MoveCommand -> Pair Fixed2 Fixed2 -> Pair Fixed2 Fixed2
stepOverseer2 = stepOverseer 23.5

invertY :: Fixed2 -> Fixed2
invertY (Fixed2 x y) = Fixed2 x (-y)

--------------------------------------------------------------------------------
-- Long-range overseer move candidates

longRangeAvoidancePredictionTicks :: Fixed
longRangeAvoidancePredictionTicks = 90 -- arbitrary

data LongRangeOverseerMoveCandidate = LongRangeOverseerMoveCandidate
  { lromc_pos :: Fixed2
  , lromc_moveCmd :: MoveCommand
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeProtoLongRangeOverseerMoveCandidates ::
  LinearDragParams ->
  WIM.IntMap MoveCommand LongRangeOverseerMoveCandidate
makeProtoLongRangeOverseerMoveCandidates dragParams =
  let posE :!: _velE =
        longStepOverseer dragParams MoveE (0 :!: (0 :: Fixed2)) -- TODO: could just precompute
      nonNeutralMoveCommands =
        [MoveE, MoveNE, MoveN, MoveNW, MoveW, MoveSW, MoveS, MoveSE]

      neutralMoveCandidate =
        LongRangeOverseerMoveCandidate
          { lromc_pos = 0
          , lromc_moveCmd = MoveNeutral
          }

      nonNeutralMoveCandidates =
        flip map nonNeutralMoveCommands $ \moveCmd ->
          let Fixed2 dx dy = arrowKeyDirection' moveCmd
              px = -dy
              py = dx -- perpendicular direction
              rotate (Fixed2 x y) = Fixed2 (dx * x + px * y) (dy * x + py * y)
          in  LongRangeOverseerMoveCandidate
                { lromc_pos = rotate posE
                , lromc_moveCmd = moveCmd
                }
  in  WIM.fromList $
        map (\lrmc -> (lromc_moveCmd lrmc, lrmc)) $
          neutralMoveCandidate : nonNeutralMoveCandidates

makeLongRangeOverseerMoveCandidates ::
  LinearDragParams ->
  WIM.IntMap MoveCommand LongRangeOverseerMoveCandidate ->
  Fixed2 ->
  Fixed2 ->
  WIM.IntMap MoveCommand LongRangeOverseerMoveCandidate
makeLongRangeOverseerMoveCandidates dragParams protoCandidates initialPos initialVel =
  let posNeutral :!: _velNeutral =
        longStepOverseer dragParams MoveNeutral (initialPos :!: initialVel)
      addNeutralPos lromc = lromc & field @"lromc_pos" +~ posNeutral
  in  WIM.map addNeutralPos protoCandidates

longStepOverseer :: LinearDragParams -> MoveCommand -> Pair Fixed2 Fixed2 -> Pair Fixed2 Fixed2
longStepOverseer dragParams moveCommand =
  forwardSimulateKinematics
    dragParams
    (arrowKeyDirection' moveCommand)
    longRangeAvoidancePredictionTicks
