{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Eta reduce" #-}

module Pixelpusher.Game.Bots.AI.Kinematics (
  HasKinematics (..),
  forwardSimulateKinematics,
  forwardSimulateKinematicsWithAccel,
  restingPosition,
  restingDistance,
  impulseDistanceFactor,
) where

import Data.Strict.Tuple (Pair ((:!:)))
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2 (..))
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Game.LinearDragParams

-- TODO: Proper modelling of overseers carrying flags, which have different
-- kinematics

-- Helper class
class HasKinematics a where
  posVel :: Lens' a (Pair Fixed2 Fixed2)

instance HasKinematics (Pair Fixed2 Fixed2) where
  posVel = id
  {-# INLINE posVel #-}

-- | Final position after coming to a rest under no acceleration.
{-# INLINEABLE restingPosition #-}
restingPosition :: (HasKinematics a) => LinearDragParams -> a -> Fixed2
restingPosition dragParams a =
  let pos :!: vel = view posVel a
  in  pos + Fixed.map (* ldp_recipExpRate dragParams) vel

-- | Compute the distanced travelled given an initial velocity after coming to
-- a rest under no acceleration.
{-# INLINEABLE restingDistance #-}
restingDistance :: LinearDragParams -> Fixed -> Fixed
restingDistance dragParams vel = vel * ldp_recipExpRate dragParams

{-# INLINEABLE forwardSimulateKinematics #-}
forwardSimulateKinematics ::
  (HasKinematics a) => LinearDragParams -> Fixed2 -> Fixed -> a -> a
forwardSimulateKinematics dragParams accelDir duration a =
  over posVel (forwardSimulateKinematics' dragParams 0 accelDir duration) a

{-# INLINEABLE forwardSimulateKinematicsWithAccel #-}
forwardSimulateKinematicsWithAccel ::
  (HasKinematics a) => LinearDragParams -> Fixed2 -> Fixed2 -> Fixed -> a -> a
forwardSimulateKinematicsWithAccel dragParams ambientAccel accelDir duration a =
  over
    posVel
    (forwardSimulateKinematics' dragParams ambientAccel accelDir duration)
    a

forwardSimulateKinematics' ::
  LinearDragParams ->
  Fixed2 ->
  Fixed2 ->
  Fixed ->
  Pair Fixed2 Fixed2 ->
  Pair Fixed2 Fixed2
forwardSimulateKinematics'
  LinearDragParams{..}
  ambientAccel
  accelDir
  duration
  (pos_init :!: v_initial) =
    let t = duration
        v0 = v_initial
        vt =
          Fixed.map (* ldp_terminalVelocity) accelDir
            + Fixed.map (/ ldp_tickDrag) ambientAccel
        exp' = Fixed.approxExp (-ldp_expRate * t)
        displacement =
          Fixed.map (* t) vt
            + Fixed.map (\dv -> (dv * ldp_recipExpRate) * (1 - exp')) (v0 - vt)
        velocity = vt + Fixed.map (* exp') (v0 - vt)
    in  pos_init + displacement :!: velocity

impulseDistanceFactor :: LinearDragParams -> Fixed -> Fixed
impulseDistanceFactor dragParams duration =
  let pos' :!: _vel' =
        forwardSimulateKinematics'
          dragParams
          0
          (Fixed2 0 0)
          duration
          (0 :!: Fixed2 1 0)
      Fixed2 distance _ = pos'
  in  distance
