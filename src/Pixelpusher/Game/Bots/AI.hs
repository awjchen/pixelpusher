{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.AI (
  BotState,
  BotDebugState (..),
  getBotDebugState,
  defaultBotState,
  botControl,
  interpolateMouse,
) where

import Control.Applicative ((<|>))
import Control.Monad.ST
import Data.Bits
import Data.Foldable (minimumBy, toList)
import Data.Function (on)
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.List qualified as List
import Data.Maybe (fromMaybe)
import Data.Serialize (Serialize (..))
import Data.Word
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.Bots.View
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.Bots.AI.DroneMovement
import Pixelpusher.Game.Bots.AI.DroneTarget
import Pixelpusher.Game.Bots.AI.DropFlag
import Pixelpusher.Game.Bots.AI.Intentions
import Pixelpusher.Game.Bots.AI.MoveTarget
import Pixelpusher.Game.Bots.AI.OverseerMovement
import Pixelpusher.Game.Bots.AI.Pathfinding
import Pixelpusher.Game.Bots.AI.PsiStorm
import Pixelpusher.Game.Bots.AI.RefinedView
import Pixelpusher.Game.Bots.AI.ViewSummary

--------------------------------------------------------------------------------

botControl ::
  GameParams ->
  MWC.Gen s ->
  Time ->
  BotID ->
  BotView ->
  BotState ->
  [PlayerControls] ->
  ST s (PlayerControls, BotState)
botControl params gen time botID botView botState delayedControls = do
  let botState' = bs_state botState
      refinedViewMaybe =
        refineBotView
          params
          time
          botID
          botView
          delayedControls
          (bs_castedPsiStorm botState')
          (bs_castedOverseerDash botState')
  case refinedViewMaybe of
    Nothing ->
      -- TODO: This branch is currently never executed because bot views are
      -- only generated when the bot's overseers are alive and `refineBotView`
      -- fails exactly when the bot has no overseer
      pure (defaultControls, botState)
    Just refinedBotView -> botControl2 params gen botState' time refinedBotView

data BotState = BotState
  { bs_state :: BotState'
  , bs_debugState :: BotDebugState
  -- ^ Only for debugging, not used in bot logic
  }

-- | Only serialize non-debug state.
--
-- If we were to serialize the debug state, it would prevent us from adding and
-- using new debug outputs to debug an existing game state snapshot. This is
-- because a client compiled with new debug fields would expect to read those
-- fields from the existing snapshot, but wouldn't find them.
instance Serialize BotState where
  put botState = put $ bs_state botState
  get = get <&> \botState' -> BotState botState' emptyDebugState

defaultBotState :: BotState
defaultBotState =
  BotState
    { bs_state = defaultBotState'
    , bs_debugState = emptyDebugState
    }

data BotState' = BotState'
  { bs_droneTargetMode :: DroneTargetMode
  , bs_targetMousePos :: Fixed2
  -- ^ Desired mouse position
  , bs_droneChargeSinceTime :: Maybe Time
  -- ^ Time at which we have been charging drone dash
  , bs_mouseState :: BotMouseState
  , bs_castedPsiStorm :: Maybe CastedPsiStorm
  -- ^ Time at which a psi-storm command was queued
  , bs_castedOverseerDash :: Maybe CastedOverseerDash
  -- ^ Time at which an overseer dash command was queued
  , bs_lastDroneMovePotentials :: IntMap Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

defaultBotState' :: BotState'
defaultBotState' =
  BotState'
    { bs_droneTargetMode = DirectTargeting
    , bs_targetMousePos = 0
    , bs_droneChargeSinceTime = Nothing
    , bs_mouseState = initialBotMouseState
    , bs_castedPsiStorm = Nothing
    , bs_castedOverseerDash = Nothing
    , bs_lastDroneMovePotentials = IntMap.empty
    }

data BotMouseState = BotMouseState
  { bms_mouseAction :: MouseAction
  , bms_psiStormIntentHistory :: Word8
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

initialBotMouseState :: BotMouseState
initialBotMouseState =
  BotMouseState
    { bms_mouseAction = MouseControlDrones initialTime DroneAttraction
    , bms_psiStormIntentHistory = 0
    }

data MouseAction
  = MouseControlDrones Time DroneCommand
  | MouseCastPsiStorm Time Fixed2
  deriving stock (Generic)
  deriving anyclass (Serialize)

getBotDebugState :: BotState -> BotDebugState
getBotDebugState = bs_debugState

data BotDebugState = BotDebugState
  { bs_overseerPathfinding :: Maybe PathfindingResult
  , bs_dronePathfinding :: Maybe PathfindingResult
  , bs_psiStormCursor :: Maybe Fixed2
  , bs_threateningDrones :: [Fixed2]
  , bs_totalAlliedHealth :: Fixed
  , bs_totalEnemyHealth :: Fixed
  , bs_movePotentials :: Maybe [(Fixed2, Fixed)]
  , bs_droneMovePotentials :: Maybe [(Fixed2, Fixed)]
  , bs_longRangeAvoidancePotentials :: Maybe [(Fixed2, Fixed)]
  , bs_chosenMovePos :: Maybe Fixed2
  , bs_chosenDroneMovePos :: Maybe Fixed2
  , bs_moveTargetLabel :: String
  , bs_flagCmd :: FlagCommand
  }

emptyDebugState :: BotDebugState
emptyDebugState =
  BotDebugState
    { bs_overseerPathfinding = Nothing
    , bs_dronePathfinding = Nothing
    , bs_psiStormCursor = Nothing
    , bs_threateningDrones = []
    , bs_totalAlliedHealth = 0
    , bs_totalEnemyHealth = 0
    , bs_movePotentials = Nothing
    , bs_droneMovePotentials = Nothing
    , bs_longRangeAvoidancePotentials = Nothing
    , bs_chosenMovePos = Nothing
    , bs_chosenDroneMovePos = Nothing
    , bs_moveTargetLabel = ""
    , bs_flagCmd = FlagNeutral
    }

botControl2 ::
  GameParams ->
  MWC.Gen s ->
  BotState' ->
  Time ->
  RefinedBotView ->
  ST s (PlayerControls, BotState)
botControl2 params gen botState time botView@RefinedBotView{..} = do
  let viewSummary = makeViewSummary params botView

  -- Mouse position: Drones and psi-storm

  let droneTarget = chooseDroneTarget (gp_bots params) botView
  newDroneTargetMode <-
    MWC.uniformRM @Word32 (0, 9) gen <&> \roll ->
      if roll == 0
        then bs_droneTargetMode botState
        else case bs_droneTargetMode botState of
          DirectTargeting -> LeadTargeting
          LeadTargeting -> DirectTargeting

  (droneMoveResult, newDroneMovePotentials) <- do
    -- TODO: this charge ticks value is too small by one "bot logic step",
    -- since the dash is delayed by one step to get the mouse into the proper
    -- position
    let rawChargeTicks =
          if gp_botAllowDroneDash (gp_bots params)
            then maybe 0 (time `diffTime`) (bs_droneChargeSinceTime botState)
            else 0
        ticksAlive = time `diffTime` obv_aliveSince rbv_myOverseer
        chargeTicks =
          min (gp_maxDroneDashChargeTicks (gp_cast params)) $
            min ticksAlive rawChargeTicks
    droneMovement
      params
      gen
      botView
      newDroneTargetMode
      droneTarget
      chargeTicks
      (bs_lastDroneMovePotentials botState)
  let (droneMousePos, newDroneCmd, chosenDroneMovePos) =
        case view _1 <$> droneMoveResult of
          Nothing -> (obv_pos rbv_myOverseer, DroneAttraction, Nothing)
          Just dmc -> (dmc_mousePos dmc, dmc_droneCmd dmc, Just (dmc_pos dmc))
  let dronePathfinding = view _2 <$> droneMoveResult
      debugDroneMovePotentials = view _3 <$> droneMoveResult

  let psiStormCast = castPsiStorm params time botView viewSummary

  -- TODO: Cleanup this mouse state stuff.
  -- One thing it's trying to do is to not switch from DroneNeutral to
  -- DroneAttract (or DroneRepel) right away, but to wait one "bot logic step"
  -- so that the mouse cursor has time to move to the correct position.
  let newMouseState =
        let psiStormIntended =
              case psiStormCast of
                PSC_DoNotCast -> False
                PSC_DelayCast _ -> True
                PSC_Cast _ -> True
            newPsiStormIntentHistory =
              unsafeShiftL
                (bms_psiStormIntentHistory (bs_mouseState botState))
                1
                .|. if psiStormIntended then 1 else 0
            psiStormTargetMaybe =
              case psiStormCast of
                PSC_DoNotCast -> Nothing
                PSC_DelayCast _ -> Nothing
                PSC_Cast targetPos ->
                  let recentHistory = newPsiStormIntentHistory .&. 7
                  in  if popCount recentHistory >= 2
                        then Just targetPos
                        else Nothing
            newMouseAction =
              case bms_mouseAction (bs_mouseState botState) of
                MouseControlDrones sinceTime oldDroneCmd ->
                  case psiStormTargetMaybe of
                    Nothing ->
                      MouseControlDrones
                        (if newDroneCmd == oldDroneCmd then sinceTime else time)
                        newDroneCmd
                    Just targetPos -> MouseCastPsiStorm time targetPos
                MouseCastPsiStorm sinceTime _ ->
                  case psiStormTargetMaybe of
                    Nothing ->
                      MouseControlDrones time newDroneCmd
                    Just targetPos ->
                      MouseCastPsiStorm sinceTime targetPos
        in  BotMouseState
              { bms_mouseAction = newMouseAction
              , bms_psiStormIntentHistory = newPsiStormIntentHistory
              }

      (finalDroneCmd, psiStormCmd, targetMousePos) =
        case bms_mouseAction newMouseState of
          MouseControlDrones sinceTime droneCmd ->
            let droneCmd' =
                  if time `diffTime` sinceTime >= C.botControlFrequencyTicks
                    then droneCmd
                    else DroneNeutral
            in  (droneCmd', PsiStormOff, droneMousePos)
          MouseCastPsiStorm sinceTime psiStormCursor ->
            let psiStormCmd' =
                  if time `diffTime` sinceTime >= C.botControlFrequencyTicks
                    && time >= rbv_myPsiStormCooldownTime
                    && gp_botAllowPsiStorm (gp_bots params)
                    then PsiStormOn
                    else PsiStormOff
            in  (DroneNeutral, psiStormCmd', psiStormCursor)

  -- The rest

  let !flagCmd = computeFlagCommand params botView viewSummary

  let !finalMoveTarget = computeMoveGoal params time botView flagCmd
      myPos = obv_pos rbv_myOverseer
      debugMoveTargetPos = bmt_pos (mt_broad finalMoveTarget)
      pathfindingObstacles =
        let softObstacles =
              flip map rbv_softObstacles $ \sobv ->
                Obstacle
                  { obs_id = getIndex (sobv_obstacleID sobv)
                  , obs_pos = sobv_pos sobv
                  , obs_radius = gp_softObstacleRadius (gp_dynamics params)
                  , obs_intersectionsTraversable = True
                  }
        in  softObstacles ++ mt_pathfindingObstacles finalMoveTarget
      overseerPathfinding =
        pathfind
          (gp_dynamics params)
          pathfindingObstacles
          myPos
          debugMoveTargetPos
      waypoints = fromMaybe [] (pr_path overseerPathfinding)
      !immediateMoveTarget =
        case List.dropWhile (\pos -> Fixed.normLeq (pos - myPos) 84) waypoints of -- arbitrary radius
          [] -> finalMoveTarget
          firstWaypoint : _ ->
            MoveTarget
              { mt_label = mt_label finalMoveTarget
              , mt_broad =
                  BroadMoveTarget
                    { bmt_pos = firstWaypoint
                    , bmt_radius = 1
                    }
              , mt_requiresPrecision = False
              , mt_urgency = mt_urgency finalMoveTarget
              , mt_pathfindingObstacles = mt_pathfindingObstacles finalMoveTarget
              }

  let ( normalOverseerMovementResults
        , dashOverseerMovementResults
        , longRangeOverseerAvoidanceDebugPotentials
        ) =
          overseerMovement
            params
            time
            botView
            viewSummary
            immediateMoveTarget
      overseerMovementResult =
        let bestMoveResult overseerMovementResults =
              if WIM.null overseerMovementResults
                then Nothing
                else
                  Just $
                    minimumBy
                      (compare `on` omr_totalPotential)
                      overseerMovementResults
            bestNormalResultMaybe =
              bestMoveResult normalOverseerMovementResults
        in  bestNormalResultMaybe >>= \bestNormalResult ->
              let -- Only use dashes that yield a significantly better potential
                  -- than moving normally
                  bestNormalPotential = omr_totalPotential bestNormalResult
                  significantDashes =
                    -- This arbitrary threshold will need to be adjusted as the
                    -- potentials change
                    WIM.filter
                      (\omr -> omr_totalPotential omr - bestNormalPotential < -0.3)
                      dashOverseerMovementResults
                  -- Try to only dash in directions that have good short-range
                  -- potentials, or else we'll try to dash straight into
                  -- enemies. We don't really know what we're doing, though ...
                  safeDirectionsIGuess =
                    -- This arbitrary threshold will need to be adjusted as the
                    -- potentials change
                    WIM.filter
                      (\omr -> omr_totalPotential omr - bestNormalPotential < 0.3)
                      normalOverseerMovementResults
                  chosenDash =
                    bestMoveResult $
                      WIM.intersectionWith
                        const
                        significantDashes
                        safeDirectionsIGuess
              in  chosenDash <|> bestNormalResultMaybe

      !moveCmd = maybe MoveNeutral omr_moveCmd overseerMovementResult
      !dashCmd = maybe OverseerDashOff omr_dashCmd overseerMovementResult
      !chosenMovePos = omr_pos <$> overseerMovementResult
      !debugMovementPotentials =
        concatMap omr_debugMovementPotentials (toList normalOverseerMovementResults)
          ++ concatMap omr_debugMovementPotentials (toList dashOverseerMovementResults)

  let finalDashCmd =
        if gp_botAllowOverseerDash (gp_bots params)
          && dashCmd == OverseerDashOn
          && time >= rbv_myOverseerDashCooldownTime
          then OverseerDashOn
          else OverseerDashOff

  let controls =
        PlayerControls
          { control_buttons =
              defaultButtonStates
                & control_move
                .~ moveCmd
                & control_drone
                .~ finalDroneCmd
                & control_psiStorm
                .~ psiStormCmd
                & control_flag
                .~ flagCmd
                & control_overseerDash
                .~ finalDashCmd
          , control_worldPos = targetMousePos
          }

  let newBotState =
        BotState
          { bs_state =
              BotState'
                { bs_droneTargetMode = newDroneTargetMode
                , bs_targetMousePos = targetMousePos
                , bs_droneChargeSinceTime =
                    case finalDroneCmd of
                      DroneNeutral ->
                        case bs_droneChargeSinceTime botState of
                          Nothing -> Just time
                          Just oldTime -> Just oldTime
                      DroneAttraction -> Nothing
                      DroneRepulsion -> Nothing
                , bs_mouseState = newMouseState
                , bs_castedPsiStorm =
                    case psiStormCmd of
                      PsiStormOff -> bs_castedPsiStorm botState
                      PsiStormOn ->
                        if time < rbv_myPsiStormCooldownTime
                          then bs_castedPsiStorm botState
                          else
                            Just
                              CastedPsiStorm
                                { cps_time = time
                                , cps_pos = targetMousePos
                                }
                , bs_castedOverseerDash =
                    case dashCmd of
                      OverseerDashOff -> bs_castedOverseerDash botState
                      OverseerDashOn ->
                        if time < rbv_myOverseerDashCooldownTime
                          then bs_castedOverseerDash botState
                          else Just CastedOverseerDash{cod_time = time}
                , bs_lastDroneMovePotentials = newDroneMovePotentials
                }
          , bs_debugState =
              BotDebugState
                { bs_overseerPathfinding =
                    if gp_enableDebugData params
                      then Just overseerPathfinding
                      else Nothing
                , bs_dronePathfinding =
                    if gp_enableDebugData params
                      then dronePathfinding
                      else Nothing
                , bs_psiStormCursor =
                    case psiStormCast of
                      PSC_DoNotCast -> Nothing
                      PSC_DelayCast cursorPos -> Just cursorPos
                      PSC_Cast cursorPos -> Just cursorPos
                , bs_threateningDrones =
                    map dbv_pos $ ai_threateningDrones $ vs_myInfo viewSummary
                , bs_totalAlliedHealth = vs_totalAlliedHealth viewSummary
                , bs_totalEnemyHealth = vs_totalEnemyHealth viewSummary
                , bs_movePotentials =
                    if gp_enableDebugData params
                      then Just debugMovementPotentials
                      else Nothing
                , bs_droneMovePotentials =
                    if gp_enableDebugData params
                      then debugDroneMovePotentials
                      else Nothing
                , bs_longRangeAvoidancePotentials =
                    if gp_enableDebugData params
                      then Just longRangeOverseerAvoidanceDebugPotentials
                      else Nothing
                , bs_chosenMovePos = chosenMovePos
                , bs_chosenDroneMovePos = chosenDroneMovePos
                , bs_moveTargetLabel = mt_label finalMoveTarget
                , bs_flagCmd = flagCmd
                }
          }
  pure (controls, newBotState)

--------------------------------------------------------------------------------
-- Mouse movement interpolation
--
-- To make the bot mouse movement more human-like

interpolateMouse :: Fixed2 -> Fixed2 -> Fixed2 -> Fixed2
interpolateMouse oldPos prevTargetPos targetPos =
  let overshootTargetPos =
        let diff = targetPos - prevTargetPos
        in  prevTargetPos + Fixed.map (* distanceOvershootFactor) diff
      (distance, direction) =
        Fixed.normAndNormalize (overshootTargetPos - oldPos)
      moveDistance = distFraction * distance
  in  oldPos + Fixed.map (* moveDistance) direction
  where
    distFraction = 0.15
    distanceOvershootFactor = 1.3
