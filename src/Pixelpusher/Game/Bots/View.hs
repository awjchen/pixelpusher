{-# LANGUAGE RecordWildCards #-}

module Pixelpusher.Game.Bots.View (
  BotView (..),
  BotViewEntities (..),
  OverseerBotView (..),
  obv_totalOverseerHealth,
  DroneBotView (..),
  SoftObstacleBotView (..),
  PsiStormBotView (..),
  FlagBotView (..),
  PowerupBotView (..),
  makeBotViews,
) where

import Control.Monad (foldM)
import Control.Monad.ST (ST)
import Control.Monad.State.Strict (execStateT, modify')
import Control.Monad.Trans.Class (lift)
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.List qualified as List
import Data.Maybe (fromMaybe, isNothing)
import Data.Strict.Tuple (Pair ((:!:)))
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Fixed qualified as Fixed
import Pixelpusher.Custom.Util (expectJust)
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Custom.WrappedIntSet qualified as WIS
import Pixelpusher.Game.ActorID (ActorID (BotActor))
import Pixelpusher.Game.BotID (BotID)
import Pixelpusher.Game.CollisionEvents (BotViewCollisionEvent (..))
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.FlagEvents
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Game.Parameters
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerControlState
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.PlayerStatus
import Pixelpusher.Game.State.Core qualified as Store -- TODO: qualify as Core
import Pixelpusher.Game.State.Store.ActorEntity
import Pixelpusher.Game.State.Store.Combat
import Pixelpusher.Game.State.Store.Dynamics
import Pixelpusher.Game.State.Store.Flag
import Pixelpusher.Game.State.Store.Minion
import Pixelpusher.Game.State.Store.Powerup
import Pixelpusher.Game.State.Store.PsiStorm
import Pixelpusher.Game.State.Store.Team
import Pixelpusher.Game.Team
import Pixelpusher.Game.TeamScore
import Pixelpusher.Game.Time
import Pixelpusher.Store.EntityID

import Pixelpusher.Game.Bots.AI.Kinematics

--------------------------------------------------------------------------------

data BotView = BotView
  { bv_westTeamFlag :: FlagBotView
  , bv_eastTeamFlag :: FlagBotView
  , bv_entities :: BotViewEntities
  , bv_alliesTotal :: Int
  , bv_enemiesTotal :: Int
  , bv_alliesAlive :: Int
  , bv_enemiesAlive :: Int
  , bv_enemyScore :: Int
  , bv_alliedOverseerPositions :: [(ActorID, Fixed2)]
  -- ^ All allied overseers of the bot, including the bot itself
  , bv_visibleEnemyOverseerPositions :: [Fixed2]
  , bv_mapPowerups :: MapPowerupStatuses
  , bv_myPsiStormCooldownTime :: Time
  , bv_myOverseerDashStamina :: DashStamina
  }
  deriving stock (Generic)

data BotViewEntities = BotViewEntities
  { bve_overseers :: [OverseerBotView]
  , bve_drones :: [DroneBotView]
  , bve_softObstacles :: [SoftObstacleBotView]
  , bve_psiStorms :: [PsiStormBotView]
  , bve_powerups :: [PowerupBotView]
  }
  deriving stock (Generic)

emptyBotViewEntities :: BotViewEntities
emptyBotViewEntities =
  BotViewEntities
    { bve_overseers = []
    , bve_drones = []
    , bve_softObstacles = []
    , bve_psiStorms = []
    , bve_powerups = []
    }

data OverseerBotView = OverseerBotView
  { obv_entityID :: Int
  , obv_actorID :: ActorID
  , obv_team :: Team
  , obv_class :: PlayerClass
  , obv_pos :: Fixed2
  , obv_vel :: Fixed2
  , -- Unit vector
    obv_accelDir :: Fixed2
  , obv_hasFlag :: Bool
  , obv_health :: Fixed
  , obv_shields :: Fixed
  , obv_dashCooldownUntil :: Time
  , obv_aliveSince :: Time
  , obv_psiStormCooldownUntil :: Time
  }
  deriving stock (Generic)

instance HasKinematics OverseerBotView where
  posVel f obv =
    f (obv_pos obv :!: obv_vel obv) <&> \(pos :!: vel) ->
      obv{obv_pos = pos, obv_vel = vel}
  {-# INLINE posVel #-}

obv_totalOverseerHealth :: OverseerBotView -> Fixed
obv_totalOverseerHealth obv = obv_health obv + obv_shields obv

data DroneBotView = DroneBotView
  { dbv_actorID :: Maybe ActorID
  , dbv_orphaned :: Bool
  , dbv_team :: Team
  , dbv_class :: PlayerClass
  , dbv_pos :: Fixed2
  , dbv_vel :: Fixed2
  , -- Unit vector
    dbv_accelDir :: Fixed2
  , dbv_chargeTicks :: Ticks
  , dbv_health :: Fixed
  }
  deriving stock (Generic)

instance HasKinematics DroneBotView where
  posVel f dbv =
    f (dbv_pos dbv :!: dbv_vel dbv) <&> \(pos :!: vel) ->
      dbv{dbv_pos = pos, dbv_vel = vel}
  {-# INLINE posVel #-}

data SoftObstacleBotView = SoftObstacleBotView
  { sobv_obstacleID :: EntityID SoftObstacle
  , sobv_pos :: Fixed2
  , sobv_vel :: Fixed2
  }
  deriving stock (Generic)

data PsiStormBotView = PsiStormBotView
  { psbv_id :: EntityID PsiStorm
  , psbv_pos :: Fixed2
  , psbv_castTime :: Time
  }
  deriving stock (Generic)

data FlagBotView = FlagBotView
  { fbv_pos :: Fixed2
  , fbv_type :: FlagType
  }

data PowerupBotView = PowerupBotView
  { pbv_pos :: Fixed2
  , pbv_health :: Fixed
  , pbv_radius :: Fixed
  }

--------------------------------------------------------------------------------

makeBotViews ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MinionStore s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  FlagReadStore s ->
  PowerupReadStore s ->
  PsiStormStore s ->
  GameParams ->
  Time ->
  [(Team, Fixed2, FlagType)] ->
  MapPowerupStatuses ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  TeamScore ->
  [BotViewCollisionEvent] ->
  ST s (WIM.IntMap BotID BotView)
makeBotViews
  dynComps
  combatComps
  minionComps
  teamComps
  actorComps
  flagComps
  powerupComps
  psiStormComps
  params
  time
  flags
  mapPowerups
  playerControlsStatuses
  teamScore
  botViewCollisionEvents = do
    overseerPositions' <-
      overseerPositions dynComps (WIM.map snd playerControlsStatuses)
    WIM.traverseWithKey
      ( makeBotView
          dynComps
          combatComps
          minionComps
          teamComps
          actorComps
          flagComps
          powerupComps
          psiStormComps
          params
          time
          flags
          mapPowerups
          overseerPositions'
          playerControlsStatuses
          teamScore
      )
      (groupBotViewEvents botViewCollisionEvents)

groupBotViewEvents ::
  [BotViewCollisionEvent] ->
  WIM.IntMap BotID (WIS.IntSet (SEntityID DynamicsEntity))
groupBotViewEvents =
  WIM.fromListWith WIS.union
    . map
      ( \(BotViewCollisionEvent botID dynID) ->
          (botID, WIS.singleton dynID)
      )

-- TODO: Just pass in the entire entity store
makeBotView ::
  DynamicsReadStore s ->
  CombatReadStore s ->
  MinionStore s ->
  Store.ReadOnly (TeamStore s) ->
  Store.ReadOnly (ActorEntityStore s) ->
  FlagReadStore s ->
  PowerupReadStore s ->
  PsiStormStore s ->
  GameParams ->
  Time ->
  [(Team, Fixed2, FlagType)] ->
  MapPowerupStatuses ->
  OverseerPositions ->
  WIM.IntMap ActorID (PlayerControls, PlayerStatus) ->
  TeamScore ->
  BotID ->
  WIS.IntSet (SEntityID DynamicsEntity) ->
  ST s BotView
makeBotView
  dynComps
  combatComps
  minionComps
  teamComps
  actorComps
  flagComps
  powerupComps
  psiStormComps
  params
  time
  flags
  mapPowerups
  overseerPositions'
  playerControlsStatuses
  teamScore
  botID
  entities = do
    bv_entities <-
      flip execStateT emptyBotViewEntities $
        for_ (WIS.toList entities) $ \dynID ->
          case refineEntityID dynID of
            OverseerID overseerID -> do
              overseerView <- lift $ do
                let obv_entityID = getIndex overseerID
                obv_actorID <- Store.sureLookup actorComps overseerID
                (obv_pos, obv_vel) <- readPosVel dynComps overseerID
                (obv_health, obv_shields) <-
                  getHealthShields combatComps overseerID
                obv_team <- Store.sureLookup teamComps overseerID
                obv_hasFlag <- readHasFlag flagComps overseerID
                pure $
                  let (playerControls, playerStatus) =
                        fromMaybe
                          (error "makeBotView: player status lookup failed")
                          (WIM.lookup obv_actorID playerControlsStatuses)
                      obv_accelDir = arrowKeyDirection playerControls
                      obv_class = ps_playerClass playerStatus
                      obv_dashCooldownUntil =
                        dashReadyTime (gp_cast params) obv_class $
                          pcs_overseerDashStamina $
                            ps_controlState playerStatus
                      obv_psiStormCooldownUntil =
                        pcs_psiStormCooldownUntil (ps_controlState playerStatus)
                      obv_aliveSince =
                        case ps_overseerStatus playerStatus of
                          OverseerAlive _ spawnTime -> spawnTime
                          OverseerDead{} -> error "makeBotView: overseer bot view"
                  in  OverseerBotView{..}
              modify' $ over (field @"bve_overseers") $ (:) overseerView
            DroneID droneID -> do
              droneView <- lift $ do
                dbv_actorID <- Store.lookup actorComps droneID
                (dbv_pos, dbv_vel) <- readPosVel dynComps droneID
                dbv_orphaned <-
                  isNothing <$> lookupMaster minionComps droneID
                dbv_team <- Store.sureLookup teamComps droneID
                dbv_health <- getHealth combatComps droneID
                let (dbv_accelDir, dbv_chargeTicks, dbv_class) =
                      -- TODO: Orphaned drones still have a class, so this
                      -- default is incorrect
                      fromMaybe (0, 0, defaultPlayerClass) $ do
                        actorID <- dbv_actorID
                        (controls, status) <- WIM.lookup actorID playerControlsStatuses
                        let mousePos = control_worldPos controls
                            droneControl =
                              controls ^. to control_buttons . control_drone
                            accelScale =
                              case droneControl of
                                DroneNeutral -> 0
                                DroneAttraction -> 1
                                DroneRepulsion -> -1
                            accelDir =
                              Fixed.map (* accelScale) $
                                Fixed.normalize (mousePos - dbv_pos)
                        let chargeTicks =
                              case ps_overseerStatus status of
                                OverseerDead{} -> 0
                                OverseerAlive _ spawnTime ->
                                  case pcs_lastDroneCommand (ps_controlState status) of
                                    DroneNeutral ->
                                      diffTime time $
                                        max spawnTime $
                                          pcs_lastDroneCommandInitialTime $
                                            ps_controlState status
                                    _ -> 0
                        pure (accelDir, chargeTicks, ps_playerClass status)
                pure DroneBotView{..}
              modify' $ over (field @"bve_drones") $ (:) droneView
            SoftObstacleID softObstacleID -> do
              softObstacleView <- lift $ do
                let sobv_obstacleID = softObstacleID
                (sobv_pos, sobv_vel) <- readPosVel dynComps softObstacleID
                pure SoftObstacleBotView{..}
              modify' $ over (field @"bve_softObstacles") $ (:) softObstacleView
            PsiStormID psiStormID -> do
              psiStormView <- lift $ do
                let psbv_id = psiStormID
                psbv_pos <- readPos dynComps psiStormID
                psbv_castTime <- Store.sureLookup psiStormComps psiStormID
                pure PsiStormBotView{..}
              modify' $ over (field @"bve_psiStorms") $ (:) psiStormView
            PowerupID powerupID -> do
              powerupView <- lift $ do
                (pbv_pos, pbv_radius) <- readPosRadius dynComps powerupID
                pbv_health <- readPowerupHealth powerupComps powerupID
                pure PowerupBotView{..}
              modify' $ over (field @"bve_powerups") $ (:) powerupView
            _ -> pure ()
    let (_, playerStatus) =
          expectJust "makeBotView: could not find BotID" $
            WIM.lookup (BotActor botID) playerControlsStatuses
        ( bv_alliesTotal
          , bv_enemiesTotal
          , bv_alliesAlive
          , bv_enemiesAlive
          , bv_alliedOverseerPositions
          , bv_visibleEnemyOverseerPositions
          ) =
            let OverseerPositions{..} = overseerPositions'
            in  case ps_team playerStatus of
                  TeamW ->
                    ( op_westPlayers
                    , op_eastPlayers
                    , op_westPlayersAlive
                    , op_eastPlayersAlive
                    , op_westOverseerPositions
                    , op_eastVisibleOverseerPositions
                    )
                  TeamE ->
                    ( op_eastPlayers
                    , op_westPlayers
                    , op_eastPlayersAlive
                    , op_westPlayersAlive
                    , op_eastOverseerPositions
                    , op_westVisibleOverseerPositions
                    )
        (westFlags, eastFlags) =
          List.partition (\(team, _, _) -> team == TeamW) flags
        -- Note:
        -- The bot logic assumes the presence of a flag. However, during the
        -- tutorial, we may introduce bots before flags, in which case there
        -- will be no flag. For this case, we make up arbitrary flag data and
        -- hope thing goes wrong.
        bv_westTeamFlag =
          case westFlags of
            [(TeamW, pos, flagType)] -> FlagBotView pos flagType
            _ -> FlagBotView 0 BaseFlagType -- made-up flag data
        bv_eastTeamFlag =
          case eastFlags of
            [(TeamE, pos, flagType)] -> FlagBotView pos flagType
            _ -> FlagBotView 0 BaseFlagType -- made-up flag data
        bv_mapPowerups = mapPowerups
        bv_myPsiStormCooldownTime =
          pcs_psiStormCooldownUntil (ps_controlState playerStatus)
        bv_myOverseerDashStamina =
          pcs_overseerDashStamina $ ps_controlState playerStatus
        bv_enemyScore =
          case ps_team playerStatus of
            TeamW -> teamScore_e teamScore
            TeamE -> teamScore_w teamScore
    pure BotView{..}

data OverseerPositions = OverseerPositions
  { op_westPlayers :: Int
  , op_eastPlayers :: Int
  , op_westPlayersAlive :: Int
  , op_eastPlayersAlive :: Int
  , op_westOverseerPositions :: [(ActorID, Fixed2)]
  , op_eastOverseerPositions :: [(ActorID, Fixed2)]
  , op_westVisibleOverseerPositions :: [Fixed2]
  , op_eastVisibleOverseerPositions :: [Fixed2]
  }

overseerPositions ::
  DynamicsReadStore s ->
  WIM.IntMap ActorID PlayerStatus ->
  ST s OverseerPositions
overseerPositions dynComps playerStatuses = do
  ( op_westPlayers
    , op_eastPlayers
    , op_westPlayersAlive
    , op_eastPlayersAlive
    , op_westOverseerPositions
    , op_eastOverseerPositions
    ) <- do
    let countOverseer
          ( !westPlayers
            , !eastPlayers
            , !westPlayersAlive
            , !eastPlayersAlive
            , westPositions
            , eastPositions
            )
          (actorID, playerStatus) = do
            case ps_overseerStatus playerStatus of
              OverseerDead{} ->
                pure $
                  case ps_team playerStatus of
                    TeamW ->
                      ( westPlayers + 1
                      , eastPlayers
                      , westPlayersAlive
                      , eastPlayersAlive
                      , westPositions
                      , eastPositions
                      )
                    TeamE ->
                      ( westPlayers
                      , eastPlayers + 1
                      , westPlayersAlive
                      , eastPlayersAlive
                      , westPositions
                      , eastPositions
                      )
              OverseerAlive overseerID _ -> do
                pos <- readPos dynComps overseerID
                pure $
                  case ps_team playerStatus of
                    TeamW ->
                      ( westPlayers + 1
                      , eastPlayers
                      , westPlayersAlive + 1
                      , eastPlayersAlive
                      , (actorID, pos) : westPositions
                      , eastPositions
                      )
                    TeamE ->
                      ( westPlayers
                      , eastPlayers + 1
                      , westPlayersAlive
                      , eastPlayersAlive + 1
                      , westPositions
                      , (actorID, pos) : eastPositions
                      )
    foldM countOverseer (0, 0, 0, 0, [], []) $
      WIM.toList playerStatuses
  let (op_westVisibleOverseerPositions, op_eastVisibleOverseerPositions) =
        inViewOverseers
          id
          (map snd op_westOverseerPositions, map snd op_eastOverseerPositions)
  pure $! OverseerPositions{..}

-- | Helper: Filter each of two sets of overseers for the overseers within view
-- radius of an overseer in the other set. Naive implementation.
inViewOverseers :: (a -> Fixed2) -> ([a], [a]) -> ([a], [a])
inViewOverseers getPos (overseers1, overseers2) =
  let positions1 = map getPos overseers1
      positions2 = map getPos overseers2
      isInViewOf positions overseer =
        any
          (\pos -> Fixed.normLeq (getPos overseer - pos) C.viewRadius)
          positions
  in  ( filter (isInViewOf positions2) overseers1
      , filter (isInViewOf positions1) overseers2
      )
