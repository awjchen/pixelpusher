module Pixelpusher.Game.LinearDragParams (
  LinearDragParams (..),
  makeLinearDragParams,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)

-- | For simulating kinematics in the bot logic
data LinearDragParams = LinearDragParams
  { ldp_terminalVelocity :: Fixed
  , ldp_tickDrag :: Fixed
  , ldp_expRate :: Fixed
  , ldp_recipExpRate :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeLinearDragParams :: Fixed -> Fixed -> LinearDragParams
makeLinearDragParams accel tickDrag =
  let expRate = -log (1 - tickDrag)
  in  LinearDragParams
        { ldp_terminalVelocity = accel / tickDrag
        , ldp_tickDrag = tickDrag
        , ldp_expRate = expRate
        , ldp_recipExpRate = recip expRate
        }
