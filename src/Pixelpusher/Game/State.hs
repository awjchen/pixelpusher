{-# LANGUAGE RecordWildCards #-}

-- | This module encapsulates the entirety of the game state
-- and game logic (including debug state and logic).
module Pixelpusher.Game.State (
  -- * State
  GameState,
  SnapshotJump (..),
  initializeGameState,
  initializeGameStateWithGen,
  integrateGameState,

  -- ** State snapshot
  GameStateSnapshot,
  getSnapshotTime,

  -- * Query
  GameScene (..),
  getGameScene,
  getGameSyncTime,
  getGamePlayers,

  -- * Debug
  getGameSnapshotJumps,
  getGameSnapshotLabels,
  getPauseState,
  getDebugMarks,
) where

import Control.Arrow ((&&&))
import Control.Monad.ST
import Control.Monad.State.Class
import Control.Monad.Trans.Class
import Data.Foldable (for_)
import Data.Generics.Product.Fields
import Data.IntMap.Strict (IntMap)
import Data.IntMap.Strict qualified as IntMap
import Data.Maybe
import Data.STRef
import Data.Serialize (Serialize)
import GHC.Generics (Generic)
import Lens.Micro.Platform.Custom
import System.Random.MWC qualified as MWC

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Mutable (Copyable (..), Mutable (..))
import Pixelpusher.Custom.Util (withSTRef')
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CollisionEvents (ObstacleOverlap)
import Pixelpusher.Game.Debug (
  DebugCommand (..),
  DebugMarks (..),
  DebugSnapshotLabels (..),
  DebugSnapshotSlot (Snapshot1, Snapshot2, Snapshot3),
  PauseState (Paused, Playing, Resuming),
 )
import Pixelpusher.Game.Debug qualified as Debug
import Pixelpusher.Game.FlagEvents (FlagType)
import Pixelpusher.Game.GameCommand (GameCommand)
import Pixelpusher.Game.GameEvents (GameEvents)
import Pixelpusher.Game.GamePhase (GamePhase)
import Pixelpusher.Game.MapPowerups (MapPowerupStatuses)
import Pixelpusher.Game.Parameters (BaseGameParams, GameParams)
import Pixelpusher.Game.Particles (Particles)
import Pixelpusher.Game.Player (Player)
import Pixelpusher.Game.PlayerControls (PlayerControls)
import Pixelpusher.Game.PlayerID (PlayerID)
import Pixelpusher.Game.PlayerStatus (PlayerView)
import Pixelpusher.Game.RenderingComponent (RenderingComponents)
import Pixelpusher.Game.Team (Team)
import Pixelpusher.Game.Time (SyncTime, Time)
import Pixelpusher.Game.Time qualified as Time
import Pixelpusher.Game.Tutorial (TutorialPhase)

-- The next layer of the game state:
import Pixelpusher.Game.State.Outer (
  GameSceneOuter (..),
  GameStateOuter,
  GameStateOuterSnapshot,
 )
import Pixelpusher.Game.State.Outer qualified as Outer

--------------------------------------------------------------------------------
-- Game state types

-- | The entirety of the game state, plus debug state.
--
-- Not deriving a `Generic` instance to preserve the abstraction.
data GameState s = GameState
  { gs_syncTime :: STRef s SyncTime
  , gs_debugState :: STRef s DebugGameState
  , gs_gameState :: GameStateOuter s
  }

data DebugGameState = DebugGameState
  { dgs_pauseState :: PauseState
  , dgs_snapshotJumps :: [SnapshotJump]
  , dgs_debugSnapshot1 :: Maybe LabelledSnapshot
  , dgs_debugSnapshot2 :: Maybe LabelledSnapshot
  , dgs_debugSnapshot3 :: Maybe LabelledSnapshot
  , dgs_mostRecentSnapshotSlot :: Maybe DebugSnapshotSlot
  , dgs_snapshotCreationCounter :: Int
  , dgs_snapshotLoadCounter :: IntMap Int
  , dgs_debugMarks :: DebugMarks
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

initialDebugGameState :: DebugGameState
initialDebugGameState =
  DebugGameState
    { dgs_pauseState = Playing
    , dgs_snapshotJumps = []
    , dgs_debugSnapshot1 = Nothing
    , dgs_debugSnapshot2 = Nothing
    , dgs_debugSnapshot3 = Nothing
    , dgs_mostRecentSnapshotSlot = Nothing
    , dgs_snapshotCreationCounter = 0
    , dgs_snapshotLoadCounter = mempty
    , dgs_debugMarks = Debug.emptyDebugMarks
    }

debugSnapshotLens ::
  DebugSnapshotSlot -> Lens' DebugGameState (Maybe LabelledSnapshot)
debugSnapshotLens = \case
  Snapshot1 -> field @"dgs_debugSnapshot1"
  Snapshot2 -> field @"dgs_debugSnapshot2"
  Snapshot3 -> field @"dgs_debugSnapshot3"

data LabelledSnapshot = LabelledSnapshot
  { ls_snapshotId :: Int
  , ls_loadNo :: Int
  , ls_syncTime :: SyncTime
  , ls_snapshotJumps :: [SnapshotJump]
  , ls_snapshot :: GameStateOuterSnapshot
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | A jump indicates the exclusion of the frames `[from, to)`
data SnapshotJump = SnapshotJump
  { sj_from :: SyncTime
  , sj_to :: SyncTime
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------
-- Game state initialization

-- | Initialize the game state, seeding the PRNG with the system's PRNG.
initializeGameState :: BaseGameParams -> IO (GameState RealWorld)
initializeGameState params =
  MWC.createSystemRandom >>= stToIO . initializeGameStateWithGen params

-- | Initialize the game state with given a PRNG.
initializeGameStateWithGen :: BaseGameParams -> MWC.Gen s -> ST s (GameState s)
initializeGameStateWithGen baseParams gen = do
  gameState' <- Outer.initializeGameStateOuter baseParams gen
  syncTimeRef <- newSTRef Time.initialSyncTime
  debugStateRef <- newSTRef initialDebugGameState
  pure $!
    GameState
      { gs_syncTime = syncTimeRef
      , gs_debugState = debugStateRef
      , gs_gameState = gameState'
      }

--------------------------------------------------------------------------------
-- Game state accessors

-- | A projection of 'GameState' for presenting to players
data GameScene = GameScene
  { scene_gameParams :: GameParams
  , scene_time :: Time
  , scene_roundStart :: Time
  , scene_renderingComps :: RenderingComponents
  , scene_gamePhase :: GamePhase
  , scene_particles :: Particles
  , scene_players :: WIM.IntMap ActorID (Player, PlayerView)
  , scene_flags :: [(Team, Fixed2, FlagType)]
  , scene_tutorialPhase :: TutorialPhase
  , scene_obstacleOverlaps :: [ObstacleOverlap]
  , scene_mapPowerups :: MapPowerupStatuses
  , scene_syncTime :: SyncTime
  , scene_debugSnapshotLabels :: DebugSnapshotLabels
  , scene_pauseState :: PauseState
  , scene_debugMarks :: DebugMarks
  }

getGameScene :: GameState s -> Maybe ActorID -> ST s GameScene
getGameScene gameState mActorID = do
  GameSceneOuter{..} <-
    Outer.getGameSceneOuter (gs_gameState gameState) mActorID
  scene_syncTime <- getGameSyncTime gameState
  scene_debugSnapshotLabels <- getGameSnapshotLabels gameState
  scene_pauseState <- getPauseState gameState
  scene_debugMarks <- getDebugMarks gameState
  pure $! GameScene{..}

getGameSyncTime :: GameState s -> ST s SyncTime
getGameSyncTime = readSTRef . gs_syncTime

getGameSnapshotJumps :: GameState s -> ST s [SnapshotJump]
getGameSnapshotJumps = fmap dgs_snapshotJumps . readSTRef . gs_debugState

getGameSnapshotLabels :: GameState s -> ST s DebugSnapshotLabels
getGameSnapshotLabels gameState =
  readSTRef (gs_debugState gameState) <&> \debugState ->
    let dsl_current = do
          snapshotSlot <- dgs_mostRecentSnapshotSlot debugState
          (ls_snapshotId &&& ls_loadNo)
            <$> view (debugSnapshotLens snapshotSlot) debugState
        dsl_slot1 = ls_snapshotId <$> dgs_debugSnapshot1 debugState
        dsl_slot2 = ls_snapshotId <$> dgs_debugSnapshot2 debugState
        dsl_slot3 = ls_snapshotId <$> dgs_debugSnapshot3 debugState
    in  DebugSnapshotLabels{..}

getPauseState :: GameState s -> ST s PauseState
getPauseState = fmap dgs_pauseState . readSTRef . gs_debugState

getDebugMarks :: GameState s -> ST s DebugMarks
getDebugMarks = fmap dgs_debugMarks . readSTRef . gs_debugState

getGamePlayers :: GameState s -> ST s (WIM.IntMap ActorID Player)
getGamePlayers = Outer.getPlayers . gs_gameState

--------------------------------------------------------------------------------
-- Game state serialization

data GameStateSnapshot = GameStateSnapshot
  { gss_syncTime :: SyncTime
  , gss_debugState :: DebugGameState
  , gss_gameState :: GameStateOuterSnapshot
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance Mutable s GameStateSnapshot (GameState s) where
  freeze gameState =
    GameStateSnapshot
      <$> freeze (gs_syncTime gameState)
      <*> freeze (gs_debugState gameState)
      <*> freeze (gs_gameState gameState)
  thaw snapshot =
    GameState
      <$> thaw (gss_syncTime snapshot)
      <*> thaw (gss_debugState snapshot)
      <*> thaw (gss_gameState snapshot)

getSnapshotTime :: GameStateSnapshot -> SyncTime
getSnapshotTime = gss_syncTime

-- Not derived because we're avoiding a `Generic` instance on `GameState`.
-- Pattern-matching to make it harder to forget to copy a field.
instance Copyable s (GameState s) where
  copy
    (GameState target_syncTime target_debugState target_gameState)
    (GameState source_syncTime source_debugState source_gameState) = do
      copy target_syncTime source_syncTime
      copy target_debugState source_debugState
      copy target_gameState source_gameState

--------------------------------------------------------------------------------
-- Game state integration

-- | Step the game state to the next time step.
integrateGameState ::
  WIM.IntMap PlayerID PlayerControls ->
  [GameCommand] ->
  Maybe DebugCommand ->
  GameState s ->
  ST s GameEvents
integrateGameState newPlayerControls gameCmds debugCommandMaybe GameState{..} = do
  syncTime <-
    modifySTRef' gs_syncTime Time.nextSyncTick >> readSTRef gs_syncTime

  pauseState <-
    withSTRef' gs_debugState $ do
      -- Handle debug command
      for_ debugCommandMaybe $ \case
        DC_SaveSnapshot snapshotSlot -> do
          snapshot <- lift $ freeze gs_gameState
          labelledSnapshot <- do
            let ls_snapshot = snapshot
            ls_snapshotId <- field @"dgs_snapshotCreationCounter" <%= (+ 1)
            let ls_loadNo = 0
            let ls_syncTime = syncTime
            ls_snapshotJumps <- gets dgs_snapshotJumps
            pure LabelledSnapshot{..}
          debugSnapshotLens snapshotSlot .= Just labelledSnapshot
          field @"dgs_mostRecentSnapshotSlot" .= Just snapshotSlot
        DC_LoadSnapshot snapshotSlot -> do
          use (debugSnapshotLens snapshotSlot) >>= \case
            Nothing -> pure ()
            Just snapshot@LabelledSnapshot{..} -> do
              loadNo <-
                field @"dgs_snapshotLoadCounter" %%= \snapshotLoads ->
                  let loadNo =
                        (+ 1) $ fromMaybe 0 $ IntMap.lookup ls_snapshotId snapshotLoads
                      newSnapshotLoads =
                        IntMap.insert ls_snapshotId loadNo snapshotLoads
                  in  (loadNo, newSnapshotLoads)
              field @"dgs_snapshotJumps"
                .= SnapshotJump{sj_from = ls_syncTime, sj_to = syncTime}
                : ls_snapshotJumps
              lift $ do
                savedGameState <- thaw ls_snapshot
                copy gs_gameState savedGameState
              debugSnapshotLens snapshotSlot .= Just snapshot{ls_loadNo = loadNo}
              field @"dgs_mostRecentSnapshotSlot" .= Just snapshotSlot
        DC_Pause ->
          field @"dgs_pauseState" .= Paused
        DC_Unpause ->
          field @"dgs_pauseState" %= \case
            Paused ->
              Resuming (Time.addSyncTicks Debug.resumeDelayTicks syncTime)
            Resuming t -> Resuming t
            Playing -> Playing
        DC_SaveMark markSlot pos ->
          field @"dgs_debugMarks" . Debug.debugMarkSlotLens markSlot .= Just pos
        DC_ClearMark markSlot ->
          field @"dgs_debugMarks" . Debug.debugMarkSlotLens markSlot .= Nothing
        DC_DamageDrones actorID ->
          lift $ Outer.debugDamageDrones gs_gameState actorID
        DC_DamageOverseer actorID ->
          lift $ Outer.debugDamageOverseer gs_gameState actorID
        DC_SwitchTeams actorID ->
          lift $ Outer.debugSwitchTeams gs_gameState actorID

      -- Transition pause state
      field @"dgs_pauseState" <%= \case
        Paused -> Paused
        Resuming resumeTime ->
          if syncTime >= resumeTime then Playing else Resuming resumeTime
        Playing -> Playing

  -- Step game state only when unpaused
  case pauseState of
    Paused -> pure mempty
    Resuming _ -> pure mempty
    Playing -> Outer.stepGameStateOuter gs_gameState newPlayerControls gameCmds
