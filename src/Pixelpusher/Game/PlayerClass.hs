{-# LANGUAGE OverloadedStrings #-}

module Pixelpusher.Game.PlayerClass (
  PlayerClass (..),
  defaultPlayerClass,
  zealotText,
  templarText,
  respawnClassSelectionDeadTimeInitial,
  respawnClassSelectionDeadTimeFinal,
  moveCommandToPlayerClass,
  playerClassToMoveCommand,
  deathTimeToPlayerClass,
) where

import Data.Serialize (Serialize)
import Data.String (IsString)
import GHC.Generics (Generic)
import System.Random.MWC qualified as MWC
import Toml qualified
import Toml.Schema.FromValue qualified as Toml
import Toml.Schema.ToValue qualified as Toml

import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerControls
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

data PlayerClass
  = Zealot
  | Templar
  deriving stock (Generic, Bounded, Enum, Eq)
  deriving anyclass (Serialize, MWC.Uniform)

-- For bad code that needs it
defaultPlayerClass :: PlayerClass
defaultPlayerClass = Zealot

zealotText :: (IsString a) => a
{-# INLINE zealotText #-}
zealotText = "zealot"

templarText :: (IsString a) => a
{-# INLINE templarText #-}
templarText = "templar"

instance Toml.FromValue PlayerClass where
  fromValue = \case
    Toml.Text' _l text
      | text == templarText -> pure Templar
      | text == zealotText -> pure Zealot
    _ ->
      fail $
        "Player class is not one of '"
          <> zealotText
          <> "' or '"
          <> templarText
          <> "'"

instance Toml.ToValue PlayerClass where
  toValue presetServer =
    Toml.Text $
      case presetServer of
        Templar -> templarText
        Zealot -> zealotText

respawnClassSelectionDeadTimeInitial :: Ticks
respawnClassSelectionDeadTimeInitial = Ticks $ 5 * C.tickRate_hz `div` 2

respawnClassSelectionDeadTimeFinal :: Ticks
respawnClassSelectionDeadTimeFinal = Ticks $ 2 * C.tickRate_hz

-- Note: Must be kept in-sync with `playerClassToMoveCommand`
moveCommandToPlayerClass :: MoveCommand -> Maybe PlayerClass
moveCommandToPlayerClass = \case
  MoveW -> Just Zealot
  MoveE -> Just Templar
  _ -> Nothing

-- Note: Must be kept in-sync with `moveCommandToPlayerClass`
playerClassToMoveCommand :: PlayerClass -> MoveCommand
playerClassToMoveCommand = \case
  Zealot -> MoveW
  Templar -> MoveE

-- | Mapping from overseer death times to player classes for
-- pseudo-randomization of bot class selection.
--
-- Defined in this module to help me remember to update it when adding a new
-- class
deathTimeToPlayerClass :: Time -> PlayerClass
deathTimeToPlayerClass time =
  case getTime time `mod` 2 of
    0 -> Zealot
    1 -> Templar
    _ -> error "timeToPlayerClass"
