module Pixelpusher.Game.ClassParameter (
  ClassParameter (..),
  getClassParam,
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Game.PlayerClass

data ClassParameter a = ClassParameter
  { cp_templar :: a
  , cp_zealot :: a
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

getClassParam :: PlayerClass -> ClassParameter a -> a
getClassParam = \case
  Templar -> cp_templar
  Zealot -> cp_zealot
{-# INLINE getClassParam #-}
