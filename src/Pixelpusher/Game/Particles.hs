{-# LANGUAGE AllowAmbiguousTypes #-}

-- | Particles are temporary pieces of data used purely for visual effects and
-- notifications. In particular, particle data should never be used to make
-- changes to the game state.
module Pixelpusher.Game.Particles (
  -- * Particles
  Particle (..),

  -- * Particle types
  IsParticle (..),
  SomeParticle (..),
  Particles (..),
  partitionParticles,
  OverseerDeathParticle (..),
  DroneDeathParticle (..),
  OverseerRemainsParticle (..),
  CollisionDamageParticle (..),
  KillNotificationParticle (..),
  CastRippleParticle (..),
  PsiStormDamageParticle (..),
  LogMsgParticle (..),
  getFlagPickupMessage,
  FlagPickupParticle (..),
  FlagCaptureParticle (..),
  FlagRecoverParticle (..),
  FlagDropParticle (..),
  DashParticle (..),
  FlagExplodeParticle (..),
  FlagFadeParticle (..),
  ScreenshakeParticle (..),
  ShieldPickupParticle (..),
) where

import Data.Functor (($>))
import Data.Serialize (Serialize)
import GHC.Generics (Generic, Generically (..))

import Pixelpusher.Custom.Fixed (Fixed, Fixed2)
import Pixelpusher.Custom.Serialize ()
import Pixelpusher.Custom.WrappedIntMap qualified as WIM
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.CastEvents
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.PlayerClass
import Pixelpusher.Game.PlayerColors
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team
import Pixelpusher.Game.Time

--------------------------------------------------------------------------------

-- | All particles are temporary and have start and end times. These times are
-- not necessarily the same as that of any associated visual effects.
data Particle a = Particle
  { particle_startTime :: Time
  , particle_endTime :: Time
  , particle_type :: a
  }
  deriving stock (Generic, Functor, Foldable, Traversable)

instance (Serialize a) => Serialize (Particle a)

--------------------------------------------------------------------------------
-- Particle types

-- | A class for types representing particles
class IsParticle a where
  toSomeParticle :: a -> SomeParticle

  lifetimeTicks :: Ticks
  -- ^ Duration for which the particle should be kept in the store. Must be at
  -- least as long as the duration of any derived visual effects.

-- | Sum of all particle types
data SomeParticle
  = PT_OverseerDeath OverseerDeathParticle
  | PT_DroneDeath DroneDeathParticle
  | PT_OverseerPersistentDeath OverseerRemainsParticle
  | PT_CollisionDamage CollisionDamageParticle
  | PT_KillNotification KillNotificationParticle
  | PT_CastRipple CastRippleParticle
  | PT_PsiStormDamage PsiStormDamageParticle
  | PT_LogMsg LogMsgParticle
  | PT_Dash DashParticle
  | PT_FlagExplode FlagExplodeParticle
  | PT_FlagFade FlagFadeParticle
  | PT_Screenshake ScreenshakeParticle
  | PT_ShieldPickup ShieldPickupParticle
  deriving stock (Generic)
  deriving anyclass (Serialize)

-- | A collection of all kinds of particles
data Particles = Particles
  { particles_overseerDeath :: [Particle OverseerDeathParticle]
  , particles_droneDeath :: [Particle DroneDeathParticle]
  , particles_overseerRemains :: [Particle OverseerRemainsParticle]
  , particles_collisionDamage :: [Particle CollisionDamageParticle]
  , particles_killNotification :: [Particle KillNotificationParticle]
  , particles_castRipple :: [Particle CastRippleParticle]
  , particles_psiStormDamage :: [Particle PsiStormDamageParticle]
  , particles_logMsg :: [Particle LogMsgParticle]
  , particles_dash :: [Particle DashParticle]
  , particles_flagExplode :: [Particle FlagExplodeParticle]
  , particles_flagFade :: [Particle FlagFadeParticle]
  , particles_screenshake :: [Particle ScreenshakeParticle]
  , particles_shieldPickup :: [Particle ShieldPickupParticle]
  }
  deriving stock (Generic)
  deriving (Semigroup, Monoid) via Generically Particles

partitionParticles :: [Particle SomeParticle] -> Particles
partitionParticles = foldr select mempty

select :: Particle SomeParticle -> Particles -> Particles
select p ps = case particle_type p of
  PT_OverseerDeath x ->
    ps{particles_overseerDeath = (p $> x) : particles_overseerDeath ps}
  PT_DroneDeath x ->
    ps{particles_droneDeath = (p $> x) : particles_droneDeath ps}
  PT_OverseerPersistentDeath x ->
    ps{particles_overseerRemains = (p $> x) : particles_overseerRemains ps}
  PT_CollisionDamage x ->
    ps{particles_collisionDamage = (p $> x) : particles_collisionDamage ps}
  PT_KillNotification x ->
    ps{particles_killNotification = (p $> x) : particles_killNotification ps}
  PT_CastRipple x ->
    ps{particles_castRipple = (p $> x) : particles_castRipple ps}
  PT_PsiStormDamage x ->
    ps{particles_psiStormDamage = (p $> x) : particles_psiStormDamage ps}
  PT_LogMsg x ->
    ps{particles_logMsg = (p $> x) : particles_logMsg ps}
  PT_Dash x ->
    ps{particles_dash = (p $> x) : particles_dash ps}
  PT_FlagExplode x ->
    ps{particles_flagExplode = (p $> x) : particles_flagExplode ps}
  PT_FlagFade x ->
    ps{particles_flagFade = (p $> x) : particles_flagFade ps}
  PT_Screenshake x ->
    ps{particles_screenshake = (p $> x) : particles_screenshake ps}
  PT_ShieldPickup x ->
    ps{particles_shieldPickup = (p $> x) : particles_shieldPickup ps}

--------------------------------------------------------------------------------

data OverseerDeathParticle = OverseerDeathParticle
  { odp_color :: PlayerColor
  , odp_pos :: Fixed2
  , odp_vel :: Fixed2
  , odp_scale :: Fixed
  , odp_entityID :: Int
  , odp_actorID :: ActorID
  , odp_killedTeam :: Team
  , odp_killedClass :: PlayerClass
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle OverseerDeathParticle where
  lifetimeTicks = Ticks 240
  toSomeParticle = PT_OverseerDeath
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data DroneDeathParticle = DroneDeathParticle
  { ddp_eid :: Int
  , ddp_pos :: Fixed2
  , ddp_vel :: Fixed2
  , ddp_scale :: Fixed
  , ddp_team :: Team
  , ddp_class :: PlayerClass
  , ddp_recentDamageAtDeath :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle DroneDeathParticle where
  lifetimeTicks = Ticks 16
  toSomeParticle = PT_DroneDeath

--------------------------------------------------------------------------------

data OverseerRemainsParticle = OverseerRemainsParticle
  { orp_color :: PlayerColor
  , orp_pos :: Fixed2
  , orp_scale :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle OverseerRemainsParticle where
  lifetimeTicks = Ticks $ 8 * C.tickRate_hz
  toSomeParticle = PT_OverseerPersistentDeath

--------------------------------------------------------------------------------

data CollisionDamageParticle = CollisionDamageParticle
  { cdp_pos :: Fixed2
  , cdp_vel :: Fixed2
  , cdp_damage :: Fixed
  , cdp_direction :: Fixed2 -- relative position of collision participants
  , cdp_mPlayerColor1 :: Maybe PlayerColor
  , cdp_mPlayerColor2 :: Maybe PlayerColor
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle CollisionDamageParticle where
  lifetimeTicks = Ticks C.tickRate_hz
  toSomeParticle = PT_CollisionDamage
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data KillNotificationParticle = KillNotificationParticle
  { knp_killedName :: PlayerName
  , knp_killedTeam :: Team
  , knp_mKiller :: Maybe (ActorID, PlayerName)
  , knp_assisters :: WIM.IntMap ActorID ()
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle KillNotificationParticle where
  lifetimeTicks = Ticks $ C.tickRate_hz * 5
  toSomeParticle = PT_KillNotification
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

newtype CastRippleParticle = CastRippleParticle
  { crp_pos :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle CastRippleParticle where
  lifetimeTicks = Ticks $ C.tickRate_hz `div` 2
  toSomeParticle = PT_CastRipple
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data PsiStormDamageParticle = PsiStormDamageParticle
  { psdp_pos :: Fixed2
  , psdp_vel :: Fixed2
  , psdp_mPlayerColor :: Maybe PlayerColor
  , psdp_damage :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle PsiStormDamageParticle where
  lifetimeTicks = Ticks C.tickRate_hz
  toSomeParticle = PT_PsiStormDamage
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data LogMsgParticle
  = LogMsg_FlagPickup FlagPickupParticle
  | LogMsg_FlagCapture FlagCaptureParticle
  | LogMsg_FlagRecover FlagRecoverParticle
  | LogMsg_AnyMsg String
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle LogMsgParticle where
  lifetimeTicks = Ticks $ C.tickRate_hz * 5
  toSomeParticle = PT_LogMsg
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

data FlagPickupParticle = FlagPickupParticle
  { fpp_carrierTeam :: Team
  , fpp_flagPos :: Fixed2
  , fpp_carrierName :: PlayerName
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

getFlagPickupMessage :: LogMsgParticle -> Maybe FlagPickupParticle
getFlagPickupMessage (LogMsg_FlagPickup x) = Just x
getFlagPickupMessage _ = Nothing

data FlagCaptureParticle = FlagCaptureParticle
  { fcp_team :: Team
  , fcp_name :: PlayerName
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data FlagRecoverParticle = FlagRecoverParticle
  { frp_team :: Team
  , frp_name :: Maybe PlayerName
  , frp_pos :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

data FlagDropParticle = FlagDropParticle
  { fdp_pos :: Fixed2
  , fdp_vel :: Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

--------------------------------------------------------------------------------

data DashParticle = DashParticle
  { dp_pos :: Fixed2
  , dp_vel :: Fixed2
  , dp_radius :: Fixed
  , dp_direction :: Fixed2
  , dp_magnitudeFraction :: Fixed
  , dp_dashType :: DashType
  , dp_team :: Team
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle DashParticle where
  lifetimeTicks = Ticks (2 * C.tickRate_hz)
  toSomeParticle = PT_Dash
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data FlagExplodeParticle = FlagExplodeParticle
  { fep_flagTeam :: Team
  , fep_pos :: Fixed2
  , fep_vel :: Fixed2
  , fep_radius :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle FlagExplodeParticle where
  lifetimeTicks = Ticks $ (3 * C.tickRate_hz) `div` 2
  toSomeParticle = PT_FlagExplode
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data FlagFadeParticle
  = FlagFade_Recover FlagRecoverParticle
  | FlagFade_Pickup FlagPickupParticle
  | FlagFade_Drop FlagDropParticle
  | FlagFade_Respawn Team
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle FlagFadeParticle where
  lifetimeTicks = Ticks $ C.tickRate_hz `div` 2
  toSomeParticle = PT_FlagFade
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data ScreenshakeParticle = ScreenshakeParticle
  { sp_damage :: Fixed
  , sp_direction :: Fixed2
  -- ^ The relative position of the collision with respect to the actor's
  -- overseer, modulo sign (i.e. the direction might be flipped)
  , sp_actorID :: ActorID
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle ScreenshakeParticle where
  lifetimeTicks = Ticks $ 2 * C.tickRate_hz
  toSomeParticle = PT_Screenshake
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}

--------------------------------------------------------------------------------

data ShieldPickupParticle = ShieldPickupParticle
  { spp_pos :: Fixed2
  , spp_radius :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

instance IsParticle ShieldPickupParticle where
  lifetimeTicks = Ticks $ C.tickRate_hz `div` 4
  toSomeParticle = PT_ShieldPickup
  {-# INLINE lifetimeTicks #-}
  {-# INLINE toSomeParticle #-}
