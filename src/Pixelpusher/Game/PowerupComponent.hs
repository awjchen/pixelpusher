{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Pixelpusher.Game.PowerupComponent (
  PowerupComponent (..),
  PowerupComponentVec (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics

import Pixelpusher.Custom.Fixed (Fixed)
import Pixelpusher.Game.MapPowerups
import Pixelpusher.Store.VecStore

-- | Shield powerups, in particular
data PowerupComponent = PowerupComponent
  { pc_location :: Maybe MapPowerupLocation
  , pc_consumed :: Bool
  , pc_shieldHealth :: Fixed
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

makeVecStore ''PowerupComponent
