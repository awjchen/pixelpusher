module Pixelpusher.Game.GameCommand (
  GameCommand (..),
) where

import Data.Serialize (Serialize)
import GHC.Generics (Generic)

import Pixelpusher.Game.Parameters (BaseGameParams)
import Pixelpusher.Game.PlayerClass (PlayerClass)
import Pixelpusher.Game.PlayerID (PlayerID)
import Pixelpusher.Game.PlayerName
import Pixelpusher.Game.Team (Team)

--------------------------------------------------------------------------------

data GameCommand
  = PlayerJoin
      PlayerID
      PlayerName
      PlayerClass
      (Maybe Team)
  | PlayerLeave PlayerID
  | -- | Just for playtesting
    SwitchGameParams BaseGameParams
  deriving stock (Generic)
  deriving anyclass (Serialize)
