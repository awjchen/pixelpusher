module Pixelpusher.Game.PlayerControls.Internal (
  ButtonStates (..),
  defaultButtonStates,
) where

import Data.Serialize (Serialize)
import Data.Word (Word8)

newtype ButtonStates = ButtonStates
  { _getButtonStates :: Word8
  }
  deriving newtype (Eq, Show, Serialize)

defaultButtonStates :: ButtonStates
defaultButtonStates = ButtonStates 0
