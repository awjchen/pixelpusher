module Pixelpusher.Game.PlayerID (
  PlayerID,
  PlayerIDPool,
  newPlayerIDPool,
  borrowPlayerID,
  returnPlayerID,

  -- * Debug
  unsafeDebugPlayerID,
) where

import Data.Serialize (Serialize)
import Data.Word (Word8)

import Pixelpusher.Custom.SmallBitSet (SmallBitSet)
import Pixelpusher.Custom.SmallBitSet qualified as SmallBitSet
import Pixelpusher.Game.PlayerID.Internal (PlayerID (..), unsafeDebugPlayerID)

--------------------------------------------------------------------------------

-- | A pool of a finite number of 'PlayerID's.
newtype PlayerIDPool = PlayerIDPool SmallBitSet
  deriving newtype (Serialize)

newPlayerIDPool :: Word8 -> PlayerIDPool
newPlayerIDPool maxUsers = PlayerIDPool $ SmallBitSet.new maxUsers

borrowPlayerID :: PlayerIDPool -> (Maybe PlayerID, PlayerIDPool)
borrowPlayerID (PlayerIDPool bitSet) =
  let (mWord8, bitSet') = SmallBitSet.takeSmallestBit bitSet
  in  (PlayerID <$> mWord8, PlayerIDPool bitSet')

returnPlayerID :: PlayerID -> PlayerIDPool -> PlayerIDPool
returnPlayerID (PlayerID word8) (PlayerIDPool bitSet) =
  PlayerIDPool $ SmallBitSet.returnBit word8 bitSet
