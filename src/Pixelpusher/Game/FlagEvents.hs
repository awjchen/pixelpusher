module Pixelpusher.Game.FlagEvents (
  FlagEvents (..),
  FlagPickup (..),
  FlagCapture (..),
  FlagRecovery (..),
  FlagType (..),
) where

import GHC.Generics (Generic, Generically (..))

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Game.ActorID (ActorID)
import Pixelpusher.Game.Team

data FlagEvents = FlagEvents
  { flagEvents_pickups :: [FlagPickup]
  , flagEvents_captures :: [FlagCapture]
  , flagEvents_recoveries :: [FlagRecovery]
  }
  deriving (Generic)
  deriving (Semigroup, Monoid) via Generically FlagEvents

data FlagPickup = FlagPickup
  { flagPickup_carrierTeam :: Team
  , flagPickup_actor :: ActorID
  , flagPickup_flagPosition :: Fixed2
  , flagPickup_type :: FlagType
  }
  deriving (Generic)

data FlagCapture = FlagCapture
  { flagCapture_team :: Team
  -- ^ The team that scored
  , flagCapture_actorID :: ActorID
  , flagCapture_position :: Fixed2
  -- ^ The position of the overseer capturing the flag
  , flagCapture_velocity :: Fixed2
  -- ^ The velocity of the overseer capturing the flag
  }
  deriving (Generic)

data FlagRecovery = FlagRecovery
  { flagRecovery_team :: Team
  -- ^ The team that recovered their flag
  , flagRecovery_actorID :: Maybe ActorID
  -- ^ The player that recovered the flag, if any
  , flagRecovery_position :: Fixed2
  -- ^ The position of the flag being recovered
  }
  deriving (Generic)

data FlagType
  = BaseFlagType
  | DroppedFlagType
  | -- | Flag being carried by overseer
    OverseerFlagType
  deriving stock (Eq)
