module Pixelpusher.Game.Debug (
  DebugMode (..),
  DebugCommand (..),
  DebugSnapshotSlot (..),
  PauseState (..),
  DebugSnapshotLabels (..),
  DebugMarks (..),
  emptyDebugMarks,
  DebugMarkSlot (..),
  debugMarkSlotLens,
  markColor1,
  markColor2,
  markColor3,
  markColor4,
  markColor5,
  markColor6,
  resumeDelayTicks,
) where

import Data.Generics.Product.Fields
import Data.Serialize (Serialize)
import GHC.Generics
import Lens.Micro.Platform.Custom

import Pixelpusher.Custom.Fixed (Fixed2)
import Pixelpusher.Custom.Float (Float4 (..))
import Pixelpusher.Game.ActorID
import Pixelpusher.Game.Constants qualified as C
import Pixelpusher.Game.Time

data DebugMode = NoDebugMode | DebugMode
  deriving stock (Generic)
  deriving anyclass (Serialize)

data DebugCommand
  = DC_SaveSnapshot DebugSnapshotSlot
  | DC_LoadSnapshot DebugSnapshotSlot
  | DC_Pause
  | DC_Unpause
  | DC_SaveMark DebugMarkSlot Fixed2
  | DC_ClearMark DebugMarkSlot
  | DC_DamageDrones ActorID
  | DC_DamageOverseer ActorID
  | DC_SwitchTeams ActorID
  deriving stock (Generic)
  deriving anyclass (Serialize)

data DebugSnapshotSlot = Snapshot1 | Snapshot2 | Snapshot3
  deriving stock (Generic)
  deriving anyclass (Serialize)

data PauseState
  = Paused
  | Resuming SyncTime -- Time at which to transition to `Playing`
  | Playing
  deriving stock (Generic)
  deriving anyclass (Serialize)

data DebugSnapshotLabels = DebugSnapshotLabels
  { dsl_current :: Maybe (Int, Int)
  , dsl_slot1 :: Maybe Int
  , dsl_slot2 :: Maybe Int
  , dsl_slot3 :: Maybe Int
  }

data DebugMarks = DebugMarks
  { dm_mark1 :: Maybe Fixed2
  , dm_mark2 :: Maybe Fixed2
  , dm_mark3 :: Maybe Fixed2
  , dm_mark4 :: Maybe Fixed2
  , dm_mark5 :: Maybe Fixed2
  , dm_mark6 :: Maybe Fixed2
  }
  deriving stock (Generic)
  deriving anyclass (Serialize)

emptyDebugMarks :: DebugMarks
emptyDebugMarks =
  DebugMarks
    { dm_mark1 = Nothing
    , dm_mark2 = Nothing
    , dm_mark3 = Nothing
    , dm_mark4 = Nothing
    , dm_mark5 = Nothing
    , dm_mark6 = Nothing
    }

data DebugMarkSlot = Mark1 | Mark2 | Mark3 | Mark4 | Mark5 | Mark6
  deriving stock (Generic)
  deriving anyclass (Serialize)

debugMarkSlotLens :: DebugMarkSlot -> Lens' DebugMarks (Maybe Fixed2)
debugMarkSlotLens = \case
  Mark1 -> field @"dm_mark1"
  Mark2 -> field @"dm_mark2"
  Mark3 -> field @"dm_mark3"
  Mark4 -> field @"dm_mark4"
  Mark5 -> field @"dm_mark5"
  Mark6 -> field @"dm_mark6"

markColor1 :: Float4
markColor1 = Float4 0.0 1.0 1.0 0.5

markColor2 :: Float4
markColor2 = Float4 1.0 0.0 1.0 0.5

markColor3 :: Float4
markColor3 = Float4 1.0 1.0 0.0 0.5

markColor4 :: Float4
markColor4 = Float4 1.0 0.3 0.3 0.5

markColor5 :: Float4
markColor5 = Float4 0.3 1.0 0.3 0.5

markColor6 :: Float4
markColor6 = Float4 0.3 0.3 1.0 0.5

resumeDelayTicks :: Ticks
resumeDelayTicks = Ticks $ 3 * C.tickRate_hz
